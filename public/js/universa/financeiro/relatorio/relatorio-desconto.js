(function ($, window, document) {
    'use strict';

    var RelatorioDesconto = function () {
        VersaShared.call(this);
        var __relatorioDesconto = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                financeiroTituloTipo: '',
                acadgeralAreaConhecimento: '',
                acadCurso: '',
                orgCampus: '',
                meioPagamento: '',
                acessoPessoas: '',
                acadPeriodoLetivo: '',
                turmaAluno: '',
                descontoTipo: ''
            },
            data: {
                arrTituloEstado: [],
                arrDescontoSituacao: [],
                arrPeriodoAtual: [],
                pessoaLogada: [],
                arrRelatorios: []
            },
            value: {
                dataAtual: '',
                grupoUsuariosFiltro: ''
            },
            datatables: {},
            wizardElement: '#relatorio-desconto-wizard',
            formElement: '#relatorio-desconto-form'
        };

        this.retornaGrupoUsariosFiltro = function () {
            return __relatorioDesconto.options.value.grupoUsuariosFiltro || '';

        };

        this.setSteps = function () {
            $('#periodoLetivo').select2({
                language: 'pt-BR',
                allowerClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioDesconto.options.url.acadPeriodoLetivo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.per_nome, id: el.per_id};
                        });
                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $('#turma').select2('val', '').trigger('change');
                if (!$("#periodoLetivo").val()) {
                    $("#turma").select2('disable');
                } else {
                    $("#turma").select2('enable');
                }
            });

            if (__relatorioDesconto.options.data.arrPeriodoAtual) {
                $('#periodoLetivo').select2("data",
                    __relatorioDesconto.options.data.arrPeriodoAtual).trigger("change");
            }

            $("#tipoRelatorio").select2({
                data: [{id: 'sintetico', text: 'Sintético por Aluno'}],
                language: 'pt-BR'
            }).select2('val', 'sintetico').trigger("change");

            $('#turma').select2({
                language: 'pt-BR',
                allowerClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioDesconto.options.url.turmaAluno,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            perId: $("#periodoLetivo").val(),
                            cursocampusId: $("#curso").val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.turma_nome, id: el.turma_id};
                        });
                        return {results: transformed};
                    }
                }
            });

            $('#tiposDesconto').select2({
                language: 'pt-BR',
                allowerClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioDesconto.options.url.descontoTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.desctipoDescricao, id: el.desctipoId};
                        });
                        return {results: transformed};
                    }
                }
            });

            $("#estadoDesconto").select2({
                languagem: 'pt-BR',
                allowClear: true,
                data: [{id: 'Ativo', text: 'Ativo'}, {id: 'Inativo', text: 'Inativo'}]
            });

            var situacoes = [{id: "Sim", text: "Aplicado"}];
            situacoes = situacoes.concat(__relatorioDesconto.options.data.arrDescontoSituacao);

            $('#situacaoDesconto').select2({
                languagem: 'pt-BR',
                allowClear: true,
                multiple: true,
                data: situacoes
            });


            $("#areaId").select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioDesconto.options.url.acadgeralAreaConhecimento,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_descricao, id: el.area_id};
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $("#curso").select2('val', '').trigger("change");
            });

            $('#tipoTitulo').select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioDesconto.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.tipotitulo_nome, id: el.tipotitulo_id}
                        });
                        return {results: transformed};
                    }
                }


            });

            $("#estadoTitulo").select2({
                allowClear: true,
                data: __relatorioDesconto.options.data.arrTituloEstado,
                multiple: true,
                language: 'pt-BR'
            });

            $('#campusCurso').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioDesconto.options.url.orgCampus,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.camp_nome;
                            el.id = el.camp_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $("#curso").select2("val", "").trigger("change");

                if (!$('#campusCurso').val()) {
                    $("#curso").select2('disable');
                } else {
                    $("#curso").select2('enable');
                }


            });

            $('#curso').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioDesconto.options.url.acadCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            campus: $("#campusCurso").val(),
                            areaConhecimento: $("#areaId").val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.curso_nome;
                            el.id = el.curso_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $("#turma").select2("val", '').trigger("change");
            });

            $('#usuarioDesconto').select2({
                language: 'pt-BR',
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __relatorioDesconto.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            agruparPessoas: true,
                            grupo: __relatorioDesconto.retornaGrupoUsariosFiltro()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome || el.login;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            __relatorioDesconto.iniciarElementoDatePicker($('#vencimentoFim, #vencimentoInicial'));

            if (!$("#curso").val() && !$('#campusCurso').val()) {
                $("#curso").select2('disable');
            }
            if (!$("#turma").val() && !$('#periodoLetivo').val()) {
                $("#turma").select2('disable');
            }
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
        };
    };

    $("#limpar").click(function (event) {
        $('#areaId').select2('val', '').trigger('change');
        $('#campusCurso').select2('val', '').trigger('change');
        $('#curso').select2('val', '').trigger('change');
        $('#periodoLetivo').select2('val', '').trigger('change');
        $('#turma').select2('val', '').trigger('change');
        $('#tipoTitulo').select2('val', '').trigger('change');
        $('#estadoTitulo').select2('val', '').trigger('change');
        $('#tiposDesconto').select2('val', '').trigger('change');
        $('#situacaoDesconto').select2('val', '').trigger('change');
        $('#usuarioDesconto').select2('val', '').trigger('change');
        $('#estadoDesconto').select2('val', '').trigger('change');
        $('#vencimentoInicial').val('').trigger('change');
        $('#vencimentoFim').val('').trigger('change');
        $('#percentualInicial').val('').trigger('change');
        $('#percentualFinal').val('').trigger('change');

    });

    $.relatorioDesconto = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.relatorio.relatorio-desconto");

        if (!obj) {
            obj = new RelatorioDesconto();
            obj.run(params);
            $(window).data('universa.financeiro.relatorio.relatorio-desconto', obj);
        }

        return obj;
    };
})
(window.jQuery, window, document);