(function ($, window, document) {
    'use strict';

    var RelatorioSintetico = function () {
        VersaShared.call(this);
        var __relatorioSintetico = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                financeiroTituloTipo: '',
                acadgeralAreaConhecimento: '',
                acadCurso: '',
                orgCampus: '',
                orgAgenteEducacional: '',
                acessoPessoas: '',
                meioPagamento: '',
                nivel: '',
                unidade: ''
            },
            data: {
                arrRelatorios: []
            },
            value: {
                arrTituloEstado: null,
                submeterRelatorio: null
            },
            datatables: {},
            wizardElement: '#relatorio-sintetico-wizard',
            formElement: '#relatorio-sintetico-form',
            validator: null
        };

        this.filtrosRelatorio = {
            areaId: true,
            campId: true,
            cursoId: true,
            usuarioCadastroAluno: true,
            pesIdAgente: true,
            tipotituloId: true,
            meioDePagamento: true,
            tituloEstado: true,
            usuarioAutor: true,
            usuarioBaixa: true,
            tituloDataProcessamentoInicio: false,
            tituloDataProcessamentoFim: false,
            tituloDataVencimentoInicio: false,
            tituloDataVencimentoFim: false,
            tituloDataPagamentoInicio: false,
            tituloDataPagamentoFim: false,
            tituloDataBaixaInicio: false,
            tituloDataBaixaFim: false,
            alunoCadastroInicio: false,
            alunoCadastroFim: false,
            meioPag: false,
            agentesOpcao: false
        };

        this.steps = {};

        this.setSteps = function () {
            var data = [];
            var x = 1;

            while (x < 60) {
                data[x - 1] = {id: x, text: String(x)}
                x++;
            }


            $('#nivelEnsino').select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioSintetico.options.url.nivel,
                    dataType: 'json',
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.nivel_nome, id: el.nivel_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#unidadeId').select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioSintetico.options.url.unidade,
                    dataType: 'json',
                    data: function (query) {
                        return {
                            query: query,
                            campus: $("#campId").val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.unidade_nome, id: el.unidade_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#tituloParcela').select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                data: data
            });

            $('#meioDePagamento').select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioSintetico.options.url.meioPagamento,
                    dataType: 'json',
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.meioPagamentoDescricao, id: el.meioPagamentoId};
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#tipoRelatorio')
                .select2({
                    allowClear: true,
                    data: __relatorioSintetico.options.data.arrRelatorios || [],
                    minimumResultsForSearch: -1,
                    language: 'pt-BR'
                })
                .change(function () {
                    var data = $(this).select2('data') || {};
                    var filtros = data['filtros'] || {};
                    var ext = data['ext'] || [];


                    for (var chave in __relatorioSintetico.filtrosRelatorio) {
                        var isSelect2 = __relatorioSintetico.filtrosRelatorio[chave];

                        $('[name=' + chave + ']').prop('disabled', false).trigger('change');

                        if (typeof filtros[chave] != "undefined") {
                            if (isSelect2) {
                                $('[name=' + chave + ']').val(filtros[chave]).select2('disable').trigger('change');
                            } else {
                                $('[name=' + chave + ']').val(filtros[chave]).prop('disabled', true).trigger('change');
                            }
                        }
                    }

                    $('#gerar-xlsx, #gerar-pdf').hide();

                    if (ext.indexOf('pdf') != -1) {
                        $('#gerar-pdf').show();
                    }

                    if (ext.indexOf('xlsx') != -1) {
                        $('#gerar-xlsx').show();
                    }
                })
                .select2('val', 'financeiro_titulos')
                .trigger('change');

            $("#areaId").select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioSintetico.options.url.acadgeralAreaConhecimento,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_descricao, id: el.area_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#tituloEstado').select2({
                allowClear: true,
                data: __relatorioSintetico.options.data.arrTituloEstado,
                minimumInputLength: -1,
                multiple: true,
                language: 'pt-BR'
            });

            $("#tipotituloId").select2({
                language: 'pt-BR',
                multiple: true,
                minimumInputLength: -1,
                ajax: {
                    url: __relatorioSintetico.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.tipotitulo_nome;
                            el.id = el.tipotitulo_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#campId').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioSintetico.options.url.orgCampus,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.camp_nome;
                            el.id = el.camp_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on("change", function () {
                $('#unidadeId').select2('val', '');
                $('#unidadeId').select2('disable', true);

                if ($("#campId").val()) {
                    $('#unidadeId').select2('enable', true);
                }
            });

            $('#cursoId').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioSintetico.options.url.acadCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.curso_nome;
                            el.id = el.curso_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#usuarioAutor, #usuarioBaixa, #usuarioCadastroAluno').select2({
                language: 'pt-BR',
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __relatorioSintetico.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (pesquisa) {
                        return {query: pesquisa};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome || el.login;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#pesIdAgente').select2({
                language: 'pt-BR',
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __relatorioSintetico.options.url.orgAgenteEducacional,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });
            $('#periodoLetivo').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioSintetico.options.url.periodoLetivo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.per_id;
                            el.text = el.per_nome;
                            return el;
                        });
                        return {results: transformed};
                    }
                }

            }).on('change', function (e) {
                if (!$('#periodoLetivo').val()) {
                    $('#situacaoalunoPeriodoLetivo').select2('data', "").trigger('change');
                    $('#alunosTurma').select2('data', "").trigger('change');
                    $('#situacaoalunoPeriodoLetivo').select2('disable', true);
                    $('#alunosTurma').select2('disable', true);
                } else {
                    $('#situacaoalunoPeriodoLetivo').select2('enable', true);
                    $('#alunosTurma').select2('enable', true);
                }
            });

            $("#situacaoalunoPeriodoLetivo").select2({
                language: 'pt-BR',
                multiple: false,
                allowClear: true,
                data: __relatorioSintetico.options.data.arrSituacaoAlunoPeriodo
            });

            $('#alunosTurma').select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __relatorioSintetico.options.url.turmaAlunos,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            perId: $("#periodoLetivo").val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.turma_id;
                            el.text = el.turma_nome;
                            return el;
                        });
                        return {results: transformed};
                    }
                }
            });


            if (!$('#periodoLetivo').val()) {
                $('#situacaoalunoPeriodoLetivo').select2('disable', true);
                $('#alunosTurma').select2('disable', true);
            }
            __relatorioSintetico.iniciarElementoDatePicker($(
                '#tituloDataVencimentoInicio, #tituloDataVencimentoFim,' +
                '#tituloDataPagamentoInicio, #tituloDataPagamentoFim,' +
                '#tituloDataBaixaInicio, #tituloDataBaixaFim,' +
                '#alunoCadastroInicio, #alunoCadastroFim,' +
                '#tituloDataProcessamentoInicio, #tituloDataProcessamentoFim'
            ));

            $('#unidadeId').select2('disable',true);
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
        };
    };
    $("#limpar").click(function (event) {
        $('#areaId').select2('val', '').trigger('change');
        $('#campId').select2('val', '').trigger('change');
        $('#cursoId').select2('val', '').trigger('change');
        $('#tipotituloId').select2('val', '').trigger('change');
        $('#tituloEstado').select2('val', '').trigger('change');
        $('#usuarioAutor').select2('val', '').trigger('change');
        $('#usuarioBaixa').select2('val', '').trigger('change');
        $('#usuarioCadastroAluno').select2('val', '').trigger('change');
        $('#tituloDataVencimentoInicio').val('').trigger('change');
        $('#tituloDataVencimentoFim').val('').trigger('change');
        $('#tituloDataPagamentoInicio').val('').trigger('change');
        $('#tituloDataPagamentoFim').val('').trigger('change');
        $('#tituloDataBaixaInicio').val('').trigger('change');
        $('#tituloDataBaixaFim').val('').trigger('change');
        $('#alunoCadastroInicio').val('').trigger('change');
        $('#alunoCadastroFim').val('').trigger('change');
        $('#tituloDataProcessamentoInicio').val('').trigger('change');
        $('#tituloDataProcessamentoFim').val('').trigger('change');
        $('#meioDePagamento').val('').trigger('change');
        $("#tituloParcela").select2('val', '').trigger("change");
        $('#periodoLetivo').select2('val', '').trigger('change');
        $('#situacaoalunoPeriodoLetivo').select2('val', '').trigger('change');
        $('#alunosTurma').select2('val', '').trigger('change');
        $("#unidadeId").select2("val",'').trigger("change");
        $("#nivelEnsino").select2("val",'').trigger("change");

    });

    $.relatorioSintetico = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.relatorio.sintetico");

        if (!obj) {
            obj = new RelatorioSintetico();
            obj.run(params);
            $(window).data('universa.financeiro.relatorio.sintetico', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);