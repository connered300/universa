(function ($, window, document) {
    'use strict';
    var BoletosAluno = function () {
        VersaShared.call(this);

        var __boletosAluno = this;

        this.defaults = {
            ajax: true,
            url: {
                search: '',
                campusCurso: '',
                campus: '',
                curso: '',
                periodo: '',
                turma: '',
                exibeBoletos: '',
                financeiroTipoTitulo: ''
            },
            data: {
                arrSituacao: [],
                titulosRemovidos: []
            },
            datatables: {
                boletoAlunoTable: null,
                dataTableAlunoBoletoRemovidos: null
            },
            value: {
                dataAtual: ''
            }
        };

        this.setSteps = function () {
            var $search = $("input[type^='search']");
            var $campusCurso = $('#campusCurso');
            var $perId = $('#perId');
            var $cursoId = $('#cursoId');
            var $turmaId = $('#turmaId');
            var $situacao = $('#situacao');
            var $tipoTitulo = $('#tituloTipo');
            var $vencInicial = $('#vencTituloInicial');
            var $vencFinal = $('#vencTituloFinal');

            __boletosAluno.iniciarElementoDatePicker($vencInicial);
            __boletosAluno.iniciarElementoDatePicker($vencFinal);
            var dataAtual = __boletosAluno.options.value.dataAtual;

            $situacao.select2({
                allowCelar: true,
                data: __boletosAluno.options.data.arrSituacao
            });

            $perId.select2({
                allowClear: true,
                ajax: {
                    url: __boletosAluno.options.url.periodo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.per_nome, id: el.per_id};
                        });

                        transformed.push({text: 'Sem Período', id: 'null'});
                        transformed.push({text: 'Alunos Desligados', id: 'desligados'});

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $situacao.select2("enable");

                if ($perId.val() == 'desligados' || $perId.val() == 'null') {
                    if ($situacao.val()) {
                        __boletosAluno.showNotificacaoInfo("Não é possivel selecionar situação para esse período!");
                    }

                    $situacao.select2("disable");
                }

                $campusCurso.select2('val', '').trigger('change');
                $campusCurso.select2($perId.val() ? 'enable' : 'disable');
            });

            $campusCurso.select2({
                allowClear: true,
                ajax: {
                    url: __boletosAluno.options.url.campus,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var perId = $('#perId').select2('data') || {};
                        perId = perId['id'] || '';

                        return {
                            agruparPorCampus: true,
                            query: query,
                            perId: perId
                        }
                    },
                    results: function (data) {

                        var transformed = $.map(data, function (el) {
                            return {text: el.camp_nome, id: el.camp_id};
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $cursoId.select2('val', '').trigger('change');
                $cursoId.select2($campusCurso.val() ? 'enable' : 'disable');
            });

            $cursoId.select2({
                allowClear: true,
                ajax: {
                    url: __boletosAluno.options.url.curso,
                    dataType: 'json',
                    delay: 250,
                    data: function (e) {
                        var perId = $('#perId').select2('data') || {};
                        perId = perId['id'] || '';

                        if (perId['id'] == "desligados") {
                            perId = '';
                        }

                        return {
                            perId: perId,
                            cursoPossuiPeriodoLetivo: 'Sim',
                            campId: $campusCurso.val()
                        }
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.curso_nome, id: el.curso_id, cursocampus: el.cursocampus_id};
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $turmaId.select2('val', '').trigger('change');
                $turmaId.select2($cursoId.val() ? 'enable' : 'disable');
            });

            $tipoTitulo.select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __boletosAluno.options.url.financeiroTipoTitulo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.tipotitulo_id;
                            el.text = el.tipotitulo_nome;
                            return el;
                        });
                        return {results: transformed};
                    }
                }
            });

            $turmaId.select2({
                language: 'pt-BR',
                ajax: {
                    url: __boletosAluno.options.url.turma,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var perId = $('#perId').select2('data') || {};
                        perId = perId['id'] || '';

                        return {
                            query: query,
                            cursoId: $cursoId.val(),
                            perId: perId
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.turma_id;
                            el.text = el.turma_nome;
                            return el;
                        });
                        return {results: transformed};
                    }
                }
            }).on("change", function () {
                $("#situacao").val('').trigger('change');
            });

            $("#alunosFiltroExecutar").click(function (e) {
                if (!__boletosAluno.validaBusca()) {
                    e.stopImmediatePropagation();
                    return false;
                }

                __boletosAluno.options.data.titulosRemovidos = [];
                __boletosAluno.iniciarDataTablesRemovidos();
                __boletosAluno.atualizarListagem();
                __boletosAluno.options.data.titulosRemovidos = [];
            });

            $('#alunosFiltroLimpar').click(__boletosAluno.limparCampos);

            $('#gerarBoletos').on('click', function () {
                if (!__boletosAluno.options.datatables.boletoAlunoTable.api().row(0).data()) {
                    __boletosAluno.showNotificacaoInfo("Não há boletos disponíveis!");
                    return false;
                }

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja gerar os boletos destes alunos ? \nEsta Ação pode demorar de acordo com a quantidade de boletos!',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        __boletosAluno.addOverlay(
                            $('.card'),
                            "Aguarde..."
                        );
                        var arrFiltros = __boletosAluno.retornaFiltros(),
                            titulosNotIn = '';

                        if (JSON.stringify(arrFiltros['titulosNotIn']) != '[]') {
                            titulosNotIn = arrFiltros['titulosNotIn'].join(",")
                        }

                        var camp = arrFiltros.filter.camp_id ? '&camp_id=' + arrFiltros.filter.camp_id : '',
                            curso = arrFiltros.filter.curso_id ? '&curso_id=' + arrFiltros.filter.curso_id : '',
                            perId = arrFiltros.filter.per_id ? '&per_id=' + arrFiltros.filter.per_id : '',
                            situacao = arrFiltros.filter.situacao_id ? '&situacao_id=' + arrFiltros.filter.situacao_id : '',
                            turma = arrFiltros.filter.turma_id ? '&turma_id=' + arrFiltros.filter.turma_id : '',
                            tipoTitulo = arrFiltros.filter.tituloTipo ? '&tipotitulo_id=' + arrFiltros.filter.tituloTipo : '',
                            dataVencimentoInicial =
                                arrFiltros.filter.vencTituloInicial ? '&vencimento_inicial=' + arrFiltros.filter.vencTituloInicial : '',
                            dataVencimentoFinal = arrFiltros.filter.vencTituloFinal ? '&vencimento_final=' + arrFiltros.filter.vencTituloFinal : '';

                        var url = __boletosAluno.options.url.exibeBoletos + '?' +
                            camp + curso + perId + situacao + turma + dataVencimentoInicial +
                            dataVencimentoFinal + tipoTitulo +
                            (titulosNotIn ? '&titulosNotIn=' + titulosNotIn : '');

                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Desejar visualizar os boletos em uma nova aba?',
                            buttons: "[Não][Sim]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Sim") {
                                window.open(url, '_blank');
                                __boletosAluno.removeOverlay($('.card'));
                            } else {
                                location.href = url;
                            }
                        });
                    }
                });
            });

            $situacao.select2("enable");

            if ($perId.val() == 'desligados' || $perId.val() == 'null') {
                $situacao.select2('data', []).trigger("change");
                $situacao.select2("disable");
            }

            __boletosAluno.iniciarListagem();
            __boletosAluno.iniciarDataTablesRemovidos();

            if (dataAtual) {
                $vencFinal.val(dataAtual);
                $vencInicial.val(dataAtual);
            }

        };

        this.iniciarDataTablesRemovidos = function () {
            var cont = -1;
            var $tabela = $("#dataTableAlunoBoletoRemovidos");

            if ($.fn.DataTable.isDataTable("#dataTableAlunoBoletoRemovidos")) {
                try {
                    $tabela.DataTable().destroy();
                } catch (err) {
                }

                $tabela.DataTable().destroy();
            }

            __boletosAluno.options.datatables.dataTableAlunoBoletoRemovidos = $tabela.DataTable({
                    "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

                    "oTableTools": {
                        "aButtons": [],
                        "sSwfPath": "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                    },
                    autoWidth: true,
                    data: JSON.parse('{"draw":"1", "recordsTotal": 0, "recordsFiltered": 0, "data": []}'),
                    columnDefs: [
                        {name: "titulo_id", targets: ++cont, data: "titulo_id"},
                        {name: "matricula", targets: ++cont, data: "matricula"},
                        {name: "nome", targets: ++cont, data: "nome"},
                        {name: "tipotitulo_nome", targets: ++cont, data: "tipotitulo_nome"},
                        {name: "titulo_valor", targets: ++cont, data: "valor"},
                        {name: "titulo_data_vencimento", targets: ++cont, data: "vencimento"},
                        {
                            name: "acao",
                            targets: ++cont,
                            data: "acao",
                            searchable: false,
                            search: false,
                            orderable: false
                        }
                    ],
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum título removido!",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                }
            );
        };

        this.iniciaBotoesDataTables = function () {
            $('.boletoSelecao').on('click', function (e) {
                var row = $(this).parents('tr');

                __boletosAluno.addOverlay($(".row"));

                row.find('.boletoSelecao').remove();
                var data = __boletosAluno.options.datatables.boletoAlunoTable.api().row(row).data();

                if (data) {
                    var aluno = data.nome + ' / ' + data.matricula;

                    __boletosAluno.showNotificacaoInfo("O aluno/matricula: " + aluno + " foi adicionado a lista de boletos removidos!");

                    data['acao'] = '\
                <div class="text-center">\
                    <div class="btn-group">\
                        <button type="button" class="btn btn-xs btn-primary boletoAlunoRetornar">\
                            <i class="fa fa-plus-circle"></i>\
                        </button>\
                    </div>\
                </div>';

                    __boletosAluno.options.data.titulosRemovidos.push(data['titulo_id']);
                    __boletosAluno.options.datatables.dataTableAlunoBoletoRemovidos.row.add(data).draw();
                    __boletosAluno.atualizarListagem();
                    __boletosAluno.removeOverlay($(".row"));
                }
                e.stopImmediatePropagation();
            });

            $(".boletoAlunoRetornar").on('click', function (e) {
                __boletosAluno.addOverlay($(".row"));
                var row = $(this).parents('tr');
                var data = __boletosAluno.options.datatables.dataTableAlunoBoletoRemovidos.row(row).data();

                data['action'] = '\
                <div class="text-center">\
                    <div class="btn-group">\
                        <button type="button" class="btn btn-xs btn-danger financeiroTituloConfig-remove">\
                            <i class="fa fa-times fa-inverse"></i>\
                        </button>\
                    </div>\
                </div>';

                for (var i in __boletosAluno.options.data.titulosRemovidos) {
                    if (__boletosAluno.options.data.titulosRemovidos[i] == data['titulo_id']) {
                        delete __boletosAluno.options.data.titulosRemovidos[i];
                        break;
                    }
                }

                __boletosAluno.options.datatables.dataTableAlunoBoletoRemovidos.row(row).remove();
                __boletosAluno.options.datatables.dataTableAlunoBoletoRemovidos.draw();
                __boletosAluno.atualizarListagem();
                __boletosAluno.removeOverlay($(".row"));
                e.stopImmediatePropagation();
            });
        };

        this.limparCampos = function () {
            $('#campusCurso').select2('val', '').trigger('change');
            $('#cursoId').select2('val', '').trigger('change');
            $('#turmaId').select2('val', '').trigger('change');
            $('#perId').select2('val', '').trigger('change');
            $('#tituloTipo').select2('val', '').trigger('change');
            $("#vencTituloFinal").val(__boletosAluno.options.value.dataAtual);
            $("#vencTituloInicial").val(__boletosAluno.options.value.dataAtual);

            __boletosAluno.options.data.titulosRemovidos = [];
            __boletosAluno.iniciarDataTablesRemovidos();
            __boletosAluno.atualizarListagem();
        };

        this.retornaFiltros = function () {
            var d = [];
            d.filter = {};
            var campusCurso = $('#campusCurso').val() || "";
            var turma = $('#turmaId').val() || "";
            var cursoId = $('#cursoId').val() || "";
            var perId = $('#perId').select2('data') ? $('#perId').select2('data')['id'] : "";
            var tituloTipo = $('#tituloTipo').val();
            var vencTituloFinal = $("#vencTituloFinal").val();
            var vencTituloInicial = $("#vencTituloInicial").val();
            var situacao = $("#situacao").val();

            d.titulosNotIn = __boletosAluno.options.data.titulosRemovidos || [];

            if (vencTituloInicial) {
                d.filter['vencTituloInicial'] = vencTituloInicial;
            }

            if (vencTituloFinal) {
                d.filter['vencTituloFinal'] = vencTituloFinal;
            }

            if (tituloTipo) {
                d.filter['tituloTipo'] = tituloTipo;
            }

            if (campusCurso) {
                d.filter['camp_id'] = campusCurso;
            }

            if (turma) {
                d.filter['turma_id'] = turma;
            }

            if (cursoId) {
                d.filter['curso_id'] = cursoId;
            }

            if (perId) {
                d.filter['per_id'] = perId;
            }

            if (situacao) {
                d.filter['situacao_id'] = situacao
            }

            if (Object.keys(d.filter).length == 0) {
                d.filter['naoTrazerDados'] = true;
            }

            d.filter['trazerAlunosSemPeriodo'] = true;

            return d;
        };

        this.atualizarListagem = function () {
            __boletosAluno.addOverlay($(".card"));
            __boletosAluno.options.datatables.boletoAlunoTable.api().ajax.reload(null, false);
            __boletosAluno.iniciaBotoesDataTables();
        };

        this.iniciarListagem = function () {
            var $tabela = $("#dataTableAlunoBoleto");
            var cont = -1;

            __boletosAluno.options.datatables.boletoAlunoTable = $tabela.dataTable({
                    order: [[2, "asc"]],
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __boletosAluno.options.url.search,
                        type: "POST",
                        data: function (d) {
                            var arrFiltros = __boletosAluno.retornaFiltros();

                            d.filter = arrFiltros.filter || {};
                            d.index = true;
                            d.titulosNotIn = arrFiltros['titulosNotIn'];

                            return d;
                        },
                        dataSrc: function (json) {
                            var data = json.data;
                            for (var row in data) {
                                data[row].acao =
                                    '<div class="text-center">\
                                        <div class="btn-group">\
                                            <button type="button" class="btn btn-xs btn-danger boletoSelecao">\
                                                <i class="fa fa-times fa-inverse"></i>\
                                            </button>\
                                        </div>\
                                    </div>';
                                data[row]['vencimento'] = __boletosAluno.formatarData(data[row]['titulo_data_vencimento']);
                                data[row]['valor'] = __boletosAluno.formatarMoeda(data[row]['titulo_valor']);
                            }

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "titulo_id", targets: ++cont, data: "titulo_id"},
                        {name: "matricula", targets: ++cont, data: "matricula"},
                        {name: "nome", targets: ++cont, data: "nome"},
                        {name: "tipotitulo_nome", targets: ++cont, data: "tipotitulo_nome"},
                        {name: "titulo_valor", targets: ++cont, data: "valor"},
                        {name: "titulo_data_vencimento", targets: ++cont, data: "vencimento"},
                        {
                            name: "acao",
                            targets: ++cont,
                            data: "acao",
                            searchable: false,
                            search: false,
                            orderable: false
                        }
                    ],
                    "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

                    "oTableTools": {
                        "aButtons": [],
                        "sSwfPath": "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                    },
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Combine os filtros para trazer resultados!",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                }
            ).on('draw.dt', function (e) {
                    __boletosAluno.iniciaBotoesDataTables();
                    __boletosAluno.removeOverlay($(".card"));
                    $("#gerarBoletos").prop("disabled", true);

                    if (__boletosAluno.options.datatables.boletoAlunoTable.api().row(0).data()) {
                        $("#gerarBoletos").prop("disabled", false);
                    }
                })
        };
        this.formatarData = function (data) {
            if (!data) {
                return ' - '
            }
            data = data.split("-");
            return data[2].slice(0, 2) + '/' + data[1] + '/' + data[0];
        };

        this.validaBusca = function () {
            var vencimentoInicial = $('#vencTituloInicial').val();
            var vencimentoFinal = $('#vencTituloFinal').val();
            const umPeriodoMaximo = 2678400000 * 2;

            if (!vencimentoInicial || !vencimentoFinal) {
                __boletosAluno.showNotificacaoInfo("Favor preencher os dois campos de data!");
                return false;
            }

            var dataAtual = __boletosAluno.formatDate(__boletosAluno.options.value.dataAtual, 'yy-mm-dd'),
                msgErro = '';
            vencimentoInicial = __boletosAluno.formatDate(vencimentoInicial, 'yy-mm-dd');
            vencimentoFinal = __boletosAluno.formatDate(vencimentoFinal, 'yy-mm-dd');

            vencimentoInicial = new Date(vencimentoInicial).getTime();
            vencimentoFinal = new Date(vencimentoFinal).getTime();
            dataAtual = new Date(dataAtual).getTime();

            if (vencimentoInicial < dataAtual) {
                msgErro += 'Vencimento inicial inválido!</br>';
            }

            if (vencimentoFinal > (vencimentoInicial + umPeriodoMaximo)) {
                msgErro += 'Vencimento Final inválido!</br>';
            }

            if (msgErro) {
                __boletosAluno.showNotificacaoWarning("Erros:\n" + msgErro);
                __boletosAluno.showNotificacaoInfo(
                    "O vencimento inicial deve ser maior ou igual a data atual!</br>" +
                    "Já o final deve ser maior (no máximo máximo 2 mêses) ou igual a data atual!");
                return false;
            }

            return true;
        };

        this.run = function (opts) {
            __boletosAluno.setDefaults(opts);
            __boletosAluno.setSteps();
        };
    };

    $.boletosAluno = function (params) {
        params = params || [];
        var obj = $(window).data("universa.financeiro.relatorio.boletos-aluno");

        if (!obj) {
            obj = new BoletosAluno();
            obj.run(params);
            $(window).data('universa.financeiro.relatorio.boletos-aluno', obj);
        }

        return obj;
    };
})
(window.jQuery, window, document);