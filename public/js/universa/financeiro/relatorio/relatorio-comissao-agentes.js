(function ($, window, document) {
    'use strict';

    var RelatorioComissaoAgente = function () {
        VersaShared.call(this);
        var __relatorioComissaoAgente = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                financeiroTituloTipo: '',
                acadgeralAreaConhecimento: '',
                nivelEnsino: ''
            },
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#relatorio-comissao-agente-wizard',
            formElement: '#relatorio-comissao-agente-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#nivelEnsino").select2({
                multiple: true,
                ajax: {
                    url: __relatorioComissaoAgente.options.url.nivelEnsino,
                    dataType: 'json',
                    data: function (query) {
                        return {query: query}
                    },
                    results: function (data) {
                        var resultado = $.map(data, function (elements) {
                            elements.text = elements.nivelNome;
                            elements.id = elements.nivelId;
                            return elements;
                        });
                        return {results: resultado};
                    }
                }
            });

            $('#agente').select2({
                ajax: {
                    url: __relatorioComissaoAgente.options.url.orgAgenteEducacional,
                    dataType: 'json',
                    data: function (query) {
                        return {query: query}
                    },
                    results: function (data) {
                        var resultado = $.map(data, function (elements) {
                            elements.text = elements.pes_nome;
                            elements.id = elements.pes_id;
                            return elements;
                        });
                        return {results: resultado};
                    }
                },
                multiple: true
            });

            $('#tipoTitulo').select2({
                ajax: {
                    url: __relatorioComissaoAgente.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    data: function (query) {
                        return {query: query}
                    },
                    results: function (data) {
                        var resultado = $.map(data, function (elements) {
                            elements.text = elements.tipotitulo_nome;
                            elements.id = elements.tipotitulo_id;
                            return elements;
                        });
                        return {results: resultado};
                    }
                },
                multiple: true
            });

            var arrTipos = [
                {'id': 'comissao', 'text': 'Relatório Por Comissão'},
                {'id': 'valorPago', 'text': 'Relatório Por Valor Pago'}
            ];

            $("#relatorioTipo").select2({data: arrTipos});
            $("#relatorioTipo").select2('val', 'comissonario').trigger("change");

            __relatorioComissaoAgente.iniciarElementoDatePicker($('#dataInicio, #dataFinal'));
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
        };
    };

    $.relatorioComissaoAgente = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.relatorio.comissao-agente");

        if (!obj) {
            obj = new RelatorioComissaoAgente();
            obj.run(params);
            $(window).data('universa.financeiro.relatorio.comissao-agente', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);