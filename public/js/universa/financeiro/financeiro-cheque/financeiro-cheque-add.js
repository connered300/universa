(function ($, window) {
    'use strict';

    var FinanceiroChequeAdd = function () {
        VersaShared.call(this);
        var __financeiroChequeAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                boletoBanco: '',
                pessoa: '',
                buscaCEP: ''
            },
            data: {},
            value: {
                pes: ''
            },
            datatables: {},
            wizardElement: '#financeiro-cheque-wizard',
            formElement: '#financeiro-cheque-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $form = $('#financeiro-cheque-form');

            var $pes = $form.find("[name=pes]");
            var $chequeEmitente = $form.find("[name=chequeEmitente]");

            __financeiroChequeAdd.iniciarElementoDatePicker(
                $form.find("[name=chequeEmissao], [name=chequeVencimento], [name=chequeCompensado]")
            );
            $form.find("[name=chequeValor]").inputmask({
                showMaskOnHover: false,
                alias: 'currency',
                groupSeparator: "",
                radixPoint: ".",
                placeholder: "0",
                allowMinus: false,
                prefix: ""
            });
            $form.find('[name=chequeBanco]').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: __financeiroChequeAdd.options.url.boletoBanco,
                        data: {query: request.term},
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            var transformed = $.map(data || [], function (el) {
                                return {label: el.banc_nome, id: el.banc_id};
                            });

                            if (Object.keys(transformed).length == 0) {
                                transformed.push({label: '-'});
                            }

                            response(transformed);
                        }
                    });
                },
                minLength: 1
            });

            $pes.select2({
                language: 'pt-BR',
                minimumInputLength: 1,
                ajax: {
                    url: __financeiroChequeAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query, pesquisaPFPJ: true};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_nome, id: el.pes_id};
                        });

                        return {results: transformed};
                    }
                }
            }).change(function () {
                var data = $(this).select2('data') || {};
                $chequeEmitente.val(data['text'] || '');
            });

            $('#pes').select2("data", __financeiroChequeAdd.getPes());
        };

        this.setValidations = function () {
            __financeiroChequeAdd.options.validator.settings.rules = {
                chequeEmitente: {maxlength: 255, required: true}, chequeNum: {maxlength: 45, required: true},
                chequeBanco: {maxlength: 45, required: true}, chequeAgencia: {maxlength: 45, required: true},
                chequePraca: {maxlength: 45, required: true}, chequeEmissao: {required: true, dateBR: true},
                chequeVencimento: {required: true, dateBR: true}, chequeCompensado: {dateBR: true},
                chequeValor: {maxlength: 10, number: true, required: true}, chequeConta: {maxlength: 45}
            };
            __financeiroChequeAdd.options.validator.settings.messages = {
                chequeEmitente: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                chequeNum: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                chequeBanco: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                chequeAgencia: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                chequePraca: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                chequeEmissao: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                chequeVencimento: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                chequeCompensado: {dateBR: 'Informe uma data válida!'},
                chequeValor: {maxlength: 'Tamanho máximo: 10!', required: 'Campo obrigatório!'},
                chequeConta: {maxlength: 'Tamanho máximo: 45!'}
            };

            $(__financeiroChequeAdd.options.formElement).submit(function (e) {
                var ok = $(__financeiroChequeAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__financeiroChequeAdd.options.formElement);

                if (__financeiroChequeAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __financeiroChequeAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __financeiroChequeAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__financeiroChequeAdd.options.listagem) {
                                    $.financeiroChequeIndex().reloadDataTableFinanceiroCheque();
                                    __financeiroChequeAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#financeiro-cheque-modal').modal('hide');
                                }

                                __financeiroChequeAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaFinanceiroCheque = function (cheque_id, callback) {
            var $form = $(__financeiroChequeAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __financeiroChequeAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + cheque_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroChequeAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __financeiroChequeAdd.removeOverlay($form);
                },
                erro: function () {
                    __financeiroChequeAdd.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroChequeAdd.removeOverlay($form);
                }
            });
        };

        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.financeiroChequeAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-cheque.add");

        if (!obj) {
            obj = new FinanceiroChequeAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-cheque.add', obj);
        }

        return obj;
    };
})(window.jQuery, window);