(function ($, window, document) {
    'use strict';
    var FinanceiroChequeIndex = function () {
        VersaShared.call(this);
        var __financeiroChequeIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: '',
                info: ''
            },
            value: {
                permissaoAdicionarCheque: false,
                permissaoEditaCheque: false
            },
            datatables: {
                financeiroCheque: null,
                financeiroChequeInfo: null
            }
        };

        this.setSteps = function () {
            var btnNovoCheque = $("#btn-financeiro-cheque-add");
            var btnEditaCheque = $(".btn-edita-observacao");

            btnNovoCheque.addClass("hidden");
            btnEditaCheque.addClass("hidden");

            if (__financeiroChequeIndex.options.value.permissaoAdicionarCheque) {
                btnNovoCheque.removeClass("hidden");
            }
            if (__financeiroChequeIndex.options.value.permissaoEditaCheque) {
                btnEditaCheque.removeClass("hidden");
            }

            var colNum = 0;
            __financeiroChequeIndex.options.datatables.financeiroCheque = $('#dataTableFinanceiroCheque').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __financeiroChequeIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-info financeiroCheque-view" title="Informações do cheque">\
                                    <i class="fa fa-list-alt fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['cheque_data_cadastro_formatado'] = __financeiroChequeIndex.formatDate(
                                data[row]['cheque_data_cadastro']
                            );
                            data[row]['cheque_vencimento_formatado'] = __financeiroChequeIndex.formatDate(
                                data[row]['cheque_vencimento']
                            );

                            data[row]['dadosBanco'] = 'Banco: ' + data[row]['banco'] + ' | Agência: ' + data[row]['cheque_agencia'] + ' | Conta: ' +
                                data[row]['cheque_conta'] + ' | Número: ' + data[row]['cheque_num'];

                            data[row]['acao'] = btnGroup;
                        }

                        __financeiroChequeIndex.removeOverlay($('#container-financeiro-cheque'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "cheque_id", targets: colNum++, data: "cheque_id"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},//aluno/pessoa
                    {name: "cheque_emitente", targets: colNum++, data: "cheque_emitente"},
                    {name: "banco", targets: colNum++, data: "dadosBanco", className: "text-right"},
                    {name: "cheque_valor", targets: colNum++, data: "cheque_valor", className: "text-right"},
                    {name: "usuario_id", targets: colNum++, data: "usuario_login"},
                    {name: "cheque_data_cadastro", targets: colNum++, data: "cheque_data_cadastro_formatado"},
                    {name: "cheque_vencimento", targets: colNum++, data: "cheque_vencimento_formatado"},
                    {name: "qtd", targets: colNum++, data: "qtd", className: "text-right"},
                    {name: "cheque_id", targets: colNum++, data: "acao"}
                ],
                "dom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[6, 'desc']]
            });

            $('#dataTableFinanceiroCheque').on('click', '.financeiroCheque-view', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroChequeIndex.options.datatables.financeiroCheque.fnGetData($pai);
                var $modal = $('#modal-financeiro-cheque');
                var divObservacaoEdicao = $(".edicaoObservacao");

                $(".chequeId").val(data['cheque_id']);

                __financeiroChequeIndex.addOverlay($("#modal-financeiro-cheque"));
                $('#chequePes-static').html(data['pes_nome']);
                $('#agencia-static').html(data['cheque_agencia']);
                $('#conta-static').html(data['cheque_conta']);
                $('#numero-static').html(data['cheque_num']);
                $('#emitente-static').html(data['cheque_emitente']);
                $('#pesNome-static').html(data['pes_nome']);
                $('#praca-static').html(data['cheque_praca']);
                $('#dataEmissao-static').html(__financeiroChequeIndex.formatDate(data['cheque_emissao']));
                $('#dataVencimento-static').html(__financeiroChequeIndex.formatDate(data['cheque_vencimento']));
                $('#chequeValor-static').html(data['cheque_valor']);
                $('#usarioCadastro-static').html(data['usuario_login']);
                $('#chequeObservacao-static').html(data['cheque_observacao']);

                if (__financeiroChequeIndex.options.datatables.financeiroChequeInfo) {
                    $('#dataTableFinanceiroChequeInformacoes').DataTable().destroy();
                }

                divObservacaoEdicao.addClass("hidden");

                var colNum = 0;
                __financeiroChequeIndex.options.datatables.financeiroChequeInfo = $('#dataTableFinanceiroChequeInformacoes').dataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    lengthChange: false,
                    ajax: {
                        url: __financeiroChequeIndex.options.url.info,
                        type: "POST",
                        data: function (query) {
                            return {
                                query: query,
                                chequeId: data['cheque_id']
                            };
                        },
                        dataSrc: function (json) {
                            var data = json.data;
                            var totalizacoes = json['somatorio'] || {};
                            var $chequeExtra = $('#dataTableFinanceiroTituloTotais');

                            $chequeExtra.hide();

                            if (Object.keys(totalizacoes).length > 0) {
                                $chequeExtra.show();

                                var conteudo = '';
                                conteudo += 'Quantidade de títulos vinculados: <b>' +
                                    totalizacoes['quantidade_titulos_vinculados'] +
                                    '</b> &nbsp; ';
                                conteudo += 'Valor cheque: <b>' +
                                    __financeiroChequeIndex.formatarMoeda(totalizacoes['cheque_valor']) +
                                    '</b> &nbsp; ';
                                conteudo += 'Valor utilizado: <b>' +
                                    __financeiroChequeIndex.formatarMoeda(totalizacoes['valorUtilizado']) +
                                    '</b> &nbsp; ';
                                conteudo += 'Saldo: <b>' +
                                    __financeiroChequeIndex.formatarMoeda(totalizacoes['saldoCheque']) +
                                    '</b> &nbsp; ';

                                $chequeExtra.html(conteudo);
                            }

                            for (var row in data) {
                                data[row]['valor_utilizado_formatado'] = __financeiroChequeIndex.formatarMoeda(
                                    data[row]['valor_utilizado']
                                );

                                if (data[row]['titulocheque_estado'] != 'Ativo') {
                                    data[row]['valor_utilizado_formatado'] = (
                                        '<b>(Estornado)</b> ' + data[row]['valor_utilizado_formatado']
                                    );
                                }
                            }

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "aluno", targets: colNum++, data: "alunoNome"},
                        {name: "titulo", targets: colNum++, data: "tituloId"},
                        {name: "descricao_titulo", targets: colNum++, data: "descricaoTitulo"},
                        {name: "valor_titulo", targets: colNum++, data: "valorPago", className: "text-right"},
                        {
                            name: "valor_utilizado",
                            targets: colNum++,
                            data: "valor_utilizado_formatado",
                            className: "text-right"
                        },
                        {name: "usuario_cadastro", targets: colNum++, data: "usuarioCadastro"}
                    ],
                    "dom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

                $modal.modal('show');

                __financeiroChequeIndex.removeOverlay($("#modal-financeiro-cheque"));

                btnEditaCheque.on("click", function (e) {
                    $("#chequeObservacao").val($("#chequeObservacao-static").text());
                    divObservacaoEdicao.removeClass("hidden");

                    e.stopImmediatePropagation();
                    return false;
                });

                $("#salvarObsevacao").on("click",function (e) {
                    var novaObservacao = $("#chequeObservacao").val();
                    var chequeId = $(".chequeId").val();
                    var arrDados = [];

                    if (novaObservacao == $("#chequeObservacao-static").text()) {
                        __financeiroChequeIndex.showNotificacaoInfo("Observações identicas, forneça uma obsevação diferente!");

                        e.stopImmediatePropagation();
                        return false;
                    }

                    if(!chequeId){
                        __financeiroChequeIndex.showNotificacaoWarning("Cheque não identificado, atualize a página! \n Se o problema persistir contate o suporte!");
                        e.stopImmediatePropagation();
                        return false;
                    }

                    arrDados = {
                        'id': chequeId,
                        'chequeObservacao': novaObservacao,
                        'editaObservacao': true
                    };

                    __financeiroChequeIndex.addOverlay($(".modal-content"), "Aguarde...");
                    $.ajax({
                        url: __financeiroChequeIndex.options.url.edit,
                        type: 'POST',
                        dataType: 'json',
                        data: arrDados,
                        success: function (data) {
                            if (data.erro) {

                                __financeiroChequeIndex.showNotificacaoWarning(
                                    data['mensagem'] || "Não foi possível salvar a observação!"
                                )
                            } else {
                                __financeiroChequeIndex.showNotificacaoSuccess(
                                    data['mensagem'] || "Observação salva!"
                                );

                                $("#chequeObservacao-static").text(novaObservacao);
                                divObservacaoEdicao.addClass("hidden");
                            }
                        }
                    }).done(function () {
                        __financeiroChequeIndex.removeOverlay($(".modal-content"));
                    });

                    e.stopImmediatePropagation();
                });

                $("#cancelarEdicaoObservacao").on("click", function (e) {
                    divObservacaoEdicao.addClass("hidden");
                    e.stopImmediatePropagation();
                });

            });

            $('#btn-financeiro-cheque-add').click(function (e) {
                if (__financeiroChequeIndex.options.ajax) {
                    __financeiroChequeIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-cheque-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['chequeId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['chequeId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#usuario").select2('data', data['usuario']).trigger("change");

            $('#financeiro-cheque-modal .modal-title').html(actionTitle + ' cheque');
            $('#financeiro-cheque-modal').modal();
            __financeiroChequeIndex.removeOverlay($('#container-financeiro-cheque'));
        };

        this.reloadDataTableFinanceiroCheque = function () {
            this.getDataTableFinanceiroCheque().api().ajax.reload(null, false);
        };

        this.getDataTableFinanceiroCheque = function () {
            if (!__financeiroChequeIndex.options.datatables.financeiroCheque) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroCheque')) {
                    __financeiroChequeIndex.options.datatables.financeiroCheque =
                        $('#dataTableFinanceiroCheque').DataTable();
                } else {
                    __financeiroChequeIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroChequeIndex.options.datatables.financeiroCheque;
        };

        this.run = function (opts) {
            __financeiroChequeIndex.setDefaults(opts);
            __financeiroChequeIndex.setSteps();
        };
    };

    $.financeiroChequeIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-cheque.index");

        if (!obj) {
            obj = new FinanceiroChequeIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-cheque.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);