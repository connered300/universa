(function ($, window, document) {
    'use strict';
    var FinanceiroRetornoIndex = function () {
        VersaShared.call(this);
        var __financeiroRetornoIndex = this;
        this.defaults = {
            url: {
                urlBaixa: '',
                search: '',
                searchDetalhes: '',
                download: ''
            },
            dataTables: {
                tableBoletos: undefined,
                financeiroRetorno: undefined
            },
            data: {},
            value: {
                retornoId: -1
            },
            formElement: '#efetua-baixa'
        };

        this.setFields = function () {
            var colNum = -1;

            $('#dataTableFinanceiroRetorno').dataTable({
                processing: true,
                serverSide: true,
                searching: false,
                ajax: {
                    url: __financeiroRetornoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnsStr = __financeiroRetornoIndex.createBtnGroup([
                            {
                                class: 'btn-primary btn-retorno-detalhe-view', icon: 'fa-eye',
                                title: 'Visualizar detalhes processados'
                            }
                        ]);

                        for (var row in data) {
                            data[row]['arquivo'] = 'Sem Arquivo';

                            if (data[row]['arq_chave']) {
                                var url = __financeiroRetornoIndex.options.url.download + '/' + data[row]['arq_chave'];
                                data[row]['arquivo'] = '<a href="' + url + '">' + data[row]['arq_nome'] + '</a>';
                            }

                            data[row]['retorno_data_formatada'] = __financeiroRetornoIndex.formatDate(
                                data[row]['retorno_data']
                            );
                            data[row]['btn-action'] = btnsStr;
                        }

                        return data;
                    }
                },
                columnDefs: [
                    {name: "retorno_id", targets: ++colNum, data: "retorno_id"},
                    {name: "arq_id", targets: ++colNum, data: "arquivo"},
                    {name: "retorno_data", targets: ++colNum, data: "retorno_data_formatada"},
                    {name: "retorno_status", targets: ++colNum, data: "retorno_status"},
                    {name: "btn-action", targets: ++colNum, data: "btn-action"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[2, 'desc']]
            });

            var $dataTableDetalhe = $("#financeiroDetalhe");

            $dataTableDetalhe.dataTable({
                processing: true,
                serverSide: true,
                searching: false,
                ajax: {
                    url: __financeiroRetornoIndex.options.url.searchDetalhes,
                    type: "POST",
                    data: function (d) {
                        __financeiroRetornoIndex.addOverlay($dataTableDetalhe, 'Carregando dados...');
                        d.retornoId = __financeiroRetornoIndex.options.value.retornoId || '-1';

                        return d;
                    },
                    dataSrc: function (json) {
                        __financeiroRetornoIndex.removeOverlay($dataTableDetalhe);

                        return json.data || [];
                    }
                },
                columns: [
                    {name: "detalhe_id", data: "detalhe_id"},
                    {name: "detalhe_nosso_numero", data: "detalhe_nosso_numero"},
                    {name: "detalhe_valor_pago", data: "detalhe_valor_pago"},
                    {name: "detalhe_status", data: "detalhe_status"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[2, 'desc']]
            });

            $(document).on('click', '.btn-retorno-detalhe-view', function () {
                var $modal = $("#retorno-detalhe-modal");
                var $pai = $(this).closest('tr');
                var dados = $('#dataTableFinanceiroRetorno').dataTable().fnGetData($pai);

                __financeiroRetornoIndex.options.value.retornoId = dados['retorno_id'] || '-1';

                $dataTableDetalhe.dataTable().api().ajax.reload();

                $modal.modal('show');
            });

            $('#retorno').change(function () {
                var content = $(this).val();

                if (content != null) {
                    var tipo = content.slice(content.lastIndexOf('.'), content.length);

                    switch (tipo.toLowerCase()) {
                        case ".rem":
                            __financeiroRetornoIndex.showNotification({
                                content: 'Favor inserir arquivo de retorno (.RET) ao invés de um arquivo de remessa (.REM)',
                                title: 'Formato inválido',
                                warning: 'warning',
                                timeout: 9000
                            });
                            break;
                        case ".ret":
                            $('#nome-arquivo').val(content);
                            if (!$('#auto-baixa').prop('checked') && $('#retorno').val() != '') {
                                $('.opcao').show(300);
                                $('#salvar').show(800);
                            } else {
                                $('#efetua-baixa').submit();
                            }
                            break;
                        default:
                            __financeiroRetornoIndex.showNotification({
                                content: 'Arquivo selecionado não é de retorno nem de remessa',
                                title: 'Formato inválido',
                                warning: 'danger',
                                timeout: 5000
                            });
                    }
                }
            });

            $(document).on('click', '#auto-baixa', function () {
                if ($(this).prop('checked')) {
                    $('#salvar, .opcao').hide(300);
                    var content = $('#retorno').val();
                    if (content != '') {
                        $('#efetua-baixa').submit();
                    }
                }
            });

            $(__financeiroRetornoIndex.options.formElement).submit(function (e) {
                var $form = $(__financeiroRetornoIndex.options.formElement);
                var data = new FormData();

                $('.ajax-loader').show();
                $('.default-content').hide();

                $.each(jQuery('#retorno')[0].files, function (i, file) {
                    data.append('file-' + i, file);
                });

                data.append('ajax', true);

                $.ajax({
                    url: $form.attr('action'),
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.ajax-loader').hide();
                        $('.default-content').show();

                        if (data.error == true) {
                            __financeiroRetornoIndex.showNotificacaoDanger(data['message']);
                            return false;
                        } else {
                            __financeiroRetornoIndex.showNotificacaoSuccess(data['message']);
                            __financeiroRetornoIndex.reloadDataTables();
                        }
                    }
                });
                e.preventDefault();
                e.stopPropagation();
            });

            $(document).on('click', '.panel-heading', function () {
                var id = $(this).attr('data-id');
                $('#' + id).toggle(400);
            });
        };

        this.reloadDataTables = function () {
            $("#dataTableFinanceiroRetorno").dataTable().api().ajax.reload();
        };

        this.run = function (opts) {
            __financeiroRetornoIndex.setDefaults(opts);
            __financeiroRetornoIndex.setFields();
        };

        this.showNotification = function (param) {
            var options = $.extend(
                true,
                {
                    content: '',
                    title: '',
                    type: 'info',
                    icon: '',
                    color: '',
                    timeout: false
                },
                param
            );

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
                case 'custom':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || '';
                    options.color = '#2f2929';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        };
    };

    $.financeiroRetornoIndex = function (params) {
        params = params || [];

        var obj = $(window).data('universa.financeiro.retorno.index');

        if (!obj) {
            obj = new FinanceiroRetornoIndex();
            obj.run(params);
            $(window).data('universa.financeiro.retorno.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);