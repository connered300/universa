(function ($, window, document) {
    'use strict';
    var FinanceiroTituloTipoIndex = function () {
        VersaShared.call(this);
        var __financeiroTituloTipoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: '',
                prioridade:''
            },
            datatables: {
                financeiroTituloTipo: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __financeiroTituloTipoIndex.options.datatables.financeiroTituloTipo =
                $('#dataTableFinanceiroTituloTipo').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __financeiroTituloTipoIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                            <button type="button" class="btn btn-xs btn-warning financeiroTituloTipo-prioridade-up" title="Aumentar prioridade!">\
                                    <i class="fa fa-arrow-up fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-warning financeiroTituloTipo-prioridade-down" title="Diminuir prioridade!">\
                                    <i class="fa fa-arrow-down fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-primary financeiroTituloTipo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger financeiroTituloTipo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __financeiroTituloTipoIndex.removeOverlay($('#container-financeiro-titulo-tipo'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "tipotitulo_id", targets: colNum++, data: "tipotitulo_id"},
                        {name: "tipotitulo_nome", targets: colNum++, data: "tipotitulo_nome"},
                        {name: "tipotitulo_descricao", targets: colNum++, data: "tipotitulo_descricao"},
                        {name: "tipotitulo_tempo_vencimento", targets: colNum++, data: "tipotitulo_tempo_vencimento"},
                        {
                            name: "tipotitulo_tempo_vencimento_tipo", targets: colNum++,
                            data: "tipotitulo_tempo_vencimento_tipo"
                        },
                        {name: "tipotitulo_matricula_emissao", targets: colNum++, data: "tipotitulo_matricula_emissao"},
                        {name: "prioridade", targets: colNum++, data: "prioridade"},
                        {name: "tipotitulo_id", targets: colNum++, data: "acao", orderable: false, searchable: false}
                    ],
                    "dom": "fr" +
                           "t" +
                           "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[6, 'asc']]
                });

            $('#dataTableFinanceiroTituloTipo').on('click', '.financeiroTituloTipo-prioridade-up', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroTituloTipoIndex.options.datatables.financeiroTituloTipo.fnGetData($pai);
                data['acao'] = 'up';

                __financeiroTituloTipoIndex.addOverlay($("#container-financeiro-titulo-tipo"));

                $.ajax({
                    url: __financeiroTituloTipoIndex.options.url.prioridade,
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        __financeiroTituloTipoIndex.reloadDataTableFinanceiroTituloTipo();
                        __financeiroTituloTipoIndex.removeOverlay($("#container-financeiro-titulo-tipo"));
                    }
                });

            });

            $('#dataTableFinanceiroTituloTipo').on('click', '.financeiroTituloTipo-prioridade-down', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroTituloTipoIndex.options.datatables.financeiroTituloTipo.fnGetData($pai);
                data['acao'] = 'down';

                __financeiroTituloTipoIndex.addOverlay($("#container-financeiro-titulo-tipo"));

                $.ajax({
                    url: __financeiroTituloTipoIndex.options.url.prioridade,
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        __financeiroTituloTipoIndex.reloadDataTableFinanceiroTituloTipo();
                        __financeiroTituloTipoIndex.removeOverlay($("#container-financeiro-titulo-tipo"));
                    }
                });
            });


            $('#dataTableFinanceiroTituloTipo').on('click', '.financeiroTituloTipo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroTituloTipoIndex.options.datatables.financeiroTituloTipo.fnGetData($pai);

                if (!__financeiroTituloTipoIndex.options.ajax) {
                    location.href = __financeiroTituloTipoIndex.options.url.edit + '/' + data['tipotitulo_id'];
                } else {
                    $.financeiroTituloTipoAdd().pesquisaFinanceiroTituloTipo(
                        data['tipotitulo_id'],
                        function (data) { __financeiroTituloTipoIndex.showModal(data); }
                    );
                }
            });


            $('#btn-financeiro-titulo-tipo-add').click(function (e) {
                if (__financeiroTituloTipoIndex.options.ajax) {
                    __financeiroTituloTipoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableFinanceiroTituloTipo').on('click', '.financeiroTituloTipo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __financeiroTituloTipoIndex.options.datatables.financeiroTituloTipo.fnGetData($pai);
                var arrRemover = {
                    tipotituloId: data['tipotitulo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de tipo de título "' + data['tipotitulo_descricao'] +
                             '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __financeiroTituloTipoIndex.addOverlay(
                                $('#container-financeiro-titulo-tipo'),
                                'Aguarde, solicitando remoção de registro de tipo de título...'
                            );
                            $.ajax({
                                url: __financeiroTituloTipoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __financeiroTituloTipoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de tipo de título:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __financeiroTituloTipoIndex.removeOverlay($('#container-financeiro-titulo-tipo'));
                                    } else {
                                        __financeiroTituloTipoIndex.reloadDataTableFinanceiroTituloTipo();
                                        __financeiroTituloTipoIndex.showNotificacaoSuccess(
                                            "Registro de tipo de título removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-titulo-tipo-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['tipotituloId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['tipotituloId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#tipotituloTempoVencimentoTipo").select2('val', data['tipotituloTempoVencimentoTipo']).trigger("change");
            $("#tipotituloMatriculaEmissao").select2('val', data['tipotituloMatriculaEmissao']).trigger("change");
            $("#tipotituloGrupo").select2('val', data['tipotituloGrupo']).trigger("change");
            $('#financeiro-titulo-tipo-modal .modal-title').html(actionTitle + ' tipo de título');
            $('#financeiro-titulo-tipo-modal').modal();
        };

        this.reloadDataTableFinanceiroTituloTipo = function () {
            this.getDataTableFinanceiroTituloTipo().api().ajax.reload(null, false);
        };

        this.getDataTableFinanceiroTituloTipo = function () {
            if (!__financeiroTituloTipoIndex.options.datatables.financeiroTituloTipo) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroTituloTipo')) {
                    __financeiroTituloTipoIndex.options.datatables.financeiroTituloTipo =
                        $('#dataTableFinanceiroTituloTipo').DataTable();
                } else {
                    __financeiroTituloTipoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroTituloTipoIndex.options.datatables.financeiroTituloTipo;
        };

        this.run = function (opts) {
            __financeiroTituloTipoIndex.setDefaults(opts);
            __financeiroTituloTipoIndex.setSteps();
        };
    };

    $.financeiroTituloTipoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-titulo-tipo.index");

        if (!obj) {
            obj = new FinanceiroTituloTipoIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-titulo-tipo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);