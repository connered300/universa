(function ($, window, document) {
    'use strict';

    var FinanceiroTituloTipoAdd = function () {
        VersaShared.call(this);
        var __financeiroTituloTipoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {
                arrTipotituloDocumentoGrupo: null
            },
            value: {},
            datatables: {},
            wizardElement: '#financeiro-titulo-tipo-wizard',
            formElement: '#financeiro-titulo-tipo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#tipotituloTempoVencimento").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#tipotituloTempoVencimentoTipo,#tipotituloMatriculaEmissao").select2({language: 'pt-BR'}).trigger("change");

            $('#tipotituloGrupo').select2({
                data: __financeiroTituloTipoAdd.options.data.arrTipotituloDocumentoGrupo || null
            });
        };

        this.setValidations = function () {
            __financeiroTituloTipoAdd.options.validator.settings.rules = {
                tipotituloNome: {maxlength: 45, required: true},
                tipotituloDescricao: {maxlength: 255},
                tipotituloTempoVencimento: {maxlength: 11, number: true},
                tipotituloTempoVencimentoTipo: {maxlength: 3, required: true},
                tipotituloMatriculaEmissao: {maxlength: 3}
            };
            __financeiroTituloTipoAdd.options.validator.settings.messages = {
                tipotituloNome: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                tipotituloDescricao: {maxlength: 'Tamanho máximo: 255!'},
                tipotituloTempoVencimento: {
                    maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
                tipotituloTempoVencimentoTipo: {maxlength: 3, required: 'Campo obrigatório!'},
                tipotituloMatriculaEmissao: {maxlength: 'Tamanho máximo: 3!'}
            };

            $(__financeiroTituloTipoAdd.options.formElement).submit(function (e) {
                var ok = $(__financeiroTituloTipoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__financeiroTituloTipoAdd.options.formElement);

                if (__financeiroTituloTipoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __financeiroTituloTipoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __financeiroTituloTipoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__financeiroTituloTipoAdd.options.listagem) {
                                    $.financeiroTituloTipoIndex().reloadDataTableFinanceiroTituloTipo();
                                    __financeiroTituloTipoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#financeiro-titulo-tipo-modal').modal('hide');
                                }

                                __financeiroTituloTipoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaFinanceiroTituloTipo = function (tipotitulo_id, callback) {
            var $form = $(__financeiroTituloTipoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __financeiroTituloTipoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + tipotitulo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroTituloTipoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __financeiroTituloTipoAdd.removeOverlay($form);
                },
                erro: function () {
                    __financeiroTituloTipoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroTituloTipoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.financeiroTituloTipoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-titulo-tipo.add");

        if (!obj) {
            obj = new FinanceiroTituloTipoAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-titulo-tipo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);