(function ($, window) {
    'use strict';
    var PainelFinanceiro = function () {
        VersaShared.call(this);

        var __painelFinanceiro = this;

        this.defaults = {
            url: {
                pessoa: '',
                periodoLetivo: '',
                pessoasPesquisa: '',
                financeiroTitulo: '',
                financeiroTituloRenegociacao: '',
                financeiroTituloTipo: '',
                financeiroMeioPagamento: '',
                financeiroTituloInformacoes: '',
                boletoExibe: '',
                boletoCarne: '',
                pagseguroCobranca: '',
                financeiroTituloPagamento: '',
                financeiroTituloCancelamento: '',
                financeiroTituloEnvio: '',
                financeiroAtualizarTitulo: '',
                financeiroTituloNovo: '',
                financeiroValores: '',
                alunoPesquisa: '',
                alunoCursoInformacao: '',
                editarAluno: '',
                thumbnail: '',
                comprovantePagamento: '',
                estornarTitulo: '',
                alterarObservacoes: '',
                financeiroCheque: '',
                pesquisaAluno: '',
                financeiroTituloConfig: '',
                acadperiodoAluno: ''
            },
            datatable: {
                financeiroTitulo: null,
                financeiroTituloRenegociacao: null,
                financeiroTituloPagamento: null,
                financeiroTituloOrigem: null,
                financeiroTituloDestino: null,
                descontos: null,
                pagamentos: null
            },
            data: {
                arrPessoas: {},
                pessoas: [],
                variaveisDeRenegociacao: {},
                variaveisDePagamento: {},
                variaveisDeAtualizacao: {},
                pagamentos: {},
                valores: [],
                variaveisDePagamentoAdd: {},
                titulosVinculadosCartao:{}

            },
            value: {
                arrPermissoes: {},
                arrPessoas: null,
                limparFiltros: true,
                arrMeioPagamento: [],
                arrFiltros: [],
                meioPagamentoPadrao: 1,
                smsAtivado: 0,
                pagseguroAtivado: 0,
                boletoAtivado: 1,
                boletoRemessaAtivada: 1,
                financeiroMaxParcelas: 0,
                financeiroMaxPercentualDesconto: 0,
                tituloEmVisualizacao: [],
                financeiroFiltroPadrao: 0,
                financeiroSelecaoMultipla: 1,
                financeiroAtualizacaoTituloAberto: 1,
                financeiroExibicaoCodigoPessoa: 1,
                financeiroExibicaoCPFPessoa: 0,
                financeiroExibicaoPessoaJuridica: 1,
                financeiroBuscaExibeCodigoAluno: 0,
                financeiroExibicaoSituacaoAluno: 0,
                financeiroExibicaoPeriodoAluno: 0,
                financeiroExibicaoTurmaAluno: 0,
                financeiroEnviarTitulosCopiaAgente: 0,
                financeiroEmissaoCarneAtivado: 0,
                usaPorcentagem: false,
                isentarJuros: false,
                isentarMulta: false,
                financeiroComprovanteImpressaoDireta: 0,
                aluno: {},
                constDescontoIncentivoAcumulativoSim: '',
                alunoPer: ''
            }
        };

        this.calcCor = function (num) {
            num = num || 0;
            var r = (num * 15) % 255;
            var g = (num * 75) % 255;
            var b = (num * 125) % 255;
            return 'rgb(' + r + ',' + g + ',' + b + ')';
        };

        this.verificarExibicaoCodigoPessoa = function () {
            return (__painelFinanceiro.options.value.financeiroExibicaoCodigoPessoa || 0);
        };

        this.verificarBuscaExibicaoCPFPessoa = function () {
            return (__painelFinanceiro.options.value.financeiroExibicaoCPFPessoa || 0 );
        };

        this.verificarExibicaoPessoaJuridica = function () {
            return 1 * (__painelFinanceiro.options.value.financeiroExibicaoPessoaJuridica || 0);
        };

        this.verificarExibicaoCodigoAluno = function () {
            return (__painelFinanceiro.options.value.financeiroBuscaExibeCodigoAluno || 0);
        };

        this.verificarExibicaoSituacaoAluno = function () {
            return (__painelFinanceiro.options.value.financeiroExibicaoSituacaoAluno || 0);
        };

        this.verificarExibicaoPeriodoAluno = function () {
            return (__painelFinanceiro.options.value.financeiroExibicaoPeriodoAluno || 0);
        };

        this.verificarExibicaoTurmaAluno = function () {
            return (__painelFinanceiro.options.value.financeiroExibicaoTurmaAluno || 0);
        };

        this.verificarBloqueioPesquisaPorPesId = function () {
            return 1 * (!__painelFinanceiro.verificarExibicaoCodigoPessoa());
        };

        this.verificarBloqueioPesquisaPorCPF = function () {
            return 1 * (!__painelFinanceiro.verificarBuscaExibicaoCPFPessoa());
        };

        this.verificarBloqueioPesquisaCodigoAluno = function () {
            return 1 * (!__painelFinanceiro.verificarExibicaoCodigoAluno());
        };

        this.getMeioPagamento = function () {
            var transformed = $.map(__painelFinanceiro.options.value.arrMeioPagamento, function (el) {
                el.id = el.meioPagamentoId;
                el.text = el.meioPagamentoDescricao;

                return el;
            });

            return {results: transformed};
        };

        this.getAlunoPer = function () {
            return __painelFinanceiro.options.value.alunoPer || null;
        };

        this.setAlunoPer = function (alunoPerId) {
            __painelFinanceiro.options.value.alunoPer = alunoPerId;
        };

        this.getMeioPagamentoAVista = function () {
            var transformed = [];
            $.each(__painelFinanceiro.options.value.arrMeioPagamento, function (i, el) {
                if (el['meioPagamentoTipo'] == 'a vista') {
                    el.id = el.meioPagamentoId;
                    el.text = el.meioPagamentoDescricao;
                    transformed.push(el);
                }
            });

            return {results: transformed};
        };

        this.getMeioPagamentoPadrao = function () {
            var meioPagamentoPadrao = null;

            $.each(__painelFinanceiro.options.value.arrMeioPagamento, function (i, el) {
                if (el['meioPagamentoId'] == __painelFinanceiro.options.value.meioPagamentoPadrao) {
                    el.id = el.meioPagamentoId;
                    el.text = el.meioPagamentoDescricao;
                    meioPagamentoPadrao = el;
                }
            });

            return meioPagamentoPadrao || null;
        };

        this.pessoa = {
            recarregarInformacoes: function (evento) {
                var $pessoasPesquisa = $('#pessoasPesquisa');
                var arrData = $pessoasPesquisa.select2('data') || [];

                if (arrData['alunocursoId']) {
                    $.ajax({
                        url: __painelFinanceiro.options.url.acadperiodoAluno,
                        method: 'POST',
                        dataType: 'json',
                        delay: 450,
                        async: false,
                        data: {
                            alunocursoId: arrData['alunocursoId'],
                            pesquisaPeriodo: true,
                            periodoCorrente: true
                        },
                        success: function (alunoPerId) {
                            __painelFinanceiro.setAlunoPer(alunoPerId['alunoPerId']);
                        }
                    });

                }

                if (arrData && arrData['pes_id']) {
                    arrData = [arrData];
                }

                if (arrData && arrData.length == 0 && __painelFinanceiro.options.value.arrPessoas && !evento) {
                    $.storage.set('financeiro.painel.titulosSelecionados', {});
                    arrData = __painelFinanceiro.options.value.arrPessoas || null;
                } else if (arrData && arrData.length > 0) {
                    __painelFinanceiro.options.data.arrPessoas =
                        __painelFinanceiro.pessoa.formatarDadosPessoas(arrData);
                }

                if (arrData && arrData.length == 0) {
                    arrData = null;
                }
                var selecaoMultipla = (__painelFinanceiro.options.value.financeiroSelecaoMultipla || false);

                if (!selecaoMultipla && arrData) {
                    arrData = arrData[0];
                }

                $pessoasPesquisa.select2('data', arrData);

                if (arrData) {
                    $('#painelAbas').removeClass('hidden');
                    $('#painelAviso').addClass('hidden');
                } else {
                    $('#painelAbas').addClass('hidden');
                    $('#painelAviso').removeClass('hidden');
                }

                __painelFinanceiro.pessoa.corrigeTitulosDePessoasSelecionadas();

                if (arrData) {
                    __painelFinanceiro.pessoa.carregarInformacoes();
                }
            },
            forcaRecarregarInformacoes: function () {

                __painelFinanceiro.options.value.limparFiltros = false;
                var $pessoasPesquisa = $('#pessoasPesquisa');
                var arrData = $pessoasPesquisa.select2('data') || {};

                if (arrData['pes_id']) {
                    arrData = [arrData];
                }

                $pessoasPesquisa.select2('data', null);
                $pessoasPesquisa.trigger('change');
                $pessoasPesquisa.select2('data', arrData);
                $pessoasPesquisa.trigger('change');
            },
            pessoasSelecionadasRelacionadas: function () {
                var pessoasSelecionadas = $('#pessoasPesquisa').select2('data') || [];
                var arrPesId = [];

                if (pessoasSelecionadas['pes_id']) {
                    pessoasSelecionadas = [pessoasSelecionadas];
                }

                $.each(pessoasSelecionadas, function (i, arrPessoa) {
                    arrPesId.push(arrPessoa['pes_id']);

                    if (arrPessoa['responsavel_financeiro_por']) {
                        var responsavelFinanceiroPor = arrPessoa['responsavel_financeiro_por'] || '';
                        responsavelFinanceiroPor = responsavelFinanceiroPor.split(',');
                        arrPesId = $.merge(arrPesId, responsavelFinanceiroPor);
                    }
                });

                return arrPesId;
            },
            alunosNaSelecaoDePessoas: function () {
                var pessoasSelecionadas = $('#pessoasPesquisa').select2('data') || [];
                var arrAlunoId = {};

                if (pessoasSelecionadas['pes_id']) {
                    pessoasSelecionadas = [pessoasSelecionadas];
                }

                $.each(pessoasSelecionadas, function (i, arrPessoa) {
                    if (!arrPessoa['aluno_id'] || arrAlunoId[arrPessoa['aluno_id']]) {
                        return false;
                    }

                    arrAlunoId[arrPessoa['aluno_id']] = arrPessoa['aluno_id'];
                });

                return arrAlunoId;
            },
            alunosNaSelecaoDePessoasComoObjeto: function () {
                var pessoasSelecionadas = $('#pessoasPesquisa').select2('data') || [];
                var arrAlunoId = {};

                if (pessoasSelecionadas['pes_id']) {
                    pessoasSelecionadas = [pessoasSelecionadas];
                }

                $.each(pessoasSelecionadas, function (i, arrPessoa) {
                    if (!arrPessoa['aluno_id'] || arrAlunoId[arrPessoa['aluno_id']]) {
                        return false;
                    }

                    arrAlunoId[arrPessoa['aluno_id']] = arrPessoa;
                });

                return arrAlunoId;
            },
            corrigeTitulosDePessoasSelecionadas: function () {
                var pessoasSelecionadas = __painelFinanceiro.pessoa.pessoasSelecionadasRelacionadas();
                var titulosSelecionados = $.storage.get('financeiro.painel.titulosSelecionados') || {};

                $.each(titulosSelecionados, function (tituloId, arrTitulo) {
                    var encontrado = false;

                    $.each(pessoasSelecionadas, function (i, pesId) {
                        if (pesId == arrTitulo['pes_id']) {
                            encontrado = true;
                        }
                    });

                    if (!encontrado) {
                        delete titulosSelecionados[tituloId];
                    }
                });

                $.storage.set('financeiro.painel.titulosSelecionados', titulosSelecionados);

                return titulosSelecionados;
            },
            pessoasRelacionadasATitulosSelecionadas: function () {
                var titulosSelecionados = __painelFinanceiro.titulos.titulosSelecionadas() || [];
                var arrPessoas = [];
                var arrPesId = {};

                $.each(titulosSelecionados, function (tituloId, arrTitulo) {
                    if (arrPesId[arrTitulo['pes_id']]) {
                        return false;
                    }

                    arrPesId[arrTitulo['pes_id']] = arrTitulo['pes_id'];
                    arrPessoas.push({
                        id: arrTitulo['pes_id'],
                        pesId: arrTitulo['pes_id'],
                        pes_id: arrTitulo['pes_id'],
                        text: arrTitulo['pes_nome'],
                        pesNome: arrTitulo['pes_nome'],
                        pes_nome: arrTitulo['pes_nome']
                    });
                });

                return arrPessoas;
            },
            formatarDadosAlunos: function (data) {
                return __painelFinanceiro.pessoa.formatarDadosPessoas(data, 'alunocurso_id', true);
            },
            formatarDadosPessoas: function (data, id, exibirCurso) {

                data = data || [];
                id = id || 'pes_id';
                exibirCurso = exibirCurso || false;

                if (data['pes_id']) {
                    data = [data];
                }

                return $.map(data, function (el) {
                    var aluno = (el['aluno_id'] || '').replace(/^0+/g, '') || 0,
                        matricula = (el['alunocurso_id'] || '').replace(/^0+/g, '') || 0,
                        cursoNome = el['curso_nome'] || '',
                        matsitDescricao = el['matsit_descricao'] || '-',
                        cpf = (el['pes_cpf'] || '').replace(/[^0-9]/ig, ''),
                        cnpj = (el['pes_cnpj'] || '').replace(/[^0-9]/ig, ''),
                        label = [],
                        turmaDesc = el['turma_nome'],
                        periodoLetivo = el['per_nome'],
                        possuiPeriodoLetivo = el['curso_possui_periodo_letivo'];

                    label.push('Nome: ' + el['pes_nome']);

                    if (__painelFinanceiro.verificarBuscaExibicaoCPFPessoa() && cpf) {
                        label.push('CPF: ' + cpf);
                    }

                    if (__painelFinanceiro.verificarExibicaoCodigoPessoa()) {
                        label.push('Código: ' + __painelFinanceiro.numPad(el['pes_id'], 10, '0'));
                    }

                    if (aluno && __painelFinanceiro.verificarExibicaoCodigoAluno()) {
                        label.push('Aluno: ' + __painelFinanceiro.numPad(aluno, 11, '0'));
                    }

                    if (matricula) {
                        label.push('Matrícula: ' + __painelFinanceiro.numPad(matricula, 11, '0'));
                    }

                    if (cursoNome && exibirCurso) {
                        label.push('Curso: ' + cursoNome);
                    }

                    if (matsitDescricao && matsitDescricao != "-" && __painelFinanceiro.verificarExibicaoSituacaoAluno()) {
                        label.push('Situacao: ' + matsitDescricao);
                    }

                    if (cnpj) {
                        label.push('CNPJ: ' + cnpj);
                    }

                    if (periodoLetivo && __painelFinanceiro.verificarExibicaoPeriodoAluno()) {
                        label.push('Periodo Letivo: ' + periodoLetivo);
                    }

                    if (turmaDesc && __painelFinanceiro.verificarExibicaoTurmaAluno()) {
                        label.push('Turma: ' + turmaDesc);
                    }


                    el.text = label.join(' | ');
                    el.id = el[id];
                    el.pesId = el['pes_id'];
                    el.pesNome = el['pes_nome'];
                    el.arrPessoasId = matricula ? matricula : '';

                    return el;
                });
            },
            formatarDadosPessoasSomente: function (data) {
                data = data || [];
                var arrRetorno = [];
                var arrPesId = {};

                $.each(data, function (i, el) {
                    el.id = el['pes_id'];

                    if (arrPesId[el.id]) {
                        return true;
                    }

                    var matricula = (el['alunocurso_id'] || '').replace(/^0+/g, '') || 0;
                    var cpf = (el['pes_cpf'] || '').replace(/[^0-9]/ig, '');
                    var cnpj = (el['pes_cnpj'] || '').replace(/[^0-9]/ig, '');
                    var label = [];

                    if (__painelFinanceiro.verificarExibicaoCodigoPessoa()) {
                        label.push('Código: ' + __painelFinanceiro.numPad(el['pes_id'], 10, '0'));
                    }

                    if (cpf) {
                        label.push('CPF: ' + cpf);
                    }

                    if (cnpj) {
                        label.push('CNPJ: ' + cnpj);
                    }

                    label.push('Nome: ' + el['pes_nome']);
                    el.text = label.join(' | ');
                    el.pesId = el['pes_id'];
                    el.pesNome = el['pes_nome'];
                    el.arrPessoasId = matricula ? matricula : '';

                    arrPesId[el.id] = el;
                    arrRetorno.push(el);
                });

                return arrRetorno;
            },
            iniciarCamposInterface: function () {
                $('#pessoasPesquisa').select2({
                    allowClear: true,
                    multiple: (__painelFinanceiro.options.value.financeiroSelecaoMultipla || false),
                    language: 'pt-BR',
                    ajax: {
                        url: __painelFinanceiro.options.url.pessoa,
                        data: function (query) {
                            return {
                                query: query,
                                agruparAluno: true,
                                pesquisarPJ: __painelFinanceiro.verificarExibicaoPessoaJuridica(),
                                bloquearPesquisaPorPesId: __painelFinanceiro.verificarBloqueioPesquisaPorPesId(),
                                bloquearPesquisaPorCPF: __painelFinanceiro.verificarBloqueioPesquisaPorCPF(),
                                bloquearPesquisaPeloAlunoId: __painelFinanceiro.verificarBloqueioPesquisaCodigoAluno()
                            };
                        },
                        dataType: 'json',
                        results: function (data) {
                            return {results: __painelFinanceiro.pessoa.formatarDadosPessoas(data)};
                        }
                    },
                    minimumInputLength: 1
                }).on('change', __painelFinanceiro.pessoa.recarregarInformacoes);

                $('#btn-financeiro-titulo-filtrar').click(__painelFinanceiro.titulos.atualizarDatatablesTitulos);
                $('#btn-financeiro-titulo-filtros-limpar').click(__painelFinanceiro.titulos.limparFiltros);
                $('#btn-financeiro-titulo-renegociar').click(__painelFinanceiro.renegociacao.efetuarRenegociacaoTitulos);
                $('#btn-financeiro-titulo-pagamento').click(__painelFinanceiro.pagamento.efetuarPagamentoTitulos);
                $('#btn-financeiro-titulo-novo').click(__painelFinanceiro.novoTitulo.exibirModal);
                $('#btn-financeiro-titulo-cancelar').click(__painelFinanceiro.cancelamento.cancelarTitulos);
                $('#btn-financeiro-titulo-enviar').click(__painelFinanceiro.envio.exibirModal);
                $('#btn-financeiro-titulo-emitir-carne').click(__painelFinanceiro.carne.imprimir);


                $(
                    '#financeiro-titulo-novo-form, ' +
                    '#financeiro-titulo-pagamento-form, ' +
                    '#financeiro-titulo-renegociacao-form, ' +
                    '#financeiro-titulo-enviar-form'
                ).on(
                    'keyup keypress', '[type=text],[type=radio],[type=checkbox],[type=search]',
                    function (e) {
                        var keyCode = e.keyCode || e.which;

                        if (keyCode === 13) {
                            e.preventDefault();
                            return false;
                        }
                    });

                var arrItensPermissao = {
                    renegociacao: '#btn-financeiro-titulo-renegociar',
                    pagamento: '#btn-financeiro-titulo-pagamento',
                    novoTitulo: '#btn-financeiro-titulo-novo',
                    cancelarTitulo: '#btn-financeiro-titulo-cancelar',
                    envioTitulos: '#btn-financeiro-titulo-enviar'
                };

                $.each(arrItensPermissao, function (permissao, elemento) {
                    var permissaoConcedida = (__painelFinanceiro.options.value.arrPermissoes[permissao] || 0);

                    if (!permissaoConcedida) {
                        $(elemento).hide();
                    }
                });

                if (!__painelFinanceiro.options.value.financeiroEmissaoCarneAtivado) {
                    $('#btn-financeiro-titulo-emitir-carne').hide();
                }

                if (!(__painelFinanceiro.options.value.arrPermissoes['alterarObservacao'] || 0)) {
                    $('#alterarObservacao .btn').prop('disabled', true);
                }

                if (!__painelFinanceiro.options.value.smsAtivado) {
                    $('#tituloEnviarSms')
                        .prop('checked', false)
                        .closest('.form-group')
                        .addClass('hidden');
                }
            },
            carregarInformacoes: function () {
                var $selecaoAlunoDadosPessoais = $('#selecaoAlunoDadosPessoais');

                if (__painelFinanceiro.options.value.limparFiltros) {
                    __painelFinanceiro.titulos.limparFiltros();

                    var filtroPadrao = __painelFinanceiro.options.value.financeiroFiltroPadrao || '';

                    if (filtroPadrao) {
                        $('#titulos-pre-definidos').find('[data-filtro="' + filtroPadrao + '"]').click();

                    }
                }

                __painelFinanceiro.options.value.limparFiltros = true;

                __painelFinanceiro.titulos.atualizarDatatablesTitulos();
                __painelFinanceiro.observacao.carregarObservacaoPessoaSelecionada();


                var alunos = __painelFinanceiro.pessoa.alunosNaSelecaoDePessoasComoObjeto(),
                    arrFiltro = {alunoId: Object.keys(alunos || {})},
                    alunoId = arrFiltro.alunoId[0] || null;

                var aluno = alunos[alunoId] || null;

                if (aluno) {
                    aluno = $.extend({}, aluno, __painelFinanceiro.keysToCamelCase(aluno));
                }

                if (!arrFiltro.alunoId[0]) {
                    arrFiltro.alunoId[0] = "-";
                }

                $.financeiroDescontoIndex().setAlunoPerId(__painelFinanceiro.getAlunoPer());
                $.financeiroDescontoIndex().setAluno(aluno);
                $.financeiroDescontoIndex().setFilterDatatables(arrFiltro);
                $.financeiroDescontoIndex().setBloqueaBtnNovoDesconto(
                    aluno ? false : true
                );

                __painelFinanceiro.alunoInformacoes.selecionarAlunoPelaPessoaSelecionada();

                var permissoes = __painelFinanceiro.options.value.arrPermissoes;

                if (!permissoes.adicionarDesconto) {
                    $('#btn-financeiro-desconto-add').hide();
                }
            }
        };

        this.alunoInformacoes = {
            iniciarCamposInterface: function () {
                $('#selecaoAlunoDadosPessoais').select2({
                    allowClear: true,
                    language: 'pt-BR',
                    ajax: {
                        url: __painelFinanceiro.options.url.pessoa,
                        data: function (query) {
                            return {
                                query: query,
                                somentePesId: __painelFinanceiro.pessoa.pessoasSelecionadasRelacionadas(),
                                pesquisarPJ: __painelFinanceiro.verificarExibicaoPessoaJuridica(),
                                bloquearPesquisaPorPesId: __painelFinanceiro.verificarBloqueioPesquisaPorPesId()
                            };

                        },

                        dataType: 'json',
                        results: function (data) {
                            return {results: __painelFinanceiro.pessoa.formatarDadosAlunos(data)};
                        }
                    },
                    formatSelection: function (el) {
                        return el.pes_nome;
                    }
                }).on('change', function () {
                    __painelFinanceiro.alunoInformacoes.carregarInformacoesAluno();
                });
            },
            selecionarAlunoPelaPessoaSelecionada: function () {
                var $pessoasPesquisa = $('#pessoasPesquisa');

                var $selecaoAlunoDadosPessoais = $('#selecaoAlunoDadosPessoais');
                var arrPessoas = $pessoasPesquisa.select2('data') || {};
                var aluno = null;

                if (arrPessoas['pes_id']) {
                    arrPessoas = [arrPessoas];
                }

                $.each(arrPessoas, function (i, arrPessoa) {
                    if (arrPessoa['alunocurso_id']) {
                        aluno = arrPessoa;
                        aluno['id'] = arrPessoa['alunocurso_id'];
                        return false;
                    } else if (arrPessoa['pes_id']) {
                        aluno = arrPessoa;
                        aluno['id'] = arrPessoa['pes_id'];
                        return false;
                    }
                });

                $selecaoAlunoDadosPessoais.select2('data', aluno || null).trigger('change');
            },
            carregarInformacoesAluno: function () {
                var arrData = $('#selecaoAlunoDadosPessoais').select2('data') || {};
                var $painelInformacoesAluno = $('#painel-informacoes-aluno');

                $painelInformacoesAluno.addClass('hidden');

                if (!arrData['pesId']) {
                    return false;
                }

                $painelInformacoesAluno.removeClass('hidden');

                __painelFinanceiro.alunoInformacoes.carregaDados(arrData, 'Carregando...');

                __painelFinanceiro.addOverlay($painelInformacoesAluno, 'Aguarde, carregando informações do aluno.');

                $.ajax({
                    url: __painelFinanceiro.options.url.alunoCursoInformacao,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        alunoId: arrData['aluno_id'] || '',
                        pesId: arrData['pes_id'] || '',
                        alunocursoId: arrData['alunocurso_id'] || ''
                    },
                    success: function (arrResposta) {
                        if (arrResposta.alunoCurso) {
                            __painelFinanceiro.alunoInformacoes.carregaDados(arrResposta.alunoCurso, 'Indisponível');
                        }

                        __painelFinanceiro.removeOverlay($painelInformacoesAluno);
                    },
                    error: function (xhr, error, thrown) {
                        __painelFinanceiro.showNotificacaoWarning(
                            "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                        );
                        __painelFinanceiro.removeOverlay($painelInformacoesAluno);
                    }
                });
            },
            carregaDados: function (arrData, padrao) {
                var arrCampos = [
                    'pesNome', 'pesDataNascimento', 'pesCpf', 'pesRg',
                    'pesRgEmissao', 'pesSexo', 'pesNaturalidade', 'pesNascUf',
                    'alunoId', 'alunoPai', 'alunoMae', 'alunoEtnia',

                    'conContatoCelular', 'conContatoTelefone', 'conContatoEmail',
                    'endBairro', 'endCep', 'endCidade', 'endComplemento', 'endData',
                    'endEstado', 'endId', 'endLogradouro', 'endNumero', 'alunocursoId',
                    'cursoId', 'grupoLeitorNome', 'cursoNome', 'campId', 'campNome',
                    'perNome', 'tiposelNome', 'alunocursoSituacao', 'turmaNome', 'turmaTurno', 'situacaoMatricula',
                    'periodoLetivo', 'situacaoId', 'situacaoDescricao', 'pesNomeAgenteAluno'
                ];
                arrData = arrData || {};
                padrao = padrao || false;

                $.each(arrCampos, function (i, campo) {
                    var valor = typeof arrData[campo] == 'undefined' ? false : arrData[campo];

                    if (valor === false && padrao) {
                        valor = padrao;
                    }
                    $('.form-control-static.' + campo).html(valor);
                });

                var imagemAluno = typeof arrData['imagemAluno'] == 'undefined' ? '' : arrData['imagemAluno'];
                var $imagemAluno = $('#imagemAluno');

                $imagemAluno.find('img').hide();
                $imagemAluno.find('span').remove();

                if (imagemAluno) {
                    $imagemAluno.append('<span><i class="fa fa-spinner fa-2x fa-spin"></i>Carregando imagem...</span>');
                    $imagemAluno.find('img').load(function () {
                        $imagemAluno.find('span').remove();
                        $imagemAluno.find('img').show();
                    }).attr(
                        'src',
                        __painelFinanceiro.options.url.thumbnail + '/' + imagemAluno + '?width=100&height=80'
                    );
                } else {
                    $imagemAluno.append('<span>' + (padrao || 'Indisponível!') + '</span>');
                }

                if (arrData['alunoId']) {
                    $('.pesId').html(
                        '<a href="' + __painelFinanceiro.options.url.editarAluno + '/' + arrData['alunoId'] + '">' +
                        arrData['alunoId'] +
                        '</a>'
                    );
                } else {
                    $('.pesId').html('-');
                }
            }
        };

        this.titulos = {
            titulosSelecionadas: function () {
                var titulosSelecionados = {};

                $('.selecionar-titulo:checked').each(function (i, item) {
                    var $pai = $(item).closest('tr');
                    var data = __painelFinanceiro.getDatatable('financeiroTitulo').fnGetData($pai);

                    titulosSelecionados[data['titulo_id']] = data;
                });

                $.storage.set('financeiro.painel.titulosSelecionados', titulosSelecionados);

                return titulosSelecionados;
            },
            iniciarCamposInterface: function () {
                var $tipotitulo = $("#tipotitulo"),
                    $tituloEstado = $("#tituloEstado"),
                    $titulosSelecionados = $("#titulosSelecionados"),
                    $tituloDataInicial = $("#tituloDataInicial"),
                    $tituloDataFinal = $("#tituloDataFinal");

                $('#tituloalunocursoId').select2({
                    allowClear: true,
                    language: 'pt-BR',
                    multiple: true,
                    ajax: {
                        url: __painelFinanceiro.options.url.alunoPesquisa,

                        data: function (query) {
                            return {
                                query: query,
                                pesId: __painelFinanceiro.pessoa.pessoasSelecionadasRelacionadas()
                            };
                        },
                        dataType: 'json',
                        results: function (data) {
                            return {results: __painelFinanceiro.pessoa.formatarDadosAlunos(data)};
                        }
                    },
                    formatSelection: function (el) {
                        return el.pes_nome + '/' + el.curso_nome;
                    }
                });

                $tipotitulo.select2({
                    language: 'pt-BR',
                    multiple: true,
                    ajax: {
                        url: __painelFinanceiro.options.url.financeiroTituloTipo,
                        dataType: 'json',
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.text = el.tipotitulo_nome;
                                el.id = el.tipotitulo_id;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });

                $tituloEstado.select2({language: 'pt-BR'});

                __painelFinanceiro.iniciarElementoDatePicker($(
                    '#tituloDataInicial, #tituloDataFinal,' +
                    '#tituloDataPagamentoInicial, #tituloDataPagamentoFinal'
                ));

                $('#corpo-recibo-modal').load(function () {
                    __painelFinanceiro.removeOverlay($(this).closest('.modal-content'));
                });

                var titulosPreDefinidos = $('#titulos-pre-definidos');

                titulosPreDefinidos.html('');

                if (Object.keys(__painelFinanceiro.options.value.arrFiltros || []).length == 0) {
                    titulosPreDefinidos.closest('.form-group').parent().hide();
                }

                $.each(__painelFinanceiro.options.value.arrFiltros, function (i, arrFiltro) {
                    $('#titulos-pre-definidos').append(
                        ' <a href="#" class="btn btn-xs btn-info titulos-pre-definidos-aplicar"' +
                        ' data-filtro="' + arrFiltro['filtroId'] + '">' +
                        arrFiltro['filtroNome'] +
                        '</a>'
                    );
                });

                $(document).on('click', '.titulos-pre-definidos-aplicar', function (e) {
                    var filtroId = $(this).data('filtro');
                    __painelFinanceiro.titulos.limparFiltros();

                    $.each(__painelFinanceiro.options.value.arrFiltros, function (i, arrFiltro) {
                        if (arrFiltro['filtroId'] == filtroId) {
                            $tituloEstado.select2('val', arrFiltro['filtroEstado']);
                            $tipotitulo.select2('data', arrFiltro['tipoTitulo']);
                            __painelFinanceiro.titulos.atualizarDatatablesTitulos();
                        }
                    });

                    e.preventDefault();
                    e.stopPropagation();
                });

                $(".panel-title a").click(function (e) {
                    $(".panel-title .fa").toggleClass('fa-chevron-right').toggleClass('fa-chevron-down');
                });
            },
            limparFiltros: function () {
                $("#tipotitulo").select2('val', '');
                $("#tituloEstado").select2('val', '');
                $("#tituloDataInicial").val('');
                $("#tituloDataFinal").val('');
                $("#tituloDataPagamentoInicial").val('');
                $("#tituloDataPagamentoFinal").val('');
                $("#titulosSelecionados").prop('checked', false);
                $("#tituloalunocursoId").select2('val', '');
            },
            atualizarDatatablesTitulos: function () {
                if (__painelFinanceiro.getDatatable('financeiroTitulo')) {
                    __painelFinanceiro.getDatatable('financeiroTitulo').api().ajax.reload(null, false);
                }
            },
            iniciarDataTableTitulo: function () {
                var colNum = -1,
                    $tipotitulo = $("#tipotitulo"),
                    $tituloEstado = $("#tituloEstado"),
                    $tituloDataInicial = $("#tituloDataInicial"),
                    $tituloDataFinal = $("#tituloDataFinal"),
                    $tituloDataPagamentoInicial = $("#tituloDataPagamentoInicial"),
                    $tituloDataPagamentoFinal = $("#tituloDataPagamentoFinal"),
                    $titulosSelecionados = $("#titulosSelecionados"),
                    $tituloalunocursoId = $("#tituloalunocursoId"),
                    $dataTableFinanceiroTitulo = $("#dataTableFinanceiroTitulo");

                var dtFinanceiroTitulo = $dataTableFinanceiroTitulo.dataTable({
                    processing: true,
                    serverSide: true,
                    searchDelay: 350,
                    ajax: {
                        url: __painelFinanceiro.options.url.financeiroTitulo,
                        type: "POST",
                        data: function (d) {
                            d.filter = d.filter || {};
                            var tipotitulo = $tipotitulo.val() || "",
                                pesId = (
                                    (__painelFinanceiro.pessoa.pessoasSelecionadasRelacionadas() || []).join(',')
                                ),
                                tituloEstado = $tituloEstado.val() || "",
                                tituloDataInicial = $tituloDataInicial.val() || "",
                                tituloDataFinal = $tituloDataFinal.val() || "",
                                tituloDataPagamentoInicial = $tituloDataPagamentoInicial.val() || "",
                                tituloDataPagamentoFinal = $tituloDataPagamentoFinal.val() || "",
                                alunocursoId = $tituloalunocursoId.val() || "",
                                titulosSelecionados = $titulosSelecionados.prop('checked');

                            pesId = pesId || "-1";

                            if (tipotitulo) {
                                d.filter['tipotitulo'] = tipotitulo;
                            }

                            if (tituloEstado) {
                                d.filter['tituloEstado'] = tituloEstado;
                            }

                            if (tituloDataInicial) {
                                d.filter['tituloDataInicial'] = tituloDataInicial;
                            }

                            if (tituloDataFinal) {
                                d.filter['tituloDataFinal'] = tituloDataFinal;
                            }

                            if (tituloDataPagamentoInicial) {
                                d.filter['tituloDataPagamentoInicial'] = tituloDataPagamentoInicial;
                            }

                            if (tituloDataPagamentoFinal) {
                                d.filter['tituloDataPagamentoFinal'] = tituloDataPagamentoFinal;
                            }

                            if (titulosSelecionados) {
                                d.filter['tituloId'] = Object.keys(
                                    $.storage.get('financeiro.painel.titulosSelecionados') || {}
                                );

                                if (d.filter['tituloId'].length == 0) {
                                    d.filter['tituloId'] = [0];
                                }
                            }

                            if (alunocursoId) {
                                d.filter['alunocursoId'] = alunocursoId;
                            }

                            if (Object.keys(d.filter).length == 0) {
                                $('#indicador-filtro-ativo').addClass('hidden');
                            } else {
                                $('#indicador-filtro-ativo').removeClass('hidden');
                            }

                            d.filter['pesId'] = pesId;

                            __painelFinanceiro.addOverlay($('#titulos-tab'), 'Aguarde, carregando títulos!');
                            __painelFinanceiro.addOverlay($('#container-financeiro-desconto'), "Aguarde, carregando descontos!");

                            return d;
                        },
                        error: function (xhr, error, thrown) {
                            __painelFinanceiro.showNotificacaoWarning(
                                "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                            );
                            __painelFinanceiro.removeOverlay($('#titulos-tab'));
                            __painelFinanceiro.removeOverlay($('#container-financeiro-desconto'));
                        },
                        dataSrc: function (json) {
                            var data = json.data;
                            var totalizacoes = json['totalizacoes'] || {};

                            var titulosSelecionados = $.storage.get('financeiro.painel.titulosSelecionados') || {};

                            $.each(data, function (row, titulo) {
                                var checked = titulosSelecionados[titulo['titulo_id']] ? ' checked' : '';
                                var dataProcessamento = __painelFinanceiro.formatDate(titulo['titulo_data_processamento']);
                                var dataVencimento = __painelFinanceiro.formatDate(titulo['titulo_data_vencimento']);
                                var dataPagamento = __painelFinanceiro.formatDate(titulo['titulo_data_pagamento']);
                                var vencido = false;
                                var chaveValor = 'titulo_valor_pago';

                                var btns = [
                                    {
                                        class: 'btn-primary btn-financeiro-titulo-view', icon: 'fa-eye',
                                        title: 'Visualizar informações do título'
                                    }
                                ];

                                if (parseInt(titulo['dias_atraso']) > 0) {
                                    vencido = true;
                                }

                                titulo['curso_sigla'] = (
                                    titulo['curso_sigla'] ||
                                    (titulo['curso_nome'] || '').substr(0, 3).toUpperCase()
                                );

                                titulo['check'] = '&nbsp;';
                                var corRGB = __painelFinanceiro.calcCor(titulo['tipotitulo_id']);
                                var style = (
                                    ' style="background-color: ' + corRGB +
                                    ';display:block;text-align: center; width: 100%;padding: 1px 4px;"'
                                );

                                if (titulo['titulo_estado'] == 'Aberto') {
                                    titulo['check'] = (
                                        '<input type="checkbox" class="form-check-input selecionar-titulo"' + checked +
                                        '>'
                                    );
                                    chaveValor = 'titulo_valor_calc';
                                }

                                if (titulo['titulo_estado'] == 'Pago') {
                                    btns.push(
                                        {
                                            class: 'btn-info btn-financeiro-titulo-comprovante-emitir',
                                            icon: 'fa-print', title: 'Imprimir comprovante de pagamento do título'
                                        });
                                }

                                titulo['check'] = '<div' + style + '>' + titulo['check'] + '</div>';

                                if (titulo['valor_desconto_antecipado'] &&
                                    parseFloat(titulo[chaveValor]) > parseFloat(titulo['valor_desconto_antecipado'])) {
                                    titulo['titulo_valor_calc'] -= titulo['valor_desconto_antecipado'];
                                }

                                titulo['titulo_valor_formatado'] = (
                                    '<span ' + (vencido ? ' class="text-danger"' : '') + '>' +
                                    __painelFinanceiro.formatarMoeda(titulo[chaveValor]) +
                                    '</span>'
                                );

                                titulo['acao'] = __painelFinanceiro.createBtnGroup(btns);
                                titulo['titulo_data_processamento_formatado'] = dataProcessamento;
                                titulo['titulo_data_vencimento_formatado'] = dataVencimento;
                                titulo['titulo_data_pagamento_formatado'] = dataPagamento;

                                data[row] = titulo;
                            });

                            __painelFinanceiro.removeOverlay($('#titulos-tab'));
                            __painelFinanceiro.removeOverlay($('#container-financeiro-desconto'));

                            var $tituloTotais = $('#dataTableFinanceiroTituloTotais');

                            $tituloTotais.hide();

                            if (Object.keys(totalizacoes).length > 0) {
                                $tituloTotais.show();
                                var conteudo = '';
                                conteudo += 'Total Juros: <b>' +
                                    __painelFinanceiro.formatarMoeda(totalizacoes['total_juros']) +
                                    '</b> &nbsp; ';
                                conteudo += 'Total Multas: <b>' +
                                    __painelFinanceiro.formatarMoeda(totalizacoes['total_multa']) +
                                    '</b> &nbsp; ';
                                conteudo += 'Total Descontos: <b>' +
                                    __painelFinanceiro.formatarMoeda(totalizacoes['total_desconto']) +
                                    '</b> &nbsp; ';
                                conteudo += 'Qtd. Vencidos: <b>' +
                                    totalizacoes['qtd_titulos_vencidos'] +
                                    '</b> &nbsp; ';
                                conteudo += 'Total Vencidos: <b>' +
                                    __painelFinanceiro.formatarMoeda(totalizacoes['total_final_vencidos']) +
                                    '</b> &nbsp; ';
                                conteudo += 'Total Final: <b>' +
                                    __painelFinanceiro.formatarMoeda(totalizacoes['total_final']) +
                                    '</b>';

                                $tituloTotais.html(conteudo);

                            }

                            __painelFinanceiro.removeOverlay($('#container-financeiro-desconto'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "titulo_id", targets: ++colNum, data: "check", orderable: false, searchable: false},
                        {name: "titulo_id", targets: ++colNum, data: "titulo_id"},
                        {name: "pes_nome", targets: ++colNum, data: "pes_nome", searchable: false},
                        {name: "titulo_descricao", targets: ++colNum, data: "titulo_descricao"},
                        {name: "curso_sigla", targets: ++colNum, data: "curso_sigla", searchable: false},
                        {
                            name: "titulo_data_vencimento",
                            targets: ++colNum,
                            data: "titulo_data_vencimento_formatado",
                            searchable: false
                        },
                        {
                            name: "titulo_data_pagamento", targets: ++colNum,
                            data: "titulo_data_pagamento_formatado",
                            searchable: false
                        },
                        {name: "titulo_valor", targets: ++colNum, data: "titulo_valor_formatado", searchable: false},
                        {name: "titulo_estado", targets: ++colNum, data: "titulo_estado", searchable: false},
                        {name: "titulo_id", targets: ++colNum, data: "acao", orderable: false, searchable: false}
                    ],
                    "dom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[5, 'asc']]
                });

                __painelFinanceiro.setDatatable('financeiroTitulo', dtFinanceiroTitulo);

                $dataTableFinanceiroTitulo.on('click', '.btn-financeiro-titulo-view', function (e) {

                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable('financeiroTitulo').fnGetData($pai);
                    __painelFinanceiro.vizualizacao.carregarDetalhesTitulo(data);
                    e.preventDefault();
                    e.stopPropagation();
                });

                $dataTableFinanceiroTitulo.on('click', '.btn-financeiro-titulo-comprovante-emitir', function (e) {
                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable('financeiroTitulo').fnGetData($pai);
                    __painelFinanceiro.titulos.comprovanteEmitir(data['tituloId'] || data['titulo_id'] || '');
                    e.preventDefault();
                    e.stopPropagation();
                });

                $dataTableFinanceiroTitulo.on('click', '.selecionar-titulo', function () {
                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable('financeiroTitulo').fnGetData($pai);
                    var titulosSelecionados = $.storage.get('financeiro.painel.titulosSelecionados') || {};

                    titulosSelecionados[data['titulo_id']] = data;

                    if (!$(this).prop('checked')) {
                        delete titulosSelecionados[data['titulo_id']];
                    }

                    $.storage.set('financeiro.painel.titulosSelecionados', titulosSelecionados);
                });
            },
            comprovanteEmitir: function (tituloId) {
                tituloId = tituloId || '';

                if (!tituloId) {
                    __painelFinanceiro.showNotificacaoWarning(
                        'Forneça um título válido para emitir um comprovante de pagamento!'
                    );
                    return;
                }

                var $modal = $('#financeiro-titulo-comprovante-modal');

                __painelFinanceiro.addOverlay($modal.find('.modal-content'), 'Aguarde, carregando comprovante...');

                var url = __painelFinanceiro.options.url.comprovantePagamento + '?tituloId=' + tituloId;

                $('#corpo-recibo-modal').attr('src', url);

                if (!__painelFinanceiro.options.value.financeiroComprovanteImpressaoDireta) {
                    $modal.modal('show');
                }
            }
        };

        this.renegociacao = {
            iniciarCamposInterface: function () {
                __painelFinanceiro.inciarElementoMoeda($(
                    '#porcentagemDescontoManualRenegociacao, #porcentagemAcrescimoManualRenegociacao,' +
                    '#tituloDescontoManual, #tituloAcrescimoManual'
                ));

                __painelFinanceiro.options.value.isentarJuros = false;
                __painelFinanceiro.options.value.isentarMulta = false;

                __painelFinanceiro.iniciarElementoDatePicker($("#tituloDataVencimento"), {minDate: 0});
                __painelFinanceiro.inciarElementoInteiro($("#tituloParcelas"));

                $(
                    '#porcentagemDescontoManualRenegociacao, #porcentagemAcrescimoManualRenegociacao,' +
                    '#tituloDescontoManual, #tituloAcrescimoManual,' +
                    '#tituloParcelas'
                ).change(function () {
                        $('#renegociacaoLogin, #renegociacaoSenha').val('');
                    });
                $(
                    '#porcentagemDescontoManualRenegociacao, #porcentagemAcrescimoManualRenegociacao,' +
                    '#tituloDescontoManual, #tituloAcrescimoManual,' +
                    '#tituloParcelas, #tituloDataVencimento, #tituloObservacoes'
                ).change(__painelFinanceiro.renegociacao.atualizarInformacoesRenegociacao);

                $('#porcentagemAtivoRenegociacao').on('change', function () {
                    var checked = $(this).prop('checked');
                    var $camposPorcentagem = $('.campos-porcentagem-renegociacao');
                    var $camposValor = $('.campos-valor-renegociacao');

                    if (checked) {
                        __painelFinanceiro.options.value.usaPorcentagem = true;
                        $('#porcentagemDescontoManualRenegociacao').val(0);
                        $('#porcentagemAcrescimoManualRenegociacao').val(0);

                        $camposPorcentagem.removeClass('hidden');
                        $camposValor.find('input').prop('disabled', true);
                    } else {
                        __painelFinanceiro.options.value.usaPorcentagem = false;
                        $camposPorcentagem.addClass('hidden');
                        $camposValor.find('input').prop('disabled', false);
                    }
                });

                $('#isentarJurosRenegociacao').on('change', function () {
                    __painelFinanceiro.options.value.isentarJuros = $(this).prop('checked');
                    __painelFinanceiro.renegociacao.atualizarInformacoesRenegociacao();
                });

                $('#isentarMultaRenegociacao').on('change', function () {
                    __painelFinanceiro.options.value.isentarMulta = $(this).prop('checked');
                    __painelFinanceiro.renegociacao.atualizarInformacoesRenegociacao();
                });

                $("#responsavelTitulo").select2({
                    language: 'pt-BR',
                    ajax: {
                        url: __painelFinanceiro.options.url.pessoa,
                        dataType: 'json',
                        data: function (query) {
                            return {
                                query: query,
                                somentePesId: __painelFinanceiro.pessoa.pessoasSelecionadasRelacionadas(),
                                pesquisarPJ: __painelFinanceiro.verificarExibicaoPessoaJuridica(),
                                bloquearPesquisaPorPesId: __painelFinanceiro.verificarBloqueioPesquisaPorPesId()
                            };
                        },
                        results: function (data) {
                            return {results: __painelFinanceiro.pessoa.formatarDadosPessoasSomente(data)};
                        }
                    },
                    formatSelection: function (el) {
                        return el.pes_nome;
                    }
                });

                $('#financeiro-titulo-renegociacao-form').validate({
                    submitHandler: function () {
                        var $modal = $('#financeiro-titulo-renegociacao-modal');
                        __painelFinanceiro.renegociacao.atualizarInformacoesRenegociacao();

                        __painelFinanceiro.addOverlay(
                            $modal.find('.modal-content'),
                            'Efetuando renegociação de títulos. Aguarde.'
                        );

                        __painelFinanceiro.options.data.variaveisDeRenegociacao['isentarJuros'] =
                            __painelFinanceiro.options.value.isentarJuros;
                        __painelFinanceiro.options.data.variaveisDeRenegociacao['isentarMulta'] =
                            __painelFinanceiro.options.value.isentarMulta;

                        $.ajax({
                            url: __painelFinanceiro.options.url.financeiroTituloRenegociacao,
                            type: 'POST',
                            dataType: 'json',
                            data: __painelFinanceiro.options.data.variaveisDeRenegociacao,
                            success: function (data) {
                                if (data.erro) {
                                    __painelFinanceiro.showNotificacaoWarning(
                                        data['mensagem'] || "Não foi possível renegociar títulos!"
                                    )
                                } else {
                                    __painelFinanceiro.showNotificacaoSuccess(
                                        data['mensagem'] || "Renegociação efetuada com sucesso!"
                                    );

                                    __painelFinanceiro.pessoa.forcaRecarregarInformacoes();

                                    $modal.modal('hide');
                                }
                                __painelFinanceiro.removeOverlay($modal.find('.modal-content'));
                            },
                            error: function (xhr, error, thrown) {
                                __painelFinanceiro.showNotificacaoWarning(
                                    "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                                );
                                __painelFinanceiro.removeOverlay($modal.find('.modal-content'));
                            }
                        });

                        return false;
                    },
                    ignore: '.ignore',
                    rules: {
                        tituloAcrescimoManual: {minlength: 4},
                        tituloDescontoManual: {minlength: 4},
                        tituloParcelas: {required: true, min: 1},
                        responsavelTitulo: {required: true},
                        tituloObservacoes: {required: true},
                        tituloDataVencimento: {required: true, dateBR: true},
                        renegociacaoSenha: {
                            required: function () {
                                return $('#renegociacaoSenha').is(':visible');
                            }
                        },
                        renegociacaoLogin: {
                            required: function () {
                                return $('#renegociacaoLogin').is(':visible');
                            }
                        }
                    },
                    messages: {
                        tituloAcrescimoManual: {minlength: 'Tamanho mínimo: 1!'},
                        tituloDescontoManual: {minlength: 'Tamanho mínimo: 1!'},
                        tituloParcelas: {required: 'Campo obrigatório!', min: 'Valor mínimo: 1!'},
                        responsavelTitulo: {required: 'Campo obrigatório!'},
                        tituloObservacoes: {required: 'Campo obrigatório!'},
                        tituloDataVencimento: {required: 'Campo obrigatório!', dateBR: 'Data inválida!'},
                        renegociacaoSenha: {required: 'Campo obrigatório!'},
                        renegociacaoLogin: {required: 'Campo obrigatório!'}
                    }
                });
            },
            limparDatatablesTitulosRenegociacao: function () {
                if (__painelFinanceiro.getDatatable('financeiroTituloRenegociacao')) {
                    $("#dataTableTitulosRenegociacao").find("tbody tr").each(function (i, line) {
                        var data = __painelFinanceiro.getDatatable('financeiroTituloRenegociacao').fnGetData(line);

                        if (!data) {
                            return;
                        }

                        __painelFinanceiro.getDatatable('financeiroTituloRenegociacao').api().row(line).remove().draw();
                    });
                }
            },
            atualizarDatatablesTitulosRenegociacao: function () {
                var $form = $('#financeiro-titulo-renegociacao-form');

                if (__painelFinanceiro.getDatatable('financeiroTituloRenegociacao')) {
                    __painelFinanceiro.renegociacao.limparDatatablesTitulosRenegociacao();
                    var $autenticacaoPagamento = $form.find('.autenticacao-renegociacao');
                    var titulosRenegociacao = __painelFinanceiro.titulos.titulosSelecionadas() || [];
                    var btnGroup = '\
                <div class="text-center">\
                    <div class="btn-group">\
                        <button type="button" class="btn btn-xs btn-primary btn-financeiro-titulo-view"\
                            title="Visualizar informações do título">\
                            <i class="fa fa-eye fa-inverse"></i>\
                        </button>\
                        <button type="button" class="btn btn-xs btn-danger btn-financeiro-titulo-renegociacao-remover"\
                            title="Remover título da renegociação">\
                            <i class="fa fa-times"></i>\
                        </button>\
                    </div>\
                </div>';

                    var variaveisDeRenegociacao = __painelFinanceiro.options.data.variaveisDeRenegociacao || {};
                    var senha = $('#renegociacaoSenha').val() || '';
                    var login = $('#renegociacaoLogin').val() || '';
                    variaveisDeRenegociacao.senha = senha;
                    variaveisDeRenegociacao.login = login;

                    variaveisDeRenegociacao = {
                        titulos: [],
                        numeroTitulos: 0,
                        permissaoRequerida: variaveisDeRenegociacao.permissaoRequerida || false,
                        senha: variaveisDeRenegociacao.senha,
                        login: variaveisDeRenegociacao.login,
                        porcentagemDescontoManual: variaveisDeRenegociacao.porcentagemDescontoManual,
                        porcentagemAcrescimoManual: variaveisDeRenegociacao.porcentagemAcrescimoManual,
                        valorTotalTitulos: 0,
                        valorTotalMulta: 0,
                        valorTotalJuros: 0,
                        diasAtraso: 0,
                        mediaAtraso: 0,
                        valorTotalAPagar: 0,
                        valorFinal: 0,
                        valorParcela: 0,
                        acrescimo: variaveisDeRenegociacao.acrescimo || 0,
                        descontoManual: variaveisDeRenegociacao.descontoManual || 0,
                        desconto: variaveisDeRenegociacao.desconto || 0,
                        parcelas: variaveisDeRenegociacao.parcelas || 1,
                        observacao: variaveisDeRenegociacao.observacao || '',
                        vencimento: variaveisDeRenegociacao.vencimento || '',
                        responsavelTitulo: variaveisDeRenegociacao.responsavelTitulo || '',

                    };

                    $.each(titulosRenegociacao, function (row, titulo) {
                        titulo['acao'] = btnGroup;
                        __painelFinanceiro.getDatatable('financeiroTituloRenegociacao').api().row.add(titulo).draw();
                        variaveisDeRenegociacao.titulos.push(titulo['titulo_id']);
                        variaveisDeRenegociacao.numeroTitulos += 1;
                        variaveisDeRenegociacao.valorTotalTitulos += parseFloat(titulo['titulo_valor']);
                        variaveisDeRenegociacao.valorTotalMulta += parseFloat(titulo['titulo_multa_calc']);
                        variaveisDeRenegociacao.valorTotalJuros += parseFloat(titulo['titulo_juros_calc']);
                        variaveisDeRenegociacao.diasAtraso += parseFloat(titulo['dias_atraso']);
                        titulosRenegociacao[row] = titulo;
                    });

                    variaveisDeRenegociacao.mediaAtraso = __painelFinanceiro.roundNumber(
                        (variaveisDeRenegociacao.diasAtraso / variaveisDeRenegociacao.numeroTitulos), 2
                    );
                    variaveisDeRenegociacao.valorTotalAPagar = __painelFinanceiro.roundNumber(
                        (
                            variaveisDeRenegociacao.valorTotalJuros +
                            variaveisDeRenegociacao.valorTotalMulta +
                            variaveisDeRenegociacao.valorTotalTitulos
                        ), 2
                    );

                    if (variaveisDeRenegociacao.valorTotalJuros && __painelFinanceiro.options.value.isentarJuros && !__painelFinanceiro.options.value.isentarMulta) {
                        variaveisDeRenegociacao.valorTotalAPagar = __painelFinanceiro.roundNumber(
                            (
                                variaveisDeRenegociacao.valorTotalAPagar -
                                variaveisDeRenegociacao.valorTotalJuros

                            ), 2
                        );
                    }

                    if (variaveisDeRenegociacao.valorTotalMulta && __painelFinanceiro.options.value.isentarMulta && !__painelFinanceiro.options.value.isentarJuros) {
                        variaveisDeRenegociacao.valorTotalAPagar = __painelFinanceiro.roundNumber(
                            (
                                variaveisDeRenegociacao.valorTotalAPagar -
                                variaveisDeRenegociacao.valorTotalMulta
                            ),
                            2
                        )
                        ;
                    }

                    if (__painelFinanceiro.options.value.isentarJuros && __painelFinanceiro.options.value.isentarMulta) {
                        variaveisDeRenegociacao.valorTotalAPagar = __painelFinanceiro.roundNumber(
                            (
                                variaveisDeRenegociacao.valorTotalTitulos
                            ), 2
                        );
                    }

                    if (__painelFinanceiro.options.value.usaPorcentagem) {
                        variaveisDeRenegociacao.descontoManual = parseFloat(
                            ((variaveisDeRenegociacao.valorTotalAPagar || 0) *
                            variaveisDeRenegociacao.porcentagemDescontoManual) / 100
                        );
                        variaveisDeRenegociacao.acrescimo = parseFloat(
                            ((variaveisDeRenegociacao.valorTotalAPagar || 0) *
                            variaveisDeRenegociacao.porcentagemAcrescimoManual) / 100
                        );
                        $('#tituloDescontoManual').val(variaveisDeRenegociacao.descontoManual);
                        $('#tituloAcrescimoManual').val(variaveisDeRenegociacao.acrescimo);
                    } else {
                        variaveisDeRenegociacao.porcentagemDescontoManual =
                            (variaveisDeRenegociacao.descontoManual * 100) / (variaveisDeRenegociacao.valorTotalAPagar);
                        variaveisDeRenegociacao.porcentagemAcrescimoManual =
                            (variaveisDeRenegociacao.acrescimo * 100) / (variaveisDeRenegociacao.valorTotalAPagar);
                        $('#porcentagemDescontoManualRenegociacao').val(variaveisDeRenegociacao.porcentagemDescontoManual);
                        $('#porcentagemAcrescimoManualRenegociacao').val(variaveisDeRenegociacao.porcentagemAcrescimoManual);
                    }

                    var arrMotivo = [];

                    var extrapolouNumeroMaximoParcelas = (
                        variaveisDeRenegociacao.parcelas > __painelFinanceiro.options.value.financeiroMaxParcelas
                    );
                    var extrapolouDescontoMaximo = (
                        variaveisDeRenegociacao.porcentagemDescontoManual >
                        __painelFinanceiro.options.value.financeiroMaxPercentualDesconto
                    );

                    if (extrapolouNumeroMaximoParcelas) {
                        arrMotivo.push('Extrapolou o número máximo de parcelas;');
                    }

                    if (extrapolouDescontoMaximo) {
                        arrMotivo.push('Extrapolou o percentual máximo de desconto;');
                    }

                    arrMotivo = arrMotivo.join(' ');

                    $form.find('.motivoAutenticacao').html(arrMotivo || '-');

                    if (extrapolouDescontoMaximo || extrapolouNumeroMaximoParcelas) {
                        $autenticacaoPagamento.removeClass('hidden');
                        variaveisDeRenegociacao.permissaoRequerida = true;
                    } else {
                        $autenticacaoPagamento.addClass('hidden');
                        variaveisDeRenegociacao.permissaoRequerida = false;
                    }

                    variaveisDeRenegociacao.valorFinal = __painelFinanceiro.roundNumber(
                        (
                            variaveisDeRenegociacao.valorTotalAPagar +
                            variaveisDeRenegociacao.acrescimo -
                            variaveisDeRenegociacao.descontoManual
                        ), 2
                    );

                    variaveisDeRenegociacao.valorParcela = __painelFinanceiro.roundNumber(
                        (
                            variaveisDeRenegociacao.valorFinal /
                            variaveisDeRenegociacao.parcelas
                        ), 2
                    );

                    $('#numeroTitulos-static').html(variaveisDeRenegociacao.numeroTitulos);
                    $('#mediaAtraso-static').html(variaveisDeRenegociacao.mediaAtraso);
                    $('#valorTotalJuros-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDeRenegociacao.valorTotalJuros)
                    );
                    $('#valorTotalMulta-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDeRenegociacao.valorTotalMulta)
                    );
                    $('#valorTotalTitulos-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDeRenegociacao.valorTotalTitulos)
                    );
                    $('#valorTotalAPagar-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDeRenegociacao.valorTotalAPagar)
                    );
                    $('#valorFinal-static').html(
                        variaveisDeRenegociacao.parcelas + ' x ' +
                        __painelFinanceiro.formatarMoeda(variaveisDeRenegociacao.valorParcela) +
                        '<hr class="no-margin">' +
                        __painelFinanceiro.formatarMoeda(variaveisDeRenegociacao.valorFinal)
                    );

                    __painelFinanceiro.options.data.variaveisDeRenegociacao = variaveisDeRenegociacao;

                    if (variaveisDeRenegociacao.numeroTitulos == 0) {
                        __painelFinanceiro.showNotificacaoWarning("Selecione títulos para efetuar a renegociação!");
                        $('#financeiro-titulo-renegociacao-modal').modal('hide');
                        return false;
                    }
                } else {
                    return false;
                }

                return true;
            },

            atualizarInformacoesRenegociacao: function (isenta) {
                var variaveisDeRenegociacao = __painelFinanceiro.options.data.variaveisDeRenegociacao || false;
                var acrescimoManual = $('#tituloAcrescimoManual').val() || '0';
                var descontoManual = $('#tituloDescontoManual').val() || '0';
                var parcelas = $('#tituloParcelas').val() || 1;
                var observacao = $('#tituloObservacoes').val() || '';
                var vencimento = $('#tituloDataVencimento').val() || '';
                var responsavelTitulo = $('#responsavelTitulo').val() || '';
                var porcentagemDescontoManual = $('#porcentagemDescontoManualRenegociacao').val() || '0';
                var porcentagemAcrescimoManual = $('#porcentagemAcrescimoManualRenegociacao').val() || '0';
                var senha = $('#renegociacaoSenha').val() || '';
                var login = $('#renegociacaoLogin').val() || '';

                acrescimoManual = parseFloat(acrescimoManual.replace('.', '').replace(',', '.'));
                descontoManual = parseFloat(descontoManual.replace('.', '').replace(',', '.'));
                porcentagemDescontoManual = parseFloat(porcentagemDescontoManual.replace('.', '').replace(',', '.'));
                porcentagemAcrescimoManual = parseFloat(porcentagemAcrescimoManual.replace('.', '').replace(',', '.'));

                variaveisDeRenegociacao.acrescimo = acrescimoManual;
                variaveisDeRenegociacao.desconto = descontoManual;
                variaveisDeRenegociacao.descontoManual = descontoManual;
                variaveisDeRenegociacao.porcentagemDescontoManual = porcentagemDescontoManual;
                variaveisDeRenegociacao.porcentagemAcrescimoManual = porcentagemAcrescimoManual;
                variaveisDeRenegociacao.parcelas = parseInt(parcelas);
                variaveisDeRenegociacao.observacao = observacao;
                variaveisDeRenegociacao.vencimento = vencimento;
                variaveisDeRenegociacao.responsavelTitulo = responsavelTitulo;
                variaveisDeRenegociacao.senha = senha;
                variaveisDeRenegociacao.login = login;

                __painelFinanceiro.options.data.variaveisDeRenegociacao = variaveisDeRenegociacao;

                $('#financeiro-titulo-renegociacao-form').valid();
                return __painelFinanceiro.renegociacao.atualizarDatatablesTitulosRenegociacao();

            },
            efetuarRenegociacaoTitulos: function () {
                $('#porcentagemAcrescimoManualRenegociacao').val(0);
                $('#porcentagemDescontoManualRenegociacao').val(0);
                $('#tituloAcrescimoManual').val(0);
                $('#tituloDescontoManual').val(0);
                $('#tituloObservacoes').val('');
                $('#renegociacaoLogin').val('');
                $('#renegociacaoSenha').val('');
                $('#tituloDataVencimento').val(__painelFinanceiro.formatDate(new Date()));
                $('#porcentagemAtivoRenegociacao').prop('checked', false).trigger('change');
                $('#isentarJurosRenegociacao').prop('checked', false).trigger('change');
                $('#isentarMultaRenegociacao').prop('checked', false).trigger('change');
                $('#tituloParcelas').val(1);

                var responsavel = null;
                var arrPessoas = __painelFinanceiro.pessoa.pessoasRelacionadasATitulosSelecionadas();

                if (arrPessoas.length == 1) {
                    responsavel = arrPessoas[0];
                }

                $('#responsavelTitulo').select2('data', responsavel).trigger('change');

                if (__painelFinanceiro.renegociacao.atualizarInformacoesRenegociacao()) {
                    $('#financeiro-titulo-renegociacao-modal').modal('show');
                }
            },
            iniciarDataTableRenegociacaoTitulo: function () {
                var colNum = -1;
                var $dataTableTitulosRenegociacao = $('#dataTableTitulosRenegociacao');

                var dtFinanceiroTitulo = $dataTableTitulosRenegociacao.dataTable({
                    processing: true,
                    serverSide: false,
                    data: [],
                    columnDefs: [
                        {name: "titulo_id", targets: ++colNum, data: "titulo_id"},
                        {name: "pes_nome", targets: ++colNum, data: "pes_nome"},
                        {name: "titulo_descricao", targets: ++colNum, data: "titulo_descricao"},
                        {
                            name: "titulo_data_processamento", targets: ++colNum,
                            data: "titulo_data_processamento_formatado"
                        },
                        {name: "titulo_data_vencimento", targets: ++colNum, data: "titulo_data_vencimento_formatado"},
                        {name: "titulo_valor", targets: ++colNum, data: "titulo_valor"},
                        {name: "titulo_valor", targets: ++colNum, data: "titulo_valor_formatado"},
                        {name: "titulo_estado", targets: ++colNum, data: "titulo_estado"},
                        {name: "titulo_id", targets: ++colNum, data: "acao", orderable: false}
                    ],
                    "dom": "rt<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[5, 'desc']]
                });

                __painelFinanceiro.setDatatable('financeiroTituloRenegociacao', dtFinanceiroTitulo);

                $dataTableTitulosRenegociacao.on('click', '.btn-financeiro-titulo-view', function (e) {

                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable('financeiroTituloRenegociacao').fnGetData($pai);
                    __painelFinanceiro.vizualizacao.carregarDetalhesTitulo(data);
                    e.preventDefault();
                    e.stopPropagation();
                });

                $dataTableTitulosRenegociacao.on('click', '.btn-financeiro-titulo-renegociacao-remover', function (e) {
                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable('financeiroTituloRenegociacao').fnGetData($pai);
                    var titulosSelecionados = (
                        $.storage.get('financeiro.painel.titulosSelecionados') || {}
                    );

                    titulosSelecionados[data['titulo_id']] = data;
                    delete titulosSelecionados[data['titulo_id']];

                    $.storage.set(
                        'financeiro.painel.titulosSelecionados',
                        titulosSelecionados
                    );

                    __painelFinanceiro.titulos.atualizarDatatablesTitulos();
                    __painelFinanceiro.renegociacao.atualizarDatatablesTitulosRenegociacao();
                    e.preventDefault();
                    e.stopPropagation();
                });
            }
        };

        this.envio = {
            exibirModal: function () {
                var $titulosDestinatarioEnvio = $('#titulosDestinatarioEnvio');
                var $modal = $('#financeiro-titulo-envio-modal');

                if (__painelFinanceiro.getDatatable('financeiroTituloRenegociacao')) {
                    var titulosEnvio = __painelFinanceiro.titulos.titulosSelecionadas() || [],
                        arrTitulosEnvioInfo = {},
                        arrInformacoes = [];

                    $.each(titulosEnvio, function (row, titulo) {
                        var arrTituloEnvioInfo = arrTitulosEnvioInfo[titulo['pes_id']] || false;

                        if (!arrTituloEnvioInfo) {
                            arrTituloEnvioInfo = {pes_id: titulo['pes_id'], pes_nome: titulo['pes_nome'], titulos: []};
                        }

                        arrTituloEnvioInfo.titulos.push(parseInt(titulo['titulo_id']));
                        arrTitulosEnvioInfo[titulo['pes_id']] = arrTituloEnvioInfo;
                    });

                    if (Object.keys(arrTitulosEnvioInfo).length == 0) {
                        __painelFinanceiro.showNotificacaoWarning("Selecione títulos para efetuar envios!");
                        $titulosDestinatarioEnvio.html('-');
                        $modal.modal('hide');

                        return false;
                    }

                    $.each(arrTitulosEnvioInfo, function (row, arrTituloEnvioInfo) {
                        arrInformacoes.push(
                            '<b>' + arrTituloEnvioInfo['pes_nome'] + '</b>' +
                            ' (' + arrTituloEnvioInfo['titulos'].length + ' títulos): ' +
                            arrTituloEnvioInfo['titulos'].join(', ')
                        );
                    });

                    $titulosDestinatarioEnvio.html(arrInformacoes.join('<br>'));
                } else {
                    return false;
                }

                $modal.modal('show');

                return true;
            },
            iniciarCamposInterface: function () {
                if (!__painelFinanceiro.options.value.financeiroEnviarTitulosCopiaAgente) {
                    $('.campoEnviarComCopia').hide();
                } else {
                    $('[name="tituloEnviarEmail"]').on('change', function (e) {
                        if ($(this).prop('checked')) {
                            $('.campoEnviarComCopia').show();
                        } else {
                            $('.campoEnviarComCopia').hide();
                            $('[name="tituloEnviarEmailComCopia"]').attr('checked', false);
                        }
                    });
                }

                $('#financeiro-titulo-envio-enviar').click(function (e) {
                    var $modal = $('#financeiro-titulo-envio-modal'), enviarEmailComCopia = false;

                    if (!__painelFinanceiro.getDatatable('financeiroTituloRenegociacao')) {
                        return false;
                    }

                    var titulosEnvio = __painelFinanceiro.titulos.titulosSelecionadas() || [],
                        arrTitulos = [];

                    $.each(titulosEnvio, function (row, titulo) {
                        arrTitulos.push(titulo['titulo_id']);
                    });

                    if (arrTitulos.length == 0) {
                        __painelFinanceiro.showNotificacaoWarning("Selecione títulos para efetuar envios!");
                        $modal.modal('hide');

                        return false;
                    }

                    var arrVia = [];

                    if ($('#tituloEnviarSms').is(':checked')) {
                        arrVia.push('sms');
                    }

                    if ($('#tituloEnviarEmail').is(':checked')) {
                        arrVia.push('email');
                    }

                    if ($('#tituloEnviarEmailComCopia').is(':checked')) {
                        enviarEmailComCopia = true;
                    }

                    if (arrVia.length == 0) {
                        __painelFinanceiro.showNotificacaoInfo("Selecione uma forma de envio!");
                        return false;
                    }

                    __painelFinanceiro.envio.executarEnvio(arrTitulos, arrVia, enviarEmailComCopia);
                    $modal.modal('hide');

                    e.preventDefault();
                    e.stopPropagation();
                });
            },
            executarEnvio: function (arrTitulos, arrVia, enviarEmailComCopia) {
                if (arrVia.length == 0) {
                    __painelFinanceiro.showNotificacaoInfo("Selecione uma forma de envio!");
                    return false;
                }

                __painelFinanceiro.addOverlay($('#titulos-tab'), 'Aguarde. Enviando títulos!');
                $.ajax({
                    url: __painelFinanceiro.options.url.financeiroTituloEnvio,
                    type: 'POST',
                    dataType: 'json',
                    data: {titulos: arrTitulos, via: arrVia, enviarEmailComCopia: enviarEmailComCopia ? 1 : 0},
                    success: function (data) {
                        if (data.erro) {
                            __painelFinanceiro.showNotificacaoWarning(
                                data['mensagem'] || "Não foi possível enviar títulos!"
                            )
                        } else {
                            __painelFinanceiro.showNotificacaoSuccess(
                                data['mensagem'] || "Envio de títulos efetuado com sucesso!"
                            );
                        }
                        __painelFinanceiro.removeOverlay($('#titulos-tab'));
                    },
                    error: function (xhr, error, thrown) {
                        __painelFinanceiro.showNotificacaoWarning(
                            "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                        );
                        __painelFinanceiro.removeOverlay($('#titulos-tab'));
                    }
                });
            }
        };

        this.carne = {
            imprimir: function () {
                var titulosCarne = __painelFinanceiro.titulos.titulosSelecionadas() || [],
                    arrTitulos = [];

                $.each(titulosCarne, function (row, titulo) {
                    arrTitulos.push(parseInt(titulo['titulo_id']));
                });

                if (arrTitulos.length == 0) {
                    __painelFinanceiro.showNotificacaoInfo("Selecione títulos para gerar o carnê!");

                    return false;
                }

                window.open(__painelFinanceiro.options.url.boletoCarne + '/' + arrTitulos.join(','));

                return true;
            }
        };

        this.pagamentoAdd = {
            meioDePagamentoCheque: function () {
                var arrData = $("#pagamentoMeioPagamento").select2('data') || {};
                var meiodePagamentoCheque = (arrData['meioPagamentoCamposCheque'] || 'Não') == 'Sim';

                return meiodePagamentoCheque;
            },
            exibirModal: function (arrDados, naoAbrirModal) {
                naoAbrirModal = naoAbrirModal || false;
                arrDados = arrDados || {};
                arrDados['pagamentoGUID'] = arrDados['pagamentoGUID'] || __painelFinanceiro.createGUID();
                var pagamentoMeioPagamento = null;
                var variaveisDePagamento = __painelFinanceiro.options.data.variaveisDePagamento || {};

                arrDados['pagamentoValor'] = arrDados['pagamentoValor'] || '';

                if (!arrDados['pagamentoValor'] && variaveisDePagamento['valorFinal']) {
                    arrDados['pagamentoValor'] = __painelFinanceiro.formatarMoedaUS(
                        variaveisDePagamento['valorFinal'] - variaveisDePagamento['pagamentoValor']
                    );

                    arrDados['pagamentoValor'] = __painelFinanceiro.formatarMoedaUS(
                        arrDados['pagamentoValor'] > 0 ? arrDados['pagamentoValor'] : 0);
                }

                if (arrDados['meioPagamentoId']) {
                    pagamentoMeioPagamento = arrDados;
                    pagamentoMeioPagamento['id'] = arrDados['meioPagamentoId'];
                    pagamentoMeioPagamento['text'] = arrDados['meioPagamentoDescricao'];
                }

                var dataAtual = __painelFinanceiro.formatDate(new Date());
                var resp = $('#pagamentoResponsavelTitulo').select2('data') || {};
                resp = resp['pesNome'] || '';

                $('#pagamentoGUID').val(arrDados['pagamentoGUID'] || '');
                $('#pagamentoMeioPagamento').select2('data', pagamentoMeioPagamento).trigger('change');
                $('#pagamentoValor').val(arrDados['pagamentoValor'] || 0);
                $('#chequeEmitente').val(arrDados['chequeEmitente'] || resp || '');
                $('#chequeNum').val(arrDados['chequeNum'] || '');
                $('#chequeBanco').val(arrDados['chequeBanco'] || '');
                $('#chequeAgencia').val(arrDados['chequeAgencia'] || '');
                $('#chequePraca').val(arrDados['chequePraca'] || '');
                $('#chequeEmissao').val(arrDados['chequeEmissao'] || dataAtual || '');
                $('#chequeVencimento').val(arrDados['chequeVencimento'] || dataAtual || '');
                $('#chequeCompensado').val(arrDados['chequeCompensado'] || '');
                $('#chequeValor').val(arrDados['chequeValor'] || arrDados['pagamentoValor'] || '');
                $('#chequeConta').val(arrDados['chequeConta'] || '');

                if (!naoAbrirModal) {
                    $('#financeiro-pagamento-add-modal').modal('show');
                }
            },
            iniciarCamposInterface: function () {
                $('#chequeBanco, #chequeValor, #chequeAgencia, #chequeConta, #chequeNum').val('');
                __painelFinanceiro.iniciarElementoDatePicker(
                    $("#chequeEmissao, #chequeVencimento, #chequeCompensado")
                );

                $('#pagamentoValor, #chequeValor').inputmask({
                    showMaskOnHover: false,
                    alias: 'currency',
                    groupSeparator: "",
                    radixPoint: ",",
                    placeholder: "0",
                    allowMinus: false,
                    prefix: ""
                });

                $("#chequeBanco").inputmask({
                    showMaskOnHover: false,
                    mask: ['9{0,9}'],
                    rightAlign: true,
                    allowMinus: false
                });
                $("#pagamentoMeioPagamento").select2({
                    language: 'pt-BR',
                    data: __painelFinanceiro.getMeioPagamentoAVista
                }).change(function () {
                    if (__painelFinanceiro.pagamentoAdd.meioDePagamentoCheque()) {
                        $('#pagamentoDadosCheque').removeClass('hidden');
                    } else {
                        $('#pagamentoDadosCheque').addClass('hidden');
                    }
                }).trigger('change');

                $('#chequeBanco, #chequeValor, #chequeAgencia, #chequeConta, #chequeNum').change(function () {
                    //Tratamento específico para cheques
                    if (!__painelFinanceiro.pagamentoAdd.meioDePagamentoCheque) {
                        return false;
                    }
                    var arrDados = $('#pagamento-add-form').serializeJSON();
                    var $modal = $('#financeiro-pagamento-add-modal');

                    if (arrDados.chequeBanco && arrDados.chequeAgencia && arrDados.chequeNum && arrDados.chequeConta) {
                        __painelFinanceiro.addOverlay($modal.find('.modal-content'), 'Aguarde.');

                        if (arrDados.chequeId) {
                            arrDados.chequeId = '';
                        }

                        $.ajax({
                            url: __painelFinanceiro.options.url.financeiroCheque,
                            dataType: 'json',
                            type: 'POST',
                            data: arrDados,
                            success: function (data) {
                                __painelFinanceiro.removeOverlay($modal.find('.modal-content'));

                                $('#chequeValor').prop('readonly', false);
                                if (data && data['chequeId']) {
                                    $('#pagamentoValor').addClass('disable');

                                    __painelFinanceiro.showNotificacaoInfo(
                                        'Este cheque já foi cadastrado! Saldo: R$ '
                                        + __painelFinanceiro.formatarMoeda(data['chequeSaldo'])
                                    );
                                    $('#chequeValor').prop('readonly', true);
                                } else {
                                    arrDados['chequeSaldo'] = (
                                        __painelFinanceiro.formatarMoeda(data['chequeValor']) ||
                                        arrDados['chequeValor'] ||
                                        arrDados['pagamentoValor']
                                    );
                                }

                                $('#chequeEmitente').val(data['chequeEmitente'] || arrDados['chequeEmitente']);
                                $('#chequeId').val(data['chequeId'] || arrDados['chequeId']);
                                $('#chequePraca').val(data['chequePraca'] || arrDados['chequePraca']);

                                if (!data['chequeId']) {
                                    if (parseFloat(arrDados['chequeValor']) % 1 != 0 || parseFloat(arrDados['chequeValor']) < 0) {
                                        $('#chequeValor,#chequeSaldo').val(arrDados['pagamentoValor']);
                                    } else {
                                        $('#chequeValor,#chequeSaldo').val(arrDados['chequeValor']);
                                    }
                                } else {
                                    $('#chequeValor').val(__painelFinanceiro.formatarMoeda(data['chequeValor']));
                                    $('#chequeSaldo').val(__painelFinanceiro.formatarMoeda(data['chequeSaldo']));
                                }
                            },
                            error: function (xhr, error, thrown) {
                                __painelFinanceiro.showNotificacaoWarning(
                                    "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                                );
                                __painelFinanceiro.removeOverlay($modal.find('.modal-content'));
                            }
                        });
                    }
                });

                __painelFinanceiro.options.data.variaveisDePagamentoAdd = {};
            },
            setarValidacoes: function () {
                $('#pagamento-add-form').validate({
                    submitHandler: __painelFinanceiro.pagamentoAdd.salvarPagamento,

                    ignore: '.ignore',
                    rules: {
                        pagamentoMeioPagamento: {required: true},
                        pagamentoValor: {maxlength: 10, required: true},
                        chequeEmitente: {
                            maxlength: 255, required: __painelFinanceiro.pagamentoAdd.meioDePagamentoCheque
                        },
                        chequeNum: {
                            maxlength: 45, required: __painelFinanceiro.pagamentoAdd.meioDePagamentoCheque
                        },
                        chequeBanco: {
                            maxlength: 45, required: __painelFinanceiro.pagamentoAdd.meioDePagamentoCheque
                        },
                        chequeAgencia: {
                            maxlength: 45, required: __painelFinanceiro.pagamentoAdd.meioDePagamentoCheque
                        },
                        chequePraca: {
                            maxlength: 45, required: __painelFinanceiro.pagamentoAdd.meioDePagamentoCheque
                        },
                        chequeEmissao: {
                            required: __painelFinanceiro.pagamentoAdd.meioDePagamentoCheque, dateBR: true
                        },
                        chequeVencimento: {
                            required: __painelFinanceiro.pagamentoAdd.meioDePagamentoCheque, dateBR: true
                        },
                        chequeCompensado: {dateBR: true},
                        chequeValor: {
                            maxlength: 10, required: __painelFinanceiro.pagamentoAdd.meioDePagamentoCheque
                        },
                        chequeConta: {
                            maxlength: 45, required: __painelFinanceiro.pagamentoAdd.meioDePagamentoCheque
                        }
                    },
                    messages: {
                        pagamentoValor: {maxlength: 'Tamanho máximo: 10!', required: 'Campo obrigatório!'},
                        pagamentoMeioPagamento: {maxlength: 'Tamanho máximo: 10!', required: 'Campo obrigatório!'},
                        chequeEmitente: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                        chequeNum: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                        chequeBanco: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                        chequeAgencia: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                        chequePraca: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                        chequeEmissao: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                        chequeVencimento: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                        chequeCompensado: {dateBR: 'Informe uma data válida!'},
                        chequeValor: {
                            maxlength: 'Tamanho máximo: 10!', required: 'Campo obrigatório!'
                        },
                        chequeConta: {maxlength: 'Tamanho máximo: 45!'}
                    }
                });
            },
            salvarPagamento: function () {
                var pagamentos = __painelFinanceiro.options.data.pagamentos || {};
                var arrDados = $('#pagamento-add-form').serializeJSON();
                var arrMeioPagamento = $("#pagamentoMeioPagamento").select2('data') || {};
                var chequePreenchido = arrDados['chequeAgencia'] && arrDados['chequeBanco'] && arrDados['chequePraca'] ?
                    true : false;

                if (arrDados['chequeSaldo'] <= 0 && chequePreenchido) {
                    __painelFinanceiro.showNotificacaoInfo("Este cheque não tem saldo!");
                    return false;
                }

                arrDados = $.extend({}, arrMeioPagamento, arrDados);

                if (arrDados['id']) {
                    delete arrDados['id'];
                    delete arrDados['text'];
                }

                var chaveCheque = '';

                if (__painelFinanceiro.pagamentoAdd.meioDePagamentoCheque()) {

                    chaveCheque = (
                        arrDados['chequeNum'] + '+' + arrDados['chequeConta'] + '+' + arrDados['chequeBanco']
                    );

                    if (!arrDados['chequeId']) {
                        if (__painelFinanceiro.formatarMoedaUS(arrDados['chequeValor']) <
                            __painelFinanceiro.formatarMoedaUS(arrDados['pagamentoValor'])) {

                            __painelFinanceiro.showNotificacaoDanger("O valor do cheque é menor que o pagamento!");
                            return false;
                        }

                    } else {
                        if (__painelFinanceiro.formatarMoedaUS(arrDados['pagamentoValor']) >
                            __painelFinanceiro.formatarMoedaUS(arrDados['chequeSaldo'])) {

                            __painelFinanceiro.showNotificacaoDanger("O valor informado excede o saldo do cheque");
                            return false;
                        }
                    }

                }

                arrDados['meioPagamento'] = arrDados['meioPagamentoId'];
                arrDados['pagamentoValor'] = arrDados['pagamentoValor'];
                arrDados['valorPagamento'] = arrDados['pagamentoValor'];

                var meiodePagamentoDuplicado = false;
                var chequeDuplicado = false;

                $.each(pagamentos, function (i, pagamento) {
                    var meiodePagamentoIgual = (arrDados['meioPagamentoId'] == pagamento['meioPagamentoId']);
                    var meiodePagamentoGUIDIgual = (arrDados['pagamentoGUID'] == pagamento['pagamentoGUID']);
                    var usoMultiplo = (pagamento['meioPagamentoUsoMultiplo'] == 'Sim');
                    var meiodePagamentoCheque = (pagamento['meioPagamentoCamposCheque'] == 'Sim');

                    if (!meiodePagamentoGUIDIgual && !usoMultiplo && meiodePagamentoIgual) {
                        meiodePagamentoDuplicado = true;
                    }

                    if (meiodePagamentoCheque && !meiodePagamentoGUIDIgual) {
                        var chaveCheque2 = (
                            pagamento['chequeNum'] + '+' + pagamento['chequeConta'] + '+' + pagamento['chequeBanco']
                        );

                        if (chaveCheque == chaveCheque2) {
                            chequeDuplicado = true;
                        }
                    }
                });

                if (meiodePagamentoDuplicado == true) {
                    __painelFinanceiro.showNotificacaoWarning(
                        'Não é possível incluir o meio de pagamento ' +
                        arrDados['meioPagamentoDescricao'] +
                        ' mais de uma vez!'
                    );

                    return false;
                }

                if (chequeDuplicado == true) {
                    __painelFinanceiro.showNotificacaoWarning(
                        'Não é possível incluir o mesmo cheque mais de uma vez!'
                    );

                    return false;
                }

                if (chequePreenchido) {

                    var descricaoCheque = '<span style="font-size: 10px;"></br>' +
                        (arrDados['chequeId'] ? ' Cod.: ' + arrDados['chequeId'] + " - " : " Novo - ") +
                        ' Banco: ' + arrDados['chequeBanco'] + ' | Agência: ' + arrDados['chequeAgencia'] +
                        ' | Conta: ' + arrDados['chequeConta'] + ' | Número: ' + arrDados['chequeNum'] + '</span>';

                    arrDados['meioPagamentoDescricao'] = arrDados['meioPagamentoDescricao'] + descricaoCheque;
                }

                pagamentos[arrDados['pagamentoGUID']] = arrDados;

                __painelFinanceiro.pagamento.atualizarDatatablesMeioPagamento();

                $('#financeiro-pagamento-add-modal').modal('hide');

                return false;
            }
        };

        this.pagamento = {
            iniciarCamposInterface: function () {
                __painelFinanceiro.inciarElementoMoeda($(
                    '#pagamentoTituloDescontoManual, #pagamentoTituloAcrescimoManual,' +
                    '#porcentagemDescontoManual,#porcentagemAcrescimoManual'
                ));

                $('#porcentagemAtivo').on('change', function () {
                    var checked = $(this).prop('checked');
                    var $camposPorcentagem = $('.campos-porcentagem-pagamento');
                    var $camposValor = $('.campos-valor-pagamento');

                    if (checked) {
                        $camposPorcentagem.removeClass('hidden');
                        $camposValor.find('input').prop('disabled', true);
                    } else {
                        $camposPorcentagem.addClass('hidden');
                        $camposValor.find('input').prop('disabled', false);
                    }
                });

                $('#tituloPagamentoEfetuarComEmissaoComprovante').click(function (e) {
                    var $form = $('#financeiro-titulo-pagamento-form');
                    $form.data('emitirComprovante', true);
                    $form.submit();

                    e.preventDefault();
                    e.stopPropagation();
                });

                __painelFinanceiro.iniciarElementoDatePicker($("#pagamentoTituloData"), {maxDate: 0});

                $(
                    '#pagamentoTituloDescontoManual, #pagamentoTituloAcrescimoManual, ' +
                    '#pagamentoValorTotalMultaIsentar, #pagamentoValorTotalJurosIsentar,' +
                    '#porcentagemDescontoManual, #porcentagemAcrescimoManual'
                ).change(
                    function () {
                        $('#pagamentoLogin, #pagamentoSenha').val('');
                    }
                );

                $(
                    '#pagamentoTituloDescontoManual, #pagamentoTituloAcrescimoManual, ' +
                    '#pagamentoValorTotalMultaIsentar, #pagamentoValorTotalJurosIsentar,' +
                    '#pagamentoTituloObservacoes, #pagamentoTituloData,' +
                    '#porcentagemDescontoManual, #porcentagemAcrescimoManual'
                ).change(__painelFinanceiro.pagamento.atualizarInformacoesPagamento);

                $('#pagamentoValorTotalMultaIsentar').on('change', function () {
                    if (this.checked) {
                        __painelFinanceiro.showNotificacaoInfo(
                            'Ao marcar esta opção, o valor de <b>multa</b> será <b>desconsiderado</b> ' +
                            'do valor final do título.'
                        );
                    }
                });

                $('#pagamentoValorTotalJurosIsentar').on('change', function () {
                    if (this.checked) {
                        __painelFinanceiro.showNotificacaoInfo(
                            'Ao marcar esta opção, o valor de <b>juros</b> será <b>desconsiderado</b> ' +
                            'do valor final do título.'
                        );
                    }
                });

                $("#pagamentoResponsavelTitulo").select2({
                    language: 'pt-BR',
                    ajax: {
                        url: __painelFinanceiro.options.url.pessoa,
                        dataType: 'json',
                        data: function (query) {
                            return {
                                query: query,
                                somentePesId: __painelFinanceiro.pessoa.pessoasSelecionadasRelacionadas(),
                                pesquisarPJ: __painelFinanceiro.verificarExibicaoPessoaJuridica(),
                                bloquearPesquisaPorPesId: __painelFinanceiro.verificarBloqueioPesquisaPorPesId()
                            };
                        },
                        results: function (data) {
                            return {results: __painelFinanceiro.pessoa.formatarDadosPessoasSomente(data)};
                        }
                    },
                    formatSelection: function (el) {
                        return el.pes_nome;
                    }
                });

                $('#financeiro-titulo-pagamento-add-btn').click(function () {
                    __painelFinanceiro.pagamentoAdd.exibirModal();
                });

                $('#meioPagamentoTroco').select2({
                        allowClear: true,
                        language: 'pt-BR',
                        data: function () {
                            var variaveis = __painelFinanceiro.options.data.variaveisDePagamento || {};
                            var pagamentos = variaveis['pagamentos'] || {};
                            var resultados = [];

                            $.each(pagamentos, function (chave, pagamento) {
                                var valor = pagamento['valorPagamento'] ? pagamento['valorPagamento'] : pagamento['pagamentoValor'];
                                resultados.push({
                                    id: chave,
                                    text: pagamento['meioPagamentoDescricao'] + ' | R$ ' + valor
                                });
                            });
                            return {results: resultados};
                        }
                    }
                );

                $('#pagamento-troco-cancelar-btn').click(function () {
                    $('#financeiro-pagamento-troco-modal').modal('hide');

                    return false;
                });

                var $form = $('#financeiro-titulo-pagamento-form');

                $('#pagamento-troco-salvar-btn').click(function () {
                    var variaveisDePagamento = __painelFinanceiro.options.data.variaveisDePagamento || {};

                    if (!$('#meioPagamentoTroco').val()) {
                        __painelFinanceiro.showNotificacaoWarning('Escolha um meio de pagamento!');

                        return false;
                    }

                    $('#financeiro-pagamento-troco-modal').modal('hide');

                    variaveisDePagamento.meioTroco = $('#meioPagamentoTroco').val();

                    if (variaveisDePagamento.pagamentoTroco > variaveisDePagamento.pagamentos[variaveisDePagamento.meioTroco].pagamentoValor) {
                        __painelFinanceiro.showNotificacaoWarning('O troco não pode ser maior que meio de pagamento aplicado!');

                        return false;
                    }
                    __painelFinanceiro.options.data.variaveisDePagamento = variaveisDePagamento;

                    $form.submit();
                });

                $('#pagamento-confirma-salvar-btn').click(function () {
                    var variaveisDePagamento = __painelFinanceiro.options.data.variaveisDePagamento || {};

                    if ($('#confirmarAcao').val() == 'desconto') {
                        variaveisDePagamento.lancarDiferencaNoMeioDePagamentoPadrao = 0;

                        if (variaveisDePagamento.pagamentoTroco < 0) {
                            variaveisDePagamento.descontoManual = (
                                variaveisDePagamento.descontoManual + (variaveisDePagamento.pagamentoTroco * -1)
                            );
                            variaveisDePagamento.pagamentoTroco = 0;
                            __painelFinanceiro.pagamento.atualizarDatatablesTitulosPagamento();
                        }
                    } else if ($('#confirmarAcao').val() == 'pagamento') {
                        variaveisDePagamento.lancarDiferencaNoMeioDePagamentoPadrao = 1;
                    } else {
                        __painelFinanceiro.showNotificacaoWarning('Valor do pagamento inferior ao valor total!');
                        return false;
                    }

                    __painelFinanceiro.options.data.variaveisDePagamento = variaveisDePagamento;

                    $('#financeiro-confirmar-pagamento-modal').modal('hide');

                    __painelFinanceiro.removeOverlay($('#financeiro-titulo-pagamento-modal').find('.modal-content'));

                    $form.submit();
                });

                $('#pagamento-confirma-cancelar-btn').click(function () {
                    $('#financeiro-confirmar-pagamento-modal').modal('hide');
                    return false;
                });

                $('#confirmarAcao').select2({
                    data: [
                        {id: 'desconto', text: 'Conceder desconto para o valor faltante.'},
                        {id: 'pagamento', text: 'Inserir o valor faltante no meio de pagamento padrão.'}
                    ]
                });

                $form.validate({
                    submitHandler: function (form) {
                        var $modal = $('#financeiro-titulo-pagamento-modal');
                        var variaveisDePagamento = __painelFinanceiro.options.data.variaveisDePagamento || {};
                        var pagamentos = variaveisDePagamento['pagamentos'] || {};
                        $('#confirmarAcao').val('').trigger('change');

                        __painelFinanceiro.pagamento.atualizarInformacoesPagamento();

                        if (variaveisDePagamento.valorFinal < 0) {
                            __painelFinanceiro.showNotificacaoWarning("O valor do pagamento deve ser maior que zero!");

                            return false;
                        }

                        if (variaveisDePagamento.pagamentoTroco < 0 && !variaveisDePagamento.lancarDiferencaNoMeioDePagamentoPadrao) {
                            $('#confirmarAcao').val('pagamento').trigger('change');

                            $('#financeiro-confirmar-pagamento-modal').modal('show');

                            return false;
                        }

                        if (!variaveisDePagamento.meioTroco && variaveisDePagamento.pagamentoTroco > 0) {
                            var meioPagtoTroco = null;

                            $.each(variaveisDePagamento.pagamentos, function (chave, pagamento) {
                                if (variaveisDePagamento.pagamentoTroco < pagamento.pagamentoValor) {

                                    meioPagtoTroco = {
                                        id: chave,
                                        text: pagamento['meioPagamentoDescricao'] + ' | R$ ' + pagamento['pagamentoValorFormatado']
                                    };
                                    return false;
                                }
                            });

                            $('#meioPagamentoTroco').select2('data', meioPagtoTroco);
                            $('#financeiro-pagamento-troco-modal').modal('show');
                            return false;
                        }

                        __painelFinanceiro.addOverlay(
                            $modal.find('.modal-content'),
                            'Efetuando pagamento de títulos. Aguarde.'
                        );

                        $.each(variaveisDePagamento['pagamentos'], function (posicao, valor) {
                            variaveisDePagamento['pagamentos'][posicao]['pagamentoValor'] = (
                                __painelFinanceiro.formatarMoedaUS(valor['pagamentoValor'])
                            );

                            variaveisDePagamento['pagamentos'][posicao]['valorPagamento'] = (
                                __painelFinanceiro.formatarMoedaUS(valor['valorPagamento'])
                            );
                        });

                        $.ajax({
                            url: __painelFinanceiro.options.url.financeiroTituloPagamento,
                            type: 'POST',
                            method: 'POST',
                            dataType: 'json',
                            data: variaveisDePagamento,
                            success: function (data) {
                                data = data || [];

                                if (data && data.erro) {
                                    __painelFinanceiro.showNotificacaoWarning(
                                        data['mensagem'] || "Não foi possível efetuar pagamento!"
                                    );
                                } else {
                                    if ($form.data('emitirComprovante')) {
                                        __painelFinanceiro.titulos.comprovanteEmitir(data['tituloId']);
                                        $form.data('emitirComprovante', false);
                                    }

                                    __painelFinanceiro.showNotificacaoSuccess(
                                        data['mensagem'] || "Pagamento efetuado com sucesso!"
                                    );

                                    __painelFinanceiro.pessoa.forcaRecarregarInformacoes();

                                    $modal.modal('hide');
                                }

                                __painelFinanceiro.removeOverlay($modal.find('.modal-content'));
                            },
                            error: function (xhr, error, thrown) {
                                __painelFinanceiro.showNotificacaoWarning(
                                    "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                                );
                                __painelFinanceiro.removeOverlay($modal.find('.modal-content'));
                            }
                        });
                    },
                    ignore: '.ignore',
                    rules: {
                        tituloAcrescimoManual: {minlength: 4},
                        tituloDescontoManual: {minlength: 4},
                        responsavelTitulo: {required: true},
                        tituloObservacoes: {required: false},
                        pagamentoSenha: {
                            required: function () {
                                return $('#pagamentoSenha').is(':visible');
                            }
                        },
                        pagamentoLogin: {
                            required: function () {
                                return $('#pagamentoLogin').is(':visible');
                            }
                        }
                    },
                    messages: {
                        tituloAcrescimoManual: {minlength: 'Tamanho mínimo: 1!'},
                        tituloDescontoManual: {minlength: 'Tamanho mínimo: 1!'},
                        responsavelTitulo: {required: 'Campo obrigatório!'},
                        tituloObservacoes: {required: 'Campo obrigatório!'},
                        pagamentoSenha: {required: 'Campo obrigatório!'},
                        pagamentoLogin: {required: 'Campo obrigatório!'}
                    }
                });

                $('#financeiro-comprovante-impressao').click(function () {
                    $('#corpo-recibo-modal').get(0).contentWindow.print();
                })
            },
            limparDatatablesTitulosPagamento: function () {
                if (__painelFinanceiro.getDatatable('financeiroTituloPagamento')) {
                    $('#dataTableTitulosPagamento').find('tbody tr').each(function (i, line) {
                        var data = __painelFinanceiro
                            .getDatatable('financeiroTituloPagamento')
                            .fnGetData(line);

                        if (!data) {
                            return;
                        }

                        __painelFinanceiro
                            .getDatatable('financeiroTituloPagamento')
                            .api()
                            .row(line)
                            .remove()
                            .draw();
                    });
                }
            },
            atualizarDatatablesTitulosPagamento: function () {
                var $form = $('#financeiro-titulo-pagamento-form');

                if (__painelFinanceiro.getDatatable('financeiroTituloPagamento')) {
                    __painelFinanceiro.pagamento.limparDatatablesTitulosPagamento();
                    var isentarJuros = $('#pagamentoValorTotalJurosIsentar').is(':checked');
                    var isentarMulta = $('#pagamentoValorTotalMultaIsentar').is(':checked');
                    var $autenticacaoPagamento = $form.find('.autenticacao-pagamento');
                    var titulosPagamento = __painelFinanceiro.pessoa.corrigeTitulosDePessoasSelecionadas() || [];
                    var btnGroup = '\
                <div class="text-center">\
                    <div class="btn-group">\
                        <button type="button" class="btn btn-xs btn-primary btn-financeiro-titulo-view"\
                            title="Visualizar informações do título">\
                            <i class="fa fa-eye fa-inverse"></i>\
                        </button>\
                        <button type="button" class="btn btn-xs btn-danger btn-financeiro-titulo-pagamento-remover"\
                            title="Remover título da relação de pagamento">\
                            <i class="fa fa-times"></i>\
                        </button>\
                    </div>\
                </div>';

                    var variaveisDePagamento = __painelFinanceiro.options.data.variaveisDePagamento || {};
                    var senha = $('#pagamentoSenha').val() || '';
                    var login = $('#pagamentoLogin').val() || '';
                    variaveisDePagamento.senha = senha;
                    variaveisDePagamento.login = login;

                    variaveisDePagamento = {
                        lancarDiferencaNoMeioDePagamentoPadrao: 0,
                        meioTroco: 0,
                        titulos: [],
                        pagamentos: __painelFinanceiro.options.data.pagamentos,
                        permissaoRequerida: variaveisDePagamento.permissaoRequerida || false,
                        senha: variaveisDePagamento.senha,
                        login: variaveisDePagamento.login,
                        porcentagemAtivo: variaveisDePagamento.porcentagemAtivo,
                        porcentagemDescontoManual: variaveisDePagamento.porcentagemDescontoManual,
                        porcentagemAcrescimoManual: variaveisDePagamento.porcentagemAcrescimoManual,
                        numeroTitulos: 0,
                        descontoIncentivador: 0,
                        valorTotalTitulos: 0,
                        valorTotalMulta: 0,
                        valorTotalJuros: 0,
                        diasAtraso: 0,
                        mediaAtraso: 0,
                        valorTotalAPagar: 0,
                        valorFinal: 0,
                        descontos: 0,
                        isentarJuros: isentarJuros,
                        isentarMulta: isentarMulta,
                        acrescimo: variaveisDePagamento.acrescimo || 0,
                        descontoManual: variaveisDePagamento.descontoManual || 0,
                        descontoManualTitulo: 0,
                        acrescimoManualTitulo: 0,
                        pagamentoTroco: 0,
                        pagamentoValor: variaveisDePagamento.pagamentoValor || 0,
                        observacao: variaveisDePagamento.observacao || '',
                        responsavelTitulo: variaveisDePagamento.responsavelTitulo || '',
                        pagamentoTituloData: variaveisDePagamento.pagamentoTituloData || ''
                    };

                    var dataAtual = __painelFinanceiro.formatDate(new Date(), 'yymmdd');

                    $.each(titulosPagamento, function (row, titulo) {
                        titulo['acao'] = btnGroup;
                        __painelFinanceiro.getDatatable('financeiroTituloPagamento').api().row.add(titulo).draw();
                        variaveisDePagamento.titulos.push(titulo['titulo_id']);
                        variaveisDePagamento.numeroTitulos += 1;
                        variaveisDePagamento.descontos += parseFloat(titulo['descontos'] || 0);
                        variaveisDePagamento.valorTotalTitulos += parseFloat(titulo['titulo_valor'] || 0);
                        variaveisDePagamento.valorTotalMulta += parseFloat(titulo['titulo_multa_calc']);
                        variaveisDePagamento.valorTotalJuros += parseFloat(titulo['titulo_juros_calc']);
                        variaveisDePagamento.diasAtraso += parseFloat(titulo['dias_atraso']);
                        variaveisDePagamento.descontoManualTitulo += parseFloat(titulo['titulo_desconto_manual'] || 0);
                        variaveisDePagamento.acrescimoManualTitulo += parseFloat(titulo['titulo_acrescimo_manual'] || 0);

                        /*todo  Validação para o calculo do desconto de incetivo, é feito no service e na consulta*/
                        variaveisDePagamento.descontoIncentivador += parseFloat(titulo['valor_desconto_antecipado'] || 0);

                        titulosPagamento[row] = titulo;
                    });

                    variaveisDePagamento.mediaAtraso = __painelFinanceiro.roundNumber(
                        (variaveisDePagamento.diasAtraso / variaveisDePagamento.numeroTitulos), 2
                    );
                    variaveisDePagamento.valorTotalAPagar = __painelFinanceiro.roundNumber(
                        (
                            (isentarJuros ? 0 : variaveisDePagamento.valorTotalJuros) +
                            (isentarMulta ? 0 : variaveisDePagamento.valorTotalMulta) +
                            variaveisDePagamento.valorTotalTitulos
                        ), 2
                    );

                    if (variaveisDePagamento.porcentagemAtivo) {
                        variaveisDePagamento.descontoManual = parseFloat(
                            ((variaveisDePagamento.valorTotalAPagar || 0) *
                            variaveisDePagamento.porcentagemDescontoManual) / 100
                        );
                        variaveisDePagamento.acrescimo = parseFloat(
                            ((variaveisDePagamento.valorTotalAPagar || 0) *
                            variaveisDePagamento.porcentagemAcrescimoManual) / 100
                        );

                        $('#pagamentoTituloDescontoManual').val(variaveisDePagamento.descontoManual);
                        $('#pagamentoTituloAcrescimoManual').val(variaveisDePagamento.acrescimo);
                    } else {
                        variaveisDePagamento.porcentagemDescontoManual =
                            (variaveisDePagamento.descontoManual * 100) / (variaveisDePagamento.valorTotalAPagar);
                        variaveisDePagamento.porcentagemAcrescimoManual =
                            (variaveisDePagamento.acrescimo * 100) / (variaveisDePagamento.valorTotalAPagar);
                        $('#porcentagemDescontoManual').val(variaveisDePagamento.porcentagemDescontoManual);
                        $('#porcentagemAcrescimoManual').val(variaveisDePagamento.porcentagemAcrescimoManual);
                    }

                    var arrMotivo = [];

                    var extrapolouDescontoMaximo = (
                        variaveisDePagamento.porcentagemDescontoManual >
                        __painelFinanceiro.options.value.financeiroMaxPercentualDesconto
                    );

                    if (extrapolouDescontoMaximo) {
                        arrMotivo.push('Extrapolou o percentual máximo de desconto;');
                    }

                    arrMotivo = arrMotivo.join(' ');

                    $form.find('.motivoAutenticacao').html(arrMotivo || '-');

                    if (extrapolouDescontoMaximo) {
                        $autenticacaoPagamento.removeClass('hidden');
                        variaveisDePagamento.permissaoRequerida = true;
                    } else {
                        $autenticacaoPagamento.addClass('hidden');
                        variaveisDePagamento.permissaoRequerida = false;
                    }

                    variaveisDePagamento.valorFinal = __painelFinanceiro.roundNumber(
                        (
                            variaveisDePagamento.valorTotalAPagar +
                            variaveisDePagamento.acrescimo -
                            (
                                variaveisDePagamento.descontos +
                                variaveisDePagamento.descontoManual +
                                variaveisDePagamento.descontoIncentivador
                            )
                        ), 2
                    );

                    variaveisDePagamento.pagamentoTroco = (
                        variaveisDePagamento.valorFinal - variaveisDePagamento.pagamentoValor
                    );

                    variaveisDePagamento.pagamentoTroco *= -1;

                    var $pagamentoValorTroco = $('#pagamentoValorTroco-static');
                    var $valorDoPagamento = $('#pagamentoValor-static');

                    $pagamentoValorTroco.removeClass('text-info');
                    $valorDoPagamento.removeClass('text-danger');

                    if (variaveisDePagamento.pagamentoTroco > 0) {
                        $pagamentoValorTroco.addClass('text-info');
                    }

                    if (
                        variaveisDePagamento.pagamentoValor < variaveisDePagamento.valorFinal &&
                        $("#financeiro-titulo-pagamento-modal").is(":visible")

                    ) {
                        $valorDoPagamento.addClass('text-danger');
                    }

                    $('#pagamentoNumeroTitulos-static').html(variaveisDePagamento.numeroTitulos);
                    $('#pagamentoMediaAtraso-static').html(variaveisDePagamento.mediaAtraso);
                    $('#pagamentoValorTotalJuros-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDePagamento.valorTotalJuros)
                    );
                    $('#pagamentoValorTotalMulta-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDePagamento.valorTotalMulta)
                    );
                    $('#pagamentoValorTotalTitulos-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDePagamento.valorTotalTitulos)
                    );
                    $('#pagamentoValorTotalAPagar-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDePagamento.valorTotalAPagar)
                    );
                    $('#pagamentoTituloDescontoManual').val(
                        __painelFinanceiro.formatarMoeda(variaveisDePagamento.descontoManual)
                    );
                    $('#pagamentoValorDesconto-static').html(
                        __painelFinanceiro.formatarMoeda(0)
                    );
                    $('#pagamentoDescontos-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDePagamento.descontos)
                    );
                    $('#pagamentoTituloAcrescimoManual').val(
                        __painelFinanceiro.formatarMoeda(variaveisDePagamento.acrescimo)
                    );
                    $('#pagamentoValorFinal-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDePagamento.valorFinal)
                    );
                    $('#pagamentoValor-static').html(
                        __painelFinanceiro.formatarMoeda(variaveisDePagamento.pagamentoValor)
                    );

                    var descontoIncentivador = variaveisDePagamento.descontoIncentivador;

                    $('#pagamentoDescontoIncentivo-static').html(
                        __painelFinanceiro.formatarMoeda(descontoIncentivador)
                    );

                    $pagamentoValorTroco.html(__painelFinanceiro.formatarMoeda(
                        parseFloat(variaveisDePagamento.pagamentoTroco) > 0 ? variaveisDePagamento.pagamentoTroco : 0
                    ));

                    if (variaveisDePagamento.pagamentoValor < variaveisDePagamento.valorFinal) {
                        $('#financeiro-titulo-pagamento-add-btn').show();
                    } else {
                        $('#financeiro-titulo-pagamento-add-btn').hide();
                    }

                    __painelFinanceiro.options.data.variaveisDePagamento = variaveisDePagamento;
                    if (variaveisDePagamento.numeroTitulos == 0) {
                        __painelFinanceiro.showNotificacaoWarning("Selecione títulos para efetuar pagamentos!");
                        $('#financeiro-titulo-pagamento-modal').modal('hide');
                        return false;
                    }
                } else {
                    return false;
                }

                return true;
            },
            atualizarInformacoesPagamento: function () {
                var variaveisDePagamento = __painelFinanceiro.options.data.variaveisDePagamento || false;
                var acrescimoManual = $('#pagamentoTituloAcrescimoManual').val() || '0';
                var descontoManual = $('#pagamentoTituloDescontoManual').val() || '0';
                var observacao = $('#pagamentoTituloObservacoes').val() || '';
                var responsavelTitulo = $('#pagamentoResponsavelTitulo').val() || '';
                var pagamentoTituloData = $('#pagamentoTituloData').val() || '';
                var porcentagemDescontoManual = $('#porcentagemDescontoManual').val() || '0';
                var porcentagemAcrescimoManual = $('#porcentagemAcrescimoManual').val() || '0';
                var porcentagemAtivo = $('#porcentagemAtivo').prop('checked') || false;
                var senha = $('#pagamentoSenha').val() || '';
                var login = $('#pagamentoLogin').val() || '';

                acrescimoManual = parseFloat(acrescimoManual.replace('.', '').replace(',', '.'));
                descontoManual = parseFloat(descontoManual.replace('.', '').replace(',', '.'));
                porcentagemDescontoManual = parseFloat(porcentagemDescontoManual.replace('.', '').replace(',', '.'));
                porcentagemAcrescimoManual = parseFloat(porcentagemAcrescimoManual.replace('.', '').replace(',', '.'));

                variaveisDePagamento.porcentagemAtivo = porcentagemAtivo;
                variaveisDePagamento.acrescimo = acrescimoManual;
                variaveisDePagamento.descontoManual = descontoManual;
                variaveisDePagamento.porcentagemDescontoManual = porcentagemDescontoManual;
                variaveisDePagamento.porcentagemAcrescimoManual = porcentagemAcrescimoManual;
                variaveisDePagamento.observacao = observacao;
                variaveisDePagamento.responsavelTitulo = responsavelTitulo;
                variaveisDePagamento.valorPagamento = variaveisDePagamento.valorPagamento || 0;
                variaveisDePagamento.senha = senha;
                variaveisDePagamento.login = login;
                variaveisDePagamento.pagamentoTituloData = (
                    pagamentoTituloData || __painelFinanceiro.formatDate(new Date())
                );

                __painelFinanceiro.options.data.variaveisDePagamento = variaveisDePagamento;

                $('#financeiro-titulo-pagamento-form').valid();

                return __painelFinanceiro.pagamento.atualizarDatatablesTitulosPagamento();
            },
            efetuarPagamentoTitulos: function (e) {
                $('#pagamentoTituloAcrescimoManual').val(0);
                $('#pagamentoTituloDescontoManual').val(0);
                $('#porcentagemAcrescimoManual').val(0);
                $('#porcentagemDescontoManual').val(0);
                $('#pagamentoTituloData').val(__painelFinanceiro.formatDate(new Date()));
                $('#pagamentoTituloParcelas').val(1);
                $('#pagamentoTituloObservacoes').val('');
                $('#pagamentos').val('');
                $('#pagamentoTituloDataVencimento').val('');
                $('#pagamentoValorTotalMultaIsentar, #pagamentoValorTotalJurosIsentar').prop('checked', false);
                $('#porcentagemAtivo').prop('checked', false).trigger('change');
                $('#pagamentoLogin').val('');
                $('#pagamentoSenha').val('');

                var responsavel = null;
                var arrPessoas = __painelFinanceiro.pessoa.pessoasRelacionadasATitulosSelecionadas();

                if (arrPessoas.length == 1) {
                    responsavel = arrPessoas[0];
                }

                $('#pagamentoResponsavelTitulo').select2('data', responsavel).trigger('change');

                __painelFinanceiro.options.data.pagamentos = {};

                if (!__painelFinanceiro.pagamento.atualizarDatatablesMeioPagamento()) {
                    return false;
                }

                var variaveisDePagamento = __painelFinanceiro.options.data.variaveisDePagamento || {};

                $('#pagamentoTituloAcrescimoManual')
                    .val(variaveisDePagamento.acrescimoManualTitulo || 0)
                    .trigger('change');
                $('#pagamentoTituloDescontoManual')
                    .val(variaveisDePagamento.descontoManualTitulo || 0)
                    .trigger('change');

                var meioPagamentoDinheiro = __painelFinanceiro.getMeioPagamentoPadrao();

                if (meioPagamentoDinheiro) {
                    __painelFinanceiro.pagamentoAdd.exibirModal($.extend({}, meioPagamentoDinheiro), true);
                    __painelFinanceiro.pagamentoAdd.salvarPagamento();
                    __painelFinanceiro.pagamento.atualizarDatatablesMeioPagamento();
                }

                $('#financeiro-titulo-pagamento-modal').modal('show');
                e.preventDefault();
                e.stopPropagation();
            },
            iniciarDataTablePagamentoTitulo: function () {
                var colNum = -1;
                var $dataTableTitulosPagamento = $('#dataTableTitulosPagamento');

                var dtFinanceiroTitulo = $dataTableTitulosPagamento.dataTable({
                    processing: true,
                    serverSide: false,
                    data: [],
                    columnDefs: [
                        {name: "titulo_id", targets: ++colNum, data: "titulo_id"},
                        {name: "pes_nome", targets: ++colNum, data: "pes_nome"},
                        {name: "titulo_descricao", targets: ++colNum, data: "titulo_descricao"},
                        {
                            name: "titulo_data_processamento", targets: ++colNum,
                            data: "titulo_data_processamento_formatado"
                        },
                        {name: "titulo_data_vencimento", targets: ++colNum, data: "titulo_data_vencimento_formatado"},
                        {name: "titulo_valor", targets: ++colNum, data: "titulo_valor"},
                        {name: "titulo_valor", targets: ++colNum, data: "titulo_valor_formatado"},
                        {name: "titulo_estado", targets: ++colNum, data: "titulo_estado"},
                        {name: "titulo_id", targets: ++colNum, data: "acao", orderable: false}
                    ],
                    "dom": "rt<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[5, 'desc']]
                });

                __painelFinanceiro.setDatatable('financeiroTituloPagamento', dtFinanceiroTitulo);

                $dataTableTitulosPagamento.on('click', '.btn-financeiro-titulo-view', function (e) {

                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable('financeiroTituloPagamento').fnGetData($pai);
                    __painelFinanceiro.vizualizacao.carregarDetalhesTitulo(data);
                    e.preventDefault();
                    e.stopPropagation();
                });

                $dataTableTitulosPagamento.on('click', '.btn-financeiro-titulo-pagamento-remover', function (e) {
                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable('financeiroTituloPagamento').fnGetData($pai);
                    var titulosSelecionados = (
                        $.storage.get('financeiro.painel.titulosSelecionados') || {}
                    );
                    titulosSelecionados[data['titulo_id']] = data;
                    delete titulosSelecionados[data['titulo_id']];

                    $.storage.set(
                        'financeiro.painel.titulosSelecionados',
                        titulosSelecionados
                    );

                    __painelFinanceiro.titulos.atualizarDatatablesTitulos();
                    __painelFinanceiro.pagamento.atualizarDatatablesTitulosPagamento();
                    e.preventDefault();
                    e.stopPropagation();
                });
            },
            iniciarDataTableMeioPagamento: function () {
                var colNum = -1;
                var $dataTableTitulosPagamento = $('#dataTableTitulosMeioPagamento');

                var dtFinanceiroTitulo = $dataTableTitulosPagamento.dataTable({
                    processing: true,
                    ordering: false,
                    serverSide: false,
                    data: [],
                    columnDefs: [
                        {name: "meioPagamentoDescricao", targets: ++colNum, data: "meioPagamentoDescricao"},
                        {name: "pagamentoValorFormatado", targets: ++colNum, data: "pagamentoValor"},
                        {name: "pagamentoId", targets: ++colNum, data: "acao", orderable: false}
                    ],
                    "dom": "rt<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                });

                __painelFinanceiro.setDatatable('pagamentos', dtFinanceiroTitulo);

                $dataTableTitulosPagamento.on('click', '.btn-financeiro-pagamentos-editar', function (e) {
                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable('pagamentos').fnGetData($pai);
                    var pagamentos = __painelFinanceiro.options.data.pagamentos || {};

                    __painelFinanceiro.pagamentoAdd.exibirModal(pagamentos[data['pagamentoGUID']]);

                    e.preventDefault();
                    e.stopPropagation();
                });

                $dataTableTitulosPagamento.on('click', '.btn-financeiro-pagamentos-remover', function (e) {
                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable('pagamentos').fnGetData($pai);

                    var pagamentos = __painelFinanceiro.options.data.pagamentos || {};

                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Deseja remover o meio de pagamento "' +
                        data['meioPagamentoDescricao'] +
                        '" da relação de pagamentos?',
                        buttons: "[Não][Sim]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Sim") {
                            delete pagamentos[data['pagamentoGUID']];
                            __painelFinanceiro.pagamento.atualizarDatatablesMeioPagamento();
                        }
                    });
                    e.preventDefault();
                    e.stopPropagation();
                });
            },
            limparDatatablesMeioPagamento: function () {
                if (__painelFinanceiro.getDatatable('pagamentos')) {
                    $('#dataTableTitulosMeioPagamento').find('tbody tr').each(function (i, line) {
                        var data = __painelFinanceiro.getDatatable('pagamentos').fnGetData(line);

                        if (!data) {
                            return;
                        }

                        __painelFinanceiro.getDatatable('pagamentos').api().row(line).remove().draw();
                    });
                }
            },
            atualizarDatatablesMeioPagamento: function () {
                if (__painelFinanceiro.getDatatable('pagamentos')) {
                    __painelFinanceiro.pagamento.limparDatatablesMeioPagamento();
                    var btnGroup = '\
                <div class="text-center">\
                    <div class="btn-group">\
                        <button type="button" class="btn btn-xs btn-primary btn-financeiro-pagamentos-editar"\
                            title="Editar meio de pagamento">\
                            <i class="fa fa-pencil fa-inverse"></i>\
                        </button>\
                        <button type="button" class="btn btn-xs btn-danger btn-financeiro-pagamentos-remover"\
                            title="Remover título da relação de pagamento">\
                            <i class="fa fa-times"></i>\
                        </button>\
                    </div>\
                </div>';
                    var variaveisDePagamento = __painelFinanceiro.options.data.variaveisDePagamento || {};
                    var pagamentos = __painelFinanceiro.options.data.pagamentos || {};
                    variaveisDePagamento.pagamentoValor = 0;

                    $.each(pagamentos, function (row, pagamento) {
                        pagamento['acao'] = btnGroup;
                        __painelFinanceiro.getDatatable('pagamentos').api().row.add(pagamento).draw();
                        variaveisDePagamento.pagamentoValor += parseFloat(
                            __painelFinanceiro.formatarMoedaUS(pagamento['pagamentoValor'])
                        );
                        pagamentos[row] = pagamento;
                    });

                    __painelFinanceiro.options.data.pagamentos = pagamentos;
                    __painelFinanceiro.options.data.variaveisDePagamento = variaveisDePagamento;
                    return __painelFinanceiro.pagamento.atualizarInformacoesPagamento();
                }
                return false;
            }
        };

        this.vizualizacao = {
            iniciarCamposView: function () {
                var $form = $('#financeiro-titulo-view-form');
                $form.on('click', '.btn-atualizar-titulo', __painelFinanceiro.vizualizacao.efetuarAtualizacaoDeTitulos);

                $form.on('click', '.btn-estornar-titulo', __painelFinanceiro.vizualizacao.financeiroEstornarTitulo);

                __painelFinanceiro.vizualizacao.iniciarDataTableTitulosOrigem();
                __painelFinanceiro.vizualizacao.iniciarDataTableTitulosDestino();

            },
            validarPercentualDeDesconto: function () {
                var tituloAtualizarDescontoManual = $('#tituloAtualizarDescontoManual').val();
                var percetualDesconto = (
                    (parseFloat(tituloAtualizarDescontoManual) * 100 ) /
                    (__painelFinanceiro.options.value.tituloEmVisualizacao['tituloValorFinal'] )
                );

                var extrapolouDescontoMaximo = (
                    percetualDesconto >
                    __painelFinanceiro.options.value.financeiroMaxPercentualDesconto

                );

                return extrapolouDescontoMaximo;
            },
            iniciarCamposInterface: function () {
                __painelFinanceiro.inciarElementoMoeda($('#tituloAtualizarDescontoManual,#tituloAtualizarAcrescimoManual'));
                __painelFinanceiro.iniciarElementoDatePicker($("#tituloAtualizarDataVencimento"), {minDate: 0});

                var arrDados = {},
                    $form = $('#financeiro-titulo-view-form'),
                    $modal = $('#financeiro-titulo-atualizar-titulo-modal');

                arrDados['tituloId'] = $form.find('[name=tituloId]').val();
                $form = $('#financeiro-titulo-atualizar-titulo-form');

                $("#tituloAtualizarDescontoManual").change(function () {
                    $('#atualizarTituloLogin,#atualizarTituloSenha').val('');

                    if (__painelFinanceiro.vizualizacao.validarPercentualDeDesconto()) {
                        $('.autenticacao-atualizacao').removeClass('hidden');
                    } else {
                        $('.autenticacao-atualizacao').addClass('hidden');
                    }

                });

                $('#tituloAtualizarDescontoManual, #tituloAtualizarDataVencimento, #tituloAtualizarAcrescimoManual')
                    .click(__painelFinanceiro.vizualizacao.atualizarInformacaoAtualizacao);

                $form.validate({
                    submitHandler: function () {
                        __painelFinanceiro.addOverlay($modal, "Atualizando o título!");

                        var dados = $form.serializeJSON();

                        $.ajax({
                            url: $form.attr('action'),
                            dataType: 'json',
                            type: 'POST',
                            data: dados,
                            success: function (data) {
                                if (data.erro == true) {
                                    __painelFinanceiro.showNotificacaoDanger(
                                        data['mensagem'] || 'Ocorreu um erro durante a atualização do título!'
                                    );
                                } else {
                                    __painelFinanceiro.showNotificacaoSuccess(
                                        data['mensagem'] || 'Título atualizado com sucesso!'
                                    );

                                    __painelFinanceiro.titulos.atualizarDatatablesTitulos();

                                    __painelFinanceiro.vizualizacao.carregarDetalhesTitulo({titulo_id: data['tituloId']});

                                    $modal.modal('hide');
                                }

                                __painelFinanceiro.removeOverlay($modal);
                            },
                            error: function (xhr, error, thrown) {
                                __painelFinanceiro.showNotificacaoWarning(
                                    "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                                );
                                __painelFinanceiro.removeOverlay($modal);
                            }
                        });
                    },
                    rules: {
                        tituloAtualizarDataVencimento: {required: true, dateBR: true},
                        tituloAtualizarDescontoManual: {minlength: 0},
                        atualizarTituloSenha: {
                            required: function () {
                                return __painelFinanceiro.vizualizacao.validarPercentualDeDesconto();

                            }
                        },
                        atualizarTituloLogin: {
                            required: function () {
                                return __painelFinanceiro.vizualizacao.validarPercentualDeDesconto();

                            }
                        }
                    },
                    messages: {
                        tituloAtualizarDataVencimento: {required: 'Campo obrigatório'},
                        tituloAtualizarDescontoManual: {minlength: 'Tamanho mínimo 1!'}
                    },
                    ignore: '.ignore'
                });
            },
            atualizarInformacaoAtualizacao: function () {
                var desconto = $('#tituloAtualizarDescontoManual').val() || '0';
                var acrescimo = $('#tituloAtualizarAcrescimoManual').val() || '0';
                var dataDeVencimento = $('#tituloAtualizarDataVencimento').val() || '';
                var observacao = $('#tituloAtualizarObservacao').val() || '';
                var senha = $('#atualizarTituloSenha').val() || '';
                var login = $('#atualizarTituloLogin').val() || '';
                var $form = $('#financeiro-titulo-atualizar-titulo-form');

                if ((parseFloat(desconto)) < 0) {
                    __painelFinanceiro.showNotificacaoDanger("Valor do Acréscimo inválido!");
                    return false;
                } else if ((parseFloat(acrescimo)) < 0) {
                    __painelFinanceiro.showNotificacaoDanger("Valor de Desconto inválido!");
                    return false;
                }

                var dataAtual = parseInt(__painelFinanceiro.formatDate(new Date(), 'yymmdd'));
                var dtVencimentoVal = 1 + parseInt(__painelFinanceiro.formatDate(dataDeVencimento, 'yymmdd'));

                if (dataAtual > dtVencimentoVal) {
                    __painelFinanceiro.showNotificacaoDanger("Data inválida!");
                    return false;
                }

                $('#tituloAtualizarDescontoManual,#tituloAtualizarAcrescimoManual').change(function () {

                    var arrDados = __painelFinanceiro.options.value.tituloEmVisualizacao || {};

                    var valorFinal = __painelFinanceiro.formatarMoedaUS(__painelFinanceiro.formatarMoeda(arrDados['tituloValorFinal']));
                    arrDados['tituloDescontoManual'] = 0 + $('.tituloAtualizarDescontoManual').val();
                    arrDados['tituloAtualizarAcrescimoManual'] = 0 + $('.tituloAtualizarAcrescimoManual').val();
                    valorFinal = __painelFinanceiro.formatarMoedaUS(valorFinal);
                    arrDados['tituloDescontoManual'] =
                        __painelFinanceiro.formatarMoedaUS(arrDados['tituloDescontoManual']);
                    arrDados['tituloAtualizarAcrescimoManual'] =
                        __painelFinanceiro.formatarMoedaUS(arrDados['tituloAtualizarAcrescimoManual']);
                    var conf = (
                        parseFloat(valorFinal) - parseFloat(arrDados['tituloDescontoManual']) +
                        parseFloat(arrDados['tituloAtualizarAcrescimoManual'])
                    );

                    if (conf < 0) {
                        __painelFinanceiro.showNotificacaoWarning(
                            "Verifique o valor do desconto digitado! " + "<br>" +
                            "Valor máximo: " + __painelFinanceiro.formatarMoeda(valorFinal));
                        arrDados['tituloDescontoManual'] = 0;
                        desconto = $('#tituloAtualizarDescontoManual').val(0);
                        $('.autenticacao-atualizacao').addClass('hidden');

                        $form.find('.atualizarTituloValorFinal').html(__painelFinanceiro.formatarMoeda(0));
                    } else {
                        $form.find('.atualizarTituloValorFinal').html(__painelFinanceiro.formatarMoeda(conf));
                    }
                });
            },
            efetuarAtualizacaoDeTitulos: function () {
                $('.autenticacao-atualizacao').addClass('hidden');
                __painelFinanceiro.removeOverlay($('#financeiro-titulo-atualizar-titulo-modal'));

                $('#tituloAtualizarDataVencimento').val(__painelFinanceiro.formatDate(new Date()));
                $('#tituloAtualizarDescontoManual').val(0);
                $('#tituloAtualizarAcrescimoManual').val(0);
                $('#tituloAtualizarObservacao').val('');
                $('#atualizarTituloLogin').val('');
                $('#atualizarTituloSenha').val('');
                $('#financeiro-titulo-atualizar-titulo-modal').modal('show');

                var arrDados = __painelFinanceiro.options.value.tituloEmVisualizacao || {};
                var $form = $('#financeiro-titulo-atualizar-titulo-form');
                $form.find('[name="tituloId"]').val(arrDados['tituloId']);

                $form.find('.atualizarTituloValorFinal').html(__painelFinanceiro.formatarMoeda(arrDados['tituloValorFinal']));

            },
            financeiroEstornarTitulo: function () {
                var $form = $('#financeiro-titulo-view-form'),
                    titulosVinculados = $form.find('[name=titulosVinculados]').val(),
                    titulosVinculadosCartao = (__painelFinanceiro.options.data.titulosVinculadosCartao).join(","),
                    tituloId = $form.find('[name=tituloId]').val(),
                    vinculos = titulosVinculados ? "Títulos Vinculados: " : 'Título Vinculado:';

                titulosVinculados = titulosVinculados ? titulosVinculados :
                    titulosVinculadosCartao ? titulosVinculadosCartao : '';

                if (titulosVinculadosCartao) {
                    __painelFinanceiro.showNotificacaoWarning("Este estorno irá estornar os títulos vinculados(" + titulosVinculadosCartao + ") pois foram pagos juntos usando cartão!");
                }

                __painelFinanceiro.confirmacao.exibir(
                    'estorno',
                    'Deseja estonar o título "' + tituloId + '"?',
                    (!titulosVinculados ? tituloId : (titulosVinculados.split(','))),
                    vinculos
                );
            },
            efetuarEstornoTitulo: function (tituloId, observacao) {
                var $form = $('#financeiro-titulo-view-form');
                var $modalContent = $form.closest('.modal-content');

                __painelFinanceiro.addOverlay($modalContent, 'Efetuando estorno do título. Aguarde.');

                $.ajax({
                    url: __painelFinanceiro.options.url.estornarTitulo,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        tituloId: tituloId,
                        observacao: observacao
                    },
                    success: function (data) {
                        __painelFinanceiro.removeOverlay($modalContent);

                        if (data.erro) {
                            __painelFinanceiro.showNotificacaoDanger(
                                "Não foi possivel atualizar título!" +
                                (data['mensagem'] || '')
                            );
                        } else {
                            var arrTitulos = data['tituloId'] || [];

                            if (arrTitulos.length > 0) {
                                var mensagem = "Título estornado!<br>";

                                if (arrTitulos.length == 1) {
                                    var arrTitulo = {'titulo_id': data['tituloId'][0]};
                                    __painelFinanceiro.vizualizacao.carregarDetalhesTitulo(
                                        arrTitulo,
                                        'Aguarde, processando informações do novo título!'
                                    );
                                    mensagem += "Novo título: " + data['tituloId'][0];
                                } else {
                                    mensagem += "Novos títulos: " + data['tituloId'].join(', ');

                                    $modalContent.closest('.modal').modal('hide');
                                }

                                __painelFinanceiro.showNotificacaoSuccess(mensagem);
                            }

                            __painelFinanceiro.titulos.atualizarDatatablesTitulos();
                        }
                    },
                    error: function (xhr, error, thrown) {
                        __painelFinanceiro.showNotificacaoWarning(
                            "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                        );
                        __painelFinanceiro.removeOverlay($modalContent);
                    }
                });
            },
            atualizarDatatables: function (elementoId, datatableNome, arrDados) {
                if (__painelFinanceiro.getDatatable(datatableNome)) {
                    $(elementoId + ' tbody tr').each(function (i, line) {
                        var data = __painelFinanceiro.getDatatable(datatableNome).fnGetData(line);

                        if (!data) {
                            return;
                        }

                        __painelFinanceiro.getDatatable(datatableNome).api().row(line).remove().draw();
                    });

                    var btnGroup = '\
                <div class="text-center">\
                    <div class="btn-group">\
                        <button type="button" class="btn btn-xs btn-primary btn-financeiro-titulo-view"\
                            title="Visualizar informações do título">\
                            <i class="fa fa-eye fa-inverse"></i>\
                        </button>\
                    </div>\
                </div>';

                    $.each(arrDados, function (row, titulo) {
                        titulo['acao'] = btnGroup;
                        __painelFinanceiro.getDatatable(datatableNome).api().row.add(titulo).draw();
                        arrDados[row] = titulo;
                    });
                } else {
                    return false;
                }

                return true;
            },
            atualizarDatatablesOrigem: function (arrDados) {
                $.each(arrDados, function (row, titulo) {
                    titulo['titulo_id'] = titulo['titulo_id_origem'];
                    titulo['titulo_descricao'] = titulo['titulo_descricao_origem'];
                    titulo['titulo_estado'] = titulo['titulo_estado_origem'];
                    titulo['titulo_valor'] = titulo['titulo_valor_origem'];
                    titulo['titulo_valor_pago'] = titulo['titulo_valor_pago_origem'] || titulo['titulo_valor'] || 0;
                    titulo['titulo_data_vencimento'] = titulo['titulo_data_vencimento_origem'];
                    arrDados[row] = titulo;
                });
                __painelFinanceiro.vizualizacao.atualizarDatatables(
                    '#dataTableTitulosOrigem',
                    'financeiroTituloOrigem',
                    arrDados
                );
            },
            atualizarDatatablesDestino: function (arrDados) {
                $.each(arrDados, function (row, titulo) {
                    titulo['titulo_id'] = titulo['titulo_id_destino'];
                    titulo['titulo_descricao'] = titulo['titulo_descricao_destino'];
                    titulo['titulo_estado'] = titulo['titulo_estado_destino'];
                    titulo['titulo_valor'] = titulo['titulo_valor_destino'] || 0;
                    titulo['titulo_valor_pago'] = titulo['titulo_valor_pago_destino'] || titulo['titulo_valor'] || 0;
                    titulo['titulo_data_vencimento'] = titulo['titulo_data_vencimento_destino'];
                    arrDados[row] = titulo;
                });
                __painelFinanceiro.vizualizacao.atualizarDatatables(
                    '#dataTableTitulosDestino',
                    'financeiroTituloDestino',
                    arrDados
                );
            },
            iniciarDataTableTitulosOrigem: function () {
                __painelFinanceiro.vizualizacao.iniciarDataTableTitulosAlteracao(
                    '#dataTableTitulosOrigem',
                    'financeiroTituloOrigem'
                );
            },
            iniciarDataTableTitulosDestino: function () {
                __painelFinanceiro.vizualizacao.iniciarDataTableTitulosAlteracao(
                    '#dataTableTitulosDestino',
                    'financeiroTituloDestino'
                );
            },
            iniciarDataTableTitulosAlteracao: function (elementoId, datatableNome) {
                var colNum = -1;

                var dtFinanceiroTitulo = $(elementoId).dataTable({
                    processing: true,
                    serverSide: false,
                    data: [],
                    columnDefs: [
                        {name: "titulo_id", targets: ++colNum, data: "titulo_id"},
                        {name: "titulo_descricao", targets: ++colNum, data: "titulo_descricao"},
                        {name: "titulo_data_vencimento", targets: ++colNum, data: "titulo_data_vencimento"},
                        {name: "titulo_valor", targets: ++colNum, data: "titulo_valor"},
                        {name: "titulo_valor_pago", targets: ++colNum, data: "titulo_valor_pago"},
                        {name: "titulo_estado", targets: ++colNum, data: "titulo_estado"},
                        {name: "alteracao_acao", targets: ++colNum, data: "alteracao_acao"},
                        {name: "titulo_id", targets: ++colNum, data: "acao", orderable: false}
                    ],
                    "dom": "rt<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'desc']]
                });

                __painelFinanceiro.setDatatable(datatableNome, dtFinanceiroTitulo);

                $(elementoId).on('click', '.btn-financeiro-titulo-view', function (e) {
                    var $pai = $(this).closest('tr');
                    var data = __painelFinanceiro.getDatatable(datatableNome).fnGetData($pai);
                    __painelFinanceiro.vizualizacao.carregarDetalhesTitulo(data);
                    e.preventDefault();
                    e.stopPropagation();
                });
            },
            exibirDados: function (arrDados) {
                arrDados = arrDados || {};
                __painelFinanceiro.options.value.tituloEmVisualizacao = arrDados;
                __painelFinanceiro.showNotificacaoWarning(arrDados['msgWarning']);
                __painelFinanceiro.options.data.titulosVinculadosCartao = arrDados.vinculos.titulosVinculadosCartao;

                var $form = $('#financeiro-titulo-view-form');
                $form.find('[name=tituloId]').val(arrDados['tituloId'] || '');

                $('#tituloConfig-static')
                    .html(arrDados['tituloConfig']['tituloconfNome'] || '-');
                $('#tituloId-static')
                    .html(arrDados['tituloId'] || '-');
                $('#usuarioAutor-static')
                    .html(arrDados['usuarioAutorLogin'] || '-');
                $('#usuarioBaixa-static')
                    .html(arrDados['usuarioBaixaLogin'] || '-');
                $('#tipotitulo-static')
                    .html((arrDados['tipotituloId'] + '-' + arrDados['tipotituloNome']) || '-');
                $('#tituloDescricao-static')
                    .html(arrDados['tituloDescricao'] || '-');
                $('#pes-static')
                    .html((arrDados['pesId'] + '-' + arrDados['pesNome']) || '-');
                $('#aluno-static')
                    .html(arrDados['alunocursoId'] ? (arrDados['alunocursoId'] + '-' + arrDados['alunoNome']) : '-');
                $('#campuscurso-static')
                    .html(arrDados['campNome'] ? (arrDados['campNome'] + '/' + arrDados['cursoNome']) : '-');
                $('#tituloParcela-static')
                    .html(arrDados['tituloParcela'] || '-');
                $('#tituloDataProcessamento-static')
                    .html(arrDados['tituloDataProcessamento'] ? __painelFinanceiro.formatDateTime(arrDados['tituloDataProcessamento']) : '-');
                $('#tituloDataVencimento-static')
                    .html(arrDados['tituloDataVencimento'] ? __painelFinanceiro.formatDate(arrDados['tituloDataVencimento']) : '-');
                $('#tituloValor-static')
                    .html(__painelFinanceiro.formatarMoeda(arrDados['tituloValor']));
                $('#tituloTipoPagamento-static')
                    .html(arrDados['tituloTipoPagamento'] || '-');
                $('#tituloDataPagamento-static')
                    .html(arrDados['tituloDataPagamento'] ? __painelFinanceiro.formatDateTime(arrDados['tituloDataPagamento']) : '-');
                $('#tituloDataBaixa-static')
                    .html(arrDados['tituloDataBaixa'] ? __painelFinanceiro.formatDateTime(arrDados['tituloDataBaixa']) : '-');
                $('#tituloMulta-static')
                    .html(__painelFinanceiro.formatarMoeda(arrDados['tituloMultaCalc']));
                $('#tituloJuros-static')
                    .html(__painelFinanceiro.formatarMoeda(arrDados['tituloJurosCalc']));
                $('#tituloEstado-static')
                    .html(arrDados['tituloEstado'] || '-');
                $('#tituloValorPago-static')
                    .html(__painelFinanceiro.formatarMoeda(arrDados['tituloValorPago']));
                $('#tituloValorPagoDinheiro-static')
                    .html(__painelFinanceiro.formatarMoeda(arrDados['tituloValorPagoDinheiro']));
                $('#tituloDescontoManual-static')
                    .html(__painelFinanceiro.formatarMoeda(arrDados['tituloDescontoManual']));
                $('#tituloAcrescimoManual-static')
                    .html(__painelFinanceiro.formatarMoeda(arrDados['tituloAcrescimoManual']));
                $('#tituloObservacoes-static')
                    .html(arrDados['tituloObservacoes'] || '-');
                $('#descontos-static')
                    .html(__painelFinanceiro.formatarMoeda(
                        parseFloat(arrDados['tituloDesconto'] || 0) +
                        parseFloat(arrDados['descontos'] || 0) +
                        parseFloat(arrDados['tituloDescontoManual'] || 0) +
                        parseFloat(arrDados['valorDescontoAntecipado'] || 0)
                    ));
                $('#tituloDesconto-static')
                    .html(__painelFinanceiro.formatarMoeda(
                        parseFloat(arrDados['descontos'] || 0)));

                var valorFinal = 0;

                if (!__painelFinanceiro.retornaSeDataMaiorQueAtual(arrDados['dataDescontoAntecipado'])) {
                    valorFinal = arrDados['tituloValorFinal'] - arrDados['valorDescontoAntecipado'];
                }

                var valorFinalStatic = arrDados['tituloEstado'] == 'Aberto' ? valorFinal : arrDados['tituloValorPago'];

                $('#tituloValorFinal-static')
                    .html(__painelFinanceiro.formatarMoeda(valorFinalStatic));
                $('#tituloDiasAtraso-static')
                    .html(arrDados['diasAtraso'] || '0');
                $('#tituloDescontoIncentivo-static')
                    .html(__painelFinanceiro.formatarMoeda(arrDados['valorDescontoAntecipado']));

                var boletos = [];
                var nossoNumero = '';

                if (arrDados['tituloEstado'] == 'Aberto' && __painelFinanceiro.options.value.boletoAtivado) {
                    var arrBoletos = arrDados['boletos'] || [];

                    $.each(arrBoletos, function (i, boleto) {
                        var text = ' &nbsp; Boleto';
                        nossoNumero += ' ' + boleto['bol_nosso_numero'];

                        if (
                            !__painelFinanceiro.options.value.boletoRemessaAtivada ||
                            (__painelFinanceiro.options.value.boletoRemessaAtivada && parseInt(boleto['esta_na_remessa']) > 0)
                        ) {
                            boletos.push({
                                class: 'btn-info', size: 'sm', element: 'a', target: '_blank',
                                title: 'Link para o boleto ' + boleto['bol_id'],
                                text: text, icon: 'fa-print',
                                href: __painelFinanceiro.options.url.boletoExibe + '/' + boleto['bol_id']
                            });
                        }
                    });

                    if (__painelFinanceiro.options.value.pagseguroAtivado) {
                        boletos.push({
                            class: 'btn btn-success', size: 'sm', element: 'a', target: '_blank',
                            title: 'Pagar com Pagseguro', text: ' &nbsp; Pagseguro', icon: 'fa-money',
                            href: __painelFinanceiro.options.url.pagseguroCobranca + '/' + arrDados['tituloId']
                        });
                    }

                    var permissaoAtualizarTitulo = __painelFinanceiro.options.value.arrPermissoes.atualizarTitulo == 1;
                    var atualizarTitulo = (
                        arrDados['diasAtraso'] > 0 ||
                        __painelFinanceiro.options.value.financeiroAtualizacaoTituloAberto
                    );

                    if (permissaoAtualizarTitulo && atualizarTitulo) {
                        boletos.push({
                            class: 'btn-warning btn-atualizar-titulo', size: 'sm',
                            title: 'Atualizar Título', text: ' &nbsp; Atualizar Título', icon: 'fa fa-refresh',
                        });

                    }
                }

                nossoNumero = nossoNumero ? nossoNumero : '-';
                $('#bolNossoNumero-static').html(nossoNumero);

                var arrBoleto = arrDados['boletos'] || [];
                arrBoleto = arrBoleto[0] || [];

                var bolValorUnitario = parseFloat(arrBoleto['bol_valor_unitario'] ? arrBoleto['bol_valor_unitario'] : 0);
                var somaDescontos = parseFloat(arrDados['somaDescontos'] ? arrDados['somaDescontos'] : 0)
                var permissaoEstornarTitulo = __painelFinanceiro.options.value.arrPermissoes.estornarTitulo || false;
                var permitirBotaoEstornarTitulo = (bolValorUnitario > somaDescontos) || (valorFinalStatic > somaDescontos);
                $form.find('[name=titulosVinculados]').val('');

                if (permissaoEstornarTitulo && (arrDados['tituloEstado'] == "Pago" || arrDados['tituloEstado'] == "Pagamento Múltiplo" ) && permitirBotaoEstornarTitulo) {
                    boletos.push({
                        class: 'btn btn-danger btn-estornar-titulo', size: 'sm',
                        title: 'Estorno de Título', text: ' &nbsp; Estornar Título', icon: 'fa fa-reply'
                    });

                    var arrOrigem = arrDados.vinculos.origem;
                    var titulosVinculados = [];

                    $.each(arrOrigem, function (i, origem) {
                        titulosVinculados.push(origem['titulo_id_origem']);
                    });

                    $form.find('[name=titulosVinculados]').val(
                        arrDados.vinculos.titulosVinculados ? arrDados.vinculos.titulosVinculados :
                            !(JSON.stringify(titulosVinculados) === '[]') ? titulosVinculados.join(',') : null);

                    $('#confirmar-acao-modal').find('[name=tituloVinculadoId]').val('');
                    if (arrDados.vinculos.tituloDestino) {
                        $('#confirmar-acao-modal').find('[name=tituloVinculadoId]').val(arrDados.vinculos.tituloDestino);
                    }
                }

                var arrPagamentos = arrDados['pagamentos'] || [];
                var arrStrPagamentos = [];

                $.each(arrPagamentos, function (i, pagamento) {
                    arrStrPagamentos.push(
                        pagamento['meio_pagamento_descricao'] +
                        ': ' +
                        pagamento['pagamento_valor_final']
                    );
                });

                if (boletos.length > 0) {
                    $('#boletos-static').html(__painelFinanceiro.createBtnGroup(boletos, {align: 'right'}));
                } else {
                    $('#boletos-static').html('-');
                }

                if (arrStrPagamentos.length > 0) {
                    $('#tituloTipoPagamento-static').html(arrStrPagamentos.join('; '));
                }

                __painelFinanceiro.vizualizacao.atualizarDatatablesDestino(arrDados['vinculos']['destino'] || []);
                __painelFinanceiro.vizualizacao.atualizarDatatablesOrigem(arrDados['vinculos']['origem'] || []);

            },
            carregarDetalhesTitulo: function (arrTitulo, mensagem) {
                mensagem = mensagem || 'Aguarde, processando informações do título!';
                __painelFinanceiro.addOverlay(
                    $(
                        '#titulos-tab, ' +
                        '#financeiro-titulo-view-modal .modal-content, ' +
                        '#financeiro-titulo-pagamento-modal .modal-content, ' +
                        '#financeiro-titulo-renegociacao-modal .modal-content'
                    ),
                    mensagem
                );

                $('#financeiro-titulo-view-modal').find('.modal-title:first')
                    .html('Detalhes do título <b>' + parseInt(arrTitulo['titulo_id']) + '</b>');

                $.ajax({
                    url: __painelFinanceiro.options.url.financeiroTituloInformacoes + '/' + arrTitulo['titulo_id'],
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        if (data.erro) {
                            __painelFinanceiro.showNotificacaoWarning(
                                data['mensagem'] || "Não foi possível encontrar informação sobre o título!"
                            );
                        } else {
                            __painelFinanceiro.vizualizacao.exibirDados(data['arrDados'] || {});

                            $('#financeiro-titulo-view-modal').modal('show');
                        }

                        __painelFinanceiro.removeOverlay(
                            $(
                                '#titulos-tab, ' +
                                '#financeiro-titulo-view-modal .modal-content, ' +
                                '#financeiro-titulo-pagamento-modal .modal-content, ' +
                                '#financeiro-titulo-renegociacao-modal .modal-content'
                            )
                        );
                    },
                    error: function (xhr, error, thrown) {
                        __painelFinanceiro.showNotificacaoWarning(
                            "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                        );

                        __painelFinanceiro.removeOverlay(
                            $(
                                '#titulos-tab, ' +
                                '#financeiro-titulo-view-modal .modal-content, ' +
                                '#financeiro-titulo-pagamento-modal .modal-content, ' +
                                '#financeiro-titulo-renegociacao-modal .modal-content'
                            )
                        );
                    }
                });
            }
        };

        this.novoTitulo = {
            exibirModal: function () {
                $(
                    '#novoTituloTituloNumeroParcelas, #novoTituloTituloValorParcelas,' +
                    '#numeroParcelas, #valorParcelas, #novoTituloTituloDataVencimento'
                ).val('').trigger('change');
                $('#novoTituloPes, #novoTituloAlunocurso, #novoTituloTipotitulo,#novoTituloValores,#novoTituloPeriodoLetivo,#tituloConfig').select2('data', null).trigger('change');
                $('#novoTituloTituloDataVencimento').val(__painelFinanceiro.formatDate(new Date()));
                $('#valorFinalValoresStatic').html('0,00');
                $('#novoTituloLogin').val('');
                $('#novoTituloSenha').val('');
                $('#novoTituloTituloObservacoes').val('');
                $('#novoTituloValorFinal-static').html('0 x 0,00<hr class="no-margin>0,00');
                $('#financeiro-titulo-novo-modal').modal('show');
                $('.divPeriodoLetivo').addClass('hidden');

                $("#novoTituloValores").select2('disable')
            },
            validaObrigatoriedadePeriodoLetivo: function () {
                var arrAlunoCurso = $('#novoTituloAlunocurso').select2('data') || {},
                    cursoPossuiPeriodoLetivo = 0;

                $('.divPeriodoLetivo').addClass('hidden');

                if (arrAlunoCurso['curso_possui_periodo_letivo'] == "Sim") {
                    cursoPossuiPeriodoLetivo = 1;
                }

                if (cursoPossuiPeriodoLetivo) {
                    $('.divPeriodoLetivo').removeClass('hidden');
                    $('#novoTituloValores').select2('disable');
                } else {
                    $('.divPeriodoLetivo').addClass('hidden');
                    $('#novoTituloValores').select2('enable');
                }

                return cursoPossuiPeriodoLetivo;
            },
            buscaValoresTitulo: function () {
                $('#novoTituloValores').val('').select('data', null);
                $('#novoTituloValores').select2('disable');
                $('#novoTituloValores').trigger('change');

                var arrTituloTipo = $('#novoTituloTipotitulo').select2('data') || {},
                    arrAlunoCurso = $('#novoTituloAlunocurso').select2('data') || {},
                    arrPeridoLetivo = $('#novoTituloPeriodoLetivo').select2('data') || {};

                __painelFinanceiro.novoTitulo.validaObrigatoriedadePeriodoLetivo();

                var arrPesquisa = {
                    query: "%",
                    cursocampusId: arrAlunoCurso['cursocampus_id'] || arrAlunoCurso['cursocampusId'] || '',
                    titulotipoId: arrTituloTipo['tipotitulo_id'] || arrTituloTipo['tipotituloId'] || '',
                    periodoLetivo: arrPeridoLetivo['per_id'] || ''
                };

                $.ajax({
                    url: __painelFinanceiro.options.url.financeiroValores,
                    data: arrPesquisa,
                    dataType: 'json',
                    success: function (arrResult) {
                        arrResult = arrResult || [];

                        if (arrResult.erro) {
                            __painelFinanceiro.showNotificacaoWarning(arrResult.erro);
                            $('#novoTituloValores').select2('');
                            $('#novoTituloValores').select2('disable');
                        } else if (arrResult[0]) {

                            var arrRet = $.map(arrResult, function (el) {
                                el.text = el['valoresParcela'] + ' x ' + el['valoresPreco'];
                                el.id = el['valoresId'];
                                el.valoresParcelaOriginal = el['valoresParcela'];
                                el.valoresPrecoOriginal = el['valoresPreco'];

                                if (el.css = (el['valorAtual']) == 0) {
                                    el.css = 'text-danger';
                                }

                                return el;
                            });

                            __painelFinanceiro.options.data.valores = arrRet || [];

                            $('#novoTituloValores').select2('enable');
                            $('#novoTituloValores').select2('data', arrRet[0] || null);
                            $('#novoTituloValores').trigger('change');
                        }
                    }
                });
            },

            iniciarCamposInterface: function () {
                var $novoTituloPes = $('#novoTituloPes'),
                    $novoTituloAlunocurso = $('#novoTituloAlunocurso'),
                    $novoTituloPeriodoLetivo = $('#novoTituloPeriodoLetivo'),
                    $novoTituloTipotitulo = $('#novoTituloTipotitulo'),
                    $novoTituloValores = $('#novoTituloValores'),
                    $novoTituloTituloNumeroParcelas = $('#novoTituloTituloNumeroParcelas'),
                    $novoTituloTituloValorParcelas = $('#novoTituloTituloValorParcelas'),
                    $novoTituloOutroValor = $('#novoTituloOutroValor'),
                    $numeroParcelas = $('#numeroParcelas'),
                    $valorParcelas = $('#valorParcelas'),
                    $modalAlteracaoValores = $('#modal-alteracao-valores'),
                    $financeiroTituloNovoModal = $('#financeiro-titulo-novo-modal'),
                    $novoTituloTituloDataVencimento = $('#novoTituloTituloDataVencimento'),
                    $alteracaoValoresForm = $('#alteracao-valores-form'),
                    $financeiroTituloNovoForm = $('#financeiro-titulo-novo-form'),
                    $valorFinalValoresStatic = $('#valorFinalValoresStatic'),


                    $novoTituloValorFinalStatic = $('#novoTituloValorFinal-static'),
                    $novoTituloConfig = $('#tituloConfig');

                $novoTituloConfig.select2("disable");
                $novoTituloValores.select2("disable");

                $('.divPeriodoLetivo').addClass('hidden');

                $financeiroTituloNovoForm.validate({
                    submitHandler: function () {
                        var arrDados = $financeiroTituloNovoForm.serializeJSON();
                        arrDados['login'] = $('[name="novoTituloLogin"]').val() || '';
                        arrDados['senha'] = $('[name="novoTituloSenha"]').val() || '';

                        if (parseFloat(arrDados['tituloNumeroParcelas']) <= 0) {
                            __painelFinanceiro.showNotificacaoWarning('Selecione um número de parcelas válido!');
                            return false;
                        }

                        if (parseFloat(arrDados['tituloValorParcelas']) <= 0) {
                            __painelFinanceiro.showNotificacaoWarning('Selecione um valor de parcelas válido!');
                            return false;
                        }

                        __painelFinanceiro.addOverlay(
                            $financeiroTituloNovoModal.find('.modal-content'),
                            'Efetuando geração de títulos. Aguarde.'
                        );

                        arrDados['arrPeriodoLetivo'] = $('#novoTituloPeriodoLetivo').select2('data') || [];
                        arrDados['arrFinanceiroValor'] = $('#novoTituloValores').select2('data') || [];
                        var arrDadosComplementares = $('#novoTituloAlunocurso').select2('data') || [];

                        if (
                            !arrDadosComplementares['alunocurso_id'] &&
                            arrDados['arrFinanceiroValor']['grupoTitulo'] == 'Documentos'
                        ) {

                            __painelFinanceiro.showNotificacaoWarning('É necessário selecionar um aluno!');
                            __painelFinanceiro.removeOverlay($financeiroTituloNovoModal.find('.modal-content'));
                            return false;
                        }

                        var cursoRegular = (arrDados['arrPeriodoLetivo']['situacao_regular'] == 'Sim' || arrDadosComplementares['alunocurso_situacao'] == 'Deferido');

                        if (
                            arrDados['arrFinanceiroValor']['grupoTitulo'] == 'Academico' && !arrDadosComplementares['alunocurso_id'] && !cursoRegular
                        ) {
                            if (!arrDadosComplementares['alunper_id'] && arrDadosComplementares['curso_possui_periodo_letivo'] == 'Sim') {
                                __painelFinanceiro.showNotificacaoWarning("Título do grupo Acadêmico, é necessário que o aluno esteja em um periodo letivo e com situação 'Matriculado'!");
                                __painelFinanceiro.removeOverlay($financeiroTituloNovoModal.find('.modal-content'));
                                return false;
                            } else {
                                __painelFinanceiro.showNotificacaoWarning("Título do grupo Acadêmico, é necessário que o aluno esteja 'Matriculado'! Ou selecionar o curso do aluno !");
                                __painelFinanceiro.removeOverlay($financeiroTituloNovoModal.find('.modal-content'));
                                return false;
                            }
                        }
                        $.ajax({
                            url: __painelFinanceiro.options.url.financeiroTituloNovo,
                            type: 'POST',
                            dataType: 'json',
                            data: arrDados,
                            success: function (data) {
                                if (data.erro) {

                                    __painelFinanceiro.showNotificacaoWarning(
                                        data['mensagem'] || "Não foi possível gerar título(s)!"
                                    )
                                } else {
                                    __painelFinanceiro.showNotificacaoSuccess(
                                        data['mensagem'] || "Geração de título(s) efetuada!"
                                    );

                                    __painelFinanceiro.pessoa.forcaRecarregarInformacoes();

                                    $financeiroTituloNovoModal.modal('hide');
                                }

                                __painelFinanceiro.removeOverlay($financeiroTituloNovoModal.find('.modal-content'));
                            },
                            error: function (xhr, error, thrown) {
                                __painelFinanceiro.showNotificacaoWarning(
                                    "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                                );
                                __painelFinanceiro.removeOverlay($financeiroTituloNovoModal.find('.modal-content'));
                            }
                        });

                        return false;
                    },
                    ignore: '.ignore',
                    rules: {
                        pes: {required: true},
                        tipotitulo: {required: true},
                        valores: {required: true},
                        tituloDataVencimento: {required: true},
                        tituloConfig: {required: true},
                        periodoLetivo: {
                            required: function () {
                                return $financeiroTituloNovoForm.find('.divPeriodoLetivo').is(':visible');
                            }
                        },
                        novoTituloSenha: {
                            required: function () {
                                return $('#novoTituloSenha').is(':visible');
                            }
                        },
                        novoTituloLogin: {
                            required: function () {
                                return $('#novoTituloLogin').is(':visible');
                            }
                        }
                    },
                    messages: {
                        pes: {required: 'Campo obrigatório!'},
                        tipotitulo: {required: 'Campo obrigatório!'},
                        valores: {required: 'Campo obrigatório!'},
                        tituloDataVencimento: {required: 'Campo obrigatório!'},
                        periodoLetivo: {required: 'Curso com restrição de periocidade!'},
                        novoTituloSenha: {required: 'Campo obrigatório!'},
                        novoTituloLogin: {required: 'Campo obrigatório!'},
                        tituloConfig: {required: 'Campo obrigatório!'}
                    }
                });

                __painelFinanceiro.iniciarElementoDatePicker($novoTituloTituloDataVencimento);
                __painelFinanceiro.inciarElementoInteiro($numeroParcelas);
                __painelFinanceiro.inciarElementoMoeda($valorParcelas);

                $('[name="numeroParcelas"],[name="valorParcelas"]').on('change', function () {
                    var numeroParcelas = parseInt(__painelFinanceiro.formatarMoedaUS($numeroParcelas.val()) || '1');
                    var valorParcela = parseFloat(__painelFinanceiro.formatarMoedaUS($valorParcelas.val()) || '0');
                    var novoValor = numeroParcelas * valorParcela;

                    $valorFinalValoresStatic.html(__painelFinanceiro.formatarMoeda(novoValor));
                });

                $alteracaoValoresForm.validate({
                    submitHandler: function () {
                        var objValor = $novoTituloValores.select2('data') || [];
                        var numeroParcelas = parseInt($numeroParcelas.val() || '0');
                        var valorParcelas = __painelFinanceiro.formatarMoedaUS($valorParcelas.val());

                        if (numeroParcelas <= 0) {
                            __painelFinanceiro.showNotificacaoWarning('Parcela inválida!');
                            return false;
                        }

                        if (valorParcelas <= 0) {
                            __painelFinanceiro.showNotificacaoWarning('Valor inválido!');
                            return false;
                        }

                        objValor['valoresPreco'] = valorParcelas || 0;
                        objValor['valoresParcela'] = numeroParcelas || 1;
                        objValor['text'] = numeroParcelas + ' x ' + __painelFinanceiro.formatarMoeda(valorParcelas);

                        $novoTituloValores.select2('data', objValor).trigger('change');

                        $('[name="novoTituloLogin"],[name="novoTituloSenha"]').val('');

                        $modalAlteracaoValores.modal('hide');
                        return false;
                    },
                    ignore: '.ignore',
                    rules: {
                        numeroParcelas: {required: true},
                        valorParcelas: {required: true}
                    },
                    messages: {
                        numeroParcelas: {required: 'Campo obrigatório!'},
                        valorParcelas: {required: 'Campo obrigatório!'}
                    }
                });

                $novoTituloAlunocurso.select2({
                    allowClear: true,
                    language: 'pt-BR',
                    ajax: {
                        url: __painelFinanceiro.options.url.alunoPesquisa,

                        data: function (query) {
                            return {
                                query: query,
                                pesId: __painelFinanceiro.pessoa.pessoasSelecionadasRelacionadas()
                            };
                        },
                        dataType: 'json',
                        results: function (data) {
                            return {results: __painelFinanceiro.pessoa.formatarDadosAlunos(data)};
                        }
                    },
                    formatSelection: function (el) {
                        return el.pes_nome + (el['curso_nome'] ? ' | ' + el['curso_nome'] : '');
                    }
                }).change(function (e) {
                    var $overlay = $("#financeiro-titulo-novo-modal");
                    var arrAluno = $novoTituloAlunocurso.select2('data') || [];

                    __painelFinanceiro.addOverlay($overlay, 'carregando...');

                    if (arrAluno) {
                        arrAluno['id'] = arrAluno['pes_id'] || '';
                    }

                    $novoTituloPes.select2('data', arrAluno).trigger('change');
                    $novoTituloPeriodoLetivo.select2('data', '').trigger('change');
                    $novoTituloTipotitulo.select2('data', '').trigger('change');

                    __painelFinanceiro.novoTitulo.validaObrigatoriedadePeriodoLetivo();

                    __painelFinanceiro.removeOverlay($overlay);
                });

                $novoTituloPeriodoLetivo.select2({
                    allowClear: true,
                    language: 'pt-br',
                    ajax: {
                        url: __painelFinanceiro.options.url.periodoLetivo,
                        dataType: 'json',
                        method: 'POST',
                        data: function (query) {
                            var arrAluno = $novoTituloAlunocurso.select2('data') || [];
                            var alunocursoId = arrAluno['alunocursoId'] || arrAluno['alunocurso_id'] || '';

                            return {
                                query: query,
                                pesquisaPeriodo: true,
                                alunocursoId: alunocursoId
                            }
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.text = el.per_nome;
                                el.id = el.per_id;

                                return el;
                            });

                            return {results: transformed};
                        }

                    }

                }).change(function (e) {
                    if (e.added) {
                        __painelFinanceiro.novoTitulo.buscaValoresTitulo();
                    }
                });

                $novoTituloPes.select2({
                    language: 'pt-BR',
                    ajax: {
                        url: __painelFinanceiro.options.url.pessoa,
                        dataType: 'json',
                        method: 'POST',
                        data: function (query) {
                            var arrAluno = $novoTituloAlunocurso.select2('data') || [];
                            var arrDados = $('#pessoasPesquisa').select2('data') || [];
                            var dados = '';

                            if (arrDados['pes_id']) {
                                arrDados = [arrDados];
                            }

                            $.map(arrDados, function (dt) {
                                dados += dt.responsavel_financeiro_por;
                            });

                            var pesId = __painelFinanceiro.pessoa.pessoasSelecionadasRelacionadas();

                            return {
                                query: query,
                                pessoaOuResponsavel: pesId,
                                pesquisarPJ: __painelFinanceiro.verificarExibicaoPessoaJuridica(),
                                bloquearPesquisaPorPesId: __painelFinanceiro.verificarBloqueioPesquisaPorPesId()
                            };
                        },
                        results: function (data) {
                            return {results: __painelFinanceiro.pessoa.formatarDadosPessoasSomente(data)};
                        }
                    },
                    formatSelection: function (el) {
                        return el.pes_nome;
                    }
                }).change(function () {
                    var $overlay = $("#financeiro-titulo-novo-modal");
                    __painelFinanceiro.addOverlay($overlay, 'carregando...');

                    $novoTituloTipotitulo.val('').select('data', null);

                    $novoTituloTipotitulo.trigger('change');

                    __painelFinanceiro.removeOverlay($overlay);
                });

                $novoTituloTipotitulo.select2({
                    language: 'pt-BR',
                    ajax: {
                        url: __painelFinanceiro.options.url.financeiroTituloTipo,
                        dataType: 'json',
                        data: function (query) {
                            return {query: query, tituloConfig: true};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.text = el.tipotitulo_nome;
                                el.id = el.tipotitulo_id;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                }).change(function (e) {
                    if (e.added) {
                        __painelFinanceiro.novoTitulo.setarTituloConfigPorTituloTipo($(this).val());
                        __painelFinanceiro.novoTitulo.buscaValoresTitulo();
                    }
                });

                $novoTituloConfig.select2({
                    allowClear: true,
                    language: 'pt-BR',
                    ajax: {
                        url: __painelFinanceiro.options.url.financeiroTituloConfig,
                        dataType: 'json',
                        data: function (query) {
                            return {
                                query: query,
                                tipoTitulo: $novoTituloTipotitulo.val(),
                                perId: $novoTituloPeriodoLetivo.val(),
                                cursoCampus: function () {
                                    var arrAlunoCurso = $novoTituloAlunocurso.select2("data") || {};

                                    return arrAlunoCurso['cursocampus_id'] || null;
                                }
                            };
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.text = el.tituloconf_nome;
                                el.id = el.tituloconf_id;

                                return el;
                            });

                            return {results: transformed}
                        }
                    }
                }).change(function () {
                    __painelFinanceiro.novoTitulo.setarDadosEstaticos();
                });

                $novoTituloValores.select2({
                    allowClear: true,
                    language: 'pt-BR',
                    data: function () {
                        return {results: __painelFinanceiro.options.data.valores};
                    }
                }).change(function () {
                    var objValor = $novoTituloValores.select2('data') || [];
                    var parcelasOriginal = objValor['valoresParcelaOriginal'] || 1;
                    var valorOriginal = objValor['valoresPrecoOriginal'] || 0;
                    var totalOriginal = parcelasOriginal * valorOriginal;

                    var numeroParcelas = objValor['valoresParcela'] || 1;
                    var valorParcela = objValor['valoresPreco'] || '0';
                    var novoValor = numeroParcelas * valorParcela;

                    var percentualDesconto = ((totalOriginal - novoValor) * 100) / (totalOriginal);

                    var arrMotivo = [];

                    var extrapolouNumeroMaximoParcelas = (
                        numeroParcelas > __painelFinanceiro.options.value.financeiroMaxParcelas
                    );
                    var extrapolouDescontoMaximo = (
                        percentualDesconto > __painelFinanceiro.options.value.financeiroMaxPercentualDesconto
                    );

                    if (extrapolouNumeroMaximoParcelas) {
                        arrMotivo.push('Extrapolou o número máximo de parcelas;');
                    }

                    if (extrapolouDescontoMaximo) {
                        arrMotivo.push('Extrapolou o percentual máximo de desconto;');
                    }

                    var arrValores = $('#novoTituloValores').select2('data') || [];

                    if (arrValores['css']) {
                        arrMotivo.push('Título com valor legado!');
                    }

                    arrMotivo = arrMotivo.join(' ');

                    $financeiroTituloNovoForm.find('.motivoAutenticacao').html(arrMotivo || '-');

                    if (extrapolouNumeroMaximoParcelas || extrapolouDescontoMaximo || arrValores['css']) {
                        $('.autenticacao-novo').removeClass('hidden');
                    } else {
                        $('.autenticacao-novo').addClass('hidden');
                    }

                    $('[name="novoTituloLogin"],[name="novoTituloSenha"]').val('');

                    $valorFinalValoresStatic.html(__painelFinanceiro.formatarMoeda(novoValor));
                    $novoTituloTituloNumeroParcelas.val(numeroParcelas || '1');
                    $novoTituloTituloValorParcelas.val(valorParcela || '0');

                    if (this.value) {
                        $novoTituloOutroValor.removeClass('hidden');
                    } else {
                        $novoTituloOutroValor.addClass('hidden');
                    }

                    $novoTituloValorFinalStatic.html(
                        numeroParcelas + ' x ' + __painelFinanceiro.formatarMoeda(valorParcela) +
                        '<hr class="no-margin">' +
                        __painelFinanceiro.formatarMoeda(novoValor)
                    );
                });

                $novoTituloOutroValor.click(function (e) {
                    var arrTituloTipo = $novoTituloTipotitulo.select2('data') || [];

                    if (Object.keys(arrTituloTipo).length == 0) {
                        __painelFinanceiro.showNotificacaoInfo('Selecione o tipo de título!');
                    } else {
                        $numeroParcelas
                            .val($novoTituloTituloNumeroParcelas.val())
                            .trigger('change');
                        $valorParcelas
                            .val($novoTituloTituloValorParcelas.val())
                            .trigger('change');
                        $modalAlteracaoValores.modal('show');
                    }

                    e.preventDefault();
                    e.stopPropagation();
                });

                $novoTituloAlunocurso.trigger('change');
                $novoTituloPes.trigger('change');
                $novoTituloValores.trigger('change');
                $novoTituloTipotitulo.trigger('change');
                $novoTituloPeriodoLetivo.trigger('change');
                $novoTituloTituloNumeroParcelas.trigger('change');
                $novoTituloTituloValorParcelas.trigger('change');
            },

            setarDadosEstaticos: function () {
                var $overlay = $("#financeiro-titulo-novo-modal");
                var dados = $('#tituloConfig').select2('data');

                __painelFinanceiro.addOverlay($overlay, 'carregando...');

                if (dados) {
                    $('.form-control-static.' + 'desconto').html(
                        dados['tituloconf_valor_desc'] > 0 ? "R$ " + __painelFinanceiro.formatarMoeda(dados['tituloconf_valor_desc']) : '-');

                    $('.form-control-static.' + 'percDesc').html(
                        dados['tituloconf_percent_desc'] > 0 ? parseFloat(dados['tituloconf_percent_desc'] * 100) + ' %' : '-');

                    $('.form-control-static.' + 'descontoAteDia').html(
                        dados['tituloconf_dia_desc'] > 0 ? dados['tituloconf_dia_desc'] + '' : dados['tituloconf_dia_desc'] == 0 ? 'Até o dia do vencimento' : '-');

                    $('.form-control-static.' + 'desconto2').html(
                        dados['tituloconf_valor_desc2'] > 0 ? "R$ " + __painelFinanceiro.formatarMoeda(dados['tituloconf_valor_desc2']) : '-');

                    $('.form-control-static.' + 'percDesc2').html(
                        dados['tituloconf_percent_desc2'] > 0 ? parseFloat(
                            dados['tituloconf_percent_desc2'] * 100) + ' %' : '-');

                    $('.form-control-static.' + 'descontoAteDia2').html(
                        dados['tituloconf_dia_desc2'] > 0 ? dados['tituloconf_dia_desc2'] + '' : dados['tituloconf_dia_desc2'] == 0 ? 'Até o dia do vencimento' : '-');

                    $('.form-control-static.' + 'percMulta').html(
                        dados['tituloconf_multa'] > 0 ? (dados['tituloconf_multa'] * 100) + ' %' : '-');

                    $('.form-control-static.' + 'percJuros').html(
                        dados['tituloconf_juros'] > 0 ? (dados['tituloconf_juros'] * 100) + ' %' : '-');

                    $('.form-control-static.' + 'cobrarMultaJurosApartir').html(
                        dados['tituloconf_dia_juros'] > 0 ? dados['tituloconf_dia_juros'] + ' dias após o vencimento' : '-');

                } else {
                    $('.form-control-static.' + 'desconto').html('-');
                    $('.form-control-static.' + 'percDesc').html('-');
                    $('.form-control-static.' + 'descontoAteDia').html('-');
                    $('.form-control-static.' + 'desconto2').html('-');
                    $('.form-control-static.' + 'percDesc2').html('-');
                    $('.form-control-static.' + 'descontoAteDia2').html('-');
                    $('.form-control-static.' + 'percMulta').html('-');
                    $('.form-control-static.' + 'percJuros').html('-');
                    $('.form-control-static.' + 'cobrarMultaJurosApartir').html('-');

                }

                __painelFinanceiro.removeOverlay($overlay);
            },

            setarTituloConfigPorTituloTipo: function (tipoTitulo) {
                var arrDados = __painelFinanceiro.options.data.arrPessoas[0] || {};
                var $overlay = $("#financeiro-titulo-novo-modal");
                var dados = {};

                __painelFinanceiro.addOverlay($overlay, 'carregando...');

                dados['tipoTitulo'] = tipoTitulo;
                dados['cursoCampus'] = arrDados['cursocampusId'];
                dados['perId'] = arrDados['per_id'];

                $.post(__painelFinanceiro.options.url.financeiroTituloConfig, dados,
                    function (data) {
                        var config = data[0] || {};

                        if (config['tituloconf_id']) {
                            var obj = config;
                            var arrAluno = $("#novoTituloAlunocurso").select2('data');

                            obj.id = config['tituloconf_id'];
                            obj.text = config['tituloconf_nome'];

                            var cursocampusId = '';

                            if (arrAluno) {
                                cursocampusId = arrAluno['cursocampusId'];
                            } else {
                                var arrPessoa = __painelFinanceiro.options.data.arrPessoas[0] || {};
                                cursocampusId = arrPessoa['cursocampusId'] || '';
                            }

                            for (var row in data) {
                                if (data[row]['cursocampus_id'] == cursocampusId) {
                                    obj['id'] = data[row]['tituloconf_id'];
                                    obj['text'] = data[row]['tituloconf_nome'];
                                    break;
                                }
                            }

                            $('#tituloConfig').select2('data', obj).trigger('change');
                        } else {
                            __painelFinanceiro.showNotificacaoInfo('Configuração não encontrada!');
                        }

                        __painelFinanceiro.removeOverlay($overlay);
                    });
            }
        };

        this.cancelamento = {
            cancelarTitulos: function () {
                var titulosSelecionados = __painelFinanceiro.titulos.titulosSelecionadas() || [], arrTitulos = [];

                $.each(titulosSelecionados, function (tituloId, arrTitulo) {
                    arrTitulos.push(arrTitulo['titulo_id']);
                });

                __painelFinanceiro.confirmacao.exibir(
                    'cancelamento',
                    'Deseja prosseguir com cancelamento?',
                    arrTitulos.join(', ')
                );
            },
            executarCancelamento: function (arrTitulos, observacao) {
                __painelFinanceiro.addOverlay($('#titulos-tab'), 'Aguarde. Cancelando títulos!');
                $.ajax({
                    url: __painelFinanceiro.options.url.financeiroTituloCancelamento,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        titulos: arrTitulos,
                        observacao: observacao
                    },
                    success: function (data) {
                        if (data.erro) {
                            __painelFinanceiro.showNotificacaoWarning(
                                data['mensagem'] || "Não foi possível cancelar títulos!"
                            )
                        } else {
                            __painelFinanceiro.showNotificacaoSuccess(
                                data['mensagem'] || "Cancelamento de títulos efetuado com sucesso!"
                            );

                            __painelFinanceiro.pessoa.forcaRecarregarInformacoes();
                        }
                        __painelFinanceiro.removeOverlay($('#titulos-tab'));
                    },
                    error: function (xhr, error, thrown) {
                        __painelFinanceiro.showNotificacaoWarning(
                            "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                        );
                        __painelFinanceiro.removeOverlay($('#titulos-tab'));
                    }
                });
            }
        };

        this.observacao = {
            iniciarCamposInterface: function () {
                var $form = $('#financeiro-observacoes'),
                    $pessoa = $form.find('[name=pesId]'),
                    $btnEditarObservacao = $("#btnEditarObservacao");

                $pessoa.select2({
                    data: function () {
                        return {results: __painelFinanceiro.options.data.arrPessoas || []};
                    }
                }).on('change', __painelFinanceiro.observacao.atualizaEsetaObservacao);

                $btnEditarObservacao.on('click', function () {
                    __painelFinanceiro.observacao.habilitarEdicaoObservacao();
                });

                $form.on('submit', function (e) {
                    var dados = $(this).serializeJSON();
                    var arrPessoa = $pessoa.select2('data') || [];

                    dados['pesId'] = arrPessoa['pesId'] || '';
                    __painelFinanceiro.addOverlay($form, 'Aguarde. Salvando observação...');

                    $.ajax({
                        url: __painelFinanceiro.options.url.alterarObservacoes,
                        type: 'Post',
                        data: dados,
                        success: function (data) {
                            if (!data.erro) {
                                __painelFinanceiro.observacao.atualizaEsetaObservacao(dados['pesObservacao']);
                                __painelFinanceiro.showNotificacaoSuccess(data.mensagem || 'Observação salva!');
                            } else {
                                __painelFinanceiro.showNotificacaoDanger(data.mensagem || "Erro ao salvar observação!");
                            }

                            __painelFinanceiro.removeOverlay($form);
                        },
                        error: function (xhr, error, thrown) {
                            __painelFinanceiro.showNotificacaoWarning(
                                "Não foi possível efetuar a operação. Tente efetuar novamente a operação ou recarregue a página."
                            );
                            __painelFinanceiro.removeOverlay($form);
                        }
                    });

                    e.stopPropagation();
                    e.preventDefault();
                });
            },
            carregarObservacaoPessoaSelecionada: function () {
                var $pessoasPesquisa = $('#pessoasPesquisa');
                var $form = $('#financeiro-observacoes');
                var $pessoa = $form.find('[name=pesId]');
                var arrPessoas = $pessoasPesquisa.select2('data') || [];

                if (!arrPessoas['pes_id'] && Object.keys(arrPessoas).length > 0) {
                    arrPessoas = arrPessoas[0];
                }

                $pessoa.select2('data', arrPessoas || null).trigger('change');
            },
            habilitarEdicaoObservacao: function () {
                $('#financeiro-observacoes').find('.observacaoInfo').removeClass('hidden');
                $('#pesObservacao').prop('disabled', false);
                $('#btnSalvarObservacao').removeClass('hidden');
                $('#btnEditarObservacao').addClass('hidden');
            },
            desabilitarEdicaoObservacao: function () {
                $('#financeiro-observacoes').find('.observacaoInfo').addClass('hidden');
                $('#pesObservacao').prop('disabled', true);
                $('#btnSalvarObservacao').addClass('hidden');
                $('#btnEditarObservacao').removeClass('hidden');
            },
            atualizaEsetaObservacao: function (observacao) {
                var $form = $('#financeiro-observacoes');
                var $pessoa = $form.find('[name=pesId]');
                var arrPessoa = $pessoa.select2('data') || [];

                observacao = observacao || arrPessoa['pesObservacao'] || '';

                if (typeof observacao != 'string') {
                    observacao = arrPessoa['pesObservacao'] || '';
                }

                $('#pesObservacao').val(observacao);
                __painelFinanceiro.observacao.desabilitarEdicaoObservacao();
            }
        };

        this.confirmacao = {
            iniciarCamposInterface: function () {
                var $modal = $('#confirmar-acao-modal'),
                    form = $('#form-confirmar-acao'),
                    campoTitulos = $modal.find('[name=titulosAcao]'),
                    campoTipo = $modal.find('[name=tipoAcao]'),
                    campoObservacao = $modal.find('[name=observacaoAcao]');

                $modal.on('click', '.cancelarAcao', function () {
                    $modal.modal('hide');
                    return false;
                });

                $modal.on('click', '.confirmarAcao', function () {
                    var strTitulos = $modal.find('[name=tituloVinculadoId ]').val() || campoTitulos.val() || '';

                    if (!strTitulos) {
                        __painelFinanceiro.showNotificacaoInfo("Selecione títulos para efetuar a ação!");
                        return false;
                    }

                    if (!campoObservacao.val() || /^\s*$/.test(campoObservacao.val())) {
                        __painelFinanceiro.showNotificacaoWarning('Preencha o campo de observação!');
                        return false;
                    } else {
                        $modal.modal('hide');

                        if (campoTipo.val() == 'cancelamento') {
                            var observacao = 'Observação cancelamento: ' + campoObservacao.val();
                            __painelFinanceiro.cancelamento.executarCancelamento(strTitulos.split(','), observacao);
                        } else if (campoTipo.val() == 'estorno') {
                            observacao = 'Observação estorno de título: ' + campoObservacao.val();
                            __painelFinanceiro.vizualizacao.efetuarEstornoTitulo(strTitulos, observacao);
                        }
                    }
                });
            },
            exibir: function (acao, titulo, arrTitulos, mensagem) {
                acao = acao || 'canelamento';
                titulo = titulo || 'Deseja prosseguir com a ação?';
                arrTitulos = arrTitulos || '';
                mensagem = mensagem || 'Títulos selecionados:';

                var $modal = $('#confirmar-acao-modal'),
                    form = $('#form-confirmar-acao'),
                    campoTipo = $modal.find('[name=tipoAcao]'),
                    campoTitulos = $modal.find('[name=titulosAcao]'),
                    campoObservacao = $modal.find('[name=observacaoAcao]');

                campoObservacao.val('');
                campoTitulos.val(arrTitulos);
                campoTipo.val(acao);

                if (!arrTitulos) {
                    __painelFinanceiro.showNotificacaoInfo("Selecione títulos para efetuar a ação!");
                    return false;
                }

                $modal.find('.modal-title').html(titulo);
                $modal.find('.informacao-adicional').html('<b>' + mensagem + '</b><br>' + arrTitulos);
                $modal.modal('show');
            }
        };

        this.retornaSeDataMaiorQueAtual = function (data) {
            var dataAtual = __painelFinanceiro.formatDate(new Date(), 'yymmdd');
            data = __painelFinanceiro.formatDate(data, 'yymmdd');

            return parseInt(data) > parseInt(dataAtual);
        };

        this.run = function (opts) {
            this.setDefaults(opts);

            this.pessoa.iniciarCamposInterface();
            this.alunoInformacoes.iniciarCamposInterface();
            this.titulos.iniciarCamposInterface();
            this.renegociacao.iniciarCamposInterface();
            this.pagamentoAdd.iniciarCamposInterface();
            this.novoTitulo.iniciarCamposInterface();
            this.envio.iniciarCamposInterface();
            this.pagamento.iniciarCamposInterface();
            this.observacao.iniciarCamposInterface();
            this.confirmacao.iniciarCamposInterface();

            this.pessoa.recarregarInformacoes();
            this.titulos.iniciarDataTableTitulo();
            this.renegociacao.iniciarDataTableRenegociacaoTitulo();
            this.pagamentoAdd.setarValidacoes();
            this.pagamento.iniciarDataTableMeioPagamento();
            this.pagamento.iniciarDataTablePagamentoTitulo();
            this.vizualizacao.iniciarCamposView();
            this.vizualizacao.iniciarCamposInterface();

        };
    };

    $.painelFinanceiro = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.painel");

        if (!obj) {
            obj = new PainelFinanceiro();
            obj.run(params);
            $(window).data('universa.financeiro.painel', obj);
        }

        return obj;
    };
})(window.jQuery, window);