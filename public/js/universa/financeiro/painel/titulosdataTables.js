(function ($, window, document) {
    'use strict';

    var TitulosDataTables = function () {
        VersaShared.call(this);

        var __TitulosDataTables = this;

        this.defaults = {
            url: {
                financeiroTitulo: '',
                pagseguroCobranca: '',
                boletoExibe: ''
            },
            data: {
                arrDados: [],
                arrTitulos: []
            },
            value: {
                pagSeguroAtivo: '',
                boletoRemessaAtivada: '',
                boletoAtivo: ''
            },
            dataTables: {
                arrDataTable: ''
            }
        };

        this.verificaPagSeguroAtivo = function () {
            return (__TitulosDataTables.options.value.pagSeguroAtivo || 0);
        };


        this.verificaBoletoAtivo = function () {
            return (__TitulosDataTables.options.value.boletoAtivo || 0);
        };

        this.verificaRemessaAtivada = function () {
            return (__TitulosDataTables.options.value.boletoRemessaAtivada || 0);
        };

        this.iniciarTitulosDataTables = {
            iniciarDataTables: function () {
                var arrDados = __TitulosDataTables.options.data.arrDados,
                    $dataTableFinanceiroTitulo = $('#dataTableFinanceiroTitulos'),
                    colNum = -1,
                    pagSeguroAtivo = __TitulosDataTables.verificaPagSeguroAtivo(),
                    boletoAtivo = __TitulosDataTables.verificaBoletoAtivo(),
                    remessaAtivada = __TitulosDataTables.verificaRemessaAtivada();

                $('.form-control-static.' + 'pesNome').html(arrDados['pesNome']);

                $dataTableFinanceiroTitulo.dataTable({
                    processing: true,
                    serverSide: false,
                    ordering: false,
                    searchDelay: 350,
                    data: function () {
                        var json = __TitulosDataTables.options.data.arrTitulos || [];
                        var data = json['data'] || [];
                        var btnBoleto = {
                            size: 'sm', element: 'a', target: '_blank',
                            class: 'btn-primary btn-boleto', icon: 'fa-download',
                            title: 'Download de boleto'
                        };

                        var btnPagseguro = {
                            size: 'sm', element: 'a', target: '_blank',
                            class: 'btn-primary btn-pagseguro', icon: 'fa-credit-card',
                            title: 'Link pagseguro'
                        };

                        $.each(data, function (row, titulo) {
                            var dataVencimento = __TitulosDataTables.formatDate(titulo['titulo_data_vencimento']),
                                dataPagamento = __TitulosDataTables.formatDate(titulo['titulo_data_pagamento']) || __TitulosDataTables.formatDate(titulo['titulo_data_baixa']),
                                btns = [];

                            titulo['acao'] = ' - ';
                            var boletoId=(titulo['bol_id']||0);

                            btnPagseguro['href'] = __TitulosDataTables.options.url.pagseguroCobranca + '/' + titulo['titulo_id'];

                            titulo['titulo_valor_formatado'] =
                                __TitulosDataTables.formatarMoeda(titulo['titulo_valor'])
                            ;


                            titulo['curso_sigla'] = (
                                titulo['curso_sigla'] ||
                                (titulo['curso_nome'] || '').substr(0, 3).toUpperCase()
                            );

                            if (titulo['titulo_estado'] != 'Pago' && !titulo['titulo_data_pagamento'] && !titulo['titulo_data_baixa']) {
                                if (boletoId && boletoAtivo) {
                                    if (!remessaAtivada || (remessaAtivada && parseInt(titulo['esta_na_remessa']) > 0)) {
                                        btnBoleto['href'] = __TitulosDataTables.options.url.boletoExibe + '/' + boletoId;
                                        btns.push(btnBoleto);
                                    }
                                }

                                if (pagSeguroAtivo) {
                                    btns.push(btnPagseguro);
                                }

                                if (btns.length > 0) {
                                    titulo['acao'] = __TitulosDataTables.createBtnGroup(btns);
                                }
                            }

                            titulo['titulo_data_vencimento_formatado'] = dataVencimento;
                            titulo['titulo_data_pagamento_formatado'] = dataPagamento;

                            data[row] = titulo;
                        });

                        return data;
                    }(),
                    columnDefs: [
                        {name: "titulo_id", targets: ++colNum, data: "titulo_id"},
                        {name: "responsavel", targets: ++colNum, data: "pes_nome", searchable: false},
                        {name: "aluno_nome", targets: ++colNum, data: "aluno_nome", searchable: false},
                        {name: "titulo_descricao", targets: ++colNum, data: "titulo_descricao"},
                        {name: "curso_nome", targets: ++colNum, data: "curso_nome", searchable: false},
                        {
                            name: "titulo_data_vencimento",
                            targets: ++colNum,
                            data: "titulo_data_vencimento_formatado",
                            searchable: false
                        },
                        {
                            name: "titulo_data_pagamento", targets: ++colNum,
                            data: "titulo_data_pagamento_formatado",
                            searchable: false
                        },
                        {
                            name: "titulo_valor",
                            targets: ++colNum,
                            data: "titulo_valor_calc_formatado",
                            searchable: false
                        },
                        {name: "titulo_estado", targets: ++colNum, data: "titulo_estado", searchable: false},
                        {name: "titulo_id", targets: ++colNum, data: "acao", orderable: false, searchable: false}
                    ],
                    createdRow: function (row, data, index) {
                        if (parseInt(data['dias_atraso'])) {
                            $(row).addClass('text-danger');
                        }
                    },
                    "dom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                });
            }
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.loadSteps();
            this.setValidations();

            this.iniciarTitulosDataTables.iniciarDataTables();
        };
    };

    $.titulosDataTables = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.titulos-acesso-externo");

        if (!obj) {
            obj = new TitulosDataTables();
            obj.run(params);
            $(window).data('universa.financeiro.titulos-acesso-externo', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);