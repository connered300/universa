(function ($, window, document) {
    'use strict';
    var FinanceiroFiltroIndex = function () {
        VersaShared.call(this);
        var __financeiroFiltroIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                financeiroFiltro: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __financeiroFiltroIndex.options.datatables.financeiroFiltro = $('#dataTableFinanceiroFiltro').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __financeiroFiltroIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary financeiroFiltro-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger financeiroFiltro-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __financeiroFiltroIndex.removeOverlay($('#container-financeiro-filtro'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "filtro_id", targets: colNum++, data: "filtro_id"},
                    {name: "filtro_nome", targets: colNum++, data: "filtro_nome"},
                    {name: "filtro_situacao", targets: colNum++, data: "filtro_situacao"},
                    {name: "filtro_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableFinanceiroFiltro').on('click', '.financeiroFiltro-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroFiltroIndex.options.datatables.financeiroFiltro.fnGetData($pai);

                if (!__financeiroFiltroIndex.options.ajax) {
                    location.href = __financeiroFiltroIndex.options.url.edit + '/' + data['filtro_id'];
                } else {
                    __financeiroFiltroIndex.addOverlay(
                        $('#container-financeiro-filtro'), 'Aguarde, carregando dados para edição...'
                    );
                    $.financeiroFiltroAdd().pesquisaFinanceiroFiltro(
                        data['filtro_id'],
                        function (data) { __financeiroFiltroIndex.showModal(data); }
                    );
                }
            });


            $('#btn-financeiro-filtro-add').click(function (e) {
                if (__financeiroFiltroIndex.options.ajax) {
                    __financeiroFiltroIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableFinanceiroFiltro').on('click', '.financeiroFiltro-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __financeiroFiltroIndex.options.datatables.financeiroFiltro.fnGetData($pai);
                var arrRemover = {
                    filtroId: data['filtro_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de filtro "' + data['filtro_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __financeiroFiltroIndex.addOverlay(
                                $('#container-financeiro-filtro'),
                                'Aguarde, solicitando remoção de registro de filtro...'
                            );
                            $.ajax({
                                url: __financeiroFiltroIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __financeiroFiltroIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de filtro:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __financeiroFiltroIndex.removeOverlay($('#container-financeiro-filtro'));
                                    } else {
                                        __financeiroFiltroIndex.reloadDataTableFinanceiroFiltro();
                                        __financeiroFiltroIndex.showNotificacaoSuccess(
                                            "Registro de filtro removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-filtro-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['filtroId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['filtroId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $form[0].reset();
            $form.deserialize(data);

            $("#filtroEstado").select2('val', data['filtroEstado']).trigger("change");
            $("#filtroSituacao").select2('val', data['filtroSituacao']).trigger("change");
            $("#tipoTitulo").select2('data', data['tipoTitulo'] || []).trigger("change");

            $('#financeiro-filtro-modal .modal-title').html(actionTitle + ' filtro');
            $('#financeiro-filtro-modal').modal();
            __financeiroFiltroIndex.removeOverlay($('#container-financeiro-filtro'));
        };

        this.reloadDataTableFinanceiroFiltro = function () {
            this.getDataTableFinanceiroFiltro().api().ajax.reload(null, false);
        };

        this.getDataTableFinanceiroFiltro = function () {
            if (!__financeiroFiltroIndex.options.datatables.financeiroFiltro) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroFiltro')) {
                    __financeiroFiltroIndex.options.datatables.financeiroFiltro =
                        $('#dataTableFinanceiroFiltro').DataTable();
                } else {
                    __financeiroFiltroIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroFiltroIndex.options.datatables.financeiroFiltro;
        };

        this.run = function (opts) {
            __financeiroFiltroIndex.setDefaults(opts);
            __financeiroFiltroIndex.setSteps();
        };
    };

    $.financeiroFiltroIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-filtro.index");

        if (!obj) {
            obj = new FinanceiroFiltroIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-filtro.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);