(function ($, window, document) {
    'use strict';

    var FinanceiroFiltroAdd = function () {
        VersaShared.call(this);
        var __financeiroFiltroAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                financeiroTituloTipo: ''
            },
            data: {
                filtroEstado: null,
                filtroSituacao: null
            },
            value: {
                tipoTitulo: null,
                filtroEstado: null,
                filtroSituacao: null
            },
            datatables: {},
            wizardElement: '#financeiro-filtro-wizard',
            formElement: '#financeiro-filtro-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $tipoTitulo     = $('#tipoTitulo'),
                $filtroEstado   = $('#filtroEstado'),
                $filtroSituacao = $('#filtroSituacao');

            $filtroEstado.select2({
                allowClear: true,
                data: __financeiroFiltroAdd.options.data.filtroEstado,
                minimumInputLength: -1,
                multiple: true,
                language: 'pt-BR'
            }).trigger("change");
            $filtroSituacao.select2({
                allowClear: true,
                minimumInputLength: -1,
                data: __financeiroFiltroAdd.options.data.filtroSituacao,
                language: 'pt-BR'
            }).trigger("change");
            $tipoTitulo.select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __financeiroFiltroAdd.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.tipotitulo_id;
                            el.text = el.tipotitulo_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $tipoTitulo.select2("data", __financeiroFiltroAdd.getTipoTitulo());
            $filtroEstado.select2("data", __financeiroFiltroAdd.getFiltroEstado());
            $filtroSituacao.select2("data", __financeiroFiltroAdd.getFiltroSituacao());
        };

        this.setValidations = function () {
            __financeiroFiltroAdd.options.validator.settings.rules =
            {
                filtroNome: {maxlength: 60, required: true},
                filtroSituacao: {required: true}
            };
            __financeiroFiltroAdd.options.validator.settings.messages = {
                filtroNome: {maxlength: 'Tamanho máximo: 60!', required: 'Campo Obrigatório!'},
                filtroSituacao: {required: 'Campo Obrigatório!'}
            };

            $(__financeiroFiltroAdd.options.formElement).submit(function (e) {
                var ok = $(__financeiroFiltroAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__financeiroFiltroAdd.options.formElement);

                if (__financeiroFiltroAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __financeiroFiltroAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __financeiroFiltroAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__financeiroFiltroAdd.options.listagem) {
                                    $.financeiroFiltroIndex().reloadDataTableFinanceiroFiltro();
                                    __financeiroFiltroAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#financeiro-filtro-modal').modal('hide');
                                }

                                __financeiroFiltroAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaFinanceiroFiltro = function (filtro_id, callback) {
            var $form = $(__financeiroFiltroAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __financeiroFiltroAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + filtro_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroFiltroAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __financeiroFiltroAdd.removeOverlay($form);
                },
                erro: function () {
                    __financeiroFiltroAdd.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroFiltroAdd.removeOverlay($form);
                }
            });
        };

        this.setTipoTitulo = function (tipoTitulo) {
            this.options.value.tipoTitulo = tipoTitulo || null;
        };

        this.getTipoTitulo = function () {
            return this.options.value.tipoTitulo || null;
        };

        this.setFiltroEstado = function (filtroEstado) {
            this.options.value.filtroEstado = filtroEstado || null;
        };

        this.getFiltroEstado = function () {
            return this.options.value.filtroEstado || null;
        };

        this.setFiltroSituacao = function (filtroSituacao) {
            this.options.value.filtroSituacao = filtroSituacao || null;
        };

        this.getFiltroSituacao = function () {
            return this.options.value.filtroSituacao || null;
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.financeiroFiltroAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-filtro.add");

        if (!obj) {
            obj = new FinanceiroFiltroAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-filtro.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);