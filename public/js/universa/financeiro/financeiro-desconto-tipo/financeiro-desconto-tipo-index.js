(function ($, window, document) {
    'use strict';
    var FinanceiroDescontoTipoIndex = function () {
        VersaShared.call(this);
        var __financeiroDescontoTipoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                financeiroDescontoTipo: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __financeiroDescontoTipoIndex.options.datatables.financeiroDescontoTipo =
                $('#dataTableFinanceiroDescontoTipo').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __financeiroDescontoTipoIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary financeiroDescontoTipo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger financeiroDescontoTipo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __financeiroDescontoTipoIndex.removeOverlay($('#container-financeiro-desconto-tipo'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "desctipo_id", targets: colNum++, data: "desctipo_id"},
                        {name: "desctipo_descricao", targets: colNum++, data: "desctipo_descricao"},
                        {name: "desctipo_percmax", targets: colNum++, data: "desctipo_percmax"},
                        {name: "desctipo_valormax", targets: colNum++, data: "desctipo_valormax"},
                        {name: "desctipo_modalidade", targets: colNum++, data: "desctipo_modalidade"},
                        {name: "desctipo_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableFinanceiroDescontoTipo').on('click', '.financeiroDescontoTipo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __financeiroDescontoTipoIndex.options.datatables.financeiroDescontoTipo.fnGetData($pai);

                if (!__financeiroDescontoTipoIndex.options.ajax) {
                    location.href = __financeiroDescontoTipoIndex.options.url.edit + '/' + data['desctipo_id'];
                } else {
                    __financeiroDescontoTipoIndex.addOverlay(
                        $('#container-financeiro-desconto-tipo'), 'Aguarde, carregando dados para edição...'
                    );
                    $.financeiroDescontoTipoAdd().pesquisaFinanceiroDescontoTipo(
                        data['desctipo_id'],
                        function (data) {
                            __financeiroDescontoTipoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-financeiro-desconto-tipo-add').click(function (e) {
                if (__financeiroDescontoTipoIndex.options.ajax) {
                    __financeiroDescontoTipoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableFinanceiroDescontoTipo').on('click', '.financeiroDescontoTipo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __financeiroDescontoTipoIndex.options.datatables.financeiroDescontoTipo.fnGetData($pai);
                var arrRemover = {
                    desctipoId: data['desctipo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de desconto tipo "' + data['desctipo_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __financeiroDescontoTipoIndex.addOverlay(
                                $('#container-financeiro-desconto-tipo'),
                                'Aguarde, solicitando remoção de registro de desconto tipo...'
                            );
                            $.ajax({
                                url: __financeiroDescontoTipoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __financeiroDescontoTipoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de desconto tipo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __financeiroDescontoTipoIndex.removeOverlay($('#container-financeiro-desconto-tipo'));
                                    } else {
                                        __financeiroDescontoTipoIndex.reloadDataTableFinanceiroDescontoTipo();
                                        __financeiroDescontoTipoIndex.showNotificacaoSuccess(
                                            "Registro de desconto tipo removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-desconto-tipo-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['desctipoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['desctipoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#desctipoDescontoAcumulativo").select2('val', data['desctipoDescontoAcumulativo']).trigger('change');


            $("#desctipoPercmax").val(data['desctipoPercmax'] || '0').trigger('change');
            $("#desctipoValormax").val(data['desctipoValormax'] || '0').trigger('change');

            $("#desctipoModalidade").select2('val', data['desctipoModalidade']).trigger("change");
            $("#desctipoLimitaVencimento").select2('val', data['desctipoLimitaVencimento']).trigger("change");
            $("#permissao").select2('data', data['permissao']).trigger("change");

            $('#financeiro-desconto-tipo-modal .modal-title').html(actionTitle + ' desconto tipo');
            $('#financeiro-desconto-tipo-modal').modal();
            __financeiroDescontoTipoIndex.removeOverlay($('#container-financeiro-desconto-tipo'));
        };

        this.reloadDataTableFinanceiroDescontoTipo = function () {
            this.getDataTableFinanceiroDescontoTipo().api().ajax.reload(null, false);
        };

        this.getDataTableFinanceiroDescontoTipo = function () {
            if (!__financeiroDescontoTipoIndex.options.datatables.financeiroDescontoTipo) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroDescontoTipo')) {
                    __financeiroDescontoTipoIndex.options.datatables.financeiroDescontoTipo =
                        $('#dataTableFinanceiroDescontoTipo').DataTable();
                } else {
                    __financeiroDescontoTipoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroDescontoTipoIndex.options.datatables.financeiroDescontoTipo;
        };

        this.run = function (opts) {
            __financeiroDescontoTipoIndex.setDefaults(opts);
            __financeiroDescontoTipoIndex.setSteps();
        };
    };

    $.financeiroDescontoTipoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-desconto-tipo.index");

        if (!obj) {
            obj = new FinanceiroDescontoTipoIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-desconto-tipo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);