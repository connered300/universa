(function ($, window, document) {
    'use strict';

    var FinanceiroDescontoTipoAdd = function () {
        VersaShared.call(this);
        var __financeiroDescontoTipoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                financeiroTituloTipo: '',
            },
            data: {},
            value: {
                permissao: null,
                desctipoDescontoAcumulativo: null
            },
            datatables: {},
            wizardElement: '#financeiro-desconto-tipo-wizard',
            formElement: '#financeiro-desconto-tipo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#desctipoPercmax").inputmask({showMaskOnHover: false, alias: 'number'});
            $("#desctipoValormax").inputmask({
                showMaskOnHover: false, alias: 'currency', prefix: '', groupSeparator: ''
            });

            $("#desctipoPercmax, #desctipoValormax").change(function () {
                var desctipoPercmax = parseFloat($('#desctipoPercmax').val() || '0');
                var desctipoValormax = parseFloat($('#desctipoValormax').val() || '0');

                $("#desctipoPercmax, #desctipoValormax").prop('disabled', true);

                if (desctipoPercmax != 0) {
                    $("#desctipoPercmax").prop('disabled', false);
                    $("#desctipoValormax").val('');
                    desctipoValormax = 0;
                }

                if (desctipoValormax != 0) {
                    $("#desctipoValormax").prop('disabled', false);
                    $("#desctipoPercmax").val('');
                    desctipoPercmax = 0;
                }

                if (desctipoPercmax == 0 && desctipoValormax == 0) {
                    $("#desctipoPercmax, #desctipoValormax").prop('disabled', false);
                }
            }).trigger('change');

            $("#desctipoDescontoAcumulativo").select2({
                data: __financeiroDescontoTipoAdd.options.value.desctipoDescontoAcumulativo
            });

            $("#permissao").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: __financeiroDescontoTipoAdd.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.tipotitulo_id;
                            el.text = el.tipotitulo_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroDescontoTipoAdd.getPermissao()) {
                $('#permissao').select2("data", __financeiroDescontoTipoAdd.getPermissao());
            }

            $("#desctipoModalidade").select2({language: 'pt-BR'}).trigger("change");
            $("#desctipoLimitaVencimento").select2({language: 'pt-BR'}).trigger("change");
        };

        this.setPermissao = function (permissao) {
            this.options.value.permissao = permissao || null;
        };

        this.getPermissao = function () {
            return this.options.value.permissao || null;
        };

        this.setValidations = function () {
            __financeiroDescontoTipoAdd.options.validator.settings.rules = {
                desctipoDescricao: {maxlength: 45, required: true},
                desctipoPercmax: {maxlength: 6, number: true, max: 100},
                desctipoValormax: {maxlength: 10, number: true},
                desctipoModalidade: {maxlength: 13},
                desctipoLimitaVencimento: {maxlength: 3, required: true},
                desctipoDescontoAcumulativo: {maxlength: 3, required: true},

            };
            __financeiroDescontoTipoAdd.options.validator.settings.messages = {
                desctipoDescricao: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                desctipoPercmax: {maxlength: 'Tamanho máximo: 6!', number: 'Número inválido!'},
                desctipoValormax: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                desctipoModalidade: {maxlength: 'Tamanho máximo: 13!'},
                desctipoLimitaVencimento: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
                desctipoDescontoAcumulativo: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
            };

            $(__financeiroDescontoTipoAdd.options.formElement).submit(function (e) {
                var ok = $(__financeiroDescontoTipoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__financeiroDescontoTipoAdd.options.formElement);

                if (__financeiroDescontoTipoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __financeiroDescontoTipoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __financeiroDescontoTipoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__financeiroDescontoTipoAdd.options.listagem) {
                                    $.financeiroDescontoTipoIndex().reloadDataTableFinanceiroDescontoTipo();
                                    __financeiroDescontoTipoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#financeiro-desconto-tipo-modal').modal('hide');
                                }

                                __financeiroDescontoTipoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaFinanceiroDescontoTipo = function (desctipo_id, callback) {
            var $form = $(__financeiroDescontoTipoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __financeiroDescontoTipoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + desctipo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroDescontoTipoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __financeiroDescontoTipoAdd.removeOverlay($form);
                },
                erro: function () {
                    __financeiroDescontoTipoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroDescontoTipoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.financeiroDescontoTipoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-desconto-tipo.add");

        if (!obj) {
            obj = new FinanceiroDescontoTipoAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-desconto-tipo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);