(function ($, window, document) {
    'use strict';

    var BoletoBancoAdd = function () {
        VersaShared.call(this);
        var __boletoBancoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {boletoLayout: ''},
            data: {},
            value: {layout: null},
            datatables: {},
            wizardElement: '#boleto-banco-wizard',
            formElement: '#boleto-banco-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#bancCodigo").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
            $("#layout").select2({
                language: 'pt-BR',
                ajax: {
                    url: __boletoBancoAdd.options.url.boletoLayout,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.layout_id, id: el.layout_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__boletoBancoAdd.getLayout()) {
                $('#layout').select2("data", __boletoBancoAdd.getLayout());
            }
        };

        this.setLayout = function (layout) {
            this.options.value.layout = layout || null;
        };

        this.getLayout = function () {
            return this.options.value.layout || null;
        };
        this.setValidations = function () {
            __boletoBancoAdd.options.validator.settings.rules = {
                bancCodigo: {maxlength: 11, number: true, required: true}, bancNome: {maxlength: 128, required: true},
                layout: {maxlength: 11, number: true},
            };
            __boletoBancoAdd.options.validator.settings.messages = {
                bancCodigo: {
                    maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                }, bancNome: {maxlength: 'Tamanho máximo: 128!', required: 'Campo obrigatório!'},
                layout: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
            };

            $(__boletoBancoAdd.options.formElement).submit(function (e) {
                var ok = $(__boletoBancoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__boletoBancoAdd.options.formElement);

                if (__boletoBancoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __boletoBancoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __boletoBancoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__boletoBancoAdd.options.listagem) {
                                    $.boletoBancoIndex().reloadDataTableBoletoBanco();
                                    __boletoBancoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#boleto-banco-modal').modal('hide');
                                }

                                __boletoBancoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaBoletoBanco = function (banc_id, callback) {
            var $form = $(__boletoBancoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __boletoBancoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + banc_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __boletoBancoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __boletoBancoAdd.removeOverlay($form);
                },
                erro: function () {
                    __boletoBancoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __boletoBancoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.boletoBancoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.boleto-banco.add");

        if (!obj) {
            obj = new BoletoBancoAdd();
            obj.run(params);
            $(window).data('universa.financeiro.boleto-banco.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);