(function ($, window, document) {
    'use strict';
    var BoletoBancoIndex = function () {
        VersaShared.call(this);
        var __boletoBancoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                boletoBanco: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __boletoBancoIndex.options.datatables.boletoBanco = $('#dataTableBoletoBanco').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __boletoBancoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary boletoBanco-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger boletoBanco-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __boletoBancoIndex.removeOverlay($('#container-boleto-banco'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "banc_id", targets: colNum++, data: "banc_id"},
                    {name: "banc_codigo", targets: colNum++, data: "banc_codigo"},
                    {name: "banc_nome", targets: colNum++, data: "banc_nome"},
                    {name: "layout_id", targets: colNum++, data: "layout_id"},
                    {name: "banc_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableBoletoBanco').on('click', '.boletoBanco-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __boletoBancoIndex.options.datatables.boletoBanco.fnGetData($pai);

                if (!__boletoBancoIndex.options.ajax) {
                    location.href = __boletoBancoIndex.options.url.edit + '/' + data['banc_id'];
                } else {
                    $.boletoBancoAdd().pesquisaBoletoBanco(
                        data['banc_id'],
                        function (data) { __boletoBancoIndex.showModal(data); }
                    );
                }
            });


            $('#btn-boleto-banco-add').click(function (e) {
                if (__boletoBancoIndex.options.ajax) {
                    __boletoBancoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableBoletoBanco').on('click', '.boletoBanco-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __boletoBancoIndex.options.datatables.boletoBanco.fnGetData($pai);
                var arrRemover = {
                    bancId: data['banc_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de banco "' + data['banc_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __boletoBancoIndex.addOverlay(
                                $('#container-boleto-banco'), 'Aguarde, solicitando remoção de registro de banco...'
                            );
                            $.ajax({
                                url: __boletoBancoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __boletoBancoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de banco:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __boletoBancoIndex.removeOverlay($('#container-boleto-banco'));
                                    } else {
                                        __boletoBancoIndex.reloadDataTableBoletoBanco();
                                        __boletoBancoIndex.showNotificacaoSuccess(
                                            "Registro de banco removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#boleto-banco-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['bancId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['bancId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#layout").select2('data', data['layout']).trigger("change");

            $('#boleto-banco-modal .modal-title').html(actionTitle + ' banco');
            $('#boleto-banco-modal').modal();
        };

        this.reloadDataTableBoletoBanco = function () {
            this.getDataTableBoletoBanco().api().ajax.reload(null, false);
        };

        this.getDataTableBoletoBanco = function () {
            if (!__boletoBancoIndex.options.datatables.boletoBanco) {
                if (!$.fn.dataTable.isDataTable('#dataTableBoletoBanco')) {
                    __boletoBancoIndex.options.datatables.boletoBanco = $('#dataTableBoletoBanco').DataTable();
                } else {
                    __boletoBancoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __boletoBancoIndex.options.datatables.boletoBanco;
        };

        this.run = function (opts) {
            __boletoBancoIndex.setDefaults(opts);
            __boletoBancoIndex.setSteps();
        };
    };

    $.boletoBancoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.boleto-banco.index");

        if (!obj) {
            obj = new BoletoBancoIndex();
            obj.run(params);
            $(window).data('universa.financeiro.boleto-banco.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);