(function ($, window, document) {
    'use strict';

    var FinanceiroDescontoTituloAdd = function () {
        VersaShared.call(this);
        var __financeiroDescontoTituloAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                financeiroDesconto: '',
                financeiroTitulo: '',
                acessoPessoas: ''
            },
            data: {},
            value: {
                desconto: null,
                titulo: null,
                usuarioAlteracao: null,
                usuarioCriacao: null
            },
            datatables: {},
            wizardElement: '#financeiro-desconto-titulo-wizard',
            formElement: '#financeiro-desconto-titulo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#desconto").select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroDescontoTituloAdd.options.url.financeiroDesconto,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.desconto_id, id: el.desconto_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroDescontoTituloAdd.getDesconto()) {
                $('#desconto').select2("data", __financeiroDescontoTituloAdd.getDesconto());
            }
            $("#titulo").select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroDescontoTituloAdd.options.url.financeiroTitulo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.titulo_id, id: el.titulo_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroDescontoTituloAdd.getTitulo()) {
                $('#titulo').select2("data", __financeiroDescontoTituloAdd.getTitulo());
            }
            $("#usuarioAlteracao").select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroDescontoTituloAdd.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.id, id: el.id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroDescontoTituloAdd.getUsuarioAlteracao()) {
                $('#usuarioAlteracao').select2("data", __financeiroDescontoTituloAdd.getUsuarioAlteracao());
            }
            $("#usuarioCriacao").select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroDescontoTituloAdd.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.id, id: el.id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroDescontoTituloAdd.getUsuarioCriacao()) {
                $('#usuarioCriacao').select2("data", __financeiroDescontoTituloAdd.getUsuarioCriacao());
            }
            $("#descontoTituloCriacao")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#descontoTituloAlteracao")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#descontoTituloSituacao").select2({language: 'pt-BR'}).trigger("change");
        };

        this.setDesconto = function (desconto) {
            this.options.value.desconto = desconto || null;
        };

        this.getDesconto = function () {
            return this.options.value.desconto || null;
        };

        this.setTitulo = function (titulo) {
            this.options.value.titulo = titulo || null;
        };

        this.getTitulo = function () {
            return this.options.value.titulo || null;
        };

        this.setUsuarioAlteracao = function (usuarioAlteracao) {
            this.options.value.usuarioAlteracao = usuarioAlteracao || null;
        };

        this.getUsuarioAlteracao = function () {
            return this.options.value.usuarioAlteracao || null;
        };

        this.setUsuarioCriacao = function (usuarioCriacao) {
            this.options.value.usuarioCriacao = usuarioCriacao || null;
        };

        this.getUsuarioCriacao = function () {
            return this.options.value.usuarioCriacao || null;
        };
        this.setValidations = function () {
            __financeiroDescontoTituloAdd.options.validator.settings.rules = {
                desconto: {number: true, required: true}, titulo: {number: true, required: true},
                usuarioAlteracao: {number: true}, usuarioCriacao: {number: true},
                descontoTituloCriacao: {required: true, dateBR: true},
                descontoTituloAlteracao: {required: true, dateBR: true},
                descontoTituloSituacao: {maxlength: 8, required: true},
            };
            __financeiroDescontoTituloAdd.options.validator.settings.messages = {
                desconto: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                titulo: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                usuarioAlteracao: {number: 'Número inválido!'}, usuarioCriacao: {number: 'Número inválido!'},
                descontoTituloCriacao: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                descontoTituloAlteracao: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                descontoTituloSituacao: {maxlength: 'Tamanho máximo: 8!', required: 'Campo obrigatório!'},
            };

            $(__financeiroDescontoTituloAdd.options.formElement).submit(function (e) {
                var ok = $(__financeiroDescontoTituloAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__financeiroDescontoTituloAdd.options.formElement);

                if (__financeiroDescontoTituloAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __financeiroDescontoTituloAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __financeiroDescontoTituloAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__financeiroDescontoTituloAdd.options.listagem) {
                                    $.financeiroDescontoTituloIndex().reloadDataTableFinanceiroDescontoTitulo();
                                    __financeiroDescontoTituloAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#financeiro-desconto-titulo-modal').modal('hide');
                                }

                                __financeiroDescontoTituloAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaFinanceiroDescontoTitulo = function (desconto_titulo_id, callback) {
            var $form = $(__financeiroDescontoTituloAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __financeiroDescontoTituloAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + desconto_titulo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroDescontoTituloAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __financeiroDescontoTituloAdd.removeOverlay($form);
                },
                erro: function () {
                    __financeiroDescontoTituloAdd.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroDescontoTituloAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.financeiroDescontoTituloAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-desconto-titulo.add");

        if (!obj) {
            obj = new FinanceiroDescontoTituloAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-desconto-titulo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);