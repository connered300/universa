(function ($, window, document) {
    'use strict';
    var FinanceiroDescontoTituloIndex = function () {
        VersaShared.call(this);
        var __financeiroDescontoTituloIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                financeiroDescontoTitulo: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __financeiroDescontoTituloIndex.options.datatables.financeiroDescontoTitulo =
                $('#dataTableFinanceiroDescontoTitulo').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __financeiroDescontoTituloIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary financeiroDescontoTitulo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger financeiroDescontoTitulo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                                data[row]['desconto_titulo_criacao_formatado'] = __financeiroDescontoTituloIndex
                                    .formatDateTime(data[row]['desconto_titulo_criacao']);
                                data[row]['desconto_titulo_alteracao_formatado'] = __financeiroDescontoTituloIndex
                                    .formatDateTime(data[row]['desconto_titulo_alteracao']);


                                if (
                                    data[row]['desconto_titulo_aplicado'] != 'Não' ||
                                    data[row]['titulo_estado'] != 'Aberto'
                                ) {
                                    data[row]['acao'] = __financeiroDescontoTituloIndex.createBtnGroup([]);
                                } else {
                                    var btns = [];

                                    if (data[row]['desconto_titulo_situacao'] != 'Deferido') {
                                        btns.push(
                                            {
                                                class: 'btn-primary btn-descontoTitulo-deferir', icon: 'fa-check',
                                                title: 'Deferir título'
                                            }
                                        );
                                    }
                                    if (data[row]['desconto_titulo_situacao'] != 'Indeferido') {
                                        btns.push(
                                            {
                                                class: 'btn-warning btn-descontoTitulo-indeferir', icon: 'fa-ban',
                                                title: 'Indeferir título'
                                            }
                                        );
                                    }
                                    if (data[row]['desconto_titulo_situacao'] == 'Pendente') {
                                        btns.push(
                                            {
                                                class: 'btn-danger btn-descontoTitulo-remove', icon: 'fa-times',
                                                title: 'Remover desconto do título.'
                                            }
                                        );
                                    }

                                    data[row]['acao'] = __financeiroDescontoTituloIndex.createBtnGroup(btns);

                                }
                            }

                            __financeiroDescontoTituloIndex.removeOverlay($('#container-financeiro-desconto-titulo'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "desconto_titulo_id", targets: colNum++, data: "desconto_titulo_id"},
                        {name: "desconto_dados", targets: colNum++, data: "desconto_dados"},
                        {name: "titulo_dados", targets: colNum++, data: "titulo_dados"},
                        {name: "usuario_criacao_login", targets: colNum++, data: "usuario_criacao_login"},
                        {name: "usuario_alteracao_login", targets: colNum++, data: "usuario_alteracao_login"},
                        {name: "desconto_titulo_criacao", targets: colNum++, data: "desconto_titulo_criacao_formatado"},
                        {
                            name: "desconto_titulo_alteracao", targets: colNum++,
                            data: "desconto_titulo_alteracao_formatado"
                        },
                        {name: "desconto_titulo_aplicado", targets: colNum++, data: "desconto_titulo_aplicado"},
                        {name: "desconto_titulo_situacao", targets: colNum++, data: "desconto_titulo_situacao"},
                        {name: "desconto_titulo_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[6, 'desc']]
                });

            $('#dataTableFinanceiroDescontoTitulo').on('click', '.btn-descontoTitulo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __financeiroDescontoTituloIndex.options.datatables.financeiroDescontoTitulo.fnGetData($pai);
                var arrRemover = {
                    descontoTituloId: data['desconto_titulo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de desconto "' + data['desconto_dados'] +
                    '" do título "' + data['desconto_dados'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __financeiroDescontoTituloIndex.addOverlay(
                                $('#container-financeiro-desconto-titulo'),
                                'Aguarde, solicitando remoção de registro de desconto título...'
                            );
                            $.ajax({
                                url: __financeiroDescontoTituloIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __financeiroDescontoTituloIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de desconto título:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __financeiroDescontoTituloIndex.removeOverlay($('#container-financeiro-desconto-titulo'));
                                    } else {
                                        __financeiroDescontoTituloIndex.reloadDataTableFinanceiroDescontoTitulo();
                                        __financeiroDescontoTituloIndex.showNotificacaoSuccess(
                                            "Registro de desconto título removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });


            $('#dataTableFinanceiroDescontoTitulo').on(
                'click',
                '.btn-descontoTitulo-deferir, .btn-descontoTitulo-indeferir',
                function () {
                    var $pai = $(this).closest('tr');
                    var data = __financeiroDescontoTituloIndex.options.datatables.financeiroDescontoTitulo.fnGetData($pai);
                    var sit = $(this).hasClass('btn-descontoTitulo-indeferir') ? 'Indeferi' : 'Deferi';
                    var situacao = sit + 'do';
                    var arrDados = __financeiroDescontoTituloIndex.keysToCamelCase(data);

                    arrDados['descontoTituloSituacao'] = situacao;
                    arrDados['desconto'] = arrDados['descontoId'];
                    arrDados['titulo'] = arrDados['tituloId'];
                    arrDados['ajax'] = true;

                    if (data['desconto_percentual'] > 100) {
                        __financeiroDescontoTituloIndex.showNotificacaoWarning('Valor do percentual não pode extrapolar 100%!');
                        return false;
                    }
                    if (data['desconto_percentual'] == 100) {

                        __financeiroDescontoTituloIndex.showNotificacaoInfo('Atenção você esta deferindo um desconto de 100% para este título! ');
                    }

                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Deseja ' + sit + 'r o desconto  "' + data['desconto_dados'] + '" no título "' +
                        data['titulo_dados'] + '"?',
                        buttons: "[Não][Sim]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Sim") {
                            setTimeout(function () {
                                __financeiroDescontoTituloIndex.addOverlay(
                                    $('#container-financeiro-desconto-titulo'),
                                    'Aguarde, solicitando alteração no desconto do título...'
                                );
                                $.ajax({
                                    url: __financeiroDescontoTituloIndex.options.url.edit + '/' +
                                    arrDados['descontoTituloId'],
                                    type: 'POST',
                                    dataType: 'json',
                                    data: arrDados,
                                    success: function (data) {
                                        if (data.erro) {
                                            __financeiroDescontoTituloIndex.showNotificacaoDanger(
                                                "Não foi possivel cadastrar ou deferir o desconto deste título:\n" +
                                                "<i>" + data['mensagem'] + "</i>"
                                            );
                                            __financeiroDescontoTituloIndex.removeOverlay($('#container-financeiro-desconto-titulo'));
                                        } else {
                                            __financeiroDescontoTituloIndex.reloadDataTableFinanceiroDescontoTitulo();
                                            __financeiroDescontoTituloIndex.showNotificacaoSuccess(
                                                "O desconto do título foi " + situacao + "!"
                                            );
                                        }
                                    }
                                });
                            }, 400);
                        }
                    });
                }
            );

        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-desconto-titulo-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['descontoTituloId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['descontoTituloId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#desconto").select2('data', data['desconto']).trigger("change");
            $("#titulo").select2('data', data['titulo']).trigger("change");
            $("#usuarioAlteracao").select2('data', data['usuarioAlteracao']).trigger("change");
            $("#usuarioCriacao").select2('data', data['usuarioCriacao']).trigger("change");
            $("#descontoTituloSituacao").select2('val', data['descontoTituloSituacao']).trigger("change");

            $('#financeiro-desconto-titulo-modal .modal-title').html(actionTitle + ' desconto título');
            $('#financeiro-desconto-titulo-modal').modal();
        };

        this.reloadDataTableFinanceiroDescontoTitulo = function () {
            this.getDataTableFinanceiroDescontoTitulo().api().ajax.reload(null, false);
        };

        this.getDataTableFinanceiroDescontoTitulo = function () {
            if (!__financeiroDescontoTituloIndex.options.datatables.financeiroDescontoTitulo) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroDescontoTitulo')) {
                    __financeiroDescontoTituloIndex.options.datatables.financeiroDescontoTitulo =
                        $('#dataTableFinanceiroDescontoTitulo').DataTable();
                } else {
                    __financeiroDescontoTituloIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroDescontoTituloIndex.options.datatables.financeiroDescontoTitulo;
        };

        this.run = function (opts) {
            __financeiroDescontoTituloIndex.setDefaults(opts);
            __financeiroDescontoTituloIndex.setSteps();
        };
    };

    $.financeiroDescontoTituloIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-desconto-titulo.index");

        if (!obj) {
            obj = new FinanceiroDescontoTituloIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-desconto-titulo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);



