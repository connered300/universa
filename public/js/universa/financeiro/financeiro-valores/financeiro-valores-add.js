(function ($, window) {
    'use strict';

    var FinanceiroValoresAdd = function () {
        VersaShared.call(this);
        var __financeiroValoresAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                campusCurso: '',
                acadperiodoLetivo: '',
                financeiroTituloTipo: '',
                acadgeralAreaConhecimento: '',
                area: null
            },
            data: {},
            value: {
                cursocampus: null,
                per: null,
                tipotitulo: null
            },
            datatables: {},
            wizardElement: '#financeiro-valores-wizard',
            formElement: '#financeiro-valores-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $cursocampus       = $("#cursocampus"),
                $tipotitulo        = $("#tipotitulo"),
                $valoresDatas      = $("#valoresDataInicio, #valoresDataFim"),
                $valoresDataInicio = $("#valoresDataInicio"),
                $valoresDataFim    = $("#valoresDataFim"),
                $area              = $("#area"),
                $per               = $("#per");

            $cursocampus.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __financeiroValoresAdd.options.url.campusCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.camp_nome + ' / ' + el.curso_nome, id: el.cursocampus_id};
                        });

                        return {results: transformed};
                    }
                }
            }).change(function () {
                if (this.value != '') {
                    $area.select2('data', null);
                    $area.select2('disable');
                    $area.val('');
                } else {
                    $area.select2('enable');
                }
            });

            if (__financeiroValoresAdd.getCursocampus()) {
                $cursocampus.select2("data", __financeiroValoresAdd.getCursocampus());
            }

            $tipotitulo.select2({
                language: 'pt-BR',
                ajax: {
                    url: __financeiroValoresAdd.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.tipotitulo_nome, id: el.tipotitulo_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__financeiroValoresAdd.getTipotitulo()) {
                $tipotitulo.select2("data", __financeiroValoresAdd.getTipotitulo());
            }

            $area.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __financeiroValoresAdd.options.url.acadgeralAreaConhecimento,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_descricao, id: el.area_id};
                        });

                        return {results: transformed};
                    }
                }
            }).change(function () {
                if (this.value != '') {
                    $cursocampus.select2('data', null);
                    $cursocampus.select2('disable');
                    $cursocampus.val('');
                } else {
                    $cursocampus.select2('enable');
                }
            });

            if (__financeiroValoresAdd.getArea()) {
                $area.select2("data", __financeiroValoresAdd.getArea());
            }

            $per.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __financeiroValoresAdd.options.url.acadperiodoLetivo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.per_nome, id: el.per_id};
                        });

                        return {results: transformed};
                    }
                }
            }).change(function () {
                if (this.value != '') {
                    $valoresDatas.prop('disabled', true).val('');
                } else {
                    $valoresDatas.prop('disabled', false);
                }
            });

            if (__financeiroValoresAdd.getPer()) {
                $per.select2("data", __financeiroValoresAdd.getPer());
            }

            $("#valoresPreco").inputmask({showMaskOnHover: false, alias: 'currency', prefix: '', groupSeparator: ''});
            $valoresDataInicio
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $valoresDataFim
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

            $valoresDatas.change(function () {
                if ($valoresDataInicio.val() != '' || $valoresDataFim.val() != '') {
                    $per.select2('disable');
                    $per.val('');
                } else {
                    $per.select2('enable');
                }

                $per.trigger('change');
            });

            $("#valoresParcela").inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});

            $("#valoresParcela, #valoresPreco").change(function () {
                var valoresPreco = parseFloat($('#valoresPreco').val());
                var valoresParcela = parseInt($('#valoresParcela').val());

                $('#valoresPrecoTotal').html(__financeiroValoresAdd.formatarMoeda(
                    valoresParcela * valoresPreco
                ));
            }).trigger('change');
        };

        this.setCursocampus = function (cursocampus) {
            this.options.value.cursocampus = cursocampus || null;
        };

        this.getCursocampus = function () {
            return this.options.value.cursocampus || null;
        };

        this.setPer = function (per) {
            this.options.value.per = per || null;
        };

        this.getPer = function () {
            return this.options.value.per || null;
        };

        this.setTipotitulo = function (tipotitulo) {
            this.options.value.tipotitulo = tipotitulo || null;
        };

        this.getTipotitulo = function () {
            return this.options.value.tipotitulo || null;
        };

        this.setArea = function (area) {
            this.options.value.area = area || null;
        };

        this.getArea = function () {
            return this.options.value.area || null;
        };

        this.setValidations = function () {
            __financeiroValoresAdd.options.validator.settings.rules = {
                cursocampus: {maxlength: 10, number: true},
                per: {maxlength: 10, number: true},
                tipotitulo: {maxlength: 11, number: true, required: true},
                area: {number: true},
                valoresPreco: {maxlength: 10, number: true},
                valoresDataInicio: {dateBR: true},
                valoresDataFim: {dateBR: true},
                valoresParcela: {maxlength: 11, number: true, required: true}
            };
            __financeiroValoresAdd.options.validator.settings.messages = {
                cursocampus: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                per: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                tipotitulo: {
                    maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                },
                area: {number: 'Número inválido!'},
                valoresPreco: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
                valoresDataInicio: {dateBR: 'Informe uma data válida!'},
                valoresDataFim: {dateBR: 'Informe uma data válida!'},
                valoresParcela: {
                    maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!', required: 'Campo obrigatório!'
                }
            };

            $(__financeiroValoresAdd.options.formElement).submit(function (e) {
                var ok = $(__financeiroValoresAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                $(
                    '#valoresDataInicio, #valoresDataFim, ' +
                    '#valoresParcela, #per, #area, #cursocampus'
                ).prop('disabled', false);

                var $form = $(__financeiroValoresAdd.options.formElement);

                if (__financeiroValoresAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __financeiroValoresAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __financeiroValoresAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__financeiroValoresAdd.options.listagem) {
                                    $.financeiroValoresIndex().reloadDataTableFinanceiroValores();
                                    __financeiroValoresAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#financeiro-valores-modal').modal('hide');
                                }

                                __financeiroValoresAdd.removeOverlay($form);

                                $(
                                    '#valoresDataInicio, #valoresDataFim, ' +
                                    '#valoresParcela, #per, #area, #cursocampus'
                                ).trigger('change');
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaFinanceiroValores = function (valores_id, callback) {
            var $form = $(__financeiroValoresAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __financeiroValoresAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + valores_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __financeiroValoresAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __financeiroValoresAdd.removeOverlay($form);
                },
                erro: function () {
                    __financeiroValoresAdd.showNotificacaoDanger('Erro desconhecido!');
                    __financeiroValoresAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.financeiroValoresAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-valores.add");

        if (!obj) {
            obj = new FinanceiroValoresAdd();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-valores.add', obj);
        }

        return obj;
    };
})(window.jQuery, window);