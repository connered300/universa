(function ($, window, document) {
    'use strict';
    var FinanceiroValoresIndex = function () {
        VersaShared.call(this);
        var __financeiroValoresIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                financeiroValores: null
            }
        };

        this.setSteps = function () {
            var colNum = -1;
            var $dataTableFinanceiroValores = $('#dataTableFinanceiroValores');

            __financeiroValoresIndex.options.datatables.financeiroValores = $dataTableFinanceiroValores.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __financeiroValoresIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary financeiroValores-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger financeiroValores-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __financeiroValoresIndex.removeOverlay($('#container-financeiro-valores'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "valores_id", targets: ++colNum, data: "valores_id"},
                    {name: "campus", targets: ++colNum, data: "campus"},
                    {name: "per_nome", targets: ++colNum, data: "per_nome"},
                    {name: "tipotitulo_nome", targets: ++colNum, data: "tipotitulo_nome"},
                    {name: "area_descricao", targets: ++colNum, data: "area_descricao"},
                    {name: "valores_preco", targets: ++colNum, data: "valores_preco"},
                    {name: "valores_data_inicio", targets: ++colNum, data: "valores_data_inicio"},
                    {name: "valores_data_fim", targets: ++colNum, data: "valores_data_fim"},
                    {name: "valores_parcela", targets: ++colNum, data: "valores_parcela"},
                    {name: "valores_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableFinanceiroValores.on('click', '.financeiroValores-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __financeiroValoresIndex.options.datatables.financeiroValores.fnGetData($pai);

                if (!__financeiroValoresIndex.options.ajax) {
                    location.href = __financeiroValoresIndex.options.url.edit + '/' + data['valores_id'];
                } else {
                    __financeiroValoresIndex.addOverlay(
                        $('#container-financeiro-valores'), 'Aguarde, carregando dados para edição...'
                    );
                    $.financeiroValoresAdd().pesquisaFinanceiroValores(
                        data['valores_id'],
                        function (data) { __financeiroValoresIndex.showModal(data); }
                    );
                }
            });


            $('#btn-financeiro-valores-add').click(function (e) {
                if (__financeiroValoresIndex.options.ajax) {
                    __financeiroValoresIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableFinanceiroValores.on('click', '.financeiroValores-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __financeiroValoresIndex.options.datatables.financeiroValores.fnGetData($pai);
                var arrRemover = {
                    valoresId: data['valores_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de valores "' + data['cursocampus_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __financeiroValoresIndex.addOverlay(
                                $('#container-financeiro-valores'),
                                'Aguarde, solicitando remoção de registro de valores...'
                            );
                            $.ajax({
                                url: __financeiroValoresIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __financeiroValoresIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de valores:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __financeiroValoresIndex.removeOverlay($('#container-financeiro-valores'));
                                    } else {
                                        __financeiroValoresIndex.reloadDataTableFinanceiroValores();
                                        __financeiroValoresIndex.showNotificacaoSuccess(
                                            "Registro de valores removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#financeiro-valores-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['valoresId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['valoresId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#cursocampus").select2('data', data['cursocampus'] || null).trigger("change");
            $("#per").select2('data', data['per'] || null);
            $("#tipotitulo").select2('data', data['tipotitulo'] || null).trigger("change");
            $("#area").select2('data', data['area'] || null).trigger("change");

            $('#valoresDataInicio, #valoresDataFim, #per').trigger('change');
            $("#valoresParcela, #valoresPreco").trigger("change");

            var $modal = $('#financeiro-valores-modal');

            $modal.find('.modal-title').html(actionTitle + ' valores');
            $modal.modal();

            __financeiroValoresIndex.removeOverlay($('#container-financeiro-valores'));
        };

        this.reloadDataTableFinanceiroValores = function () {
            this.getDataTableFinanceiroValores().api().ajax.reload(null, false);
        };

        this.getDataTableFinanceiroValores = function () {
            if (!__financeiroValoresIndex.options.datatables.financeiroValores) {
                if (!$.fn.dataTable.isDataTable('#dataTableFinanceiroValores')) {
                    __financeiroValoresIndex.options.datatables.financeiroValores =
                        $('#dataTableFinanceiroValores').DataTable();
                } else {
                    __financeiroValoresIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __financeiroValoresIndex.options.datatables.financeiroValores;
        };

        this.run = function (opts) {
            __financeiroValoresIndex.setDefaults(opts);
            __financeiroValoresIndex.setSteps();
        };
    };

    $.financeiroValoresIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.financeiro-valores.index");

        if (!obj) {
            obj = new FinanceiroValoresIndex();
            obj.run(params);
            $(window).data('universa.financeiro.financeiro-valores.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);