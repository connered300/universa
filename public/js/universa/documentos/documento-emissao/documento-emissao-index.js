(function ($, window, document) {
    'use strict';
    var DocumentoEmissaoIndex = function () {
        VersaShared.call(this);
        var __documentoEmissaoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                documentoEmissao: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __documentoEmissaoIndex.options.datatables.documentoEmissao = $('#dataTableDocumentoEmissao').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __documentoEmissaoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary documentoEmissao-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger documentoEmissao-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                            data[row]['emissao_data_formatada'] = __documentoEmissaoIndex.formatDateTime(
                                data[row]['emissao_data']
                            );
                        }

                        __documentoEmissaoIndex.removeOverlay($('#container-documento-emissao'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "emissao_id", targets: colNum++, data: "emissao_id"},
                    {name: "modelo_descricao", targets: colNum++, data: "modelo_descricao"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "emissao_data", targets: colNum++, data: "emissao_data_formatada"},
                    {name: "login", targets: colNum++, data: "login"},
                    {name: "emissao_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableDocumentoEmissao').on('click', '.documentoEmissao-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __documentoEmissaoIndex.options.datatables.documentoEmissao.fnGetData($pai);

                if (!__documentoEmissaoIndex.options.ajax) {
                    location.href = __documentoEmissaoIndex.options.url.edit + '/' + data['emissao_id'];
                } else {
                    __documentoEmissaoIndex.addOverlay(
                        $('#container-documento-emissao'), 'Aguarde, carregando dados para edição...'
                    );
                    $.documentoEmissaoAdd().pesquisaDocumentoEmissao(
                        data['emissao_id'],
                        function (data) { __documentoEmissaoIndex.showModal(data); }
                    );
                }
            });


            $('#btn-documento-emissao-add').click(function (e) {
                if (__documentoEmissaoIndex.options.ajax) {
                    __documentoEmissaoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableDocumentoEmissao').on('click', '.documentoEmissao-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __documentoEmissaoIndex.options.datatables.documentoEmissao.fnGetData($pai);
                var arrRemover = {
                    emissaoId: data['emissao_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de emissão "' + data['modelo_id'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __documentoEmissaoIndex.addOverlay(
                                $('#container-documento-emissao'),
                                'Aguarde, solicitando remoção de registro de emissão...'
                            );
                            $.ajax({
                                url: __documentoEmissaoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __documentoEmissaoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de emissão:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __documentoEmissaoIndex.removeOverlay($('#container-documento-emissao'));
                                    } else {
                                        __documentoEmissaoIndex.reloadDataTableDocumentoEmissao();
                                        __documentoEmissaoIndex.showNotificacaoSuccess(
                                            "Registro de emissão removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#documento-emissao-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['emissaoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['emissaoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#modelo").select2('data', data['modelo']).trigger("change");
            $("#pes").select2('data', data['pes']).trigger("change");
            $("#usuarioIdEmissao").select2('data', data['usuarioIdEmissao']).trigger("change");

            $('#documento-emissao-modal .modal-title').html(actionTitle + ' emissão');
            $('#documento-emissao-modal').modal();
            __documentoEmissaoIndex.removeOverlay($('#container-documento-emissao'));
        };

        this.reloadDataTableDocumentoEmissao = function () {
            this.getDataTableDocumentoEmissao().api().ajax.reload(null, false);
        };

        this.getDataTableDocumentoEmissao = function () {
            if (!__documentoEmissaoIndex.options.datatables.documentoEmissao) {
                if (!$.fn.dataTable.isDataTable('#dataTableDocumentoEmissao')) {
                    __documentoEmissaoIndex.options.datatables.documentoEmissao =
                        $('#dataTableDocumentoEmissao').DataTable();
                } else {
                    __documentoEmissaoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __documentoEmissaoIndex.options.datatables.documentoEmissao;
        };

        this.run = function (opts) {
            __documentoEmissaoIndex.setDefaults(opts);
            __documentoEmissaoIndex.setSteps();
        };
    };

    $.documentoEmissaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.documentos.documento-emissao.index");

        if (!obj) {
            obj = new DocumentoEmissaoIndex();
            obj.run(params);
            $(window).data('universa.documentos.documento-emissao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);