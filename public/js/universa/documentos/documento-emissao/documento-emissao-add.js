(function ($, window, document) {
    'use strict';

    var DocumentoEmissaoAdd = function () {
        VersaShared.call(this);
        var __documentoEmissaoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                documentoModelo: '',
                pessoa: '',
                acessoPessoas: ''
            },
            data: {},
            value: {
                modelo: null,
                pes: null,
                usuarioIdEmissao: null
            },
            datatables: {},
            wizardElement: '#documento-emissao-wizard',
            formElement: '#documento-emissao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#modelo").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __documentoEmissaoAdd.options.url.documentoModelo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.modelo_id;
                            el.text = el.modelo_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#pes").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __documentoEmissaoAdd.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#usuarioIdEmissao").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __documentoEmissaoAdd.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.login;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            __documentoEmissaoAdd.iniciarElementoDatePicker($("#emissaoData"));

            $('#modelo').select2("data", __documentoEmissaoAdd.getModelo());
            $('#pes').select2("data", __documentoEmissaoAdd.getPes());
            $('#usuarioIdEmissao').select2("data", __documentoEmissaoAdd.getUsuarioIdEmissao());
        };

        this.setModelo = function (modelo) {
            this.options.value.modelo = modelo || null;
        };

        this.getModelo = function () {
            return this.options.value.modelo || null;
        };

        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };

        this.setUsuarioIdEmissao = function (usuarioIdEmissao) {
            this.options.value.usuarioIdEmissao = usuarioIdEmissao || null;
        };

        this.getUsuarioIdEmissao = function () {
            return this.options.value.usuarioIdEmissao || null;
        };
        this.setValidations = function () {
            __documentoEmissaoAdd.options.validator.settings.rules = {
                modelo: {number: true, required: true}, pes: {number: true, required: true},
                emissaoData: {dateBR: true}, usuarioIdEmissao: {number: true, required: true},
            };
            __documentoEmissaoAdd.options.validator.settings.messages = {
                modelo: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                pes: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                emissaoData: {dateBR: 'Informe uma data válida!'},
                usuarioIdEmissao: {number: 'Número inválido!', required: 'Campo obrigatório!'},
            };

            $(__documentoEmissaoAdd.options.formElement).submit(function (e) {
                var ok = $(__documentoEmissaoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__documentoEmissaoAdd.options.formElement);

                if (__documentoEmissaoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __documentoEmissaoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __documentoEmissaoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__documentoEmissaoAdd.options.listagem) {
                                    $.documentoEmissaoIndex().reloadDataTableDocumentoEmissao();
                                    __documentoEmissaoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#documento-emissao-modal').modal('hide');
                                }

                                __documentoEmissaoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaDocumentoEmissao = function (emissao_id, callback) {
            var $form = $(__documentoEmissaoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __documentoEmissaoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + emissao_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __documentoEmissaoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __documentoEmissaoAdd.removeOverlay($form);
                },
                erro: function () {
                    __documentoEmissaoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __documentoEmissaoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.documentoEmissaoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.documentos.documento-emissao.add");

        if (!obj) {
            obj = new DocumentoEmissaoAdd();
            obj.run(params);
            $(window).data('universa.documentos.documento-emissao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);