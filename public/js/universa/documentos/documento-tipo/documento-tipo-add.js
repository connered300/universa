(function ($, window, document) {
    'use strict';

    var DocumentoTipoAdd = function () {
        VersaShared.call(this);
        var __documentoTipoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#documento-tipo-wizard',
            formElement: '#documento-tipo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __documentoTipoAdd.options.validator.settings.rules = {tdocDescricao: {maxlength: 45, required: true},};
            __documentoTipoAdd.options.validator.settings.messages =
            {tdocDescricao: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},};

            $(__documentoTipoAdd.options.formElement).submit(function (e) {
                var ok = $(__documentoTipoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__documentoTipoAdd.options.formElement);

                if (__documentoTipoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __documentoTipoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __documentoTipoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__documentoTipoAdd.options.listagem) {
                                    $.documentoTipoIndex().reloadDataTableDocumentoTipo();
                                    __documentoTipoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#documento-tipo-modal').modal('hide');
                                }

                                __documentoTipoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaDocumentoTipo = function (tdoc_id, callback) {
            var $form = $(__documentoTipoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __documentoTipoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + tdoc_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __documentoTipoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __documentoTipoAdd.removeOverlay($form);
                },
                erro: function () {
                    __documentoTipoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __documentoTipoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.documentoTipoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.documentos.documento-tipo.add");

        if (!obj) {
            obj = new DocumentoTipoAdd();
            obj.run(params);
            $(window).data('universa.documentos.documento-tipo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);