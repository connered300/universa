(function ($, window, document) {
    'use strict';
    var DocumentoTipoIndex = function () {
        VersaShared.call(this);
        var __documentoTipoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                documentoTipo: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __documentoTipoIndex.options.datatables.documentoTipo = $('#dataTableDocumentoTipo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __documentoTipoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary documentoTipo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger documentoTipo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __documentoTipoIndex.removeOverlay($('#container-documento-tipo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "tdoc_id", targets: colNum++, data: "tdoc_id"},
                    {name: "tdoc_descricao", targets: colNum++, data: "tdoc_descricao"},
                    {name: "tdoc_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableDocumentoTipo').on('click', '.documentoTipo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __documentoTipoIndex.options.datatables.documentoTipo.fnGetData($pai);

                if (!__documentoTipoIndex.options.ajax) {
                    location.href = __documentoTipoIndex.options.url.edit + '/' + data['tdoc_id'];
                } else {
                    __documentoTipoIndex.addOverlay(
                        $('#container-documento-tipo'), 'Aguarde, carregando dados para edição...'
                    );
                    $.documentoTipoAdd().pesquisaDocumentoTipo(
                        data['tdoc_id'],
                        function (data) { __documentoTipoIndex.showModal(data); }
                    );
                }
            });


            $('#btn-documento-tipo-add').click(function (e) {
                if (__documentoTipoIndex.options.ajax) {
                    __documentoTipoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableDocumentoTipo').on('click', '.documentoTipo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __documentoTipoIndex.options.datatables.documentoTipo.fnGetData($pai);
                var arrRemover = {
                    tdocId: data['tdoc_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de tipo de documento "' + data['tdoc_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __documentoTipoIndex.addOverlay(
                                $('#container-documento-tipo'), 'Aguarde, solicitando remoção de registro de tipo de documento...'
                            );
                            $.ajax({
                                url: __documentoTipoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __documentoTipoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de tipo de documento:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __documentoTipoIndex.removeOverlay($('#container-documento-tipo'));
                                    } else {
                                        __documentoTipoIndex.reloadDataTableDocumentoTipo();
                                        __documentoTipoIndex.showNotificacaoSuccess(
                                            "Registro de tipo de documento removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#documento-tipo-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['tdocId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['tdocId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $('#documento-tipo-modal .modal-title').html(actionTitle + ' tipo de documento');
            $('#documento-tipo-modal').modal();
            __documentoTipoIndex.removeOverlay($('#container-documento-tipo'));
        };

        this.reloadDataTableDocumentoTipo = function () {
            this.getDataTableDocumentoTipo().api().ajax.reload(null, false);
        };

        this.getDataTableDocumentoTipo = function () {
            if (!__documentoTipoIndex.options.datatables.documentoTipo) {
                if (!$.fn.dataTable.isDataTable('#dataTableDocumentoTipo')) {
                    __documentoTipoIndex.options.datatables.documentoTipo = $('#dataTableDocumentoTipo').DataTable();
                } else {
                    __documentoTipoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __documentoTipoIndex.options.datatables.documentoTipo;
        };

        this.run = function (opts) {
            __documentoTipoIndex.setDefaults(opts);
            __documentoTipoIndex.setSteps();
        };
    };

    $.documentoTipoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.documentos.documento-tipo.index");

        if (!obj) {
            obj = new DocumentoTipoIndex();
            obj.run(params);
            $(window).data('universa.documentos.documento-tipo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);