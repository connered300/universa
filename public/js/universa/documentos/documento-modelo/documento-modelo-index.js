(function ($, window, document) {
    'use strict';
    var DocumentoModeloIndex = function () {
        VersaShared.call(this);
        var __documentoModeloIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                documentoModelo: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __documentoModeloIndex.options.datatables.documentoModelo = $('#dataTableDocumentoModelo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __documentoModeloIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary documentoModelo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger documentoModelo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                            data[row]['modelo_data_inicio_formatada'] = __documentoModeloIndex.formatDate(
                                data[row]['modelo_data_inicio']
                            );
                            data[row]['modelo_data_fim_formatada'] = __documentoModeloIndex.formatDate(
                                data[row]['modelo_data_fim']
                            );
                        }

                        __documentoModeloIndex.removeOverlay($('#container-documento-modelo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "modelo_id", targets: colNum++, data: "modelo_id"},
                    {name: "modelo_descricao", targets: colNum++, data: "modelo_descricao"},
                    {name: "tdoc_descricao", targets: colNum++, data: "tdoc_descricao"},
                    {name: "camp_nome", targets: colNum++, data: "camp_nome"},
                    {name: "curso_nome", targets: colNum++, data: "curso_nome"},
                    {name: "area_descricao", targets: colNum++, data: "area_descricao"},
                    {name: "modelo_data_inicio", targets: colNum++, data: "modelo_data_inicio_formatada"},
                    {name: "modelo_data_fim", targets: colNum++, data: "modelo_data_fim_formatada"},
                    {name: "modelo_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableDocumentoModelo').on('click', '.documentoModelo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __documentoModeloIndex.options.datatables.documentoModelo.fnGetData($pai);

                if (!__documentoModeloIndex.options.ajax) {
                    location.href = __documentoModeloIndex.options.url.edit + '/' + data['modelo_id'];
                } else {
                    __documentoModeloIndex.addOverlay(
                        $('#container-documento-modelo'), 'Aguarde, carregando dados para edição...'
                    );
                    $.documentoModeloAdd().pesquisaDocumentoModelo(
                        data['modelo_id'],
                        function (data) { __documentoModeloIndex.showModal(data); }
                    );
                }
            });


            $('#btn-documento-modelo-add').click(function (e) {
                if (__documentoModeloIndex.options.ajax) {
                    __documentoModeloIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableDocumentoModelo').on('click', '.documentoModelo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __documentoModeloIndex.options.datatables.documentoModelo.fnGetData($pai);
                var arrRemover = {
                    modeloId: data['modelo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de modelo "' + data['modelo_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __documentoModeloIndex.addOverlay(
                                $('#container-documento-modelo'),
                                'Aguarde, solicitando remoção de registro de modelo...'
                            );
                            $.ajax({
                                url: __documentoModeloIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __documentoModeloIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de modelo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __documentoModeloIndex.removeOverlay($('#container-documento-modelo'));
                                    } else {
                                        __documentoModeloIndex.reloadDataTableDocumentoModelo();
                                        __documentoModeloIndex.showNotificacaoSuccess(
                                            "Registro de modelo removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#documento-modelo-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['modeloId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['modeloId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#tdoc").select2('data', data['tdoc']).trigger("change");
            $("#camp").select2('data', data['camp']).trigger("change");
            $("#curso").select2('data', data['curso']).trigger("change");
            $("#area").select2('data', data['area']).trigger("change");
            $("#grupoId").select2('data', data['arrGrupo']).trigger("change");
            $("#tipotitulo").select2('data', data['tipotitulo']).trigger("change");
            $("#modeloContexto").val(data['modeloContexto'] || '');
            $("#modeloConteudo").val(data['modeloConteudo'] || '');
            $("#modeloConteudo").trigger("change");

            $('#documento-modelo-modal .modal-title').html(actionTitle + ' modelo');
            $('#documento-modelo-modal').modal();
            __documentoModeloIndex.removeOverlay($('#container-documento-modelo'));
        };

        this.reloadDataTableDocumentoModelo = function () {
            this.getDataTableDocumentoModelo().api().ajax.reload(null, false);
        };

        this.getDataTableDocumentoModelo = function () {
            if (!__documentoModeloIndex.options.datatables.documentoModelo) {
                if (!$.fn.dataTable.isDataTable('#dataTableDocumentoModelo')) {
                    __documentoModeloIndex.options.datatables.documentoModelo =
                        $('#dataTableDocumentoModelo').DataTable();
                } else {
                    __documentoModeloIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __documentoModeloIndex.options.datatables.documentoModelo;
        };

        this.run = function (opts) {
            __documentoModeloIndex.setDefaults(opts);
            __documentoModeloIndex.setSteps();
        };
    };

    $.documentoModeloIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.documentos.documento-modelo.index");

        if (!obj) {
            obj = new DocumentoModeloIndex();
            obj.run(params);
            $(window).data('universa.documentos.documento-modelo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);