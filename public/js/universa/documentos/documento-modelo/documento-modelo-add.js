(function ($, window, document) {
    'use strict';

    var DocumentoModeloAdd = function () {
        VersaShared.call(this);
        var __documentoModeloAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                documentoTipo: '',
                orgCampus: '',
                acadCurso: '',
                acadgeralAreaConhecimento: '',
                acessoGrupo: ''
            },
            data: {},
            value: {
                tdoc: null,
                camp: null,
                curso: null,
                area: null,
                grupo: null
            },
            datatables: {},
            wizardElement: '#documento-modelo-wizard',
            formElement: '#documento-modelo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#tdoc").select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __documentoModeloAdd.options.url.documentoTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.tdoc_id;
                            el.text = el.tdoc_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#grupoId").select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __documentoModeloAdd.options.url.acessoGrupo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.id;
                            el.text = el.grup_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#camp").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __documentoModeloAdd.options.url.orgCampus,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.camp_id;
                            el.text = el.camp_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#curso").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __documentoModeloAdd.options.url.acadCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.curso_id;
                            el.text = el.curso_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#area").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __documentoModeloAdd.options.url.acadgeralAreaConhecimento,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.area_id;
                            el.text = el.area_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $("#tipotitulo").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __documentoModeloAdd.options.url.financeiroTituloTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.tipotitulo_id;
                            el.text = el.tipotitulo_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });


            $('#tdoc').select2("data", __documentoModeloAdd.getTdoc());
            $('#area').select2("data", __documentoModeloAdd.getArea());
            $('#camp').select2("data", __documentoModeloAdd.getCamp());
            $('#curso').select2("data", __documentoModeloAdd.getCurso());
            $('#tipotitulo').select2("data", __documentoModeloAdd.getTipotitulo());
            $('#grupoId').select2("data", __documentoModeloAdd.getGrupo());


            $('#modeloConteudo').ckeditor({
                height: '200px', linkShowAdvancedTab: false,
                autoStartup: true,
                extraAllowedContent: (
                    'style;body;*{*};' +
                    'img[src,alt,width,height,style,class,id];' +
                    'iframe[src,alt,width,height,style,class,id];' +
                    'td[class,colspan,width,height,style];' +
                    'th[class,colspan,width,height,style];' +
                    'tr[class,style];' +
                    'h1[class,width,height,style,id];' +
                    'h2[class,width,height,style,id];' +
                    'h3[class,width,height,style,id];' +
                    'div[class,width,height,style,id];' +
                    'span[class,width,height,style,id];' +
                    'p[class,width,height,style,id];' +
                    'table[src,alt,width,height,style,id,class]'
                ),
                enterMode: Number(2),
                toolbar: [
                    [
                        'Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-',
                        'NumberedList', 'BulletedList'
                    ],
                    ['Table'],
                    ['Link', 'Unlink'],
                    ['Undo', 'Redo', '-', 'SelectAll'],
                    ['Maximize', 'Source']
                ]
            });

            __documentoModeloAdd.iniciarElementoDatePicker($("#modeloDataInicio,#modeloDataFim"));

            $("#modeloContexto").select2({minimumInputLength: -1, allowClear: true, language: 'pt-BR'});
        };

        this.setTdoc = function (tdoc) {
            this.options.value.tdoc = tdoc || null;
        };

        this.getTdoc = function () {
            return this.options.value.tdoc || null;
        };

        this.setCamp = function (camp) {
            this.options.value.camp = camp || null;
        };

        this.getCamp = function () {
            return this.options.value.camp || null;
        };

        this.setCurso = function (curso) {
            this.options.value.curso = curso || null;
        };

        this.getCurso = function () {
            return this.options.value.curso || null;
        };

        this.setArea = function (area) {
            this.options.value.area = area || null;
        };

        this.getArea = function () {
            return this.options.value.area || null;
        };


        this.setTipotitulo = function (tipotitulo) {
            this.options.value.tipotitulo = tipotitulo || null;
        };

        this.getTipotitulo = function () {
            return this.options.value.tipotitulo || null;
        };

        this.setGrupo = function (grupo) {
            this.options.value.grupo = grupo || null;
        };

        this.getGrupo = function () {
            return this.options.value.grupo || null;
        };

        this.setValidations = function () {
            __documentoModeloAdd.options.validator.settings.rules = {
                tdoc: {number: true, required: true}, camp: {number: true}, curso: {number: true}, area: {number: true},
                modeloDataInicio: {required: true, dateBR: true}, modeloDataFim: {required: true, dateBR: true},
                modeloDescricao: {maxlength: 150, required: true}, grupoId: {required: true}
            };
            __documentoModeloAdd.options.validator.settings.messages = {
                tdoc: {number: 'Número inválido!', required: 'Campo obrigatório!'}, camp: {number: 'Número inválido!'},
                curso: {number: 'Número inválido!'}, area: {number: 'Número inválido!'},
                modeloDataInicio: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                modeloDataFim: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                modeloDescricao: {maxlength: 'Tamanho máximo: 150!', required: 'Campo Obrigatório'}, grupoId: {required: 'Campo obrigatório!'}
            };

            $(__documentoModeloAdd.options.formElement).submit(function (e) {
                var ok = $(__documentoModeloAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__documentoModeloAdd.options.formElement);

                if (__documentoModeloAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __documentoModeloAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __documentoModeloAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__documentoModeloAdd.options.listagem) {
                                    $.documentoModeloIndex().reloadDataTableDocumentoModelo();
                                    __documentoModeloAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#documento-modelo-modal').modal('hide');
                                }

                                __documentoModeloAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaDocumentoModelo = function (modelo_id, callback) {
            var $form = $(__documentoModeloAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __documentoModeloAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + modelo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __documentoModeloAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __documentoModeloAdd.removeOverlay($form);
                },
                erro: function () {
                    __documentoModeloAdd.showNotificacaoDanger('Erro desconhecido!');
                    __documentoModeloAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.documentoModeloAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.documentos.documento-modelo.add");

        if (!obj) {
            obj = new DocumentoModeloAdd();
            obj.run(params);
            $(window).data('universa.documentos.documento-modelo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);