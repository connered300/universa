(function ($, window, document) {
    'use strict';

    var DocumentoGrupoAdd = function () {
        VersaShared.call(this);
        var __documentoGrupoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                acessoGrupo: '',
                documentoTipo: ''
            },
            data: {},
            value: {
                id: null,
                tdoc: null
            },
            datatables: {},
            wizardElement: '#documento-grupo-wizard',
            formElement: '#documento-grupo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#id").select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __documentoGrupoAdd.options.url.acessoGrupo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.grup_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__documentoGrupoAdd.getId()) {
                $('#id').select2("data", __documentoGrupoAdd.getId());
            }
            $("#tdoc").select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __documentoGrupoAdd.options.url.documentoTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.tdoc_id;
                            el.text = el.tdoc_descricao;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__documentoGrupoAdd.getTdoc()) {
                $('#tdoc').select2("data", __documentoGrupoAdd.getTdoc());
            }
            $("#docgrupoExigencia").select2({
                minimumInputLength: -1, allowClear: true, language: 'pt-BR'
            }).trigger("change");
            $("#docgrupoFormato").select2({
                minimumInputLength: -1, allowClear: true, language: 'pt-BR'
            }).trigger("change");
        };

        this.setId = function (id) {
            this.options.value.id = id || null;
        };

        this.getId = function () {
            return this.options.value.id || null;
        };

        this.setTdoc = function (tdoc) {
            this.options.value.tdoc = tdoc || null;
        };

        this.getTdoc = function () {
            return this.options.value.tdoc || null;
        };
        this.setValidations = function () {
            __documentoGrupoAdd.options.validator.settings.rules = {
                id: {number: true, required: true}, tdoc: {number: true, required: true},
                docgrupoExigencia: {maxlength: 11, required: true}, docgrupoFormato: {maxlength: 7, required: true},
                docgrupoDescricao: {maxlength: 45},
            };
            __documentoGrupoAdd.options.validator.settings.messages = {
                id: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                tdoc: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                docgrupoExigencia: {maxlength: 'Tamanho máximo: 11!', required: 'Campo obrigatório!'},
                docgrupoFormato: {maxlength: 'Tamanho máximo: 7!', required: 'Campo obrigatório!'},
                docgrupoDescricao: {maxlength: 'Tamanho máximo: 45!'},
            };

            $(__documentoGrupoAdd.options.formElement).submit(function (e) {
                var ok = $(__documentoGrupoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__documentoGrupoAdd.options.formElement);

                if (__documentoGrupoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __documentoGrupoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __documentoGrupoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__documentoGrupoAdd.options.listagem) {
                                    $.documentoGrupoIndex().reloadDataTableDocumentoGrupo();
                                    __documentoGrupoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#documento-grupo-modal').modal('hide');
                                }

                                __documentoGrupoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaDocumentoGrupo = function (docgrupo_id, callback) {
            var $form = $(__documentoGrupoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __documentoGrupoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + docgrupo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __documentoGrupoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __documentoGrupoAdd.removeOverlay($form);
                },
                erro: function () {
                    __documentoGrupoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __documentoGrupoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.documentoGrupoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.documentos.documento-grupo.add");

        if (!obj) {
            obj = new DocumentoGrupoAdd();
            obj.run(params);
            $(window).data('universa.documentos.documento-grupo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);