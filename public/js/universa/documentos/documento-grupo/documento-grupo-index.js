(function ($, window, document) {
    'use strict';
    var DocumentoGrupoIndex = function () {
        VersaShared.call(this);
        var __documentoGrupoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                documentoGrupo: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __documentoGrupoIndex.options.datatables.documentoGrupo = $('#dataTableDocumentoGrupo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __documentoGrupoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary documentoGrupo-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger documentoGrupo-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __documentoGrupoIndex.removeOverlay($('#container-documento-grupo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "docgrupo_id", targets: colNum++, data: "docgrupo_id"},
                    {name: "grup_nome", targets: colNum++, data: "grup_nome"},
                    {name: "tdoc_descricao", targets: colNum++, data: "tdoc_descricao"},
                    {name: "docgrupo_exigencia", targets: colNum++, data: "docgrupo_exigencia"},
                    {name: "docgrupo_formato", targets: colNum++, data: "docgrupo_formato"},
                    {name: "docgrupo_descricao", targets: colNum++, data: "docgrupo_descricao"},
                    {name: "docgrupo_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableDocumentoGrupo').on('click', '.documentoGrupo-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __documentoGrupoIndex.options.datatables.documentoGrupo.fnGetData($pai);

                if (!__documentoGrupoIndex.options.ajax) {
                    location.href = __documentoGrupoIndex.options.url.edit + '/' + data['docgrupo_id'];
                } else {
                    __documentoGrupoIndex.addOverlay(
                        $('#container-documento-grupo'), 'Aguarde, carregando dados para edição...'
                    );
                    $.documentoGrupoAdd().pesquisaDocumentoGrupo(
                        data['docgrupo_id'],
                        function (data) { __documentoGrupoIndex.showModal(data); }
                    );
                }
            });


            $('#btn-documento-grupo-add').click(function (e) {
                if (__documentoGrupoIndex.options.ajax) {
                    __documentoGrupoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableDocumentoGrupo').on('click', '.documentoGrupo-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __documentoGrupoIndex.options.datatables.documentoGrupo.fnGetData($pai);
                var arrRemover = {
                    docgrupoId: data['docgrupo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de grupo "' + data['docgrupo_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __documentoGrupoIndex.addOverlay(
                                $('#container-documento-grupo'), 'Aguarde, solicitando remoção de registro de grupo...'
                            );
                            $.ajax({
                                url: __documentoGrupoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __documentoGrupoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de grupo:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __documentoGrupoIndex.removeOverlay($('#container-documento-grupo'));
                                    } else {
                                        __documentoGrupoIndex.reloadDataTableDocumentoGrupo();
                                        __documentoGrupoIndex.showNotificacaoSuccess(
                                            "Registro de grupo removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#documento-grupo-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['docgrupoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['docgrupoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#id").select2('data', data['id']).trigger("change");
            $("#tdoc").select2('data', data['tdoc']).trigger("change");
            $("#docgrupoExigencia").select2('val', data['docgrupoExigencia']).trigger("change");
            $("#docgrupoFormato").select2('val', data['docgrupoFormato']).trigger("change");

            $('#documento-grupo-modal .modal-title').html(actionTitle + ' grupo');
            $('#documento-grupo-modal').modal();
            __documentoGrupoIndex.removeOverlay($('#container-documento-grupo'));
        };

        this.reloadDataTableDocumentoGrupo = function () {
            this.getDataTableDocumentoGrupo().api().ajax.reload(null, false);
        };

        this.getDataTableDocumentoGrupo = function () {
            if (!__documentoGrupoIndex.options.datatables.documentoGrupo) {
                if (!$.fn.dataTable.isDataTable('#dataTableDocumentoGrupo')) {
                    __documentoGrupoIndex.options.datatables.documentoGrupo = $('#dataTableDocumentoGrupo').DataTable();
                } else {
                    __documentoGrupoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __documentoGrupoIndex.options.datatables.documentoGrupo;
        };

        this.run = function (opts) {
            __documentoGrupoIndex.setDefaults(opts);
            __documentoGrupoIndex.setSteps();
        };
    };

    $.documentoGrupoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.documentos.documento-grupo.index");

        if (!obj) {
            obj = new DocumentoGrupoIndex();
            obj.run(params);
            $(window).data('universa.documentos.documento-grupo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);