(function ($, window, document) {
    'use strict';

    var ExemplarIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                url: {
                    empresa: '',
                    titulo: ''
                },
                data: {
                    grupoBibliografico: [],
                    exemplarAcesso: [],
                    pesquisa: false
                },
                value: {
                    grupoBibliografico: null,
                    exemplarAcesso: null
                }
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
            $.storage = new $.store();
            $('.close-modal').click(function () {
                $($(this).closest('.modal')).modal('hide');
            });
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="biblioteca-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.biblioteca-overlay').remove();
        };

        priv.showNotificacao = function (param) {
            var options = $.extend(true, {content: '', title: '', type: 'info', icon: '', color: '', timeout: false},
                                   param);

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: "fa " + options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        };

        priv.makeSelect2Field = function (settings) {
            settings = settings || [];
            settings = $.extend(true, {
                fieldId: '',
                optionsAttr: '',
                dataAttr: '',
                ajax: false,
                data: null,
                select2Extra: {},
                changeCallback: null
            }, settings);

            if (!settings.dataAttr) {
                settings.dataAttr = settings.optionsAttr;
            }

            return {
                settings: settings,
                setValue: function (option) {
                    option = option || null;
                    priv.options.value[this.settings.optionsAttr] = option;
                    $(this.settings.fieldId).select2("val", option);
                },
                setValueBySearch: function (search) {
                    var data = priv.options.data[this.settings.dataAttr] || null;

                    if (!data) {
                        return;
                    }

                    var selection = $.grep(data, function (el) {
                        if (el.id == search || el.text.toLowerCase() == search.toLowerCase()) {
                            return el;
                        }
                    });

                    if (!this.settings.select2Extra.multiple && !this.settings.select2Extra.tags) {
                        selection = selection[0] || null;
                    }

                    this.setValue(selection || null);
                },
                getValue: function () {
                    return priv.options.value[this.settings.optionsAttr];
                },
                init: function () {
                    var select2Helper = this;
                    var select2Options = {
                        language: 'pt-BR',
                        allowClear: true,
                        initSelection: function (element, callback) {
                            callback(select2Helper.getValue());
                        }
                    };

                    if (this.settings.ajax) {
                        select2Options['ajax'] = {url: '', dataType: 'json', delay: 250, data: null, results: ''};
                        select2Options['ajax'] = $.extend(true, select2Options['ajax'], this.settings.ajax);
                        delete select2Options['data'];
                    } else if (this.settings.data) {
                        select2Options['data'] = this.settings.data;
                        delete select2Options['ajax'];
                    } else {
                        delete select2Options['ajax'];
                        delete select2Options['data'];
                    }

                    if (this.settings.select2Extra) {
                        select2Options = $.extend(true, select2Options, this.settings.select2Extra);
                    }

                    $(this.settings.fieldId).select2(select2Options);

                    if (this.settings.select2Extra.allowClear) {
                        $(this.settings.fieldId).on('change select2-selected select2-clearing select2-removed',
                                                    function () {
                                                        var $item = $(this);

                                                        if ($item.val() != '') {
                                                            $item.parent().find('.select2-container').addClass('select2-allowclear');
                                                        } else {
                                                            $item.parent().find('.select2-container').removeClass('select2-allowclear');
                                                        }
                                                    });
                    }

                    if (this.settings.changeCallback) {
                        $(this.settings.fieldId).on('change', this.settings.changeCallback);
                    }
                    $(this.settings.fieldId).select2("val", select2Helper.getValue());
                    $(this.settings.fieldId).trigger('change');
                }
            };
        };

        priv.setFilters = function () {
            var grupoBibliografico = priv.makeSelect2Field({
                fieldId: '#grupoBibliografico',
                optionsAttr: 'grupoBibliografico',
                data: priv.options.data.grupoBibliografico,
                select2Extra: {allowClear: true, tags: true}
            });

            var editora = priv.makeSelect2Field({
                fieldId: '#editora', optionsAttr: 'editora', ajax: {
                    url: priv.options.url.empresa,
                    dataType: 'json', delay: 250,
                    data: function (query) { return {query: query, empresaTipo: 'Editora'};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome_fantasia;
                            el.id = el.pes_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var autor = priv.makeSelect2Field({
                fieldId: '#autor', optionsAttr: 'autor', ajax: {
                    url: priv.options.url.autor,
                    dataType: 'json', delay: 250,
                    data: function (query) { return {query: query};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.autor_referencia;
                            el.id = el.autor_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var assunto = priv.makeSelect2Field({
                fieldId: '#assunto', optionsAttr: 'assunto', ajax: {
                    url: priv.options.url.assunto,
                    dataType: 'json', delay: 250,
                    data: function (query) { return {query: query};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.assunto_descricao;
                            el.id = el.assunto_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var areaCnpq = priv.makeSelect2Field({
                fieldId: '#areaCnpq', optionsAttr: 'areaCnpq', ajax: {
                    url: priv.options.url.areaCnpq,
                    dataType: 'json', delay: 250,
                    data: function (query) { return {query: query};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_cnpq_codigo + ': ' + el.area_cnpq_descricao, id: el.area_cnpq_id};
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var exemplarAcesso = priv.makeSelect2Field({
                fieldId: '#exemplarAcesso',
                optionsAttr: 'exemplarAcesso',
                data: priv.options.data.exemplarAcesso,
                select2Extra: {allowClear: true, tags: true}
            });

            grupoBibliografico.init();
            editora.init();
            autor.init();
            assunto.init();
            areaCnpq.init();
            exemplarAcesso.init();

            var filtrosSalvos = $.storage.get('exemplar.listagem.filtros') || {};

            if (filtrosSalvos['grupoBibliografico']) {
                $('#grupoBibliografico').select2('data', filtrosSalvos['grupoBibliografico']);
            }
            if (filtrosSalvos['editora']) {
                $('#editora').select2('data', filtrosSalvos['editora']);
            }
            if (filtrosSalvos['autor']) {
                $('#autor').select2('data', filtrosSalvos['autor']);
            }
            if (filtrosSalvos['assunto']) {
                $('#assunto').select2('data', filtrosSalvos['assunto']);
            }
            if (filtrosSalvos['areaCnpq']) {
                $('#areaCnpq').select2('data', filtrosSalvos['areaCnpq']);
            }
            if (filtrosSalvos['exemplarAcesso']) {
                $('#exemplarAcesso').select2('data', filtrosSalvos['exemplarAcesso']);
            }
        };

        priv.formataDadosExemplarPopUp = function (item) {
            var limpaVar = function (valor) {
                valor = (typeof valor == 'undefined' ? '-' : valor);
                valor = (valor == null ? '-' : valor);
                valor = (valor == 'null' ? '-' : valor);
                valor = (valor == '' ? '-' : valor);

                return valor;
            };
            var html = '\
            <table border="0" width="100%" class="table table-striped table-hover col-sm-12">\
                <tr>\
                    <th class="text-right col-sm-2"><strong>Titulo:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['titulo_titulo']) + '</td>\
                    <th class="text-right col-sm-2"><strong>Código do título:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['titulo_id']) + '</td>\
                </tr>\
                <tr>\
                    <th class="text-right col-sm-2"><strong>Assunto(s):</strong></th>\
                    <td class="text-left col-sm-10" colspan="3">' + limpaVar(item['titulo_assuntos']) + '</td>\
                </tr>\
                <tr>\
                    <th class="text-right col-sm-2"><strong>Autor(es):</strong></th>\
                    <td class="text-left col-sm-10" colspan="3">' + limpaVar(item['autor_referencia']) + '</td>\
                </tr>\
                <tr>\
                    <th class="text-right col-sm-2"><strong>Editora:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['pes_nome_fantasia']) + '</td>\
                    <th class="text-right col-sm-2"><strong>ISBN:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['titulo_edicao_isbn']) + '</td>\
                </tr>\
                <tr>\
                    <th class="text-right col-sm-2"><strong>Ano Publicação:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['titulo_edicao_ano']) + '</td>\
                    <th class="text-right col-sm-2"><strong>Edição:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['titulo_edicao_numero']) + '</td>\
                </tr>\
                <tr>\
                    <th class="text-right col-sm-2"><strong>Volume:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['titulo_edicao_volume']) + '</td>\
                    <th class="text-right col-sm-2"><strong>Tomo:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['titulo_edicao_tomo']) + '</td>\
                </tr>\
                <tr>\
                    <th class="text-right col-sm-2"><strong>CDU/UDC/CDD:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['titulo_edicao_cdu']) + '</td>\
                    <th class="text-right col-sm-2"><strong>Cutter:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['titulo_edicao_cutter']) + '</td>\
                </tr>\
                <tr>\
                    <th class="text-right col-sm-2"><strong>Código do exemplar/Tombo:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['exemplar_id']) + '</td>\
                    <th class="text-right col-sm-2"><strong>Número do exemplar:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['exemplar_codigo']) + '</td>\
                </tr>\
                <tr>\
                    <th class="text-right col-sm-2"><strong>Acesso ao exemplar:</strong></th>\
                    <td class="text-left col-sm-4">' + limpaVar(item['exemplar_acesso']) + '</td>\
                    <th class="text-right col-sm-2"><strong>Emprestado:</strong></th>\
                    <td class="text-left col-sm-4">' + (item['exemplar_emprestado'] == 'S' ? 'Sim' : 'Não') + '</td>\
                </tr>\
                <tr>\
                    <th class="text-right col-sm-2"><strong>Exemplar baixado:</strong></th>\
                    <td class="text-left col-sm-10" colspan="3">' +
                       (item['exemplar_baixa_data'] != null ? 'Sim' : 'Não') + '</td>\
                </tr>\
            </table>';

            return html;
        };

        priv.formataValoresExemplar = function (item) {
            var titulo = '';

            titulo += '<b>' +
                      (item['exemplar_acesso'] == 'Kit Saraiva' ? '[Kit Saraiva] ' : '') +
                      item['titulo_titulo'] +
                      '</b>. ';
            titulo += item['autor_referencia'];
            titulo += ' / ';
            titulo += ' (';
            titulo += 'Grupo: ' + item['grupo_bibliografico_nome'];
            titulo += ', ';
            titulo += 'Título: ' + parseInt(item['titulo_id']);
            titulo += ', ';
            titulo += 'Exemplar: ' + item['exemplar_codigo'];
            titulo += ')';

            item['titulo_formatado'] = titulo;
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableTitulo = $('#dataTableTitulo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    data: function (d) {
                        d.filter = d.filter || {};
                        d.index = true;
                        var filtrosSalvos = {};
                        var grupoBibliografico = $('#grupoBibliografico').val() || "";
                        var editora = $('#editora').val() || "";
                        var autor = $('#autor').val() || "";
                        var assunto = $('#assunto').val() || "";
                        var areaCnpq = $('#areaCnpq').val() || "";
                        var exemplarAcesso = $('#exemplarAcesso').val() || "";
                        d.filter['emprestimo'] = true;

                        if (grupoBibliografico) {
                            d.filter['grupoBibliografico'] = grupoBibliografico;
                            filtrosSalvos['grupoBibliografico'] = $('#grupoBibliografico').select2('data');
                        }

                        if (editora) {
                            d.filter['editora'] = editora;
                            filtrosSalvos['editora'] = $('#editora').select2('data');
                        }

                        if (autor) {
                            d.filter['autor'] = autor;
                            filtrosSalvos['autor'] = $('#autor').select2('data');
                        }

                        if (assunto) {
                            d.filter['assunto'] = assunto;
                            filtrosSalvos['assunto'] = $('#assunto').select2('data');
                        }

                        if (areaCnpq) {
                            d.filter['areaCnpq'] = areaCnpq;
                            filtrosSalvos['areaCnpq'] = $('#areaCnpq').select2('data');
                        }

                        if (exemplarAcesso) {
                            d.filter['exemplarAcesso'] = exemplarAcesso;
                            filtrosSalvos['exemplarAcesso'] = $('#exemplarAcesso').select2('data');
                        }

                        $.storage.set('exemplar.listagem.filtros', filtrosSalvos);

                        return d;
                    },
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        for (var row in data) {
                            priv.formataValoresExemplar(data[row]);
                            var exemplarURL = priv.options.url.titulo + '/' + data[row]['titulo_id'];

                            if (!priv.options.data.pesquisa) {
                                var exemplarBaixado = data[row]['exemplar_baixa_data'] != null;
                                data[row].acao = (
                                    '<div class="text-center">\
                                        <div class="btn-group btn-group-xs text-center">\
                                            <a href="' + exemplarURL + '" class="btn btn-default" title="Editar título">\
                                            <i class="fa fa-book"></i>\
                                        </a>\
                                        <button type="button" class="btn btn-default exemplar-info" \
                                                title="Visualizar informações detalhadas">\
                                            <i class="fa fa-info"></i>\
                                        </button>' + (exemplarBaixado ? '' : '\
                                        <button type="button" class="btn btn-warning exemplar-baixa" \
                                                title="Baixa exemplar">\
                                            <i class="fa fa-arrow-down"></i>\
                                        </button>') + '\
                                    </div>\
                                </div>'
                                );
                            } else {
                                var exemplarBaixado = data[row]['exemplar_baixa_data'] != null;
                                var exemplarEmprestado = data[row]['exemplar_emprestado'] == 'S';
                                var exemplarRestrito = (data[row]['exemplar_acesso'] || '').toLowerCase() == 'restrito';
                                var disabledEmp = exemplarBaixado || exemplarEmprestado || exemplarRestrito;
                                var descricaoEmp = 'Emprestar';

                                if (exemplarEmprestado) {
                                    descricaoEmp = 'Emprestado';
                                } else if (exemplarBaixado) {
                                    descricaoEmp = 'Baixado';
                                } else if (exemplarRestrito) {
                                    descricaoEmp = 'Restrito';
                                }

                                data[row].acao = (
                                    '<div class="text-center">\
                                        <button type="button" class="btn btn-default exemplar-emprestimo"\
                                                ' + (disabledEmp ? ' disabled' : '') + '>\
                                            <i class="fa fa-level-down"></i> ' + descricaoEmp + '\
                                        </button>\
                                    </div>\
                                </div>'
                                );
                            }
                        }

                        return data;
                    }
                },
                columnDefs: [
                    {name: "exemplar_id", targets: colNum++, data: "exemplar_id"},
                    {name: "titulo_titulo", targets: colNum++, data: "titulo_formatado"},
                    {name: "titulo_id", targets: colNum++, data: "acao", orderable: false},
                    {name: "titulo_id", targets: colNum++, data: "titulo_id", visible: false, searchable: true},
                    {
                        name: "autor_referencia", targets: colNum++, data: "autor_referencia", visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_volume", targets: colNum++, data: "titulo_edicao_volume", visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_isbn", targets: colNum++, data: "titulo_edicao_isbn", visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_cdu", targets: colNum++, data: "titulo_edicao_cdu", visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_cutter", targets: colNum++, data: "titulo_edicao_cutter", visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_ano", targets: colNum++, data: "titulo_edicao_ano", visible: false,
                        searchable: true
                    },
                    {
                        name: "pes_nome_fantasia", targets: colNum++, data: "pes_nome_fantasia", visible: false,
                        searchable: true
                    }
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[1, 'desc']],
                pageLength: (priv.options.data.pesquisa ? 5 : 10)
            });

            $('#dataTableTitulo').on('click', '.exemplar-info', function () {
                var pai = $(this).closest('tr');
                var exemplarData = priv.dataTableTitulo.fnGetData(pai);
                var html = priv.formataDadosExemplarPopUp(exemplarData);

                $('#exemplar-info .modal-body').html(html);
                $('#exemplar-info').modal('show');
            });

            $('#dataTableTitulo').on('click', '.exemplar-emprestimo', function () {
                var pai = $(this).closest('tr');
                var exemplarData = priv.dataTableTitulo.fnGetData(pai);
                $.emprestimo().selecionaItemEmprestimo(exemplarData);

                $('.modal:visible').modal('hide');
            });


            $('#dataTableTitulo').on('click', '.exemplar-baixa', function (e) {
                var $pai = $(this).closest('tr');
                var exemplarData = priv.dataTableTitulo.fnGetData($pai);

                var efetuaBaixa = function (acao) {
                    acao = acao || 'Baixar';
                    var requisicao = function (motivo) {
                        exemplarData['exemplar_baixa_data'] = $.formatDateTime('yy-mm-dd hh:ii', new Date());
                        exemplarData['exemplar_baixa_motivo'] = motivo;
                        exemplarData['acao'] = acao;

                        priv.addOverlay($('#widget-grid'), 'Aguarde, efetuando ação: ' + acao + '.');

                        $.ajax({
                            url: priv.options.urlBaixa,
                            method: 'POST',
                            dataType: 'json',
                            data: exemplarData,
                            success: function (data) {
                                if (data.erro) {
                                    priv.showNotificacao({timeout: 10000, type: 'danger', content: data.erroDescricao});
                                } else {
                                    priv.showNotificacao({
                                        timeout: 10000, type: 'success',
                                        content: 'Ação "' + acao + '" efetuada com sucesso.'
                                    });
                                }

                                priv.removeOverlay($('#widget-grid'));

                                setTimeout(function () {
                                    pub.atualizarListagem();
                                }, 600);
                            }
                        });
                    };

                    if (acao.toLowerCase() == 'devolver') {
                        requisicao('');
                    } else {
                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Deseja realmente dar baixa neste exemplar?<br>Digite o motivo abaixo:',
                            buttons: "[Cancelar][Ok]",
                            input: "text",
                            placeholder: "Motivo da baixa.",
                            inputValue: ""
                        }, function (ButtonPress, motivo) {
                            if (!motivo) {
                                priv.showNotificacao({
                                    timeout: 10000, type: 'danger', content: 'O motimo da baixa é obrigatório!'
                                });
                                return false;
                            }

                            if (ButtonPress == "Ok") {
                                requisicao(motivo);
                            }
                        });
                    }
                };

                if (exemplarData['exemplar_emprestado'] == 'S') {
                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Este exmplar está emprestado. O que deseja fazer?',
                        buttons: "[Cancelar][Devolver e Baixar][Baixar][Devolver]"
                    }, function (ButtonPress) {
                        if (ButtonPress != "Cancelar") {
                            setTimeout(function () {
                                efetuaBaixa(ButtonPress);
                            }, 600);
                        }
                    });
                } else {
                    efetuaBaixa();
                }

                e.preventDefault();
            });

            $("#titulosFiltroExecutar").click(function (event) {
                pub.atualizarListagem();
            });

            $(".panel-title").click(function (event) {
                $(".panel-title .fa").toggleClass('fa-chevron-right');
                $(".panel-title .fa").toggleClass('fa-chevron-down');
            });

            $("#titulosFiltroLimpar").click(function (event) {
                $('#grupoBibliografico').val('').trigger('change');
                $('#editora').val('').trigger('change');
                $('#autor').val('').trigger('change');
                $('#assunto').val('').trigger('change');
                $('#areaCnpq').val('').trigger('change');
                $('#exemplarAcesso').val('').trigger('change');
            });
        };

        pub.atualizarListagem = function () {
            priv.dataTableTitulo.api().ajax.reload(null, false);
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setFilters();
            priv.setSteps();
        };
    };

    $.exemplarIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.exemplar.index");

        if (!obj) {
            obj = new ExemplarIndex();
            obj.run(params);
            $(window).data('universa.exemplar.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);