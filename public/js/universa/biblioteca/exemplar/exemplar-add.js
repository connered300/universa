(function ($, window, document) {
    'use strict';

    var ExemplarAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                url: {
                    setor: '',
                    localizacao: ''
                },
                value: {
                    setor: ''
                },
                tituloScreen: false
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {
            $("#exemplarAcesso").select2({
                language: 'pt-BR'
            });
            $("#numeroDeExemplares, #exemplarCodigoNovo").inputmask({
                showMaskOnHover: false, clearIncomplete: true, alias: 'integer'
            });
            $("#setor").select2({
                language: 'pt-BR',
                ajax: {
                    url: priv.options.url.setor,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el['setor_descricao'] + '(' + el['setor_sigla'] + ')', id: el.setor_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(pub.getSetor());
                }
            });

            $('#setor').select2("val", pub.getSetor());

            $('#exemplarLocalizacao').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: priv.options.url.localizacao,
                        data: {query: request.term, local: true},
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            var transformed = $.map(data || [], function (el) {
                                el.label = el.exemplar_localizacao;

                                return el;
                            });

                            response(transformed);
                        }
                    });
                },
                minLength: 2
            });

            $('#exemplarId,#exemplarCodigo,#exemplarEmprestado,#exemplarReservado,#exemplarBaixaData').on('change', function () {
                var $this = $(this);
                var id = $this.attr('id');
                var $label = $('#' + id + 'Label');
                var value = $this.val() || '';
                var valueLabel = value;

                if (id == 'exemplarEmprestado' || id == 'exemplarReservado') {
                    value = (value == 'S' ? 'S' : 'N');
                    valueLabel = (value == 'S' ? 'Sim' : 'Não');
                } else if (id == 'exemplarCodigo') {
                    value = (value == '' ? '1' : value);
                    valueLabel = value;
                } else if (id == 'exemplarBaixaData') {
                    $('#exemplarBaixaMotivo').prop('disabled', (value == ''));
                    valueLabel = (value == '' ? '-' : $.formatDateTime('dd/mm/yy hh:ii', new Date(value)));
                }

                $this.val(value);
                $label.html(valueLabel || '-');
            }).trigger('change');
        };

        pub.setSetor = function (setor) {
            priv.options.value.setor = setor || null;
        };

        pub.getSetor = function () {
            return priv.options.value.setor;
        };

        priv.setValidations = function () {
            $("#exemplar-form").validate({
                submitHandler: function (form) {
                    if (priv.options.tituloScreen) {
                        var dados = {};
                        dados["exemplar_id"] = $("#exemplarId").val();
                        dados["exemplar_codigo"] = $("#exemplarCodigo").val();
                        dados["exemplar_codigo_novo"] = $("#exemplarCodigoNovo").val();
                        dados["exemplar_emprestado"] = $("#exemplarEmprestado").val();
                        dados["exemplar_reservado"] = $("#exemplarReservado").val();
                        dados["exemplar_acesso"] = $("#exemplarAcesso").val();
                        dados["setor_id"] = $("#setor").select2('data').id;
                        dados["setor_descricao"] = $("#setor").select2('data').text;
                        dados["exemplar_localizacao"] = $("#exemplarLocalizacao").val();
                        dados["exemplar_baixa_data"] = $("#exemplarBaixaData").val();
                        dados["exemplar_baixa_motivo"] = $("#exemplarBaixaMotivo").val();
                        dados["numero_de_exemplares"] = $("#numeroDeExemplares").val();

                        $.tituloAdd().editarExemplarItem(dados);
                        $('#novo-exemplar').modal('hide');
                        return false;
                    } else {
                        form.submit();
                    }
                },
                rules: {
                    exemplarCodigo: 'required',
                    exemplarEmprestado: 'required',
                    exemplarReservado: 'required',
                    exemplarAcesso: 'required',
                    setor: 'required',
                    numeroDeExemplares: {
                        required: function () {
                            return $('#numeroDeExemplares').is(':visible') != "";
                        }
                    },
                    exemplarBaixaMotivo: {
                        required: function () {
                            return $('#exemplarBaixaData').val() != "";
                        }
                    }
                },
                messages: {
                    exemplarCodigo: 'Campo obrigatório!',
                    exemplarEmprestado: 'Campo obrigatório!',
                    exemplarReservado: 'Campo obrigatório!',
                    exemplarAcesso: 'Campo obrigatório!',
                    setor: 'Campo obrigatório!',
                    exemplarBaixaMotivo: 'Campo obrigatório!',
                    numeroDeExemplares: 'Campo obrigatório!'
                },
                ignore: '.ignore'
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.exemplarAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.exemplar.add");

        if (!obj) {
            obj = new ExemplarAdd();
            obj.run(params);
            $(window).data('universa.exemplar.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);