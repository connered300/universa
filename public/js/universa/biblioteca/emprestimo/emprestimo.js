(function ($, window, document) {
    'use strict';

    var Emprestimo = function () {
        var pub = this;
        var versaShared = new VersaShared();
        var priv = {
            options: {
                url: {
                    pessoa: '',
                    leitorInformacao: '',
                    modalidade: '',
                    devolucao: '',
                    historico: '',
                    exemplar: '',
                    emprestimoListagem: '',
                    emprestimoAcao: '',
                    thumbnail: '',
                    emprestimoComprovante: ''
                },
                datatable: {
                    emprestimo: null,
                    devolucao: null,
                    historico: null,
                    emprestimosAtivos: null
                },
                data: {
                    leitor: {},
                    modalidades: {},
                    exemplar: {},
                    emprestimos: {}
                },
                dataParam: {
                    leitor: [],
                    modalidadeGrupoBibliografico: [],
                    grupoBibliograficoInserido: [],
                    modalidades: []
                },
                value: {
                    dataAtual: '',
                    diasParaRenovacao: '',
                    quantidadeDeRenovacoes: ''
                }
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });

            $.storage = new $.store();

            //Leitora de código de barras
            $(document).on('keydown', '.select2-input, .ui-autocomplete-input, .form-control', function (e) {
                if (e.ctrlKey && (e.which == 74)) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }
            });

            $('.close-modal').click(function () {
                $($(this).closest('.modal')).modal('hide');
            });
        };

        priv.showNotificacao = function (param) {
            var options = $.extend(true, {content: '', title: '', type: 'info', icon: '', color: '', timeout: false},
                param);

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: "fa " + options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        };

        priv.numPad = function (n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="emprestimo-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('.emprestimo-overlay').remove();
        };

        priv.chkDiaUtil = function (objDate) {
            var chkUtil = objDate.getDay() == 0 || objDate.getDay() == 6;

            if (!chkUtil) {
                chkUtil = $.inArray($.formatDateTime('yy-mm-dd', objDate), priv.options.data.leitor.feriados) != -1;
            }

            return chkUtil;
        };

        priv.recarregarInformacoesLeitor =
            function (manterDatatableEmprestimo, recarregarEmprestimosAtivos, naoPerguntar, parametros, emprestimoId) {
                manterDatatableEmprestimo = manterDatatableEmprestimo || false;
                recarregarEmprestimosAtivos = recarregarEmprestimosAtivos || false;
                naoPerguntar = naoPerguntar || false;
                emprestimoId = emprestimoId || false;

                if (typeof manterDatatableEmprestimo == 'object') {
                    manterDatatableEmprestimo = false;
                    $('#pesquisaLeitor').data('confirmacao', true);
                }

                var arrData = $('#pesquisaLeitor').select2('data') || {};
                var confirmacao = $('#pesquisaLeitor').data('confirmacao');
                confirmacao = typeof confirmacao == 'undefined' ? true : confirmacao;

                if (arrData['pesId'] && !confirmacao && !naoPerguntar) {
                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Deseja continuar trabalhando com o leitor "' + arrData['pesNome'] + '"?',
                        buttons: "[Não][Sim]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Sim") {
                            $('#pesquisaLeitor').data('confirmacao', true);
                        } else {
                            $('#pesquisaLeitor').select2('data', null);
                            $('#pesquisaLeitor').select2('focus');
                            $('#pesquisaLeitor').data('confirmacao', false);
                        }

                        if (emprestimoId) {
                            $.SmartMessageBox({
                                title: "Confirme a operação:",
                                content: 'Deseja gerar comprovante de empréstimo?',
                                buttons: "[Cancelar][Ok]"
                            }, function (ButtonPress) {
                                if (ButtonPress == "Ok") {
                                    var $modal = $('#emprestimo-comprovante-modal');
                                    var url = priv.options.url.emprestimoComprovante + '?emprestimoId=' + emprestimoId;

                                    versaShared.addOverlay($modal);
                                    $('#corpo-recibo-modal').attr('src', url);
                                    $modal.modal('show');
                                }
                            });
                        }
                        setTimeout(function () {
                            $("#renovarEmprestimo").prop("disabled", true);
                            priv.recarregarInformacoesLeitor(manterDatatableEmprestimo, recarregarEmprestimosAtivos);
                        }, 600);
                    });

                    return;
                }

                $('#pesquisaLeitor').data('confirmacao', false);
                $('#leitorAcoes')[arrData['pesId'] ? 'removeClass' : 'addClass']('hidden');
                $('#leitorAcoesAviso')[!arrData['pesId'] ? 'removeClass' : 'addClass']('hidden');

                priv.options.data.leitor = arrData;

                if (arrData['pesId']) {
                    priv.carregarInformacoesLeitor();
                    priv.carregarInformacaoEmprestimo(manterDatatableEmprestimo);
                }

                if (priv.options.datatable.devolucao) {
                    priv.addOverlay($('#leitorEmprestimoDevolucao'),
                        'Aguarde, carregando informação dos empréstimos atuais do leitor.');
                    priv.options.datatable.devolucao.api().ajax.reload(null, false);
                }

                if (priv.options.datatable.historico) {
                    priv.addOverlay($('#leitorEmprestimoHistorico'),
                        'Aguarde, carregando histórico de empréstimo do leitor.');
                    priv.options.datatable.historico.api().ajax.reload(null, false);
                }

                if (recarregarEmprestimosAtivos && priv.options.datatable.emprestimosAtivos) {
                    priv.addOverlay($('#emprestimosAtivos'), 'Aguarde, carregando informação dos empréstimos ativos.');
                    priv.options.datatable.emprestimosAtivos.api().ajax.reload(null, false);
                }
            };

        priv.formataValoresExemplar = function (item) {
            var titulo = '', atraso = '', data_previsao = '', data_entrega = '', data = '', leitor = '';

            titulo += '<b>' +
                (item['exemplar_acesso'] == 'Kit Saraiva' ? '[Kit Saraiva] ' : '') +
                item['titulo_titulo'] +
                '</b>. ';
            titulo += item['autor_referencia'];
            titulo += ' / ';
            titulo += ' (';
            titulo += 'Grupo: ' + item['grupo_bibliografico_nome'];
            titulo += ', ';
            titulo += 'Título: ' + parseInt(item['titulo_id']);
            titulo += ', ';
            titulo += 'Exemplar: ' + item['exemplar_codigo'];
            titulo += ')';
            atraso = item['emprestimo_atraso'] == 'S' ? 'Sim' : 'Não';
            data = new Date(item['emprestimo_data']);
            data = $.formatDateTime('dd/mm/yy', data);
            data_previsao = new Date(item['emprestimo_devolucao_data_previsao']);
            data_previsao = $.formatDateTime('dd/mm/yy', data_previsao);
            data_entrega = item['emprestimo_devolucao_data_efetuada'] || false;
            data_entrega = data_entrega ? new Date(data_entrega) : false;
            data_entrega = (data_entrega ? $.formatDateTime('dd/mm/yy hh:ii', data_entrega) : '-');
            leitor = parseInt(item['leitor_id']) + ' - <b>' + item['leitor_nome'] + '</b>';

            item['titulo_f'] = titulo;
            item['atraso_f'] = atraso;
            item['data_f'] = data;
            item['data_previsao_f'] = data_previsao;
            item['data_entrega_f'] = data_entrega;
            item['leitor_f'] = leitor;
        };

        priv.pesquisaUsuario = function () {
            $('#pesquisaLeitor').select2({
                allowClear: true,
                language: 'pt-BR',
                ajax: {
                    url: priv.options.url.pessoa,
                    dataType: 'json', delay: 0,
                    data: function (query) {
                        return {query: query, leitor: true, cursoAtivo: true};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            var matricula = parseInt(el.alunocurso_id) || 0;
                            var label = '';
                            label += 'Código: ';
                            label += priv.numPad(el.pes_id, 10, '0');

                            if (matricula) {
                                label += ' | Matrícula: ';
                                label += priv.numPad(matricula, 10, '0');
                            }

                            label += ' | Nome: ';
                            label += el.pes_nome;

                            el.text = label;
                            el.id = el.pes_id;
                            el.pesId = el.pes_id;
                            el.grupoLeitorId = el.grupo_leitor_id;
                            el.pesNome = el.pes_nome;
                            el.alunocursoId = matricula ? matricula : '';

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                formatSelection: function (el) {
                    return el.pes_nome;
                },
                minimumInputLength: 1
            });

            $('#pesquisaLeitor').on('change', priv.recarregarInformacoesLeitor);

        };

        priv.pesquisaModalidadeEmprestimo = function () {
            $('#modalidadeEmprestimo').select2({
                allowClear: true,
                language: 'pt-BR',
                data: function () {
                    var arrModalidades = $.map(priv.options.data.modalidades, function (modalidade) {
                        modalidade.id = modalidade.modalidadeEmprestimoId;
                        modalidade.text = modalidade.modalidadeEmprestimoDescricao;

                        return modalidade;
                    });

                    return {results: arrModalidades};
                }
            });

            $('#modalidadeEmprestimo').on('change', function (event) {
                var $modalidadeEmprestimo = $(this);
                var $modalidadeEmprestimoGrupoRegras = $('#modalidadeEmprestimoGrupoRegras tbody');
                $modalidadeEmprestimoGrupoRegras.html('');

                $('#exemplarListagemBtn, #exemplarPesquisa').prop('disabled', true);

                if (Object.keys(priv.options.data.modalidades).length == 0) {
                    $modalidadeEmprestimoGrupoRegras
                        .append('<tr><td colspan="6" class="text-center">O grupo do leitor não possui modalidades de empréstimo!</td></tr>');
                    return;
                }

                var modalidade = $modalidadeEmprestimo.val() || '';

                if (!modalidade) {
                    $modalidadeEmprestimoGrupoRegras
                        .append('<tr><td colspan="6" class="text-center">Selecione uma modalidade!</td></tr>');
                    return;
                }

                modalidade = priv.options.data.modalidades[modalidade];
                var grupos = modalidade['modalidadeEmprestimoGrupo'] || {};

                if (Object.keys(grupos).length == 0) {
                    $modalidadeEmprestimoGrupoRegras
                        .append('<tr><td colspan="6" class="text-center">Sem regras para esta modalidade!</td></tr>');
                    return;
                }

                $('#exemplarListagemBtn, #exemplarPesquisa').prop('disabled', false);
                priv.options.dataParam.modalidadeGrupoBibliografico = [];

                $.each(grupos, function (i, grupo) {
                    var modalidadeEmprestimoNumeroVolumes = grupo['modalidadeEmprestimoNumeroVolumes'] || 0;
                    var modalidadeEmprestimoPrazoMaximo = grupo['modalidadeEmprestimoPrazoMaximo'] || 0;
                    var emprestimoData = new Date();
                    var emprestimoPrevisaoEntrega = new Date();
                    var leitorNumeroItensEmprestados = priv.options.data.leitor['leitorNumeroItensEmprestados'] || {};
                    var itensEmprestimoGrupo = {};

                    for (var i in leitorNumeroItensEmprestados) {
                        if (leitorNumeroItensEmprestados[i]['grupoBibliograficoId'] == grupo['grupoBibliograficoId']) {
                            itensEmprestimoGrupo = leitorNumeroItensEmprestados[i];
                            priv.options.dataParam.modalidadeGrupoBibliografico.push(grupo['grupoBibliograficoId']);
                        }
                    }

                    emprestimoPrevisaoEntrega.setDate(emprestimoPrevisaoEntrega.getDate() +
                        modalidadeEmprestimoPrazoMaximo);

                    while (priv.chkDiaUtil(emprestimoPrevisaoEntrega)) {
                        emprestimoPrevisaoEntrega.setDate(emprestimoPrevisaoEntrega.getDate() + 1);
                    }

                    var $line = $('<tr/>');
                    $line.append($('<td/>').html(grupo['grupoBibliograficoNome'] || '-'));

                    if (modalidadeEmprestimoPrazoMaximo >= 0) {
                        var modalidadeEmprestimoNumeroVolumesRestantes = modalidadeEmprestimoNumeroVolumes -
                            itensEmprestimoGrupo['grupoBibliograficoQtd'];

                        if (modalidadeEmprestimoNumeroVolumesRestantes == 0 && modalidadeEmprestimoPrazoMaximo > 0) {
                            priv.showNotificacao({
                                timeout: 10000,
                                type: 'info',
                                content: 'Este usuário atingiu o limite de exemplares de "' +
                                grupo['grupoBibliograficoNome'] + '" permitido nesta modalidade.'
                            });
                        }

                        $line.append($('<td/>').html(modalidadeEmprestimoNumeroVolumes));
                        $line.append($('<td/>').html('<b>' + modalidadeEmprestimoNumeroVolumesRestantes + '</b>'));
                        $line.append($('<td/>').html(
                            modalidadeEmprestimoPrazoMaximo ?
                            modalidadeEmprestimoPrazoMaximo + ' Dia' +
                            (modalidadeEmprestimoPrazoMaximo > 1 ? 's' : '') :
                                '-'
                        ));
                        $line.append($('<td/>').html(
                            modalidadeEmprestimoPrazoMaximo ? $.datepicker.formatDate("dd/mm/yy", emprestimoData) : '-'
                        ));
                        $line.append($('<td/>').html(
                            modalidadeEmprestimoPrazoMaximo ? $.datepicker.formatDate("dd/mm/yy",
                                emprestimoPrevisaoEntrega) : '-'
                        ));
                    }

                    $modalidadeEmprestimoGrupoRegras.append($line);
                });

                var strTitulosRemover = [];
                var titulosRemover = [];

                for (var i in priv.options.dataParam.grupoBibliograficoInserido) {
                    var numExemplarGrupoInserido = priv.options.dataParam.grupoBibliograficoInserido[i];
                    var modalidadeTemGrupo = $.inArray(parseInt(i),
                        priv.options.dataParam.modalidadeGrupoBibliografico);

                    if (numExemplarGrupoInserido > 0 && modalidadeTemGrupo == -1) {
                        $.each(priv.options.data.exemplar, function (tituloId, exemplar) {
                            if (parseInt(exemplar['grupo_bibliografico_id']) == parseInt(i)) {
                                strTitulosRemover.push(exemplar['titulo_f']);
                                titulosRemover.push(tituloId);
                            }
                        });
                    }
                }

                if (strTitulosRemover.length > 0) {
                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Os exemplares abaixo não podem ser emprestados nesta modalidade, deseja removelos?<br>' +
                        strTitulosRemover.join('<br>'),
                        buttons: "[Cancelar][Ok]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Ok") {
                            $('#dataTableEmprestimo tbody tr').each(function (i, line) {
                                var exemplarData = priv.options.datatable.emprestimo.fnGetData(line);

                                if (!exemplarData) {
                                    return;
                                }

                                if ($.inArray(exemplarData['titulo_id'], titulosRemover) != -1) {
                                    delete priv.options.data.exemplar[exemplarData['titulo_id']];
                                    priv.options.dataParam.grupoBibliograficoInserido[exemplarData['grupo_bibliografico_id']]--;

                                    $('#emprestimoExemplarRemoveTodos,#emprestimoExemplarEmprestar')
                                        .prop('disabled', Object.keys(priv.options.data.exemplar).length == 0);
                                    priv.options.datatable.emprestimo.api().row(line).remove().draw();
                                }
                            });
                        } else {
                            $modalidadeEmprestimo.select2('data', event.removed || '');
                            $modalidadeEmprestimo.trigger('change');
                        }
                    });
                }
            });
        };

        priv.titulosQueNaoPodemSerEmprestados = function () {
            var leitorTitulosExemplaresEmPoder = (
                priv.options.data.leitor['leitorTitulosExemplaresEmPoder'] || {}
            );
            var titulosIgnorar = $.merge(
                Object.keys(priv.options.data.exemplar),
                Object.keys(leitorTitulosExemplaresEmPoder)
            );
            return {
                todos: titulosIgnorar,
                aemprestar: Object.keys(priv.options.data.exemplar),
                empoder: Object.keys(leitorTitulosExemplaresEmPoder)
            };
        };

        priv.pesquisaExemplaresDisponiveis = function () {
            $('#exemplarPesquisa').autocomplete({
                source: function (request, response) {
                    var grupos = priv.options.dataParam.modalidadeGrupoBibliografico.join(',');
                    var titulosIgnorar = priv.titulosQueNaoPodemSerEmprestados();

                    $.ajax({
                        url: priv.options.url.exemplar,
                        data: {
                            query: request.term,
                            grupoBibliograficoId: grupos,
                            emprestimo: true,
                            tituloIgnorar: titulosIgnorar['todos']
                        },
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            var transformed = $.map(data || [], function (el) {
                                el.label = '';
                                el.label += priv.numPad(el.titulo_id, 10, '0') + ' | ';
                                el.label += priv.numPad(el.exemplar_id, 10, '0') + ' | ';
                                el.label += priv.numPad(el.exemplar_codigo, 3, '0') + ' | ';
                                el.label += el.titulo_titulo + ' | ';
                                el.label += el.autor_referencia;

                                return el;
                            });

                            if (Object.keys(transformed).length == 0) {
                                transformed.push({label: '-'});
                            }

                            response(transformed);
                        }
                    });
                },
                select: function (event, ui) {
                    $("#exemplarPesquisa").val('');
                    pub.selecionaItemEmprestimo(ui.item);
                    return false;
                },
                minLength: 1
            });

            var dataAutocomplate = $('#exemplarPesquisa').data("autocomplete") ||
                $('#exemplarPesquisa').data("ui-autocomplete");

            dataAutocomplate._renderItem = function (ul, el) {
                if (el.label == '-') {
                    return $('<li class="ui-menu-item-header">').append('Sem resultados para a pesquisa.').appendTo(ul);
                }

                var label = '';
                var classItem = 'ui-menu-item';
                var descricaoIndisponibilidade = '';

                if (el.exemplar_disponibilidade == 'N') {
                    classItem += ' ui-state-disabled';
                    descricaoIndisponibilidade = '[Exemplar ' + el.exemplar_disponibilidade_descricao + '] ';
                }

                if (ul.find('.ui-menu-item-header').length == 0) {
                    label += '<span class="autocomplete-part-10">Cód. do título</span>';
                    label += '<span class="autocomplete-part-10">Cód. do Exemplar/Tombo</span>';
                    label += '<span class="autocomplete-part-10">Núm. do Exemplar</span>';
                    label += '<span class="autocomplete-part-45">Título</span>';
                    label += '<span class="autocomplete-part-25">Autores</span>';

                    $('<li class="ui-menu-item-header">').append(label).appendTo(ul);

                }

                label = '<span class="autocomplete-part-10">' + priv.numPad(el.titulo_id, 10, '0') + '</span>';
                label += '<span class="autocomplete-part-10">' + priv.numPad(el.exemplar_id, 10, '0') + '</span>';
                label += '<span class="autocomplete-part-10">' + priv.numPad(el.exemplar_codigo, 3, '0') + '</span>';
                label += '<span class="autocomplete-part-45">' +
                    (el.exemplar_acesso == 'Kit Saraiva' ? '[Kit Saraiva] ' : '') +
                    descricaoIndisponibilidade +
                    el.grupo_bibliografico_nome +
                    ': ' +
                    el.titulo_titulo +
                    '</span>';
                label += '<span class="autocomplete-part-25">' + el.autor_referencia + '</span>';

                return $('<li class="' + classItem + '">')
                    .append('<a>' + label + '</a>')
                    .appendTo(ul);
            };

            $('#exemplarListagemBtn').click(function () {
                $.exemplarIndex().atualizarListagem();
                $('#exemplar-pesquisa-avancada').modal('show');
            });
        };

        pub.selecionaItemEmprestimo = function (item) {
            var titulosIgnorar = priv.titulosQueNaoPodemSerEmprestados();
            var msg = '';
            var emprestar = false;
            var exemplarBaixado = item['exemplar_baixa_data'] != null;
            var exemplarEmprestado = item['exemplar_emprestado'] == 'S';
            var exemplarRestrito = (item['exemplar_acesso'] || '').toLowerCase() == 'restrito';

            if (exemplarEmprestado) {
                msg = 'Exemplar encontra-se emprestado.';
            } else if (exemplarBaixado) {
                msg = 'Exemplar encontra-se baixado.';
            } else if (exemplarRestrito) {
                msg = 'Exemplar encontra-se com acesso restrito.';
            } else if (titulosIgnorar['empoder'].indexOf(item['titulo_id']) != -1) {
                msg = 'O aluno já está com um exemplar deste livro em poder.';
            } else if (titulosIgnorar['aemprestar'].indexOf(item['titulo_id']) != -1) {
                msg = 'Já existe um exemplar deste livro para efetivar o empréstimo.';
            } else {
                emprestar = true;
            }

            if (!emprestar) {
                priv.showNotificacao({timeout: 15000, type: 'warning', content: msg});

                return;
            }

            priv.formataValoresExemplar(item);
            item['item_remove'] =
                '<button type="button" class="btn btn-xs btn-warning emprestimoItemRemove">' +
                '<i class="fa fa-trash-o"></i>' +
                '</button>';
            priv.options.data.exemplar[item['titulo_id']] = item;
            priv.options.dataParam.grupoBibliograficoInserido[item['grupo_bibliografico_id']]++;

            $('#emprestimoExemplarRemoveTodos, #emprestimoExemplarEmprestar')
                .prop('disabled', Object.keys(priv.options.data.exemplar).length == 0);

            priv.options.datatable.emprestimo.api().row.add(item).draw(false);
        };

        priv.limpaDatatableEmprestimo = function () {
            $('#dataTableEmprestimo tbody tr').each(function (i, line) {
                var exemplarData = priv.options.datatable.emprestimo.fnGetData(line);

                if (!exemplarData) {
                    return;
                }

                delete priv.options.data.exemplar[exemplarData['titulo_id']];
                priv.options.dataParam.grupoBibliograficoInserido[exemplarData['grupo_bibliografico_id']]--;

                $('#emprestimoExemplarRemoveTodos, #emprestimoExemplarEmprestar')
                    .prop('disabled', Object.keys(priv.options.data.exemplar).length == 0);
                priv.options.datatable.emprestimo.api().row(line).remove().draw();
            });
        };

        priv.acaoBotaoEmprestimo = function () {
            $('#emprestimoExemplarRemoveTodos').click(function () {
                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover <b>TODOS</b>  or exemplares?',
                    buttons: "[Cancelar][Ok]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Ok") {
                        priv.limpaDatatableEmprestimo();
                    }
                });
            });

            $('#emprestimoExemplarEmprestar').click(function () {

                var parametros = {
                    acao: 'emprestar',
                    pesId: priv.options.data.leitor.pesId,
                    modalidadeEmprestimoId: $('#modalidadeEmprestimo').val(),
                    senha: '',
                    isencaoSenha: 1,
                    exemplarId: []
                };

                $('#dataTableEmprestimo tbody tr').each(function (i, line) {
                    var exemplarData = priv.options.datatable.emprestimo.fnGetData(line);

                    if (!exemplarData) {
                        return;
                    }

                    if (parametros['isencaoSenha'] && exemplarData['exemplar_acesso'] != 'Kit Saraiva') {
                        parametros['isencaoSenha'] = 0;
                    }

                    parametros.exemplarId.push(exemplarData['exemplar_id']);
                });

                parametros['exemplarId'] = parametros['exemplarId'].join(',');

                if (parametros['isencaoSenha']) {
                    priv.efetuarEmprestimo(parametros);
                } else {
                    $.SmartMessageBox({
                        title: "Confirmação do usuário:",
                        content: 'Por favor digite a senha do <b>leitor</b> abaixo:',
                        buttons: "[Cancelar][Ok]",
                        input: "password",
                        placeholder: "Senha do leitor",
                        inputValue: ""
                    }, function (ButtonPress, senhaLeitor) {
                        if (ButtonPress == "Ok") {
                            if (!senhaLeitor) {
                                priv.showNotificacao({
                                    timeout: 5000,
                                    type: 'warning',
                                    content: 'Para efetuar o empréstimo é necessário que o leitor informe uma senha válida!'
                                });

                                return;
                            }

                            parametros['senha'] = senhaLeitor;

                            priv.efetuarEmprestimo(parametros);
                        }
                    });
                }
            });
        };

        priv.efetuarEmprestimo = function (parametros) {
            priv.addOverlay($('#leitorEmprestimo'), 'Aguarde, efetuando empréstimo dos exemplares.');

            $.ajax({
                url: priv.options.url.emprestimoAcao,
                method: 'POST',
                dataType: 'json',
                data: parametros,
                success: function (data) {
                    if (data.erro) {
                        priv.showNotificacao({timeout: 10000, type: 'danger', content: data.erroDescricao});
                        priv.removeOverlay($('#leitorEmprestimo'));
                    } else {
                        var emprestimoId = data.emprestimoId || false;
                        priv.showNotificacao({
                            timeout: 10000, type: 'success', content: 'Empréstimo efetuado com sucesso.'
                        });

                        priv.removeOverlay($('#leitorEmprestimo'));

                        setTimeout(function () {
                            priv.recarregarInformacoesLeitor(false, true, false, parametros, emprestimoId);
                        }, 600);
                    }
                }
            });
        };

        priv.carregarInformacoesLeitor = function () {
            var carregaDados = function (arrData, padrao) {
                var arrCampos = [
                    'alunocursoId',
                    'cursoId',
                    'grupoLeitorNome',
                    'cursoNome',
                    'campId',
                    'campNome',
                    'pesId',
                    'pesNome',
                    'leitorInfoEmprestimosNum',
                    'leitorInfoEmprestimosAtraso',
                    'leitorInfoEmprestimosInteresse',
                    'turmaNome',
                    'turmaTurno',
                    'situacaoMatricula',
                    'periodoLetivo'
                ];
                arrData = arrData || {};
                padrao = padrao || false;

                for (var i in arrCampos) {
                    var campo = arrCampos[i];
                    var valor = typeof arrData[campo] == 'undefined' ? false : arrData[campo];

                    if (valor === false && padrao) {
                        valor = padrao;
                    }

                    $('#' + campo).html(valor);
                }

                var imagemAluno = typeof arrData['imagemAluno'] == 'undefined' ? '' : arrData['imagemAluno'];

                $('#imagemAluno img').hide();
                $('#imagemAluno span').remove();

                if (imagemAluno) {
                    $('#imagemAluno').append('<span><i class="fa fa-spinner fa-2x fa-spin"></i>Carregando imagem...</span>');
                    $('#imagemAluno img').attr('src',
                        priv.options.url.thumbnail + '/' + imagemAluno + '?width=100&height=80');
                    $('#imagemAluno img').load(function () {
                        $('#imagemAluno span').remove();
                        $('#imagemAluno img').show();
                    });
                } else {
                    $('#imagemAluno').append('<span>' + (padrao || 'Indisponível!') + '</span>');
                }
            };

            carregaDados(priv.options.data.leitor, 'Carregando...');
            priv.addOverlay($('#leitorInformacao'), 'Aguarde, carregando informações sobre o leitor.');

            $.ajax({
                url: priv.options.url.leitorInformacao,
                method: 'POST',
                dataType: 'json',
                data: {
                    pesId: priv.options.data.leitor['pes_id'],
                    matricula: priv.options.data.leitor['alunocurso_id']
                },
                success: function (arrLeitor) {
                    if (arrLeitor.leitor) {
                        priv.options.data.leitor = arrLeitor.leitor;
                        var bloquearModalidade = false;
                        var mensagem = '';

                        if (priv.options.data.leitor['pessoaSituacao'] == 'Inativo') {
                            mensagem = 'O leitor está com cadastro inativado!<br>' +
                                'Procure tesouraria ou a direção da biblioteca.';
                            bloquearModalidade = true;

                            priv.showNotificacao({timeout: 20000, type: 'danger', content: mensagem});
                        }

                        if (Object.keys(priv.options.data.leitor['leitorMultaInfo']).length > 0) {
                            mensagem = 'O leitor possui multa(s) em aberto!<br>' +
                                'Procure tesouraria ou a direção da biblioteca.';
                            bloquearModalidade = true;

                            priv.showNotificacao({timeout: 20000, type: 'danger', content: mensagem});
                        }

                        if (Object.keys(priv.options.data.leitor['leitorSuspensaoInfo']).length > 0) {
                            var suspensaoInicio = new Date(priv.options.data.leitor['leitorSuspensaoInfo']['suspensaoInicio']),
                                suspensaoFim = new Date(priv.options.data.leitor['leitorSuspensaoInfo']['suspensaoFim']);
                            bloquearModalidade = true;
                            suspensaoInicio = $.formatDateTime('dd/mm/yy hh:ii', suspensaoInicio);
                            suspensaoFim = $.formatDateTime('dd/mm/yy hh:ii', suspensaoFim);
                            mensagem = 'O leitor está suspenso de ' + suspensaoInicio + ' à ' + suspensaoFim + '!<br>' +
                                'Procure direção da biblioteca.';

                            priv.showNotificacao({timeout: 20000, type: 'danger', content: mensagem});
                        }

                        if (priv.options.data.leitor['leitorInfoEmprestimosAtraso'] > 70) {
                            mensagem = 'Este leitor tem um percentual de <b>atraso</b> alto!';

                            priv.showNotificacao({timeout: 10000, type: 'warning', content: mensagem});
                        }

                        if (priv.options.data.leitor['leitorNumeroDeAtrasoSemEntrega'] > 0) {
                            mensagem = 'Este leitor possui pendências e não pode efetuar empréstimos!';
                            bloquearModalidade = true;

                            priv.showNotificacao({timeout: 10000, type: 'danger', content: mensagem});
                        }

                        $('#modalidadeEmprestimo').prop('disabled', bloquearModalidade);

                        var leitorNumeroItensEmprestados = (
                            priv.options.data.leitor['leitorNumeroItensEmprestados'] || []
                        );

                        for (var i in leitorNumeroItensEmprestados) {
                            var grupoBibliograficoId = leitorNumeroItensEmprestados[i]['grupoBibliograficoId'];
                            priv.options.dataParam.grupoBibliograficoInserido[grupoBibliograficoId] = 0;
                        }
                    }

                    carregaDados(priv.options.data.leitor, 'Indisponível');
                    priv.removeOverlay($('#leitorInformacao'));
                }
            });

        };

        priv.carregarInformacaoEmprestimo = function (manterDatatableEmprestimo) {
            manterDatatableEmprestimo = manterDatatableEmprestimo || false;

            if (!manterDatatableEmprestimo) {
                priv.limpaDatatableEmprestimo();
            }

            $('#modalidadeEmprestimo').select2('data', null);
            $('#modalidadeEmprestimo').trigger('change');

            if (!priv.options.data.leitor['grupoLeitorId']) {
                return;
            }

            priv.addOverlay($('#leitorEmprestimo'), 'Aguarde, carregando modalidades de empréstimo do grupo.');

            $.ajax({
                url: priv.options.url.modalidade,
                method: 'POST',
                dataType: 'json',
                data: {grupoLeitorId: priv.options.data.leitor['grupoLeitorId'] || ''},
                success: function (arrLeitor) {
                    var modalidades = arrLeitor.modalidades || {};

                    if (Object.keys(modalidades).length > 0) {
                        priv.options.data.modalidades = modalidades;
                    } else {
                        priv.options.data.modalidades = [];
                        priv.showNotificacao({
                            timeout: 15000,
                            type: 'danger',
                            content: 'O grupo do leitor não possui modalidades de empréstimo!'
                        });
                    }

                    $('#modalidadeEmprestimo').select2('data', null);
                    $('#modalidadeEmprestimo').trigger('change');

                    priv.removeOverlay($('#leitorEmprestimo'));
                }
            });
        };

        priv.inciaDatatableEmprestimo = function () {
            var colNum = 0;
            priv.options.datatable.emprestimo = $('#dataTableEmprestimo').dataTable({
                processing: true,
                columnDefs: [
                    {name: "exemplar_id", targets: colNum++, data: "exemplar_id"},
                    {name: "titulo_f", targets: colNum++, data: "titulo_f"},
                    {name: "item_remove", targets: colNum++, data: "item_remove", ordering: false}
                ],
                "dom": "rt",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0]]
            });

            $('#dataTableEmprestimo').on('click', '.emprestimoItemRemove', function () {
                var pai = $(this).closest('tr');
                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover este exemplar?',
                    buttons: "[Cancelar][Ok]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Ok") {
                        var exemplarData = priv.options.datatable.emprestimo.fnGetData(pai);
                        delete priv.options.data.exemplar[exemplarData['titulo_id']];

                        $('#emprestimoExemplarRemoveTodos,#emprestimoExemplarEmprestar')
                            .prop('disabled', Object.keys(priv.options.data.exemplar).length == 0);
                        priv.options.datatable.emprestimo.api().row(pai).remove().draw();
                    }
                });
            });
        };

        priv.inciaDatatableDevolucao = function () {
            var colNum = 0;
            var esconderKitSaraiva = $.storage.get('titulo.add.devolucao.filtro.kit-saraiva') || 0;
            var $dataTableEmprestimoDevolucao = $('#dataTableEmprestimoDevolucao');

            priv.options.datatable.devolucao = $dataTableEmprestimoDevolucao.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.url.emprestimoListagem,
                    data: function (d) {
                        d.leitorId = priv.options.data.leitor.pesId || -1;
                        d.devolucao = true;
                        d.esconderKitSaraiva = esconderKitSaraiva;

                        return d;
                    },
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        for (var row in data) {
                            data[row].acao = '<div class="text-center">' +
                                '<input type="checkbox" class="emprestimoItemSelecao" title="selecionar">' +
                                '</div>';
                            priv.formataValoresExemplar(data[row]);

                            data[row].btns = '-';

                            data[row].btns =
                                '<div class="text-center">' +
                                ' <button type="button" class="btn btn-xs btn-primary btn-emprestimo-comprovante"' +
                                'title="Imprimir comprovante de empréstimo">' +
                                '<i class="fa fa-print"></i></button>' +
                                '</div>';
                        }

                        priv.removeOverlay($('#leitorEmprestimoDevolucao'));
                        $('#marcarTodosEmprestimos').prop('checked', false);
                        $('#cancelarEmprestimosSelecionados, #entregarEmprestimosSelecionados').prop('disabled', true);
                        $('#marcarTodosEmprestimos').prop('disabled', (data || []).length == 0);

                        return data;
                    }
                },
                columnDefs: [
                    {name: "acao", targets: colNum++, data: "acao", orderable: false},
                    {name: "exemplar_id", targets: colNum++, data: "exemplar_id"},
                    {name: "titulo_titulo", targets: colNum++, data: "titulo_f"},
                    {
                        name: "modalidade_emprestimo_descricao", targets: colNum++,
                        data: "modalidade_emprestimo_descricao"
                    },
                    {name: "emprestimo_data", targets: colNum++, data: "data_f", type: "date-eu"},
                    {
                        name: "emprestimo_devolucao_data_previsao", targets: colNum++, data: "data_previsao_f",
                        type: "date-eu"
                    },
                    {name: "emprestimo_atraso", targets: colNum++, data: "atraso_f"},
                    {name: "usuario_login", targets: colNum++, data: "usuario_login"},
                    {name: "btns", targets: colNum++, data: "btns", searchable: false, search: false, orderable: false},
                    {name: "autor_referencia", targets: colNum++, data: "autor_referencia", visible: false},
                    {name: "titulo_id", targets: colNum++, data: "titulo_id", visible: false, searchable: true},
                    {
                        name: "exemplar_codigo", targets: colNum++, data: "exemplar_codigo", visible: false,
                        searchable: true
                    },
                    {
                        name: "autor_referencia", targets: colNum++, data: "autor_referencia", visible: false,
                        searchable: true
                    },
                    {
                        name: "grupo_bibliografico_id", targets: colNum++, data: "grupo_bibliografico_id",
                        visible: false, searchable: true
                    },
                    {
                        name: "grupo_bibliografico_nome", targets: colNum++, data: "grupo_bibliografico_nome",
                        visible: false, searchable: true
                    }
                ],
                "dom": "<'dt-toolbar'<'col-xs-4 dt-toolbar-devolucao no-padding'><'col-xs-4 no-padding'r><'col-xs-4 no-padding'f>>" +
                't' +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[5, 'asc']]
            });

            $('.dt-toolbar-devolucao').append(
                '<div class="checkbox">' +
                '<label>' +
                '<input type="checkbox" id="btn-devolucao-filtro-kit-saraiva">' +
                'Esconder kit Saraiva' +
                '</label>' +
                '</div>'
            );

            $('#btn-devolucao-filtro-kit-saraiva').prop('checked', esconderKitSaraiva);
            $('#btn-devolucao-filtro-kit-saraiva').change(function () {
                esconderKitSaraiva = $(this).prop('checked') ? 1 : 0;
                $.storage.set('titulo.add.devolucao.filtro.kit-saraiva', esconderKitSaraiva);
                priv.options.datatable.devolucao.api().ajax.reload(null, false);
            });

            var validaRenovacao = function () {
                var quantidadeMaximaEmprestimos = parseInt(priv.options.value.quantidadeDeRenovacoes || 0),
                    quantidade = 0,
                    pendencias = 0;

                var dataAtual = new Date((priv.options.value.dataAtual).split('/').reverse().join('/')+" 00:00");
                var diasAntesDoVencimentoParaRenovar = priv.options.value.diasParaRenovacao;

                $('#dataTableEmprestimoDevolucao .emprestimoItemSelecao:checked').each(function (i, item) {
                    var pai = $(this).closest('tr');
                    var arrEmprestimo = priv.options.datatable.devolucao.fnGetData(pai);
                    quantidade++;

                    if (parseInt(arrEmprestimo['quantidadeDeEmprestimosConcedidos'] || 0) > quantidadeMaximaEmprestimos) {
                        pendencias++;
                        return true;
                    }

                    var dataDevolucao = (arrEmprestimo['emprestimo_devolucao_data_previsao']).split("-");

                    dataDevolucao = (dataDevolucao.join("-")) + " 00:00:00";

                    dataDevolucao = new Date(dataDevolucao);
                    dataDevolucao = new Date(dataDevolucao.getTime() - (diasAntesDoVencimentoParaRenovar * 24 * 60 * 60 * 1000));

                    if (dataAtual > dataDevolucao || arrEmprestimo['atraso_f'] == 'Sim') {
                        pendencias++;
                        return true;
                    }
                });

                return quantidade == 0 || pendencias;
            };

            var habilitaBotoes = function (ignorarCheckbox) {
                $("#renovarEmprestimo").prop("disabled", validaRenovacao());

                ignorarCheckbox = ignorarCheckbox || false;
                var itensSelecionados = $dataTableEmprestimoDevolucao.find('.emprestimoItemSelecao:checked').length;
                var itensEmPoder = priv.options.data.leitor['leitorTitulosExemplaresEmPoder'] || {};

                $('#cancelarEmprestimosSelecionados, #entregarEmprestimosSelecionados')
                    .prop('disabled', itensSelecionados == 0);
                if (!ignorarCheckbox) {
                    $('#marcarTodosEmprestimos').prop('checked', Object.keys(itensEmPoder).length == itensSelecionados);
                }
            };

            $('#marcarTodosEmprestimos').on('change', function () {
                $dataTableEmprestimoDevolucao.find('.emprestimoItemSelecao').prop('checked', $(this).prop('checked'));
                habilitaBotoes(true);
            });

            $dataTableEmprestimoDevolucao.on('change', '.emprestimoItemSelecao', function () {
                habilitaBotoes();
            });

            $dataTableEmprestimoDevolucao.on('click', 'td', function (e) {
                if ($(this).find('.emprestimoItemSelecao').length > 0 || $(this).hasClass('dataTables_empty')) {
                    return;
                }

                $(this)
                    .parent()
                    .find('.emprestimoItemSelecao')
                    .promise()
                    .done(function (item) {
                        $(item).prop('checked', !$(item).prop('checked'));
                    });
                habilitaBotoes();
            });

            var $modalComprovante = $('#emprestimo-comprovante-modal');

            $('#corpo-recibo-modal').load(function () {
                versaShared.removeOverlay($modalComprovante);
            });

            $dataTableEmprestimoDevolucao.on('click', '.btn-emprestimo-comprovante', function () {
                var pai = $(this).closest('tr');
                var arrEmprestimo = priv.options.datatable.devolucao.fnGetData(pai);
                var $modal = $('#emprestimo-comprovante-modal');
                var url = priv.options.url.emprestimoComprovante + '?emprestimoId=' + arrEmprestimo['emprestimo_id'];
                $('#corpo-recibo-modal').attr('src', url);

                versaShared.addOverlay($modal);
                $modal.modal('show');
            });

            $('#emprestimo-comprovante-impressao').click(function (e) {
                $('#corpo-recibo-modal').get(0).contentWindow.print();
                e.preventDefault();
                e.stopPropagation();
            });

            $('#cancelarEmprestimosSelecionados, #entregarEmprestimosSelecionados, #renovarEmprestimo').on('click', function () {
                var acao = 'cancelar', acao2 = 'Cancelamento', emprestimos = [];

                if ($(this).attr('id') == 'entregarEmprestimosSelecionados') {
                    acao = 'devolver';
                    acao2 = 'Devolução';
                }

                if ($(this).attr('id') == 'renovarEmprestimo') {
                    acao = 'renovar';
                    acao2 = 'Renovação';
                }

                $dataTableEmprestimoDevolucao.find('.emprestimoItemSelecao:checked').each(function (i, item) {
                    var pai = $(this).closest('tr');
                    var arrEmprestimo = priv.options.datatable.devolucao.fnGetData(pai);
                    emprestimos.push(arrEmprestimo['emprestimo_id']);
                });

                if (emprestimos.length == 0) {
                    priv.showNotificacao({
                        timeout: 5000, type: 'danger', content: 'Selecione ao menos um empréstimo para ' + acao + '.'
                    });
                }

                var strEmrestimos = '';

                if (emprestimos.length == 1) {
                    strEmrestimos = 'do empréstimo selecionado';
                } else {
                    strEmrestimos = 'dos ' + emprestimos.length + ' empréstimos selecionados';

                    if (Object.keys(priv.options.data.leitor.leitorTitulosExemplaresEmPoder || {}).length ==
                        emprestimos.length) {
                        strEmrestimos = 'de TODOS os empréstimos';
                    }
                }

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente efetuar a ' + acao2.toLowerCase() + ' <b>' +
                    strEmrestimos.toUpperCase() + '</b>?',
                    buttons: "[Cancelar][Ok]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Ok") {
                        priv.addOverlay($('#leitorEmprestimoDevolucao'),
                            'Aguarde, efetuando devolução ' + strEmrestimos + '.');

                        $.ajax({
                            url: priv.options.url.emprestimoAcao,
                            method: 'POST',
                            dataType: 'json',
                            data: {acao: acao, emprestimoId: emprestimos.join(',')},
                            success: function (data) {
                                if (data.erro) {
                                    priv.showNotificacao({timeout: 10000, type: 'danger', content: data.erroDescricao});
                                } else {
                                    priv.showNotificacao({
                                        timeout: 10000, type: 'success',
                                        content: acao2 + ' ' + strEmrestimos + ' efetuado com sucesso.'
                                    });
                                }

                                priv.removeOverlay($('#leitorEmprestimoDevolucao'));
                                setTimeout(function () {
                                    priv.recarregarInformacoesLeitor(true, true);
                                }, 600);
                            }
                        });
                    }
                });
            });
        };

        priv.inciaDatatableEmprestimosAtivos = function () {
            var colNum = 0;
            var esconderKitSaraiva = $.storage.get('titulo.add.emprestimos-ativos.filtro.kit-saraiva') || 0;

            priv.addOverlay($('#emprestimosAtivos'), 'Aguarde, carregando informação dos empréstimos ativos.');
            priv.options.datatable.emprestimosAtivos = $('#dataTableEmprestimosAtivos').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.url.emprestimoListagem,
                    data: function (d) {
                        d.devolucao = true;
                        d.pes_info = true;
                        d.esconderKitSaraiva = esconderKitSaraiva;

                        return d;
                    },
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        for (var row in data) {
                            data[row].acao = '<div class="text-center">' +
                                '<input type="checkbox" class="emprestimoItemSelecao" title="selecionar">' +
                                '</div>';
                            priv.formataValoresExemplar(data[row]);

                        }

                        priv.removeOverlay($('#emprestimosAtivos'));
                        $('#marcarTodosEmprestimos').prop('checked', false);
                        $('#cancelarEmprestimosSelecionados, #entregarEmprestimosSelecionados').prop('disabled', true);
                        $('#marcarTodosEmprestimos').prop('disabled', (data || []).length == 0);

                        return data;
                    }
                },
                columnDefs: [
                    {
                        name: "emprestimo_id", targets: colNum++, data: "acao", searchable: false, search: false,
                        orderable: false
                    },
                    {name: "exemplar_id", targets: colNum++, data: "exemplar_id"},
                    {name: "titulo_titulo", targets: colNum++, data: "titulo_f"},
                    {name: "leitor_id", targets: colNum++, data: "leitor_f"},
                    {name: "emprestimo_data", targets: colNum++, data: "data_f", type: "date-eu"},
                    {
                        name: "emprestimo_devolucao_data_previsao", targets: colNum++, data: "data_previsao_f",
                        type: "date-eu"
                    },
                    {name: "emprestimo_atraso", targets: colNum++, data: "atraso_f"},
                    {name: "usuario_login", targets: colNum++, data: "usuario_login"},
                    {name: "autor_referencia", targets: colNum++, data: "autor_referencia", visible: false},
                    {name: "titulo_id", targets: colNum++, data: "titulo_id", visible: false, searchable: true},
                    {
                        name: "exemplar_codigo", targets: colNum++, data: "exemplar_codigo", visible: false,
                        searchable: true
                    },
                    {
                        name: "autor_referencia", targets: colNum++, data: "autor_referencia", visible: false,
                        searchable: true
                    },
                    {
                        name: "grupo_bibliografico_id", targets: colNum++, data: "grupo_bibliografico_id",
                        visible: false, searchable: true
                    },
                    {
                        name: "modalidade_emprestimo_descricao", targets: colNum++,
                        data: "modalidade_emprestimo_descricao", visible: false, searchable: true
                    },
                    {name: "leitor_id", targets: colNum++, data: "leitor_id", visible: false, searchable: true},
                    {name: "leitor_nome", targets: colNum++, data: "leitor_nome", visible: false, searchable: true},
                    {
                        name: "grupo_bibliografico_nome", targets: colNum++, data: "grupo_bibliografico_nome",
                        visible: false, searchable: true
                    }
                ],
                "dom": "<'dt-toolbar'<'col-xs-4 dt-toolbar-emprestimos-ativos no-padding'><'col-xs-4 no-padding'r><'col-xs-4 no-padding'f>>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[5, 'asc']]
            });

            $('.dt-toolbar-emprestimos-ativos').append(
                '<div class="checkbox">' +
                '<label>' +
                '<input type="checkbox" id="btn-emprestimos-ativos-filtro-kit-saraiva">' +
                'Esconder kit Saraiva' +
                '</label>' +
                '</div>'
            );

            $('#btn-emprestimos-ativos-filtro-kit-saraiva').prop('checked', esconderKitSaraiva);
            $('#btn-emprestimos-ativos-filtro-kit-saraiva').change(function () {
                esconderKitSaraiva = $(this).prop('checked') ? 1 : 0;
                $.storage.set('titulo.add.emprestimos-ativos.filtro.kit-saraiva', esconderKitSaraiva);
                priv.options.datatable.emprestimosAtivos.api().ajax.reload(null, false);
            });

            $('#dataTableEmprestimosAtivos').on('click', 'td', function (e) {
                if ($(this).find('.emprestimoItemSelecao').length > 0 || $(this).hasClass('dataTables_empty')) {
                    return;
                }

                $(this)
                    .parent()
                    .find('.emprestimoItemSelecao')
                    .promise()
                    .done(function (item) {
                        $(item).prop('checked', !$(item).prop('checked'));
                    });

                var itensSelecionados = $('#dataTableEmprestimosAtivos .emprestimoItemSelecao:checked').length;
                $('#entregarEmprestimosAtivosSelecionados').prop('disabled', itensSelecionados == 0);
            });

            $('#entregarEmprestimosAtivosSelecionados').on('click', function () {
                var acao = 'devolver';
                var acao2 = 'Devolução';
                var emprestimos = [];

                $('#dataTableEmprestimosAtivos .emprestimoItemSelecao:checked').each(function (i, item) {
                    var pai = $(this).closest('tr');
                    var arrEmprestimo = priv.options.datatable.emprestimosAtivos.fnGetData(pai);
                    emprestimos.push(arrEmprestimo['emprestimo_id']);
                });

                if (emprestimos.length == 0) {
                    priv.showNotificacao({
                        timeout: 5000, type: 'danger', content: 'Selecione ao menos um empréstimo para ' + acao + '.'
                    });
                }

                var strEmrestimos = '';

                if (emprestimos.length == 1) {
                    strEmrestimos = 'do empréstimo selecionado';
                } else {
                    strEmrestimos = 'dos ' + emprestimos.length + ' empréstimos selecionados';
                    //TODO: tamanho do datatable
                    if (0 == emprestimos.length) {
                        strEmrestimos = 'de TODOS os empréstimos';
                    }
                }

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente efetuar a ' + acao2.toLowerCase() + ' <b>' +
                    strEmrestimos.toUpperCase() + '</b>?',
                    buttons: "[Cancelar][Ok]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Ok") {
                        priv.addOverlay($('#emprestimosAtivos'), 'Aguarde, efetuando devolução ' + strEmrestimos + '.');

                        $.ajax({
                            url: priv.options.url.emprestimoAcao,
                            method: 'POST',
                            dataType: 'json',
                            data: {acao: acao, emprestimoId: emprestimos.join(',')},
                            success: function (data) {
                                if (data.erro) {
                                    priv.showNotificacao({timeout: 10000, type: 'danger', content: data.erroDescricao});
                                } else {
                                    priv.showNotificacao({
                                        timeout: 10000, type: 'success',
                                        content: acao2 + ' ' + strEmrestimos + ' efetuado com sucesso.'
                                    });
                                }

                                priv.removeOverlay($('#emprestimosAtivos'));
                                setTimeout(function () {
                                    priv.recarregarInformacoesLeitor(true, true, true);
                                }, 600);
                            }
                        });
                    }
                });
            });
        };

        priv.inciaDatatableHistorico = function () {
            var colNum = 0;
            priv.options.datatable.historico = $('#dataTableEmprestimoHistorico').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.url.emprestimoListagem,
                    data: function (d) {
                        d.leitorId = priv.options.data.leitor.pesId || -1;

                        return d;
                    },
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        for (var row in data) {
                            priv.formataValoresExemplar(data[row]);
                            data[row]['usuario_login_f'] = 'Emp.: ' + data[row]['usuario_login'];

                            if (data[row]['usuario_login_devolucao']) {
                                data[row]['usuario_login_f'] += '<br>';
                                data[row]['usuario_login_f'] +=
                                    'Dev.: ' + (data[row]['usuario_login_devolucao'] || '-');
                            }
                        }

                        priv.removeOverlay($('#leitorEmprestimoHistorico'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "exemplar_id", targets: colNum++, data: "exemplar_id"},
                    {name: "titulo_titulo", targets: colNum++, data: "titulo_f"},
                    {
                        name: "modalidade_emprestimo_descricao", targets: colNum++,
                        data: "modalidade_emprestimo_descricao"
                    },
                    {name: "emprestimo_data", targets: colNum++, data: "data_f", type: "date-eu"},
                    {
                        name: "emprestimo_devolucao_data_efetuada", targets: colNum++, data: "data_entrega_f",
                        type: "date-eu"
                    },
                    {name: "emprestimo_atraso", targets: colNum++, data: "atraso_f"},
                    {name: "usuario_login_devolucao", targets: colNum++, data: "usuario_login_f"},
                    {name: "titulo_id", targets: colNum++, data: "titulo_id", visible: false, searchable: true},
                    {
                        name: "exemplar_codigo", targets: colNum++, data: "exemplar_codigo", visible: false,
                        searchable: true
                    },
                    {
                        name: "autor_referencia", targets: colNum++, data: "autor_referencia", visible: false,
                        searchable: true
                    },
                    {
                        name: "grupo_bibliografico_id", targets: colNum++, data: "grupo_bibliografico_id",
                        visible: false, searchable: true
                    },
                    {
                        name: "grupo_bibliografico_nome", targets: colNum++, data: "grupo_bibliografico_nome",
                        visible: false, searchable: true
                    }
                ],
                "dom": "rf" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[3, 'desc']]
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.pesquisaUsuario();
            priv.pesquisaModalidadeEmprestimo();
            priv.pesquisaExemplaresDisponiveis();
            priv.pesquisaExemplaresDisponiveis();
            priv.inciaDatatableEmprestimo();
            priv.acaoBotaoEmprestimo();
            priv.inciaDatatableDevolucao();
            priv.inciaDatatableHistorico();
            priv.inciaDatatableEmprestimosAtivos();
        };
    };

    $.emprestimo = function (params) {
        params = params || [];

        var obj = $(window).data("universa.emprestimo");

        if (!obj) {
            obj = new Emprestimo();
            obj.run(params);
            $(window).data('universa.emprestimo', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);