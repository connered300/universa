(function ($, window, document) {
    'use strict';

    var EmpresaIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="biblioteca-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.biblioteca-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableEmpresa = $('#dataTableEmpresa').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary empresa-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger empresa-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "pes_nome_fantasia", targets: colNum++, data: "pes_nome_fantasia"},
                    {name: "pes_cnpj", targets: colNum++, data: "pes_cnpj"},
                    {name: "con_contato_telefone", targets: colNum++, data: "con_contato_telefone"},
                    {name: "end_cidade", targets: colNum++, data: "end_cidade"},
                    {name: "end_estado", targets: colNum++, data: "end_estado"},
                    {name: "con_contato_email", targets: colNum++, data: "con_contato_email"},
                    {name: "empresa_tipo", targets: colNum++, data: "empresa_tipo"},
                    {name: "pes_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0]]
            });

            $('#dataTableEmpresa').on('click', '.empresa-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableEmpresa.fnGetData($pai);

                location.href = priv.options.urlEdit + '/' + data['pes_id'];
            });

            $('#dataTableEmpresa').on('click', '.empresa-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableEmpresa.fnGetData($pai);
                var arrRemover = {
                    pesId: data['pes_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover a empresa "' + data['pes_nome_fantasia'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        priv.addOverlay($('.widget-body'), 'Aguarde, solicitando remoção de empresa...');
                        $.ajax({
                            url: priv.options.urlRemove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível remover a empresa:",
                                        content: "<i>" + data['erroDescricao'] + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });

                                    priv.removeOverlay($('.widget-body'));
                                } else {
                                    priv.dataTableEmpresa.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Empresa removida!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.empresaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.empresa.index");

        if (!obj) {
            obj = new EmpresaIndex();
            obj.run(params);
            $(window).data('universa.empresa.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);