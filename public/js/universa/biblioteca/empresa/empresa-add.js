(function ($, window, document) {
    'use strict';

    var EmpresaAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlBuscaCEP: '',
                empresaTipo: []
            },
            data:{
                arrEstados: []
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {
            $("#pesCnpj").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99.999.999/9999-99']});
            $("#empresaTipo").select2({
                language: 'pt-BR',
                tags: true,
                data: priv.options.empresaTipo,
                initSelection: function (element, callback) {
                    var value = $(element).val().split(',');
                    var dados = $.map(value, function (val) {
                        return {text: val, id: val};
                    });

                    callback(dados);
                }
            });
            $("#pesInscEstadual").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['9{8,14}']});
            $("#endCep").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']});
            $('#endNumero').inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
            $("#conContatoTelefone").inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['(99) 9999-9999', '(99) 99999-9999']
            });

            $("#buscarCep").click(function () {
                var endCep = $("#endCep").val();
                var endCepNums = endCep.replace(/[^0-9]/g, '');

                var erroCEP = function () {
                    $.smallBox({
                        title: "Erro!",
                        content: "<i>CEP inválido ou não encontrado!</i>",
                        color: "#C46A69",
                        icon: "fa fa-times",
                        timeout: 5000
                    });
                };

                if (endCepNums.length < 8) {
                    erroCEP();

                    return;
                }

                $("#buscarCep").prop('disabled', true);

                $.ajax({
                    url: priv.options.urlBuscaCEP,
                    dataType: 'json',
                    type: 'post',
                    data: {cep: endCep},
                    success: function (data) {
                        $("#buscarCep").prop('disabled', false);

                        if (!data.dados) {
                            erroCEP();
                        }

                        $("#endLogradouro").val(((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim());
                        $("#endCidade").val(data.dados.cid_nome || '');
                        $("#endEstado").val(data.dados.est_uf || '').trigger('change');
                        $("#endBairro").val(data.dados.bai_nome || '');
                    },
                    erro: function () {
                        erroCEP();
                        $("#buscarCep").prop('disabled', false);
                    }
                });
            });

            $('#endCidade').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: priv.options.urlBuscaCEP,
                        data: {cidade: request.term},
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            var transformed = $.map(data.dados || [], function (el) {
                                el.label = el.cid_nome;

                                return el;
                            });

                            response(transformed);
                        }
                    });
                },
                minLength: 2,
                focus: function (event, ui) {
                    $("#endCidade").val(ui.item.cid_nome);

                    return false;
                },
                select: function (event, ui) {
                    $("#endCidade").val(ui.item.cid_nome);
                    $("#endEstado").val(ui.item.est_uf).trigger('change');

                    return false;
                }
            });

            $('[name=endEstado]').select2({
                language: 'pt-BR',
                allowClear: true,
                data: function () {
                    return {results: priv.options.data.arrEstados || []};
                }
            });

        };

        priv.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $("#empresa-form").validate({
                submitHandler: submitHandler,
                rules: {
                    pesCnpj: {required: false, cnpj: true},
                    empresaTipo: {required: true},
                    pesInscEstadual: {required: false},
                    pesNomeFantasia: {required: true, maxlength: 255},
                    pesNome: {required: false, maxlength: 255},
                    endCep: {required: false},
                    endLogradouro: {required: false},
                    endNumero: {required: false},
                    endComplemento: {required: false},
                    endBairro: {required: false},
                    endCidade: {required: true},
                    endEstado: {required: true},
                    conContatoTelefone: {telefoneValido:true, required: false},
                    conContatoEmail: {required: false, email: true},
                    empresaWebsite: {required: false, url: true},
                    empresaObservacao: {required: false}
                },
                messages: {
                    pesCnpj: {
                        required: 'CNPJ obrigatório!',
                        cnpj: 'CNPJ inválido!'
                    },
                    empresaTipo: {required: 'Campo obrigatório!'},
                    pesInscEstadual: {
                        required: 'Inscrição estadual obrigatória!',
                        minlength: 'Inscrição estadual: mínimo 8 números!',
                        maxlength: 'Inscrição estadual: máximo 14 números!'
                    },
                    pesNomeFantasia: {
                        required: 'Nome fantasia obrigatório!',
                        maxlength: 'Nome fantasia: máximo 255 caracteres!'
                    },
                    pesNome: {
                        required: 'Razão social obrigatória!',
                        maxlength: 'Razão Social: máximo 255 caracteres!'
                    },
                    endCep: {required: 'CEP obrigatório!'},
                    endLogradouro: {required: 'Logradouro obrigatório!'},
                    endNumero: {required: 'Número obrigatório!'},
                    endComplemento: {required: 'Complemento obrigatório!'},
                    endBairro: {required: 'Bairro obrigatório!'},
                    endCidade: {required: 'Cidade obrigatória!'},
                    endEstado: {required: 'Estado obrigatório!'},
                    conContatoTelefone: {required: 'Telefone obrigatório!'},
                    conContatoEmail: {required: 'Email obrigatório!', email: 'Email inválido!'},
                    empresaWebsite: {required: 'Website obrigatório!', url: 'Url de website inválido!'},
                    empresaObservacao: {required: 'Observações obrigatórias!'}
                },
                ignore: '.ignore'
            });
        };


        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.empresaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.empresa.add");

        if (!obj) {
            obj = new EmpresaAdd();
            obj.run(params);
            $(window).data('universa.empresa.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);