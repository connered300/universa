(function ($, window, document) {
    'use strict';

    var AreaCnpqIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#area-cnpq-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['area_cnpq_id']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $(".has-error .help-block").hide();
            $(".has-error").removeClass("has-error");

            $('#nova-area-cnpq .modal-title').html(actionTitle + ' área');
            $('#areaCnpqCodigo').val(data['area_cnpq_codigo'] || '');
            $('#areaCnpqDescricao').val(data['area_cnpq_descricao'] || '');
            $('#areaCnpqId').val(data['area_cnpq_id'] || '');
            $('#areaCnpqTitulos').val(data['area_cnpq_titulos'] || '');
            $('#nova-area-cnpq').modal();
        };

        priv.setSteps = function () {
            priv.dataTableAreaCnpq = $('#dataTableAreaCnpq').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        return json.data;
                    }
                },
                columnDefs: [
                    {name: "area_cnpq_codigo", targets: 0, data: "area_cnpq_codigo"},
                    {name: "area_cnpq_descricao", targets: 1, data: "area_cnpq_descricao"},
                    {name: "area_cnpq_titulos", targets: 2, data: "area_cnpq_titulos"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0]]
            });

            $(document).on("click", "#dataTableAreaCnpq tbody tr", function (event) {
                var position = priv.dataTableAreaCnpq.fnGetPosition(this);
                var data = priv.dataTableAreaCnpq.fnGetData(position);
                priv.showModal(data)

            });

            $('#btn-nova-area-cnpq').click(function () {
                priv.showModal()
            });
        };

        pub.getDataTableAreaCnpq = function () {
            return priv.dataTableAreaCnpq;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.areaCnpqIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.areaCnpq.index");

        if (!obj) {
            obj = new AreaCnpqIndex();
            obj.run(params);
            $(window).data('universa.areaCnpq.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);