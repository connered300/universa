(function ($, window, document) {
    'use strict';

    var AreaCnpqAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                ajaxSubmit: false,
                listagem: false
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {
            $('#areaCnpqCodigo').inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['9.99.99.99-9']});
        };

        priv.setValidations = function () {
            $("#area-cnpq-form").validate({
                submitHandler: function (form) {
                    var areaCnpqTitulos = $('#areaCnpqTitulos').val() || 0;
                    var $form = $(form);

                    if (areaCnpqTitulos > 0) {
                        var confirmacao = $form.data('confirmacaoEnvio') || false;

                        if (!confirmacao) {
                            $.SmartMessageBox({
                                title: "Confirme a operação:",
                                content: 'Esta area CNPq está ligada a ' + areaCnpqTitulos + ' título' + (areaCnpqTitulos == 1 ? '' : 's') + '. Deseja realmente alterá-la?',
                                buttons: "[Cancelar][Ok]"
                            }, function (ButtonPress) {
                                if (ButtonPress == "Ok") {
                                    $("#area-cnpq-form").data('confirmacaoEnvio', true);
                                    $("#area-cnpq-form").submit();
                                }
                            });

                            return false;
                        }

                        $("#area-cnpq-form").data('confirmacaoEnvio', false);
                    }

                    if (priv.options.ajaxSubmit) {
                        var dados = $form.serializeJSON();
                        dados['ajax'] = true;
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível salvar a área CPNq:",
                                        content: "<i>" + data.erro.join('<br>') + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });
                                } else if (priv.options.listagem) {
                                    $.areaCnpqIndex().getDataTableAreaCnpq().api().ajax.reload(null, false);
                                    $('#nova-area-cnpq').modal('hide');
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Área CNPq salva!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    } else {
                        form.submit();
                    }
                },
                rules: {
                    areaCnpqCodigo: 'required',
                    areaCnpqDescricao: 'required'
                },
                messages: {
                    areaCnpqCodigo: 'Campo obrigatório!',
                    areaCnpqDescricao: 'Campo obrigatório!'
                },
                ignore: '.ignore'
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.areaCnpqAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.areaCnpq.add");

        if (!obj) {
            obj = new AreaCnpqAdd();
            obj.run(params);
            $(window).data('universa.areaCnpq.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);