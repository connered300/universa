(function ($, window, document) {
    'use strict';

    var BibliotecaVirtualIndex = function () {
        VersaShared.call(this);

        var __bibliotecaVirtual = this;

        this.defaults = {
            url: {
                search: '',
                acessarBiblioteca: '',
                remover: '',
                integrarAluno: ''
            },
            dataTable: {
                pessoa: null
            },
            formAdicionar: "#form-adicionar-usuario",
            formCriarUrl: "#form-criar-url",
            formRemover: "#form-remover-usuario"
        };

        this.setSteps = function () {
            var $usuarioCategoria = $(__bibliotecaVirtual.options.formAdicionar).find("[name='usuarioCategoria']"),
                $dataTable        = $('#dataTablePessoa');

            __bibliotecaVirtual.options.dataTable.pessoa = $dataTable.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __bibliotecaVirtual.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        for (var row in data) {
                            if (data[row]['integrado'] == "Não") {
                                data[row]['acao'] = __bibliotecaVirtual.createBtnGroup(
                                    [
                                        {
                                            class: 'btn-info novo-usuario',
                                            icon: 'fa-plus',
                                            title: 'Conceder acesso a biblioteca virtual'
                                        }
                                    ]
                                );
                            } else {
                                data[row]['acao'] = __bibliotecaVirtual.createBtnGroup(
                                    [
                                        {
                                            class: 'btn-info exibir-modal',
                                            icon: 'fa-eye',
                                            title: 'Acessar a biblioteca virtual'
                                        },
                                        {
                                            class: 'btn-danger remover-usuario',
                                            icon: 'fa-times',
                                            title: 'Remover acesso a biblioteca virtual'
                                        }
                                    ]
                                );
                            }
                        }

                        return data;
                    }
                },
                columns: [
                    {name: "pes_id", targets: 0, data: "pes_id"},
                    {name: "pes_nome", targets: 1, data: "pes_nome"},
                    {name: "con_contato_email", targets: 7, data: "con_contato_email"},
                    {name: "integrado", targets: 7, data: "integrado"},
                    {name: "categoria", targets: 7, data: "categoria"},
                    {name: "acao", targets: 7, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": [],
                    "sSwfPath": "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[1]]
            });

            $dataTable.on('click', '.novo-usuario', function () {
                var $pai = $(this).closest('tr');
                var data = __bibliotecaVirtual.options.dataTable.pessoa.fnGetData($pai);

                __bibliotecaVirtual.integrarAluno(data);
            });

            $dataTable.on('click', '.remover-usuario', function () {
                var $pai = $(this).closest('tr');
                var data = __bibliotecaVirtual.options.dataTable.pessoa.fnGetData($pai);

                __bibliotecaVirtual.removerAluno(data);
            });

            $dataTable.on('click', '.exibir-modal', function () {
                var $pai = $(this).closest('tr');
                var data = __bibliotecaVirtual.options.dataTable.pessoa.fnGetData($pai);

                __bibliotecaVirtual.showModalMinhaBiblioteca(data);
            });

            $usuarioCategoria.select2({
                allowClear: true,
                language: 'pt-BR',
                data: [
                    {id: 1, text: 'Professores'},
                    {id: 2, text: 'Alunos'},
                    {id: 3, text: 'Outros'}
                ],
                initSelection: function (element, callback) {
                    callback({id: 2, text: 'Alunos'}).trigger('change');
                }
            });
        };

        this.reloadDataTable = function () {
            __bibliotecaVirtual.options.dataTable.pessoa.api().ajax.reload();
        };

        this.integrarAluno = function (arrDados) {
            var $dataTable = $('#dataTablePessoa');

            $.ajax({
                url: __bibliotecaVirtual.options.url.integrarAluno,
                type: 'post',
                dataType: 'json',
                data: arrDados,
                beforeSend: function () {
                    __bibliotecaVirtual.addOverlay($dataTable, 'Aguarde, enviando requisição...');
                },
                success: function (data) {
                    __bibliotecaVirtual.removeOverlay($dataTable);

                    if (!data['success']) {
                        __bibliotecaVirtual.showNotificacaoDanger(
                            data['message'] || 'Erro ao integrar aluno, verifique os dados e tente novamente!'
                        );

                        return false;
                    }

                    __bibliotecaVirtual.showNotificacaoInfo(
                        data['message']
                    );

                    __bibliotecaVirtual.reloadDataTable();
                }
            });
        };

        this.removerAluno = function (arrDados) {
            var $dataTable = $('#dataTablePessoa');

            $.ajax({
                url: __bibliotecaVirtual.options.url.remover,
                type: 'post',
                dataType: 'json',
                data: arrDados,
                beforeSend: function () {
                    __bibliotecaVirtual.addOverlay($dataTable, 'Aguarde, enviando requisição...');
                },
                success: function (data) {
                    __bibliotecaVirtual.removeOverlay($dataTable);

                    if (!data['success']) {
                        __bibliotecaVirtual.showNotificacaoDanger(
                            data['message'] || 'Erro ao remover registro, verifique o log do sistema'
                        );

                        return false;
                    }

                    __bibliotecaVirtual.showNotificacaoInfo(
                        data['message'] || 'Removido com sucesso'
                    );

                    __bibliotecaVirtual.reloadDataTable();
                }
            })
        };

        this.showModalMinhaBiblioteca = function (data) {
            var $modal     = $("#modal-minha-biblioteca"),
                $dataTable = $('#dataTablePessoa'),
                $iframe    = $("#iframe-minha-biblioteca");

            $(document).keyup(function (e) {
                if (e.keyCode == 27) {
                    $modal.modal('hide');
                }

                return false;
            });

            if ($modal.find("#loaded").val() == data['pes_id']) {
                $modal.modal('show');
                return false;
            }

            $.ajax({
                url: __bibliotecaVirtual.options.url.acessarBiblioteca,
                type: 'post',
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    __bibliotecaVirtual.addOverlay($dataTable, 'Aguarde, enviando requisição...');
                },
                success: function (data) {
                    __bibliotecaVirtual.removeOverlay($dataTable);

                    var pesId = data['pesId'] ? data['pesId'] : false;
                    $modal.find("#loaded").val(pesId);

                    if (!data['success']) {
                        __bibliotecaVirtual.showNotificacaoDanger(
                            data['message'] || 'Erro ao integrar aluno, verifique os dados e tente novamente!'
                        );

                        return false;
                    }

                    if (data['message']) {
                        __bibliotecaVirtual.showNotificacaoInfo(
                            data['message']
                        );
                    }

                    if (data['url']) {
                        $iframe.attr("src", data['url']);
                        $modal.modal('show');
                    }
                }
            });
        };

        this.run = function (opts) {
            __bibliotecaVirtual.setDefaults(opts);
            __bibliotecaVirtual.setSteps();
        };
    };

    $.bibliotecaVirtualIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.biblioteca.biblioteca-virtual.biblioteca-virtual-index");

        if (!obj) {
            obj = new BibliotecaVirtualIndex();
            obj.run(params);
            $(window).data('universa.biblioteca.biblioteca-virtual.biblioteca-virtual-index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);