(function ($, window, document) {
    'use strict';

    var ColecaoIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.showModal = function (data) {
            data = data || {};
            var colecao = [];
            var actionTitle = 'Cadastrar';
            var $form = $('#colecao-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                var empresaId = data['colecao_empresa_id'];
                var empresaDescricao = data['colecao_empresa_nome'];
                colecao = {id: empresaId, text: empresaDescricao};

                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['colecao_id']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $(".has-error .help-block").hide();
            $(".has-error").removeClass("has-error");

            $.colecaoAdd().setEditora(colecao);


            $('#nova-colecao .modal-title').html(actionTitle + ' coleção');
            $('#editora').select2('val', colecao);
            $('#colecaoId').val(data['colecao_id'] || '');
            $('#colecaoTitulos').val(data['colecao_titulos'] || '');
            $('#colecaoDescricao').val(data['colecao_descricao'] || '');
            $('#nova-colecao').modal();
        };

        priv.setSteps = function () {
            priv.dataTableColecao = $('#dataTableColecao').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        return json.data;
                    }
                },
                columnDefs: [
                    {name: "colecao_descricao", targets: 0, data: "colecao_descricao"},
                    {name: "colecao_empresa_nome", targets: 1, data: "colecao_empresa_nome"},
                    {name: "colecao_titulos", targets: 2, data: "colecao_titulos"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0]]
            });

            $(document).on("click", "#dataTableColecao tbody tr", function (event) {
                var position = priv.dataTableColecao.fnGetPosition(this);
                var data = priv.dataTableColecao.fnGetData(position);
                priv.showModal(data)
            });

            $('#btn-nova-colecao').click(function () {
                priv.showModal()
            });
        };

        pub.getDataTableColecao = function () {
            return priv.dataTableColecao;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.colecaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.colecao.index");

        if (!obj) {
            obj = new ColecaoIndex();
            obj.run(params);
            $(window).data('universa.colecao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);