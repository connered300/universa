(function ($, window, document) {
    'use strict';

    var ColecaoAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                colecao: null,
                urlPesquisaEditora: '',
                ajaxSubmit: false,
                listagem: false
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {
            $("#editora").select2({
                language: 'pt-BR',
                ajax: {
                    url: priv.options.urlPesquisaEditora,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query, empresaTipo: 'Editora'};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_nome_fantasia, id: el.pes_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(pub.getEditora());
                }
            });

            $('#editora').select2("val", pub.getEditora());
        };

        priv.setValidations = function () {
            $("#colecao-form").validate({
                submitHandler: function (form) {
                    var colecaoTitulos = $('#colecaoTitulos').val() || 0;
                    var $form = $(form);

                    if (colecaoTitulos > 0) {
                        var confirmacao = $form.data('confirmacaoEnvio') || false;

                        if (!confirmacao) {
                            $.SmartMessageBox({
                                title: "Confirme a operação:",
                                content: 'Esta coleção está ligada a ' + colecaoTitulos + ' título' + (colecaoTitulos == 1 ? '' : 's') + '. Deseja realmente alterá-la?',
                                buttons: "[Cancelar][Ok]"
                            }, function (ButtonPress) {
                                if (ButtonPress == "Ok") {
                                    $("#colecao-form").data('confirmacaoEnvio', true);
                                    $("#colecao-form").submit();
                                }
                            });

                            return false;
                        }

                        $("#colecao-form").data('confirmacaoEnvio', false);
                    }

                    if (priv.options.ajaxSubmit) {
                        var dados = $form.serializeJSON();
                        dados['ajax'] = true;
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível salvar a coleção:",
                                        content: "<i>" + data.erro.join('<br>') + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });
                                } else if (priv.options.listagem) {
                                    $.colecaoIndex().getDataTableColecao().api().ajax.reload(null, false);
                                    $('#nova-colecao').modal('hide');
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Coleção salva!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    } else {
                        form.submit();
                    }
                },
                rules: {
                    colecaoDescricao: 'required',
                    editora: 'required'
                },
                messages: {
                    colecaoDescricao: 'Campo obrigatório!',
                    editora: 'Campo obrigatório!'
                },
                ignore: '.ignore'
            });
        };

        pub.setEditora = function (editora) {
            priv.options.editora = editora || null;
        };

        pub.getEditora = function () {
            return priv.options.editora;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.colecaoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.colecao.add");

        if (!obj) {
            obj = new ColecaoAdd();
            obj.run(params);
            $(window).data('universa.colecao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);