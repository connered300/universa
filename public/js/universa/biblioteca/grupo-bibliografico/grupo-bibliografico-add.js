(function ($, window, document) {
    'use strict';

    var GrupoBibliograficoAdd = function () {
        VersaShared.call(this);
        var __grupoBibliograficoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#grupo-bibliografico-wizard',
            formElement: '#grupo-bibliografico-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $grupoBibliograficoSituacao = $("#grupoBibliograficoSituacao");

            var arrSimNao = [{'id': 'Sim', 'text': 'Sim'}, {'id': 'Não', 'text': 'Não'}];

            $(
                "#grupoBibliograficoAcessoPortal," +
                "#grupoBibliograficoAutor," +
                "#grupoBibliograficoExemplares," +
                "#grupoBibliograficoLocalEdicao"
            ).select2({data: arrSimNao});

            $grupoBibliograficoSituacao.select2({
                allowClear: true,
                language: 'pt-BR'
            }).trigger("change");
        };

        this.setValidations = function () {
            __grupoBibliograficoAdd.options.validator.settings.rules = {
                grupoBibliograficoNome: {maxlength: 60, required: true},
                grupoBibliograficoAcessoPortal: {maxlength: 60, required: true},
                grupoBibliograficoAutor: {required: true},
                grupoBibliograficoExamplares: {required: true},
                grupoBibliograficoLocalEdicao: {required: true},
                grupoBibliograficoSituacao: {maxlength: 7, required: true}
            };
            __grupoBibliograficoAdd.options.validator.settings.messages = {
                grupoBibliograficoNome: {maxlength: 'Tamanho máximo: 60!', required: 'Campo obrigatório!'},
                grupoBibliograficoAcessoPortal: {required: 'Campo obrigatório!'},
                grupoBibliograficoAutor: {required: 'Campo obrigatório!'},
                grupoBibliograficoExamplares: {required: 'Campo obrigatório!'},
                grupoBibliograficoLocalEdicao: {required: 'Campo obrigatório!'},
                grupoBibliograficoSituacao: {maxlength: 'Tamanho máximo: 7!', required: 'Campo obrigatório!'}
            };

            $(__grupoBibliograficoAdd.options.formElement).submit(function (e) {
                var ok = $(__grupoBibliograficoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__grupoBibliograficoAdd.options.formElement);

                if (__grupoBibliograficoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __grupoBibliograficoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __grupoBibliograficoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__grupoBibliograficoAdd.options.listagem) {
                                    $.grupoBibliograficoIndex().reloadDataTableGrupoBibliografico();
                                    __grupoBibliograficoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#grupo-bibliografico-modal').modal('hide');
                                }

                                __grupoBibliograficoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaGrupoBibliografico = function (grupo_bibliografico_id, callback) {
            var $form = $(__grupoBibliograficoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __grupoBibliograficoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + grupo_bibliografico_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __grupoBibliograficoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __grupoBibliograficoAdd.removeOverlay($form);
                },
                erro: function () {
                    __grupoBibliograficoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __grupoBibliograficoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.grupoBibliograficoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.biblioteca.grupo-bibliografico.add");

        if (!obj) {
            obj = new GrupoBibliograficoAdd();
            obj.run(params);
            $(window).data('universa.biblioteca.grupo-bibliografico.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);