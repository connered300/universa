(function ($, window, document) {
    'use strict';
    var GrupoBibliograficoIndex = function () {
        VersaShared.call(this);
        var __grupoBibliograficoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                GrupoBibliografico: null
            }
        };

        this.setSteps = function () {
            var $dataTableGrupoBibliografico = $('#dataTableGrupoBibliografico');
            var colNum = -1;
            __grupoBibliograficoIndex.options.datatables.GrupoBibliografico = $dataTableGrupoBibliografico.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __grupoBibliograficoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __grupoBibliograficoIndex.createBtnGroup(btns);
                        }

                        __grupoBibliograficoIndex.removeOverlay($('#container-grupo-bibliografico'));

                        return data;
                    }
                },

                columnDefs: [
                    {name: "grupo_bibliografico_id", targets: ++colNum, data: "grupo_bibliografico_id"},
                    {name: "grupo_bibliografico_nome", targets: ++colNum, data: "grupo_bibliografico_nome"},
                    {name: "grupo_bibliografico_situacao", targets: ++colNum, data: "grupo_bibliografico_situacao"},
                    {
                        name: "grupo_bibliografico_local_edicao",
                        targets: ++colNum,
                        data: "grupo_bibliografico_local_edicao"
                    },
                    {name: "grupo_bibliografico_autor", targets: ++colNum, data: "grupo_bibliografico_autor"},
                    {
                        name: "grupo_bibliografico_acesso_portal",
                        targets: ++colNum,
                        data: "grupo_bibliografico_acesso_portal"
                    },
                    {
                        name: "grupo_bibliografico_id",
                        targets: ++colNum,
                        data: "acao",
                        orderable: false,
                        searchable: false
                    }
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableGrupoBibliografico.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __grupoBibliograficoIndex.options.datatables.GrupoBibliografico.fnGetData($pai);

                if (!__grupoBibliograficoIndex.options.ajax) {
                    location.href = __grupoBibliograficoIndex.options.url.edit + '/' + data['grupo_bibliografico_id'];
                } else {
                    __grupoBibliograficoIndex.addOverlay(
                        $('#container-grupo-bibliografico'), 'Aguarde, carregando dados para edição...'
                    );
                    $.grupoBibliograficoAdd().pesquisaGrupoBibliografico(
                        data['grupo_bibliografico_id'],
                        function (data) {
                            __grupoBibliograficoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-grupo-bibliografico-add').click(function (e) {
                if (__grupoBibliograficoIndex.options.ajax) {
                    __grupoBibliograficoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableGrupoBibliografico.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __grupoBibliograficoIndex.options.datatables.GrupoBibliografico.fnGetData($pai);
                var arrRemover = {
                    grupoBibliograficoId: data['grupo_bibliografico_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de grupo bibliográfico "' + data['grupo_bibliografico_nome'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __grupoBibliograficoIndex.addOverlay(
                                $('#container-grupo-bibliografico'), 'Aguarde, solicitando remoção de registro de grupo bibliográfico...'
                            );
                            $.ajax({
                                url: __grupoBibliograficoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __grupoBibliograficoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de grupo bibliográfico:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __grupoBibliograficoIndex.removeOverlay($('#container-grupo-bibliografico'));
                                    } else {
                                        __grupoBibliograficoIndex.reloadDataTableGrupoBibliografico();
                                        __grupoBibliograficoIndex.showNotificacaoSuccess(
                                            "Registro de grupo bibliográfico removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {

            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#grupo-bibliografico-form'),
                $modal = $('#grupo-bibliografico-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['grupoBibliograficoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['grupoBibliograficoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $form[0].reset();
            $form.deserialize(data);

            $(
                "#grupoBibliograficoAcessoPortal," +
                "#grupoBibliograficoAutor," +
                "#grupoBibliograficoExemplares," +
                "#grupoBibliograficoLocalEdicao"
            ).select2('val', 'Sim').trigger("change");
            $("#grupoBibliograficoSituacao").select2("val", 'Inativo').trigger("change");

            if (data['grupoBibliograficoId']) {
                var $grupoBibliograficoSituacao = $("#grupoBibliograficoSituacao");
                $grupoBibliograficoSituacao.select2('val', data['grupoBibliograficoSituacao']).trigger("change");
                $("#grupoBibliograficoAcessoPortal").select2('val', data['grupoBibliograficoAcessoPortal']).trigger("change");
                $("#grupoBibliograficoAutor").select2('val', data['grupoBibliograficoAutor']).trigger("change");
                $("#grupoBibliograficoExemplares").select2('val', data['grupoBibliograficoExemplares']).trigger("change");
                $("#grupoBibliograficoLocalEdicao").select2('val', data['grupoBibliograficoLocalEdicao']).trigger("change");
            }

            $modal.find('.modal-title').html(actionTitle + ' grupo bibliográfico');
            $modal.modal();
            __grupoBibliograficoIndex.removeOverlay($('#container-grupo-bibliografico'));
        };

        this.reloadDataTableGrupoBibliografico = function () {
            this.getDataTableGrupoBibliografico().api().ajax.reload();
        };

        this.getDataTableGrupoBibliografico = function () {
            if (!__grupoBibliograficoIndex.options.datatables.GrupoBibliografico) {
                if (!$.fn.dataTable.isDataTable('#dataTableGrupoBibliografico')) {
                    __grupoBibliograficoIndex.options.datatables.GrupoBibliografico = $('#dataTableGrupoBibliografico').DataTable();
                } else {
                    __grupoBibliograficoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __grupoBibliograficoIndex.options.datatables.GrupoBibliografico;
        };

        this.run = function (opts) {
            __grupoBibliograficoIndex.setDefaults(opts);
            __grupoBibliograficoIndex.setSteps();
        };
    };

    $.grupoBibliograficoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.biblioteca.grupo-bibliografico.index");

        if (!obj) {
            obj = new GrupoBibliograficoIndex();
            obj.run(params);
            $(window).data('universa.biblioteca.grupo-bibliografico.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);