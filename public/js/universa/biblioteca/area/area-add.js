(function ($, window, document) {
    'use strict';

    var AreaAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                ajaxSubmit: false,
                listagem: false
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {};

        priv.setValidations = function () {
            $("#area-form").validate({
                submitHandler: function (form) {
                    var areaTitulos = $('#areaTitulos').val() || 0;
                    var $form = $(form);

                    if (areaTitulos > 0) {
                        var confirmacao = $form.data('confirmacaoEnvio') || false;

                        if (!confirmacao) {
                            $.SmartMessageBox({
                                title: "Confirme a operação:",
                                content: 'Esta área está ligada a ' + areaTitulos + ' título' + (areaTitulos == 1 ? '' : 's') + '. Deseja realmente alterá-la?',
                                buttons: "[Cancelar][Ok]"
                            }, function (ButtonPress) {
                                if (ButtonPress == "Ok") {
                                    $("#area-form").data('confirmacaoEnvio', true);
                                    $("#area-form").submit();
                                }
                            });

                            return false;
                        }

                        $("#area-form").data('confirmacaoEnvio', false);
                    }

                    if (priv.options.ajaxSubmit) {
                        var dados = $form.serializeJSON();
                        dados['ajax'] = true;
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível salvar a área:",
                                        content: "<i>" + data.erro.join('<br>') + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });
                                } else if (priv.options.listagem) {
                                    $.areaIndex().getDataTableArea().api().ajax.reload(null, false);
                                    $('#nova-area').modal('hide');
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Área salva!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    } else {
                        form.submit();
                    }
                },
                rules: {
                    areaDescricao: 'required'
                },
                messages: {
                    areaDescricao: 'Campo obrigatório!'
                },
                ignore: '.ignore'
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.areaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.area.add");

        if (!obj) {
            obj = new AreaAdd();
            obj.run(params);
            $(window).data('universa.area.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);