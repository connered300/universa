(function ($, window, document) {
    'use strict';

    var AreaIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#area-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['area_id']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $(".has-error .help-block").hide();
            $(".has-error").removeClass("has-error");

            $('#nova-area .modal-title').html(actionTitle + ' área');
            $('#areaDescricao').val(data['area_descricao'] || '');
            $('#areaId').val(data['area_id'] || '');
            $('#areaTitulos').val(data['area_titulos'] || '');
            $('#nova-area').modal();
        };

        priv.setSteps = function () {
            priv.dataTableArea = $('#dataTableArea').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        return json.data;
                    }
                },
                columnDefs: [
                    {name: "area_descricao", targets: 0, data: "area_descricao"},
                    {name: "area_titulos", targets: 1, data: "area_titulos"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0]]
            });

            $(document).on("click", "#dataTableArea tbody tr", function (event) {
                var position = priv.dataTableArea.fnGetPosition(this);
                var data = priv.dataTableArea.fnGetData(position);
                priv.showModal(data)

            });

            $('#btn-nova-area').click(function () {
                priv.showModal()
            });
        };

        pub.getDataTableArea = function () {
            return priv.dataTableArea;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.areaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.area.index");

        if (!obj) {
            obj = new AreaIndex();
            obj.run(params);
            $(window).data('universa.area.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);