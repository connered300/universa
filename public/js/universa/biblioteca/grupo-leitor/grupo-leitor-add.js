(function ($, window, document) {
    'use strict';

    var GrupoLeitorAdd = function () {
        VersaShared.call(this);
        var __grupoLeitorAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {acessoGrupo: ''},
            data: {},
            value: {acessoGrupo: null},
            datatables: {},
            wizardElement: '#grupo-leitor-wizard',
            formElement: '#grupo-leitor-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $acessoGrupo = $("#acessoGrupo");
            $acessoGrupo.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __grupoLeitorAdd.options.url.acessoGrupo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.grup_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $acessoGrupo.select2("data", __grupoLeitorAdd.getAcessoGrupo());
        };

        this.setAcessoGrupo = function (acessoGrupo) {
            this.options.value.acessoGrupo = acessoGrupo || null;
        };

        this.getAcessoGrupo = function () {
            return this.options.value.acessoGrupo || null;
        };
        this.setValidations = function () {
            __grupoLeitorAdd.options.validator.settings.rules = {
                grupoLeitorNome: {maxlength: 30, required: true},
                acessoGrupo: {number: true}
            };
            __grupoLeitorAdd.options.validator.settings.messages = {
                grupoLeitorNome: {maxlength: 'Tamanho máximo: 30!', required: 'Campo obrigatório!'},
                acessoGrupo: {number: 'Número inválido!'}
            };

            $(__grupoLeitorAdd.options.formElement).submit(function (e) {
                var ok = $(__grupoLeitorAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__grupoLeitorAdd.options.formElement);

                if (__grupoLeitorAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __grupoLeitorAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __grupoLeitorAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__grupoLeitorAdd.options.listagem) {
                                    $.grupoLeitorIndex().reloadDataTableGrupoLeitor();
                                    __grupoLeitorAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#grupo-leitor-modal').modal('hide');
                                }

                                __grupoLeitorAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaGrupoLeitor = function (grupo_leitor_id, callback) {
            var $form = $(__grupoLeitorAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __grupoLeitorAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + grupo_leitor_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __grupoLeitorAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __grupoLeitorAdd.removeOverlay($form);
                },
                erro: function () {
                    __grupoLeitorAdd.showNotificacaoDanger('Erro desconhecido!');
                    __grupoLeitorAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.grupoLeitorAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.biblioteca.grupo-leitor.add");

        if (!obj) {
            obj = new GrupoLeitorAdd();
            obj.run(params);
            $(window).data('universa.biblioteca.grupo-leitor.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);