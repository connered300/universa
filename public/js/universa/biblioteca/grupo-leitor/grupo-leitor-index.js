(function ($, window, document) {
    'use strict';
    var GrupoLeitorIndex = function () {
        VersaShared.call(this);
        var __grupoLeitorIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                GrupoLeitor: null
            }
        };

        this.setSteps = function () {
            var $dataTableGrupoLeitor = $('#dataTableGrupoLeitor');
            var colNum = -1;
            __grupoLeitorIndex.options.datatables.GrupoLeitor = $dataTableGrupoLeitor.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __grupoLeitorIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __grupoLeitorIndex.createBtnGroup(btns);
                        }

                        __grupoLeitorIndex.removeOverlay($('#container-grupo-leitor'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "grupo_leitor_id", targets: ++colNum, data: "grupo_leitor_id"},
                    {name: "grupo_leitor_nome", targets: ++colNum, data: "grupo_leitor_nome"},
                    {name: "acesso_grupo_id", targets: ++colNum, data: "acesso_grupo_id"},
                    {name: "grupo_leitor_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableGrupoLeitor.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __grupoLeitorIndex.options.datatables.GrupoLeitor.fnGetData($pai);

                if (!__grupoLeitorIndex.options.ajax) {
                    location.href = __grupoLeitorIndex.options.url.edit + '/' + data['grupo_leitor_id'];
                } else {
                    __grupoLeitorIndex.addOverlay(
                        $('#container-grupo-leitor'), 'Aguarde, carregando dados para edição...'
                    );
                    $.grupoLeitorAdd().pesquisaGrupoLeitor(
                        data['grupo_leitor_id'],
                        function (data) {
                            __grupoLeitorIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-grupo-leitor-add').click(function (e) {
                if (__grupoLeitorIndex.options.ajax) {
                    __grupoLeitorIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableGrupoLeitor.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __grupoLeitorIndex.options.datatables.GrupoLeitor.fnGetData($pai);
                var arrRemover = {
                    grupoLeitorId: data['grupo_leitor_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de grupo leitor "' + data['grupo_leitor_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __grupoLeitorIndex.addOverlay(
                                $('#container-grupo-leitor'), 'Aguarde, solicitando remoção de registro de grupo leitor...'
                            );
                            $.ajax({
                                url: __grupoLeitorIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __grupoLeitorIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de grupo leitor:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __grupoLeitorIndex.removeOverlay($('#container-grupo-leitor'));
                                    } else {
                                        __grupoLeitorIndex.reloadDataTableGrupoLeitor();
                                        __grupoLeitorIndex.showNotificacaoSuccess(
                                            "Registro de grupo leitor removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#grupo-leitor-form'),
                $modal = $('#grupo-leitor-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['grupoLeitorId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['grupoLeitorId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $acessoGrupo = $("#acessoGrupo");
            $acessoGrupo.select2('data', data['acessoGrupo']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' grupo leitor');
            $modal.modal();
            __grupoLeitorIndex.removeOverlay($('#container-grupo-leitor'));
        };

        this.reloadDataTableGrupoLeitor = function () {
            this.getDataTableGrupoLeitor().api().ajax.reload();
        };

        this.getDataTableGrupoLeitor = function () {
            if (!__grupoLeitorIndex.options.datatables.GrupoLeitor) {
                if (!$.fn.dataTable.isDataTable('#dataTableGrupoLeitor')) {
                    __grupoLeitorIndex.options.datatables.GrupoLeitor = $('#dataTableGrupoLeitor').DataTable();
                } else {
                    __grupoLeitorIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __grupoLeitorIndex.options.datatables.GrupoLeitor;
        };

        this.run = function (opts) {
            __grupoLeitorIndex.setDefaults(opts);
            __grupoLeitorIndex.setSteps();
        };
    };

    $.grupoLeitorIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.biblioteca.grupo-leitor.index");

        if (!obj) {
            obj = new GrupoLeitorIndex();
            obj.run(params);
            $(window).data('universa.biblioteca.grupo-leitor.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);