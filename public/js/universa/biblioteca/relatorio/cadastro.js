
$(document).ready(function(){
    $('#dataInicial, #dataFinal')
        .datepicker({prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'})
        .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

    $('#editora').select2({
        ajax:{
            url: window.urlEditora||'',
            dataType: 'json',
            data: function (query) { return {query: query};},
            results: function (data) {
                var transformed = $.map(data, function (el) {
                    el.text = el.pes_nome_fantasia;
                    el.id = el.pes_id;

                    return el;
                });

                return {results: transformed};
            }
        },
        multiple: true
    });

    $('#grupoBibliografico').select2({
        data: window.dataGrupoBibliografico,
        multiple: true
    });

    $('#usuario').select2({
        ajax:{
            url: window.urlPessoa||'',
            dataType: 'json',
            data: function (query) { return {query: query};},
            results: function (data){
                var resposta = $.map(data, function(resp){
                    resp.text = resp.pes_nome;
                    resp.id = resp.id;

                    return resp;
                });

                return {results: resposta};
            }
        },
    });
});