(function ($, window, document) {
    'use strict';

    var ExemplarRelatorioBaixa = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                url: {
                    assunto: '',
                    areaCnpq: '',
                    empresa: '',
                    pessoa: ''
                },
                data: {
                    grupoBibliografico: []
                },
                value: {
                    grupoBibliografico: null
                }
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {
            var grupoBibliografico = $('#grupoBibliografico').select2({
                data: priv.options.data.grupoBibliografico,
                allowClear: true,
                tags: true
            });

            var editora = $('#editora').select2({
                ajax: {
                    url: priv.options.url.empresa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) { return {query: query, empresaTipo: 'Editora'};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome_fantasia;
                            el.id = el.pes_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                tags: true,
                minimumInputLength: 1
            });
            var pessoa = $('#usuario').select2({
                ajax: {
                    url: priv.options.url.pessoa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) { return {query: query};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome;
                            el.id = el.id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                tags: true,
                minimumInputLength: 1
            });

            $('#dataInicial, #dataFinal')
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
        };

        priv.setValidations = function () {
            $("#exemplar-baixa-form").validate({
                rules: {
                    dataInicial: {required: true, dateBR: true},
                    dataFinal: {required: true, dateBR: true}
                },
                messages: {
                    dataInicial: {required: 'Campo obrigatório!'},
                    dataFinal: {required: 'Campo obrigatório!'}
                },
                ignore: '.ignore'
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.exemplarRelatorioBaixa = function (params) {
        params = params || [];

        var obj = $(window).data("universa.relatorio.exemplar.baixa");

        if (!obj) {
            obj = new ExemplarRelatorioBaixa();
            obj.run(params);
            $(window).data('universa.relatorio.exemplar.baixa', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);