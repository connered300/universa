(function ($, window, document) {
    'use strict';

    var TituloRanking = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                url: {
                    assunto: '',
                    areaCnpq: '',
                    empresa: '',
                    autor: ''
                },
                data: {
                    grupoBibliografico: []
                },
                value: {
                    grupoBibliografico: null
                }
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
            $.storage = new $.store();
        };


        priv.makeSelect2Field = function (settings) {
            settings = settings || [];
            settings = $.extend(true, {
                fieldId: '',
                optionsAttr: '',
                dataAttr: '',
                ajax: false,
                data: null,
                select2Extra: {},
                changeCallback: null
            }, settings);

            if (!settings.dataAttr) {
                settings.dataAttr = settings.optionsAttr;
            }

            return {
                settings: settings,
                setValue: function (option) {
                    option = option || null;
                    priv.options.value[this.settings.optionsAttr] = option;
                    $(this.settings.fieldId).select2("val", option);
                },
                setValueBySearch: function (search) {
                    var data = priv.options.data[this.settings.dataAttr] || null;

                    if (!data) {
                        return;
                    }

                    var selection = $.grep(data, function (el) {
                        if (el.id == search || el.text.toLowerCase() == search.toLowerCase()) {
                            return el;
                        }
                    });

                    if (!this.settings.select2Extra.multiple && !this.settings.select2Extra.tags) {
                        selection = selection[0] || null;
                    }

                    this.setValue(selection || null);
                },
                getValue: function () {
                    return priv.options.value[this.settings.optionsAttr];
                },
                init: function () {
                    var select2Helper = this;
                    var select2Options = {
                        language: 'pt-BR',
                        allowClear: true,
                        initSelection: function (element, callback) {
                            callback(select2Helper.getValue());
                        }
                    };

                    if (this.settings.ajax) {
                        select2Options['ajax'] = {url: '', dataType: 'json', delay: 250, data: null, results: ''};
                        select2Options['ajax'] = $.extend(true, select2Options['ajax'], this.settings.ajax);
                        delete select2Options['data'];
                    } else if (this.settings.data) {
                        select2Options['data'] = this.settings.data;
                        delete select2Options['ajax'];
                    } else {
                        delete select2Options['ajax'];
                        delete select2Options['data'];
                    }

                    if (this.settings.select2Extra) {
                        select2Options = $.extend(true, select2Options, this.settings.select2Extra);
                    }

                    $(this.settings.fieldId).select2(select2Options);

                    if (this.settings.select2Extra.allowClear) {
                        $(this.settings.fieldId).on('change select2-selected select2-clearing select2-removed', function () {
                            var $item = $(this);

                            if ($item.val() != '') {
                                $item.parent().find('.select2-container').addClass('select2-allowclear');
                            } else {
                                $item.parent().find('.select2-container').removeClass('select2-allowclear');
                            }
                        });
                    }

                    if (this.settings.changeCallback) {
                        $(this.settings.fieldId).on('change', this.settings.changeCallback);
                    }
                    $(this.settings.fieldId).select2("val", select2Helper.getValue());
                    $(this.settings.fieldId).trigger('change');
                }
            };
        };

        priv.setFilters = function () {
            var grupoBibliografico = $('#grupoBibliografico').select2({
                data: priv.options.data.grupoBibliografico,
                allowClear: true,
                tags: true
            });

            var editora = $('#editora').select2({
                ajax: {
                    url: priv.options.url.empresa,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) { return {query: query, empresaTipo: 'Editora'};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome_fantasia;
                            el.id = el.pes_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                tags: true,
                minimumInputLength: 1
            });

            var autor = priv.makeSelect2Field({
                fieldId: '#autor', optionsAttr: 'autor', ajax: {
                    url: priv.options.url.autor,
                    dataType: 'json', delay: 250,
                    data: function (query) { return {query: query};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.autor_referencia;
                            el.id = el.autor_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var assunto = priv.makeSelect2Field({
                fieldId: '#assunto', optionsAttr: 'assunto', ajax: {
                    url: priv.options.url.assunto,
                    dataType: 'json', delay: 250,
                    data: function (query) { return {query: query};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.assunto_descricao;
                            el.id = el.assunto_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var areaCnpq = priv.makeSelect2Field({
                fieldId: '#areaCnpq', optionsAttr: 'areaCnpq', ajax: {
                    url: priv.options.url.areaCnpq,
                    dataType: 'json', delay: 250,
                    data: function (query) { return {query: query};},
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_cnpq_codigo + ': ' + el.area_cnpq_descricao, id: el.area_cnpq_id};
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            grupoBibliografico.init();
            editora.init();
            autor.init();
            assunto.init();
            areaCnpq.init();

            var filtrosSalvos = $.storage.get('universa.relatorio.titulo.ranking.filtros') || {};

            $('#dataInicial, #dataFinal')
                .datepicker({prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'})
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

            $('#numeroRegistros').inputmask({
                showMaskOnHover: false, clearIncomplete: true, alias: 'integer'
            });

            if (filtrosSalvos['grupoBibliografico']) {
                $('#grupoBibliografico').select2('data', filtrosSalvos['grupoBibliografico']);
            }

            if (filtrosSalvos['editora']) {
                $('#editora').select2('data', filtrosSalvos['editora']);
            }

            if (filtrosSalvos['autor']) {
                $('#autor').select2('data', filtrosSalvos['autor']);
            }

            if (filtrosSalvos['assunto']) {
                $('#assunto').select2('data', filtrosSalvos['assunto']);
            }

            if (filtrosSalvos['areaCnpq']) {
                $('#areaCnpq').select2('data', filtrosSalvos['areaCnpq']);
            }

            if (filtrosSalvos['dataInicial']) {
                $('#dataInicial').val(filtrosSalvos['dataInicial']);
            }

            if (filtrosSalvos['dataFinal']) {
                $('#dataFinal').val(filtrosSalvos['dataFinal']);
            }

            if (filtrosSalvos['numeroRegistros']) {
                $('#numeroRegistros').val(filtrosSalvos['numeroRegistros']);
            } else {
                $('#numeroRegistros').val(10);
            }

            if (filtrosSalvos['rankingTipo']) {
                $('[name=rankingTipo][value=' + filtrosSalvos['rankingTipo'] + ']').prop('checked', true);
            } else {
                $('[name=rankingTipo]:first').prop('checked', true);
            }

            if (filtrosSalvos['ocultarTituloZerado']) {
                $('#ocultarTituloZerado').prop('checked', true);
            } else {
                $('#ocultarTituloZerado').prop('checked', false);
            }
        };

        priv.setSteps = function () {
        };

        priv.setValidations = function () {
            $("#titulo-ranking-form").validate({
                submitHandler: function (form) {
                    var dataInicial = $("#dataInicial").val();
                    var dataFinal = $("#dataFinal").val();

                    dataInicial = dataInicial.replace(/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/g, "$3$2$1");
                    dataFinal = dataFinal.replace(/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/g, "$3$2$1");
                    dataInicial = parseInt(dataInicial);
                    dataFinal = parseInt(dataFinal);

                    if (dataFinal <= dataInicial) {
                        $.smallBox({
                            title: "Erro:",
                            content: "<i>A data inicial deve ser menor que a data final!</i>",
                            color: "#C46A69",
                            icon: "fa fa-times",
                            timeout: 10000
                        });
                    } else {
                        var filtrosSalvos = {
                            grupoBibliografico: $('#grupoBibliografico').select2('data') || "",
                            editora: $('#editora').select2('data') || "",
                            autor: $('#autor').select2('data') || "",
                            assunto: $('#assunto').select2('data') || "",
                            areaCnpq: $('#areaCnpq').select2('data') || "",
                            dataInicial: $('#dataInicial').val() || "",
                            rankingTipo: $('input[name=rankingTipo]:checked').val() || "",
                            dataFinal: $('#dataFinal').val() || "",
                            numeroRegistros: $('#numeroRegistros').val() || "",
                            ocultarTituloZerado: $('#ocultarTituloZerado').prop('checked') ? 's' : ''
                        };

                        $.storage.set('universa.relatorio.titulo.ranking.filtros', filtrosSalvos);

                        form.submit();
                    }
                },
                rules: {
                    dataInicial: {required: true, dateBR: true},
                    dataFinal: {required: true, dateBR: true},
                    numeroRegistros: {required: true, min: 5, max: 200}
                },
                messages: {
                    dataInicial: {required: 'Campo obrigatório!'},
                    dataFinal: {required: 'Campo obrigatório!'},
                    numeroRegistros: {
                        required: 'Campo obrigatório!',
                        min: 'O valor deve maior que 5.',
                        max: 'O valor deve ser menor que 200.'
                    }
                },
                ignore: '.ignore'
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setFilters();
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.tituloRanking = function (params) {
        params = params || [];

        var obj = $(window).data("universa.relatorio.titulo.ranking");

        if (!obj) {
            obj = new TituloRanking();
            obj.run(params);
            $(window).data('universa.relatorio.titulo.ranking', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);