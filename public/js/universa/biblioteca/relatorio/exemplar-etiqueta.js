(function ($, window, document) {
    'use strict';

    var ExemplarEtiqueta = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            data: [],
            options: {}
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        $(document).on('ready', function () {
            $("#etiquetaLayout").select2({
                data: priv.options.data
            })
        });

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {
            $('#exemplarIntervalo').inputmask('Regex', {
                'regex': "\\d+(?:-\\d+)?(?:,\\d+(?:-\\d+)?)*"
            }).change(function () {
                var total = 0, intervalos = $(this).val();
                intervalos = intervalos.trim().split(',');

                for (var i in intervalos) {
                    var inicio = intervalos[i].split('-');
                    var fim = null;

                    if (inicio.length > 1) {
                        fim = inicio[1];
                        inicio = inicio[0];

                        if (fim > inicio) {
                            total += (fim - inicio) + 1;
                        } else if (inicio > fim) {
                            total += (inicio - fim) + 1;
                        } else {
                            total += 1;
                        }
                    } else {
                        total += 1;
                    }
                }

                $(this).next('.informacao').html(total + ' exemplares');
            });
        };

        priv.setValidations = function () {
            $("#exemplar-etiquetas-form").validate({
                rules: {
                    exemplarIntervalo: {required: true},
                    etiquetaLayout: {required: true}
                },
                messages: {
                    exemplarIntervalo: {required: 'Campo obrigatório!'},
                    etiquetaLayout: {required: 'Campo obrigatório!'}
                },
                ignore: '.ignore'
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.exemplarEtiqueta = function (params) {
        params = params || [];

        var obj = $(window).data("universa.relatorio.exemplar.etiqueta");

        if (!obj) {
            obj = new ExemplarEtiqueta();
            obj.run(params);
            $(window).data('universa.relatorio.exemplar.etiqueta', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);