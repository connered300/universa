$(document).ready(function(){
    $('#dataInicial, #dataFinal')
        .datepicker({prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'})
        .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

    $('#grupoBibliografico').select2({
        data: window.grupoBibliografico,
        multiple: true
    });

    $('#leitor').select2({
        ajax: {
            url: window.aluno,
            dataType: 'json',
            data: function (carrie){return {query: carrie};},
            results: function(data){
                var maped = $.map(data,function(t){
                    t.text = t.pesNome;
                    t.id = t.pesId;

                    return t;
                });

                return {results: maped};
            }
        },
        multiple: true
    });

    $('#editora').select2({
        ajax: {
            url: window.empresa,
            dataType: 'json',
            data: function (carrie){return {query: carrie};},
            results: function(data){
                var maped = $.map(data,function(t){
                    t.text = t.pes_nome_fantasia;
                    t.id = t.pes_id;

                    return t;
                });

                return {results: maped};
            }
        },
        multiple: true
    });

    $('#user').select2({
        ajax: {
            url: window.pessoa,
            dataType: 'json',
            data: function (carrie){return {query: carrie};},
            results: function(data){
                var maped = $.map(data,function(t){
                    t.text = t.pes_nome;
                    t.id = t.id;

                    return t;
                });

                return {results: maped};
            }
        },
        multiple: true,
        minimumInputLength: 3
    });

});
