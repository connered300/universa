$(document).ready(function(){
    $('#dataInicial, #dataFinal')
        .datepicker({prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'})
        .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

    $('#grupoBibliografico').select2({
        data: window.dataGrupoBibliografico,
        multiple: true
    });


    $('#usuario').select2({
        ajax:{
            url: window.urlPessoa,
            dataType: 'json',
            data: function(query){return {query:query}},
            results: function(data){
                var resultado = $.map(data,function(elements){
                    elements.text = elements.pes_nome;
                    elements.id = elements.id;
                    return elements;
                });
                return {results:resultado};
            }
        },
        minimumInputLength: 3
    });

    validaForm();
});

function countChecked(){
    var num = $("input[type=checkbox]:checked").length;
    return (num > 0);
}

function validaForm(){
    $("#gerar").click(function(evento){
        if(!countChecked()){
            evento.preventDefault();
            $.smallBox({
                title: "<b>Tipo de Operação</b>",
                content: "Selecione pelo menos um Tipo de Operação",
                color: "#C46A69",
                icon: "fa fa-times",
                timeout: 10000
            });
        }
    });
}
