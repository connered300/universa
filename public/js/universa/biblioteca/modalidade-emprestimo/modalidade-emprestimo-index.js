(function ($, window, document) {
    'use strict';

    var ModalidadeEmprestimoIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.setSteps = function () {
            priv.dataTableModalidadeEmprestimo = $('#dataTableModalidadeEmprestimo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        return json.data;
                    }
                },
                columnDefs: [
                    {name: "modalidade_emprestimo_descricao", targets: 0, data: "modalidade_emprestimo_descricao"},
                    {name: "grupo_leitor_nome", targets: 1, data: "grupo_leitor_nome"},
                    {name: "modalidade_emprestimo_situacao", targets: 2, data: "modalidade_emprestimo_situacao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": [],
                    "sSwfPath": "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0]]
            });

            $(document).on("click", "#dataTableModalidadeEmprestimo tbody tr", function (event) {
                var position = priv.dataTableModalidadeEmprestimo.fnGetPosition(this);
                var data = priv.dataTableModalidadeEmprestimo.fnGetData(position);

                location.href = priv.options.urlEdit + '/' + data['modalidade_emprestimo_id'];
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.modalidadeEmprestimoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.modalidade-emprestimo.index");

        if (!obj) {
            obj = new ModalidadeEmprestimoIndex();
            obj.run(params);
            $(window).data('universa.modalidade-emprestimo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);