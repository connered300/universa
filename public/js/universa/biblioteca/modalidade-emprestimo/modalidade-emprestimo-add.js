(function ($, window, document) {
    'use strict';

    var ModalidadeEmprestimoAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                modalidadeEmprestimoGrupos: null,
                gruposSelecionados: null,
                grupoBibliografico: {}
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {
            priv.steps[0] = function () {
                var getGrupoBibliografico = function () {
                    var itens = $("#modalidadeEmprestimoGrupos").componente('exportJSON');
                    var grupoBibliograficoSelecionados = [];

                    $.map(itens, function (el) {
                        if (el.grupoBibliografico) {
                            grupoBibliograficoSelecionados = $.merge(grupoBibliograficoSelecionados, el.grupoBibliografico.split(','));
                        }
                    });

                    var grupoBibliografico = [];

                    $.grep(priv.options.grupoBibliografico, function (el) {
                        var modalidadeEmprestimoGrupo = el.id + "";

                        if ($.inArray(modalidadeEmprestimoGrupo, grupoBibliograficoSelecionados) == -1) {
                            grupoBibliografico.push(el);
                        }
                    });

                    return grupoBibliografico;
                };
                $('#grupoLeitor').select2({language: 'pt-BR'});
                $('#modalidadeEmprestimoDiasSuspensao').maskMoney({precision: 0, thousands: '', decimal: '', allowZero: true});

                $("#modalidadeEmprestimoGrupos").componente({
                    inputName: 'modalidadeEmprestimoGrupo',
                    classCardExtra: 'col-sm-12',
                    labelContainer: '<div class="form-group"/>',
                    tplItem: '\
                        <div class="well">\
                            <div class="modalidadeEmprestimoGrupo-remover">\
                                <a href="#" class="btn btn-danger btn-xs item-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </a>\
                             </div>\
                            <div class="componente-content">\
                            </div >\
                        </div>',
                    fieldClass: 'form-control',
                    fields: [
                        {name: 'modalidadeEmprestimoGrupoId', label: 'id', type: 'hidden'},
                        {
                            name: 'grupoBibliografico', label: 'Grupo bibliográfico', type: 'text',
                            addCallback: function (e) {
                                var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                var $parent = field.parent();
                                $parent.addClass('col-sm-2');

                                var select2Options = {
                                    language: 'pt-BR',
                                    data: function () {
                                        return {results: getGrupoBibliografico()};
                                    }
                                };

                                if (itemConfig['grupoBibliografico']) {
                                    select2Options.initSelection = function (element, callback) {
                                        callback(itemConfig['grupoBibliografico']);
                                    };
                                }

                                field.select2(select2Options);

                                if (itemConfig['grupoBibliografico']) {
                                    field.select2("val", itemConfig['grupoBibliografico']).trigger('change');
                                }
                            }
                        },
                        {
                            name: 'modalidadeEmprestimoNumeroVolumes', label: 'Número de exemplares', type: 'text',
                            addCallback: function (e) {
                                var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                var $parent = field.parent();
                                $parent.addClass('col-sm-2');
                                field.maskMoney({precision: 0, thousands: '', decimal: ''});
                            }
                        },
                        {
                            name: 'modalidadeEmprestimoPrazoMaximo', label: 'Prazo de entrega(dias)', type: 'text',
                            addCallback: function (e) {
                                var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                var $parent = field.parent();
                                $parent.addClass('col-sm-2');
                                field.maskMoney({precision: 0, thousands: '', decimal: ''});
                            }
                        },
                        {
                            name: 'modalidadeEmprestimoValorMulta', label: 'Valor da multa', type: 'text',
                            addCallback: function (e) {
                                var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                var $parent = field.parent();
                                $parent.addClass('col-sm-2');
                                field.maskMoney({precision: 2, thousands: '', decimal: ',', allowZero: true});
                            }
                        },
                        {
                            name: 'modalidadeEmprestimoDiasNaoUteis', label: 'Dias considerados na multa',
                            addCallback: function (e) {
                                var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                var $parent = field.parent();
                                $parent.addClass('col-sm-2');

                                var select2Options = {
                                    language: 'pt-BR',
                                    multiple: true,
                                    data: [
                                        {id: 'Sabado', 'text': 'Sábado'},
                                        {id: 'Domingo', 'text': 'Domingo'},
                                        {id: 'Feriado', 'text': 'Feriados'}
                                    ]
                                };

                                if (itemConfig['modalidadeEmprestimoDiasNaoUteis']) {
                                    select2Options.initSelection = function (element, callback) {
                                        callback(itemConfig['modalidadeEmprestimoDiasNaoUteis']);
                                    };
                                }

                                field.select2(select2Options);

                                if (itemConfig['modalidadeEmprestimoDiasNaoUteis']) {
                                    field.select2("val", itemConfig['modalidadeEmprestimoDiasNaoUteis']);
                                    field.trigger('change');
                                }
                            }
                        },
                        {
                            name: 'modalidadeEmprestimoTempoReserva', label: 'Validade de reserva(horas)', type: 'text',
                            addCallback: function (e) {
                                var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                var $parent = field.parent();
                                $parent.addClass('col-sm-2');
                                field.maskMoney({precision: 0, thousands: '', decimal: '', allowZero: true});
                            }
                        }
                    ],
                    removeCallback: function (parametersCallback) {
                        var item     = parametersCallback.item,
                            event    = parametersCallback.event,
                            settings = parametersCallback.settings,
                            data     = parametersCallback.data;

                        var confirmacao = item.data('confirmacaoRemocao') || false;

                        if (!confirmacao) {
                            $.SmartMessageBox({
                                title: "Confirme a operação:",
                                content: 'Deseja realmente remover esta regra da modalidade?',
                                buttons: "[Cancelar][Ok]"
                            }, function (ButtonPress) {
                                if (ButtonPress == "Ok") {
                                    $(item).data('confirmacaoRemocao', true);
                                    $(event.currentTarget).click();
                                }
                            });

                            return true;
                        }

                        return false;
                    }

                });

                if (priv.options.modalidadeEmprestimoGrupos) {
                    $.each(priv.options.modalidadeEmprestimoGrupos, function (i, item) {
                        $('#modalidadeEmprestimoGrupos').componente(['add', item]);
                    });
                }
            };
        };

        priv.loadSteps = function (begin, end) {
            begin = begin || 0;
            end = end || priv.steps.length;

            for (var i = begin; i <= end; i++) {
                var action = priv.__getFunc(priv.steps, i);
                if (action) {
                    action();
                }
            }
        };

        priv.setValidations = function () {
            var submitHandler = function (form) {
                var componente = $("#modalidadeEmprestimoGrupos").componente();
                var modalidadeEmprestimoGrupos = componente.items();
                var removeErro = function (elemento) {
                    if (elemento.find('>.component-item').length > 0) {
                        elemento.find('>.component-item').removeClass('bg-danger');
                        elemento.find('>.component-item .form-group .help-block').remove();
                    } else {
                        elemento.removeClass('bg-danger');
                    }
                };

                var addErro = function (elemento) {
                    var $card = null;

                    if (elemento.find('>.component-item').length > 0) {
                        $card = elemento.find('>.component-item');
                        $card.addClass('bg-danger');
                    } else {
                        $card = elemento;
                        $card.addClass('bg-danger');
                    }
                };

                var errors = [];

                if (modalidadeEmprestimoGrupos.length < 1) {
                    errors.push('Inclua ao menos uma regra na modalidade de emprestimo!');
                }

                $.each(modalidadeEmprestimoGrupos, function (i, item) {
                    var $item = $(item);
                    var id = $(item).data('componente.id');
                    removeErro($item);

                    var campoGrupoBibliografico = $item.find('[name="' + componente.makeFieldName({id: id, name: 'grupoBibliografico'}) + '"]');
                    var campoNumeroMaximoItens = $item.find('[name="' + componente.makeFieldName({id: id, name: 'modalidadeEmprestimoNumeroVolumes'}) + '"]');
                    var campoNumeroMaximoDias = $item.find('[name="' + componente.makeFieldName({id: id, name: 'modalidadeEmprestimoPrazoMaximo'}) + '"]');
                    //var campoValorMultaAtraso = $item.find('[name="' + componente.makeFieldName({id: id, name: 'modalidadeEmprestimoValorMulta'}) + '"]');
                    //var campoDiasContabilizarMulta = $item.find('[name="' + componente.makeFieldName({id: id, name: 'modalidadeEmprestimoDiasNaoUteis'}) + '"]');
                    var campoTempoMaximoParaReserva = $item.find('[name="' + componente.makeFieldName({id: id, name: 'modalidadeEmprestimoTempoReserva'}) + '"]');

                    var camposVazios = [];

                    if (campoGrupoBibliografico.val() == "") {
                        camposVazios.push('grupo bibliográfico');
                    }
                    if (campoNumeroMaximoItens.val() == "") {
                        camposVazios.push('número exemplares');
                    }
                    if (campoNumeroMaximoDias.val() == "") {
                        camposVazios.push('prazo de entrega');
                    }
                    /*if(campoValorMultaAtraso.val() == ""){
                     camposVazios.push('valor da multa');
                     }
                     if(campoDiasContabilizarMulta.val() == ""){
                     camposVazios.push('dias considerados na multa:');
                     }*/
                    if (campoTempoMaximoParaReserva.val() == "") {
                        camposVazios.push('validade da reserva');
                    }

                    if (camposVazios.length > 0) {
                        errors.push("a regra " + (i + 1) + " possui campos vazios. Por favor preencha os campos: <b>" + camposVazios.join(', ') + '</b>.');
                        addErro($item, true);
                    }
                });

                if (Object.keys(errors).length > 0) {
                    if (errors) {
                        addErro($("#modalidadeEmprestimoGrupos"));

                        $.smallBox({
                            title: "Não foi possível salvar a modalidade:",
                            content: "<i>" + errors.join('<br>') + "</i>",
                            color: "#C46A69",
                            icon: "fa fa-times",
                            timeout: 10000
                        });

                        return false;
                    }

                    return false;
                }

                removeErro($("#modalidadeEmprestimoGrupos"));

                form.submit();
            };

            $("#modalidade-form").validate({
                submitHandler: submitHandler,
                rules: {
                    grupoLeitor: 'required',
                    modalidadeEmprestimoDescricao: 'required',
                    modalidadeEmprestimoDiasSuspensao: 'required',
                    modalidadeEmprestimoSituacao: 'required'
                },
                messages: {
                    grupoLeitor: 'Campo obrigatório!',
                    modalidadeEmprestimoDescricao: 'Campo obrigatório!',
                    modalidadeEmprestimoDiasSuspensao: 'Campo obrigatório!',
                    modalidadeEmprestimoSituacao: 'Campo obrigatório!'
                },
                ignore: '.ignore'
            });
        };


        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
            priv.loadSteps();
        };
    };

    $.modalidadeEmprestimoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.modalidade-emprestimo.add");

        if (!obj) {
            obj = new ModalidadeEmprestimoAdd();
            obj.run(params);
            $(window).data('universa.modalidade-emprestimo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);