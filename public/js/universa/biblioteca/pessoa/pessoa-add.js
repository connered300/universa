(function ($, window, document) {
    'use strict';

    var PessoaAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlBuscaCEP: '',
                urlPesquisaPF: '',
                pessoaPesquisa: null
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.notificacaoPessoa = function (erroDescricao) {
            erroDescricao = erroDescricao || 'Erro ao pesquisar pessoa por CPF.';
            $.smallBox({
                title: "Informação:",
                content: "<i>" + erroDescricao + "</i>",
                color: "#336699",
                icon: "fa fa-info"
            });
        };

        priv.setSteps = function () {
            $("#pesCpf").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['999.999.999-99']});
            $("#endCep").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']});
            $('#endNumero').inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
            $("#conContatoTelefone, #conContatoCelular").inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['(99) 9999-9999', '(99) 99999-9999']
            });

            if ($("#pesId").val() != "") {
                $("#buscarCpf").parent().addClass('hide');
            } else {
                $('#pesNome').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: priv.options.urlPesquisaPF,
                            data: {pesNome: request.term},
                            type: 'POST',
                            dataType: "json",
                            success: function (data) {
                                var transformed = $.map(data.pessoaFisica || [], function (el) {
                                    el.label = el.pes_nome;

                                    return el;
                                });

                                response(transformed);
                            }
                        });
                    },
                    minLength: 2,
                    select: function (event, ui) {
                        priv.options.pessoaPesquisa = ui.item;
                        var dataNascimento = new Date(ui.item['pes_data_nascimento']);
                        dataNascimento = dataNascimento.getDate() + '/' + dataNascimento.getMonth() + '/' + dataNascimento.getFullYear();
                        var endereco = ui.item['end_logradouro'] + ', ' + ui.item['end_numero'] + ' ' + ui.item['end_complemento'] + ', ' + ui.item['end_bairro'];
                        endereco += ', ' + ui.item['end_cidade'] + ' - ' + ui.item['end_estado'];
                        endereco = endereco.replace(/(, ){2,}/, ', ');

                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: '' +
                            'Deseja realmente carregar os dados da pessoa abaixo para importação na biblioteca?' +
                            '<br>Nome:<b> ' + ui.item['pes_nome'] + '</b>' +
                            '<br>Sexo:<b> ' + ui.item['pes_sexo'] + '</b>' +
                            '<br>CPF:<b> ' + ui.item['pes_cpf'] + '</b>' +
                            '<br>Data de nascimento:<b> ' + dataNascimento + '</b>' +
                            '<br>Endereço:<b> ' + endereco + '</b>' +
                            '',
                            buttons: "[Não][Sim]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Sim") {
                                priv.pesquisarPesssoaPeloDocumento({pesId: priv.options.pessoaPesquisa['pes_id']});
                            }

                            priv.options.pessoaPesquisa = null;
                        });

                        return false;
                    }
                });
            }

            $("#buscarCpf").click(function () {
                var pesCpf = $("#pesCpf").val();
                var pesCpfNums = pesCpf.replace(/[^0-9]/g, '');

                if (pesCpfNums.length < 11) {
                    priv.notificacaoPessoa('CPF inválido!');

                    return;
                }

                priv.pesquisarPesssoaPeloDocumento({pesCpf: pesCpf});
            });
            $("#pessoaSenha, #pessoaSenhaConfirmacao").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            $("#buscarCep").click(function () {
                var endCep = $("#endCep").val();
                var endCepNums = endCep.replace(/[^0-9]/g, '');

                var erroCEP = function () {
                    $.smallBox({
                        title: "Erro!",
                        content: "<i>CEP inválido ou não encontrado!</i>",
                        color: "#C46A69",
                        icon: "fa fa-times",
                        timeout: 5000
                    });
                };

                if (endCepNums.length < 8) {
                    erroCEP();

                    return;
                }

                $("#buscarCep").prop('disabled', true);

                $.ajax({
                    url: priv.options.urlBuscaCEP,
                    dataType: 'json',
                    type: 'post',
                    data: {cep: endCep},
                    success: function (data) {
                        $("#buscarCep").prop('disabled', false);

                        if (!data.dados) {
                            erroCEP();
                        }

                        $("#endLogradouro").val(((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim());
                        $("#endCidade").val(data.dados.cid_nome || '');
                        $("#endEstado").val(data.dados.est_uf || '').trigger('change');
                        $("#endBairro").val(data.dados.bai_nome || '');
                    },
                    erro: function () {
                        erroCEP();
                        $("#buscarCep").prop('disabled', false);
                    }
                });
            });

            $('#endCidade').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: priv.options.urlBuscaCEP,
                        data: {cidade: request.term},
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            var transformed = $.map(data.dados || [], function (el) {
                                el.label = el.cid_nome;

                                return el;
                            });

                            response(transformed);
                        }
                    });
                },
                minLength: 2,
                focus: function (event, ui) {
                    $("#endCidade").val(ui.item.cid_nome);

                    return false;
                },
                select: function (event, ui) {
                    $("#endCidade").val(ui.item.cid_nome);
                    $("#endEstado").val(ui.item.est_uf).trigger('change');

                    return false;
                }
            });

            //TODO: corrigir
            $('#endEstado').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: priv.options.urlBuscaCEP,
                        data: {estado: request.term},
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            var transformed = $.map(data.dados || [], function (el) {
                                return el.est_uf;
                            });

                            response(transformed);
                        }
                    });
                },
                minLength: 0
            });

            if ($.browser.webkit) {
                $("input[autocomplete=off]").prop("type", "search");
            }
            $("#grupoLeitor").select2({
                language: 'pt-BR',
                allowClear: true
            });
        };

        priv.pesquisarPesssoaPeloDocumento = function (criterioBusca) {
            criterioBusca = criterioBusca || {};

            $("#buscarCpf").prop('disabled', true);
            $("#pesCpf").prop('disabled', true);
            $("#pesNome").prop('disabled', true);

            $.ajax({
                url: priv.options.urlPesquisaPF,
                dataType: 'json',
                type: 'post',
                data: criterioBusca,
                success: function (data) {
                    $("#buscarCpf").prop('disabled', false);
                    $("#pesCpf").prop('disabled', false);
                    $("#pesNome").prop('disabled', false);

                    if (data.erro) {
                        priv.notificacaoPessoa(data.erroDescricao);
                    } else {
                        $('[id^=smallbox]').trigger('click');
                        var mensagem = 'ATENÇÃO: As alterações efetuadas refletirão no cadastro __PESSOA__.';
                        mensagem += '<br>As alterações no <b>nome</b> e <b>CPF</b> podem ser efetuadas __RESPONSAVEL__.';

                        if (data.pessoaFisica['pessoaAluno']) {
                            mensagem = mensagem.replace('__PESSOA__', 'do aluno');
                            mensagem = mensagem.replace('__RESPONSAVEL__', 'pela <b>secretaria</b>.');
                        } else if (!data.pessoaFisica['pessoaAluno'] && data.pessoaFisica['pessoaUsuario']) {
                            mensagem = mensagem.replace('__PESSOA__', 'desta pessoa');
                            mensagem = mensagem.replace('__RESPONSAVEL__', 'pelo <b>administrador do sistema</b>.');
                        }

                        if (data.pessoaFisica['pessoaUsuario']) {
                            $('#pessoaSenha, #pessoaSenhaConfirmacao').attr('disabled', 'disabled');
                        } else {
                            $('#pessoaSenha, #pessoaSenhaConfirmacao').removeAttr('disabled');
                        }


                        priv.notificacaoPessoa(mensagem);

                        if (data.pessoaFisica['pessoaUsuario']) {
                            priv.notificacaoPessoa('ATENÇÃO: A modificação da <b>senha</b> deste usuário <b>não</b> está disponível na <b>biblioteca</b>.');
                        }

                        $("#pessoaImportada").val('1');
                        $('#pessoa-form').deserialize(data.pessoaFisica);
                    }
                },
                erro: function () {
                    priv.notificacaoPessoa('Erro desconhecido!');
                    $("#buscarCpf").prop('disabled', false);
                    $("#pesCpf").prop('disabled', false);
                    $("#pesNome").prop('disabled', false);
                }
            });
        };

        priv.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            var $form = $("#pessoa-form");

            $form.validate({
                submitHandler: submitHandler,
                rules: {
                    pesCpf: {required: false, cpf: true},
                    pesNome: {required: false, maxlength: 255},
                    endCep: {required: false},
                    endLogradouro: {required: false},
                    endNumero: {
                        required: function () {
                            return $form.find('[name=endComplemento]').val() == '';
                        }, number: true
                    },
                    endComplemento: {
                        required: function () {
                            return $form.find('[name=endNumero]').val() == '';
                        }
                    },
                    endBairro: {required: false},
                    endCidade: {required: true},
                    endEstado: {required: true},
                    conContatoTelefone: {telefoneValido:true, required: false},
                    conContatoEmail: {required: false, email: true},
                    pessoaSenha: {
                        required: function () {
                            return $('#pesId').val() == "";
                        }
                    },
                    pessoaSenhaConfirmacao: {
                        equalTo: "#pessoaSenha"
                    },
                    grupoLeitor: {required: false},
                    pessoaSituacao: {required: false},
                    pessoaObservacao: {required: false}
                },
                messages: {
                    pesCpf: {
                        required: 'CPF obrigatório!',
                        cpf: 'CPF inválido!'
                    },
                    pesNome: {required: 'Nome obrigatório!', maxlength: 'Nome: máximo 255 caracteres!'},
                    endCep: {required: 'CEP obrigatório!'},
                    endLogradouro: {required: 'Logradouro obrigatório!'},
                    endNumero: {required: 'Número obrigatório!'},
                    endComplemento: {required: 'Complemento obrigatório!'},
                    endBairro: {required: 'Bairro obrigatório!'},
                    endCidade: {required: 'Cidade obrigatória!'},
                    endEstado: {required: 'Estado obrigatório!'},
                    conContatoTelefone: {required: 'Telefone obrigatório!'},
                    conContatoEmail: {required: 'Email obrigatório!', email: 'Email inválido!'},
                    pessoaSenha: {required: 'Senha obrigatória!'},
                    pessoaSenhaConfirmacao: {equalTo: 'Os valores digitados são diferentes!'},
                    grupoLeitor: {required: 'Grupo de leitor obrigatório!'},
                    pessoaSituacao: {required: 'Situação obrigatória!'},
                    pessoaObservacao: {required: 'Observações obrigatórias!'}
                },
                ignore: '.ignore'
            });
        };


        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.pessoaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.pessoa.add");

        if (!obj) {
            obj = new PessoaAdd();
            obj.run(params);
            $(window).data('universa.pessoa.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);