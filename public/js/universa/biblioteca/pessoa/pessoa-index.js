(function ($, window, document) {
    'use strict';

    var PessoaIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.setSteps = function () {
            priv.dataTablePessoa = $('#dataTablePessoa').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        return json.data;
                    }
                },
                columnDefs: [
                    {name: "alunocurso_id", targets: 0, data: "alunocurso_id"},
                    {name: "pes_nome", targets: 1, data: "pes_nome"},
                    {name: "pes_cpf", targets: 2, data: "pes_cpf"},
                    {name: "grupo_leitor_nome", targets: 3, data: "grupo_leitor_nome"},
                    {name: "con_contato_telefone", targets: 4, data: "con_contato_telefone"},
                    {name: "end_cidade", targets: 5, data: "end_cidade"},
                    {name: "end_estado", targets: 6, data: "end_estado"},
                    {name: "con_contato_email", targets: 7, data: "con_contato_email"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": [],
                    "sSwfPath": "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[1]]
            });

            $(document).on("click", "#dataTablePessoa tbody tr", function (event) {
                var position = priv.dataTablePessoa.fnGetPosition(this);
                var data = priv.dataTablePessoa.fnGetData(position);

                location.href = priv.options.urlEdit + '/' + data['pes_id'];
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.pessoaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.pessoa.index");

        if (!obj) {
            obj = new PessoaIndex();
            obj.run(params);
            $(window).data('universa.pessoa.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);