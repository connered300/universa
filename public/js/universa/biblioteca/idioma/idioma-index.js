(function ($, window, document) {
    'use strict';

    var IdiomaIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#idioma-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['idioma_id']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $(".has-error .help-block").hide();
            $(".has-error").removeClass("has-error");

            $('#novo-idioma .modal-title').html(actionTitle + ' idioma');
            $('#idiomaDescricao').val(data['idioma_descricao'] || '');
            $('#idiomaId').val(data['idioma_id'] || '');
            $('#idiomaTitulos').val(data['idioma_titulos'] || '');
            $('#novo-idioma').modal();
        };

        priv.setSteps = function () {
            priv.dataTableIdioma = $('#dataTableIdioma').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        return json.data;
                    }
                },
                columnDefs: [
                    {name: "idioma_descricao", targets: 0, data: "idioma_descricao"},
                    {name: "idioma_titulos", targets: 1, data: "idioma_titulos"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0]]
            });

            $(document).on("click", "#dataTableIdioma tbody tr", function (event) {
                var position = priv.dataTableIdioma.fnGetPosition(this);
                var data = priv.dataTableIdioma.fnGetData(position);
                priv.showModal(data)

            });

            $('#btn-novo-idioma').click(function () {
                priv.showModal()
            });
        };

        pub.getDataTableIdioma = function () {
            return priv.dataTableIdioma;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.idiomaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.idioma.index");

        if (!obj) {
            obj = new IdiomaIndex();
            obj.run(params);
            $(window).data('universa.idioma.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);