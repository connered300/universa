(function ($, window, document) {
    'use strict';

    var TituloAdd = function () {
        VersaShared.call(this);
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                url: {
                    assunto: '',
                    area: '',
                    areaCnpq: '',
                    colecao: '',
                    idioma: '',
                    empresa: '',
                    cidade: '',
                    autor: '',
                    pessoa: '',
                    doador: '',
                    exemplar: '',
                    thumbBaseUrl: '',
                    downloadBaseUrl: ''
                },
                data: {
                    grupoBibliografico: [],
                    idioma: [],
                    setor: [],
                    exemplaresCodigo: [],
                    arrDados: []
                },
                value: {
                    grupoBibliografico: [],
                    area: null,
                    areaCnpq: null,
                    colecao: null,
                    editora: null,
                    assuntosRelacionados: null,
                    idiomasRelacionados: null,
                    autor: null,
                    exemplar: null,
                    exemplares: null,
                    aquisicao: null
                },
                datatables: {
                    exemplares: null
                },
                urlBuscaCEP: '',
                wizardElement: '#titulo-wizard',
                formElement: '#titulo-form'
            }
        };
        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.makeSelect2Field = function (settings) {
            settings = settings || [];
            settings = $.extend(true, {
                fieldId: '',
                optionsAttr: '',
                dataAttr: '',
                ajax: false,
                data: null,
                select2Extra: {},
                changeCallback: null
            }, settings);

            if (!settings.dataAttr) {
                settings.dataAttr = settings.optionsAttr;
            }

            return {
                settings: settings,
                setValue: function (option) {
                    option = option || null;
                    priv.options.value[this.settings.optionsAttr] = option;
                    $(this.settings.fieldId).select2("val", option);
                },
                setValueBySearch: function (search) {
                    var data = priv.options.data[this.settings.dataAttr] || null;

                    if (!data) {
                        return;
                    }

                    var selection = $.grep(data, function (el) {
                        if (el.id == search || el.text.toLowerCase() == search.toLowerCase()) {
                            return el;
                        }
                    });

                    if (!this.settings.select2Extra.multiple && !this.settings.select2Extra.tags) {
                        selection = selection[0] || null;
                    }

                    this.setValue(selection || null);
                },
                getValue: function () {
                    return priv.options.value[this.settings.optionsAttr];
                },
                init: function () {
                    var select2Helper = this;
                    var select2Options = {
                        language: 'pt-BR',
                        allowClear: true,
                        initSelection: function (element, callback) {
                            callback(select2Helper.getValue());
                        }
                    };

                    if (this.settings.ajax) {
                        select2Options['ajax'] = {url: '', dataType: 'json', delay: 250, data: null, results: ''};
                        select2Options['ajax'] = $.extend(true, select2Options['ajax'], this.settings.ajax);
                        delete select2Options['data'];
                    } else if (this.settings.data) {
                        select2Options['data'] = this.settings.data;
                        delete select2Options['ajax'];
                    } else {
                        delete select2Options['ajax'];
                        delete select2Options['data'];
                    }

                    if (this.settings.select2Extra) {
                        select2Options = $.extend(true, select2Options, this.settings.select2Extra);
                    }

                    $(this.settings.fieldId).select2(select2Options);

                    if (this.settings.select2Extra.allowClear) {
                        $(this.settings.fieldId).on('change select2-selected select2-clearing select2-removed', function () {
                            var $item = $(this);

                            if ($item.val() != '') {
                                $item.parent().find('.select2-container').addClass('select2-allowclear');
                            } else {
                                $item.parent().find('.select2-container').removeClass('select2-allowclear');
                            }
                        });
                    }

                    if (this.settings.changeCallback) {
                        $(this.settings.fieldId).on('change', this.settings.changeCallback);
                    }
                    $(this.settings.fieldId).select2("val", select2Helper.getValue());
                    $(this.settings.fieldId).trigger('change');
                }
            };
        };

        priv.showNotificacao = function (param) {
            var options = $.extend(true, {
                content: '',
                title: '',
                type: 'info',
                icon: '',
                color: '',
                timeout: false
            }, param);

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: "fa " + options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            $.smallBox(optionsNotification);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="emprestimo-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('.emprestimo-overlay').remove();
        };

        priv.removeErroComponente = function (elemento) {
            if (elemento.find('>.component-item').length > 0) {
                elemento.find('>.component-item').removeClass('bg-danger');
                elemento.find('>.component-item .form-group .help-block').remove();
            } else {
                elemento.removeClass('bg-danger');
            }
        };

        priv.adicionaErroComponente = function (elemento) {
            var $card = null;

            if (elemento.find('>.component-item').length > 0) {
                $card = elemento.find('>.component-item');
                $card.addClass('bg-danger');
            } else {
                $card = elemento;
                $card.addClass('bg-danger');
            }
        };

        priv.getAutorCutter = function () {
            var componente = $("#autores").componente();
            var autores = componente.items();
            var data = [];
            var titulo = $("#tituloTitulo").val();

            var ignorar='(a|da|das|dal|dalla|das|des|del|della|dello|di|do|dos|du|dus|e|em|i|la|las|le|les|lo|los|na|nas|no|nos|o|u|van|von|y|um|uma)'
            titulo = titulo.toLowerCase();

            //remove todos os caracteres especiais e numeros
            var r=new RegExp('([^a-z ]+)','gm');
            titulo = titulo.replace(r, '');
            titulo = titulo.trim();

            //remove os itens a ignorar do meio da string
            r=new RegExp('(\\s+'+ignorar+'\\s+)','gm');
            titulo = titulo.replace(r, ' ');

            //Remove os itens do inicio da string
            r=new RegExp('^'+ignorar+'\\s+','gm');
            titulo = titulo.replace(r, '');

            titulo = titulo.trim();

            $.each(autores, function (i, item) {
                var $item = $(item);
                var id = $(item).data('componente.id');
                var campoAutor = $item.find('[name="' + componente.makeFieldName({id: id, name: 'autor'}) + '"]');

                if (campoAutor.val() != "") {
                    var campoAutorData = campoAutor.select2('data').autor_cutter || '';

                    if (campoAutorData) {
                        data.push(campoAutorData + titulo[0]);
                    }
                }
            });

            return $.unique(data);
        };

        priv.getExemplaresTags = function () {
            var aquisicoes = $('#aquisicoes').componente('exportJSON');
            var arrExemplaresSelecionados = [];

            $.each(aquisicoes, function (index, item) {
                if (item['exemplares']) {
                    var expode = item['exemplares'].split(",");
                    arrExemplaresSelecionados = $.merge(arrExemplaresSelecionados, expode);
                }
            });

            var data = [];

            for (var i in priv.options.value.exemplares || []) {
                var item = priv.options.value.exemplares[i];
                var codigo = item['exemplar_codigo'] + '';
                var adiciona = true;

                $.each(arrExemplaresSelecionados, function (index, exemplarSelecionado) {
                    if (exemplarSelecionado == codigo) {
                        adiciona = false;
                        return true;
                    }
                });

                if (adiciona) {
                    data.push({id: codigo, text: codigo});
                }
            }

            return {results: data};
        };

        priv.step1 = function () {

            var tituloAcessoPortal = $("#tituloAcessoPortal");
            var grupoBibliografico = priv.makeSelect2Field({
                fieldId: '#grupoBibliografico',
                optionsAttr: 'grupoBibliografico',
                data: priv.options.data.grupoBibliografico,
                select2Extra: {allowClear: true}
            });

            $('#grupoBibliografico').on("change", function () {
                if ($('#grupoBibliografico').val()) {
                    var arrGrupoBibliografico = $('#grupoBibliografico').select2("data");
                    $("#tituloAcessoPortal").select2('val',
                        priv.options.data.arrDados['tituloAcessoPortal'] || arrGrupoBibliografico['grupoBibliograficoAcessoPortal']
                    ).trigger("change");
                }
            });

            tituloAcessoPortal.select2(
                {data: [{'id': 'Sim', 'text': 'Sim'}, {'id': 'Não', 'text': 'Não'}]});

            var area = priv.makeSelect2Field({
                fieldId: '#area', optionsAttr: 'area', ajax: {
                    url: priv.options.url.area,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_descricao, id: el.area_id};
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true}
            });
            var areaCnpq = priv.makeSelect2Field({
                fieldId: '#areaCnpq', optionsAttr: 'areaCnpq', ajax: {
                    url: priv.options.url.areaCnpq,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_cnpq_codigo + ': ' + el.area_cnpq_descricao, id: el.area_cnpq_id};
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true}
            });
            var colecao = priv.makeSelect2Field({
                fieldId: '#colecao', optionsAttr: 'colecao', ajax: {
                    url: priv.options.url.colecao,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.colecao_descricao, id: el.colecao_id};
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true}
            });
            var assuntosRelacionados = priv.makeSelect2Field({
                fieldId: '#assuntosRelacionados', optionsAttr: 'assuntosRelacionados', ajax: {
                    url: priv.options.url.assunto,
                    data: function (query) {
                        var data = {query: query};
                        var assuntoId = $('#assuntosRelacionados').val();

                        if (assuntoId != "") {
                            data['assuntoId'] = assuntoId;
                        }

                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.assunto_descricao, id: el.assunto_id};
                        });

                        return {results: transformed};
                    }
                }, select2Extra: {
                    allowClear: true,
                    tags: true
                }
            });
            var idiomasRelacionados = priv.makeSelect2Field({
                fieldId: '#idiomasRelacionados',
                optionsAttr: 'idiomasRelacionados',
                dataAttr: 'idioma',
                data: priv.options.data.idioma,
                select2Extra: {tags: true, allowClear: true}
            });

            grupoBibliografico.init();
            area.init();
            areaCnpq.init();
            colecao.init();
            assuntosRelacionados.init();
            idiomasRelacionados.init();

            tituloAcessoPortal.val(priv.options.data.arrDados['tituloAcessoPortal'] || '').trigger("change");

            var arrGrupoBibliografico = priv.options.data.grupoBibliografico;
            var grupoBibliograficoId = priv.options.value.grupoBibliografico;
            var tamanho = arrGrupoBibliografico.length, x = 0;

            if (grupoBibliograficoId) {
                for (x = 0; x < tamanho; x++) {
                    if (arrGrupoBibliografico[x]['grupoBibliograficoId'] == grupoBibliograficoId['id']) {
                        $("#tituloAcessoPortal").val(
                            priv.options.data.arrDados['tituloAcessoPortal'] || arrGrupoBibliografico[x]['grupoBibliograficoAcessoPortal'] || ''
                        ).trigger("change");
                    }
                }
            }

            var arquivoImagemTitulo = $('#arquivoImagem').vfile({
                thumbBaseUrl: priv.options.url.thumbBaseUrl,
                downloadBaseUrl: priv.options.url.downloadBaseUrl
            });

            if (priv.options.data.arrDados['arquivoImagem']) {
                var arrImagem = priv.options.data.arrDados['arquivoImagem'];


                arrImagem['tipo'] = arrImagem['arqTipo'];
                arrImagem['chave'] = arrImagem['arqChave'];
                arrImagem['tamanho'] = arrImagem['arqTamanho'];
                arrImagem['nome'] = arrImagem['arqNome'];
                arquivoImagemTitulo.setSelection(arrImagem);
            }

            var arquivoDigital = $('#arquivoDigital').vfile({
                thumbBaseUrl: priv.options.url.thumbBaseUrl,
                downloadBaseUrl: priv.options.url.downloadBaseUrl,
                defaultMessage: 'Sem arquivo selecionado',
                invalidMessage: 'Arquivo inválido!',
                allowedTypes: [
                    'application/pdf'
                ]
            });

            var arrAquivo = [];

            if (arrAquivo = priv.options.data.arrDados['arquivoDigital']) {

                arrAquivo['tipo'] = arrAquivo['arqTipo'];
                arrAquivo['chave'] = arrAquivo['arqChave'];
                arrAquivo['tamanho'] = arrAquivo['arqTamanho'];
                arrAquivo['nome'] = arrAquivo['arqNome'];
                arquivoDigital.setSelection(arrAquivo);
            }


            $(".btn-download-arquivo").on("click", function () {
                var arquivoDigital = priv.options.data.arrDados['arquivoDigital'];
                if (arquivoDigital['arqChave']) {
                    location.href = (
                        priv.options.url.downloadBaseUrl +
                        '/gerenciador-arquivos/arquivo/download/' +
                        arquivoDigital['arqChave']
                    );
                } else {
                    pub.showNotificacao({
                        content: 'Não foi possível localizar este arquivo!',
                        type: 'warning',
                        timeout: 5000
                    });
                }
            });

            $(".file-label").on("click", function () {
                $("#alteracaoImagem").val(true);
            });

            $(document).on('click', '.btn-excluir-arquivo', function () {
                $("#alteracaoArquivo").val(true);
                $('#' + $(this).attr('id')).remove();
                $('#tabela-anexos').addClass('hidden');
            });

            function validaArquivo(arquivoNome, arquivoTipo) {

                var retorno = true, extensao = '';

                if (arquivoTipo != 'application/pdf') {
                    return false;
                }
                var ext = arquivoNome.replace(/(^[^.*\.]*)\./, '');

                /*Depois do nome o que tiver qualquer extensão não será aceito*/

                if (extensao && value != "pdf") {
                    retorno = false;
                }

                return retorno;
            };

            $('#btn-anexar-add').on('click', function (e) {
                if ($("#tabela-anexos").is(":visible")) {
                    pub.showNotificacao({
                        content: 'É permitido somente um arquivo!',
                        type: 'info',
                        timeout: 5000
                    });

                    return false;
                }

                var $element = $('.divAnexoInput').clone();
                var $input = $element.find('input');
                var $btn = $element.find('button');
                var $label = $element.find('.nomeArquivo');
                var id = pub.createGUID();

                $element.removeClass('divAnexoInput');
                $element.addClass(id);

                $label.attr('id', id);
                $input.attr('id', id);
                $btn.attr('id', id);

                $input.trigger('click');

                $input.change(function () {
                    $label.text(
                        this.files[0].name
                    );

                    var ok = validaArquivo(this.files[0].name, this.files[0].type);

                    ok = true;

                    if (!ok) {
                        priv.showNotificacao({
                            content: 'Arquivo inválido!',
                            type: 'warning',
                            timeout: 5000
                        });
                        return false;
                    }

                    $element.appendTo('#arrMensagemAnexos-add');

                    var row = "<tr id=" + id + ">";
                    row += "<th scope='row'>" + this.files[0].name + "</th>";
                    row += "<th scope='row'>" + $btn[0].outerHTML + "</th>";
                    row += "</tr>";

                    $('#tabela-anexos-corpo').append(row);
                    $('#tabela-anexos').removeClass('hidden');
                });
            });

            if (!grupoBibliografico.getValue()) {
                grupoBibliografico.setValueBySearch('Livro');
            }
            if (!idiomasRelacionados.getValue()) {
                idiomasRelacionados.setValueBySearch('Português');
            }
            $('#tituloPaginas').inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});
        };

        priv.step2 = function () {
            var editora = priv.makeSelect2Field({
                fieldId: '#editora', optionsAttr: 'editora', ajax: {
                    url: priv.options.url.empresa,
                    dataType: 'json', delay: 250,
                    data: function (query) {
                        return {query: query, empresaTipo: 'Editora'};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome_fantasia;
                            el.id = el.pes_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                changeCallback: function () {
                    var data = $(this).select2('data') || [];

                    if (Object.keys(data).length > 0) {
                        if (data['end_cidade']) {
                            $('#tituloEdicaoLocal').val(data.end_cidade + ', ' + data.end_estado.replace(/[^A-Z]/g, ''));
                        }
                    }
                },
                select2Extra: {allowClear: true}
            });

            editora.init();

            var arrDados = priv.options.data.arrDados;

            if (arrDados['pesEditora']) {
                var arrAux = {
                    'id': arrDados['pesEditora']['pes']['pesId'],
                    'text': arrDados['pesEditora']['pes']['pesNome']
                };

                $("#editora").select2("data", arrAux);
            }

            $("#tituloEdicaoAno").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['9999']});
            $("#tituloEdicaoAno").val($("#tituloEdicaoAno").val() || (new Date()).getFullYear());
            $("#tituloEdicaoIsbn").inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['9{1,5}-9{1,7}-9{1,5}-9|X', '9{3}-9{1,5}-9{1,7}-9{1,5}-9|X']
            });
            $("#tituloEdicaoCutter").inputmask({
                showMaskOnHover: false,
                clearIncomplete: true,
                mask: ['A9{1,3}[a{0,}]']
            });
            $('#tituloEdicaoCutter').autocomplete({
                source: function (request, response) {
                    var data = priv.getAutorCutter();
                    var term = request.term.replace(/[^a-z0-9]/gi, '');
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");

                    response($.grep(data, function (value) {
                        value = value.label || value.value || value;
                        return matcher.test(value) || matcher.test(value);
                    }));
                }
            });
            $("#autores").componente({
                inputName: 'autor',
                classCardExtra: 'col-sm-12',
                labelContainer: '<div class="form-group"/>',
                tplItem: '\
                        <div class="col-sm-12">\
                            <div class="componente-content">\
                            </div >\
                            <div class="remover-autor">\
                                <a href="#" class="btn btn-danger btn-xs item-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </a>\
                             </div>\
                        </div>',
                fieldClass: 'form-control',
                fields: [
                    {name: 'tituloAutorId', label: 'Código', type: 'hidden'},
                    {
                        name: 'tipo', label: 'Tipo', type: 'select', options: [
                        {value: 'Autor principal', text: 'Autor principal'},
                        {value: 'Colaborador', text: 'Colaborador'},
                        {value: 'Coordenador', text: 'Coordenador'},
                        {value: 'Organizador', text: 'Organizador'},
                        {value: 'Orientador', text: 'Orientador'},
                        {value: 'Tradutor', text: 'Tradutor'}
                    ],
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-4');
                            $parent.find('label').remove();

                            field.select2({
                                language: 'pt-BR'
                            });

                            var tipo = itemConfig['tipo'] || 'Autor principal';
                            field.select2("val", tipo);

                            field.trigger('change');
                        }
                    },
                    {
                        name: 'autor', label: 'Autor',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-8');
                            $parent.find('label').remove();

                            field.select2({
                                language: 'pt-BR',
                                ajax: {
                                    url: priv.options.url.autor,
                                    dataType: 'json', delay: 250,
                                    data: function (query) {
                                        return {query: query};
                                    },
                                    results: function (data) {
                                        var transformed = $.map(data, function (el) {
                                            el.text = el.autor_referencia;
                                            el.id = el.autor_id;

                                            return el;
                                        });

                                        return {results: transformed};
                                    }
                                },
                                initSelection: function (element, callback) {
                                    callback(itemConfig['autor'] || null);
                                }
                            });

                            field.select2("val", itemConfig['autor'] || null).trigger('change');
                        }
                    }
                ],
                removeCallback: function (parametersCallback) {
                    var item = parametersCallback.item,
                        event = parametersCallback.event,
                        settings = parametersCallback.settings,
                        data = parametersCallback.data;

                    var confirmacao = item.data('confirmacaoRemocao') || false;

                    if (!confirmacao) {
                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Deseja realmente remover este autor?',
                            buttons: "[Cancelar][Ok]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Ok") {
                                $(item).data('confirmacaoRemocao', true);
                                $(event.currentTarget).click();
                            }
                        });

                        return true;
                    }

                    return false;
                }
            });
            $('#tituloEdicaoVolume').inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});
            $('#tituloEdicaoTomo').inputmask({showMaskOnHover: false, clearIncomplete: true, mask: '*{0,10}'});
            $("#tituloEdicaoVolume").val($("#tituloEdicaoVolume").val() || "");
            $("#tituloEdicaoTomo").val($("#tituloEdicaoTomo").val() || "");
            $("#tituloEdicaoNumero").val($("#tituloEdicaoNumero").val() || "");
            $('#tituloEdicaoLocal').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: priv.options.url.cidade,
                        data: {cidade: request.term},
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            var transformed = $.map(data.dados || [], function (el) {
                                el.label = el.cid_nome + ', ' + el.est_uf;

                                return el;
                            });

                            response(transformed);
                        }
                    });
                },
                minLength: 2
            });

            if (priv.options.value.autor) {
                $.each(priv.options.value.autor, function (i, item) {
                    $('#autores').componente(['add', item]);
                });
            } else {
                $('#autores').componente(['add', {}]);
            }
        };

        priv.showModalExemplar = function (data) {
            data = data || {};
            var setor = null;
            var actionTitle = 'Cadastrar';
            var $form = $('#exemplar-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['exemplar_id']);
                setor = {id: data['setor_id'], text: data['setor_descricao'] + '(' + data['setor_sigla'] + ')'};
                $('#numeroDeExemplares').val('').parent().hide();
            } else {
                actionForm[actionForm.length - 1] = 'add';
                $('#numeroDeExemplares').val('1').parent().show();
            }

            $('#gerar-novo-codigo')[actionTitle == 'Editar' ? 'show' : 'hide']();
            $('#gerar-novo-codigo .btn').unbind('click').bind('click', function (e) {
                $('#exemplarCodigoNovo').val(priv.getProximoExemplar());
                e.preventDefault();
            });

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $('#novo-exemplar .modal-title').html(actionTitle + ' exemplar');
            $('#exemplarId').val(data['exemplar_id'] || '').trigger('change');
            $('#exemplarCodigoNovo').val('');
            $('#exemplarCodigo').val(
                data['exemplar_codigo'] || priv.getProximoExemplar()
            ).trigger('change');
            $('#exemplarEmprestado').val(data['exemplar_emprestado'] || 'N').trigger('change');
            $('#exemplarReservado').val(data['exemplar_reservado'] || 'N').trigger('change');
            $('#exemplarLocalizacao').val(data['exemplar_localizacao'] || '');
            $('#exemplarAcesso').select2('val', data['exemplar_acesso'] || 'Livre');
            $('#setor').select2('data', setor);
            $('#exemplarBaixaData').val(data['exemplar_baixa_data'] || '').trigger('change');
            $('#exemplarBaixaMotivo').val(data['exemplar_baixa_motivo'] || '');
            $('#novo-exemplar').modal();
        };

        priv.step3 = function () {
            var colNum = 0;
            priv.options.datatables.exemplares = $('#dataTableExemplares').dataTable({
                processing: true,
                columnDefs: [
                    {name: "exemplar_id", targets: colNum++, data: "exemplar_id_r"},
                    {name: "exemplar_codigo", targets: colNum++, data: "exemplar_codigo"},
                    {name: "exemplar_emprestado", targets: colNum++, data: "exemplar_emprestado_r"},
                    {name: "exemplar_reservado", targets: colNum++, data: "exemplar_reservado_r"},
                    {name: "exemplar_acesso", targets: colNum++, data: "exemplar_acesso"},
                    {name: "setor_descricao", targets: colNum++, data: "setor_descricao"},
                    {name: "exemplar_localizacao", targets: colNum++, data: "exemplar_localizacao"},
                    {name: "exemplar_baixa_data", targets: colNum++, data: "exemplar_baixa_data_r"},
                    {name: "exemplar_acoes", targets: colNum++, data: 'exemplar_acoes'}
                ],
                "dom": "<'dt-toolbar'<'col-xs-3 dt-toolbar-exemplar no-padding'B><'col-xs-6 exemplar-aviso'><'col-xs-3 no-padding'f>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[1, 'desc']]
            });

            $('.exemplar-aviso').append(
                '<div class="text text-info">' +
                '<b>Atenção:</b> ' +
                'Os exemplares só são salvos no sistema junto com o títulos.' +
                '</div>'
            );

            $('.dt-toolbar-exemplar').append(
                '<div class="btn-group">' +
                '<button type="button" class="btn btn-primary" id="btn-add-exemplar">' +
                'Adicionar</button>' +
                '<button type="button" class="btn btn-primary" id="btn-importar-exemplar">' +
                'Importar</button>' +
                '</div>'
            );

            $('#btn-add-exemplar').click(function () {
                priv.showModalExemplar()
            });

            $('#btn-importar-exemplar').click(function (e) {
                $.SmartMessageBox({
                    title: "Digite o código do exemplar/Tombo para importar:",
                    content: 'Ao importar um exemplar ele deixará de ser listado no título anterior.',
                    buttons: "[Cancelar][Importar]",
                    input: "text",
                    placeholder: "Código do exemplar/Tombo",
                    inputValue: ""
                }, function (ButtonPress, codigoExemplar) {
                    if (!codigoExemplar) {
                        return false;
                    }

                    if (ButtonPress == "Importar") {
                        if (codigoExemplar == "") {
                            priv.showNotificacao({
                                content: 'Digite um código de exemplar/Tombo para efetuar a importação!',
                                type: 'danger',
                                timeout: 5000
                            });
                            return;
                        }

                        priv.options.value.exemplares = priv.options.value.exemplares || [];

                        for (var i in priv.options.value.exemplares) {
                            if (parseInt(codigoExemplar) == parseInt(priv.options.value.exemplares[i]['exemplar_id'])) {
                                priv.showNotificacao({
                                    content: 'Exemplar já se encontra na listagem de exemplares!',
                                    type: 'danger',
                                    timeout: 5000
                                });
                                return;
                            }
                        }

                        priv.addOverlay($('#widget-grid'), 'Aguarde, buscando informações sobre o exemplar.');

                        $.ajax({
                            url: priv.options.url.exemplar,
                            data: {query: codigoExemplar, exemplar: true},
                            type: 'POST',
                            dataType: "json",
                            success: function (data) {
                                priv.removeOverlay($('#widget-grid'));

                                // Heuristica aplicada para suprir deficiência do plugin SmartMessageBox quando
                                // várias mensagens são exibidas em sequência
                                setTimeout(function () {
                                    if (Object.keys(data).length > 0) {
                                        $.SmartMessageBox({
                                            title: "Confirme a operação:",
                                            content: 'Deseja mover o exemplar ' + parseInt(data['exemplar_id']) + ' do título "' + data['titulo_titulo'] + '" (' + parseInt(data['titulo_id']) + ') para o título atual?',
                                            buttons: "[Não][Sim]"
                                        }, function (ButtonPress1) {
                                            if (ButtonPress1 == "Sim") {
                                                pub.editarExemplarItem(data, true);
                                            }
                                        });
                                    } else {
                                        priv.showNotificacao({
                                            content: 'Exemplar não existe!',
                                            type: 'danger',
                                            timeout: 5000
                                        });
                                    }
                                }, 600);
                            }
                        });
                    }
                });

                e.preventDefault();
            });

            $('#dataTableExemplares').on('click', '.btn-item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = priv.options.datatables.exemplares.fnGetData($pai);
                priv.showModalExemplar(data);
            });

            $('#dataTableExemplares').on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var exemplarData = priv.options.datatables.exemplares.fnGetData($pai);
                var idNovo = exemplarData['exemplar_id'] + '-' + exemplarData['exemplar_codigo'];

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover este exemplar?',
                    buttons: "[Cancelar][Ok]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Ok") {
                        var arrExemplarManter = $.grep(priv.options.value.exemplares || [], function (item) {
                            var idItem = item['exemplar_id'] + '-' + item['exemplar_codigo'];
                            return (idItem != idNovo);
                        });

                        priv.options.value.exemplares = arrExemplarManter;
                        pub.redrawDatatableExemplares();
                    }
                });

                e.preventDefault();
            });
            $('#dataTableExemplares').on('click', '.btn-baixa', function (e) {
                var $pai = $(this).closest('tr');
                var exemplarData = priv.options.datatables.exemplares.fnGetData($pai);
                var idNovo = exemplarData['exemplar_id'] + '-' + exemplarData['exemplar_codigo'];

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente dar baixa neste exemplar?<br>Digite o motivo abaixo:',
                    buttons: "[Cancelar][Ok]",
                    input: "text",
                    placeholder: "Motivo da baixa.",
                    inputValue: ""
                }, function (ButtonPress, motivo) {
                    if (!motivo) {
                        return false;
                    }

                    if (ButtonPress == "Ok") {
                        var arrExemplarManter = [];
                        $.each(priv.options.value.exemplares || [], function (i, item) {
                            var idItem = item['exemplar_id'] + '-' + item['exemplar_codigo'];

                            if (idItem == idNovo) {
                                item['exemplar_baixa_data'] = $.formatDateTime('yy-mm-dd hh:ii', new Date());
                                item['exemplar_baixa_motivo'] = motivo;
                            }

                            arrExemplarManter.push(item);
                        });

                        priv.options.value.exemplares = arrExemplarManter;
                        pub.redrawDatatableExemplares();
                    }
                });

                e.preventDefault();
            });

            $('#dataTableExemplares').on('click', '.btn-baixa-cancelar', function (e) {
                var $pai = $(this).closest('tr');
                var exemplarData = priv.options.datatables.exemplares.fnGetData($pai);
                var idNovo = exemplarData['exemplar_id'] + '-' + exemplarData['exemplar_codigo'];

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente re-ativar este exemplar?',
                    buttons: "[Cancelar][Ok]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Ok") {
                        var arrExemplarManter = [];
                        $.each(priv.options.value.exemplares || [], function (i, item) {
                            var idItem = item['exemplar_id'] + '-' + item['exemplar_codigo'];

                            if (idItem == idNovo) {
                                item['exemplar_baixa_data'] = '';
                                item['exemplar_baixa_motivo'] = '';
                            }

                            arrExemplarManter.push(item);
                        });

                        priv.options.value.exemplares = arrExemplarManter;
                        pub.redrawDatatableExemplares();
                    }
                });

                e.preventDefault();
            });

            pub.redrawDatatableExemplares();
        };

        pub.retornaObrigatoriedadeCampo = function (campo) {
            var retorno = true,
                arrDados = [],
                grupoBibliografico = $("#grupoBibliografico").val();

            if (!grupoBibliografico) {
                priv.showNotificacao({content: "Selecione um grupo bibliográfico!", type: 'warning'});
                $(".previous").click();
            }

            arrDados = $("#grupoBibliografico").select2("data");

            switch (campo) {
                case 'tituloEdicaoLocal':

                    if (arrDados['grupoBibliograficoLocalEdicao'] == 'Não') {
                        retorno = false;
                    }

                    break;
                case'autor' :

                    if (arrDados['grupoBibliograficoAutor'] == 'Não') {
                        retorno = false;
                    }

                    break;
                case'exemplar':

                    if (arrDados['grupoBibliograficoExemplares'] == 'Não') {
                        retorno = false;
                    }

                    break;
            }
            return retorno;
        }

        pub.editarExemplarItem = function (dados, importacao) {
            importacao = importacao || false;
            var idNovo = dados['exemplar_id'] + '-' + dados['exemplar_codigo'];
            var novo = true;
            priv.options.value.exemplares = priv.options.value.exemplares || [];

            for (var i in priv.options.value.exemplares) {
                var item = priv.options.value.exemplares[i];
                var idItem = item['exemplar_id'] + '-' + item['exemplar_codigo'];

                if (idNovo == idItem) {
                    if (dados['exemplar_codigo_novo']) {
                        dados['exemplar_codigo'] = dados['exemplar_codigo_novo'];
                    }

                    priv.options.value.exemplares[i] = dados;
                    novo = false;
                    break;
                }
            }

            if (novo && !importacao) {
                for (var i = 0; i < dados['numero_de_exemplares'] || 0; i++) {
                    var item = $.extend(true, {}, dados);
                    item['exemplar_codigo'] = priv.getProximoExemplar();
                    priv.options.value.exemplares.push(item);
                }
            } else if (importacao) {
                priv.options.value.exemplares.push(dados);
            }
            pub.redrawDatatableExemplares();
        };

        pub.redrawDatatableExemplares = function () {
            priv.options.datatables.exemplares.fnClearTable();
            var data = [];

            $.each(priv.options.value.exemplares || [], function (i, item) {
                item['exemplar_id_r'] = item['exemplar_id'] || '-';
                item['exemplar_emprestado_r'] = item['exemplar_emprestado'] == 'N' ? 'Não' : 'Sim';
                item['exemplar_reservado_r'] = item['exemplar_reservado'] == 'N' ? 'Não' : 'Sim';
                item['exemplar_baixa_data_r'] = (
                    item['exemplar_baixa_data'] ?
                        $.formatDateTime('dd/mm/yy hh:ii', new Date(item['exemplar_baixa_data'])) :
                        ''
                );
                item['exemplar_acoes'] = '';
                item['exemplar_acoes'] = '\
                <div class="btn-group btn-group-xs text-center">\
                    <a href="#" class="btn btn-primary btn-xs btn-item-edit" title="Editar">\
                        <i class="fa fa-pencil fa-inverse"></i>\
                    </a>\
                    <a href="#" class="btn btn-warning btn-xs btn-baixa"\
                        title="Efetuar baixa" style="display: ' + (item['exemplar_baixa_data'] ? 'none' : 'block') + '">\
                        <i class="fa fa-arrow-down fa-inverse"></i>\
                    </a>\
                    <a href="#" class="btn btn-info btn-xs btn-baixa-cancelar"\
                        title="Desfazer baixa" style="display: ' + (item['exemplar_baixa_data'] ? 'block' : 'none') + '">\
                        <i class="fa fa-arrow-up fa-inverse"></i>\
                    </a>\
                    <a href="#" class="btn btn-danger btn-xs item-remove"\
                        title="Remover exemplar" style="display: ' + (item['exemplar_id_r'] == '-' ? 'block' : 'none') + '">\
                        <i class="fa fa-times fa-inverse"></i>\
                    </a>\
                </div>';

                data.push(item);
            });

            if (data.length > 0) {
                priv.options.datatables.exemplares.fnAddData(data);
            }

            priv.options.datatables.exemplares.fnDraw();
        };

        priv.getProximoExemplar = function () {
            var founded = [];
            var candidate = [];
            var min = 1;
            var max = 0;

            $.each(priv.options.value.exemplares || [], function (i, item) {
                var codigo = parseInt(item['exemplar_codigo']);
                founded[codigo] = codigo;

                if (codigo > max) {
                    max = codigo;
                }
            });

            for (var i = min; i <= max; i++) {
                if (!founded[i]) {
                    candidate.push(i);
                }
            }

            if (candidate.length > 0) {
                return candidate[0];
            }

            return max + 1;
        };

        pub.getDataTable = function (pos) {
            return priv.options.datatables[pos];
        };

        pub.getOptionValue = function (pos) {
            return priv.options.value[pos];
        };

        priv.step4 = function () {
            $("#aquisicoes").componente({
                inputName: 'aquisicao',
                classCardExtra: 'col-sm-12',
                labelContainer: '<div class="form-group"/>',
                tplItem: '\
                        <div class="col-sm-12">\
                            <div class="componente-content">\
                            </div >\
                            <div class="remover-autor">\
                                <a href="#" class="btn btn-danger btn-xs item-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </a>\
                             </div>\
                        </div>',
                fieldClass: 'form-control',
                fields: [
                    {name: 'aquisicaoId', label: 'Código', type: 'hidden'},
                    {
                        name: 'tipo', label: 'Tipo', type: 'select',
                        options: [
                            {value: 'Compra', text: 'Compra'},
                            {value: 'Doacao', text: 'Doação'},
                            {value: 'Permuta', text: 'Permuta'},
                            {value: 'Outro', text: 'Outro'}
                        ],
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            var $item = field.closest('.componente-item');
                            $parent.addClass('col-sm-2');
                            field.select2({
                                language: 'pt-BR'
                            });
                            field.on('change', function () {
                                var tipo = (this.value + '').toLowerCase();
                                $item.find('.aquisicao-compra, .aquisicao-doacao, .aquisicao-permuta, .aquisicao-outro').hide();
                                $item.find('.aquisicao-' + tipo).show();
                            });

                            field
                                .select2('val', itemConfig['tipo'] || 'Compra')
                                .trigger('change');
                        }
                    },
                    {
                        name: 'exemplares', label: 'Exemplares',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-2');

                            itemConfig['exemplares'] = itemConfig['exemplares'] || null;

                            field.select2({
                                language: 'pt-BR',
                                tags: true,
                                data: priv.getExemplaresTags
                            });

                            field.select2("data", itemConfig['exemplares']).trigger('change');
                        }
                    },
                    {
                        name: 'data', label: 'Data',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-2');
                            field
                                .datepicker({
                                    prevText: '<i class="fa fa-chevron-left"></i>',
                                    nextText: '<i class="fa fa-chevron-right"></i>'
                                })
                                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
                            var data = $.datepicker.formatDate("dd/mm/yy", new Date());
                            data = field.val() || data;
                            field.val(data);
                        }
                    },
                    {
                        name: 'fornecedor', label: 'Fornecedor',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent
                                .addClass('aquisicao-compra')
                                .addClass('col-sm-3');
                            field.select2({
                                language: 'pt-BR',
                                ajax: {
                                    url: priv.options.url.empresa,
                                    dataType: 'json', delay: 250,
                                    data: function (query) {
                                        return {query: query, empresaTipo: 'Fornecedor'};
                                    },
                                    results: function (data) {
                                        var transformed = $.map(data, function (el) {
                                            el.text = el.pes_nome_fantasia;
                                            el.id = el.pes_id;

                                            return el;
                                        });

                                        return {results: transformed};
                                    }
                                }
                            });

                            field.select2("data", itemConfig['fornecedor'] || null).trigger('change');
                        }
                    },
                    {
                        name: 'notaValor', label: 'Valor',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent
                                .addClass('aquisicao-compra')
                                .addClass('col-sm-1');
                            field.inputmask({
                                showMaskOnHover: false,
                                clearIncomplete: true,
                                alias: 'currency',
                                groupSeparator: ".",
                                radixPoint: ",",
                                placeholder: "0",
                                prefix: "",
                                clearMaskOnLostFocus: true
                            });
                        }
                    },
                    {
                        name: 'notaFiscal', label: 'Nota Fiscal',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent
                                .addClass('aquisicao-compra')
                                .addClass('col-sm-2');
                            field.inputmask({
                                showMaskOnHover: false, clearIncomplete: true, alias: 'integer'
                            });
                        }
                    },
                    {
                        name: 'doador', label: 'Doador',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent
                                .addClass('aquisicao-doacao')
                                .addClass('col-sm-6');
                            field.select2({
                                language: 'pt-BR',
                                ajax: {
                                    url: priv.options.url.doador,
                                    dataType: 'json', delay: 250,
                                    data: function (query) {
                                        return {query: query};
                                    },
                                    results: function (data) {
                                        var transformed = $.map(data, function (el) {
                                            var tipo = el.pes_tipo;
                                            tipo = tipo.charAt(0).toUpperCase() + tipo.substr(1).toLowerCase();

                                            el.text = tipo + ': ' + el.pes_nome;
                                            el.id = el.pes_tipo + '-' + el.pes_id;

                                            return el;
                                        });

                                        return {results: transformed};
                                    }
                                }
                            });

                            if (itemConfig['doador']) {
                                field.select2("data", itemConfig['doador'] || null).trigger('change');
                            }
                        }
                    },
                    {
                        name: 'permutaDescricao', label: 'Descrição da permuta',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent
                                .addClass('aquisicao-permuta')
                                .addClass('col-sm-6');
                        }
                    },
                    {
                        name: 'observacao', label: 'Observação',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent
                                .addClass('aquisicao-outro')
                                .addClass('col-sm-6');
                        }
                    }
                ],
                addCallback: function (e) {
                    var item = e.data.item, itemSettings = e.data.itemSettings, settings = e.data.settings;
                    var id = item.data('componente.id') || '';
                    var campoTipo = item.find('[name="' + settings.makeFieldName({id: id, name: 'tipo'}) + '"]');


                    campoTipo.trigger('change');
                },
                removeCallback: function (parametersCallback) {
                    var item = parametersCallback.item,
                        event = parametersCallback.event,
                        settings = parametersCallback.settings,
                        data = parametersCallback.data;

                    var confirmacao = item.data('confirmacaoRemocao') || false;

                    if (!confirmacao) {
                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Deseja realmente remover esta aquisição?',
                            buttons: "[Cancelar][Ok]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Ok") {
                                $(item).data('confirmacaoRemocao', true);
                                $(event.currentTarget).click();
                            }
                        });

                        return true;
                    }

                    return false;
                }
            });

            if (priv.options.value.aquisicao) {
                $.each(priv.options.value.aquisicao, function (i, item) {
                    $('#aquisicoes').componente(['add', item]);
                });
            }
        };

        priv.setSteps = function () {
            priv.steps[0] = priv.step1;
            priv.steps[1] = priv.step2;
            priv.steps[2] = priv.step3;
            priv.steps[3] = priv.step4;
        };

        priv.loadSteps = function (begin, end) {
            begin = begin || 0;
            end = end || priv.steps.length;

            for (var i = begin; i <= end; i++) {
                var action = priv.__getFunc(priv.steps, i);
                if (action) {
                    action();
                }
            }
        };

        priv.setValidationStep1 = function () {
            priv.$validator.settings.rules = {
                grupoBibliografico: {required: true},
                dataRegistro: {required: true, dateBR: true},
                tituloTitulo: {required: true},
                tituloPaginas: {required: false},
                area: {required: true},
                areaCnpq: {required: true},
                assuntosRelacionados: {required: true},
                idiomasRelacionados: {required: true}
            };
            priv.$validator.settings.messages = {
                grupoBibliografico: {required: 'Campo obrigatório!'},
                tituloTitulo: {required: 'Campo obrigatório!'},
                tituloPaginas: {required: 'Campo obrigatório!'},
                area: {required: 'Campo obrigatório!'},
                areaCnpq: {required: 'Campo obrigatório!'},
                assuntosRelacionados: {required: 'Campo obrigatório!'},
                idiomasRelacionados: {required: 'Campo obrigatório!'}
            };

            return !$(priv.options.formElement).valid();
        };

        priv.setValidationStep2 = function () {
            priv.$validator.settings.rules = {
                tituloEdicaoVolume: {number: true},
                tituloEdicaoTomo: {maxlength: 10},
                tituloEdicaoIsbn: {required: false, maxlength: 18},
                tituloEdicaoNumero: {number: false},
                tituloEdicaoAno: {minlength: 4, maxlength: 4},
                tituloEdicaoLocal: {required: pub.retornaObrigatoriedadeCampo('tituloEdicaoLocal')}
            };
            priv.$validator.settings.messages = {
                tituloEdicaoVolume: {number: 'Número inválido!'},
                tituloEdicaoTomo: {maxlength: 'Tamanho máximo: 10!'},
                tituloEdicaoIsbn: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 18!'},
                tituloEdicaoNumero: {number: 'Número inválido!'},
                tituloEdicaoAno: {minlength: 'Tamanho mínimo: 4!', maxlength: 'Tamanho máximo: 4!'},
                tituloEdicaoLocal: {required: 'Campo obrigatório!'}
            };

            if (!$(priv.options.formElement).valid()) {
                return true;
            }

            var errors = [];
            var componente = $("#autores").componente();
            var autores = componente.items();
            var arrAutores = {};

            priv.removeErroComponente($("#autores"));

            if (pub.retornaObrigatoriedadeCampo('autor')) {
                if (autores.length < 1) {
                    errors.push("Inclua ao menos um autor para o título!");
                }

                $.each(autores, function (i, item) {
                    var $item = $(item);
                    var id = $(item).data('componente.id');
                    priv.removeErroComponente($item);

                    var campoTipo = $item.find('[name="' + componente.makeFieldName({id: id, name: 'tipo'}) + '"]');
                    var campoAutor = $item.find('[name="' + componente.makeFieldName({id: id, name: 'autor'}) + '"]');

                    var camposVazios = [];

                    if (campoTipo.val() == "") {
                        camposVazios.push('tipo de autor');
                    }

                    if (campoAutor.val() == "") {
                        camposVazios.push('autor');
                    }

                    if (camposVazios.length > 0) {
                        errors.push("O autor " + (i + 1) + " possui campos vazios. Por favor preencha os campos: <b>" + camposVazios.join(', ') + '</b>.');
                        priv.adicionaErroComponente($item, true);
                    } else {
                        var chaveAutor = campoAutor.val();

                        if (arrAutores[chaveAutor]) {
                            errors.push(
                                "O autor \"" + campoAutor.select2('data').text + "\" esta vinculado mais de uma vez!"
                            );
                            priv.adicionaErroComponente($item, true);
                        } else {
                            arrAutores[chaveAutor] = true;
                        }
                    }
                });


                if (Object.keys(errors).length > 0) {
                    if (errors) {
                        priv.adicionaErroComponente($("#autores"));
                        priv.showNotificacao({content: errors.join('<br>'), type: 'warning'});

                        return true;
                    }

                    return true;
                }
            }

            return false;
        };

        priv.setValidationStep3 = function () {
            var error = '';


            if (pub.retornaObrigatoriedadeCampo('exemplar')) {
                if (Object.keys(priv.options.value.exemplares || []).length < 1) {
                    error = "Inclua ao menos um exemplar para o título!";
                }

                if (error) {
                    priv.showNotificacao({content: error, type: 'warning'});

                    return true;
                }
            }

            return false;
        };

        priv.setValidationStep4 = function () {
            var errors = [];
            var componente = $("#aquisicoes").componente();
            var aquisicoes = componente.items();
            var arrAquisicoes = {};

            priv.removeErroComponente($("#aquisicoes"));

            //if (aquisicoes.length < 1) {
            //    errors.push("Inclua ao menos uma aquisição!");
            //}

            $.each(aquisicoes, function (i, item) {
                var $item = $(item);
                var id = $(item).data('componente.id');
                priv.removeErroComponente($item);

                var campoTipo = $item.find('[name="' + componente.makeFieldName({id: id, name: 'tipo'}) + '"]');
                var campoData = $item.find('[name="' + componente.makeFieldName({id: id, name: 'data'}) + '"]');
                var campoExemplares = $item.find('[name="' + componente.makeFieldName({
                        id: id,
                        name: 'exemplares'
                    }) + '"]');
                var campoFornecedor = $item.find('[name="' + componente.makeFieldName({
                        id: id,
                        name: 'fornecedor'
                    }) + '"]');
                var campoValor = $item.find('[name="' + componente.makeFieldName({id: id, name: 'valor'}) + '"]');
                var campoNotaFiscal = $item.find('[name="' + componente.makeFieldName({
                        id: id,
                        name: 'notaFiscal'
                    }) + '"]');
                var campoDoador = $item.find('[name="' + componente.makeFieldName({id: id, name: 'doador'}) + '"]');
                var campoPermutaDescricao = $item.find('[name="' + componente.makeFieldName({
                        id: id,
                        name: 'permutaDescricao'
                    }) + '"]');
                var campoObservacao = $item.find('[name="' + componente.makeFieldName({
                        id: id,
                        name: 'observacao'
                    }) + '"]');

                var valueTipo = campoTipo.val().toLowerCase();
                var valueData = campoData.val();
                var valueExemplares = campoExemplares.val();
                var valueFornecedor = campoFornecedor.val();
                var valueValor = campoValor.val();
                var valueNotaFiscal = campoNotaFiscal.val();
                var valueDoador = campoDoador.val();
                var valuePermutaDescricao = campoPermutaDescricao.val();
                var valueObservacao = campoObservacao.val();

                var camposVazios = [];

                if (valueTipo == "") {
                    camposVazios.push("tipo");
                }

                if (valueData == "") {
                    camposVazios.push("data");
                }

                if (valueExemplares == "") {
                    camposVazios.push("exemplares");
                }

                if (valueTipo == "compra" && valueFornecedor == "") {
                    camposVazios.push("fornecedor");
                }

                if (valueTipo == "compra" && valueNotaFiscal == "") {
                    camposVazios.push("nota fiscal");
                }

                if (valueTipo == "doacao" && valueDoador == "") {
                    camposVazios.push("doador");
                }

                if (valueTipo == "permuta" && valuePermutaDescricao == "") {
                    camposVazios.push("permuta");
                }

                if (valueTipo == "outro" && valueObservacao == "") {
                    camposVazios.push("observação");
                }

                if (camposVazios.length > 0) {
                    errors.push('A aquisição ' + (i + 1) + ' possui campos vazios. Por favor preencha os campos: <b>' + camposVazios.join(', ') + '</b>.');
                    priv.adicionaErroComponente($item, true);
                }
            });

            //if (Object.keys(priv.getExemplaresTags()['results']).length > 0) {
            //    errors.push('Existem exemplares sem aquisição definida!');
            //}

            if (Object.keys(errors).length > 0) {
                if (errors) {
                    priv.adicionaErroComponente($("#autores"));
                    priv.showNotificacao({content: errors.join('<br>'), type: 'warning'});

                    return true;
                }

                return true;
            }

            return false;
        };

        priv.setValidations = function () {
            priv.$validator = $(priv.options.formElement).validate({ignore: '.ignore'});

            $(document).on("change", ".form-control:hidden", function () {
                $(priv.options.formElement).valid();
            });

            priv.validations[0] = priv.setValidationStep1;
            priv.validations[1] = priv.setValidationStep2;
            priv.validations[2] = priv.setValidationStep3;
            priv.validations[3] = priv.setValidationStep4;

            $(priv.options.formElement).submit(function (e) {
                var ok = priv.validate();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                $('#exemplares').val(JSON.stringify(priv.options.value.exemplares));
            });
        };

        priv.validate = function () {
            var navigation = $(priv.options.wizardElement).find('ul:first');
            var nitens = navigation.find('li').length;

            for (var i = 0; i < nitens; i++) {
                var action = priv.__getFunc(priv.validations, i);

                if (action) {
                    var err = action();

                    if (err) {
                        $(".form-wizard-nav li a").eq(i).click();

                        return false;
                    }
                }
            }

            return true;
        };

        priv.wizard = function () {
            var handleTabShow = function (tab, navigation, index, wizard) {
                var total = navigation.find('li').length;
                var current = index + 0;
                var percent = (current / (total - 1)) * 100;
                var percentWidth = 100 - (100 / total) + '%';

                navigation.find('li').removeClass('done');
                navigation.find('li.active').prevAll().addClass('done');

                wizard.find('.progress-bar').css({width: percent + '%'});
                $('.form-wizard-horizontal').find('.progress').css({'width': percentWidth});
            };

            var itens = $(priv.options.wizardElement).find('.form-wizard li');
            itens.css('width', (100 / itens.length) + '%');

            $(priv.options.wizardElement).find('.progress').css({
                'width': ((100 / itens.length) * (itens.length - 1 ) * 0.99) + '%',
                'margin': '0 ' + ((100 / itens.length) / 2) + '%'
            });

            $(priv.options.wizardElement).bootstrapWizard({
                tabClass: '',
                onTabShow: function (tab, navigation, index) {
                    handleTabShow(tab, navigation, index, $(priv.options.wizardElement));
                },
                onTabClick: function (objAbaAtiva, objNavegacao, indexAbaAtiva, indexAbaDestino) {
                    var numeroAbas = objNavegacao.find('li').length;
                    if (indexAbaDestino >= numeroAbas) {
                        indexAbaDestino = numeroAbas - 1;
                    }

                    if (indexAbaAtiva < indexAbaDestino) {
                        for (var i = indexAbaAtiva; i < indexAbaDestino; i++) {
                            var action = priv.__getFunc(priv.validations, i);

                            if (action) {
                                var err = action();

                                if (err) {
                                    return false;
                                }
                            }

                            $(priv.options.wizardElement).bootstrapWizard('next');
                        }

                        return false;
                    }
                },
                onNext: function (objAbaAtiva) {
                    var indexAbaAtiva = objAbaAtiva.index();
                    var action = priv.__getFunc(priv.validations, indexAbaAtiva);

                    if (action) {
                        var err = action();

                        if (err) {
                            return false;
                        }
                    }
                }
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.loadSteps();
            priv.setValidations();
            priv.wizard();
        };
    };

    $.tituloAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.titulo.add");

        if (!obj) {
            obj = new TituloAdd();
            obj.run(params);
            $(window).data('universa.titulo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);