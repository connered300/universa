(function ($, window, document) {
    'use strict';

    var TituloIndex = function () {
        VersaShared.call(this);
        var vs = VersaShared();
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                url: {
                    empresa: '',
                    visualizador: ''
                },
                data: {
                    grupoBibliografico: []
                },
                value: {
                    grupoBibliografico: null
                }
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
            $.storage = new $.store();
        };


        priv.makeSelect2Field = function (settings) {
            settings = settings || [];
            settings = $.extend(true, {
                fieldId: '',
                optionsAttr: '',
                dataAttr: '',
                ajax: false,
                data: null,
                select2Extra: {},
                changeCallback: null
            }, settings);

            if (!settings.dataAttr) {
                settings.dataAttr = settings.optionsAttr;
            }

            return {
                settings: settings,
                setValue: function (option) {
                    option = option || null;
                    priv.options.value[this.settings.optionsAttr] = option;
                    $(this.settings.fieldId).select2("val", option);
                },
                setValueBySearch: function (search) {
                    var data = priv.options.data[this.settings.dataAttr] || null;

                    if (!data) {
                        return;
                    }

                    var selection = $.grep(data, function (el) {
                        if (el.id == search || el.text.toLowerCase() == search.toLowerCase()) {
                            return el;
                        }
                    });

                    if (!this.settings.select2Extra.multiple && !this.settings.select2Extra.tags) {
                        selection = selection[0] || null;
                    }

                    this.setValue(selection || null);
                },
                getValue: function () {
                    return priv.options.value[this.settings.optionsAttr];
                },
                init: function () {
                    var select2Helper = this;
                    var select2Options = {
                        language: 'pt-BR',
                        allowClear: true,
                        initSelection: function (element, callback) {
                            callback(select2Helper.getValue());
                        }
                    };

                    if (this.settings.ajax) {
                        select2Options['ajax'] = {url: '', dataType: 'json', delay: 250, data: null, results: ''};
                        select2Options['ajax'] = $.extend(true, select2Options['ajax'], this.settings.ajax);
                        delete select2Options['data'];
                    } else if (this.settings.data) {
                        select2Options['data'] = this.settings.data;
                        delete select2Options['ajax'];
                    } else {
                        delete select2Options['ajax'];
                        delete select2Options['data'];
                    }

                    if (this.settings.select2Extra) {
                        select2Options = $.extend(true, select2Options, this.settings.select2Extra);
                    }

                    $(this.settings.fieldId).select2(select2Options);

                    if (this.settings.select2Extra.allowClear) {
                        $(this.settings.fieldId).on('change select2-selected select2-clearing select2-removed', function () {
                            var $item = $(this);

                            if ($item.val() != '') {
                                $item.parent().find('.select2-container').addClass('select2-allowclear');
                            } else {
                                $item.parent().find('.select2-container').removeClass('select2-allowclear');
                            }
                        });
                    }

                    if (this.settings.changeCallback) {
                        $(this.settings.fieldId).on('change', this.settings.changeCallback);
                    }
                    $(this.settings.fieldId).select2("val", select2Helper.getValue());
                    $(this.settings.fieldId).trigger('change');
                }
            };
        };

        priv.setFilters = function () {
            var grupoBibliografico = priv.makeSelect2Field({
                fieldId: '#grupoBibliografico',
                optionsAttr: 'grupoBibliografico',
                data: priv.options.data.grupoBibliografico,
                select2Extra: {allowClear: true, tags: true}
            });

            var editora = priv.makeSelect2Field({
                fieldId: '#editora', optionsAttr: 'editora', ajax: {
                    url: priv.options.url.empresa,
                    dataType: 'json', delay: 250,
                    data: function (query) {
                        return {query: query, empresaTipo: 'Editora'};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome_fantasia;
                            el.id = el.pes_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var autor = priv.makeSelect2Field({
                fieldId: '#autor', optionsAttr: 'autor', ajax: {
                    url: priv.options.url.autor,
                    dataType: 'json', delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.autor_referencia;
                            el.id = el.autor_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var assunto = priv.makeSelect2Field({
                fieldId: '#assunto', optionsAttr: 'assunto', ajax: {
                    url: priv.options.url.assunto,
                    dataType: 'json', delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.assunto_descricao;
                            el.id = el.assunto_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var areaCnpq = priv.makeSelect2Field({
                fieldId: '#areaCnpq', optionsAttr: 'areaCnpq', ajax: {
                    url: priv.options.url.areaCnpq,
                    dataType: 'json', delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_cnpq_codigo + ': ' + el.area_cnpq_descricao, id: el.area_cnpq_id};
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            grupoBibliografico.init();
            editora.init();
            autor.init();
            assunto.init();
            areaCnpq.init();

            var filtrosSalvos = $.storage.get('titulo.listagem.filtros') || {};

            if (filtrosSalvos['grupoBibliografico']) {
                $('#grupoBibliografico').select2('data', filtrosSalvos['grupoBibliografico']);
            }
            if (filtrosSalvos['editora']) {
                $('#editora').select2('data', filtrosSalvos['editora']);
            }
            if (filtrosSalvos['autor']) {
                $('#autor').select2('data', filtrosSalvos['autor']);
            }
            if (filtrosSalvos['assunto']) {
                $('#assunto').select2('data', filtrosSalvos['assunto']);
            }
            if (filtrosSalvos['areaCnpq']) {
                $('#areaCnpq').select2('data', filtrosSalvos['areaCnpq']);
            }

        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableTitulo = $('#dataTableTitulo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    data: function (d) {
                        d.filter = d.filter || {};
                        var filtrosSalvos = {};
                        var grupoBibliografico = $('#grupoBibliografico').val() || "";
                        var editora = $('#editora').val() || "";
                        var autor = $('#autor').val() || "";
                        var assunto = $('#assunto').val() || "";
                        var areaCnpq = $('#areaCnpq').val() || "";

                        if (grupoBibliografico) {
                            d.filter['grupoBibliografico'] = grupoBibliografico;
                            filtrosSalvos['grupoBibliografico'] = $('#grupoBibliografico').select2('data');
                        }

                        if (editora) {
                            d.filter['editora'] = editora;
                            filtrosSalvos['editora'] = $('#editora').select2('data');
                        }

                        if (autor) {
                            d.filter['autor'] = autor;
                            filtrosSalvos['autor'] = $('#autor').select2('data');
                        }

                        if (assunto) {
                            d.filter['assunto'] = assunto;
                            filtrosSalvos['assunto'] = $('#assunto').select2('data');
                        }

                        if (areaCnpq) {
                            d.filter['areaCnpq'] = areaCnpq;
                            filtrosSalvos['areaCnpq'] = $('#areaCnpq').select2('data');
                        }

                        $.storage.set('titulo.listagem.filtros', filtrosSalvos);

                        return d;
                    },
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnArquivo = {class: 'btn-success btn-titulo-visualizar', icon: 'fa-book'};

                        for (var row in data) {
                            var btns = [{class: 'btn-primary btn-item-edit', icon: 'fa-pencil'}];

                            if (data[row]['temArquivo']) {
                                btns.push(btnArquivo);
                            }

                            data[row]['acao'] = vs.createBtnGroup(btns);
                        }

                        setTimeout(function () {
                            vs.removeOverlay($(".widget-body"));
                        }, 2000);

                        return data;
                    }
                },
                columnDefs: [
                    {name: "titulo_id", targets: colNum++, data: "titulo_id"},
                    {name: "titulo_titulo", targets: colNum++, data: "titulo_titulo"},
                    {name: "autor_referencia", targets: colNum++, data: "autor_referencia"},
                    {name: "titulo_edicao_volume", targets: colNum++, data: "titulo_edicao_volume"},
                    {name: "titulo_edicao_isbn", targets: colNum++, data: "titulo_edicao_isbn"},
                    {name: "titulo_edicao_cdu", targets: colNum++, data: "titulo_edicao_cdu"},
                    {name: "titulo_edicao_cutter", targets: colNum++, data: "titulo_edicao_cutter"},
                    {name: "titulo_edicao_ano", targets: colNum++, data: "titulo_edicao_ano"},
                    {name: "pes_nome_fantasia", targets: colNum++, data: "pes_nome_fantasia"},
                    {name: "titulo_exemplares_quantidade", targets: colNum++, data: "titulo_exemplares_quantidade"},
                    {name: "titulo_exemplares_disponiveis", targets: colNum++, data: "titulo_exemplares_disponiveis"},
                    {name: "titulo_id", targets: colNum++, data: "acao", orderable: false, searchable: false},
                    {
                        name: "titulo_exemplares_codigos",
                        targets: colNum++,
                        data: "titulo_exemplares_codigos",
                        visible: false,
                        searchable: true
                    }
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": [],
                    "sSwfPath": "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[1, 'desc']]
            });

            function reloadDatatables() {
                priv.dataTableTitulo.api().ajax.reload();
            }

            $(document).on('click', "#titulosFiltroExecutar", function () {
                vs.addOverlay($("#dataTableTitulo"));

                window.setTimeout(reloadDatatables, 5000);
            });

            $(".panel-title").click(function (event) {
                $(".panel-title .fa").toggleClass('fa-chevron-right');
                $(".panel-title .fa").toggleClass('fa-chevron-down');
            });
            $("#titulosFiltroLimpar").click(function (event) {
                $('#grupoBibliografico').val('').trigger('change');
                $('#editora').val('').trigger('change');
                $('#autor').val('').trigger('change');
                $('#assunto').val('').trigger('change');
                $('#areaCnpq').val('').trigger('change');
            });


            $(document).on("click", ".btn-item-edit", function (event) {
                vs.addOverlay($(".widget-body"));
                var $pai = $(this).closest('tr');
                var data = priv.dataTableTitulo.fnGetData($pai);

                location.href = priv.options.urlEdit + '/' + data['titulo_id'];
            });

            $(document).on("click", ".btn-titulo-visualizar", function () {
                vs.addOverlay($(".widget-body"));
                var $pai = $(this).closest('tr');
                var data = priv.dataTableTitulo.fnGetData($pai);

                if (!data['temArquivo']) {
                    vs.showNotificacaoWarning("Este título não possui nenhum arquivo disponível!");
                    vs.removeOverlay($(".widget-body"));
                    return false;
                }

                var retorno = true, extensao = '';
                var ext = data['arquivoDigital'].replace(/(^[^.*\.]*)\./, '');

                /*Depois do nome o que tiver qualquer extensão não será aceito*/

                if (extensao && value != "pdf") {
                    retorno = false;
                }

                if (!retorno) {
                    vs.showNotificacaoWarning("O leitor de títulos esta disponível somente para títulos do formatado PDF !");
                    vs.removeOverlay($(".widget-body"));
                    return false;
                }

                $('#visualizar-arquivo-modal iframe').attr('src', priv.options.url.visualizador + '/' + data['titulo_id']);
                $('#visualizar-arquivo-modal').modal('show');
                vs.removeOverlay($(".widget-body"));
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setFilters();
            priv.setSteps();
        };
    };

    $.tituloIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.titulo.index");

        if (!obj) {
            obj = new TituloIndex();
            obj.run(params);
            $(window).data('universa.titulo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);