(function ($, window, document) {
    'use strict';

    var TituloConsulta = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                url: {
                    empresa: '',
                    urlGerenciadorDeArquivosThumb: '',
                    urlVisualizarTitulo: ''
                },
                data: {
                    grupoBibliografico: []
                },
                value: {
                    grupoBibliografico: null,
                    permiteVisualizarAcervo: false
                }
            }
        };


        var vs = new VersaShared();

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
            $.storage = new $.store();
        };


        priv.makeSelect2Field = function (settings) {
            settings = settings || [];
            settings = $.extend(true, {
                fieldId: '',
                optionsAttr: '',
                dataAttr: '',
                ajax: false,
                data: null,
                select2Extra: {},
                changeCallback: null
            }, settings);

            if (!settings.dataAttr) {
                settings.dataAttr = settings.optionsAttr;
            }

            return {
                settings: settings,
                setValue: function (option) {
                    option = option || null;
                    priv.options.value[this.settings.optionsAttr] = option;
                    $(this.settings.fieldId).select2("val", option);
                },
                setValueBySearch: function (search) {
                    var data = priv.options.data[this.settings.dataAttr] || null;

                    if (!data) {
                        return;
                    }

                    var selection = $.grep(data, function (el) {
                        if (el.id == search || el.text.toLowerCase() == search.toLowerCase()) {
                            return el;
                        }
                    });

                    if (!this.settings.select2Extra.multiple && !this.settings.select2Extra.tags) {
                        selection = selection[0] || null;
                    }

                    this.setValue(selection || null);
                },
                getValue: function () {
                    return priv.options.value[this.settings.optionsAttr];
                },
                init: function () {
                    var select2Helper = this;
                    var select2Options = {
                        language: 'pt-BR',
                        allowClear: true,
                        initSelection: function (element, callback) {
                            callback(select2Helper.getValue());
                        }
                    };

                    if (this.settings.ajax) {
                        select2Options['ajax'] = {url: '', dataType: 'json', delay: 250, data: null, results: ''};
                        select2Options['ajax'] = $.extend(true, select2Options['ajax'], this.settings.ajax);
                        delete select2Options['data'];
                    } else if (this.settings.data) {
                        select2Options['data'] = this.settings.data;
                        delete select2Options['ajax'];
                    } else {
                        delete select2Options['ajax'];
                        delete select2Options['data'];
                    }

                    if (this.settings.select2Extra) {
                        select2Options = $.extend(true, select2Options, this.settings.select2Extra);
                    }

                    $(this.settings.fieldId).select2(select2Options);

                    if (this.settings.select2Extra.allowClear) {
                        $(this.settings.fieldId).on('change select2-selected select2-clearing select2-removed', function () {
                            var $item = $(this);

                            if ($item.val() != '') {
                                $item.parent().find('.select2-container').addClass('select2-allowclear');
                            } else {
                                $item.parent().find('.select2-container').removeClass('select2-allowclear');
                            }
                        });
                    }

                    if (this.settings.changeCallback) {
                        $(this.settings.fieldId).on('change', this.settings.changeCallback);
                    }
                    $(this.settings.fieldId).select2("val", select2Helper.getValue());
                    $(this.settings.fieldId).trigger('change');
                }
            };
        };

        priv.setFilters = function () {
            var grupoBibliografico = priv.makeSelect2Field({
                fieldId: '#grupoBibliografico',
                optionsAttr: 'grupoBibliografico',
                data: priv.options.data.grupoBibliografico,
                select2Extra: {allowClear: true, tags: true}
            });

            var editora = priv.makeSelect2Field({
                fieldId: '#editora', optionsAttr: 'editora', ajax: {
                    url: priv.options.url.empresa,
                    dataType: 'json', delay: 250,
                    data: function (query) {
                        return {query: query, empresaTipo: 'Editora'};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.pes_nome_fantasia;
                            el.id = el.pes_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var autor = priv.makeSelect2Field({
                fieldId: '#autor', optionsAttr: 'autor', ajax: {
                    url: priv.options.url.autor,
                    dataType: 'json', delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.autor_referencia;
                            el.id = el.autor_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var assunto = priv.makeSelect2Field({
                fieldId: '#assunto', optionsAttr: 'assunto', ajax: {
                    url: priv.options.url.assunto,
                    dataType: 'json', delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.assunto_descricao;
                            el.id = el.assunto_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            var areaCnpq = priv.makeSelect2Field({
                fieldId: '#areaCnpq', optionsAttr: 'areaCnpq', ajax: {
                    url: priv.options.url.areaCnpq,
                    dataType: 'json', delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_cnpq_codigo + ': ' + el.area_cnpq_descricao, id: el.area_cnpq_id};
                        });

                        return {results: transformed};
                    }
                },
                select2Extra: {allowClear: true, tags: true, minimumInputLength: 1}
            });

            grupoBibliografico.init();
            editora.init();
            autor.init();
            assunto.init();
            areaCnpq.init();

            var filtrosSalvos = $.storage.get('titulo.consulta.filtros') || {};

            $('#grupoBibliografico').select2('data', filtrosSalvos['grupoBibliografico'] || null);
            $('#editora').select2('data', filtrosSalvos['editora'] || null);
            $('#autor').select2('data', filtrosSalvos['autor'] || null);
            $('#assunto').select2('data', filtrosSalvos['assunto'] || null);
            $('#areaCnpq').select2('data', filtrosSalvos['areaCnpq'] || null);
            $('#tituloTitulo').val(filtrosSalvos['tituloTitulo'] || '');

            $("#titulosFiltroExecutar").click(function (event) {
                if (!priv.dataTableTitulo) {
                    priv.setSteps();
                } else {
                    priv.dataTableTitulo.api().ajax.reload(null, false);
                }
            });

            $("#titulosFiltroLimpar").click(function (event) {
                $('#grupoBibliografico').val('').trigger('change');
                $('#editora').val('').trigger('change');
                $('#autor').val('').trigger('change');
                $('#assunto').val('').trigger('change');
                $('#areaCnpq').val('').trigger('change');
                $('#tituloTitulo').val('').trigger('change');
            });
        };

        priv.setSteps = function () {
            var colNum = 0;

            priv.dataTableTitulo = $('#dataTableTitulo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    data: function (d) {
                        d.filter = d.filter || {};
                        var filtrosSalvos = {};
                        var grupoBibliografico = $('#grupoBibliografico').val() || "";
                        var editora = $('#editora').val() || "";
                        var autor = $('#autor').val() || "";
                        var assunto = $('#assunto').val() || "";
                        var areaCnpq = $('#areaCnpq').val() || "";
                        var tituloTitulo = $('#tituloTitulo').val() || "";

                        d.filter['visualizarTituloPortal'] = true;

                        if (grupoBibliografico) {
                            d.filter['grupoBibliografico'] = grupoBibliografico;
                            filtrosSalvos['grupoBibliografico'] = $('#grupoBibliografico').select2('data');
                        }

                        if (editora) {
                            d.filter['editora'] = editora;
                            filtrosSalvos['editora'] = $('#editora').select2('data');
                        }

                        if (autor) {
                            d.filter['autor'] = autor;
                            filtrosSalvos['autor'] = $('#autor').select2('data');
                        }

                        if (assunto) {
                            d.filter['assunto'] = assunto;
                            filtrosSalvos['assunto'] = $('#assunto').select2('data');
                        }

                        if (areaCnpq) {
                            d.filter['areaCnpq'] = areaCnpq;
                            filtrosSalvos['areaCnpq'] = $('#areaCnpq').select2('data');
                        }


                        if (tituloTitulo) {
                            d.filter['tituloTitulo'] = tituloTitulo;
                            filtrosSalvos['tituloTitulo'] = $('#tituloTitulo').val();
                        }

                        $.storage.set('titulo.consulta.filtros', filtrosSalvos);

                        return d;
                    },
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        var limpaVar = function (valor) {
                            valor = (typeof valor == 'undefined' ? '-' : valor);
                            valor = (valor == null ? '-' : valor);
                            valor = (valor == 'null' ? '-' : valor);
                            valor = (valor == '' ? '-' : valor);

                            return valor;
                        };

                        for (var row in data) {
                            var imagem = '&nbsp;';
                            var btn = '';
                            var tituloId = data[row]['titulo_id'];

                            if (data[row]['arq_chave']) {
                                var linkFoto = priv.options.url.urlGerenciadorDeArquivosThumb;
                                linkFoto += '/' + data[row]['arq_chave'];
                                linkFoto += '?height=200&width=200';
                                imagem = '<img src="' + linkFoto + '" class="logo"/>'
                            }

                            if (data[row]['arquivoDigital'] && parseInt(priv.options.value.permiteVisualizarAcervo)) {
                                btn = '<button type="button" class="btn btn-xs btn-success btn-titulo-visualizar" value="' + tituloId + '" data-target="#" data-toggle="#">' +
                                    '<i class="fa fa-book "></i>' +
                                    '</button>';
                            }

                            var html = '\
                            <table border="0" width="100%" class="col-sm-12">\
                                <tr>\
                                    <th rowspan="8" class="text-center">' + imagem + "<br>" + btn + '</th>\
                                    <th class="text-right col-sm-2"><strong>Titulo:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['titulo_titulo']) + '</td>\
                                    <th class="text-right col-sm-2"><strong>Nº de Registro:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['titulo_id']) + '</td>\
                                </tr>\
                                <tr>\
                                    <th class="text-right col-sm-2"><strong>Assunto(s):</strong></th>\
                                    <td class="text-left col-sm-10" colspan="3">' + limpaVar(data[row]['titulo_assuntos']) + '</td>\
                                </tr>\
                                <tr>\
                                    <th class="text-right col-sm-2"><strong>Autor(es):</strong></th>\
                                    <td class="text-left col-sm-10" colspan="3">' + limpaVar(data[row]['autor_referencia']) + '</td>\
                                </tr>\
                                <tr>\
                                    <th class="text-right col-sm-2"><strong>Editora:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['pes_nome_fantasia']) + '</td>\
                                    <th class="text-right col-sm-2"><strong>ISBN:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['titulo_edicao_isbn']) + '</td>\
                                </tr>\
                                <tr>\
                                    <th class="text-right col-sm-2"><strong>Ano Publicação:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['titulo_edicao_ano']) + '</td>\
                                    <th class="text-right col-sm-2"><strong>Edição:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['titulo_edicao_numero']) + '</td>\
                                </tr>\
                                <tr>\
                                    <th class="text-right col-sm-2"><strong>Volume:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['titulo_edicao_volume']) + '</td>\
                                    <th class="text-right col-sm-2"><strong>Tomo:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['titulo_edicao_tomo']) + '</td>\
                                </tr>\
                                <tr>\
                                    <th class="text-right col-sm-2"><strong>CDU/UDC/CDD:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['titulo_edicao_cdu']) + '</td>\
                                    <th class="text-right col-sm-2"><strong>Cutter:</strong></th>\
                                    <td class="text-left col-sm-4">' + limpaVar(data[row]['titulo_edicao_cutter']) + '</td>\
                                </tr>\
                                <tr>\
                                    <th class="text-right col-sm-2"><strong>Exemplares:</strong></th>\
                                    <td class="text-left col-sm-10" colspan="3">\
                                    ' + limpaVar(data[row]['titulo_exemplares_disponiveis']) +
                                ' disponível(is)  de \
                                ' + limpaVar(data[row]['titulo_exemplares_quantidade']) + '\
                                    ativo(s)\
                                    </td>\
                                </tr>\
                            </table>';
                            data[row]['acao'] = html;
                        }
                        return data;
                    }
                },
                columnDefs: [
                    {name: "titulo_id", targets: colNum++, data: "acao", orderable: false},
                    {name: "titulo_titulo", targets: colNum++, data: "titulo_titulo", visible: false, searchable: true},
                    {
                        name: "autor_referencia",
                        targets: colNum++,
                        data: "autor_referencia",
                        visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_volume",
                        targets: colNum++,
                        data: "titulo_edicao_volume",
                        visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_isbn",
                        targets: colNum++,
                        data: "titulo_edicao_isbn",
                        visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_cdu",
                        targets: colNum++,
                        data: "titulo_edicao_cdu",
                        visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_cutter",
                        targets: colNum++,
                        data: "titulo_edicao_cutter",
                        visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_edicao_ano",
                        targets: colNum++,
                        data: "titulo_edicao_ano",
                        visible: false,
                        searchable: true
                    },
                    {
                        name: "pes_nome_fantasia",
                        targets: colNum++,
                        data: "pes_nome_fantasia",
                        visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_exemplares_quantidade",
                        targets: colNum++,
                        data: "titulo_exemplares_quantidade",
                        visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_exemplares_disponiveis",
                        targets: colNum++,
                        data: "titulo_exemplares_disponiveis",
                        visible: false,
                        searchable: true
                    },
                    {
                        name: "titulo_exemplares_codigos",
                        targets: colNum++,
                        data: "titulo_exemplares_codigos",
                        visible: false,
                        searchable: true
                    }
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                fnDrawCallback: function () {
                    $("#dataTableTitulo thead th").attr('class', '');

                    $(".btn-titulo-visualizar").on("click", function () {
                        vs.addOverlay($(".widget-body"));

                        $('#visualizar-arquivo-modal iframe').attr('src', priv.options.url.urlVisualizarTitulo + '/' + ($(".btn-titulo-visualizar").val()));
                        $('#visualizar-arquivo-modal').modal('show');
                        vs.removeOverlay($(".widget-body"));
                    });
                },
                order: [[1, 'asc']]
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setFilters();
        };
    };

    $.tituloConsulta = function (params) {
        params = params || [];

        var obj = $(window).data("universa.titulo.consulta");

        if (!obj) {
            obj = new TituloConsulta();
            obj.run(params);
            $(window).data('universa.titulo.consulta', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);