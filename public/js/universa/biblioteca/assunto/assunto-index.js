(function ($, window, document) {
    'use strict';

    var AssuntoIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.showModal = function (data) {
            data = data || {};
            var assuntosRelacionados = [];
            var actionTitle = 'Cadastrar';
            var $form = $('#assunto-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                var assuntoRelacionadoId = data['assunto_relacionado_id'];
                var assuntoRelacionadoDescricao = data['assunto_relacionado_descricao'];

                if (data['assunto_relacionado_id']) {
                    assuntoRelacionadoId = assuntoRelacionadoId.split(',');
                    assuntoRelacionadoDescricao = assuntoRelacionadoDescricao.split(',');

                    for (var i in assuntoRelacionadoId) {
                        assuntosRelacionados.push({id: assuntoRelacionadoId[i], text: assuntoRelacionadoDescricao[i]});
                    }
                }

                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['assunto_id']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $(".has-error .help-block").hide();
            $(".has-error").removeClass("has-error");

            $.assuntoAdd().setAssuntosRelacionados(assuntosRelacionados);


            $('#novo-assunto .modal-title').html(actionTitle + ' assunto');
            $('#assuntosRelacionados').select2('val', assuntosRelacionados);
            $('#assuntoId').val(data['assunto_id'] || '');
            $('#assuntoTitulos').val(data['assunto_titulos'] || '');
            $('#assuntoDescricao').val(data['assunto_descricao'] || '');
            $('#novo-assunto').modal();
        };

        priv.setSteps = function () {
            priv.dataTableAssunto = $('#dataTableAssunto').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        return json.data;
                    }
                },
                columnDefs: [
                    {name: "assunto_descricao", targets: 0, data: "assunto_descricao"},
                    {name: "assunto_relacionado_descricao", targets: 1, data: "assunto_relacionado_descricao"},
                    {name: "assunto_titulos", targets: 2, data: "assunto_titulos"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0]]
            });

            $(document).on("click", "#dataTableAssunto tbody tr", function (event) {
                var position = priv.dataTableAssunto.fnGetPosition(this);
                var data = priv.dataTableAssunto.fnGetData(position);
                priv.showModal(data)
            });

            $('#btn-novo-assunto').click(function () {
                priv.showModal()
            });
        };

        pub.getDataTableAssunto = function () {
            return priv.dataTableAssunto;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.assuntoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.assunto.index");

        if (!obj) {
            obj = new AssuntoIndex();
            obj.run(params);
            $(window).data('universa.assunto.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);