(function ($, window, document) {
    'use strict';

    var AssuntoAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                assuntosRelacionados: null,
                urlPesquisaAssunto: '',
                ajaxSubmit: false,
                listagem: false
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {
            $("#assuntosRelacionados").select2({
                language: 'pt-BR',
                tags: true,
                ajax: {
                    url: priv.options.urlPesquisaAssunto,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        var assuntoId = $('#assuntoId').val();

                        if (assuntoId != "") {
                            data['assuntoId'] = assuntoId;
                        }

                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.assunto_descricao, id: el.assunto_id};
                        });

                        return {results: transformed};
                    }
                },
                allowClear: true,
                initSelection: function (element, callback) {
                    callback(pub.getAssuntosRelacionados());
                }
            });

            if (priv.options.assuntosRelacionados) {
                $('#assuntosRelacionados').select2("val", priv.options.assuntosRelacionados);
            }
        };

        priv.setValidations = function () {
            $("#assunto-form").validate({
                submitHandler: function (form) {
                    var assuntoTitulos = $('#assuntoTitulos').val() || 0;
                    var $form = $(form);

                    if (assuntoTitulos > 0) {
                        var confirmacao = $form.data('confirmacaoEnvio') || false;

                        if (!confirmacao) {
                            $.SmartMessageBox({
                                title: "Confirme a operação:",
                                content: 'Este assunto está ligado a ' + assuntoTitulos + ' título' + (assuntoTitulos == 1 ? '' : 's') + '. Deseja realmente alterá-lo?',
                                buttons: "[Cancelar][Ok]"
                            }, function (ButtonPress) {
                                if (ButtonPress == "Ok") {
                                    $("#assunto-form").data('confirmacaoEnvio', true);
                                    $("#assunto-form").submit();
                                }
                            });

                            return false;
                        }

                        $("#assunto-form").data('confirmacaoEnvio', false);
                    }

                    if (priv.options.ajaxSubmit) {
                        var dados = $form.serializeJSON();
                        dados['ajax'] = true;
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível salvar o assunto:",
                                        content: "<i>" + data.erro.join('<br>') + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });
                                } else if (priv.options.listagem) {
                                    $.assuntoIndex().getDataTableAssunto().api().ajax.reload(null, false);
                                    $('#novo-assunto').modal('hide');
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Assunto salvo!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    } else {
                        form.submit();
                    }
                },
                rules: {
                    assuntoDescricao: 'required'
                },
                messages: {
                    assuntoDescricao: 'Campo obrigatório!'
                },
                ignore: '.ignore'
            });
        };

        pub.setAssuntosRelacionados = function (assuntosRelacionados) {
            priv.options.assuntosRelacionados = assuntosRelacionados || null;
        };

        pub.getAssuntosRelacionados = function () {
            return priv.options.assuntosRelacionados;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.assuntoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.assunto.add");

        if (!obj) {
            obj = new AssuntoAdd();
            obj.run(params);
            $(window).data('universa.assunto.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);