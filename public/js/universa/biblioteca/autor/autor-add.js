(function ($, window, document) {
    'use strict';

    var AutorAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlAutocompleteCutter: '',
                ajaxSubmit: false,
                listagem: false
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {
            $("#autorCutter").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: priv.options.urlAutocompleteCutter,
                        data: {query: request.term},
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.label = el.cutter_num + ' - ' + el.cutter_nome;

                                return el;
                            });

                            if (Object.keys(transformed).length == 0) {
                                transformed.push({label: '-'});
                            }

                            response(transformed);
                        }
                    });
                },
                select: function (event, ui) {
                    $("#autorCutter").val(ui.item.cutter_nome[0] + ui.item.cutter_num);

                    return false;
                },
                change: function (event, ui) {
                    if (ui.item == null) {
                        $(this).val((ui.item ? ui.item.id : ""));
                    }
                },
                minLength: 1
            });
            var dataAutocomplete = $('#autorCutter').data("autocomplete") || $('#autorCutter').data("ui-autocomplete");

            dataAutocomplete._renderItem = function (ul, el) {
                if (el.label == '-') {
                    return $('<li class="ui-menu-item-no-result">').append('Sem resultados para a pesquisa.').appendTo(ul);
                }

                return $("<li></li>")
                    .append($("<a></a>").html(el.label))
                    .appendTo(ul);
            };
        };

        priv.setValidations = function () {
            $("#autor-form").validate({
                submitHandler: function (form) {
                    var autorTitulos = $('#autorTitulos').val() || 0;
                    var $form = $(form);

                    if (autorTitulos > 0) {
                        var confirmacao = $form.data('confirmacaoEnvio') || false;

                        if (!confirmacao) {
                            $.SmartMessageBox({
                                title: "Confirme a operação:",
                                content: 'Este autor está ligado a ' + autorTitulos + ' título' + (autorTitulos == 1 ? '' : 's') + '. Deseja realmente alterá-lo?',
                                buttons: "[Cancelar][Ok]"
                            }, function (ButtonPress) {
                                if (ButtonPress == "Ok") {
                                    $("#autor-form").data('confirmacaoEnvio', true);
                                    $("#autor-form").submit();
                                }
                            });

                            return false;
                        }
                    }

                    $("#autor-form").data('confirmacaoEnvio', false);

                    if (priv.options.ajaxSubmit) {
                        var dados = $form.serializeJSON();
                        dados['ajax'] = true;
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível salvar o autor:",
                                        content: "<i>" + data.erro.join('<br>') + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });
                                } else if (priv.options.listagem) {
                                    $.autorIndex().getDataTableAutor().api().ajax.reload(null, false);
                                    $('#novo-autor').modal('hide');
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Autor salvo!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    } else {
                        form.submit();
                    }
                },
                rules: {
                    autorReferencia: 'required',
                    autorCutter: 'required'
                },
                messages: {
                    autorReferencia: 'Campo obrigatório!',
                    autorCutter: 'Campo obrigatório!'
                },
                ignore: '.ignore'
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.autorAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.autor.add");

        if (!obj) {
            obj = new AutorAdd();
            obj.run(params);
            $(window).data('universa.autor.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);