(function ($, window, document) {
    'use strict';

    var AutorIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#autor-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['autor_id']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $(".has-error .help-block").hide();
            $(".has-error").removeClass("has-error");

            $('#novo-autor .modal-title').html(actionTitle + ' autor');
            $('#autorReferencia').val(data['autor_referencia'] || '');
            $('#autorCutter').val(data['autor_cutter'] || '');
            $('#autorId').val(data['autor_id'] || '');
            $('#autorTitulos').val(data['autor_titulos'] || '');
            $('#novo-autor').modal();
        };

        priv.setSteps = function () {
            priv.dataTableAutor = $('#dataTableAutor').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        return json.data;
                    }
                },
                columnDefs: [
                    {name: "autor_referencia", targets: 0, data: "autor_referencia"},
                    {name: "autor_cutter", targets: 1, data: "autor_cutter"},
                    {name: "autor_titulos", targets: 2, data: "autor_titulos"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0]]
            });

            $(document).on("click", "#dataTableAutor tbody tr", function (event) {
                var position = priv.dataTableAutor.fnGetPosition(this);
                var data = priv.dataTableAutor.fnGetData(position);
                priv.showModal(data)

            });

            $('#btn-novo-autor').click(function () {
                priv.showModal()
            });
        };

        pub.getDataTableAutor = function () {
            return priv.dataTableAutor;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.autorIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.autor.index");

        if (!obj) {
            obj = new AutorIndex();
            obj.run(params);
            $(window).data('universa.autor.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);