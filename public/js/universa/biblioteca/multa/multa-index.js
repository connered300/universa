(function ($, window, document) {
    'use strict';

    var MultaIndex = function () {
        VersaShared.call(this);

        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlCancelar: '',
                urlBaixa: '',
                permissaoBaixa: false,
                permissaoCancelar: false,
                comprovantePagamento: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#multa-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['multa_id']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $(".has-error .help-block").hide();
            $(".has-error").removeClass("has-error");

            $('#nova-multa .modal-title').html(actionTitle + ' multa');
            $('#multaDescricao').val(data['multa_descricao'] || '');
            $('#multaId').val(data['multa_id'] || '');
            $('#multaTitulos').val(data['multa_titulos'] || '');
            $('#nova-multa').modal();
        };

        priv.setSteps = function () {
            var colNum = 0;
            var $dataTableMulta = $('#dataTableMulta');
            priv.dataTableMulta = $dataTableMulta.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        for (var row in data) {
                            var titulo_valor_pago = data[row]['titulo_valor_pago'];
                            var titulo_data_processamento = new Date(data[row]['titulo_data_processamento']);
                            titulo_data_processamento = $.formatDateTime('dd/mm/yy hh:ii', titulo_data_processamento);
                            var titulo_data_vencimento = new Date(data[row]['titulo_data_vencimento']);
                            titulo_data_vencimento = $.formatDateTime('dd/mm/yy hh:ii', titulo_data_vencimento);
                            var titulo_data_pagamento = data[row]['titulo_data_pagamento'] || false;
                            titulo_data_pagamento = titulo_data_pagamento ? new Date(titulo_data_pagamento) : false;
                            titulo_data_pagamento = (titulo_data_pagamento ? $.formatDateTime('dd/mm/yy hh:ii', titulo_data_pagamento) : '-');

                            data[row]['titulo_data_processamento_f'] = titulo_data_processamento;
                            data[row]['titulo_data_vencimento_f'] = titulo_data_vencimento;
                            data[row]['titulo_data_pagamento_f'] = titulo_data_pagamento;
                            data[row]['titulo_valor_pago_f'] = titulo_valor_pago;

                            if ((data[row]['titulo_estado'] || "").toLowerCase() != 'pago') {
                                data[row]['titulo_data_pagamento_f'] = '-';
                                data[row]['titulo_valor_pago_f'] = '-';
                            }

                            data[row]['acao'] = '-';
                            var btns = [];

                            if ((data[row]['titulo_estado'] || "").toLowerCase() == 'aberto') {


                                if (priv.options.permissaoCancelar) {
                                    btns.push({
                                        class: 'btn-danger btn-xs multa-cancelamento',
                                        icon: 'fa-times fa-inverse',
                                        title: 'Cancelar multa'
                                    });
                                }

                                if (priv.options.permissaoBaixa) {
                                    btns.push({
                                        class: 'btn-success btn-xs multa-baixa',
                                        icon: 'fa-arrow-down fa-inverse',
                                        title: 'Baixar da multa'
                                    });
                                }

                                data[row]['acao'] = pub.createBtnGroup(btns);
                            }

                            if (data[row]['titulo_data_pagamento']) {
                                btns.push({
                                    class: 'btn-primary btn-xs comprovante-pagamento',
                                    icon: 'fa fa-print fa-inverse',
                                    title: 'Imprimir comprovante'
                                });

                                data[row]['acao'] = pub.createBtnGroup(btns);
                            }
                        }

                        return data;
                    }
                },
                columnDefs: [
                    {name: "titulo_id", targets: colNum++, data: "acao", orderable: false},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "login_baixa", targets: colNum++, data: "login_baixa"},
                    {name: "login_autor", targets: colNum++, data: "login_autor"},
                    {
                        name: "titulo_data_processamento",
                        targets: colNum++,
                        data: "titulo_data_processamento_f",
                        type: "date-eu"
                    },
                    {
                        name: "titulo_data_vencimento",
                        targets: colNum++,
                        data: "titulo_data_vencimento_f",
                        type: "date-eu"
                    },
                    {
                        name: "titulo_data_pagamento",
                        targets: colNum++,
                        data: "titulo_data_pagamento_f",
                        type: "date-eu"
                    },
                    {name: "titulo_valor", targets: colNum++, data: "titulo_valor"},
                    {name: "titulo_valor_pago", targets: colNum++, data: "titulo_valor_pago_f"},
                    {name: "titulo_observacoes", targets: colNum++, data: "titulo_observacoes"},
                    {name: "titulo_estado", targets: colNum++, data: "titulo_estado"},
                    {name: "titulo_id", targets: colNum++, data: "titulo_id", visible: false, searchable: true},
                    {name: "tipotitulo_id", targets: colNum++, data: "tipotitulo_id", visible: false, searchable: true},
                    {name: "pes_id", targets: colNum++, data: "pes_id", visible: false, searchable: true},
                    {name: "usuario_baixa", targets: colNum++, data: "usuario_baixa", visible: false, searchable: true},
                    {name: "usuario_autor", targets: colNum++, data: "usuario_autor", visible: false, searchable: true},
                    {name: "emprestimo_id", targets: colNum++, data: "emprestimo_id", visible: false, searchable: false}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                orderable: false,
                order: [[4, 'desc']]
            });

            $('#corpo-recibo-modal').load(function () {
                pub.removeOverlay($(this).closest('.modal-content'));
            });

            $dataTableMulta.on('click', '.comprovante-pagamento', function (e) {
                var $pai = $(this).closest('tr');
                var arrMulta = priv.dataTableMulta.fnGetData($pai);

                var $modal = $('#multa-comprovante-modal');
                var tituloId = arrMulta['titulo_id'] || null;

                if (tituloId) {
                    var url = priv.options.comprovantePagamento + '?exibirObs=1&tituloId=' + tituloId;
                    $('#corpo-recibo-modal').attr('src', url);
                    pub.addOverlay($modal.find('.modal-content'), 'Aguarde, carregando comprovante...');
                    $modal.modal('show');

                    $('#multa-comprovante-impressao').click(function () {
                        $('#corpo-recibo-modal').get(0).contentWindow.print();
                    })
                }
            });

            $dataTableMulta.on('click', '.multa-cancelamento', function (e) {
                var $pai = $(this).closest('tr');
                var arrTitulo = priv.dataTableMulta.fnGetData($pai);
                var arrCancelar = {
                    tituloId: arrTitulo['titulo_id'],
                    emprestimoId: arrTitulo['emprestimo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente cancelar esta multa?<br>Digite o motivo abaixo:',
                    buttons: "[Cancelar][Ok]",
                    input: "text",
                    placeholder: "Motivo do cancelamento.",
                    inputValue: ""
                }, function (ButtonPress, motivo) {
                    if (!motivo) {
                        return false;
                    }

                    if (ButtonPress == "Ok") {
                        arrCancelar['motivo'] = motivo;
                        $.ajax({
                            url: priv.options.urlCancelar,
                            type: 'POST',
                            dataType: 'json',
                            data: arrCancelar,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível cancelar a multa:",
                                        content: "<i>" + data.erro.join('<br>') + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });
                                } else {
                                    priv.dataTableMulta.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Multa cancelada!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });

            $dataTableMulta.on('click', '.multa-baixa', function (e) {
                var $pai = $(this).closest('tr');
                var arrTitulo = priv.dataTableMulta.fnGetData($pai);

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente baixar esta multa?',
                    buttons: "[Cancelar][Ok]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Ok") {
                        $.ajax({
                            url: priv.options.urlBaixa,
                            type: 'POST',
                            dataType: 'json',
                            data: {tituloId: arrTitulo['titulo_id']},
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível baixar a multa:",
                                        content: data['erroDescricao'],
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });
                                } else {
                                    priv.dataTableMulta.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Multa Baixada!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        pub.getDataTableMulta = function () {
            return priv.dataTableMulta;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.multaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.multa.index");

        if (!obj) {
            obj = new MultaIndex();
            obj.run(params);
            $(window).data('universa.multa.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);