(function ($, window, document) {
    'use strict';

    var SuspensaoIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlCancelar: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableSuspensao = $('#dataTableSuspensao').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        for (var row in data) {
                            var tituloTitulo    = '',
                                suspensaoInicio = '',
                                suspensaoFim    = '',
                                pesNome         = '';

                            tituloTitulo += '<b>' + data[row]['titulo_titulo'] + '</b>. ';
                            tituloTitulo += ' (';
                            tituloTitulo += 'Grupo: ' + data[row]['grupo_bibliografico_nome'];
                            tituloTitulo += ', ';
                            tituloTitulo += 'Título: ' + parseInt(data[row]['titulo_id']);
                            tituloTitulo += ', ';
                            tituloTitulo += 'Exemplar: ' + data[row]['exemplar_codigo'];
                            tituloTitulo += ')';

                            suspensaoInicio = new Date(data[row]['suspensao_inicio']);
                            suspensaoInicio = $.formatDateTime('dd/mm/yy hh:ii', suspensaoInicio);

                            suspensaoFim = new Date(data[row]['suspensao_fim']);
                            suspensaoFim = $.formatDateTime('dd/mm/yy hh:ii', suspensaoFim);

                            pesNome = parseInt(data[row]['pes_id']) + ' - <b>' + data[row]['pes_nome'] + '</b>';

                            data[row]['titulo_titulo_f'] = tituloTitulo;
                            data[row]['suspensao_inicio_f'] = suspensaoInicio;
                            data[row]['suspensao_fim_f'] = suspensaoFim;
                            data[row]['pes_nome_f'] = pesNome;

                            if (data[row]['suspensao_estado'] == 'Cumprida') {
                                data[row]['acao'] = ' - ';
                            } else if (data[row]['suspensao_estado'] == 'Ativa') {
                                data[row]['acao'] = '<button type="button" class="btn btn-xs btn-danger suspensao-baixa">' +
                                                    '<i class="fa fa-arrow-down fa-inverse"></i>' +
                                                    '</button>';
                            } else {
                                data[row]['acao'] = '<button type="button" class="btn btn-xs btn-info suspensao-info">' +
                                                    '<i class="fa fa-info-circle fa-inverse"></i>' +
                                                    '</button>';
                            }
                        }

                        return data;
                    }
                },
                columnDefs: [
                    {name: "emprestimo_id", targets: colNum++, data: "acao"},
                    {name: "suspensao_estado", targets: colNum++, data: "suspensao_estado"},
                    {name: "suspensao_inicio", targets: colNum++, data: "suspensao_inicio_f"},
                    {name: "suspensao_fim", targets: colNum++, data: "suspensao_fim_f"},
                    {name: "pes_nome", targets: colNum++, data: "pes_nome_f"},
                    {name: "titulo_titulo", targets: colNum++, data: "titulo_titulo_f"},
                    {name: "modalidade_emprestimo_descricao", targets: colNum++, data: "modalidade_emprestimo_descricao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                ordering: false
            });

            $('#dataTableSuspensao').on('click', '.suspensao-baixa', function (e) {
                var $pai = $(this).closest('tr');
                var arrTitulo = priv.dataTableSuspensao.fnGetData($pai);
                var arrLiberar = {
                    emprestimoId: arrTitulo['emprestimo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente <b>LIBERAR</b> esta suspensao?<br>Digite o motivo abaixo:',
                    buttons: "[Cancelar][Ok]",
                    input: "text",
                    placeholder: "Motivo pelo qual o aluno está sendo liberado.",
                    inputValue: ""
                }, function (ButtonPress, motivo) {
                    if (!motivo) {
                        return false;
                    }

                    if (ButtonPress == "Ok") {
                        arrLiberar['motivo'] = motivo;
                        $.ajax({
                            url: priv.options.urlCancelar,
                            type: 'POST',
                            dataType: 'json',
                            data: arrLiberar,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível liberar a suspensão:",
                                        content: "<i>" + data.erro.join('<br>') + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });
                                } else {
                                    priv.dataTableSuspensao.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Suspensão liberada!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });

            $('#dataTableSuspensao').on('click', '.suspensao-info', function (e) {
                var $pai = $(this).closest('tr');
                var arrTitulo = priv.dataTableSuspensao.fnGetData($pai);

                var dataLiberacao = new Date(arrTitulo['suspensao_liberacao_data']);
                dataLiberacao = $.formatDateTime('dd/mm/yy hh:ii', dataLiberacao);
                var strInfoLiberacao = '';
                strInfoLiberacao += '<b>Data da liberação:</b> ' + dataLiberacao + '<br>';
                strInfoLiberacao += '<b>Usuário responsável:</b> ' + arrTitulo['login'] + '<br>';
                strInfoLiberacao += '<b>Motivo:</b> ' + arrTitulo['suspensao_liberacao_motivo'] + '<br>';

                $.SmartMessageBox({
                    title: "Informação sobre suspensão liberada:",
                    content: strInfoLiberacao,
                    buttons: "[Ok]"
                });

                e.preventDefault();
            });
        };

        pub.getDataTableSuspensao = function () {
            return priv.dataTableSuspensao;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.suspensaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.suspensao.index");

        if (!obj) {
            obj = new SuspensaoIndex();
            obj.run(params);
            $(window).data('universa.suspensao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);