(function ($, window, document) {
    'use strict';

    var SetorIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: ''
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#setor-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['setor_id']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);

            $(".has-error .help-block").hide();
            $(".has-error").removeClass("has-error");

            $('#novo-setor .modal-title').html(actionTitle + ' setor');
            $('#setorSigla').val(data['setor_sigla'] || '');
            $('#setorDescricao').val(data['setor_descricao'] || '');
            $('#setorId').val(data['setor_id'] || '');
            $('#setorTitulos').val(data['setor_titulos'] || '');
            $('#novo-setor').modal();
        };

        priv.setSteps = function () {
            priv.dataTableSetor = $('#dataTableSetor').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        return json.data;
                    }
                },
                columnDefs: [
                    {name: "setor_sigla", targets: 0, data: "setor_sigla"},
                    {name: "setor_descricao", targets: 1, data: "setor_descricao"},
                    {name: "setor_titulos", targets: 2, data: "setor_titulos"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": []
                },
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $(document).on("click", "#dataTableSetor tbody tr", function (event) {
                var position = priv.dataTableSetor.fnGetPosition(this);
                var data = priv.dataTableSetor.fnGetData(position);
                priv.showModal(data)

            });

            $('#btn-novo-setor').click(function () {
                priv.showModal()
            });
        };

        pub.getDataTableSetor = function () {
            return priv.dataTableSetor;
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.setorIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.setor.index");

        if (!obj) {
            obj = new SetorIndex();
            obj.run(params);
            $(window).data('universa.setor.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);