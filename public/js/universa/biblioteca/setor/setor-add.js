(function ($, window, document) {
    'use strict';

    var SetorAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                ajaxSubmit: false,
                listagem: false
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.setSteps = function () {};

        priv.setValidations = function () {
            $("#setor-form").validate({
                submitHandler: function (form) {
                    var setorTitulos = $('#setorTitulos').val() || 0;
                    var $form = $(form);

                    if (setorTitulos > 0) {
                        var confirmacao = $form.data('confirmacaoEnvio') || false;

                        if (!confirmacao) {
                            $.SmartMessageBox({
                                title: "Confirme a operação:",
                                content: 'Este setor está ligado a ' + setorTitulos + ' título' + (setorTitulos == 1 ? '' : 's') + '. Deseja realmente alterá-lo?',
                                buttons: "[Cancelar][Ok]"
                            }, function (ButtonPress) {
                                if (ButtonPress == "Ok") {
                                    $("#setor-form").data('confirmacaoEnvio', true);
                                    $("#setor-form").submit();
                                }
                            });

                            return false;
                        }

                        $("#setor-form").data('confirmacaoEnvio', false);
                    }

                    if (priv.options.ajaxSubmit) {
                        var dados = $form.serializeJSON();
                        dados['ajax'] = true;
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível salvar o setor:",
                                        content: "<i>" + data.erro.join('<br>') + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });
                                } else if (priv.options.listagem) {
                                    $.setorIndex().getDataTableSetor().api().ajax.reload(null, false);
                                    $('#novo-setor').modal('hide');
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Setor salvo!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    } else {
                        form.submit();
                    }
                },
                rules: {
                    setorSigla: {required: true, maxlength: 5},
                    setorDescricao: 'required'
                },
                messages: {
                    setorSigla: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo atingido!'},
                    setorDescricao: 'Campo obrigatório!'
                },
                ignore: '.ignore'
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
        };
    };

    $.setorAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.setor.add");

        if (!obj) {
            obj = new SetorAdd();
            obj.run(params);
            $(window).data('universa.setor.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);