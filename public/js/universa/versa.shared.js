var VersaShared = VersaShared || function () {
        this.validations = this.validations || [];
        this.steps = this.steps || {};
        this.defaults = this.defaults || {};
        this.options = this.options || this.defaults || {};
        var __versaShared = this;

        /**
         * Cria um identificador exclusivo (GUID)
         * @returns {string}
         */
        this.createGUID = function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };

        /**
         * Verificar se uma determinada posição de um objeto ou um array é uma função,
         * caso positivo retorna a função, caso negativo retorna falso
         * @param container {Object|Array}
         * @param position {string}
         * @returns {function|bool}
         */
        this.getFunction = function (container, position) {
            container = container || window;
            position = position || false;

            if (this.isFunction(container, position)) {
                return container[position];
            }

            return false;
        };

        /**
         * Verificar se uma determinada posição de um objeto ou um array é uma função,
         * caso positivo retorna true, caso negativo retorna falso
         * @param container {Object|Array}
         * @param position {string}
         * @returns {bool}
         */
        this.isFunction = function (container, position) {
            container = container || window;
            position = position || false;

            if (position && container[position] && typeof container[position] == 'function') {
                return true;
            }

            return false;
        };

        this.setDefaultsValidator = function () {
            if ($['validator']) {
                $.validator.setDefaults({
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        }
                        else if (element.parent('label').length) {
                            error.insertAfter(element.parent());
                        }
                        else {
                            error.insertAfter(element);
                        }
                    }
                });
            }
        };

        this.setDefaults = function (opts, defaults) {
            defaults = defaults || this.defaults || [];
            opts = opts || [];

            this.options = $.extend(true, defaults, opts);

            __versaShared.setDefaultsValidator();

            if ($['browser'] && $['browser']['webkit']) {
                $("input[type!=password][autocomplete=off]").prop("type", "search");
            }

            if ($['datetimepicker']) {
                $.datetimepicker.setLocale('pt-BR');
            }

            if ($['fn'] && $['fn']['dataTable']) {
                $.fn.dataTable.ext.errMode = 'none';
            }

            if (this.options.formElement) {
                var optsValidate = {ignore: '.ignore'};

                if (this.isFunction(this, 'submitHandler')) {
                    optsValidate['submitHandler'] = this.submitHandler;
                }

                this.options.validator = $(this.options.formElement).validate(optsValidate);
            }

            $('.close-modal, .modal-header>.close').each(function (i, item) {
                var $item = $(item);

                if (!$item.data('clickmodal')) {
                    $item.click(function () {
                        $($(this).closest('.modal')).modal('hide');
                    });
                    $item.data('clickmodal', true);
                }
            });

            //Leitora de código de barras
            $(document).on('keydown', '.select2-input, .ui-autocomplete-input, .form-control', function (e) {
                if (e.ctrlKey && (e.which == 74)) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }
            });

            if ($['store'] && !$['storage']) {
                $.storage = new $.store();
            }
        };

        this.numPad = function (n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        };

        this.showNotificacao = function (param) {
            var options = $.extend(
                true,
                {content: '', title: '', type: 'info', icon: '', color: '', timeout: false},
                param
            );

            switch (options.type) {
                case 'danger':
                    options.icon = 'fa-times-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#a90329';
                    break;
                case 'info':
                    options.icon = 'fa-info-circle';
                    options.title = options.title || 'Informação:';
                    options.color = '#57889c';
                    break;
                case 'success':
                    options.icon = 'fa-check-circle';
                    options.title = options.title || 'Sucesso:';
                    options.color = '#739e73';
                    break;
                case 'warning':
                    options.icon = 'fa-exclamation-circle';
                    options.title = options.title || 'Atenção:';
                    options.color = '#c79121';
                    break;
            }

            var optionsNotification = {
                title: options.title,
                content: "<i>" + options.content + "</i>",
                color: options.color,
                icon: "fa " + options.icon
            };

            if (options.timeout) {
                optionsNotification.timeout = options.timeout;
            }

            if ($['smallBox'] && !__versaShared.options['useToaster']) {
                $.smallBox(optionsNotification);
            } else if ($['toaster']) {
                if (optionsNotification['title'][optionsNotification['title'].length - 1] == ':') {
                    optionsNotification['title'] = optionsNotification['title'].substring(
                        0,
                        optionsNotification['title'].length - 1
                    );
                }

                optionsNotification['message'] = '<br>' + optionsNotification['content'];
                optionsNotification['priority'] = options.type;

                if (optionsNotification['timeout']) {
                    $.toaster({settings: {timeout: optionsNotification['timeout'] / 5}});
                }


                $.toaster(optionsNotification);
            } else {
                var mensagem = optionsNotification.title + '<br>' + optionsNotification.content;
                mensagem = mensagem.replace(/<br(\s|)(\/|)>/img, "\n");
                mensagem = $('<p/>').html(mensagem).text();
                alert(mensagem);
            }

        };

        this.showNotificacaoFN = function (content, title, timeout, type) {
            content = content || '';
            title = title || '';
            timeout = timeout || 20000;

            if (!content) {
                return;
            }

            this.showNotificacao({
                content: content, type: type, title: title, timeout: timeout
            });
        };

        this.showNotificacaoInfo = function (content, title, timeout) {
            this.showNotificacaoFN(content, title, timeout, 'info');
        };

        this.showNotificacaoSuccess = function (content, title, timeout) {
            this.showNotificacaoFN(content, title, timeout, 'success');
        };

        this.showNotificacaoDanger = function (content, title, timeout) {
            this.showNotificacaoFN(content, title, timeout, 'danger');
        };

        this.showNotificacaoWarning = function (content, title, timeout) {
            this.showNotificacaoFN(content, title, timeout, 'warning');
        };

        this.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        this.removeOverlay = function (el) {
            el.find('.app-overlay').remove();
        };

        this.removeErroComponente = function (elemento) {
            if (elemento.find('>.component-item').length > 0) {
                elemento.find('>.component-item').removeClass('bg-danger');
                elemento.find('>.component-item .form-group .help-block').remove();
            } else {
                elemento.removeClass('bg-danger');
            }
        };

        this.adicionaErroComponente = function (elemento) {
            var $card = null;

            if (elemento.find('>.component-item').length > 0) {
                $card = elemento.find('>.component-item');
                $card.addClass('bg-danger');
            } else {
                $card = elemento;
                $card.addClass('bg-danger');
            }
        };

        this.setValidations = function () {
            $(document).on("change", ".form-control:hidden", function () {
                $(__versaShared.options.formElement).valid();
            });

            $(__versaShared.options.formElement).submit(function (e) {
                var ok = __versaShared.validate();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }
            });
        };

        this.validate = function () {
            var navigation = $(this.options.wizardElement).find('ul:first');
            var nitens = navigation.find('li').length || Object.keys(__versaShared.steps).length;
            var keys = Object.keys(this.steps);

            var $form = $(__versaShared.options.formElement);
            $form.data('noSubmit', true);

            for (var i = 0; i < nitens; i++) {
                if (__versaShared.isFunction(__versaShared.steps[keys[i]] || [], 'validate')) {
                    var err = __versaShared.steps[keys[i]].validate();

                    if (err) {
                        navigation.find("li a").eq(i).trigger('click');

                        return false;
                    }
                }
            }

            $form.data('noSubmit', false);

            return true;
        };

        this.loadSteps = function (begin, end) {
            var keys = Object.keys(this.steps);

            for (var i = 0; i < keys.length; i++) {
                if (this.isFunction(this.steps[keys[i]] || [], 'init')) {
                    this.steps[keys[i]].init();
                }
            }
        };

        this.wizard = function () {
            var handleTabShow = function (tab, navigation, index, wizard) {
                var total = navigation.find('li').length;
                var current = index + 0;
                var percent = (current / (total - 1)) * 100;
                var percentWidth = 100 - (100 / total) + '%';

                navigation.find('li').removeClass('done');
                navigation.find('li.active').prevAll().addClass('done');

                wizard.find('.progress-bar').css({width: percent + '%'});
                $(__versaShared.options.wizardElement)
                    .find('.form-wizard-horizontal')
                    .find('.progress')
                    .css({'width': percentWidth});
            };

            var itens = $(__versaShared.options.wizardElement).find('.form-wizard li');
            itens.css('width', (100 / itens.length) + '%');

            $(__versaShared.options.wizardElement).find('.progress').css({
                'width': ((100 / itens.length) * (itens.length - 1 ) * 0.99) + '%',
                'margin': '0 ' + ((100 / itens.length) / 2) + '%'
            });

            $(__versaShared.options.wizardElement).bootstrapWizard({
                tabClass: '',
                onTabShow: function (tab, navigation, index) {
                    handleTabShow(tab, navigation, index, $(__versaShared.options.wizardElement));
                },
                onTabClick: function (objAbaAtiva, objNavegacao, indexAbaAtiva, indexAbaDestino) {
                    var numeroAbas = objNavegacao.find('li').length;
                    if (indexAbaDestino >= numeroAbas) {
                        indexAbaDestino = numeroAbas - 1;
                    }

                    if (indexAbaAtiva < indexAbaDestino) {
                        var keys = Object.keys(__versaShared.steps);

                        for (var i = indexAbaAtiva; i < indexAbaDestino; i++) {
                            if (__versaShared.isFunction(__versaShared.steps[keys[i]] || [], 'validate')) {
                                var err = __versaShared.steps[keys[i]].validate();

                                if (err) {
                                    return false;
                                }
                            }

                            $(__versaShared.options.wizardElement).bootstrapWizard('next');
                        }

                        return false;
                    }
                },
                onNext: function (objAbaAtiva) {
                    var indexAbaAtiva = objAbaAtiva.index();
                    var keys = Object.keys(__versaShared.steps);

                    if (__versaShared.isFunction(__versaShared.steps[keys[indexAbaAtiva]] || [], 'validate')) {
                        var err = __versaShared.steps[keys[indexAbaAtiva]].validate();

                        if (err) {
                            return false;
                        }
                    }
                }
            });
        };

        this.makeSelect2Field = function (settings) {
            settings = settings || [];
            settings = $.extend(true, {
                fieldId: '',
                optionsAttr: '',
                dataAttr: '',
                ajax: false,
                data: null,
                select2Extra: {},
                changeCallback: null
            }, settings);

            if (!settings.dataAttr) {
                settings.dataAttr = settings.optionsAttr;
            }

            return {
                settings: settings,
                setValue: function (option) {
                    option = option || null;
                    __versaShared.options.value[this.settings.optionsAttr] = option;
                    $(this.settings.fieldId).select2("val", option);
                },
                setValueBySearch: function (search) {
                    var data = __versaShared.options.data[this.settings.dataAttr] || null;

                    if (!data) {
                        return;
                    }

                    var selection = $.grep(data, function (el) {
                        if (el.id == search || el.text.toLowerCase() == search.toLowerCase()) {
                            return el;
                        }
                    });

                    if (!this.settings.select2Extra.multiple && !this.settings.select2Extra.tags) {
                        selection = selection[0] || null;
                    }

                    this.setValue(selection || null);
                },
                getValue: function () {
                    return __versaShared.options.value[this.settings.optionsAttr];
                },
                init: function () {
                    var select2Helper = this;
                    var select2Options = {
                        language: 'pt-BR',
                        allowClear: true,
                        initSelection: function (element, callback) {
                            callback(select2Helper.getValue());
                        }
                    };

                    if (this.settings.ajax) {
                        select2Options['ajax'] = {url: '', dataType: 'json', delay: 250, data: null, results: ''};
                        select2Options['ajax'] = $.extend(true, select2Options['ajax'], this.settings.ajax);
                        delete select2Options['data'];
                    } else if (this.settings.data) {
                        select2Options['data'] = this.settings.data;
                        delete select2Options['ajax'];
                    } else {
                        delete select2Options['ajax'];
                        delete select2Options['data'];
                    }

                    if (this.settings.select2Extra) {
                        select2Options = $.extend(true, select2Options, this.settings.select2Extra);
                    }

                    $(this.settings.fieldId).select2(select2Options);

                    if (this.settings.select2Extra.allowClear) {
                        $(this.settings.fieldId).on(
                            'change select2-selected select2-clearing select2-removed',
                            function () {
                                var $item = $(this);

                                if ($item.val() != '') {
                                    $item.parent().find('.select2-container').addClass('select2-allowclear');
                                } else {
                                    $item.parent().find('.select2-container').removeClass('select2-allowclear');
                                }
                            });
                    }

                    if (this.settings.changeCallback) {
                        $(this.settings.fieldId).on('change', this.settings.changeCallback);
                    }
                    $(this.settings.fieldId).select2("val", select2Helper.getValue());
                    $(this.settings.fieldId).trigger('change');
                    $(this.settings.fieldId).data('select2Util', this);
                }
            };
        };

        this.procuraValorArraySelect2 = function (needle, haystack) {
            if (!needle || !haystack) {
                return false;
            }

            return $.grep(haystack, function (el) {
                if (el.id == needle) {
                    return el;
                }
            });
        };

        this.getOption = function (option, container) {
            container = container || this.options;
            option = option || false;

            if (container[option]) {
                return container[option];
            }

            return false;
        };

        this.setOption = function (option, value, container) {
            container = container || this.options;
            option = option || false;
            value = value || false;
            container[option] = value;
        };

        this.getDatatable = function (datatableName) {
            return this.getOption(datatableName, this.options.datatable);
        };

        this.setDatatable = function (datatableName, datatable) {
            this.setOption(datatableName, datatable, this.options.datatable);
        };

        this.formatNumberUS = function (num, fixed) {
            if (isNaN(num) || num == null) {
                return '';
            }

            var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
            var newNum = num.toString().match(re)[0];
            return this.formatNumber(newNum, fixed);
        };

        this.formatNumber = function (number, dec, dsep, tsep) {
            if (isNaN(number) || number == null) {
                return '';
            }

            dec = dec || 2;
            dsep = dsep || '.';
            tsep = tsep || '';

            var newNumber = parseFloat(number).toFixed(~~dec);

            var parts = newNumber.split('.');
            var fnums = parts[0];
            var decimals = parts[1] ? (dsep || '.') + parts[1] : '';

            return fnums.replace(/(\d)(?=(?:\d{3})+$)/g, '$1' + tsep) + decimals;
        };

        this.formatDate = function (date, format) {
            date = date || null;
            format = format || "dd/mm/yy";

            if (date) {
                if (date.toString().indexOf('/') != -1) {
                    date = date.replace(/(\d{2})\/(\d{2})\/(\d{4})(.*)/, "$3/$2/$1$4");
                }

                date = new Date(date);

                if (isNaN(date)) {
                    date = null;
                }
            }

            if (date) {
                date = $.datepicker.formatDate(format, date);
            } else {
                date = '-';
            }

            return date;
        };

        this.formatDateTime = function (date, format) {
            date = date || null;
            format = format || 'dd/mm/yy hh:ii';

            if (!$['formatDateTime']) {
                format = format.replace('hh:ii', '').trim();
                return __versaShared.formatDate(date, format);
            }

            if (date) {
                if (date.toString().indexOf('/') != -1) {
                    date = date.replace(/(\d{2})\/(\d{2})\/(\d{4})(T|)(.*)/, "$3/$2/$1 $5").trim();
                }

                date = new Date(date);

                if (isNaN(date)) {
                    date = null;
                }
            }

            if (date) {
                date = $.formatDateTime(format, date);
            } else {
                date = '-';
            }

            return date;
        };

        this.createBtnGroup = function (btns, options) {
            btns = btns || [];
            options = options || [];
            var btnArr = [];
            var align = (options['align'] || 'center');
            for (var i in btns) {
                var size = (btns[i]['size'] || 'xs');
                var title = (btns[i]['title'] || '');
                var text = (btns[i]['text'] || '');
                var element = (btns[i]['element'] || 'button');
                var type = (btns[i]['type'] || 'button');
                var target = (btns[i]['target'] || '');
                var icon = (btns[i]['icon'] || '');
                var href = (btns[i]['href'] || '#');
                var id = (btns[i]['id'] || '');
                var dataToggle = (btns[i]['dataToggle'] || '#');
                var dataTarget = (btns[i]['dataTarget'] || '#');
                var cssClass = (btns[i]['class'] || '');
                cssClass = 'btn' + (size ? ' btn-' + size : '') + (cssClass ? ' ' + cssClass : '');
                cssClass = ' class="' + cssClass + '"';

                if (element == 'a') {
                    target = target ? ' target="' + target + '"' : '';
                    href = href ? ' href="' + href + '"' : '';
                    type = '';
                } else if (element == 'button') {
                    type = type ? ' type="' + type + '"' : '';
                    target = '';
                    href = '';
                }

                if (id) {
                    id = ' id="' + id + '"';
                }

                if (title) {
                    title = ' title="' + title + '"';
                }

                if (dataTarget) {
                    dataTarget = ' data-target="' + dataTarget + '"';
                }

                if (dataToggle) {
                    dataToggle = ' data-toggle="' + dataToggle + '"';
                }

                if (icon) {
                    icon = '<i class="fa ' + icon + ' fa-inverse"></i>';
                }


                btnArr.push(
                    '<' + element + type + href + target + cssClass + title + dataTarget + dataToggle + id + '>' + icon + text + '</' + element + '>'
                );
            }

            return '<div class="text-' + align + '"><div class="btn-group">' + btnArr.join('\n') + '</div></div>';
        };

        this.strToUnderscore = function (str) {
            return (str.charAt(0).toLowerCase() + str.slice(1) || str).toString();
        };

        this.strToCamelCase = function (str) {
            return str.replace(/(_\w)/g, function (m) {
                return m[1].toUpperCase();
            });
        };

        this.keysToUnderscore = function (o) {
            var build, key, destKey, value;

            if (o instanceof Array) {
                build = [];

                for (key in o) {
                    value = o[key];

                    if (typeof value === "object") {
                        value = __versaShared.keysToUnderscore(value);
                    }

                    build.push(value);
                }
            } else {
                build = {};

                for (key in o) {
                    if (o.hasOwnProperty(key)) {
                        destKey = __versaShared.strToUnderscore(key);
                        value = o[key];

                        if (value !== null && typeof value === "object") {
                            value = __versaShared.keysToUnderscore(value);
                        }

                        build[destKey] = value;
                    }
                }
            }

            return build;
        };

        this.keysToCamelCase = function (o) {
            var build, key, destKey, value;

            if (o instanceof Array) {
                build = [];

                for (key in o) {
                    value = o[key];

                    if (typeof value === "object") {
                        value = __versaShared.keysToCamelCase(value);
                    }

                    build.push(value);
                }
            } else {
                build = {};

                for (key in o) {
                    if (o.hasOwnProperty(key)) {
                        destKey = __versaShared.strToCamelCase(key);
                        value = o[key];

                        if (value !== null && typeof value === "object") {
                            value = __versaShared.keysToCamelCase(value);
                        }

                        build[destKey] = value;
                    }
                }
            }

            return build;
        };

        this.formatarMoeda = function (valor) {
            if (!valor && valor !== 0) {
                return '-';
            }

            valor = parseFloat(valor) || 0;
            valor = __versaShared.formatNumber(valor, 2, ',', '.');
            return valor;
        };

        this.formatarMoedaUS = function (valor) {
            if (!valor && valor !== 0) {
                return 0;
            }

            if (valor['indexOf']) {
                valor = valor.replace(/[^0-9,]/, '').replace(',', '.');
            }


            valor = parseFloat(valor) || 0;

            return valor;
        };

        this.iniciarElementoDatePicker = function (el, extraOptions) {
            extraOptions = extraOptions || {};
            var datetimeOptions = $.extend(
                [],
                {
                    format: 'd/m/Y',
                    prevText: '<i class="fa fa-chevron-left"></i>',
                    nextText: '<i class="fa fa-chevron-right"></i>'
                },
                extraOptions
            );
            el.datepicker(datetimeOptions);
            el.inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

            return el;
        };

        this.iniciarElementoDateTimePicker = function (el, extraOptions) {
            if ($['datetimepicker']) {
                extraOptions = extraOptions || {};
                var datetimeOptions = $.extend(
                    {},
                    {
                        format: 'd/m/Y H:i',
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>'
                    },
                    extraOptions
                );

                el.datetimepicker(datetimeOptions);
                el.inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999 99:99']});

                return el;
            } else {
                return __versaShared.iniciarElementoDatePicker(el, extraOptions);
            }

        };

        this.iniciarElementoTimePicker = function (el, extraOptions) {
            if ($['datetimepicker']) {
                extraOptions = extraOptions || {};
                var datetimeOptions = $.extend(
                    {},
                    {
                        datepicker: false,
                        format: 'H:i',
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>'
                    },
                    extraOptions
                );

                el.datetimepicker(datetimeOptions);

            }

            el.inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99:99']});

            return el;
        };

        this.inciarElementoInteiro = function (el, extraOptions) {
            el.inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});

            return el;
        };

        this.inciarElementoMoeda = function (el, extraOptions) {
            el.inputmask({
                showMaskOnHover: false, alias: 'currency', groupSeparator: "", radixPoint: ",",
                placeholder: "0", prefix: "", allowMinus: false
            });

            return el;
        };

        this.roundNumber = function (num, scale) {
            // Solução original https://stackoverflow.com/a/12830454
            num = parseFloat(num);

            var number = Math.round(num * Math.pow(10, scale)) / Math.pow(10, scale);

            if (num - number > 0) {
                return (number + Math.floor(2 * Math.round((num - number) * Math.pow(10, (scale + 1))) / 10) / Math.pow(10, scale));
            } else {
                return number;
            }
        };

        /*
         *   FUNÇÃO QUE RETORNA OS VALORES DE UM FORM-STATIC
         *   @param $ el
         *   @return json
         */
        this.getFormStaticValues = function (el) {
            var arr = [];

            el.find('.form-control-static').each(function (index, element) {
                var key = ($(element).attr('class')).replace(/.* /, '');
                var value = $(element).text();

                arr.push({
                    key: key,
                    value: value
                });
            });

            return arr;
        };


        this.validaCPF = function (cpf) {
            var numeros, digitos, soma, i, resultado, digitos_iguais;
            digitos_iguais = 1;
            if (cpf.length < 11)
                return false;
            for (i = 0; i < cpf.length - 1; i++)
                if (cpf.charAt(i) != cpf.charAt(i + 1)) {
                    digitos_iguais = 0;
                    break;
                }
            if (!digitos_iguais) {
                numeros = cpf.substring(0, 9);
                digitos = cpf.substring(9);
                soma = 0;
                for (i = 10; i > 1; i--)
                    soma += numeros.charAt(10 - i) * i;
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0))
                    return false;
                numeros = cpf.substring(0, 10);
                soma = 0;
                for (i = 11; i > 1; i--)
                    soma += numeros.charAt(11 - i) * i;
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1))
                    return false;
                return true;
            }
            else
                return false;
        };

        this.systemDialog = function(extraOptions, success, error){
            extraOptions = $.extend(
                true,
                {
                    acccpt:"Aceitar",
                    cancel:"Cancelar",
                    close:"Fechar",
                    confirm:"Confirmar",
                    decline:"Recusar",
                    deny:"Negar",
                    maximize:"Maximizar",
                    ok:"OK",
                    restore:"Restaurar",
                    title:"Atenção!"
                },
                extraOptions
            );

            alertify.defaults = {
                theme: {
                    ok: "btn btn-primary",
                    cancel: "btn btn-danger"
                },
                glossary: extraOptions
            };

            return alertify.confirm(extraOptions['message'] || "Deseja prosseguir?", success, error);
        };

        return this;
    };