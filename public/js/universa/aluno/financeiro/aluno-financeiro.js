(function ($, window, document) {
    'use strict';
    var AlunoFinanceiro = function () {
        VersaShared.call(this);
        var __alunoFinanceiro = this;

        this.defaults = {
            url: {
                search: '',
                boletoExibe: '',
                pagseguroCobranca: ''
            },
            value: {
                pagseguro: ''
            },
            datatables: null
        };

        this.setSteps = function () {
            var $dataTableAlunoFinanceiro = $('#dataTablesFinanceiro');
            var colNum = -1;

            __alunoFinanceiro.options.datatables = $dataTableAlunoFinanceiro.dataTable({
                processing: true,
                serverSide: true,
                searchable: true,
                ajax: {
                    url: __alunoFinanceiro.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        var btnBoleto = {
                            size: 'sm', element: 'a', target: '_blank',
                            class: 'btn-primary btn-boleto', icon: 'fa-download',
                            title: 'Download de boleto'
                        };

                        var btnPagseguro = {
                            size: 'sm', element: 'a', target: '_blank',
                            class: 'btn-primary btn-pagseguro', icon: 'fa-credit-card',
                            title: 'Link pagseguro'
                        };

                        for (var row in data) {
                            var btns = [];
                            var boletoId = data[row]['bol_id'] || false;
                            var tituloId = data[row]['titulo_id'] || false;

                            btnBoleto['href'] = __alunoFinanceiro.options.url.boletoExibe + '/' + boletoId;
                            btnPagseguro['href'] = __alunoFinanceiro.options.url.pagseguroCobranca + '/' + tituloId;

                            data[row]['btn_acoes'] = ' - ';

                            var dataPagamento =
                                __alunoFinanceiro.formatarData(data[row]['titulo_data_pagamento']) || ' - ';

                            var dataBaixa =
                                __alunoFinanceiro.formatarData(data[row]['titulo_data_baixa']) || ' - ';

                            data[row]['titulo_data_pagamento'] = dataBaixa || dataPagamento;

                            data[row]['titulo_data_vencimento'] =
                                __alunoFinanceiro.formatarData(data[row]['titulo_data_vencimento']);

                            data[row]['titulo_valor'] = __alunoFinanceiro.formatarMoeda(data[row]['titulo_valor']);
                            data[row]['titulo_valor_pago'] =
                                __alunoFinanceiro.formatarMoeda(data[row]['titulo_valor_pago']);

                            if (data[row]['titulo_estado'] != "Pago" && data[row]['titulo_data_baixa'] == null) {
                                if (boletoId && json.boletoAtivo) {
                                    if (!json.boletoRemessaAtivada || (json.boletoRemessaAtivada && parseInt(data[row]['esta_na_remessa']) > 0)) {
                                        btns.push(btnBoleto);
                                    }
                                }

                                if (json.pagseguro) {
                                    btns.push(btnPagseguro);
                                }

                                if (btns.length > 0) {
                                    data[row]['btn_acoes'] = __alunoFinanceiro.createBtnGroup(btns);
                                }
                            }
                        }

                        return data;
                    }
                },
                columns: [
                    {name: "titulo_id", targets: ++colNum, data: "titulo_id"},
                    {name: "titulo_descricao", targets: ++colNum, data: "titulo_descricao"},
                    {name: "titulo_valor", targets: ++colNum, data: "titulo_valor"},
                    {name: "titulo_data_vencimento", targets: ++colNum, data: "titulo_data_vencimento"},
                    {name: "titulo_data_pagamento", targets: ++colNum, data: "titulo_data_pagamento"},
                    {name: "titulo_valor_pago", targets: ++colNum, data: "titulo_valor_pago"},
                    {name: "titulo_id", targets: ++colNum, data: "btn_acoes", bSearchable: false}
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "createdCell": function (td, cellData, rowData) {
                            if (rowData['titulo_estado'] == 'Aberto') {
                                $(td).parent().addClass('text-danger');
                            }
                        }
                    }
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "_START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "<<",
                        sNext: ">>",
                        sLast: "Último"
                    }
                },
                order: [[3, 'desc']]
            });
        };

        this.formatarData = function (data) {
            if (!data) {
                return ' - '
            }

            data = data.split("-");

            return data[2].slice(0, 2) + '/' + data[1] + '/' + data[0];
        };

        this.run = function (opts) {
            __alunoFinanceiro.setDefaults(opts);
            __alunoFinanceiro.setSteps();
        };
    };

    $.alunoFinanceiro = function (params) {
        params = params || [];

        var obj = $(window).data("universa.aluno.financeiro.index");

        if (!obj) {
            obj = new AlunoFinanceiro();
            obj.run(params);
            $(window).data('universa.aluno.financeiro.index', obj);
        }

        return obj;
    };


    $.alunoFinanceiro({
        url: {
            search: window.pesquisaTitulos || '',
            boletoExibe: window.boletoExibe || '',
            pagseguroCobranca: window.pagseguroCobranca || ''
        },
        value: {
            pagseguro: window.pagseguro || false
        }
    });
})(window.jQuery, window, document);