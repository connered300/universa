(function ($, window, document) {
    'use strict';
    var AlunoEstagio = function () {
        VersaShared.call(this);
        var __AlunoEstagio = this;

        this.defaults = {
            url: {
                search: '',
            },
            value: {
            },
            datatables: null
        };

        this.setSteps = function () {
            var $dataTableAlunoEstagio = $('#tableEstagio');
            var colNum = -1;

            __AlunoEstagio.options.datatables = $dataTableAlunoEstagio.dataTable({
                processing: true,
                serverSide: true,
                searchable: true,
                ajax: {
                    url: __AlunoEstagio.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        if( json['totalHoras'] ){
                            $dataTableAlunoEstagio.find('#total_horas').html("Total: <span>"+json['totalHoras'] || 0+"</span> h");
                        }
                        return json.data || [];
                    }
                },
                columns: [
                    {name: "alunoatividade_data_lancamento", targets: ++colNum, data: "alunoatividade_data_lancamento"},
                    {name: "per_nome", targets: ++colNum, data: "per_nome"},
                    {name: "alunoatividade_descricao", targets: ++colNum, data: "alunoatividade_descricao"},
                    {name: "alunoatividade_horas", targets: ++colNum, data: "alunoatividade_horas", className: "text-right"},
                    {name: "alunoatividade_data_lancamento_us", targets: ++colNum, data: "alunoatividade_data_lancamento", visible:false, searchable:false},
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "_START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "<<",
                        sNext: ">>",
                        sLast: "Último"
                    }
                },
                order: [[4, 'desc'], [1, 'desc'], [2, 'asc'] ]
            });
        };


        this.run = function (opts) {
            __AlunoEstagio.setDefaults(opts);
            __AlunoEstagio.setSteps();
        };
    };

    $.alunoEstagio = function (params) {
        params = params || [];

        var obj = $(window).data("universa.aluno.estagio.index");

        if (!obj) {
            obj = new AlunoEstagio();
            obj.run(params);
            $(window).data('universa.aluno.estagio.index', obj);
        }

        return obj;
    };

    $.alunoEstagio({url: {search: window.pesquisaAtividadesEstagio || ''}});
})(window.jQuery, window, document);