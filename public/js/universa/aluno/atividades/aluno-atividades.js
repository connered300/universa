(function ($, window, document) {
    'use strict';
    var AlunoAtividades = function () {
        VersaShared.call(this);
        var __AlunoAtividades = this;

        this.defaults = {
            url: {
                search: '',
            },
            value: {
                possuiAcessoExclusao:null
            },
            datatables: null
        };

        this.setSteps = function () {
            var $dataTableAlunoAtividades = $('#tableAtividades');
            var colNum = -1;

            __AlunoAtividades.options.datatables = $dataTableAlunoAtividades.dataTable({
                processing: true,
                serverSide: true,
                searchable: true,
                ajax: {
                    url: __AlunoAtividades.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        if( json['totalHoras'] ){
                            $dataTableAlunoAtividades.find('#total_horas').html("Total: <span>"+json['totalHoras'] || 0+"</span> h");
                        }
                        return json.data || [];
                    }
                },
                columns: [
                    {name: "aluno_atividade_data_realizacao", targets: ++colNum, data: "aluno_atividade_data_realizacao"},
                    {name: "evento_nome", targets: ++colNum, data: "evento_nome"},
                    {name: "atividadeatividade_descricao", targets: ++colNum, data: "atividadeatividade_descricao"},
                    {name: "aluno_atividade_horas_validas", targets: ++colNum, data: "aluno_atividade_horas_validas", className: "text-right"},
                    {name: "aluno_atividade_data_realizacao_us", targets: ++colNum, data: "aluno_atividade_data_realizacao", visible:false, searchable:false},
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "_START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "<<",
                        sNext: ">>",
                        sLast: "Último"
                    }
                },
                order: [[3, 'desc'], [1, 'asc']]
            });
        };


        this.run = function (opts) {
            __AlunoAtividades.setDefaults(opts);
            __AlunoAtividades.setSteps();
        };
    };

    $.alunoAtividades = function (params) {
        params = params || [];

        var obj = $(window).data("universa.aluno.atividades.index");

        if (!obj) {
            obj = new AlunoAtividades();
            obj.run(params);
            $(window).data('universa.aluno.atividades.index', obj);
        }

        return obj;
    };

    $.alunoAtividades({url: {search: window.pesquisaAtividades || ''}});
})(window.jQuery, window, document);