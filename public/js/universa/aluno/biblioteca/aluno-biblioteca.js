(function ($, window, document) {
    'use strict';
    var AlunoBiblioteca = function () {
        VersaShared.call(this);
        var __AlunoBiblioteca = this;

        this.defaults = {
            url: {
                search: window.pesquisaBiblioteca || "",
                acao: window.acao || ""
            },
            value: {
                dataAtual: window.dataAtual || null,
                diasParaRenovacao: window.diasParaRenovacao || null,
                quantidadeDeRenovacoes: window.quantidadeDeRenovacoes || null,
                permiteAlunoRenovar: window.permiteAlunoRenovar || null
            },
            datatables: null
        };

        this.setSteps = function () {
            var $dataTableAlunoBiblioteca = $('#tableBiblioteca');
            var colNum = -1;

            __AlunoBiblioteca.options.datatables = $dataTableAlunoBiblioteca.dataTable({
                processing: true,
                serverSide: true,
                searchable: true,
                ajax: {
                    url: __AlunoBiblioteca.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        var btn = ' <a href="#" class="btn btn-success btn-xs btn-item-renovar" title="Renovar Empréstimo">' +
                            '<i class="fa fa-refresh "></i> </a>';

                        var dataAtual = new Date(((__AlunoBiblioteca.options.value.dataAtual).split('/').reverse().join('/') + " 00:00:00"));
                        var diasAntesDoVencimentoParaRenovar = __AlunoBiblioteca.options.value.diasParaRenovacao;
                        var quantidadeMaximaEmprestimos = parseInt(__AlunoBiblioteca.options.value.quantidadeDeRenovacoes || 0);

                        if (__AlunoBiblioteca.options.value.permiteAlunoRenovar) {
                            for (var row in data) {
                                var ultrapassouLimiteDeDias = false,
                                    dataLimiteUltrapassada = true;

                                if (parseInt(data[row]['quantidadeDeEmprestimosConcedidos'] || 0) > quantidadeMaximaEmprestimos) {
                                    ultrapassouLimiteDeDias = true;
                                    continue;
                                }


                                var dataDevolucao = (data[row]['emprestimo_devolucao_data_previsao']).split("/");
                                dataDevolucao = (dataDevolucao[2] + "-" + dataDevolucao[1] + "-" + dataDevolucao[0]) + " 00:00:00";

                                dataDevolucao = new Date(dataDevolucao);
                                dataDevolucao = new Date(dataDevolucao.getTime() - (parseInt(diasAntesDoVencimentoParaRenovar) * 24 * 60 * 60 * 1000));

                                if (dataAtual >= dataDevolucao && parseInt(data[row]['dias']) < 0) {
                                    dataLimiteUltrapassada = false;
                                }

                                if (!data[row]['emprestimo_devolucao_data_efetuada'] && !ultrapassouLimiteDeDias && !dataLimiteUltrapassada) {
                                    data[row]['acoes'] = btn;
                                }

                            }
                        }

                        return data || [];
                    }
                },
                columns: [
                    {name: "emprestimo_data", targets: ++colNum, data: "emprestimo_data"},
                    {name: "titulo_titulo", targets: ++colNum, data: "titulo_titulo"},
                    {name: "autor_referencia", targets: ++colNum, data: "autor_referencia"},
                    {name: "grupo_bibliografico_nome", targets: ++colNum, data: "grupo_bibliografico_nome"},
                    {name: "exemplar_codigo", targets: ++colNum, data: "exemplar_codigo"},
                    {
                        name: "emprestimo_devolucao_data_previsao",
                        targets: ++colNum,
                        data: "emprestimo_devolucao_data_previsao"
                    },
                    {
                        name: "emprestimo_devolucao_data_efetuada",
                        targets: ++colNum,
                        data: "emprestimo_devolucao_data_efetuada"
                    },
                    {name: "exemplar_codigo", targets: ++colNum, data: "acoes", searchable: false},
                    {
                        name: "emprestimo_data_us",
                        targets: ++colNum,
                        data: "emprestimo_data",
                        visible: false,
                        searchable: false
                    },
                    {
                        name: "emprestimo_devolucao_data_efetuada_us",
                        targets: ++colNum,
                        data: "emprestimo_devolucao_data_efetuada",
                        visible: false,
                        searchable: false
                    },
                    {
                        name: "emprestimo_devolucao_data_previsao_us",
                        targets: ++colNum,
                        data: "emprestimo_devolucao_data_previsao",
                        visible: false,
                        searchable: false
                    },
                    {
                        name: "emprestimo_data_us",
                        targets: ++colNum,
                        data: "emprestimo_data",
                        visible: false,
                        searchable: false
                    },
                    {
                        name: "emprestimo_devolucao_data_efetuada_us",
                        targets: ++colNum,
                        data: "emprestimo_devolucao_data_efetuada",
                        visible: false,
                        searchable: false
                    },
                    {
                        name: "emprestimo_devolucao_data_previsao_us",
                        targets: ++colNum,
                        data: "emprestimo_devolucao_data_previsao",
                        visible: false,
                        searchable: false
                    },
                    {name: "dias", targets: ++colNum, data: "dias", visible: false, searchable: false}

                ],
                "columnDefs": [
                    {
                        "targets": 0,
                        "createdCell": function (td, cellData, rowData) {
                            if (rowData['dias'] > 0 && !rowData['emprestimo_devolucao_data_efetuada_us']) {
                                $(td).parent().addClass('text-danger');
                            }
                        }
                    }
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "_START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "<<",
                        sNext: ">>",
                        sLast: "Último"
                    }
                },
                fnDrawCallback: function () {
                    $(".btn-item-renovar").on("click", function () {
                        var pai = $(this).closest('tr');
                        var arrEmprestimo = __AlunoBiblioteca.options.datatables.fnGetData(pai);

                        alertify.defaults.theme.ok = "btn btn-primary";
                        alertify.defaults.theme.cancel = "btn";

                        alertify.confirm('Deseja realmente renovar o emprestimo deste acervo?')
                            .set({
                                labels: {ok: "Sim", cancel: "Não"},
                                title: "Confirme",
                                onok: function () {
                                    __AlunoBiblioteca.addOverlay($('.page-section'),
                                        'Aguarde, efetuando a renovação!');

                                    $.ajax({
                                        url: __AlunoBiblioteca.options.url.acao,
                                        method: 'POST',
                                        dataType: 'json',
                                        data: {
                                            acao: 'renovar',
                                            emprestimoId: arrEmprestimo.emprestimo_id
                                        },
                                        success: function (data) {
                                            var deuErro = data['erro'] || false;

                                            if (deuErro) {
                                                __AlunoBiblioteca.showNotificacao({
                                                    timeout: 10000,
                                                    type: 'danger',
                                                    content: data['erroDescricao'] || 'Falha ao renovar empréstimo!'
                                                });
                                            } else {
                                                __AlunoBiblioteca.showNotificacao({
                                                    timeout: 10000, type: 'success',
                                                    content: 'Renovação efetuada com sucesso.'
                                                });

                                                __AlunoBiblioteca.options.datatables.api().ajax.reload(null, false);
                                            }

                                            __AlunoBiblioteca.removeOverlay($('.page-section'));
                                        },
                                        error: function (data) {
                                            __AlunoBiblioteca.showNotificacao({
                                                timeout: 10000,
                                                type: 'danger',
                                                content: 'Falha ao renovar empréstimo!'
                                            });
                                            __AlunoBiblioteca.removeOverlay($('.page-section'));
                                        }
                                    });
                                }
                            });

                    });

                },
                order: [[8, 'desc']]
            });
        };

        this.run = function (opts) {
            __AlunoBiblioteca.setDefaults(opts);
            __AlunoBiblioteca.setSteps();
        };
    };

    $.alunoBiblioteca = function (params) {
        params = params || [];

        var obj = $(window).data("universa.aluno.biblioteca.index");

        if (!obj) {
            obj = new AlunoBiblioteca();
            obj.run(params);
            $(window).data('universa.aluno.biblioteca.index', obj);
        }

        return obj;
    };

    $.alunoBiblioteca();
})(window.jQuery, window, document);