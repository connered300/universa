(function ($, window, document) {
    'use strict';
    var AcadgeralMotivoAlteracaoIndex = function () {
        VersaShared.call(this);
        var __acadgeralMotivoAlteracaoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                acadgeralMotivoAlteracao: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __acadgeralMotivoAlteracaoIndex.options.datatables.acadgeralMotivoAlteracao =
                $('#dataTableAcadgeralMotivoAlteracao').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __acadgeralMotivoAlteracaoIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary acadgeralMotivoAlteracao-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger acadgeralMotivoAlteracao-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __acadgeralMotivoAlteracaoIndex.removeOverlay($('#container-acadgeral-motivo-alteracao'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "motivo_id", targets: colNum++, data: "motivo_id"},
                        {name: "motivo_descricao", targets: colNum++, data: "motivo_descricao"},
                        {name: "motivo_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableAcadgeralMotivoAlteracao').on('click', '.acadgeralMotivoAlteracao-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __acadgeralMotivoAlteracaoIndex.options.datatables.acadgeralMotivoAlteracao.fnGetData($pai);

                if (!__acadgeralMotivoAlteracaoIndex.options.ajax) {
                    location.href = __acadgeralMotivoAlteracaoIndex.options.url.edit + '/' + data['motivo_id'];
                } else {
                    $.acadgeralMotivoAlteracaoAdd().pesquisaAcadgeralMotivoAlteracao(
                        data['motivo_id'],
                        function (data) {
                            __acadgeralMotivoAlteracaoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-acadgeral-motivo-alteracao-add').click(function (e) {
                if (__acadgeralMotivoAlteracaoIndex.options.ajax) {
                    __acadgeralMotivoAlteracaoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableAcadgeralMotivoAlteracao').on('click', '.acadgeralMotivoAlteracao-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __acadgeralMotivoAlteracaoIndex.options.datatables.acadgeralMotivoAlteracao.fnGetData($pai);
                var arrRemover = {
                    motivoId: data['motivo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover o motivo de alteração "' + data['motivo_descricao'] + '" ?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __acadgeralMotivoAlteracaoIndex.addOverlay(
                                $('#container-acadgeral-motivo-alteracao'),
                                'Aguarde, solicitando remoção do motivo de alteração...'
                            );
                            $.ajax({
                                url: __acadgeralMotivoAlteracaoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __acadgeralMotivoAlteracaoIndex.showNotificacaoDanger(
                                            "Não foi possível remover o motivo de alteracao:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __acadgeralMotivoAlteracaoIndex.removeOverlay($('#container-acadgeral-motivo-alteracao'));
                                    } else {
                                        __acadgeralMotivoAlteracaoIndex.reloadDataTableAcadgeralMotivoAlteracao();
                                        __acadgeralMotivoAlteracaoIndex.showNotificacaoSuccess(
                                            "Motivo de alteracao removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#acadgeral-motivo-alteracao-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['motivoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['motivoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#motivoSituacao").val(data['motivoSituacao'] || null).trigger('change');


            $('#acadgeral-motivo-alteracao-modal .modal-title').html(actionTitle + ' motivo de alteração');
            $('#acadgeral-motivo-alteracao-modal').modal();
        };

        this.reloadDataTableAcadgeralMotivoAlteracao = function () {
            this.getDataTableAcadgeralMotivoAlteracao().api().ajax.reload(null, false);
        };

        this.getDataTableAcadgeralMotivoAlteracao = function () {
            if (!__acadgeralMotivoAlteracaoIndex.options.datatables.acadgeralMotivoAlteracao) {
                if (!$.fn.dataTable.isDataTable('#dataTableAcadgeralMotivoAlteracao')) {
                    __acadgeralMotivoAlteracaoIndex.options.datatables.acadgeralMotivoAlteracao =
                        $('#dataTableAcadgeralMotivoAlteracao').DataTable();
                } else {
                    __acadgeralMotivoAlteracaoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __acadgeralMotivoAlteracaoIndex.options.datatables.acadgeralMotivoAlteracao;
        };

        this.run = function (opts) {
            __acadgeralMotivoAlteracaoIndex.setDefaults(opts);
            __acadgeralMotivoAlteracaoIndex.setSteps();
        };
    };

    $.acadgeralMotivoAlteracaoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-motivo-alteracao.index");

        if (!obj) {
            obj = new AcadgeralMotivoAlteracaoIndex();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-motivo-alteracao.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);