(function ($, window, document) {
    'use strict';

    var AcadgeralMotivoAlteracaoAdd = function () {
        VersaShared.call(this);
        var __acadgeralMotivoAlteracaoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#acadgeral-motivo-alteracao-wizard',
            formElement: '#acadgeral-motivo-alteracao-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#motivoSituacao").select2({language: 'pt-BR'});
        };

        this.setValidations = function () {
            __acadgeralMotivoAlteracaoAdd.options.validator.settings.rules = {
                motivoDescricao: {maxlength: 245, required: true},
                motivoSituacao: {required: true},
            };
            __acadgeralMotivoAlteracaoAdd.options.validator.settings.messages = {
                motivoDescricao: {
                    maxlength: 'Tamanho máximo: 245!', required: 'Campo obrigatório!'
                },
                motivoSituacao: {
                    required: 'Campo obrigatório!'
                }
            };

            $(__acadgeralMotivoAlteracaoAdd.options.formElement).submit(function (e) {
                var ok = $(__acadgeralMotivoAlteracaoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__acadgeralMotivoAlteracaoAdd.options.formElement);

                if (__acadgeralMotivoAlteracaoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __acadgeralMotivoAlteracaoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __acadgeralMotivoAlteracaoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__acadgeralMotivoAlteracaoAdd.options.listagem) {
                                    $.acadgeralMotivoAlteracaoIndex().reloadDataTableAcadgeralMotivoAlteracao();
                                    __acadgeralMotivoAlteracaoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#acadgeral-motivo-alteracao-modal').modal('hide');
                                }

                                __acadgeralMotivoAlteracaoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAcadgeralMotivoAlteracao = function (motivo_id, callback) {
            var $form = $(__acadgeralMotivoAlteracaoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __acadgeralMotivoAlteracaoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + motivo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __acadgeralMotivoAlteracaoAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __acadgeralMotivoAlteracaoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __acadgeralMotivoAlteracaoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __acadgeralMotivoAlteracaoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.acadgeralMotivoAlteracaoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-motivo-alteracao.add");

        if (!obj) {
            obj = new AcadgeralMotivoAlteracaoAdd();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-motivo-alteracao.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);