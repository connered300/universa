(function ($, window, document) {
    'use strict';

    var AcadgeralAreaConhecimentoAdd = function () {
        VersaShared.call(this);
        var __acadgeralAreaConhecimentoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {acadgeralAreaConhecimento: ''},
            data: {},
            value: {areaPai: null},
            datatables: {},
            wizardElement: '#acadgeral-area-conhecimento-wizard',
            formElement: '#acadgeral-area-conhecimento-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#areaPai").select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __acadgeralAreaConhecimentoAdd.options.url.acadgeralAreaConhecimento,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.area_descricao, id: el.area_id};
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__acadgeralAreaConhecimentoAdd.getAreaPai()) {
                $('#areaPai').select2("data", __acadgeralAreaConhecimentoAdd.getAreaPai());
            }
        };

        this.setAreaPai = function (areaPai) {
            this.options.value.areaPai = areaPai || null;
        };

        this.getAreaPai = function () {
            return this.options.value.areaPai || null;
        };
        this.setValidations = function () {
            __acadgeralAreaConhecimentoAdd.options.validator.settings.rules =
            {
                areaDescricao: {maxlength: 45, required: true},
                areaPai: {maxlength: 10, number: true},
            };
            __acadgeralAreaConhecimentoAdd.options.validator.settings.messages = {
                areaDescricao: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                areaPai: {maxlength: 'Tamanho máximo: 10!', number: 'Número inválido!'},
            };

            $(__acadgeralAreaConhecimentoAdd.options.formElement).submit(function (e) {
                var ok = $(__acadgeralAreaConhecimentoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__acadgeralAreaConhecimentoAdd.options.formElement);

                if (__acadgeralAreaConhecimentoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __acadgeralAreaConhecimentoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __acadgeralAreaConhecimentoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__acadgeralAreaConhecimentoAdd.options.listagem) {
                                    $.acadgeralAreaConhecimentoIndex().reloadDataTableAcadgeralAreaConhecimento();
                                    __acadgeralAreaConhecimentoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#acadgeral-area-conhecimento-modal').modal('hide');
                                }

                                __acadgeralAreaConhecimentoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAcadgeralAreaConhecimento = function (area_id, callback) {
            var $form = $(__acadgeralAreaConhecimentoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __acadgeralAreaConhecimentoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + area_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __acadgeralAreaConhecimentoAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __acadgeralAreaConhecimentoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __acadgeralAreaConhecimentoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __acadgeralAreaConhecimentoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.acadgeralAreaConhecimentoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-area-conhecimento.add");

        if (!obj) {
            obj = new AcadgeralAreaConhecimentoAdd();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-area-conhecimento.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);