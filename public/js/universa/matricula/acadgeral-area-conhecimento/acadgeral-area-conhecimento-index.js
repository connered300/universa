(function ($, window, document) {
    'use strict';
    var AcadgeralAreaConhecimentoIndex = function () {
        VersaShared.call(this);
        var __acadgeralAreaConhecimentoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                acadgeralAreaConhecimento: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __acadgeralAreaConhecimentoIndex.options.datatables.acadgeralAreaConhecimento =
                $('#dataTableAcadgeralAreaConhecimento').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __acadgeralAreaConhecimentoIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary acadgeralAreaConhecimento-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger acadgeralAreaConhecimento-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __acadgeralAreaConhecimentoIndex.removeOverlay($('#container-acadgeral-area-conhecimento'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "area_id", targets: colNum++, data: "area_id"},
                        {name: "area_descricao", targets: colNum++, data: "area_descricao"},
                        {name: "area_pai_descricao", targets: colNum++, data: "area_pai_descricao"},
                        {name: "area_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                           "t" +
                           "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableAcadgeralAreaConhecimento').on('click', '.acadgeralAreaConhecimento-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __acadgeralAreaConhecimentoIndex.options.datatables.acadgeralAreaConhecimento.fnGetData($pai);

                if (!__acadgeralAreaConhecimentoIndex.options.ajax) {
                    location.href = __acadgeralAreaConhecimentoIndex.options.url.edit + '/' + data['area_id'];
                } else {
                    $.acadgeralAreaConhecimentoAdd().pesquisaAcadgeralAreaConhecimento(
                        data['area_id'],
                        function (data) { __acadgeralAreaConhecimentoIndex.showModal(data); }
                    );
                }
            });


            $('#btn-acadgeral-area-conhecimento-add').click(function (e) {
                if (__acadgeralAreaConhecimentoIndex.options.ajax) {
                    __acadgeralAreaConhecimentoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableAcadgeralAreaConhecimento').on('click', '.acadgeralAreaConhecimento-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __acadgeralAreaConhecimentoIndex.options.datatables.acadgeralAreaConhecimento.fnGetData($pai);
                var arrRemover = {
                    areaId: data['area_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de área de conhecimento "' + data['area_descricao'] +
                             '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __acadgeralAreaConhecimentoIndex.addOverlay(
                                $('#container-acadgeral-area-conhecimento'),
                                'Aguarde, solicitando remoção de registro de área de conhecimento...'
                            );
                            $.ajax({
                                url: __acadgeralAreaConhecimentoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __acadgeralAreaConhecimentoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de área de conhecimento:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __acadgeralAreaConhecimentoIndex.removeOverlay($('#container-acadgeral-area-conhecimento'));
                                    } else {
                                        __acadgeralAreaConhecimentoIndex.reloadDataTableAcadgeralAreaConhecimento();
                                        __acadgeralAreaConhecimentoIndex.showNotificacaoSuccess(
                                            "Registro de área de conhecimento removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#acadgeral-area-conhecimento-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['areaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['areaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#areaPai").select2('data', data['areaPai']).trigger("change");

            $('#acadgeral-area-conhecimento-modal .modal-title').html(actionTitle + ' área de conhecimento');
            $('#acadgeral-area-conhecimento-modal').modal();
        };

        this.reloadDataTableAcadgeralAreaConhecimento = function () {
            this.getDataTableAcadgeralAreaConhecimento().api().ajax.reload(null, false);
        };

        this.getDataTableAcadgeralAreaConhecimento = function () {
            if (!__acadgeralAreaConhecimentoIndex.options.datatables.acadgeralAreaConhecimento) {
                if (!$.fn.dataTable.isDataTable('#dataTableAcadgeralAreaConhecimento')) {
                    __acadgeralAreaConhecimentoIndex.options.datatables.acadgeralAreaConhecimento =
                        $('#dataTableAcadgeralAreaConhecimento').DataTable();
                } else {
                    __acadgeralAreaConhecimentoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __acadgeralAreaConhecimentoIndex.options.datatables.acadgeralAreaConhecimento;
        };

        this.run = function (opts) {
            __acadgeralAreaConhecimentoIndex.setDefaults(opts);
            __acadgeralAreaConhecimentoIndex.setSteps();
        };
    };

    $.acadgeralAreaConhecimentoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-area-conhecimento.index");

        if (!obj) {
            obj = new AcadgeralAreaConhecimentoIndex();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-area-conhecimento.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);