(function ($, window, document) {
    'use strict';
    var AcadperiodoTurmaIndex = function () {
        VersaShared.call(this);
        var __acadperiodoTurmaIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                acadperiodoTurma: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __acadperiodoTurmaIndex.options.datatables.acadperiodoTurma = $('#dataTableAcadperiodoTurma').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __acadperiodoTurmaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        for (var row in data) {
                            var url = __acadperiodoTurmaIndex.options.url.edit + '/' + data[row]['turma_id'],
                                turmaId = data[row]['turma_id'];

                            data[row]['checkbox'] = '<input type="radio" name="seletorTurmas" value="' + turmaId + '">';
                            data[row]['turma_id'] = '<a href="' + url + '">' + turmaId + '</a>';
                        }

                        return data;
                    }
                },
                columnDefs: [
                    {name: "turma_id", targets: colNum++, data: "checkbox"},
                    {name: "turma_id", targets: colNum++, data: "turma_id"},
                    {name: "turma_nome", targets: colNum++, data: "turma_nome"},
                    {name: "per_nome", targets: colNum++, data: "per_nome"},
                    {name: "turma_turno", targets: colNum++, data: "turma_turno"},
                    {name: "turma_serie", targets: colNum++, data: "turma_serie"},
                    {name: "turma_capacidade", targets: colNum++, data: "turma_capacidade"},
                    {name: "turma_numero_alunos", targets: colNum++, data: "turma_numero_alunos"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[1, 'desc']]
            });
        };

        $("#btn-remove").on('click', function (e) {
            var turmaId = '';

            $('[name="seletorTurmas"]').each(function (e, data) {
                if (data.checked) {
                    turmaId = data.value;
                }
            });

            if (!turmaId) {
                __acadperiodoTurmaIndex.showNotificacaoInfo(
                    "Selecione pelo menos uma turma para continuar!"
                );

                return false;
            }

            $.SmartMessageBox({
                title: "Confirme a operação:",
                content: 'Deseja realmente remover registro de turma "' + turmaId + '"?',
                buttons: "[Não][Sim]"
            }, function (ButtonPress) {
                if (ButtonPress == "Sim") {
                    setTimeout(function () {
                        __acadperiodoTurmaIndex.addOverlay(
                            $('#container-acadperiodo-turma'),
                            'Aguarde, solicitando remoção de registro de turma...'
                        );

                        $.ajax({
                            url: __acadperiodoTurmaIndex.options.url.remove,
                            type: 'POST',
                            dataType: 'json',
                            data: {id: turmaId, is_json: true},
                            success: function (data) {
                                __acadperiodoTurmaIndex.removeOverlay(
                                    $('#container-acadperiodo-turma')
                                );

                                if (data.erro) {
                                    __acadperiodoTurmaIndex.showNotificacaoDanger(
                                        data.erro['erro']['msg']
                                    );
                                } else {
                                    __acadperiodoTurmaIndex.showNotificacaoSuccess(
                                        "Registro removido com sucesso!"
                                    )
                                }

                                __acadperiodoTurmaIndex.reloadDatatableAcadperiodoTurma();
                            }
                        });
                    }, 400);
                }
            });

        });

        this.reloadDatatableAcadperiodoTurma = function () {
            this.getDatatableAcadperiodoTurma().api().ajax.reload(null, false);
        };

        this.getDatatableAcadperiodoTurma = function () {
            if (!__acadperiodoTurmaIndex.options.datatables.acadperiodoTurma) {
                if (!$.fn.dataTable.isDataTable('#dataTableAcadperiodoTurma')) {
                    __acadperiodoTurmaIndex.options.datatables.acadperiodoTurma = $('#dataTableAcadperiodoTurma').DataTable();
                } else {
                    __acadperiodoTurmaIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __acadperiodoTurmaIndex.options.datatables.acadperiodoTurma;
        };

        this.run = function (opts) {
            __acadperiodoTurmaIndex.setDefaults(opts);
            __acadperiodoTurmaIndex.setSteps();
        };
    };

    $.acadperiodoTurmaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadperiodo-turma.index");

        if (!obj) {
            obj = new AcadperiodoTurmaIndex();
            obj.run(params);
            $(window).data('universa.matricula.acadperiodo-turma.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);