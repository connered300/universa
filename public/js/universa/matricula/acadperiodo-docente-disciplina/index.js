var docente = (function ($, window, document) {
    "use strict";

    var tableDocencias;
    var docentes;

    var init = function () {
        tableDocencias = tableDocenciasInit();

        $.smallBox({
            icon: 'fa fa-info-circle',
            title: 'Informativo',
            color: '#57889c',
            content: 'Selecione um Período Letivo!',
            timeout:5000
        });

        $("#turma").select2({
            language: 'pt-BR',
            ajax: {
                url: '/matricula/acadperiodo-turma/search-for-json',
                dataType: 'json',
                type: 'POST',
                data: function (query) {
                    return {
                        query: query,
                        perId: $("#periodoLetivo").val()
                    };
                },
                results: function (data) {
                    var transformed = $.map(data, function (el) {
                        el.text = el.turma_nome + (el.turma_turno ? (' / ' + el.turma_turno) : '');
                        el.id = el.turma_id;

                        return el;
                    });
                    return {results: transformed};
                }
            }
        }).change(buscaDisciplinasDocentesPorTurma);


        $(document).on("click", "#listDocencia tbody tr", function () {
            var position = tableDocencias.fnGetPosition(this);
            var data = tableDocencias.fnGetData(position);

            data['text'] = data.turma;
            data['id'] = data.turma_id;

            $("#docenciaModal").modal();
            $("#turma").select2("data", data);

            buscaDisciplinasDocentesPorTurma();
        });

        // para o correto funcionamento do select no modal
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
            var that = this;
            $(document).on('focusin.modal', function (e) {
                if ($(e.target).hasClass('select2-input')) {
                    return true;
                }

                if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
                    that.$element.focus();
                }
            });
        };

        docentes = jQuery.parseJSON($("input[name='docentesJson']").val());

        $("form").submit(function () {
            if ($("#turma").select2("val") == "") {
                alert("Favor selecionar uma turma!");
                return false;
            }

            var i = $(".select-primary option[value='']:selected").length;

            if (i > 0) {
                var result = confirm("Deseja salvar turma deixando " + i + " docências sem vincular professores?");
                if (result == false) {
                    return false;
                }
            }

            $('#ajaxLoad').css({display: "block"});
            $("button[type='submit']", this).attr("disabled", true);
            return true;
        });

    };

    var tableDocenciasInit = function () {
        var dt = $("#listDocencia").dataTable({
            processing: true,
            serverSide: true,
            searchDelay: 350,
            type: "POST",
            ajax: {
                url: "/matricula/acadperiodo-docente-disciplina/listagem-docencias",
                type: "POST",
                data: function (d) {
                    d.filter = d.filter || {};
                    var periodo = $("#periodoLetivo").val();
                    d['somenteComPeriodo'] = true;
                    if (periodo) {
                        d.filter['periodoId'] = periodo;
                    }
                    return d;
                },
                async: true,
                dataSrc: function (json) {
                    return json.data;
                }
            },
            columnDefs: [
                {name: "turma", targets: 0, data: "turma"},
                {name: "disciplinas", targets: 1, data: "disciplinas"},
                {name: "professores", targets: 2, data: "professores"}

            ],
            "sDom": "<'dt-toolbar'<'col-xs-6 dt-seletor-periodo'><'col-xs-6'fr>>" +
            "t" +
            "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
            oLanguage: {
                sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                sLengthMenu: "Mostrar _MENU_ registros por página",
                sZeroRecords: "Nenhum registro encontrado",
                sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros)",
                oPaginate: {
                    sFirst: "Início",
                    sPrevious: "Anterior",
                    sNext: "Próximo",
                    sLast: "Último"
                }
            }
        });

        $('.dt-seletor-periodo').append(
            '<input type="text" name="periodoLetivo" id="periodoLetivo" class="form-control" placeholder="Selecione um Período Letivo" style="width: 100%">'
        );

        $("#periodoLetivo").select2({
            language: 'pt-BR',
            ajax: {
                url: '/matricula/acadperiodo-letivo/search-for-json',
                dataType: 'json',
                type: 'POST',
                data: function (query) {
                    return {
                        query: query
                    };
                },
                results: function (data) {
                    var transformed = $.map(data, function (el) {
                        el.text = 'Nome: ' + el.per_nome + ' / Ano: ' + el.per_ano + ' / Data Finalização: ' + (el.perDataFormatada ? el.perDataFormatada : 'Não finalizou ainda!');
                        el.id = el.per_id;

                        return el;
                    });
                    return {results: transformed};
                }
            }
        });

        $("#periodoLetivo").on("change", function () {
            $('#listDocencia').DataTable().ajax.reload();
        });

        return dt;
    };

    var buscaDisciplinasDocentesPorTurma = function () {
        $("button[type='submit']").attr("disabled", true);

        $.ajax({
            async: true,
            type: 'POST',
            url: "/matricula/acadperiodo-turma/buscas-docentes-disciplinas-turma-por-turma",
            data: {
                turmaId: $("#turma").val()
            },
            success: function (data) {
                $("#displinas-docentes").children().remove();
                if (data.disciplinas != undefined) {
                    var html = "";

                    //var docentes = $("#docentes").html();

                    for (var i = 0; i < data.disciplinas.length; i++) {
                        html = html +
                            "<section class='col-lg-12 col-sm-12 col-xs-12'>" +
                            "<div class='col-lg-6 col-sm-6 col-xs-6'>" +
                            "<div class='form-group'>" +
                            "<input type='text' disabled class='form-control' value='" + data.disciplinas[i].discNome + "'>" +
                            "<input type='hidden' name='disciplinas[]' value='" + data.disciplinas[i].disc + "'>" +
                            "</div>" +
                            "</div>" +
                            "<div class='select-primary col-lg-6 col-sm-6 col-xs-6'>" +
                            "<input type='hidden' name='docenteAtual[]' value='" + data.disciplinas[i].docenteId + "'>" +
                            "<select  name='docenteNovo[]' class='select2 select2-offscreen' tabindex='-1' >" +
                            "<optgroup label='Docentes' >" +
                            "<option value=''  >Selecione um docente</option>";
                        for (var j = 0; j < docentes.length; j++) {
                            if (docentes[j].docenteId == data.disciplinas[i].docenteId) {
                                html = html + "<option value='" + docentes[j].docenteId + "' selected>" + docentes[j].pesNome + "</option>";
                            } else {
                                html = html + "<option value='" + docentes[j].docenteId + "' >" + docentes[j].pesNome + "</option>";

                            }

                        }
                        html = html +
                            "</optgroup>" +
                            "</select>" +
                            "</div>" +
                            "</section>"
                        ;
                    }

                    $("#displinas-docentes").append(html);
                    $("#displinas-docentes  select").select2();
                    //$(".select-primary:visible").each(function(){
                    //    $(this).children().eq(0).remove();
                    //});
                    $("#displinas-docentes .select2-container").css('width', '100%');

                    //$("#displinas-docentes").children().last().find("select[name='docentes]").select2('val', data.turma_id);
                    $("button[type='submit']").attr("disabled", false);
                }
            },
            beforeSend: function () {
                $('#ajaxLoad').css({display: "block"});
            },
            complete: function () {
                $('#ajaxLoad').css({display: "none"});
            },
            error: function (a) {
                alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
            }

        });

    };

    var exibeModal = function () {
        $("#turma").select2('val', '');
        $("#displinas-docentes").children().remove();
        $("#docenciaModal").modal();
    };

    var validateForm = function () {
        if ($("#turma").select2('val') == '') {
            alert("Favor selecionar uma turma");
        }
    };


    return {
        init: init,
        tableDocenciasInit: tableDocenciasInit,
        exibeModal: exibeModal,
        buscaDisciplinasDocentesPorTurma: buscaDisciplinasDocentesPorTurma

    };

})(jQuery, window, document);

$(function () {
    docente.init();
});