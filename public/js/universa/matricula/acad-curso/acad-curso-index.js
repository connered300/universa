(function ($, window, document) {
    'use strict';

    var AcadCursoIndex = function () {
        VersaShared.call(this);

        var __AcadCursoIndex = this;

        this.defaults = {
            steps: [],
            validations: [],
            url: {
                search: '',
                edit: '',
                remove: '',
                area: '',
                campus: '',
                curso: ''
            },
            data: {
                modalidade: {},
                nivel: {},
                turno: {}
            },
            value: {
                cursoSituacao: null
            },
            dataTableAcadCurso: null
        };

        this.iniciarFiltros = function () {
            $('#campus').select2({
                ajax: {
                    url: __AcadCursoIndex.options.url.campus,
                    dataType: 'json',
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.camp_nome, id: el.camp_id};
                        });

                        return {results: transformed};
                    }
                },
                multiple: true
            });

            $('#areaPesquisa').select2({
                ajax: {
                    url: __AcadCursoIndex.options.url.area,
                    dataType: 'json',
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var resultado = $.map(data, function (elements) {
                            elements.text = elements.area_descricao;
                            elements.id = elements.area_id;

                            return elements;
                        });

                        return {results: resultado};
                    }
                },
                multiple: true
            });


            $('#turnoPesquisa').select2({
                data: __AcadCursoIndex.options.data.turno,
                language: 'pt-BR',
                multiple: true
            });

            $('#modalidadePesquisa').select2({
                data: __AcadCursoIndex.options.data.modalidade,
                language: 'pt-BR',
                multiple: true
            });

            $('#nivelPesquisa').select2({
                data: __AcadCursoIndex.options.data.nivel,
                language: 'pt-BR',
                multiple: true
            });

            $("#cursosFiltroExecutar").click(function () {
                __AcadCursoIndex.atualizarListagem();
            });

            $("#cursosFiltroLimpar").click(function () {
                $('#campus,#areaPesquisa,#turnoPesquisa,#modalidadePesquisa,#nivelPesquisa')
                    .select2('val', '')
                    .trigger('change');
            });

            $('#campus,#areaPesquisa,#turnoPesquisa,#modalidadePesquisa,#nivelPesquisa').on(
                'change select2-selected select2-clearing select2-removed',
                function () {
                    var $item = $(this);

                    if ($item.val() != '') {
                        $item.parent().find('.select2-container').addClass('select2-allowclear');
                    } else {
                        $item.parent().find('.select2-container').removeClass('select2-allowclear');
                    }
                }
            );
            $(".panel-title a").click(function () {
                $(".panel-title .fa").toggleClass('fa-chevron-right').toggleClass('fa-chevron-down');
            });
        };


        __AcadCursoIndex.atualizarListagem = function () {
            __AcadCursoIndex.dataTableAcadCurso.api().ajax.reload(null, false);
        };

        this.setSteps = function () {
            var colNum = -1;

            __AcadCursoIndex.dataTableAcadCurso = $('#dataTableAcadCurso').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __AcadCursoIndex.options.url.search,
                    type: "POST",
                    data: function (d) {
                        d.filter = d.filter || {};
                        d.index = true;

                        var campus = $('#campus').val();
                        var area = $('#areaPesquisa').val();
                        var modal = $('#modalidadePesquisa').val();
                        var nivel = $('#nivelPesquisa').val();
                        var turno = $('#turnoPesquisa').val();

                        if (campus) {
                            d.filter['campus'] = campus;
                        }
                        if (area) {
                            d.filter['areaPesquisa'] = area;
                        }
                        if (modal) {
                            d.filter['modalidadePesquisa'] = modal;
                        }
                        if (nivel) {
                            d.filter['nivelPesquisa'] = nivel;
                        }
                        if (turno) {
                            d.filter['turnoPesquisa'] = turno;
                        }

                        return d;
                    },
                    dataSrc: function (json) {
                        var data = json.data;
                        var columns = [
                            'curso_nome', 'curso_sigla', 'pes_nome', 'mod_nome', 'nivel_nome', 'curso_unidade_medida',
                            'curso_carga_horaria', 'curso_numero_inep', 'curso_numero_ocde', 'curso_numero_mec', 'curso_situacao',
                        ];

                        for (var row in data) {
                            var urlEdit = __AcadCursoIndex.options.url.edit + '/' + data[row]['curso_id'];

                            var btns = [];
                            btns.push({class: 'btn-info acadCurso-edit', icon: 'fa-pencil'});

                            for (var colPos in columns) {
                                var col = columns[colPos];
                                data[row][col] = data[row][col] || '';
                                data[row]['acao'] = __AcadCursoIndex.createBtnGroup(btns);
                            }
                        }

                        __AcadCursoIndex.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "curso_id", targets: ++colNum, data: "curso_id"},
                    {name: "curso_nome", targets: ++colNum, data: "curso_nome"},
                    {name: "curso_sigla", targets: ++colNum, data: "curso_sigla"},
                    {name: "pes_nome", targets: ++colNum, data: "pes_nome"},
                    {name: "mod_nome", targets: ++colNum, data: "mod_nome"},
                    {name: "nivel_nome", targets: ++colNum, data: "nivel_nome"},
                    {name: "curso_unidade_medida", targets: ++colNum, data: "curso_unidade_medida"},
                    {name: "curso_carga_horaria", targets: ++colNum, data: "curso_carga_horaria"},
                    {name: "curso_numero_inep", targets: ++colNum, data: "curso_numero_inep"},
                    {name: "curso_numero_ocde", targets: ++colNum, data: "curso_numero_ocde"},
                    {name: "curso_numero_mec", targets: ++colNum, data: "curso_numero_mec"},
                    {name: "curso_situacao", targets: ++colNum, data: "curso_situacao"},
                    {name: "curso_id", targets: ++colNum, data: "acao", orderable: false, searchable: false},
                    {name: "curso_id", targets: ++colNum, data: "curso_id", visible: false, searchable: true}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'desc']]
            });

            $('#dataTableAcadCurso').on('click', '.acadCurso-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __AcadCursoIndex.dataTableAcadCurso.fnGetData($pai);

                location.href = __AcadCursoIndex.options.url.edit + '/' + data['curso_id'];
            });

            $('#dataTableAcadCurso').on('click', '.acadCurso-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __AcadCursoIndex.dataTableAcadCurso.fnGetData($pai);
                var arrRemover = {cursoId: data['curso_id']};

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de curso "' + data['curso_prazo_integralizacao'] +
                    '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        __AcadCursoIndex.addOverlay(
                            $('.widget-body'),
                            'Aguarde, solicitando remoção de registro de curso...');
                        $.ajax({
                            url: __AcadCursoIndex.options.url.remove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível remover registro de curso:",
                                        content: "<i>" + data['erroDescricao'] + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });

                                    __AcadCursoIndex.removeOverlay($('.widget-body'));
                                } else {
                                    __AcadCursoIndex.dataTableAcadCurso.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Registro de curso removido!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });
        };

        __AcadCursoIndex.run = function (opts) {
            __AcadCursoIndex.setDefaults(opts);
            __AcadCursoIndex.iniciarFiltros();
            __AcadCursoIndex.setSteps();
        };
    };

    $.acadCursoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acad-curso.index");

        if (!obj) {
            obj = new AcadCursoIndex();
            obj.run(params);
            $(window).data('universa.acad-curso.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);