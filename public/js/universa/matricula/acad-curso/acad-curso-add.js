(function ($, window, document) {
    'use strict';
    var AcadCursoAdd = function () {
        VersaShared.call(this);

        var __AcadCursoAdd = this;

        this.defaults = {
            url: {
                gerenciadorDeArquivosThumb: '',
                gerenciadorDeArquivosDownload: '',
                pessoaFisica: '',
                buscaCEP: '',
                matrizCurricular: '',
                area: '',
                acadCurso: ''
            },
            data: {
                campus: [],
                periodoLetivo: [],
                cursoSituacao: [],
                cursoUnidadeMedida: [],
                metodo: [],
                metodoFinal: [],
                notaFracionada: [],
                situacaoDeferimento: []
            },
            value: {
                pes: null,
                campus: null,
                arqCarteirinha: null,
                configuracoes: null,
                cursoSituacao: null
            },
            datatables: {
                configuracoes: null
            },
            wizardElement: '#acad-curso-wizard',
            formElement: '#acad-curso-form',
            validator: null
        };

        this.steps = {};

        this.steps.dadosBasicos = {
            init: function () {
                $("#cursoNome").blur(function () {
                    $(this).removeClass('ui-autocomplete-loading');
                })
                    .autocomplete({
                        source: function (request, response) {
                            var $element = $(this.element);
                            var previous_request = $element.data("jqXHR");

                            if (previous_request) {
                                previous_request.abort();
                            }

                            $element.data("jqXHR", $.ajax({
                                url: __AcadCursoAdd.options.url.acadCurso,
                                data: {
                                    query: $("#cursoNome").val()
                                },
                                type: 'POST',
                                dataType: "json",
                                success: function (data) {
                                    var transformed = $.map(data || [], function (el) {
                                        el.label = el.curso_nome;

                                        return el;
                                    });

                                    response(transformed);
                                }
                            }));
                        },
                        minLength: 2,
                        focus: function (event, ui) {
                            var $element = $(this);
                            $element.val(ui.item.curso_nome);

                            return false;
                        },
                        select: function (event, ui) {
                            var $element = $(this);
                            $element.val(ui.item.curso_nome);
                            return false;
                        }
                    });


                $("#mod, #nivel").select2({language: 'pt-BR'});
                var $area = $("#area");
                $area.select2({
                    language: 'pt-BR',
                    ajax: {
                        url: __AcadCursoAdd.options.url.area,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.area_descricao, id: el.area_id};
                            });

                            return {results: transformed};
                        }
                    }
                });

                $area.select2("data", __AcadCursoAdd.getArea());

                $("#cursoPossuiPeriodoLetivo").select2({language: 'pt-BR'});

                $("#cursoTurno")
                    .select2({language: 'pt-BR'})
                    .change(
                    function () {
                        var turnosSelecionados = $(this).val() || '';
                        turnosSelecionados =
                            $.isArray(turnosSelecionados) ? turnosSelecionados : [turnosSelecionados];
                        var arrTurnos = ['Noturno', 'Vespertino', 'Matutino'];

                        for (var t in arrTurnos) {
                            var turno = arrTurnos[t];

                            if ($.inArray(turno, turnosSelecionados) == -1) {
                                $('#cursoVagas' + turno).val(0).prop('readonly', true);
                            } else {
                                $('#cursoVagas' + turno).prop('readonly', false);
                            }
                        }
                    }
                ).trigger('change');

                $(
                    "#cursoCargaHorariaPratica, #cursoCargaHorariaTeorica, #cursoCargaHoraria, #cursoNumeroInep, " +
                    "#cursoNumeroMec, #cursoPrazoIntegralizacao, #cursoPrazoIntegralizacaoMaxima, #cursoVagasNoturno, " +
                    "#cursoVagasVespertino, #cursoVagasMatutino"
                ).inputmask({showMaskOnHover: false, clearIncomplete: true, alias: 'integer'});

                $('#cursoCargaHorariaPratica, #cursoCargaHorariaTeorica').change(function () {
                    $('#cursoCargaHoraria').val(
                        parseInt($('#cursoCargaHorariaPratica').val() || '0') +
                        parseInt($('#cursoCargaHorariaTeorica').val() || '0')
                    );
                });

                $("#cursoNumeroOcde").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: '*{1,45}'});

                $("#cursoDataFuncionamento").datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                }).inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

                var $pes = $("#pes");

                $pes.select2({
                    language: 'pt-BR',
                    ajax: {
                        url: __AcadCursoAdd.options.url.pessoaFisica,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                var text = [];

                                if (el['pesCpf']) {
                                    text.push(el['pesCpf'])
                                }
                                if (el['pesNome']) {
                                    text.push(el['pesNome'])
                                }

                                return {id: el.pesId, text: text.join(' - ')};
                            });

                            return {results: transformed};
                        }
                    },
                    createSearchChoice: function (term) {
                        return {
                            id: term,
                            pesId: '',
                            pesNome: term,
                            text: term + ' (Criar novo registro)'
                        };
                    },
                    allowClear: true,
                    minimumInputLength: 1
                }).change(function () {
                    var data = $(this).select2('data') || [];

                    if (Object.keys(data).length > 0) {
                        $(this).next('.editarPessoa').removeClass('hidden');

                        if (!$.isNumeric(data['id']) && data['pesNome']) {
                            $(this).next('.editarPessoa').click();
                        }
                    } else {
                        $(this).next('.editarPessoa').addClass('hidden');
                    }
                }).trigger('change');

                $pes.select2('data', __AcadCursoAdd.options.value.pes || null);
                var $pessoaFisicaEditar = $('#pessoa-fisica-editar');

                $(".salvar").on("click", function () {
                    __AcadCursoAdd.addOverlay($(".widget-body"));
                    $("#acad-curso-form").submit();
                });

                $('.editarPessoa').click(function (e) {
                    var $elemento = $(this).parent().find('>input');
                    var id = $elemento.attr('id');
                    var data = $elemento.select2('data') || [];

                    $pessoaFisicaEditar.modal('show');
                    var pessoaFisicaAdd = $.pessoaFisicaAdd();
                    pessoaFisicaAdd.setCallback('aposSalvar', function (dados) {
                        var text = [];

                        if (dados['pesCpf']) {
                            text.push(dados['pesCpf'])
                        }
                        if (dados['pesNome']) {
                            text.push(dados['pesNome'])
                        }
                        dados['id'] = dados['pesId'];
                        dados['text'] = text.join(' - ');

                        $elemento.select2('data', dados);

                        $('#pessoa-fisica-editar').modal('hide');
                    });
                    pessoaFisicaAdd.buscarDadosPessoa($elemento.val());

                    $pessoaFisicaEditar.find('[name=pesNome]').val(data['pesNome'] || '');

                    e.preventDefault();
                });

                $('#cursoInformacoes').ckeditor({
                    height: '200px', linkShowAdvancedTab: false,
                    autoStartup: true,
                    extraAllowedContent: (
                        'style;body;*{*};' +
                        'img[src,alt,width,height,style,class,id];' +
                        'iframe[src,alt,width,height,style,class,id];' +
                        'td[class,colspan,width,height,style];' +
                        'th[class,colspan,width,height,style];' +
                        'tr[class,style];' +
                        'h1[class,width,height,style,id];' +
                        'h2[class,width,height,style,id];' +
                        'h3[class,width,height,style,id];' +
                        'div[class,width,height,style,id];' +
                        'span[class,width,height,style,id];' +
                        'p[class,width,height,style,id];' +
                        'table[src,alt,width,height,style,id,class]'
                    ),
                    enterMode: Number(2),
                    toolbar: [
                        [
                            'Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-',
                            'NumberedList', 'BulletedList'
                        ],
                        ['Table'],
                        ['Link', 'Unlink'],
                        ['Undo', 'Redo', '-', 'SelectAll'],
                        ['Maximize', 'Source']
                    ]
                });

                $("#cursoUnidadeMedida").select2({
                    data: __AcadCursoAdd.options.data.cursoUnidadeMedida || []
                });

                var $cursoSituacao = $("#cursoSituacao");

                $cursoSituacao.select2({
                    allowClear: true,
                    data: __AcadCursoAdd.options.data.cursoSituacao || []
                });

                $cursoSituacao.select2('val', (__AcadCursoAdd.options.value['cursoSituacao'] || '')).trigger('change');
            },
            validate: function () {
                __AcadCursoAdd.options.validator.settings.rules = {
                    mod: {required: true},
                    area: {required: true},
                    pes: {required: true},
                    nivel: {required: true},
                    cursoSituacao: {required: true},
                    cursoNome: {maxlength: 255, required: true},
                    cursoCargaHoraria: {maxlength: 11, number: true, required: true},
                    cursoCargaHorariaPratica: {maxlength: 11, number: true, required: true},
                    cursoCargaHorariaTeorica: {maxlength: 11, number: true, required: true},
                    cursoNumeroInep: {maxlength: 12, number: true},
                    cursoNumeroOcde: {maxlength: 45},
                    cursoSigla: {maxlength: 45},
                    cursoNumeroMec: {maxlength: 45},
                    cursoDataFuncionamento: {dateBR: true},
                    cursoVagasVespertino: {maxlength: 11, number: true},
                    cursoVagasMatutino: {maxlength: 11, number: true},
                    cursoPrazoIntegralizacao: {maxlength: 11, number: true},
                    cursoPrazoIntegralizacaoMaxima: {maxlength: 11, number: true},
                    cursoPossuiPeriodoLetivo: {required: true},
                    cursoUnidadeMedida: {required: true},
                    cursoTurno: {required: true}
                };
                __AcadCursoAdd.options.validator.settings.messages = {
                    area: {required: 'Campo obrigatório!'},
                    mod: {required: 'Campo obrigatório!'},
                    pes: {required: 'Campo obrigatório!'},
                    nivel: {required: 'Campo obrigatório!'},
                    cursoSituacao: {required: 'Campo obrigatório!'},
                    cursoNome: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                    cursoPossuiPeriodoLetivo: {maxlength: 'Tamanho máximo: 9!', required: 'Campo obrigatório!'},
                    cursoUnidadeMedida: {required: 'Campo obrigatório!'},
                    cursoCargaHoraria: {
                        maxlength: 'Tamanho máximo: 12!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    cursoCargaHorariaPratica: {
                        maxlength: 'Tamanho máximo: 12!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    cursoCargaHorariaTeorica: {
                        maxlength: 'Tamanho máximo: 12!', number: 'Número inválido!', required: 'Campo obrigatório!'
                    },
                    cursoNumeroInep: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    cursoNumeroOcde: {maxlength: 'Tamanho máximo: 45!'},
                    cursoSigla: {maxlength: 'Tamanho máximo: 45!'},
                    cursoNumeroMec: {maxlength: 'Tamanho máximo: 45!'},
                    cursoDataFuncionamento: {dateBR: 'Informe uma data válida!'},
                    cursoVagasNoturno: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    cursoVagasVespertino: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    cursoVagasMatutino: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    cursoPrazoIntegralizacao: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                    cursoPrazoIntegralizacaoMaxima: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'}
                };

                return !$(__AcadCursoAdd.options.formElement).valid();
            }
        };

        this.steps.outrasConfiguracoes = {
            init: function () {
                var $campus = $("#campus");

                var arqCarteirinha = $('#arqCarteirinha').vfile({
                    thumbBaseUrl: __AcadCursoAdd.options.url.gerenciadorDeArquivosThumb,
                    downloadBaseUrl: __AcadCursoAdd.options.url.gerenciadorDeArquivosDownload
                });

                if (__AcadCursoAdd.options.value.arqCarteirinha) {
                    arqCarteirinha.setSelection(__AcadCursoAdd.options.value.arqCarteirinha);
                }

                $campus.select2({
                    language: 'pt-BR',
                    multiple: true,
                    data: __AcadCursoAdd.options.data.campus

                });
                $campus.select2('data', __AcadCursoAdd.options.value.campus || []);
                __AcadCursoAdd.steps.outrasConfiguracoes.iniciaDatatablesConfiguracoes();
                __AcadCursoAdd.steps.outrasConfiguracoes.tratamentoCamposConfiguracaoModal();

            },

            validate: function () {
                __AcadCursoAdd.options.validator.settings.rules = {
                    campus: {required: true}
                };
                __AcadCursoAdd.options.validator.settings.messages = {
                    campus: {required: 'Campo obrigatório!'}
                };

                return !$(__AcadCursoAdd.options.formElement).valid();
            },
            iniciaDatatablesConfiguracoes: function () {
                var colNum = -1;
                var $dataTableConfiguracoes = $('#dataTableConfiguracoes');
                __AcadCursoAdd.options.datatables.configuracoes = $dataTableConfiguracoes.dataTable({
                    processing: true,
                    columnDefs: [
                        {name: "periodoAtividade", targets: ++colNum, data: "periodoAtividade"},
                        {name: "cursoconfigFreqMin", targets: ++colNum, data: "cursoconfigFreqMin"},
                        {name: "cursoconfigNotaMax", targets: ++colNum, data: "cursoconfigNotaMax"},
                        {name: "cursoconfigNotaMin", targets: ++colNum, data: "cursoconfigNotaMin"},
                        {name: "cursoconfigNotaFinal", targets: ++colNum, data: "cursoconfigNotaFinal"},
                        {name: "cursoconfigMediaFinalMin", targets: ++colNum, data: "cursoconfigMediaFinalMin"},
                        {name: "configuracaoAcoes", targets: ++colNum, data: 'configuracaoAcoes'}
                    ],
                    "dom": "<'dt-toolbar'<'col-xs-3 dt-toolbar-configuracao no-padding'B><'col-xs-6 configuracao-aviso'><'col-xs-3 no-padding'f>r>t",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[2, 'desc']]
                });

                $('.dt-toolbar-configuracao').append(
                    '<div class="btn-group">' +
                    '<button type="button" class="btn btn-primary" id="btn-add-configuracao">Adicionar</button>' +
                    '</div>'
                );

                $('#btn-add-configuracao').click(function () {
                    __AcadCursoAdd.steps.outrasConfiguracoes.showModalConfiguracao();
                    var data = {'GUID': __AcadCursoAdd.createGUID()};
                    __AcadCursoAdd.steps.outrasConfiguracoes.showModalConfiguracao(data);
                });


                $dataTableConfiguracoes.on('click', '.btn-item-edit', function () {
                    var $pai = $(this).closest('tr');
                    var data = __AcadCursoAdd.options.datatables.configuracoes.fnGetData($pai);
                    data['GUID'] = data['GUID'] || __AcadCursoAdd.createGUID();
                    __AcadCursoAdd.steps.outrasConfiguracoes.showModalConfiguracao(data);
                });

                __AcadCursoAdd.steps.outrasConfiguracoes.redrawDatatableConfiguracoes();
            },
            redrawDatatableConfiguracoes: function () {
                __AcadCursoAdd.options.datatables.configuracoes.fnClearTable();
                var data = [];
                $.each(__AcadCursoAdd.options.value.configuracoes || [], function (i, item) {
                    item['cursoconfigId'] = item['cursoconfigId'] || '';
                    item['GUID'] = item['GUID'] || __AcadCursoAdd.createGUID();
                    item['configuracaoAcoes'] = '';
                    item['configuracaoAcoes'] = '\
                <div class="btn-group btn-group-xs text-center">\
                    <a href="#" class="btn btn-primary btn-xs btn-item-edit" title="Editar">\
                        <i class="fa fa-pencil fa-inverse"></i>\
                    </a>\
                </div>';

                    data.push(item);
                });

                if (data.length > 0) {
                    __AcadCursoAdd.options.datatables.configuracoes.fnAddData(data);
                }

                __AcadCursoAdd.options.datatables.configuracoes.fnDraw();
            },
            showModalConfiguracao: function (data) {
                data = data || {};
                var actionTitle = 'Cadastrar';
                var $form = $('#configuracao-curso-form');
                var $modal = $('#modal-configuracao');

                if (Object.keys(data).length > 0) {
                    actionTitle = 'Configuração dos Critérios Para Desempenho Acadêmico:';
                }

                if (data['cursoconfigDeferimentoAutomatico']) {
                    $("#cursoconfigDeferimentoAutomatico").select2('val', data['cursoconfigDeferimentoAutomatico']);
                }

                $modal.find('.modal-title').html(actionTitle);
                $form[0].reset();
                $form.deserialize(data);
                var $regra = $("#cursoconfigMetodo"),
                    $regraFinal = $("#cursoconfigMetodoFinal"),
                    $notaFracionada = $("#cursoconfigNotaFracionada"),
                    $situacaoDeferimento = $("#cursoconfigSituacaoDeferimento");

                var cursoMetodo = __AcadCursoAdd.procuraValorArraySelect2(
                    data['cursoconfigMetodo'], __AcadCursoAdd.options.data.metodo
                );

                var cursoMetodoFinal = __AcadCursoAdd.procuraValorArraySelect2(
                    data['cursoconfigMetodoFinal'], __AcadCursoAdd.options.data.metodoFinal
                );

                $notaFracionada.val(parseInt(data['cursoconfigNotaFracionada']));

                var perAtivId = __AcadCursoAdd.procuraValorArraySelect2(
                    data['perAtivId'], __AcadCursoAdd.options.data.periodoLetivo
                );
                var perDesatId = __AcadCursoAdd.procuraValorArraySelect2(
                    data['perDesatId'], __AcadCursoAdd.options.data.periodoLetivo
                );

                $("#matrizCurricularId").select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __AcadCursoAdd.options.url.matrizCurricular,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {
                                query: query,
                                cursoId: $('#cursoId').val() || '-1'
                            };
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.mat_cur_descricao, id: el.mat_cur_id};
                            });

                            return {results: transformed};
                        }
                    }
                });


                if (data['matrizCurricularId']) {
                    var arrMatriz = {'id': data['matrizCurricularId'], 'text': data['matrizCurricularDesc']};

                    $("#matrizCurricularId").select2('data', arrMatriz);
                }

                $('#perAtivId').select2('data', perAtivId[0] || null);

                $('#perDesatId').select2('data', perDesatId[0] || null);

                $regra.select2('data', cursoMetodo[0] || null);

                $regraFinal.select2('data', cursoMetodoFinal[0] || null);
                $situacaoDeferimento.select2('val', data['cursoconfigSituacaoDeferimento'] || 'Deferido');

                $('#cursoconfigDataInicio, #cursoconfigDataFim').trigger('change');
                $('#perAtivId, #perDesatId,#cursoconfigMetodo,#cursoconfigMetodoFinal').trigger('change');

                $modal.modal();
            },
            editarConfiguracaoItem: function (dados) {
                var novo = true;
                __AcadCursoAdd.options.value.configuracoes = __AcadCursoAdd.options.value.configuracoes || [];
                for (var i in __AcadCursoAdd.options.value.configuracoes) {
                    var item = __AcadCursoAdd.options.value.configuracoes[i];
                    item['GUID'] = item['GUID'] || __AcadCursoAdd.createGUID();

                    if (dados['GUID'] == item['GUID']) {
                        __AcadCursoAdd.options.value.configuracoes[i] = dados;
                        novo = false;

                        break;
                    }
                }

                if (novo) {
                    __AcadCursoAdd.options.value.configuracoes.push(dados);
                }

                __AcadCursoAdd.steps.outrasConfiguracoes.redrawDatatableConfiguracoes();
            },
            tratamentoCamposConfiguracaoModal: function () {
                var $periodos = $("#perAtivId, #perDesatId");

                $("#cursoconfigDeferimentoAutomatico").select2();

                $periodos.select2({
                    allowClear: true,
                    language: 'pt-BR',
                    data: __AcadCursoAdd.options.data.periodoLetivo
                }).change(function () {
                    if ($periodos.val() != '') {
                        $("#cursoconfigDataInicio, #cursoconfigDataFim").prop('disabled', true).val('');
                    } else {
                        $("#cursoconfigDataInicio, #cursoconfigDataFim").prop('disabled', false);
                    }
                });

                $('#cursoconfigMetodo').select2({
                    allowClear: true,
                    language: 'pt-BR',
                    data: __AcadCursoAdd.options.data.metodo
                });

                $('#cursoconfigMetodoFinal').select2({
                    allowClear: true,
                    language: 'pt-BR',
                    data: __AcadCursoAdd.options.data.metodoFinal
                });

                $('#cursoconfigNotaFracionada').inputmask("numeric", {min: 0, max: 3});

                $('#cursoconfigSituacaoDeferimento').select2({
                    allowClear: true,
                    language: 'pt-BR',
                    data: __AcadCursoAdd.options.data.situacaoDeferimento
                });

                $periodos.on('change select2-selected select2-clearing select2-removed', function () {
                    var $item = $(this);

                    if ($("#cursoconfigDataInicio, #cursoconfigDataFim").val() != '') {
                        $item.parent().find('.select2-container').addClass('select2-allowclear');
                    } else {
                        $item.parent().find('.select2-container').removeClass('select2-allowclear');
                    }
                });

                $("#cursoconfigFreqMin, #cursoconfigNotaMax, #cursoconfigNotaMin, " +
                    "#cursoconfigNotaFinal, #cursoconfigMediaFinalMin, #cursoconfigNumeroMaximoPendencias")
                    .inputmask({
                        showMaskOnHover: false, clearIncomplete: true, alias: 'integer'
                    });

                $("#cursoconfigDataInicio")
                    .datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                    })
                    .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
                $("#cursoconfigDataFim")
                    .datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                    })
                    .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

                $("#cursoconfigDataInicio, #cursoconfigDataFim").change(function () {
                    if (this.value != '') {
                        $periodos.select2('disable');
                        $periodos.val('');
                    } else {
                        $periodos.select2('enable');
                    }

                    $periodos.trigger('change');
                });

                $('#btn-salvar-configuracao').click(function () {
                    var $form = $('#configuracao-curso-form');
                    var validacao = $form.validate({ignore: '.ignore'});
                    validacao.settings.rules = {
                        perAtivId: {required: true},
                        cursoconfigDataInicio: {dateBR: true, required: true},
                        perDesatId: {required: false},
                        cursoconfigNumeroMaximoPendencias: {required: true, number: true},
                        cursoconfigFreqMin: {required: true, number: true, min: 0, max: 100},
                        cursoconfigNotaMax: {required: true, number: true},
                        regra: {required: true},
                        regraFinal: {required: true},
                        cursoconfigNotaMin: {
                            required: true, number: true, min: 0, max: function () {
                                return parseInt($('#cursoconfigNotaMax').val());
                            }
                        },
                        cursoconfigNotaFinal: {
                            required: true, number: true, min: 0, max: function () {
                                return parseInt($('#cursoconfigNotaMax').val());
                            }
                        },
                        cursoconfigMediaFinalMin: {
                            required: true, number: true, min: 0, max: function () {
                                return parseInt($('#cursoconfigNotaMax').val());
                            }
                        }
                    };
                    validacao.settings.messages = {
                        perAtivId: {required: 'Campo obrigatório!'},
                        regra: {required: 'Campo obrigatório!'},
                        regraFinal: {required: 'Campo obrigatório!'},
                        cursoconfigDataInicio: {dateBR: 'Data inválida!', required: 'Campo obrigatório!'},
                        cursoconfigFreqMin: {
                            required: 'Campo obrigatório!', number: 'Número inválido!',
                            min: 'Valor dever ser maior que {0}!', max: 'Valor dever ser menor que {0}!'
                        },
                        cursoconfigNumeroMaximoPendencias: {
                            required: 'Campo obrigatório!', number: 'Número inválido!'
                        },
                        cursoconfigNotaMax: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        cursoconfigNotaMin: {
                            required: 'Campo obrigatório!', number: 'Número inválido!',
                            min: 'Valor dever ser maior que {0}!', max: 'Valor dever ser menor que {0}!'
                        },
                        cursoconfigNotaFinal: {
                            required: 'Campo obrigatório!', number: 'Número inválido!',
                            min: 'Valor dever ser maior que {0}!', max: 'Valor dever ser menor que {0}!'
                        },
                        cursoconfigMediaFinalMin: {
                            required: 'Campo obrigatório!', number: 'Número inválido!',
                            min: 'Valor dever ser maior que {0}!', max: 'Valor dever ser menor que {0}!'
                        }
                    };

                    if (!$form.valid()) {
                        __AcadCursoAdd.showNotificacaoWarning('Existem um ou mais erros no formulário!');
                        return;
                    }

                    var arrDados = $form.serializeJSON();
                    arrDados['GUID'] = arrDados['GUID'] || __AcadCursoAdd.createGUID();

                    $('#modal-configuracao').modal('hide');
                    $form.trigger("reset");


                    var periodoInicial = $('#perAtivId').select2('data') || {};
                    var periodoFinal = $('#perDesatId').select2('data') || {};

                    arrDados['periodoInicial'] = periodoInicial['text'] || '';
                    arrDados['periodoFinal'] = periodoFinal['text'] || '';

                    arrDados['periodoAtividade'] = '';

                    if (arrDados['periodoInicial']) {
                        arrDados['periodoAtividade'] = arrDados['periodoInicial'] + ' - ' + arrDados['periodoFinal'];
                    } else {
                        arrDados['periodoAtividade'] = (
                            arrDados['cursoconfigDataInicio'] + ' - ' + arrDados['cursoconfigDataFim']
                        );
                    }

                    __AcadCursoAdd.steps.outrasConfiguracoes.editarConfiguracaoItem(arrDados);
                });
            }
        };

        this.setArea = function (area) {
            this.options.value.area = area || null;
        };

        this.getArea = function () {
            return this.options.value.area || null;
        };

        this.setValidations = function () {
            $(document).on("change", ".form-control:hidden", function () {
                $(__AcadCursoAdd.options.formElement).valid();
            });

            $(this.options.formElement).submit(function (e) {
                var ok = __AcadCursoAdd.validate();

                if (!$("#cursoTurno").val() && $("#mod").val() != 3) {
                    __AcadCursoAdd.showNotificacaoInfo("Curso com modalidade diferente de EAD exige turno!");
                    ok = false;
                }

                $.ajax({
                    url: __AcadCursoAdd.options.url.acadCurso,
                    method: 'POST',
                    dataType: 'json',
                    delay: 450,
                    async: false,
                    data: {
                        cursoNome: $("#cursoNome").val(),
                        notInCurso: $("#cursoId").val()
                    },
                    success: function (arrDados) {
                        if (JSON.stringify(arrDados) != '[]') {
                            __AcadCursoAdd.showNotificacaoInfo("Já existe um curso com esse nome!");
                            ok = false;
                            $(".previous").click();
                        }
                    }
                });

                if (!ok) {
                    __AcadCursoAdd.removeOverlay($(".widget-body"));
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                $('#configuracoes').val(JSON.stringify(__AcadCursoAdd.options.value.configuracoes));
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.loadSteps();
            this.setValidations();
            this.wizard();
        };
    };

    $.acadCursoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acad-curso.add");

        if (!obj) {
            obj = new AcadCursoAdd();
            obj.run(params);
            $(window).data('universa.acad-curso.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);