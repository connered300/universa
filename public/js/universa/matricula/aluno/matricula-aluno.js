(function ($, window, document) {
    'use strict';
    var MatriculaAluno = function () {
        VersaShared.call(this);
        var __matriculaAluno = this;

        this.defaults = {
            url: {
                alunoPesquisa: '',
                alunoCursoInformacao: '',
                informacoesDisciplinas: '',
                informacoesIntegradora: '',
                verificacaoDesligamento: '',
                efetuarDesligamento: '',
                thumbnail: '',
                historicoEscolar: '',
                historicoRemover: '',
                salvaHistoricoEscolar: '',
                pesquisaDisciplina: '',
                pesquisaFormacao: '',
                dataColacao: '',
                dataExpedicao: '',
                editarAluno: '',
                periodoLetivo: ''
            },
            datatable: {
                aluno: null,
                notas: null,
                disciplinas: null,
                historico: null
            },
            data: {
                alunoCurso: [],
                informacoesDisciplinas: {},
                arrSituacoes: []
            },
            dataParam: {
                informacoesDisciplinas: []
            },
            value: {
                arrAluno: null,
                permitirEditarSituacao: null
            }
        };

        this.recarregarInformacoesAluno = function () {
            var $alunoPesquisa = $('#alunoPesquisa');
            var arrData = $alunoPesquisa.select2('data') || __matriculaAluno.options.value.arrAluno || {};
            var $resalunoNota = $("#resalunoNota");

            $resalunoNota.inputmask('remove');

            if (JSON.stringify(arrData) != '{}') {
                var notaFracao = parseInt(arrData['cursoconfig_nota_fracionada'] || '0');
                var notaMax = parseInt(arrData['cursoconfig_nota_max'] || '100');

                /*notaFracao = isNaN(notaFracao) || notaFracao < 1 ? '' : "(\\.\\d{1," + notaFracao + '})?$';
                 $resalunoNota.inputmask('Regex', {regex: "^[0-9]{1,3}" + notaFracao});*/

                $resalunoNota.inputmask({
                    showMaskOnHover: false, alias: 'numeric', groupSeparator: "", radixPoint: ".", integerDigits: 3,
                    placeholder: "0", prefix: "", allowMinus: false, digits: notaFracao, max: notaMax, min: 0
                });
            }

            __matriculaAluno.options.data.alunoCurso = arrData;
            $alunoPesquisa.select2('data', arrData);

            $('#alunoAbas')[arrData['pesId'] ? 'removeClass' : 'addClass']('hidden');
            $('#alunoAbasAviso')[!arrData['pesId'] ? 'removeClass' : 'addClass']('hidden');

            if (arrData['pesId']) {
                __matriculaAluno.carregarInformacoesAluno();
            }
        };

        this.forcaRecarregarInformacoesAluno = function () {
            var $alunoPesquisa = $('#alunoPesquisa');
            var arrData = $alunoPesquisa.select2('data') || {};
            $alunoPesquisa.select2('data', null);
            $alunoPesquisa.trigger('change');
            $alunoPesquisa.select2('data', arrData);
            $alunoPesquisa.trigger('change');
        };

        this.alunoPesquisa = function () {
            var $alunoPesquisa = $('#alunoPesquisa');
            $alunoPesquisa.select2({
                allowClear: true,
                language: 'pt-BR',
                ajax: {
                    url: __matriculaAluno.options.url.alunoPesquisa,
                    dataType: 'json', delay: 0,
                    data: function (query) {
                        return {
                            pesquisa: query,
                            cursoConfig: true
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            var matricula = parseInt(el.alunocurso_id) || 0;
                            var label = '';
                            label += 'Código: ';
                            label += __matriculaAluno.numPad(el.pes_id, 10, '0');

                            if (matricula) {
                                label += ' | Matrícula: ';
                                label += __matriculaAluno.numPad(matricula, 10, '0');
                            }

                            label += ' | Nome: ';
                            label += el.pes_nome;


                            if (matricula) {
                                label += ' | Curso: ';
                                label += el.curso_nome;
                            }

                            el.text = label;
                            el.id = el.pes_id;
                            el.pesId = el.pes_id;
                            el.pesNome = el.pes_nome;
                            el.alunocursoId = matricula ? matricula : '';

                            return el;
                        });

                        return {results: transformed};
                    }
                },
                formatSelection: function (el) {
                    return el.pes_nome;
                },
                minimumInputLength: 1
            });

            $alunoPesquisa.on('change', function () {
                __matriculaAluno.recarregarInformacoesAluno();
            });

        };

        this.carregarInformacoesAluno = function () {
            var $imagemAluno = $('#imagemAluno'),
                $matriculaPeriodo = $('#matriculaPeriodo');

            var carregaDados = function (arrData, padrao) {
                var arrCampos = [
                    'pesNome', 'pesDataNascimento', 'pesCpf', 'pesRg',
                    'pesRgEmissao', 'pesSexo', 'pesNaturalidade', 'pesNascUf',
                    'alunoId', 'alunoPai', 'alunoMae', 'alunoEtnia',
                    'conContatoCelular', 'conContatoTelefone', 'conContatoEmail',
                    'endBairro', 'endCep', 'endCidade', 'endComplemento', 'endData',
                    'endEstado', 'endId', 'endLogradouro', 'endNumero', 'alunocursoId',
                    'cursoId', 'grupoLeitorNome', 'cursoNome', 'campId', 'campNome',
                    'perNome', 'tiposelNome', 'alunocursoSituacao', 'turmaNome', 'alunoperObservacoes', 'turmaTurno', 'situacaoMatricula',
                    'periodoLetivo', 'situacaoId', 'situacaoDescricao', 'dataSituacao'
                ];
                arrData = arrData || {};
                padrao = padrao || false;

                for (var i in arrCampos) {
                    var campo = arrCampos[i];
                    var valor = typeof arrData[campo] == 'undefined' ? false : arrData[campo];

                    if (valor === false && padrao) {
                        valor = padrao;
                    }

                    $('.form-control-static.' + campo).html(valor);
                }

                var imagemAluno = typeof arrData['imagemAluno'] == 'undefined' ? '' : arrData['imagemAluno'];

                $imagemAluno.find('img').hide();
                $imagemAluno.find('span').remove();

                if (imagemAluno) {
                    $imagemAluno.append('<span><i class="fa fa-spinner fa-2x fa-spin"></i>Carregando imagem...</span>');
                    $imagemAluno.find('img').load(function () {
                        $imagemAluno.find('span').remove();
                        $imagemAluno.find('img').show();
                    }).attr(
                        'src',
                        __matriculaAluno.options.url.thumbnail + '/' + imagemAluno + '?width=100&height=80'
                    );
                } else {
                    $imagemAluno.append('<span>' + (padrao || 'Indisponível!') + '</span>');
                }


                $('.pesId').html(
                    '<a href="' + __matriculaAluno.options.url.editarAluno + '/' + arrData['alunoId'] + '">' +
                    arrData['alunoId'] +
                    '</a>'
                );
            };

            __matriculaAluno.notas.limpaDatatables();
            __matriculaAluno.disciplinas.limpaDatatables();
            // atualizando valores da aba de historico escolar
            __matriculaAluno.historico.atualizarDatatables();
            carregaDados(__matriculaAluno.options.data.alunoCurso, 'Carregando...');
            $matriculaPeriodo.select2('val', '');
            $matriculaPeriodo.trigger('change');
            __matriculaAluno.addOverlay($('#alunoCursoInformacao'), 'Aguarde, carregando informações do aluno.');
            __matriculaAluno.addOverlay($('#alunoAcademico'), 'Aguarde, carregando informações acadêmicas do aluno.');
            __matriculaAluno.addOverlay($('#alunoAdministrativo'),
                'Aguarde, carregando informações administrativas do aluno');

            $.ajax({
                url: __matriculaAluno.options.url.alunoCursoInformacao,
                method: 'POST',
                dataType: 'json',
                data: {
                    alunocursoId: __matriculaAluno.options.data.alunoCurso['alunocurso_id'],
                    verificarDependencias: 0,
                    verificarFinanceiro: 1,
                    verificarDocumentos: 0,
                    retornarIntegracoes: 0,
                    pendenciasConclusao: 1
                },
                success: function (arrResposta) {
                    if (arrResposta.alunoCurso) {
                        __matriculaAluno.options.data.alunoCurso = arrResposta.alunoCurso;

                        var matriculasPeriodosLetivos = __matriculaAluno.options.data.alunoCurso['matriculasPeriodosLetivos'] ||
                            [];
                        $matriculaPeriodo.prop('disabled', matriculasPeriodosLetivos.length == 0);

                        var alunoperId = null;

                        if (matriculasPeriodosLetivos.length > 0) {
                            alunoperId = matriculasPeriodosLetivos[0]['alunoper_id'];
                        }

                        alunoperId = __matriculaAluno.options.data.alunoCurso['alunoperId'] || alunoperId;

                        $matriculaPeriodo.select2('val', parseInt(alunoperId));
                        $matriculaPeriodo.trigger('change');
                    }

                    __matriculaAluno.desligamento.exibir();
                    __matriculaAluno.dataColacaoExpedicao.exibir();

                    carregaDados(__matriculaAluno.options.data.alunoCurso, 'Indisponível');

                    __matriculaAluno.removeOverlay($('#alunoCursoInformacao'));
                    __matriculaAluno.removeOverlay($('#alunoAcademico'));
                    __matriculaAluno.removeOverlay($('#alunoAdministrativo'));

                }
            });

        };

        this.pesquisaMatriculaPeriodo = function () {
            var $matriculaPeriodo = $('#matriculaPeriodo');
            $matriculaPeriodo.select2({
                language: 'pt-BR',
                data: function () {
                    var matriculasPeriodosLetivos = __matriculaAluno.options.data.alunoCurso['matriculasPeriodosLetivos'] ||
                        [];
                    var arrMatriculas = $.map(matriculasPeriodosLetivos, function (matricula) {
                        matricula.id = parseInt(matricula.alunoper_id);
                        matricula.text = (
                            matricula.per_ano + '/' +
                            matricula.per_semestre + ' - ' +
                            matricula.turma_nome + ' - ' +
                            matricula.matsit_descricao
                        );

                        return matricula;
                    });

                    return {results: arrMatriculas};
                }
            });

            $matriculaPeriodo.on('change', function () {
                __matriculaAluno.carregarInformacaoAcademicas();
                __matriculaAluno.carregarIntegradora();
            });

            $matriculaPeriodo.prop('disabled', true);
        };

        this.notas = {
            limpaDatatables: function () {
                if (__matriculaAluno.options.datatable.notas) {
                    __matriculaAluno.options.datatable.notas.api().destroy();
                }

                $('#tableAlunoNotas').empty();
            },
            atualizarDatatables: function () {
                var cabecalho = __matriculaAluno.options.data.informacoesDisciplinas['cabecalho'] || [],
                    dados = __matriculaAluno.options.data.informacoesDisciplinas['dados'] || [];

                cabecalho = $.map(cabecalho, function (coluna, pos) {
                    coluna.name = coluna.campo;
                    coluna.data = coluna.campo;
                    coluna.title = coluna.nome;
                    coluna.targets = pos;

                    return coluna;
                });

                __matriculaAluno.notas.limpaDatatables();

                __matriculaAluno.options.datatable.notas = $('#tableAlunoNotas').dataTable({
                    processing: true,
                    columns: cabecalho,
                    data: dados,
                    "dom": "r" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-12'p>>",
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    "iDisplayLength": 10,
                    "bLengthChange": false,
                    "bPaginate": false
                });

                __matriculaAluno.removeOverlay($('#alunoNotas'));
            }
        };

        this.disciplinas = {
            limpaDatatables: function () {
                if (__matriculaAluno.options.datatable.disciplinas) {
                    __matriculaAluno.options.datatable.disciplinas.api().destroy();
                }

                $('#tableAlunoDisciplinas').find('tbody').empty();
            },
            atualizarDatatables: function () {
                var dados = __matriculaAluno.options.data.informacoesDisciplinas['dados'] || [];

                __matriculaAluno.disciplinas.limpaDatatables();

                __matriculaAluno.options.datatable.disciplinas = $('#tableAlunoDisciplinas').dataTable({
                    processing: true,
                    columns: [
                        {name: "discSigla", targets: 0, data: "discSigla"},
                        {name: "discNome", targets: 1, data: "discNome"},
                        {name: "turmaNome", targets: 2, data: "turmaNome"},
                        {name: "situacaoDescricao", targets: 3, data: "situacaoDescricao"}
                    ],
                    data: dados,
                    "dom": "r" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-12'p>>",
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    "iDisplayLength": 10,
                    "bLengthChange": false,
                    "bPaginate": false
                });

                __matriculaAluno.removeOverlay($('#alunoDisciplinas'));
            }
        };

        this.carregarInformacaoAcademicas = function () {
            var matricula = $('#matriculaPeriodo').select2('data') || {};


            if (!matricula['alunoper_id']) {
                return;
            }

            __matriculaAluno.addOverlay($('#alunoNotas'), 'Aguarde, carregando informações sobre notas.');
            __matriculaAluno.addOverlay($('#alunoDisciplinas'), 'Aguarde, carregando informações sobre disciplinas.');

            $.ajax({
                url: __matriculaAluno.options.url.informacoesDisciplinas,
                method: 'POST',
                dataType: 'json',
                data: {alunoperId: matricula['alunoper_id'] || ''},
                success: function (arrNotas) {
                    __matriculaAluno.options.data.informacoesDisciplinas = arrNotas['informacoes'] || [];
                    __matriculaAluno.notas.atualizarDatatables();
                    __matriculaAluno.disciplinas.atualizarDatatables();
                }
            });
        };

        this.carregarIntegradora = function () {
            var matricula = $('#matriculaPeriodo').select2('data') || {};
            var $alunoProvaIntegrada = $('#alunoProvaIntegrada');

            if (!matricula['alunoper_id']) {
                return;
            }

            $alunoProvaIntegrada.html('');
            __matriculaAluno.addOverlay($alunoProvaIntegrada, 'Aguarde, carregando informações sobre integradora.');
            $alunoProvaIntegrada.load(
                __matriculaAluno.options.url.informacoesIntegradora + '/' + (matricula['alunoper_id'] || '')
            );
        };

        this.desligamento = {
            exibir: function () {
                var matricula = __matriculaAluno.options.data.alunoCurso;

                if (matricula['alunoDesligado']) {
                    $("#desligamentoAcaoEfetuada").removeClass('hidden');
                    $("#desligamentoAcaoEfetuar").addClass('hidden');
                } else {
                    $("#desligamentoAcaoEfetuada").addClass('hidden');
                    $("#desligamentoAcaoEfetuar").removeClass('hidden');
                }
            },
            verificar: function () {
                var alunoCurso = __matriculaAluno.options.data.alunoCurso || {};
                var $btnCancelamentoEfetuar = $('#btnCancelamentoEfetuar');

                if (!alunoCurso['alunoperId'] && !alunoCurso['alunocursoId']) {
                    return;
                }

                var alunoperId = alunoCurso['alunoperId'];
                var alunocursoId = alunoCurso['alunocursoId'];
                var tipoDesligamento = $('input[name=tipoDesligamento]:checked').val();
                var motivoDesligamento = $('textarea[name=motivoDesligamento]').val();
                var mensagem = '';
                var cancelarTitulos = $('input[name=cancelarTitulos]:checked').val();

                if (!tipoDesligamento) {
                    mensagem += '<p>Selecione um tipo de cancelamento!</p>';
                }

                if (!motivoDesligamento) {
                    mensagem += '<p>Descreva o motivo pelo qual o aluno está sendo desligado!</p>';
                }

                if (mensagem) {
                    $.SmartMessageBox({
                        title: 'Não foi possível efetuar o desligamento',
                        content: mensagem,
                        buttons: "[Ok]"
                    });

                    return;
                }

                var confirmacao = $btnCancelamentoEfetuar.data('confirmacaoEnvio') || false;
                var validacao = $btnCancelamentoEfetuar.data('validacaoRegras') || false;

                if (!confirmacao) {
                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Esta ação será irreversível. Deseja realmente desligar o aluno?',
                        buttons: "[Não][Sim]",
                        inputValue: ""
                    }, function (ButtonPress) {
                        if (ButtonPress == "Sim") {
                            $btnCancelamentoEfetuar.data('confirmacaoEnvio', true);
                            $btnCancelamentoEfetuar.click();

                            __matriculaAluno.addOverlay($(".modal-content"), "Efetuando desligando do aluno. Aguarde!");
                        }
                    });

                    return false;
                } else if (!validacao) {
                    $('#ajaxLoad').show();
                    $.ajax({
                        url: __matriculaAluno.options.url.verificacaoDesligamento,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            alunoperId: alunoperId,
                            alunocursoId: alunocursoId,
                            tipoDesligamento: tipoDesligamento,
                            motivoDesligamento: motivoDesligamento,
                            cancelarTitulos: cancelarTitulos
                        },
                        success: function (data) {
                            $('#ajaxLoad').hide();

                            if (data.total == 0) {
                                $btnCancelamentoEfetuar.data('validacaoRegras', true);
                                $btnCancelamentoEfetuar.click();

                                return;
                            }

                            var mensagem = '';

                            $.each(data.pendencias, function (area, pendencias) {
                                var erros = '';

                                $.each(pendencias, function (i, erro) {
                                    erros += '<li>' + erro + '</li>';
                                });

                                mensagem += '\
                                    <div>\
                                        <i class="fa fa-times text-danger"></i> <b>' + area + '</b>\
                                        <ul>' + erros + '</ul>\
                                    </div>';
                            });

                            mensagem = '<p>' +
                                'O aluno possui ' + data.total +
                                ' pendência' + (data.total == 1 ? '' : 's') + ':' +
                                '</p>' +
                                '<blockquote>' +
                                mensagem +
                                '</blockquote>' +
                                '<p>Entre em contato com os setores para regularização ' +
                                'antes de efetuar o desligamento.</p>';

                            $.SmartMessageBox({
                                title: 'Não foi possível efetuar o desligamento',
                                content: mensagem,
                                buttons: "[Ok]"
                            });

                            __matriculaAluno.removeOverlay($(".modal-content"));

                            $btnCancelamentoEfetuar.data('confirmacaoEnvio', false);
                            $btnCancelamentoEfetuar.data('validacaoRegras', false);

                            return false;
                        },
                        error: function () {

                            __matriculaAluno.showNotificacaoDanger(
                                "Falha ao verificar regras para desligamento do aluno.<br>" +
                                "Entre em contato com o suporte.",
                                "Não foi possível validar desligamento:"
                            );
                            $btnCancelamentoEfetuar.data('confirmacaoEnvio', false);
                            $btnCancelamentoEfetuar.data('validacaoRegras', false);
                            $('#ajaxLoad').hide();
                        }
                    });
                    __matriculaAluno.removeOverlay($(".modal-content"));
                    return false;
                }

                $btnCancelamentoEfetuar.data('confirmacaoEnvio', false);
                $btnCancelamentoEfetuar.data('validacaoRegras', false);

                $.ajax({
                    url: __matriculaAluno.options.url.efetuarDesligamento,
                    type: 'POST',
                    dataType: 'json',
                    delay: 250,
                    data: {
                        alunoperId: alunoperId,
                        alunocursoId: alunocursoId,
                        tipoDesligamento: tipoDesligamento,
                        motivoDesligamento: motivoDesligamento,
                        cancelarTitulos: cancelarTitulos
                    },
                    success: function (data) {
                        if (data.erro) {
                            __matriculaAluno.showNotificacaoDanger(
                                data.mensagem || 'Falha ao desligar aluno.'
                            );

                            $('input[name=tipoDesligamento]:checked').prop('checked', false);
                            $('textarea[name=motivoDesligamento]').val('');
                        } else {
                            __matriculaAluno.showNotificacaoSuccess(
                                data.mensagem || 'Aluno desligado com sucesso.'
                            );
                        }

                        $('#desligamento-modal').modal('hide');
                        __matriculaAluno.forcaRecarregarInformacoesAluno();
                        $('#ajaxLoad').hide();
                        __matriculaAluno.removeOverlay($(".modal-content"));
                        return false;
                    },
                    error: function () {
                        __matriculaAluno.showNotificacaoDanger(
                            "Falha ao verificar regras para desligamento do aluno.<br>" +
                            "Entre em contato com o suporte.",
                            "Não foi possível validar desligamento:"
                        );
                        $btnCancelamentoEfetuar.data('confirmacaoEnvio', false);
                        $btnCancelamentoEfetuar.data('validacaoRegras', false);
                        $('#ajaxLoad').hide();
                        __matriculaAluno.removeOverlay($(".modal-content"));
                    }
                });
            }
        };

        this.dataColacaoExpedicao = {
            exibir: function () {
                var matricula = __matriculaAluno.options.data.alunoCurso;
                var $mensagemDatasConclusao = $("#mensagemDatasConclusao");
                var $mensagemDatasConclusaoP = $mensagemDatasConclusao.find("p:last");

                $("#dataDaColacao").val(matricula['alunocursoDataColacao'] || '');
                $("#dataDaExpedicao").val(matricula['alunocursoDataExpedicaoDiploma'] || '');

                $mensagemDatasConclusaoP.html(
                    matricula['pendenciasConclusao']['pendencias'].join('<br>')
                );

                $mensagemDatasConclusaoP.html(
                    matricula['pendenciasConclusao']['datas'].join('<br>')
                );

                if (matricula['pendenciasConclusao']['possuiPendencias']) {
                    $("#salvarColacao").addClass('hidden');
                    $mensagemDatasConclusao.removeClass('hidden');
                    $("#formDatasConclusao").addClass('hidden');
                } else {
                    $("#salvarColacao").removeClass('hidden');
                    $mensagemDatasConclusao.addClass('hidden');
                    $("#formDatasConclusao").removeClass('hidden');
                }
            },
            init: function () {
                __matriculaAluno.iniciarElementoDatePicker($("#dataDaColacao, #dataDaExpedicao"));

                var $situacaoAluno = $("#situacaoAluno");
                var $overlay = $(".modal-overlay");

                $("#editarMatriculaPeriodo").on("click", function () {

                    __matriculaAluno.addOverlay($overlay, 'Aguarde carregando dados, para edição!');
                    __matriculaAluno.showNotificacaoWarning(
                        "Ao alterar a situação do aluno(a) no período letivo, a(s) disciplina(s) do período também serão afetadas.");

                    var arrPeriodoAluno = $("#matriculaPeriodo").select2("data") || {};
                    var arrAluno = __matriculaAluno.options.data.alunoCurso || {};
                    var $form = $("#altera-situacao-form");

                    $("#alunoperId").val(arrPeriodoAluno['alunoper_id']);
                    $('.form-control-static.' + 'turmaNome').html(arrPeriodoAluno['turma_nome'] || '-');
                    $('.form-control-static.' + 'cursoNome').html(arrAluno['cursocampusNome'] || '-');
                    $('.form-control-static.' + 'situacaoNome').html(arrPeriodoAluno['matsit_descricao'] || '-');
                    $('.form-control-static.' + 'matriculaPeriodoSituacao').html(arrPeriodoAluno['per_nome'] || '-');
                    $('.form-control-static.' + 'alunoObservacao').html(arrAluno['alunoperObservacoes'] || '-');
                    $("#altera-situacao-modal").modal("show");

                    $situacaoAluno.select2({data: (__matriculaAluno.options.data.arrSituacoes || [])});
                    $situacaoAluno.select2('val', arrPeriodoAluno['situacao_id'] || '');
                    $("#alteracaoObservacao").val('');

                    __matriculaAluno.validaAlteracaoSituacao(false);
                    __matriculaAluno.removeOverlay($overlay);

                    $form.on('submit', function (e) {
                        e.stopPropagation();
                        e.stopPropagation();
                    }).validate({
                        ignore: '.ignore',
                        submitHandler: function () {
                            if (!__matriculaAluno.validaAlteracaoSituacao(true)) {
                                return false;
                            }

                            __matriculaAluno.addOverlay($('.modal-content'), 'Aguarde...');

                            $.ajax({
                                url: $form.attr('action'),
                                type: 'POST',
                                dataType: 'json',
                                data: $form.serializeJSON(),
                                success: function (data) {
                                    if (!data['erro']) {
                                        __matriculaAluno.showNotificacaoSuccess("Situação alterada com sucesso!");
                                        $(".close").click();
                                        __matriculaAluno.recarregarInformacoesAluno();

                                    } else {
                                        __matriculaAluno.showNotificacaoDanger(
                                            data['mensagem'] || 'Não foi possível alterar a situação do aluno!'
                                        );
                                    }

                                    __matriculaAluno.removeOverlay($('.modal-content'));
                                }
                            });
                        },
                        rules: {
                            situacaoAluno: {required: true},
                            matriculaPeriodoSituacao: {required: true},
                            alteracaoObservacao: {required: true}
                        },
                        messages: {
                            situacaoAluno: {required: 'Campo Obrigatório'},
                            matriculaPeriodoSituacao: {required: 'Campo Obrigatório'},
                            alteracaoObservacao: {required: 'Campo Obrigatório'}
                        }
                    });
                });

                $("#salvarDatasConclusao").click(function () {
                    $.ajax({
                        url: __matriculaAluno.options.url.dataColacao,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            alunocursoId: __matriculaAluno.options.data.alunoCurso.aluno,
                            dataDaColacao: $("#dataDaColacao").val()
                        },
                        success: function (data) {
                            if (data.result['type'] == 'error') {
                                __matriculaAluno.showNotificacaoDanger(
                                    data.result['message'] || 'Falha ao adicionar ou atualizar a data.'
                                );
                            } else if (data.result['type'] == 'info') {
                                __matriculaAluno.showNotificacaoInfo(data.result['message']);
                            } else {
                                __matriculaAluno.showNotificacaoSuccess(
                                    data.result['message'] || 'Operação efetuada com sucesso.'
                                );

                                $('#modalColacao').modal('hide');
                            }
                        }
                    });
                    $.ajax({
                        url: __matriculaAluno.options.url.dataExpedicao,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            alunocursoId: __matriculaAluno.options.data.alunoCurso.aluno,
                            dataDaExpedicao: $("#dataDaExpedicao").val()
                        },
                        success: function (data) {
                            if (data.result['type'] == 'error') {
                                __matriculaAluno.showNotificacaoDanger(
                                    data.result['message'] || 'Falha ao adicionar ou atualizar a data.'
                                );
                            } else if (data.result['type'] == 'info') {
                                __matriculaAluno.showNotificacaoInfo(data.result['message']);
                            } else {
                                __matriculaAluno.showNotificacaoSuccess(
                                    data.result['message'] || 'Operação efetuada com sucesso.'
                                );

                                $('#modalColacao').modal('hide');
                            }
                        }
                    });
                });

                $("#btnDatasConclusao").click(function () {
                    $("#modalDatasConclusao").modal('show');
                });
            }
        };

        this.historico = {
            atualizarDatatables: function () {
                __matriculaAluno.addOverlay(
                    $('#alunoHistoricoEscolar'),
                    'Carregando histórico de disciplinas do aluno...'
                );

                if (__matriculaAluno.options.datatable.historico) {
                    __matriculaAluno.options.datatable.historico.api().ajax.reload(null, false);
                }
            },
            informacoesFormacao: function () {
                var formacaoData = (
                    $("#formacaoId").select2('data') ||
                    __matriculaAluno.options.data.alunoCurso ||
                    {}
                );

                var meses = {
                    'anual': 12,
                    'semestral': 6,
                    'trimestral': 3,
                    'bimestral': 2,
                    'mensal': 1
                };

                var formacaoAno = '';
                var formacaoRegime = '';
                var formacaoDuracao = '';
                var formacaoInicio = '';
                var formacaoQtdMeses = '';

                if (Object.keys(formacaoData).length > 0) {
                    if (formacaoData['formacao_id']) {
                        formacaoAno = parseInt(formacaoData['formacao_ano'] || '');
                        formacaoRegime = formacaoData['formacao_regime'] || '';
                        formacaoDuracao = parseInt(formacaoData['formacao_duracao'] || '');
                        formacaoQtdMeses = (formacaoDuracao * meses[formacaoRegime.toLocaleLowerCase()]);
                        formacaoInicio = formacaoAno - (formacaoQtdMeses / 12);
                    } else {
                        formacaoAno = formacaoData['alunoDataCadastro'] || '';
                        formacaoRegime = formacaoData['cursoPossuiPeriodoLetivo'] || '';
                        formacaoDuracao = parseInt(formacaoData['cursoPrazoIntegralizacao'] || '');

                        if (formacaoAno.length > 4) {
                            formacaoAno = parseInt((new RegExp(/[0-9]{4}/i)).exec(formacaoAno)[0] || '');
                        }

                        formacaoInicio = formacaoAno;
                        formacaoQtdMeses = (formacaoDuracao * meses[formacaoRegime.toLocaleLowerCase()]);
                        formacaoAno = formacaoInicio + (formacaoQtdMeses / 12);
                    }
                }

                return {
                    formacaoAno: formacaoAno,
                    formacaoRegime: formacaoRegime,
                    formacaoDuracao: formacaoDuracao,
                    formacaoInicio: formacaoInicio,
                    numeroPeriodosAno: (12 / meses[formacaoRegime.toLocaleLowerCase()])
                }
            },
            init: function () {
                var rowIndex = 0;
                var $dataTableHistorico = $('#dataTableHistorico'),
                    $resalunoAno = $('#resalunoAno'),
                    $resalunoSemestre = $('#resalunoSemestre'),
                    $resalunoSerie = $('#resalunoSerie'),
                    $equivalencias = $("#equivalencias");

                __matriculaAluno.options.datatable.historico = $dataTableHistorico.dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __matriculaAluno.options.url.historicoEscolar,
                        type: "POST",
                        dataType: 'json',
                        data: function (d) {
                            d.filter = d.filter || {};

                            var matricula = __matriculaAluno.options.data.alunoCurso || {};

                            d['alunocursoId'] = matricula['alunocurso_id'] || matricula['alunocursoId'] || '0';

                            return d;
                        },
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary btn-item-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger btn-item-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acoes'] = btnGroup;

                                var resEquivalenciasLabel = [];
                                var resEquivalenciasId = [];

                                if (data[row]['res_equivalencias']) {
                                    resEquivalenciasLabel = data[row]['res_equivalencias'].split('||');
                                    resEquivalenciasId = data[row]['res_equivalencias_id'].split('||');
                                }

                                var resEquivalencias = [];

                                for (var i in resEquivalenciasId) {
                                    resEquivalencias.push({
                                        id: resEquivalenciasId[i],
                                        text: resEquivalenciasLabel[i]
                                    });
                                }

                                data[row]['res_equivalencias_formatado'] = resEquivalenciasLabel.join('<br>');
                                data[row]['res_equivalencias_objeto'] = resEquivalencias;

                                if (data[row]['resaluno_origem'] == 'Sistema') {
                                    data[row]['acoes'] = '-';
                                }
                            }

                            __matriculaAluno.removeOverlay($('#alunoHistoricoEscolar'));

                            return data;
                        }
                    },
                    columns: [
                        {width: '5%', name: 'resaluno_origem', target: rowIndex++, data: 'resaluno_origem'},
                        {width: '25%', name: 'res_insituicao', target: rowIndex++, data: 'res_insituicao'},
                        {width: '20%', name: 'disc_nome', target: rowIndex++, data: 'disc_nome'},
                        {
                            width: '5%', name: 'resaluno_carga_horaria', target: rowIndex++,
                            data: 'resaluno_carga_horaria'
                        },
                        {width: '5%', name: 'resaluno_nota', target: rowIndex++, data: 'resaluno_nota'},
                        {width: '5%', name: 'resaluno_faltas', target: rowIndex++, data: 'resaluno_faltas'},
                        {width: '5%', name: 'resaluno_serie', target: rowIndex++, data: 'resaluno_serie'},
                        {width: '5%', name: 'resaluno_pl', target: rowIndex++, data: 'resaluno_pl'},
                        {width: '5%', name: 'resaluno_situacao', target: rowIndex++, data: 'resaluno_situacao'},
                        {
                            width: '15%', name: 'res_equivalencias', target: rowIndex++,
                            data: 'res_equivalencias_formatado'
                        },
                        {width: '5%', name: 'res_id', target: rowIndex++, data: 'acoes', searchable: false}
                    ],
                    "dom": "<'dt-toolbar'<'col-xs-6 dt-toolbar-historico no-padding'B><'col-xs-6 no-padding'f>r>t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    "pageLength": 20,
                    ordering: false
                });

                $dataTableHistorico.on('click', '.btn-item-remove', function (e) {
                    var $pai = $(this).closest('tr');
                    var data = __matriculaAluno.options.datatable.historico.fnGetData($pai);
                    __matriculaAluno.historico.removerHistorico(data);
                    e.preventDefault();
                });

                $("#formacaoId").select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __matriculaAluno.options.url.pesquisaFormacao,
                        dataType: 'json',
                        type: 'POST',
                        delay: 3,
                        data: function (query) {
                            var matricula = __matriculaAluno.options.data.alunoCurso || {};

                            return {
                                alunoId: (matricula['aluno_id'] || matricula['alunoId'] || '0'),
                                query: query
                            };
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.text = (el.formacao_instituicao || ' - ') + ' / ' + el.formacao_curso;
                                el.id = el.formacao_id;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                }).change(function () {
                    var informacoesFormacao = __matriculaAluno.historico.informacoesFormacao();
                    var resalunoAno = $resalunoAno.val();

                    if (resalunoAno != informacoesFormacao.formacaoInicio) {
                        $resalunoAno.val(informacoesFormacao.formacaoInicio).trigger('change');
                    }

                    if (this.value == '') {
                        $equivalencias.select2('disable');
                        $("#resalunoNota").prop('disabled', false);
                        $equivalencias.select2('val', '');
                        $equivalencias.trigger('change');
                    } else {
                        $equivalencias.select2('enable');
                        $("#resalunoNota").prop('disabled', true);

                        if ($equivalencias.val() == '') {
                            $equivalencias.select2('data', $("#discId").select2('data'));
                        }
                    }
                });

                $("#discId").select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __matriculaAluno.options.url.pesquisaDisciplina,
                        dataType: 'json', delay: 0,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.text = el.disc_nome;
                                el.id = el.disc_id;

                                return el;
                            });

                            return {results: transformed};
                        }
                    },
                    createSearchChoice: function (term, data) {
                        if ($(data).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                            return {id: term, text: term};
                        }
                    },
                    minimumInputLength: 1
                }).change(function () {
                    if ($equivalencias.val() == '') {
                        if ($('#formacaoId').val()) {
                            $equivalencias.select2('data', $('#discId').select2('data'));
                        } else {
                            $equivalencias.select2('val', '');
                        }
                    }

                    var alunoperId = __matriculaAluno.options.data.alunoCurso['alunoperId'] || '';
                    var discId = $('#discId').val();

                    if (!(/^\d+$/.test(discId)) && discId != "") {
                        __matriculaAluno.showNotificacaoWarning(
                            'A disciplina "' + discId + '" será criada, ' +
                            ' pois a mesma ainda não existe.'
                        );
                    }

                    if (discId != '' && alunoperId && $('#formacaoId').val() == '') {
                        $('#ajaxLoad').show();
                        $.ajax({
                            url: __matriculaAluno.options.url.informacoesDisciplinas,
                            method: 'POST',
                            dataType: 'json',
                            data: {alunoperId: alunoperId},
                            success: function (arrNotas) {
                                var informacoes = arrNotas['informacoes'] || [];
                                var disciplinas = informacoes['dados'] || [];

                                $(disciplinas).each(function (i, item) {
                                    if (discId == item['discId']) {
                                        /* TODO: Repensar esta regra
                                         __matriculaAluno.showNotificacaoWarning(
                                         'Ao adicionar a disciplina "' + item['discNome'] + '", ' +
                                         'ocorrerá dispensa da mesma no período letivo atual.'
                                         );*/
                                    }
                                });

                                $('#ajaxLoad').hide();
                            }
                        });
                    }
                });

                $equivalencias.select2({
                    language: 'pt-BR',
                    tags: true,
                    allowClear: true,
                    ajax: {
                        url: __matriculaAluno.options.url.pesquisaDisciplina,
                        dataType: 'json', delay: 0,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.text = el.disc_nome;
                                el.id = el.disc_id;

                                return el;
                            });

                            return {results: transformed};
                        }
                    },
                    createSearchChoice: function (term, data) {
                        if ($(data).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                            return {id: term, text: term};
                        }
                    },
                    minimumInputLength: 1
                }).change(function () {
                    var discId = $equivalencias.val();
                    discId = discId ? discId.split(',') : [];

                    for (var i in discId) {
                        if (!(/^\d+$/.test(discId[i])) && discId[i] != "") {
                            __matriculaAluno.showNotificacaoWarning(
                                'A disciplina "' + discId[i] + '" será criada, ' +
                                ' pois a mesma ainda não existe.'
                            );
                        }
                    }
                    var alunoperId = __matriculaAluno.options.data.alunoCurso['alunoperId'] || '';
                    var equivalencias = $equivalencias.select2('data');

                    if (equivalencias && alunoperId && $('#formacaoId').val()) {
                        $('#ajaxLoad').show();
                        $.ajax({
                            url: __matriculaAluno.options.url.informacoesDisciplinas,
                            method: 'POST',
                            dataType: 'json',
                            data: {alunoperId: alunoperId},
                            success: function (arrNotas) {
                                var informacoes = arrNotas['informacoes'] || [];
                                var disciplinas = informacoes['dados'] || [];

                                $(disciplinas).each(function (i, item) {
                                    $(equivalencias).each(function (i2, item2) {
                                        if (item2['id'] == item['discId']) {
                                            __matriculaAluno.showNotificacaoWarning(
                                                'Ao adicionar a disciplina "' + item['discNome'] + '", ' +
                                                'ocorrerá dispensa da mesma no período letivo atual.'
                                            );
                                        }
                                    });
                                });

                                $('#ajaxLoad').hide();
                            }
                        });
                    }
                });

                $("#resalunoCargaHoraria, #resalunoFaltas, #resalunoAno, #resalunoSerie").inputmask({
                    showMaskOnHover: false, mask: ['9{0,9}']
                });

                $resalunoAno.change(function () {
                    var resalunoAno = $resalunoAno.val() || '';
                    var informacoesFormacao = __matriculaAluno.historico.informacoesFormacao();

                    $resalunoSemestre.select2((resalunoAno == '') ? 'disable' : 'enable');
                    $resalunoSemestre.select2('data', null);
                    $resalunoSemestre.trigger('change');

                    if (!resalunoAno) {
                        return;
                    }

                    if (resalunoAno > informacoesFormacao.formacaoAno ||
                        resalunoAno < informacoesFormacao.formacaoInicio
                    ) {
                        __matriculaAluno.showNotificacaoWarning(
                            'De acordo com o registro do aluno, ' +
                            'os itens do histórico devem estar entre os anos ' +
                            informacoesFormacao.formacaoInicio + ' e ' + informacoesFormacao.formacaoAno
                        )
                    }
                });

                $resalunoSemestre.select2({
                    language: 'pt-BR',
                    data: function () {
                        var resalunoAno = $resalunoAno.val();
                        var informacoesFormacao = __matriculaAluno.historico.informacoesFormacao();
                        var arrPeriodo = [];
                        var periodoCount = 1;
                        var periodo = 0;
                        var ano = 0;

                        for (ano = informacoesFormacao.formacaoInicio; ano <= informacoesFormacao.formacaoAno; ano++) {
                            for (periodo = 1; periodo <= informacoesFormacao.numeroPeriodosAno; periodo++) {
                                if (ano == resalunoAno) {
                                    arrPeriodo.push({id: periodo, text: periodo + ' / ' + ano, serie: periodoCount});
                                }

                                periodoCount++;
                            }
                        }

                        if (arrPeriodo.length == 0) {
                            for (periodo = 1; periodo <= informacoesFormacao.numeroPeriodosAno; periodo++) {
                                arrPeriodo.push({
                                    id: periodo,
                                    text: periodo + ' / ' + resalunoAno
                                });
                            }
                        }

                        return {results: arrPeriodo};
                    }
                }).change(function () {
                    var arrData = $resalunoSemestre.select2('data') || {};
                    $resalunoSerie.val(arrData['serie'] || $resalunoSerie.val() || '');
                });

                $("#resalunoSemestre, #equivalencias, #discId, #formacaoId").on(
                    'change select2-selected select2-clearing select2-removed',
                    function () {
                        var $item = $(this);

                        if ($item.val() != '') {
                            $item.parent().find('.select2-container').addClass('select2-allowclear');
                        } else {
                            $item.parent().find('.select2-container').removeClass('select2-allowclear');
                        }
                    }
                );

                $("#resalunoSituacao").select2({language: 'pt-BR'});

                $('.dt-toolbar-historico').append(
                    '<div class="btn-group">' +
                    '<button type="button" class="btn btn-primary" id="btn-add-historico">Adicionar</button>' +
                    '</div>'
                );

                $('#btn-add-historico').click(function () {
                    __matriculaAluno.historico.showModalFormacao();
                });


                $dataTableHistorico.on('click', '.btn-item-edit', function () {
                    var $pai = $(this).closest('tr');
                    var data = __matriculaAluno.options.datatable.historico.fnGetData($pai);

                    var arrData = {
                        resId: data['res_id'],
                        discId: {id: data['disc_id'], text: data['disc_nome']},
                        equivalencias: data['res_equivalencias_objeto'],
                        resalunoAno: data['resaluno_ano'],
                        resalunoCargaHoraria: data['resaluno_carga_horaria'],
                        resalunoFaltas: data['resaluno_faltas'],
                        resalunoOrigem: data['resaluno_origem'],
                        resalunoSerie: data['resaluno_serie'],
                        resalunoNota: data['resaluno_nota'],
                        resalunoSemestre: {
                            id: data['resaluno_semestre'],
                            text: data['resaluno_semestre'] + ' / ' + data['resaluno_ano']
                        },
                        formacaoId: (
                            data['formacao_id'] ?
                            {
                                id: data['formacao_id'],
                                text: data['res_insituicao'],
                                formacao_id: data['formacao_id'],
                                res_insituicao: data['res_insituicao'],
                                formacao_ano: data['formacao_ano'],
                                formacao_regime: data['formacao_regime'],
                                formacao_duracao: data['formacao_duracao']
                            } :
                                null
                        ),
                        resalunoSituacao: {id: data['resaluno_situacao'], text: data['resaluno_situacao']}
                    };

                    __matriculaAluno.historico.showModalFormacao(arrData);
                });

                $('#btn-salvar-historico').click(function (e) {
                    var $historicoForm = $('#historico-form');
                    var validacao = $historicoForm.validate({ignore: '.ignore'});
                    validacao.settings.rules = {
                        resalunoCargaHoraria: {required: true, number: true},
                        resalunoNota: {required: true, max: 100},
                        //resalunoFaltas: {required: true, number: true},
                        resalunoAno: {required: true, number: true, min: 1900},
                        resalunoSerie: {required: true, number: true},
                        resalunoSemestre: {required: true},
                        resalunoSituacao: {required: true},
                        equivalencias: {
                            required: function () {
                                return $('#formacaoId').val() != "";
                            }
                        }
                    };
                    validacao.settings.messages = {
                        resalunoCargaHoraria: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        resalunoNota: {required: 'Campo obrigatório!', max: 'Valor máximo extrapolado!'},
                        resalunoFaltas: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        resalunoAno: {required: 'Campo obrigatório!', number: 'Número inválido!', min: 1900},
                        resalunoSerie: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        resalunoSemestre: {required: 'Campo obrigatório!'},
                        resalunoSituacao: {required: 'Campo obrigatório!'},
                        equivalencias: {required: 'Campo obrigatório!'}
                    };

                    if (!$historicoForm.valid()) {
                        __matriculaAluno.showNotificacaoWarning('Existem um ou mais erros no formulário!');
                        return;
                    }

                    var arrDados = $historicoForm.serializeJSON();
                    var alunoCurso = __matriculaAluno.options.data.alunoCurso || {};

                    arrDados['alunocursoId'] = alunoCurso['alunocurso_id'] || alunoCurso['alunocursoId'];
                    arrDados['resalunoOrigem'] = arrDados['resalunoOrigem'] || 'Manual';

                    __matriculaAluno.historico.salvarRegistro(arrDados);
                });

                $dataTableHistorico.on('click', '.item-remove', function (e) {
                    var $pai = $(this).closest('tr');
                    var historicoData = __matriculaAluno.options.datatables.formacoes.fnGetData($pai);
                    historicoData['GUID'] = historicoData['GUID'] || __matriculaAluno.createGUID();

                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Deseja realmente remover esta formação?',
                        buttons: "[Cancelar][Ok]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Ok") {
                            var arrFormacaoManter = $.grep(
                                __matriculaAluno.options.value.formacoes || [],
                                function (item) {
                                    item['GUID'] = item['GUID'] || __matriculaAluno.createGUID();
                                    return (item['GUID'] != historicoData['GUID']);
                                });

                            __matriculaAluno.options.value.formacoes = arrFormacaoManter;
                            __matriculaAluno.historico.atualizarDatatables();
                        }
                    });

                    e.preventDefault();
                });

                __matriculaAluno.historico.atualizarDatatables();
            },
            showModalFormacao: function (data) {
                data = data || {};
                var $form = $('#historico-form');

                $('#formacaoId, #discId, #resalunoSemestre, #equivalencias, #resalunoSituacao').select2('val', '');

                data['resalunoNota'] = data['resalunoNota'] || 0;

                $form[0].reset();
                $form.deserialize(data);

                $('[name^=resId]').val(data ? (data.resId ? data.resId : "") : "");

                if (data['resalunoSituacao']) {
                    $('#resalunoSituacao').select2('data', data['resalunoSituacao']);
                }

                if (data['formacaoId']) {
                    $('#formacaoId').select2('data', data['formacaoId']);
                }

                if (data['equivalencias']) {
                    $('#equivalencias').select2('data', data['equivalencias']);
                }

                if (data['discId']) {
                    $('#discId').select2('data', data['discId']);
                }

                $("#resalunoAno, #resalunoSemestre, #equivalencias, #discId, #formacaoId, #resalunoSituacao").trigger('change');
                $("#resalunoAno").val(data['resalunoAno']);

                if (data['resalunoSemestre']) {
                    $('#resalunoSemestre').select2('data', data['resalunoSemestre']);
                }

                $('#modal-historico').modal();
            },
            salvarRegistro: function (dados) {
                $('#ajaxLoad').show();
                $.ajax({
                    url: __matriculaAluno.options.url.salvaHistoricoEscolar,
                    type: 'POST',
                    dataType: 'json',
                    data: dados,
                    success: function (data) {
                        if (data.result['type'] == 'error') {
                            __matriculaAluno.showNotificacaoDanger(
                                data.result['message'] || 'Falha ao salvar item de histórico do aluno.'
                            );
                        } else {
                            __matriculaAluno.showNotificacaoSuccess(
                                data.result['message'] || 'Item de histórico do aluno salvo com sucesso.'
                            );

                            $('#modal-historico').modal('hide');

                            __matriculaAluno.historico.atualizarDatatables();
                        }

                        $('#ajaxLoad').hide();
                    }
                });
            },
            removerHistorico: function (data) {
                var arrRemover = {resId: data['res_id']};

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: (
                        'Deseja realmente remover o item "' +
                        data['res_insituicao'] + ' - ' + data['disc_nome'] + '"?'
                    ),
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        __matriculaAluno.addOverlay(
                            $('#alunoHistoricoEscolar'),
                            'Aguarde, solicitando remoção de item do histórico do aluno...'
                        );
                        $.ajax({
                            url: __matriculaAluno.options.url.historicoRemover,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    __matriculaAluno.showNotificacaoDanger(
                                        data['mensagem'] ||
                                        "Não foi possível remover a item do histórico."
                                    );

                                    __matriculaAluno.removeOverlay($('#alunoHistoricoEscolar'));
                                } else {
                                    __matriculaAluno.options.datatable.historico.api().ajax.reload(null, false);
                                    __matriculaAluno.showNotificacaoSuccess(
                                        data['mensagem'] ||
                                        "Item do histórico removido com sucesso."
                                    );
                                }
                            }
                        });
                    }
                });
            }
        };

        this.validaAlteracaoSituacao = function (notificar) {
            var permitirEdicaoSituacao = __matriculaAluno.options.value.permitirEditarSituacao;
            var matricula = $('#matriculaPeriodo').select2('data') || {};
            var $containerAtencao = $('.atencao');
            var msgErro = '';

            $("#situacaoAluno").select2("enable");
            $("#salvar").prop("disabled", false);
            $("#alteracaoObservacao").prop("disabled", false);

            $containerAtencao.removeClass('hidden');

            if (matricula['periodoAbertoAlteraMatricula'] == 0) {
                __matriculaAluno.desativaCamposAlteraSituacao();
                msgErro += "<p>Para alterar a situação do aluno, seu período deve esta aberto e dentro da data de inscrição inicial!</p>";
            }

            if (!permitirEdicaoSituacao) {
                __matriculaAluno.desativaCamposAlteraSituacao();
                msgErro += "<p>Você não tem permissão para alterar a situação do aluno</p>";
            }

            if (msgErro) {
                $containerAtencao.find('.form-control-static').html(msgErro);

                if (notificar) {
                    __matriculaAluno.showNotificacaoInfo(msgErro);
                }

                return false;
            } else {
                $containerAtencao.addClass('hidden');
            }

            return true;
        };

        this.desativaCamposAlteraSituacao = function () {
            $("#situacaoAluno").select2("disable");
            $("#salvar").prop("disabled", true);
            $("#alteracaoObservacao").prop("disabled", true);
        };

        this.run = function (opts) {
            __matriculaAluno.setDefaults(opts);
            __matriculaAluno.alunoPesquisa();
            __matriculaAluno.pesquisaMatriculaPeriodo();
            __matriculaAluno.historico.init();
            __matriculaAluno.dataColacaoExpedicao.init();

            $('#btnCancelamentoEfetuar').click(__matriculaAluno.desligamento.verificar);
            $('#showDesligamentoModal').click(function () {
                $("[name=tipoDesligamento]").prop('checked', false);
                $('#motivoDesligamento').val("");
                $('#desligamento-modal').modal('show');

                var arrAluno = __matriculaAluno.options.data.alunoCurso;

                $(".qtdTitulosAbertosBiblioteca").text(
                    arrAluno ? arrAluno['qtdTitulosAbertosBiblioteca'] : '-'
                );
                $(".quantidadeTitulosVencidos").text(
                    arrAluno ? arrAluno['quantidadeTitulosVencidos'] : '-'
                );
                $(".qtdTitulosAbertosAcademicos").text(
                    arrAluno ? arrAluno['qtdTitulosAbertosAcademicos'] : '-'
                );
                $(".qtdTitulosAbertosTodos").text(
                    arrAluno ? arrAluno['quantidadeTitulosAbertos'] : '-'
                );
            });
            __matriculaAluno.recarregarInformacoesAluno();
        }
    };

    $.matriculaAluno = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.aluno");

        if (!obj) {
            obj = new MatriculaAluno();
            obj.run(params);
            $(window).data('universa.matricula.aluno', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);