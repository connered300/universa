(function ($, window, document) {
    'use strict';
    var AcadgeralCadastroOrigemIndex = function () {
        VersaShared.call(this);
        var __acadgeralCadastroOrigemIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                acadgeralCadastroOrigem: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __acadgeralCadastroOrigemIndex.options.datatables.acadgeralCadastroOrigem =
                $('#dataTableAcadgeralCadastroOrigem').dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __acadgeralCadastroOrigemIndex.options.url.search,
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;
                            var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary acadgeralCadastroOrigem-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger acadgeralCadastroOrigem-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                            for (var row in data) {
                                data[row]['acao'] = btnGroup;
                            }

                            __acadgeralCadastroOrigemIndex.removeOverlay($('#container-acadgeral-cadastro-origem'));

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "origem_id", targets: colNum++, data: "origem_id"},
                        {name: "origem_nome", targets: colNum++, data: "origem_nome"},
                        {name: "origem_id", targets: colNum++, data: "acao"}
                    ],
                    "dom": "fr" +
                           "t" +
                           "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'asc']]
                });

            $('#dataTableAcadgeralCadastroOrigem').on('click', '.acadgeralCadastroOrigem-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __acadgeralCadastroOrigemIndex.options.datatables.acadgeralCadastroOrigem.fnGetData($pai);

                if (!__acadgeralCadastroOrigemIndex.options.ajax) {
                    location.href = __acadgeralCadastroOrigemIndex.options.url.edit + '/' + data['origem_id'];
                } else {
                    $.acadgeralCadastroOrigemAdd().pesquisaAcadgeralCadastroOrigem(
                        data['origem_id'],
                        function (data) { __acadgeralCadastroOrigemIndex.showModal(data); }
                    );
                }
            });


            $('#btn-acadgeral-cadastro-origem-add').click(function (e) {
                if (__acadgeralCadastroOrigemIndex.options.ajax) {
                    __acadgeralCadastroOrigemIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableAcadgeralCadastroOrigem').on('click', '.acadgeralCadastroOrigem-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __acadgeralCadastroOrigemIndex.options.datatables.acadgeralCadastroOrigem.fnGetData($pai);
                var arrRemover = {
                    origemId: data['origem_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de origem de cadastro "' + data['origem_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __acadgeralCadastroOrigemIndex.addOverlay(
                                $('#container-acadgeral-cadastro-origem'),
                                'Aguarde, solicitando remoção de registro de origem de cadastro...'
                            );
                            $.ajax({
                                url: __acadgeralCadastroOrigemIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __acadgeralCadastroOrigemIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de origem de cadastro:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __acadgeralCadastroOrigemIndex.removeOverlay($('#container-acadgeral-cadastro-origem'));
                                    } else {
                                        __acadgeralCadastroOrigemIndex.reloadDataTableAcadgeralCadastroOrigem();
                                        __acadgeralCadastroOrigemIndex.showNotificacaoSuccess(
                                            "Registro de origem de cadastro removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#acadgeral-cadastro-origem-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['origemId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['origemId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $('#acadgeral-cadastro-origem-modal .modal-title').html(actionTitle + ' origem de cadastro');
            $('#acadgeral-cadastro-origem-modal').modal();
        };

        this.reloadDataTableAcadgeralCadastroOrigem = function () {
            this.getDataTableAcadgeralCadastroOrigem().api().ajax.reload(null, false);
        };

        this.getDataTableAcadgeralCadastroOrigem = function () {
            if (!__acadgeralCadastroOrigemIndex.options.datatables.acadgeralCadastroOrigem) {
                if (!$.fn.dataTable.isDataTable('#dataTableAcadgeralCadastroOrigem')) {
                    __acadgeralCadastroOrigemIndex.options.datatables.acadgeralCadastroOrigem =
                        $('#dataTableAcadgeralCadastroOrigem').DataTable();
                } else {
                    __acadgeralCadastroOrigemIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __acadgeralCadastroOrigemIndex.options.datatables.acadgeralCadastroOrigem;
        };

        this.run = function (opts) {
            __acadgeralCadastroOrigemIndex.setDefaults(opts);
            __acadgeralCadastroOrigemIndex.setSteps();
        };
    };

    $.acadgeralCadastroOrigemIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-cadastro-origem.index");

        if (!obj) {
            obj = new AcadgeralCadastroOrigemIndex();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-cadastro-origem.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);