(function ($, window, document) {
    'use strict';

    var AcadgeralCadastroOrigemAdd = function () {
        VersaShared.call(this);
        var __acadgeralCadastroOrigemAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#acadgeral-cadastro-origem-wizard',
            formElement: '#acadgeral-cadastro-origem-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __acadgeralCadastroOrigemAdd.options.validator.settings.rules = {origemNome: {maxlength: 45},};
            __acadgeralCadastroOrigemAdd.options.validator.settings.messages =
            {origemNome: {maxlength: 'Tamanho máximo: 45!'},};

            $(__acadgeralCadastroOrigemAdd.options.formElement).submit(function (e) {
                var ok = $(__acadgeralCadastroOrigemAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__acadgeralCadastroOrigemAdd.options.formElement);

                if (__acadgeralCadastroOrigemAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __acadgeralCadastroOrigemAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __acadgeralCadastroOrigemAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__acadgeralCadastroOrigemAdd.options.listagem) {
                                    $.acadgeralCadastroOrigemIndex().reloadDataTableAcadgeralCadastroOrigem();
                                    __acadgeralCadastroOrigemAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#acadgeral-cadastro-origem-modal').modal('hide');
                                }

                                __acadgeralCadastroOrigemAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAcadgeralCadastroOrigem = function (origem_id, callback) {
            var $form = $(__acadgeralCadastroOrigemAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __acadgeralCadastroOrigemAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + origem_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    __acadgeralCadastroOrigemAdd.removeOverlay($form);

                    if (Object.keys(data).length == 0) {
                        __acadgeralCadastroOrigemAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }
                },
                erro: function () {
                    __acadgeralCadastroOrigemAdd.showNotificacaoDanger('Erro desconhecido!');
                    __acadgeralCadastroOrigemAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.acadgeralCadastroOrigemAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-cadastro-origem.add");

        if (!obj) {
            obj = new AcadgeralCadastroOrigemAdd();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-cadastro-origem.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);