/**
 * Created by eduardo on 12/28/16.
 */

var arrId = [];

$(document).ready(function () {
    campusSelect();
    $('#pesquisaCampus').change(function () {
        $('#curso').show();
        var dados = $('#pesquisaCampus').select2('data');
        cursoSelect(dados.camp_id);
        $('#grade').hide();
    });

    $('#pesquisaCurso').change(function () {
        $('#periodo').show();
        periodoSelect();
        $('#grade').hide();
    });

    $('#pesquisaPeriodo').change(function () {
        $('#turma').show();
        var dados = $(this).select2('data');
        turmaSelect(dados.per_id);
        $('#grade').hide();
    });

    $('#pesquisaTurma').change(function () {
        arrId = [];
        $('#grade').show();
        var dados = $('#pesquisaTurma').select2('data');
        criaTabela(dados);
        changeHeader(dados);
    });

    tooltip();

    $('#salvar').click(function () {
        var dados = $('#pesquisaTurma').select2('data');
        salvarDados(dados);
    });

    $(document).on('click','.excluir',function(){
        var id = "#" + $(this).attr("id");
        $(id).select2("val","");
    });

    $("#limparSim").click(function(){
        $('.disciplinas').select2("val","");
    })
});

function tooltip() {
    $(document).on('mouseover', "#semanaGrade td", function () {
        var campo = $(this).find('input.disciplinas');
        var select2 = $(this).find('.select2-container.disciplinas');
        var campoDisc = $(this).find('.campoDisc');

        if (campo) {
            var data = campo.select2('data') || {};
            var title = (data['text'] || '') + '';
            $(select2).attr('title', title.trim());
        }
    });
}

function exibeMensagem(dados) {
    var icone = '';
    if (dados) {
        icone = '&nbsp;&nbsp;<span class="glyphicon glyphicon-pencil txt-color-yellow"></span>';
        showNotificacao({
            type: 'custom',
            title: "Editando Grade",
            content: "Grade já foi definida",
            timeout: 3000
        });
        $('.cabecalho h4').append(icone);
    }else{
        icone = '&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up txt-color-green"></span>';
        showNotificacao({
            type: 'custom',
            title: "Criando Grade",
            content: "Grade ainda não definida",
            timeout: 3000
        });
        $('.cabecalho h4').append(icone);
    }
}

/*  THE FUNCTION BELOW JUST TURN THE INPUTS INTO SELECT2 OBJECTS */

function turmaSelect(per_id) {
    $('#pesquisaTurma').select2({
        ajax: {
            url: window.buscaTurma,
            dataType: 'json',
            data: function (query) { return {query: query, perId: per_id,  campusCurso: $('#pesquisaCurso').select2('data').cursocampus_id};},
            results: function (data) {
                var transformed = $.map(data, function (el) {
                    el.text = el.turma_nome;
                    el.id = el.turma_id;

                    return el;
                });

                return {results: transformed};
            }
        },
    });
}

function discSelect(id, dados, turma_id) {
        $('#' + id).select2({
            initSelection: function (element, callback) {
                var data = {id: dados.disc_id, text: dados.disc_nome};
                callback(data);
            },
            ajax: {
                url: window.buscaDisciplina,
                dataType: 'json',
                data: function (query) {
                    var arrTurma=$('#pesquisaTurma').select2('data')||{};
                    return {query: query, turmaId: arrTurma['id']};
                },
                results: function (data) {
                    var transformed = $.map(data, function (el) {
                        el.text = el.disc_nome;
                        el.id = el.disc_id;

                        return el;
                    });

                    return {results: transformed};
                }
            },
            width: "resolve"
        });
}

function discSelect2(id, turma_id) {
        $('#' + id).select2({
            ajax: {
                url: window.buscaDisciplina,
                dataType: 'json',
                data: function (query) {
                    var arrTurma=$('#pesquisaTurma').select2('data')||{};
                    return {query: query, turmaId: arrTurma['id']};
                },
                results: function (data) {
                    var transformed = $.map(data, function (el) {
                        el.text = el.disc_nome;
                        el.id = el.disc_id;

                        return el;
                    });

                    return {results: transformed};
                }
            },
            width: "resolve"
        });
}

function cursoSelect(camp_id) {
    $('#pesquisaCurso').select2({
        ajax: {
            url: window.buscaCurso,
            dataType: 'json',
            data: function (query) { return {query: query, campId: camp_id};},
            results: function (data) {
                var transformed = $.map(data, function (el) {
                    el.text = el.curso_nome;
                    el.id = el.curso_id;

                    return el;
                });

                return {results: transformed};
            }
        }
    });
    defaultValueSelect2(window.buscaCurso, "pesquisaCurso", "curso_nome", "curso_id");
}

function periodoSelect() {
    $('#pesquisaPeriodo').select2({
        ajax: {
            url: window.buscaPeriodo,
            dataType: 'json',
            data: function (query) { return {query: query};},
            results: function (data) {
                var transformed = $.map(data, function (el) {
                    el.text = el.per_nome;
                    el.id = el.per_id;

                    return el;
                });

                return {results: transformed};
            }
        }
    });
    defaultValueSelect2(window.buscaPeriodo, "pesquisaPeriodo", "per_nome", "per_id");
}

function campusSelect() {
    $('#pesquisaCampus').select2({
        ajax: {
            url: window.buscaCampus,
            dataType: 'json',
            data: function (query) { return {query: query};},
            results: function (data) {
                var transformed = $.map(data, function (el) {
                    el.text = el.camp_nome;
                    el.id = el.camp_id;

                    return el;
                });

                return {results: transformed};
            }
        },
    });
    defaultValueSelect2(window.buscaCampus, "pesquisaCampus", "camp_nome", "camp_id");
}

/*  END */

/*  THE FUNCTION BELOW GET THE FIRST VALUE FROM DATABASE THEN PUT INTO SPECIFIC DESTINATION
 *   URL (AJAX)
 *   id  ID FROM INPUT
 *   text TEXT FROM ELEMENT TO SET
 *   ID  ID FROM ELEMENT TO SET*/

function defaultValueSelect2(url, id, text, ID) {
    $.ajax(url, {
        data: {query: ''},
        dataType: "json"
    }).done(function (data) {
        var transformed = $.map(data, function (el) {
            el.text = el[text];
            el.id = el[ID];

            return el;
        });

        $('#' + id).select2('data', transformed[0] || null);
        $('#' + id).trigger('change');
    });
}

/*
 CRIA A TABELA DINAMICAMENTE DE ACORDO COM OS DADOS CADASTRADOS NA TABELA ACADGERAL_HORARIO_GRADE__PADRAO
 */

function criaTabela(dados) {
    $('table#semanaGrade').empty();

    $('table#semanaGrade').append('<thead><tr></tr></thead><tbody></tbody><tfoot><thead><tr></tr></thead>');

    $.ajax({
        url: window.buscaGrade,
        dataType: 'json',
        type: 'post',
        data: {cursocampus_id: dados.cursocampus_id},
        success: function (data) {
            var dias = tableHead(data, 'thead');
            tableBody(data, dados, dias);
        }
    });
}

var salvarDados = function (turma) {
    var dados = [];
    $('.disciplinas').each(function (e, index) {
        if ($(this).select2("data") != null) {
            dados[($(this).attr("id")).replace("s2id_", "")] = $(this).select2("data").id;
        }
    });

    dados = $.extend({}, dados);

    $.ajax({
        url: window.salvar,
        method: 'post',
        dataType: 'json',
        data: {dados: dados, turma: turma.turma_id},
        success: function (data) {
            if (data.result['type'] == "success") {
                showNotificacao({
                    type: 'success',
                    content: data.result['message'],
                    timeout: 3000
                });
            }else{
                showNotificacao({
                    type: 'warning',
                    content: data.result['message'],
                    timeout: 3000
                });
            }
        }
    });
};

/*  WRONG NAME, THIS FUNCTION CHANGE HEADER WHEN TURMA CHANGES */

function changeHeader(dados) {
    var not = $('#noturno');
    var vep = $('#vespertino');
    var mat = $('#matutino');

    if (dados.turma_turno == "Noturno") {
        not.show();
        $('#noturno h4').html(dados.turma_nome);
        vep.hide();
        mat.hide();
    } else if (dados.turma_turno == "Matutino") {
        mat.show();
        $('#matutino h4').html(dados.turma_nome);
        vep.hide();
        not.hide();
    } else {
        vep.show();
        $('#vespertino h4').html(dados.turma_nome);
        not.hide();
        mat.hide();
    }
}

/*
 CRIA O CABEÇALHO DA TABELA DINAMICAMENTE
 */

function tableHead(array) {
    var dia = '';

    array.forEach(function(e){
        var atual = e['horariograde_dia'];
        if(atual.length > dia.length){
            dia = atual;
        }
    });

    var semana = dia.split(",");
    semana.unshift('Horário');

    for (var index in semana) {
        $('#semanaGrade > thead > tr').append('<th class="titulo">' + semana[index] + '</th>');
    }

    return parseInt(index);
}

/*
 CRIA O CORPO DA TABELA DINAMICAMENTE
 */

function tableBody(array, turma, dias) {
    var dia = '';

    array.forEach(function(e){
        var atual = e['horariograde_dia'];
        if(atual.length > dia.length){
            dia = atual;
        }
    });

    var semana = dia.split(",");
    var arrId = [];
    var id = [];
    var result = [];
    var index = null;

    semana.forEach(function () {
        for (var index in array) {
            var horario = (array[index]['horariograde_padrao'].replace(":", "_")).replace(":", "_");
            arrId.push(horario);
        }
    });

    // ESSE TRECHO DE CÓDIGO PEGA O ARRAY PASSADO E REMOVE DUPLICAÇÕES
    $.each(arrId, function (i, el) {
        if ($.inArray(el, result) === -1) {
            result.push(el);
        }
    });
    // END

    for (var i in result) {
        for (var a in semana) {
            id.push(semana[a] + result[i]);
        }
    }

    array.forEach(function (dado) {
        var hora = parseInt(dado['horariograde_padrao']['0'] + dado['horariograde_padrao']['1']);
        var intervalo = dado['horariograde_tipo'] == "intervalo";

        if (hora > 6 && hora < 12) {
            $('div#matutino table#semanaGrade').append('<tr><td>' + dado['horariograde_padrao'] + '</td>' +
                                                       criaTd(dias, id, index, intervalo) + '</tr>');
        } else if (hora >= 12 && hora < 18) {
            $('div#vespertino table#semanaGrade').append('<tr><td>' + dado['horariograde_padrao'] + '</td>' +
                                                         criaTd(dias, id, index, intervalo) + '</tr>');
        } else {
            $('div#noturno table#semanaGrade').append('<tr><td>' + dado['horariograde_padrao'] + '</td>' +
                                                      criaTd(dias, id, index, intervalo) + '</tr>');
        }

        index += dias;
    });

    preencheDisciplinas(turma);

    $(".select2Element").attr("style", "width:100%");
}

/*  THIS FUNCTION MAKES TDS WITH INPUT SETTING IDS AND ELEMENTS DYNAMICALLY ACCORDING TO DAY QUANTITY ON GRADE AND CLASS SCHEDULE */

function criaTd(tamanho, array, j, intervalo) {
    var body = '';

    for (var i = 0; i < tamanho; i++) {
        if(intervalo){
            body += '<td><div class="campoDisc"><span class="glyphicon glyphicon-pause txt-color-red intervaloIcn" aria-hidden="true" ></span>&nbsp;<input type="text" class="' + "intervalo"+ '" class="disciplinas" disabled value="INTERVALO"></div></td>';
        }else {
            body += '<td><div class="campoDisc"><input type="text" id="' + array[j + i] + '" class="disciplinas">&nbsp;<i id="' + array[j + i] + '" class="glyphicon glyphicon-trash txt-color-red excluir" aria-hidden="true" ></i></div></td>';
            arrId.push(array[j + i]);
        }
    }

    return body;
}

/*  THIS FUNCTION VERIFIES IF THERE IS AT LEAST A DISC AND SET AS DEFAULT VALUE, IT DOES MAKE THE SELECT2 FIELDS TOO */

function preencheDisciplinas(array) {
    var id = [];
    var aviso = false;
    var turma = array.turma_id;

    $('.disciplinas').each(function () {
        id.push($(this).attr("id"));
    });

    var rawDisc = $.ajax({
        url: window.gradeDisciplina,
        method: 'post',
        dataType: 'json',
        async: false,
        data: {turma_id: array.turma_id},
        success: function (data) {
            return data;
        }
    }).responseText;

    var jsonDisc = JSON.parse(rawDisc);

    id.forEach(function (dados) {
        discSelect2(dados,turma);
    });

    jsonDisc.forEach(function (e) {
        var hora = e.gradedisc_hora;
        var result = false;
        var atual = false;

        if (hora != null) {
            result = (hora.replace(":", "_")).replace(":", "_");
            atual = e.gradedisc_dia + result;
        }

        if (atual != false) {
            var disc = {disc_id: e.disc_id, disc_nome: e.disc_nome};
            discSelect(atual, disc, turma.turma_id);

            if (e.disc_id) {
                aviso = true;
            }
        }
    });

    exibeMensagem(aviso);
}

/*  AS WE KNOWN BEFORE THIS FUNCTION GET SOME OPTIONS AND THEN MAKES AN ALERT */

function showNotificacao(param) {
    var options = $.extend(
        true,
        {
            content: '',
            title: '',
            type: 'info',
            icon: '',
            color: '',
            timeout: false
        },
        param
    );

    switch (options.type) {
        case 'danger':
            options.icon = 'fa-times-circle';
            options.title = options.title || 'Atenção:';
            options.color = '#a90329';
            break;
        case 'info':
            options.icon = 'fa-info-circle';
            options.title = options.title || 'Informação:';
            options.color = '#57889c';
            break;
        case 'success':
            options.icon = 'fa-check-circle';
            options.title = options.title || 'Sucesso:';
            options.color = '#739e73';
            break;
        case 'warning':
            options.icon = 'fa-exclamation-circle';
            options.title = options.title || 'Atenção:';
            options.color = '#c79121';
            break;
        case 'custom':
            options.icon = 'fa-exclamation-circle';
            options.title = options.title || '';
            options.color = '#2f2929';
            break;
    }

    var optionsNotification = {
        title: options.title,
        content: "<i>" + options.content + "</i>",
        color: options.color
    };

    if (options.timeout) {
        optionsNotification.timeout = options.timeout;
    }

    $.smallBox(optionsNotification);
}