(function ($, window, document) {
    'use strict';

    var RelatorioAlunoPorAgente = function () {
        VersaShared.call(this);
        var __relatorioAlunoPorAgente = this;
        this.defaults = {
            url: {},
            data: {},
            formElement: '#relatorio-aluno-por-agente'
        };

        this.steps = {};

        this.setSteps = function () {
            $('#alunocursoSituacao').select2({
                allowClear: true,
                placeholder: 'Situação',
                data: __relatorioAlunoPorAgente.options.data.arrAlunoSituacao
            });

            $('#pesIdAgente').select2({
                language: 'pt-BR',
                minimumInputLength: 1,
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioAlunoPorAgente.options.url.agente,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            var $form = $('#relatorio-aluno-por-agente');
            var validacao = $form.validate({ignore: '.ignore'});

            validacao.settings.rules = {
                dataInicio: {required: true},
                dataFinal: {required: true}
            };
            validacao.settings.messages = {
                dataInicio: {required: 'Campo obrigatório'},
                dataFinal: {required: 'Campo obrigatório'}
            };

            __relatorioAlunoPorAgente.iniciarElementoDatePicker($('#dataInicio, #dataFinal'));
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
        };
    };

    $.relatorioAlunoPorAgente = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.relatorio.aluno-por-agente");

        if (!obj) {
            obj = new RelatorioAlunoPorAgente();
            obj.run(params);
            $(window).data('universa.financeiro.relatorio.aluno-por-agente', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);