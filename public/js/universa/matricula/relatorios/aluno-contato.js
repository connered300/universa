(function ($, window, document) {
    'use strict';
    var AlunoContato = function () {
        VersaShared.call(this);
        var __alunoContato = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                add: '',
                edit: '',
                matriculaAlunoCurso: '',
                turma: '',
                relatorioAluno: ''
            },
            data: {
                arrSituacao: []
            },
            value: {}
            ,
            datatables: {
                acadperiodoAluno: null
            }
        };

        var $campus = $('#campus');
        var $perId = $('#perId');
        var $curso = $('#curso');
        var $turma = $('#turma');
        var $turno = $('#turno');
        var $situacao = $('#situacao');
        var $serie = $('#serie');

        this.setSteps = function () {

            var arrTurno = [
                {'id': 'Matutino', 'text': 'Matutino'},
                {'id': 'Vespertino', 'text': 'Vespertino'},
                {'id': 'Noturno', 'text': 'Noturno'},
                {'id': 'Integral', 'text': 'Integral'}];

            $situacao.select2({
                allowCelar: true,
                multiple: true,
                data: __alunoContato.options.data.arrSituacao
            });

            $turno.select2({
                allowCelar: true,
                multiple: true,
                data: arrTurno
            });

            $perId.select2({
                allowClear: true,
                ajax: {
                    url: __alunoContato.options.url.periodo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.per_nome, id: el.per_id};
                        });

                        transformed.push({text: 'Sem Período', id: 'null'});
                        transformed.push({text: 'Alunos Desligados', id: 'desligados'});

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $situacao.select2("enable");

                if ($perId.val() == 'desligados' || $perId.val() == 'null') {
                    if ($situacao.val()) {
                        __alunoContato.showNotificacaoInfo("Não é possivel selecionar situação para esse período!");
                    }

                    $situacao.select2("disable");
                }

                $campus.select2('val', '').trigger('change');
                $campus.select2($perId.val() ? 'enable' : 'disable');
            });

            $campus.select2({
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __alunoContato.options.url.campus,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var perId = $('#perId').select2('data') || {};
                        perId = perId['id'] || '';

                        return {
                            agruparPorCampus: true,
                            query: query,
                            perId: perId
                        }
                    },
                    results: function (data) {

                        var transformed = $.map(data, function (el) {
                            return {text: el.camp_nome, id: el.camp_id};
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $curso.select2('val', '').trigger('change');
                $curso.select2($campus.val() ? 'enable' : 'disable');
            });

            $curso.select2({
                allowClear: true,
                ajax: {
                    url: __alunoContato.options.url.curso,
                    dataType: 'json',
                    delay: 250,
                    data: function (e) {
                        var perId = $('#perId').select2('data') || {};
                        perId = perId['id'] || '';

                        if (perId['id'] == "desligados") {
                            perId = '';
                        }

                        return {
                            perId: perId,
                            cursoPossuiPeriodoLetivo: 'Sim',
                            campId: $campus.val()
                        }
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {
                                text: el.curso_nome,
                                id: el.curso_id,
                                cursocampus: el.cursocampus_id,
                                serie: el.curso_prazo_integralizacao

                            };
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $turma.select2('val', '').trigger('change');
                $turma.select2($curso.val() ? 'enable' : 'disable');

                $situacao.select2("data", []);
                $serie.select2({"data": []});
                $serie.select2("disable", true);

                if ($curso.val()) {
                    $serie.select2("enable");

                    var serie = ($curso.select2("data"))['serie'];
                    var arrSerie = [];
                    var x = 1;

                    for (x; x <= serie; x++) {
                        arrSerie[x - 1] = {'id': x, 'text': JSON.stringify(x)}
                    }
                    $serie.select2({
                        allowCelar: true,
                        multiple: true,
                        "data": arrSerie
                    });

                    $serie.select2("val", '').trigger("change");
                }
            });

            $turma.select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __alunoContato.options.url.turma,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var perId = $('#perId').select2('data') || {};
                        perId = perId['id'] || '';

                        return {
                            query: query,
                            curso: $curso.val(),
                            perId: perId,
                            turmaSerie: $serie.val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {

                            el.id = el.turma_id;
                            el.text = el.turma_nome;
                            return el;
                        });
                        return {results: transformed};
                    }
                }
            }).on("change", function () {
                $turno.select2("val", "").trigger("change");
                $situacao.select2("val", "").trigger("change");

                $turno.select2($turma.val() ? 'enable' : "disable");
                $situacao.select2($turma.val() ? 'enable' : "disable");

            });

            $serie.select2({
                allowCelar: true,
                multiple: true,
                data: []
            }).on("change", function () {
                $turma.select2("val", '').trigger("change");

                $turma.select2($serie.val() ? 'enable' : 'disable');
            });

            $("#relatorio-aluno-contato").on("submit", function (e) {
                var ok = __alunoContato.validaRelatorio(__alunoContato.retornaValorCampos());

                if (!ok) {
                    e.stopImmediatePropagation();
                    return false;
                }
            });

            $('#alunosFiltroLimpar').click(__alunoContato.limparCampos);

            $campus.select2($perId.val() ? 'enable' : 'disable');
            $curso.select2($campus.val() ? 'enable' : 'disable');
            $serie.select2($curso.val() ? 'enable' : 'disable');
            $turma.select2($serie.val() ? 'enable' : 'disable');
            $turno.select2($turma.val() ? 'enable' : 'disable');
            $situacao.select2($turma.val() ? 'enable' : 'disable');

        };

        this.limparCampos = function () {
            $campus.select2('val', '').trigger('change');
            $curso.select2('val', '').trigger('change');
            $turma.select2('val', '').trigger('change');
            $perId.select2('val', '').trigger('change');
            $turma.select2('val', '').trigger('change');
            $serie.select2('val', '').trigger('change');
            $situacao.select2('val', '').trigger('change');
        };

        this.retornaValorCampos = function () {
            var arrFiltros = {};

            if ($perId.val()) {
                arrFiltros.perId = $perId.val();
            }

            if ($campus.val()) {
                arrFiltros.campus = $campus.val();
            }

            if ($turno.val()) {
                arrFiltros.turno = $turno.val();
            }

            if ($curso.val()) {
                arrFiltros.curso = $curso.val();
            }

            if ($turma.val()) {
                arrFiltros.turma = $turma.val();
            }

            if ($serie.val()) {
                arrFiltros.serie = $serie.val();
            }

            if ($situacao.val()) {
                arrFiltros.situacao = $situacao.val();
            }

            return arrFiltros;
        };

        this.validaRelatorio = function (arrDados) {
            var retorno = true;
            var msgErro = '';

            if (!arrDados['perId']) {
                msgErro += ' Período';
                retorno = false;
            }
            if (!arrDados['campus']) {

                msgErro = msgErro ? msgErro + ',Campus' : 'Campus';
                retorno = false;
            }

            if (!arrDados['curso'] && (arrDados['perId'] != "desligados" && arrDados['perId'] != "null")) {
                msgErro = msgErro ? msgErro + ',Curso' : 'Curso';
                retorno = false;
            }

            if (!retorno) {
                __alunoContato.showNotificacaoInfo('Favor preencher o(s) campo(s): ' + msgErro + "!");
            }
            return retorno;
        };

        this.run = function (opts) {
            __alunoContato.setDefaults(opts);
            __alunoContato.setSteps();
        };
    };

    $.alunoContato = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.relatorios.aluno-contato");

        if (!obj) {
            obj = new AlunoContato();
            obj.run(params);
            $(window).data('universa.matricula.relatorios.aluno-contato', obj);
        }

        return obj;
    };
})
(window.jQuery, window, document);