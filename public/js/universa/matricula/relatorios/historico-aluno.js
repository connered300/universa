(function ($, window, document) {
    'use strict';

    var HistoricoAluno = function () {
        VersaShared.call(this);
        var __historicoAluno = this;
        this.defaults = {
            url: {
                campus: '',
                alunoCurso: '',
                historicoAluno: ''
            },
            data: {
                arrCampusSelect: []
            },
            value:{
                sumarizadorAtivo:'naoExibeSumarizador'
            },
            formElement: '#historicoAluno'
        };

        this.steps = {};

        this.setSteps = function () {
            var matricula = $("#matricula"),
                campus = $("#campusId"),
                observacaoHistorico = $("#observacaoHistorico"),
                observacaoEnade = $("#observacaoEnade"),
                sumarizador = $("#historicoSumarizador");

            $("#formatoHistorico").select2();
            $("#historicoCabecalho").select2();
            $("#gerarHistorico").prop("disabled", true);
            $("#salvaObservacao").prop("disabled", true);

debugger;
            sumarizador.select2();
            sumarizador.select2('val', __historicoAluno.options.value.sumarizadorAtivo).trigger("change");

            matricula.select2({
                language: 'pt-BR',
                ajax: {
                    url: __historicoAluno.options.url.alunoCurso,
                    dataType: 'json',
                    type: 'POST',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.alunocurso_id;
                            el.text = el.pes_nome + ' | Curso: ' + el.curso_nome;

                            return el;
                        });
                        return {results: transformed};
                    }
                }
            }).on("change", function () {
                var arrDados = $("#matricula").select2("data");
                $("#salvaObservacao").prop("disabled", false);

                if (arrDados) {
                    $("#gerarHistorico").prop("disabled", false);
                }

                observacaoEnade.val(arrDados['alunocurso_enade']);
                observacaoHistorico.val(arrDados['alunocurso__observacao_historico']);

                $("#salvarHistoricos").val(0);
                campus.select2("val", arrDados['camp_id']);
            });

            campus.select2({'data': __historicoAluno.options.data.arrCampusSelect});

            $("#salvaObservacao").on("click", function () {
                var arrDados = $("#matricula").select2("data");

                if (arrDados['alunocurso_enade'] == observacaoEnade.val() &&
                    arrDados['alunocurso__observacao_historico'] == observacaoHistorico.val()) {
                    __historicoAluno.showNotificacaoInfo("Não há alterações para ser Salvas!");

                    return false;
                }

                __historicoAluno.addOverlay($(".widget-body"));

                $.ajax({
                    url: __historicoAluno.options.url.historicoAluno,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        matricula: arrDados['matricula'],
                        observacaoEnade: observacaoEnade.val(),
                        observacaoHistorico: observacaoHistorico.val(),
                        salvarHistoricos: true
                    },
                    success: function (data) {
                        if (!data['erro']) {
                            __historicoAluno.showNotificacaoInfo("Observacão salva com sucesso!");

                            matricula.select2("data")['alunocurso_enade'] = observacaoEnade.val();
                            matricula.select2("data")['alunocurso__observacao_historico'] = observacaoHistorico.val();

                        } else {
                            __historicoAluno.showNotificacaoDanger("Erro ao salvar a observação: " + data['erro']);
                        }
                    }

                }).done(function () {
                    __historicoAluno.removeOverlay($(".widget-body"));
                });
            });

            $("#gerarHistorico").on('click', function (e) {
                var arrDados = $("#matricula").select2("data"),
                    aguardarEscolha = false;

                if (!matricula.val()) {
                    __historicoAluno.showNotificacaoWarning("Nenhum aluno selecionado!");

                    return false;
                }

                if (!campus.val()) {
                    __historicoAluno.showNotificacaoWarning("Selecione um campus!");

                    return false;
                }

                var alteracaoObservacaoEnade =
                    arrDados['alunocurso_enade'] != observacaoEnade.val();
                var alteracaoObservacaoHistorico =
                    arrDados['alunocurso__observacao_historico'] != observacaoHistorico.val();

                if (alteracaoObservacaoEnade || alteracaoObservacaoHistorico) {
                    aguardarEscolha = true;

                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: "Há alteração no campo de observação, deseja salvar?",
                        buttons: "[Não][Sim]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Sim") {
                            $("#salvarHistoricos").val(1);
                        }

                        aguardarEscolha = false;
                        matricula.select2("data")['alunocurso_enade'] = observacaoEnade.val();
                        matricula.select2("data")['alunocurso__observacao_historico'] = observacaoHistorico.val();

                        $("#historicoAluno").submit();
                    });
                }
                if (aguardarEscolha) {
                    return false;
                }

                matricula.select2("data")['alunocurso_enade'] = observacaoEnade.val();
                matricula.select2("data")['alunocurso__observacao_historico'] = observacaoHistorico.val();

                $("#historicoAluno").submit();

            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
        };
    };

    $.historicoAluno = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.relatorios.form-historico-aluno");

        if (!obj) {
            obj = new HistoricoAluno();
            obj.run(params);
            $(window).data("universa.matricula.relatorios.form-historico-aluno", obj);
        }

        return obj;
    };
})(window.jQuery, window, document);