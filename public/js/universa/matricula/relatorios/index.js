$(document).ready(function () {
    'use strict';
    var vs = new VersaShared();

    $('#campus').select2({
        ajax: {
            url: window.urlCampus,
            dataType: 'json',
            data: function (query) {
                return {
                    query: query
                };
            },
            results: function (data) {
                var resultado = $.map(data, function (elements) {
                    elements.text = elements.camp_nome;
                    elements.id = elements.camp_id;

                    return elements;
                });

                return {
                    results: resultado
                };
            }
        },
        multiple: true
    });

    $('#areaPesquisa').select2({
        ajax: {
            url: window.urlArea,
            dataType: 'json',
            data: function (query) {
                return {
                    query: query,
                };
            },
            results: function (data) {
                var resultado = $.map(data, function (elements) {
                    elements.text = elements.area_descricao;
                    elements.id = elements.area_id;

                    return elements;
                });

                return {
                    results: resultado
                };
            }
        },
        multiple: true
    });

    $('#turnoPesquisa').select2({
        data: window.turno,
        language: 'pt-BR',
        multiple: true
    });

    $('#modalidadePesquisa').select2({
        data: window.modalidade,
        language: 'pt-BR',
        multiple: true
    });

    $('#nivelPesquisa').select2({
        data: window.nivel,
        language: 'pt-BR',
        multiple: true
    });

    $("#periodoLetivo").change(function (e) {
        adicionaOvelay();
        buscaTurmasPeriodo();
        $("#turmas").change();
    });


    $("#turmas").on("change", function (e) {
        adicionaOvelay();
        buscaDisciplinasPeriodo();
        e.stopImmediatePropagation();
        e.preventDefault();
    });
    

    $("#campus_curso").on("change", function () {
        $("#periodoLetivo").change();
    });

    $("#relatorio").click(function () {
        if ($("select[name = 'relatorio']").val() == 'por-turma') {
            $("#turma").show();
            $("#periodoTurno").val("");
            $("#periodoSerie").val("Todos");
            $("#turno").hide();
            $("#serie").hide();
            $("#turmasIrregulares").hide();

        } else {
            alert("Atenção: O tempo para geração deste relatório para este tipo de filtro pode ser consideravelmente alto.");
            $("#turma").hide();
            $("#turno").show();
            $("#serie").show();
            $("#turmasIrregulares").show();

        }
    });

    $("#tipoRelatorio").click(function () {
        if ($("select[name = 'tipo']").val() === 'alunoFinal') {
            $("#tipoAluno").hide();
        } else {
            $("#tipoAluno").show();
        }
    });

    $("#tipo").click(function () {
        if ($("select[name = 'tipo']").val() == 'matricula/relatorios/cabecalho-gabarito') {
            alert('Selecione o dia da prova');
            $("#dia").show();
        } else {
            $("#dia").hide();
        }
    });
    /*-------------------------------CHAMADA DOS FORMULARIOS PARA OS VARIADOS TIPOS DE RELATÓRIOS---------------------------*/

    function buscaTurmasPeriodo() {
        var options = '';
        $.ajax({
            url: "/boleto-bancario/financeiro-relatorios/busca-turmas",
            dataType: 'json',
            type: 'post',
            async: false,
            data: {
                periodoId: $("select[name = 'periodoLetivo']").val(),
                campusCursosId: $("select[name = 'campus_curso']").val()
            },
            success: function (data) {
                vs.removeOverlay($(".widget-body"));

                if (data.dados.length > 0) {
                    var i;
                    for (i = 0; i < data.dados.length; i++) {
                        options += '<option data-matriz="' + data.dados[i]['matCur'] + '" data-serie="' + data.dados[i]['turmaSerie'] + '" value="' + data.dados[i]['turmaId'] + '">' + data.dados[i]['turmaNome'] + '</option>';
                        $("select[name='turmas']").val(data.dados[i]['turmaNome']);
                    }
                    $("#turmas").html(options);
                    document.getElementById("turmas").disabled = false;
                }
            },
            erro: function () {
                vs.removeOverlay($(".widget-body"));
                vs.showNotificacaoWarning('Erro ao buscar anotações! Verifique sua conexão com a internet.');
            }
        });
    }

    function buscaDisciplinasPeriodo() {
        var options = '';
        var $turmasSelecionadas = $("select[name='turmas'] option:selected ");

        $.ajax({
            url: "/matricula/relatorios/busca-disciplina-turma-periodo",
            dataType: 'json',
            type: 'post',
            async: false,
            data: {
                matId: parseInt($turmasSelecionadas.attr('data-matriz')),
                perId: $("select[name = 'periodoLetivo']").val(),
                serie: parseInt($turmasSelecionadas.attr('data-serie')),
            },
            success: function (data) {
                var dados = data.dados || [];

                vs.removeOverlay($(".widget-body"));

                if (dados.length > 0) {
                    var i;

                    for (i = 0; i < data.dados.length; i++) {
                        options += '<option value="' + data.dados[i]['disc_id'] + '">' + data.dados[i]['disc_nome'] + '</option>';
                        $("select[name='disciplina']").val(data.dados[i]['disc_nome']);
                    }

                    $("#disciplina").html(options);
                    document.getElementById("disciplina").disabled = false;
                }
            },
            erro: function () {
                vs.removeOverlay($(".widget-body"));
                vs.showNotificacaoWarning('Erro ao buscar anotações! Verifique sua conexão com a internet.');
            }
        });
    }

    function adicionaOvelay() {
        return $(".app-overlay").is(":visible") ? '' : vs.addOverlay($(".widget-body"));
    }
});