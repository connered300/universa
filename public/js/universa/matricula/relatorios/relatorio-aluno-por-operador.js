(function ($, window, document) {
    'use strict';

    var RelatorioAlunoPorOperador = function () {
        VersaShared.call(this);
        var __relatorioAlunoPorOperador = this;
        this.defaults                   = {
            url: {
                acessoPessoas: ''
            },
            data: {},
            formElement: '#relatorio-aluno-por-operador'
        };

        this.steps = {};

        this.setSteps = function () {
            $('#situacaoaluno').select2({
                allowClear: true,
                placeholder: 'Escolha uma situação',
                data: __relatorioAlunoPorOperador.options.data.arrAlunoSituacao
            });

            $('#operadormult').select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __relatorioAlunoPorOperador.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_nome + ' | ' + el.login, id: el.id};
                        });

                        return {results: transformed};
                    }

                }
            });

            $("#limpar").click(function () {
                $('#operadormult').select2('val', '').trigger('change');
                $('#situacaoaluno').select2('val', '').trigger('change');
                $('#datacadastroinicial').val('');
                $('#datacadastrofinal').val('');

            });

            var $form     = $('#relatorio-aluno-por-operador');
            var validacao = $form.validate({ignore: '.ignore'});

            validacao.settings.rules    = {
                datacadastroinicial: {required: true},
                datacadastrofinal: {required: true}
            };
            validacao.settings.messages = {
                datacadastroinicial: {required: 'Campo obrigatório'},
                datacadastrofinal: {required: 'Campo obrigatório'}
            };

            __relatorioAlunoPorOperador.iniciarElementoDatePicker($('#datacadastroinicial,#datacadastrofinal'));

        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
        };
    };

    $.relatorioAlunoPorOperador = function (params) {
        params = params || [];

        var obj = $(window).data("universa.financeiro.relatorio.aluno-por-operador");

        if (!obj) {
            obj = new RelatorioAlunoPorOperador();
            obj.run(params);
            $(window).data('universa.financeiro.relatorio.aluno-por-operador', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);