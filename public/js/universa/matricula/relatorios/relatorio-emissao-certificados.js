(function ($, window, document) {
    'use strict';

    var RelatorioEmissaoCertificados = function () {
        VersaShared.call(this);
        var __relatorioEmissaoCertificados = this;

        this.defaults = {
            url: {
                pesquisaAlunocurso: ''
            },
            data: {
                arrCertificadoModelo: []
            },
            validator: null,
            formElement: '#relatorio-emissao-certificados'
        };

        this.steps = {};

        this.setSteps = function () {
            var $form = $(__relatorioEmissaoCertificados.options.formElement),
                $modeloCertificado = $('[name="certificadoModelo"]'),
                $alunocursoId = $('[name="alunocursoId"]');

            $modeloCertificado.select2({
                allowClear: true,
                data: __relatorioEmissaoCertificados.options.data.arrCertificadoModelo || []
            });

            $alunocursoId.select2({
                ajax: {
                    url: __relatorioEmissaoCertificados.options.url.pesquisaAlunocurso,
                    dataType: 'json',
                    data: function (query) {
                        return {
                            query: query
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {
                                id: el.alunocursoId,
                                text: el.nome + ' | ' + el.cursoNome + ' | CPF: ' + el.pesCpf
                            };
                        });

                        return {results: transformed};
                    }
                }
            });

            __relatorioEmissaoCertificados.options.validator.settings.rules = {
                alunocursoId: {required: true},
                certificadoModelo: {required: true}
            };
            __relatorioEmissaoCertificados.options.validator.settings.messages = {
                alunocursoId: {required: "Campo Obrigatório"},
                certificadoModelo: {required: "Campo Obrigatório"}
            };
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setValidations();
            this.setSteps();
        };
    };

    $.relatorioEmissaoCertificados = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.relatorio.emissao-certificados");

        if (!obj) {
            obj = new RelatorioEmissaoCertificados();
            obj.run(params);
            $(window).data('universa.matricula.relatorio.emissao-certificados', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);