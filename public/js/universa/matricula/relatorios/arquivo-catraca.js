(function ($, window, document) {
    'use strict';

    var ArquivoCatraca = function () {
        VersaShared.call(this);
        var __arquivoCatraca = this;
        this.defaults = {
            url: {},
            data: {},
            formElement: '#form-arquivo-catraca'
        };

        this.steps = {};

        this.setSteps = function () {
            var $matsituacao = $('#matsituacao'),
                $cursoCampus = $('#cursocampusId'),
                $periodo = $('#perId'),
                $turma = $('#turmaId');


            $turma.select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __arquivoCatraca.options.url.turma,
                    dataType: 'json',
                    type: 'POST',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            perId: $('#perId').val(),
                            cursocampusId: $('#cursocampusId').val()
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.turma_id;
                            el.text = el.turma_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $turma.select2('disable');

            $matsituacao.select2({
                allowClear: true,
                multiple: true,
                placeholder: 'Situação no período',
                data: __arquivoCatraca.options.data.arrAlunoSituacao
            });

            $cursoCampus.select2({
                allowClear: true,
                placeholder: 'Campus',
                data: __arquivoCatraca.options.data.arrCampus
            });

            $periodo.select2({
                allowClear: true,
                placeholder: 'Período',
                data: __arquivoCatraca.options.data.arrPer
            }).on('change',function(){
                $turma.select2('enable');
            });

            var $form = $(__arquivoCatraca.options.formElement);
            var validacao = $form.validate({ignore: '.ignore'});

            validacao.settings.rules = {
                cursocampusId: {required: true},
                perId: {required: true},
                matsituacao: {required: true}
            };
            validacao.settings.messages = {
                cursocampusId: {required: 'Campo obrigatório'},
                perId: {required: 'Campo obrigatório'},
                matsituacao: {required: 'Campo obrigatório'}
            }


            $form.on('submit',function(e){
                var ok = $(this).valid();
                var $overlay = $('#form-arquivo-catraca');

                if(ok){
                    __arquivoCatraca.addOverlay($overlay, 'Gerando Arquivo...');

                    e.stopPropagation();
                    e.preventDefault();

                    $.ajax({
                        url: $form.attr('action'),
                        dataType: 'json',
                        type: 'post',
                        data: $form.serializeJSON(),
                        success: function(el){
                            if(el.error){
                                __arquivoCatraca.showNotificacaoDanger(
                                    "Não há registros para gerar o arquivo"
                                );
                            }else {
                                var arquivo = new Blob([el.result], {type: 'text'});
                                var url = URL.createObjectURL(arquivo);
                                var a = document.createElement("a");

                                a.href = url;
                                a.download = "PedestreiControl.txt";
                                document.body.appendChild(a);
                                a.click();
                            }

                            __arquivoCatraca.removeOverlay($overlay);
                        }
                    });
                }
            })

            $("#cursosFiltroLimpar").click(function () {
                $('#cursocampusId,#perId,#turmaId,#matsituacao')
                    .select2('val', '')
                    .trigger('change');
            });

            $("#cursocampusId").change(function () {
                $('#perId').select2('val', '')
                $('#turmaId').select2('val', '')
                $('#turmaId').select2('disable', 'disable')
                .trigger('change');
            });

            $("#perId").change(function () {
                $('#turmaId').select2('val', '')
                .trigger('change');
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
        };
    };

    $.arquivoCatraca = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.relatorios.gera-arquivo-catraca");

        if (!obj) {
            obj = new ArquivoCatraca();
            obj.run(params);
            $(window).data("universa.matricula.relatorios.gera-arquivo-catraca", obj);
        }

        return obj;
    };
})(window.jQuery, window, document);