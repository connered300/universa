(function ($, window) {
    'use strict';

    var Matricula = function () {
        VersaShared.call(this);

        var __Matricula = this;

        this.defaults = {
            steps: [],
            validations: [],
            url: {
                financeiroTituloTipo: '',
                searchDisciplinas: '',
                agenteEducacional: '',
                processoSeletivo: '',
                searchAlunoCurso: '',
                cadastroOrigem: '',
                searchFormacao: '',
                unidadeEstudo: '',
                buscaFormacao: '',
                formaIngresso: '',
                campusCurso: '',
                modalidade: '',
                matricular: '',
                buscaCEP: '',
                periodo: '',
                campus: '',
                search: '',
                nivel: '',
                turma: ''
            },
            value: {
                formaDeIngressoPadrao: null
            },
            data: {
                arrFormacaoTipoEnsino: [],
                arrFormacaoTipo: [],
                arrFormacaoConcluido: [],
                arrFormacaoRegime: [],
                arrEtnia: [],
                tipoTitulo: [],
                arrEstados: [],
                dadosPessoa: [],
                formacao: {},
                arrNecessidadesEspeciais: [],
                cursoPossuiPeriodicidade: false
            },
            btnEnviar: '#btn-enviar',
            btnVoltar: '#btn-voltar',
            validator: null,
            formElement: '#form-matricula-inicio',
            wizardElement: '#matricula-wizard',
            select2CampusCurso: null,
            validarProcessoSeletivo: false
        };

        this.steps = {
            dadosBasicos: {
                init: function () {
                    var $form = $(__Matricula.options.formElement);

                    var $tiposelId = $("#tiposelId"),
                        $edicaoId = $("#edicaoId"),
                        $modId = $("#modId"),
                        $nivelId = $("#nivelId"),
                        $campusCurso = $("#cursocampusId"),
                        $perId = $("#perId"),
                        $pesNome = $("#pesNome"),
                        $turmaId = $("#turmaId"),
                        $unidadeId = $("#unidadeId"),
                        $cpf = $("[name=pesCpf]");

                    var $fieldsBloqueados =
                        $("#perId, #turmaId, #pesCpf, #pesDocEstrangeiro, #turmaSerie");

                    $cpf.inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['999.999.999-99']
                    });

                    __Matricula.cleanAllFields();

                    $(__Matricula.options.btnVoltar).on('click', function () {
                        location.href = __Matricula.options.url.matricular;
                    });

                    $(__Matricula.options.btnEnviar).on('click', function (e) {
                        var ok = __Matricula.validate();
                        var $overlay = $('.place-overlay');

                        e.stopPropagation();
                        e.preventDefault();

                        __Matricula.addOverlay($overlay,
                            'Enviando Informações...'
                        );

                        if (!ok) {
                            __Matricula.removeOverlay($overlay);

                            return false;
                        }

                        var dados = $form.serializeJSON();
                        var arrFormacoes = __Matricula.steps.formacao.getArrFormacao();

                        dados['campusCurso'] = $campusCurso.val();
                        dados['arrFormacoes'] = arrFormacoes;

                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.error) {
                                    __Matricula.showNotificacaoDanger(data.result);
                                } else {
                                    $.SmartMessageBox({
                                        title: 'Aluno cadastrado com sucesso!<br>Deseja ir para listagem?',
                                        buttons: "[Não][Sim]"
                                    }, function (ButtonPress) {
                                        if (ButtonPress == "Sim") {
                                            window.open(__Matricula.options.url.matricular, '_self');
                                        } else {
                                            __Matricula.cleanAllFields();
                                            $cpf.val("").trigger("change");
                                            __Matricula.backStep(true);
                                        }
                                    });
                                }
                                __Matricula.removeOverlay($overlay);
                            }
                        });
                    });

                    $pesNome.on('change', function () {
                        $('.pesNome').text($(this).val());
                    });

                    $tiposelId.select2({
                        ajax: {
                            url: __Matricula.options.url.formaIngresso,
                            dataType: 'json',
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.tiposel_nome, id: el.tiposel_id};
                                });

                                return {results: transformed};
                            }
                        }
                    }).on('change', function (data) {
                        if (data['added']) {
                            if (data['added']['text'] == "Vestibular") {
                                $edicaoId.select2('enable');
                            } else {
                                $edicaoId.select2('disable');
                            }
                        }
                    });

                    $edicaoId.select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __Matricula.options.url.processoSeletivo,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.edicao_descricao, id: el.edicao_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $modId.select2({
                        language: 'pt-BR',
                        allowClear: true,
                        ajax: {
                            url: __Matricula.options.url.modalidade,
                            dataType: 'json',
                            allowClear: true,
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.mod_nome, id: el.mod_id};
                                });

                                return {results: transformed};
                            }
                        }
                    }).on('change', function (e) {
                        if (e.added) {
                            $("#nivelId, #cursocampusId, #unidadeId").select2('val', "").trigger("change");
                        }
                    });

                    $nivelId.select2({
                        language: 'pt-BR',
                        allowClear: true,
                        ajax: {
                            url: __Matricula.options.url.nivel,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.nivel_nome, id: el.nivel_id};
                                });

                                return {results: transformed};
                            }
                        }
                    }).on('change', function (e) {
                        if (e.added) {
                            $("#cursocampusId, #unidadeId").select2('val', "").trigger("change");
                        }
                    });

                    $campusCurso.select2({
                        ajax: {
                            url: __Matricula.options.url.campusCurso,
                            dataType: 'json',
                            data: function (query) {
                                var naoIncluirCampusCurso = [];

                                if (__Matricula.options.dadosPessoa) {
                                    $.map(__Matricula.options.dadosPessoa['campusCurso'] || [], function (el, index) {
                                        naoIncluirCampusCurso.push(el[0]['id']);
                                    });
                                }

                                return {
                                    query: query,
                                    naoIncluirCampusCurso: naoIncluirCampusCurso,
                                    modalidade: $modId.val(),
                                    nivel: $nivelId.val()
                                };
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {
                                        text: el.camp_nome + '/' + el.curso_nome,
                                        id: el.cursocampus_id,
                                        cursoPossuiPeriodoLetivo: el.curso_possui_periodo_letivo,
                                        modalidade: {id: el.mod_id, text: el.mod_nome},
                                        unidade: {id: el.unidade_id, text: el.unidade_nome},
                                        nivel: {id: el.nivel_id, text: el.nivel_nome}
                                    };
                                });

                                return {results: transformed};
                            }
                        }
                    }).on('change', function (e) {
                        var curso = e.added;

                        if (curso) {
                            $('.cursoNome').text(curso['text']);
                            __Matricula.steps.financeiro.criarPlanoDePagamento(curso['id']);

                            __Matricula.steps.dadosBasicos.setCursoPeriodicidade(
                                curso['cursoPossuiPeriodoLetivo']
                            );

                            if (curso['cursoPossuiPeriodoLetivo'] == "Sim") {
                                $("#perId").select2('enable');
                            } else {
                                $("#perId").select2('disable');
                            }

                            if (curso['modalidade']) {
                                $modId.select2('data', curso['modalidade']).trigger('change');
                            }
                            if (curso['unidade']) {
                                $unidadeId.select2('data', curso['unidade']).trigger('change');
                            }
                            if (curso['nivel']) {
                                $nivelId.select2('data', curso['nivel']).trigger('change');
                            }
                        }

                        $("#perId, #turmaId").select2('val', '').trigger("change");
                        $turmaId.select2('disable');
                        $unidadeId.select2('enable');
                    });

                    $perId.select2({
                        ajax: {
                            url: __Matricula.options.url.periodo,
                            dataType: 'json',
                            data: function (query) {
                                return {
                                    query: query,
                                    cursocampusId: __Matricula.steps.dadosBasicos.getCampusCurso(),
                                    abertoParaMatricula: true
                                };
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {id: el.per_id, text: el.per_nome};
                                });

                                return {results: transformed};
                            }
                        }
                    }).on('change', function (e) {
                        if (e.added) {
                            $turmaId.select2('enable');
                            $turmaId.select2('val', '').trigger('change');
                        }
                        __Matricula.steps.financeiro.criarPlanoDePagamento($campusCurso.val());
                    });

                    $turmaId.select2({
                        ajax: {
                            url: __Matricula.options.url.turma,
                            dataType: 'json',
                            data: function (query) {
                                return {
                                    query: query,
                                    cursocampusId: $campusCurso.val(),
                                    perId: $perId.val(),
                                    apenasTurmasComDisciplina: true
                                };
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {
                                        id: el.turma_id,
                                        text: el.turma_nome,
                                        matcurId: el.mat_cur_id,
                                        serie: el.turma_serie
                                    };
                                });

                                return {results: transformed};
                            }
                        }
                    }).on('change', function (e) {
                        var $listaDisciplinas = $('.lista-disciplinas');
                        $('#grade-disciplinas').html('<br>');

                        if (e.added) {
                            __Matricula.steps.formacao.montaGradeDisciplinasPorMatCurId(
                                e.added['matcurId'], e.added['serie']
                            );
                            $listaDisciplinas.show();
                        } else {
                            $listaDisciplinas.hide();
                        }
                    });

                    $unidadeId.select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __Matricula.options.url.unidadeEstudo,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {
                                    query: query,
                                    cursocampusId: $campusCurso.val()
                                };
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.unidade_nome, id: el.unidade_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $fieldsBloqueados.select2('disable');

                    $(document).on('click','.btn-matricula-copy',function(){
                        window.prompt("Copiar: Ctrl+C, Enter", $(this).val());
                    });

                    $cpf.on('complete', function (e) {
                        var digitosCpf = $(this).val().replace(/[^0-9]*/g, '');
                        var ok = digitosCpf.length == 11;

                        if (!ok) {
                            e.stopPropagation();
                            e.preventDefault();
                            return false;
                        } else {
                            if (!__Matricula.validaCPF(digitosCpf)) {
                                __Matricula.showNotificacaoInfo('CPF inválido!');
                                e.stopPropagation();
                                e.preventDefault();
                                return false;
                            }
                        }

                        __Matricula.addOverlay($('.place-overlay'),
                            'Aguarde, carregando informações...'
                        );

                        __Matricula.cleanAllFields();

                        if ($cpf.val()) {
                            $('.pesCpf').text($cpf.val());
                            $.ajax({
                                url: __Matricula.options.url.search,
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    pesCpf: $cpf.val(),
                                    pesquisaCompleta: true,
                                    validarAprovacaoVestibular: true
                                },
                                success: function (data) {
                                    var situacao = data['aprovacaoProcessoSeletivo'] || false,
                                        pessoa = data['pesNome'] || 'Pessoa informada',
                                        $resultado = $('[name="inscricaoResultado"]');

                                    pessoa = '<strong>' + pessoa + '</strong>';

                                    if (situacao) {
                                        situacao = situacao.toUpperCase();

                                        __Matricula.showNotificacaoSuccess(
                                            pessoa + ' está com situação <strong>"' + situacao + '"</strong> no processo seletivo!'
                                        );
                                    } else {
                                        __Matricula.steps.dadosBasicos.setFormaDeIngresso();
                                    }

                                    if (data['matriculasEncontradas']) {
                                        var cursos = [];
                                        var matriculasNoCurso = data['matriculasEncontradas']['curso'] || '-';
                                        var matriculasNoPeriodo = data['matriculasEncontradas']['periodo'] || '-';
                                        var btn = "";

                                        $.map(data['campusCurso'], function (el, index) {
                                            cursos.push(el[0]['text']);
                                        });

                                        if (matriculasNoCurso) {
                                            btn = "<button class='text-success btn-matricula-copy' value='" + matriculasNoCurso + "' >" +
                                                "Copiar" +
                                                "</button>";

                                            __Matricula.showNotificacaoInfo(
                                                "Matriculas no curso: <strong>" + matriculasNoCurso + "</strong><br>" + btn
                                            );
                                        }

                                        if (matriculasNoPeriodo) {
                                            btn = "<button class='text-success btn-matricula-copy' value='" + matriculasNoPeriodo + "' >" +
                                                "Copiar" +
                                                "</button>";

                                            __Matricula.showNotificacaoInfo(
                                                "Matriculas no período: <strong>" + matriculasNoPeriodo + "</strong><br>" + btn
                                            );
                                        }

                                        __Matricula.showNotificacaoInfo(
                                            "Cursos já matrículado: <strong>" + cursos.join(",") + "</strong>"
                                        );
                                    }

                                    switch (situacao) {
                                        case 'REPROVADO':
                                            $('#infoAdicional').hide();
                                            $resultado.val(false);
                                            $cpf.val('').trigger('change');
                                            break;
                                        case 'APROVADO':
                                            $resultado.val(true);
                                            __Matricula.steps.dadosBasicos.setDefaults(data);
                                            break;
                                        default:
                                            __Matricula.steps.dadosBasicos.setDefaults(data);
                                            break;
                                    }


                                    __Matricula.removeOverlay($('.place-overlay'));
                                }
                            });
                        }
                    });
                },
                setDefaults: function (data) {
                    __Matricula.steps.dadosBasicos.setDadosNosCampos(data || []);
                    __Matricula.options.dadosPessoa = data || [];

                    if (data['processoSeletivo']) {
                        __Matricula.steps.dadosBasicos.setInfoProcessoSeletivo(data['processoSeletivo']);
                        __Matricula.steps.dadosBasicos.setFormaDeIngresso(data['processoSeletivo'], true);
                    }

                    __Matricula.steps.formacao.setFormacao(data['formacoes'] || []);
                },
                validate: function () {
                    var $form = $(__Matricula.options.formElement);

                    __Matricula.options.validator.settings.rules = {
                        pesCpf: {required: true, cpf: true},
                        cursocampusId: {required: true},
                        pesNome: {required: true, maxlength: 255},
                        tiposelId: {required: true},
                        edicaoId: {required: true},
                        unidadeId: {required: true}
                    };
                    __Matricula.options.validator.settings.messages = {
                        pesCpf: {required: 'Campo obrigatório!', cpf: 'CPF inválido!'},
                        cursocampusId: {required: 'Campo obrigatório!'},
                        tiposelId: {required: 'Campo obrigatório!'},
                        pesNome: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 255!'},
                        edicaoId: {required: 'Campo obrigatório!'},
                        unidadeId: {required: 'Campo obrigatório!'}
                    };

                    if (__Matricula.steps.dadosBasicos.getCursoPeriodicidade()) {
                        __Matricula.options.validator.settings.rules['perId'] = {required: true};
                        __Matricula.options.validator.settings.rules['turmaId'] = {required: true};
                        __Matricula.options.validator.settings.messages['perId'] = {required: 'Campo obrigatório!'};
                        __Matricula.options.validator.settings.messages['turmaId'] = {required: 'Campo obrigatório!'};
                    }

                    return !$form.valid();
                },
                getCampusCurso: function (obj) {
                    var $sel = $('#cursocampusId');

                    if (obj) {
                        return $sel.select2('data');
                    }

                    return $sel.val();
                },
                cursoPossuiPeriodoLetivo: function () {
                    return __Matricula.steps.dadosBasicos.getCursoPeriodicidade();
                },
                getCursoPeriodicidade: function () {
                    return __Matricula.options.data.cursoPossuiPeriodicidade || false;
                },
                setCursoPeriodicidade: function (periodicidade) {
                    if (periodicidade == "Sim") {
                        __Matricula.options.data.cursoPossuiPeriodicidade = true;
                    }
                },
                setDadosNosCampos: function (arrDados) {
                    if (!(arrDados instanceof Array)) {
                        for (var text in arrDados) {
                            var valor = arrDados[text];
                            var $tiposelId = $("#tiposelId");
                            var $input = $('[name="' + text + '"]');

                            if (!valor) {
                                continue
                            }

                            switch (text) {
                                case 'pesSexo':
                                case 'alunoEtnia':
                                case 'pesNacionalidade':
                                case 'pesNascUf':
                                    $input.select2('data', {id: valor, text: valor}).trigger('change');
                                    break;
                                case 'selecaoTipo':
                                    $tiposelId.select2('data', {id: valor[0].tiposelId, text: valor[0].tiposelNome});
                                    $tiposelId.select2('enable').trigger("change");
                                    break;
                                case "pessoaNecessidades":
                                case "pesIdAgenciador":
                                    $input.select2('data', valor).trigger('change');
                                    break;
                                default:
                                    $('.' + text).text(valor);
                                    $input.val(valor);
                                    break;
                            }
                        }
                    }
                },
                setInfoProcessoSeletivo: function (arrDados) {
                    __Matricula.steps.dadosBasicos.setCursoPeriodicidade(arrDados['cursoPossuiPeriodoLetivo']);

                    for (var chave in arrDados) {
                        var valor = arrDados[chave];

                        if (!valor) {
                            continue;
                        }

                        switch (chave) {
                            case 'per_id':
                                $("#perId").select2('data', {id: valor, text: arrDados['per_nome']}).trigger("change");
                                break;
                            case 'curso':
                                $("#cursocampusId").select2('data', valor[0]).trigger("change");
                                break;
                            case 'modalidade':
                                $("#modId").select2('data', {
                                    id: valor['modId'],
                                    text: valor['modNome']
                                }).trigger("change");
                                break;
                            case 'nivel':
                                $("#nivelId").select2('data', {
                                    id: valor['nivelId'],
                                    text: valor['nivelNome']
                                }).trigger("change");
                                break;
                            case 'edicao':
                                $("#edicaoId").select2('data', {
                                    id: valor['edicaoId'],
                                    text: valor['edicaoDescricao']
                                }).trigger("change");
                                break;
                            default:
                                $('.' + chave).text(valor);
                        }
                    }
                },
                setFormaDeIngresso: function (arrDados, sobrescrever) {
                    var $tiposelId = $('#tiposelId');
                    var selecaoTipo = {};

                    if (sobrescrever) {
                        $tiposelId.select2('destroy');
                        $tiposelId.select2({data: []});
                    }

                    if (arrDados) {
                        if (arrDados[0]) {
                            selecaoTipo = {
                                id: arrDados[0]['tiposelId'],
                                text: arrDados[0]['tiposelNome']
                            }
                        } else if (arrDados['tiposel_id']) {
                            selecaoTipo = {
                                id: arrDados['tiposel_id'],
                                text: arrDados['tiposel_nome']
                            }
                        }
                    } else {
                        var dados = __Matricula.steps.dadosBasicos.getFormaDeIngressoPadrao();
                        selecaoTipo.id = dados[0]['id'];
                        selecaoTipo.text = dados[0]['text'];
                    }

                    $tiposelId.select2('data', selecaoTipo).trigger('change');

                    if (selecaoTipo['text'] == "vestibular") {
                        $("#edicaoId").select2('enable');
                    } else {
                        $("#edicaoId").select2('disable');
                    }
                },
                getFormaDeIngressoPadrao: function () {
                    return __Matricula.options.value.formaDeIngressoPadrao;
                }
            },
            dadosPessoais: {
                init: function () {
                    var $buscarCep = $(".buscarCep"),
                        $pesSexo = $("[name='pesSexo']"),
                        $pesNascUf = $("[name='pesNascUf']"),
                        $pesDataNascimento = $("[name='pesDataNascimento']"),
                        $alunoEtnia = $("[name='alunoEtnia']"),
                        $pesNacionalidade = $("[name='pesNacionalidade']"),
                        $pesIdAgenciador = $("[name='pesIdAgenciador']"),
                        $pessoaNecessidades = $("[name='pessoaNecessidades']"),
                        $pesDocEstrangeiro = $("[name='pesDocEstrangeiro']"),
                        $origem = $("[name='origem']");

                    $pesSexo.select2({
                        data: [
                            {id: "Masculino", text: "Masculino"},
                            {id: "Feminino", text: "Feminino"}
                        ]
                    });

                    $pesNacionalidade.select2({
                        data: [
                            {id: "Brasileiro", text: "Brasileiro"},
                            {id: "Estrangeiro", text: "Estrangeiro"}
                        ]
                    }).on('change', function () {
                        if ($(this).val() == 'Estrangeiro') {
                            $('.pesDocEstrangeiro').removeClass('hidden');
                            $pesDocEstrangeiro.attr('disabled', false);
                        } else {
                            $('.pesDocEstrangeiro').addClass('hidden');
                            $pesDocEstrangeiro.attr('disabled', true);
                        }
                    });

                    $origem.select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __Matricula.options.url.cadastroOrigem,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.origem_nome, id: el.origem_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $pessoaNecessidades.select2({
                        data: __Matricula.options.data.arrNecessidadesEspeciais || [],
                        multiple: true
                    });

                    $pesIdAgenciador.select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __Matricula.options.url.agenteEducacional,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.pes_nome, id: el.pes_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $alunoEtnia.select2({
                        language: 'pt-BR',
                        minimumResultsForSearch: -1,
                        data: __Matricula.options.data.arrEtnia
                    });

                    $pesDataNascimento.datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>',
                        nextText: '<i class="fa fa-chevron-right"></i>',
                        dateFormat: 'dd-mm-yy'
                    });

                    __Matricula.iniciarElementoDatePicker($pesDataNascimento);

                    __Matricula.autoCompleteEndereco($('[name=pesNaturalidade]'));

                    __Matricula.setSelect2Estados($pesNascUf);

                    $buscarCep.click(function () {
                        __Matricula.steps.dadosPessoais.buscarCep($(this));
                    });
                },
                validate: function () {
                    var $form = $(__Matricula.options.formElement);

                    __Matricula.options.validator.settings.rules = {
                        pesNacionalidade: {required: true},
                        pesDocEstrangeiro: {required: true},
                        pesNaturalidade: {required: true},
                        pesNascUf: {required: true},
                        alunoEtnia: {required: true},
                        pesDataNascimento: {required: true, dateBR: true},
                        pesSexo: {required: true},
                        pesRg: {required: true},
                        pesRgEmissao: {required: false, dateBR: true},
                        alunoPai: {required: false},
                        alunoMae: {required: false},
                        origem: {required: true}
                    };

                    __Matricula.options.validator.settings.messages = {
                        pesNacionalidade: {required: 'Campo obrigatório!'},
                        pesDocEstrangeiro: {required: 'Campo obrigatório!'},
                        pesNaturalidade: {required: 'Campo obrigatório!'},
                        pesNascUf: {required: 'Campo obrigatório!'},
                        alunoEtnia: {required: 'Campo obrigatório!'},
                        pesDataNascimento: {
                            required: 'Campo obrigatório!',
                            dateBR: 'Data inválida!'
                        },
                        pesSexo: {required: 'Campo obrigatório!'},
                        pesRg: {required: 'Campo obrigatório!'},
                        pesRgEmissao: {
                            required: 'Campo obrigatório!',
                            dateBR: 'Data inválida!'
                        },
                        alunoPai: {required: 'Campo obrigatório!'},
                        alunoMae: {required: 'Campo obrigatório!'},
                        origem: {required: 'Campo obrigatório!'}
                    };

                    return !$form.valid();
                },
                buscarCep: function ($element) {
                    var endCep = $($element).closest('form').find("[name=endCep]").val();
                    var endCepNums = endCep.replace(/[^0-9]/g, '');
                    var $overlay = $('.place-overlay');

                    if (endCepNums.length < 8) {
                        __Matricula.showNotificacaoWarning('CEP inválido ou não encontrado!');

                        return;
                    }

                    __Matricula.addOverlay(
                        $overlay,
                        'Aguarde...'
                    );

                    $element.prop('disabled', true);

                    $.ajax({
                        url: __Matricula.options.url.buscaCEP,
                        dataType: 'json',
                        type: 'post',
                        async: true,
                        data: {cep: endCep},
                        success: function (data) {
                            __Matricula.removeOverlay($overlay);
                            $element.prop('disabled', false);

                            if (!data.dados) {
                                __Matricula.showNotificacaoWarning('CEP inválido ou não encontrado!');
                            }

                            $element.closest('form').find("[name=endLogradouro]").val(
                                ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                            );
                            $element.closest('form').find("[name=endCidade]").val(data.dados.cid_nome || '');
                            $element.closest('form').find("[name=endEstado]").val(data.dados.est_uf ||
                                '').trigger('change');
                            $element.closest('form').find("[name=endBairro]").val(data.dados.bai_nome || '');
                        },
                        erro: function () {
                            __Matricula.removeOverlay($overlay);
                            __Matricula.showNotificacaoDanger('Falha ao buscar CEP!');
                            $element.prop('disabled', false);
                        }
                    });
                }
            },
            contatoEndereco: {
                init: function () {
                    var $endCep = $('[name="endCep"]');

                    __Matricula.autoCompleteEndereco($('[name=endCidade], [name=formacaoCidade]'));

                    $("[name=conContatoTelefone], [name=conContatoCelular]").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                    });

                    $endCep.inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']});
                },
                validate: function () {
                    var $form = $(__Matricula.options.formElement);
                    __Matricula.options.validator.settings.rules = {
                        endCep: {required: false},
                        endLogradouro: {required: true},
                        endNumero: {
                            required: function () {
                                return $form.find('[name=endComplemento]').val() == '';
                            }, number: true
                        },
                        endBairro: {required: false},
                        endCidade: {required: true},
                        endEstado: {required: true},
                        conContatoTelefone: {telefoneValido:true, required: false},
                        conContatoCelular: {celular: true, required: true},
                        conContatoEmail: {required: true, email: true}
                    };
                    __Matricula.options.validator.settings.messages = {
                        endCep: {required: 'Campo obrigatório!'},
                        endLogradouro: {required: 'Campo obrigatório!'},
                        endNumero: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        endBairro: {required: 'Campo obrigatório!'},
                        endCidade: {required: 'Campo obrigatória!'},
                        endEstado: {required: 'Campo obrigatório!'},
                        conContatoTelefone: {required: 'Campo obrigatório!'},
                        conContatoCelular: {required: 'Campo obrigatório!'},
                        conContatoEmail: {required: 'Campo obrigatório!', email: 'Email inválido!'}
                    };

                    return !$form.valid();
                }
            },
            formacao: {
                validarFormacao: function () {
                    var $form = $(__Matricula.options.formElement);

                    __Matricula.options.validator.settings.rules = {
                        formacaoCurso: {required: true},
                        formacaoRegime: {required: true},
                        formacaoTipoEnsino: {required: true},
                        formacaoTipo: {required: true},
                        formacaoAno: {required: true, number: true},
                        formacaoInstituicao: {required: true},
                        formacaoConcluido: {required: true}
                    };
                    __Matricula.options.validator.settings.messages = {
                        formacaoCurso: {required: "Campo Obrigatório"},
                        formacaoRegime: {required: "Campo Obrigatório"},
                        formacaoTipoEnsino: {required: "Campo Obrigatório"},
                        formacaoTipo: {required: "Campo Obrigatório"},
                        formacaoAno: {required: "Cammpo Obrigatório", number: "Somente números!"},
                        formacaoInstituicao: {required: "Campo Obrigatório"},
                        formacaoConcluido: {required: "Campo Obrigatório"}
                    };

                    return $form.valid();
                },
                iniciarCamposModal: function () {
                    var $formacaoConcluido = $('[name="formacaoConcluido"],[name="Concluido"]'),
                        $formacaoInstituicao = $('[name="formacaoInstituicao"],[name="Instituicao"]'),
                        $formacaoTipoEnsino = $('[name="formacaoTipoEnsino"],[name="TipoEnsino"]'),
                        $formacaoRegime = $('[name="formacaoRegime"],[name="Regime"]'),
                        $formacaoTipo = $('[name="formacaoTipo"],[name="Tipo"]');

                    $('#formacaoAno, [name="Ano"]').inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['9999']
                    });

                    $formacaoConcluido.select2({
                        minimumResultsForSearch: -1,
                        data: function () {
                            return {results: __Matricula.options.data.arrFormacaoConcluido || []};
                        }
                    });

                    $formacaoRegime.select2({
                        minimumResultsForSearch: -1,
                        data: function () {
                            return {results: __Matricula.options.data.arrFormacaoRegime || []};
                        }
                    });

                    $formacaoTipoEnsino.select2({
                        minimumResultsForSearch: -1,
                        data: function () {
                            return {results: __Matricula.options.data.arrFormacaoTipoEnsino || []};
                        }
                    });


                    $formacaoTipo.select2({
                        minimumResultsForSearch: -1,
                        data: function () {
                            return {results: __Matricula.options.data.arrFormacaoTipo || []};
                        }
                    });

                    $("#salvar-formacao").on('click', function () {
                        var $form = $('#form-formacao-add');

                        var ok = __Matricula.steps.formacao.validarFormacao();

                        if (ok) {
                            var arrDados = $form.serializeJSON() || {};
                            var formacaoId = arrDados['formacaoId'] || __Matricula.createGUID();
                            arrDados['formacaoId'] = formacaoId;

                            arrDados['id'] = formacaoId;
                            arrDados['text'] = arrDados['formacaoTipo'] + '/' + arrDados['formacaoCurso'];

                            __Matricula.options.data.formacao[formacaoId] = arrDados;

                            $('[name="seletorFormacao"]').select2('data', arrDados || []).trigger('change');

                            __Matricula.steps.formacao.atualizarContadorFormacoes();

                            $('#modal-formacao').modal('hide');
                        }
                    });

                    __Matricula.autoCompleteInstituicao($formacaoInstituicao);
                },
                init: function () {
                    var $seletorFormacao = $('[name="seletorFormacao"]');

                    __Matricula.steps.formacao.iniciarCamposModal();

                    $seletorFormacao.select2({
                        data: function () {
                            var arrFormacao = [];
                            var arrFormaces = __Matricula.steps.formacao.getArrFormacao();

                            $.each(arrFormaces, function (i, el) {
                                return arrFormacao.push(el);
                            });

                            return {results: arrFormacao};
                        }
                    }).on('change', function () {
                        var formacaoId = $(this).select2('data') || '';
                        $('#btn-editarFormacao').prop('disabled', formacaoId ? false : true);
                    });

                    $('#modal-formacao').on('click', '.close', function (e) {
                        $seletorFormacao.val('').trigger('change');
                    });

                    $('#btn-addFormacao').on('click', function () {
                        __Matricula.steps.formacao.showModalFormacao();
                    });

                    $('#btn-editarFormacao').on('click', function () {
                        var formacaoId = $seletorFormacao.val() || '';

                        if (formacaoId) {
                            __Matricula.steps.formacao.showModalFormacao(formacaoId);

                            return true;
                        }
                    });
                },
                validate: function () {
                    var $form = $(__Matricula.options.formElement);
                    var arrFormacao = __Matricula.steps.formacao.getArrFormacao();

                    if(Object.keys(arrFormacao).length <= 0){
                        __Matricula.showNotificacaoDanger('Por favor, adicione pelo menos uma formação anterior!');
                        return true;
                    }

                    if (!$form.valid()) {
                        __Matricula.showNotificacaoDanger('Por favor preencha os dados de formação anterior!');
                        return true;
                    }

                    return __Matricula.steps.financeiro.validate();
                },
                montaGradeDisciplinasPorMatCurId: function (matcurId, serie) {
                    var $local = $('#grade-disciplinas');
                    var $listaDisciplinas = $('.lista-disciplinas');
                    __Matricula.addOverlay($listaDisciplinas, 'Carregando as disciplinas da turma. aguarde...');

                    $.ajax({
                        url: __Matricula.options.url.searchDisciplinas,
                        type: 'POST',
                        dataType: 'json',
                        async: true,
                        data: {
                            periodo: serie,
                            matriz: matcurId
                        },
                        success: function (data) {
                            $local.html(__Matricula.steps.formacao.criarGradeDisciplinas(data['disciplinas']));
                            __Matricula.removeOverlay($listaDisciplinas);
                        }
                    });
                },
                criarGradeDisciplinas: function (arrDados) {
                    if (!arrDados) {
                        return false;
                    }

                    var html = "";

                    for (var row in arrDados) {
                        html += __Matricula.steps.formacao.criarCampoDisciplina(arrDados[row]);
                    }

                    return html;
                },
                criarCampoDisciplina: function (objDisc) {
                    if (!objDisc) {
                        return [];
                    }

                    var discNome = objDisc['discNome'];
                    var discId = objDisc['disc'];

                    return "<div class='col-xs-2'><div class='form-group'>" +
                        "<label>" + discNome + "</label>" +
                        "<input type='hidden' name='disciplina[]' class='form-control' value='" +
                        discId +
                        "'>" +
                        "</div></div>";
                },
                setFormacao: function (arrDados) {
                    var arrFormacao = {}, contador = 0;

                    $.map(arrDados, function (el) {
                        el['id'] = el['formacaoId'];
                        el['text'] = el['formacaoTipo'] + '/' + el['formacaoCurso'];

                        arrFormacao[el.formacaoId] = el;
                        contador++;
                    });

                    __Matricula.steps.formacao.atualizarContadorFormacoes(contador);

                    __Matricula.options.data.formacao = arrFormacao;
                },
                getArrFormacao: function (id) {
                    if (id) {
                        return __Matricula.options.data.formacao[id] || {};
                    }

                    return __Matricula.options.data.formacao || {};
                },
                atualizarContadorFormacoes: function(quantidade){
                    quantidade = quantidade || Object.keys(__Matricula.steps.formacao.getArrFormacao()).length || 0;

                    $("#contagemFormacoes").html(quantidade);
                },
                getPeriodoLetivo: function () {
                    return $('#perId').val();
                },
                showModalFormacao: function (formacaoId) {
                    var $modal = $('#modal-formacao');
                    var $form = $('#form-formacao-add');
                    formacaoId = formacaoId || false;

                    $form[0].reset();

                    var data = __Matricula.steps.formacao.getArrFormacao(formacaoId) || {};

                    $form.deserialize(data);

                    $form.find('[name=formacaoRegime]').select2('val', data['formacaoRegime'] || '').trigger('change');
                    $form.find('[name=formacaoTipo]').select2('val', data['formacaoTipo'] || '').trigger('change');
                    $form.find('[name=formacaoTipoEnsino]').select2('val', data['formacaoTipoEnsino'] || '').trigger('change');
                    $form.find('[name=formacaoConcluido]').select2('val', data['formacaoConcluido'] || '').trigger('change');

                    $modal.modal('show');
                }
            },
            financeiro: {
                init: function () {
                    var arrCampos = [];
                    arrCampos.push({name: 'tipotituloId', label: 'tipotituloId', type: 'hidden'});
                    arrCampos.push({name: 'configPgtoCursoId', label: 'configPgtoCursoId', type: 'hidden'});
                    arrCampos.push({name: 'configPgtoCursoValor', label: 'configPgtoCursoValor', type: 'hidden'});
                    arrCampos.push({name: 'configPgtoCursoParcela', label: 'configPgtoCursoParcela', type: 'hidden'});
                    arrCampos.push({name: 'configPgtoCursoAlterado', label: 'configPgtoCursoAlterado', type: 'hidden'});
                    arrCampos.push({name: 'tipotituloMatriculaEmissao', label: 'Emissão Obrigatória', type: 'hidden'});
                    arrCampos.push({
                        name: 'tipotituloNome', label: 'Título',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-5');
                            field.addClass('hidden');
                            field.before(
                                '<p class="form-control-static">' + itemConfig['tipotituloNome'] + '</p>'
                            );
                        }
                    });
                    arrCampos.push({
                        name: 'valoresId', label: 'Pagamento',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings;
                            var $parent = field.parent();

                            if (__Matricula.options.value.agente) {
                                $parent.addClass('col-sm-3');
                            } else {
                                $parent.addClass('col-sm-4');
                            }

                            itemConfig['valoresId'] = itemConfig['valoresId'] || null;

                            field.select2({
                                language: 'pt-BR',
                                allowClear: true,
                                minimumResultsForSearch: -1,
                                data: function () {
                                    var encontrou = false;
                                    var arrResults = $.map(itemConfig['financeiroValores'], function (el) {
                                        el.id = el['valoresId'];
                                        el.text = el['valoresParcela'] + ' x ' + el['valoresPreco'];

                                        if (encontrou == false &&
                                            parseInt(el.id) == parseInt(itemConfig['valoresId'])) {
                                            encontrou = true;
                                        }

                                        return el;
                                    });

                                    return {results: arrResults};
                                }
                            });
                        }
                    });
                    arrCampos.push({
                        name: 'vencimento', label: 'Vencimento',
                        addCallback: function (e) {
                            var field = e.data.field;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-3');

                            field.datepicker({
                                minDate: 0,
                                dateFormat: 'dd/mm/yy',
                                prevText: '<i class="fa fa-chevron-left"></i>',
                                nextText: '<i class="fa fa-chevron-right"></i>'
                            })
                        }
                    });

                    $("#tipoTitulo").componente({
                        inputName: 'tipoTitulo',
                        classCardExtra: '',
                        labelContainer: '<div class="form-group"/>',
                        tplItem: '\
                        <div>\
                            <div class="componente-content col-sm-12">\
                            </div>\
                        </div>',
                        fieldClass: 'form-control',
                        fields: arrCampos,
                        addCallback: function (e) {
                            var itemSettings = e.data.itemSettings;

                            itemSettings['financeiroValores'] = itemSettings['financeiroValores'] || [];
                        }
                    });

                    __Matricula.steps.financeiro.carregarTiposTitulo();
                },
                validate: function () {
                    var errors = [];
                    var componente = $("#tipoTitulo").componente();
                    var tipoTitulo = componente.items();

                    __Matricula.removeErroComponente($("#tipoTitulo"));

                    $.each(tipoTitulo, function (i, item) {
                        var $item = $(item);
                        var id = $(item).data('componente.id');
                        __Matricula.removeErroComponente($item);

                        var camposVazios = [];

                        var $eltipotituloNome = $item
                            .find('[name="' + componente.makeFieldName({id: id, name: 'tipotituloNome'}) + '"]');
                        var $elvaloresId = $item
                            .find('[name="' + componente.makeFieldName({id: id, name: 'valoresId'}) + '"]');
                        var $elvencimento = $item
                            .find('[name="' + componente.makeFieldName({id: id, name: 'vencimento'}) + '"]');
                        var $tituloObrigatorio = $item.find('[name="' + componente.makeFieldName({
                                id: id,
                                name: 'tipotituloMatriculaEmissao'
                            }) + '"]');

                        var tipotituloNomeVal = $eltipotituloNome.val();
                        var vencimentoVal = $elvencimento.val();
                        var valoresIdVal = $elvaloresId.val();
                        var tituloObrigatorio = $tituloObrigatorio.val();

                        if (tituloObrigatorio == 'Obrigatório') {
                            if (valoresIdVal == "") {
                                camposVazios.push("Pagamento");
                            }
                        }

                        if (tituloObrigatorio == 'Obrigatório' || valoresIdVal) {
                            if (vencimentoVal == "") {
                                camposVazios.push("Vencimento");
                            } else {

                                var data = new Date();
                                var dtVencimentoVal = __Matricula.formatDate(vencimentoVal, 'yy-mm-dd');
                                data = __Matricula.formatDate(data, 'yy-mm-dd');
                                dtVencimentoVal = new Date(dtVencimentoVal);
                                data = new Date(data);

                                if (dtVencimentoVal < data) {
                                    errors.push('O' + '&nbsp' + (i + 1) + '&ordm &nbsp' +
                                        'campo de vencimento possui valor inválido ou está vazio!');
                                }
                            }
                        }

                        if (camposVazios.length > 0) {
                            errors.push(
                                'O titulo ' + tipotituloNomeVal +
                                ' possui campos vazios. Por favor preencha os campos: <b>' +
                                camposVazios.join(', ') + '</b>.'
                            );
                            __Matricula.adicionaErroComponente($item, true);
                        }
                    });

                    if (errors.length > 0) {
                        __Matricula.showNotificacaoDanger(errors.join('<br>'));
                        return true;
                    }

                    return false;
                },
                criarPlanoDePagamento: function (cursocampus) {
                    var $form = $(__Matricula.options.formElement);

                    if (!cursocampus) {
                        return false;
                    }

                    var query = {
                        emissaoMatricula: true,
                        cursocampus: cursocampus
                    };

                    if (__Matricula.steps.dadosBasicos.cursoPossuiPeriodoLetivo() == "Sim") {
                        query['perId'] = __Matricula.steps.formacao.getPeriodoLetivo();
                    }

                    $.ajax({
                        url: __Matricula.options.url.financeiroTituloTipo,
                        type: 'POST',
                        dataType: 'json',
                        async: true,
                        data: query,
                        success: function (data) {
                            __Matricula.options.data.tipoTitulo = data || [];
                            __Matricula.steps.financeiro.carregarTiposTitulo();
                        }
                    });
                },
                carregarTiposTitulo: function () {
                    $("#tipoTitulo").componente().removeAll();

                    $.each(__Matricula.options.data.tipoTitulo || [], function (i, item) {
                        var obrigatorio = 'Opcional';
                        var itemAdd = $.extend({}, item);
                        itemAdd['configPgtoCursoAlterado'] = 'Não';

                        if (__Matricula.options.value.agente && itemAdd['tipotituloMatriculaEmissao'] != 'Sim') {
                            obrigatorio = 'Opcional';
                        } else if (itemAdd['tipotituloMatriculaEmissao'] != 'Opcional') {
                            obrigatorio = 'Obrigatório';
                        }

                        itemAdd['tipotituloNome'] = itemAdd['tipotituloNome'] + ' (' + obrigatorio + ')';
                        itemAdd['tipotituloMatriculaEmissao'] = obrigatorio;

                        $('#tipoTitulo').componente(['add', itemAdd]);
                    });
                }
            }
        };

        this.autoCompleteEndereco = function ($fields) {
            $fields.blur(function () {
                $(this).removeClass('ui-autocomplete-loading');
            })
                .autocomplete({
                    source: function (request, response) {
                        var $this = $(this);
                        var $element = $(this.element);
                        var previous_request = $element.data("jqXHR");

                        if (previous_request) {
                            previous_request.abort();
                        }

                        $element.data("jqXHR", $.ajax({
                            url: __Matricula.options.url.buscaCEP,
                            data: {cidade: request.term},
                            type: 'POST',
                            async: true,
                            dataType: "json",
                            success: function (data) {
                                var transformed = $.map(data.dados || [], function (el) {
                                    el.label = el.cid_nome;

                                    return el;
                                });

                                response(transformed);
                            }
                        }));
                    },
                    minLength: 2,
                    focus: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.cid_nome);

                        return false;
                    },
                    select: function (event, ui) {
                        var $element = $(this);
                        $element.val(ui.item.cid_nome);
                        $element.closest('form').find(
                            $element.attr('id') == 'endCidade' ? "[name=endEstado]" : (
                                $element.attr('id') == 'formacaoCidade' ?
                                    "[name=formacaoEstado]" : "[name=pesNascUf]"
                            )
                        )
                            .val(ui.item.est_uf).trigger('change');

                        return false;
                    }
                });
        };

        this.autoCompleteInstituicao = function ($fields) {
            $fields.blur(function () {
                $(this).removeClass('ui-autocomplete-loading');
            })
                .autocomplete({
                    source: function (request, response) {
                        var $this = $(this);
                        var $element = $(this.element);
                        var previous_request = $element.data("jqXHR");

                        if (previous_request) {
                            previous_request.abort();
                        }

                        var arrData = {};
                        var chave = '';

                        if ($element.attr('name') == 'formacaoInstituicao' || $element.attr('name') == 'Instituicao') {
                            chave = 'instituicao';
                        } else if ($element.attr('name') == 'formacaoCurso') {
                            chave = 'curso';
                        }

                        arrData[chave] = request.term;

                        $element.data("jqXHR", $.ajax({
                            url: __Matricula.options.url.buscaFormacao,
                            data: arrData,
                            type: 'POST',
                            async: true,
                            dataType: "json",
                            success: function (data) {
                                var transformed = $.map(data.dados || [], function (el) {
                                    return el[chave];
                                });

                                response(transformed);
                            }
                        }));
                    },
                    minLength: 0
                });
        };

        this.setSelect2Estados = function ($fields) {
            $fields.select2({
                language: 'pt-BR',
                allowClear: true,
                data: function () {
                    return {results: __Matricula.options.data.arrEstados || []};
                }
            });
        };

        this.getCurrentStep = function () {
            return $('.bootstrapWizard').find('li.active');
        };

        this.backStep = function (voltarAoInicio) {
            if (voltarAoInicio) {
                $('.bootstrapWizard').find('a')[0].click();
            } else {
                var arrSteps = $('.bootstrapWizard').find('li.done');
                var current = __Matricula.getCurrentStep();

                $.map(arrSteps, function (el) {
                    if ($(el).val() < $(current).val()) {
                        $(el).find('a').click();
                    }
                });
            }
        };

        this.cleanAllFields = function () {
            var $form = $(__Matricula.options.formElement);
            var $staticForm = $('.form-control-static');
            var $disciplinas = $('#grade-disciplinas');
            var arrFields = $form.find('input');

            $form.find('[name="inscricaoResultado"]').val(null);

            $.map(arrFields, function (el) {
                if (el.id != 'pesCpf') {
                    $(el).val('');

                    if ($(el).data('select2')) {
                        $(el).select2('val', '');
                    }
                }
            });

            $disciplinas.empty();
            $staticForm.text('-');
            $('#infoAdicional').hide();

            __Matricula.steps.dadosBasicos.setDefaults([]);

            $("#perId, #turmaId, #pesCpf, #pesDocEstrangeiro").select2('disable');
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.loadSteps();
            this.setValidations();
            this.wizard();
        };

        return this;
    };

    $.matricula = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-aluno-curso.matricula");

        if (!obj) {
            obj = new Matricula();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-aluno-curso.matricula', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);