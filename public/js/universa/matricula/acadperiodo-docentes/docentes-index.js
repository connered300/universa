(function ($, window, document) {
    'use strict';

    var DocentesIndex = function () {
        VersaShared.call(this);
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlGetInfo: '',
                urlSaveInfo: '',
                urlAnotacoesRel: '',
                urlFrequenciasRel: '',
                urlEncerramentoRel: '',
                data: {},
                queryPeriodo: '',
                values: {
                    periodo: {},
                    monthNames: [
                        "Janeiro",
                        "Fevereiro",
                        "Março",
                        "Abril",
                        "Maio",
                        "Junho",
                        "Julho",
                        "Agosto",
                        "Setembro",
                        "Outubro",
                        "Novembro",
                        "Dezembro"
                    ]
                }
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.showModal = function (data) {
            console.log(priv.options.data.periodo);
            $.ajax(priv.options.urlGetInfo, {
                data: {
                    docente: data.docente_id,
                    periodo: priv.options.data.queryPeriodo
                },
                type: 'POST',
                success: function (result) {
                    result.docente = {
                        docenteId: data.docente_id,
                        docenteNome: data.docente_descricao,
                    };

                    priv.showInfoDocente(result);
                    priv.configInfoDocente();
                    $('#diario-info').modal();
                },
                beforeSend: function () {
                    $('#ajaxLoad').css({display: "block"});
                    jQuery("#turmas-pill-nav").html('');
                    jQuery("#content-docente-disciplinas").html('');
                },
                complete: function () {
                    $('#ajaxLoad').css({display: "none"});
                }
            });
        };

        $('#periodo').select2({
            ajax: {
                url: window.BuscaPeriodos,
                dataType: 'json',
                data: function (query) {
                    return {query: query}
                },
                results: function (data) {
                    var resultado = $.map(data, function (elements) {
                        elements.id = elements.per_id;
                        elements.text = elements.per_nome;

                        return elements;
                    });
                    return {results: resultado};
                }
            }
        });

        $('#periodo').change(function () {
            priv.options.data.queryPeriodo = $(this).val();
            $("#tabela").show();
        });

        priv.showInfoDocente = function (data) {
            $("#docenteName").html("<b>" + data.docente.docenteNome + "</b>");
            var periodo = data.result.periodo;
            var turmas = data.result.turmas;

            for (var turma in turmas) {
                // adicionando lista de turmas
                var templateOptionTurma = $($("#template-turma").html().trim());
                templateOptionTurma.find("a").attr('href', "#tab-r" + turma);
                templateOptionTurma.find('span').html(Object.keys(turmas[turma].disciplinas).length);
                templateOptionTurma.find("a").append(turmas[turma].turmaNome);
                jQuery("#turmas-pill-nav").append(templateOptionTurma);

                // adicionando content de turmas
                var turmaContent = $($("#template-turma-content").html().trim());
                turmaContent.prop("id", "tab-r" + turma);
                jQuery("#content-docente-disciplinas").append(turmaContent);

                var disciplinas = turmas[turma].disciplinas;
                for (var disc in disciplinas) {
                    // adicionando disciplinas
                    var dicId = "collapse-" + disc + "" + turma;
                    var disciplinaContent = $($("#template-disciplina-content").html().trim());

                    disciplinaContent.find("a").prop("href", "#" + dicId);
                    disciplinaContent.find("a").append(turmas[turma].turmaNome + " <b>" +
                        disciplinas[disc].disciplinaNome + "</b>");
                    disciplinaContent.find("div#collapse").prop("id", dicId);
                    jQuery("div#tab-r" + turma).find("div#accordion-2").append(disciplinaContent);

                    // recupera periodo letivo
                    var dateIni = periodo['dateInit'].split('-');
                    var dateFim = periodo['dateFim'].split('-');
                    dateIni = new Date(dateIni[0], dateIni[1] - 1, dateIni[2]);
                    dateFim = new Date(dateFim[0], dateFim[1] - 1, dateFim[2]);

                    priv.options.values.periodo = {
                        id: periodo.id,
                        dateIni: dateIni,
                        dateFim: dateFim
                    };

                    // div de content disciplinas
                    var divContentDisciplinas = $("#" + dicId + " > div.panel-body");
                    var formDiario = $($("#form-diario").html().trim());

                    // adiciona formulario  e insere valores
                    divContentDisciplinas.append(formDiario);
                    formDiario = $("#" + dicId + " #docente-form");
                    formDiario.find("input[name='docdiscId']").val(turmas[turma].docdiscId);
                    var formContent = formDiario.find("#fields-form");

                    var entregaEncerramento = {
                        id: '',
                        data: ''
                    };

                    var entregas = disciplinas[disc].entregas;
                    for (var mesNumber = dateIni.getMonth(); mesNumber <= dateFim.getMonth(); mesNumber++) {
                        // adiciona mes
                        var mesContent = $($("#template-row-mes").html().trim());
                        mesContent.find("#mes").append(priv.options.values.monthNames[mesNumber]);
                        formContent.append(mesContent);
                        mesContent.find("input[name='diarioEntregaMes[]']").val(priv.options.values.monthNames[mesNumber].toLowerCase());

                        if (Object.keys(entregas).length) {
                            var arrNotEncerramento = [];
                            $.each(entregas, function (i, obj) {
                                if (!obj.diarioEncerramentoEntregaData) {
                                    arrNotEncerramento.push(obj);
                                } else {
                                    entregaEncerramento = {
                                        id: obj.diarioEntregaId,
                                        data: obj.diarioEncerramentoEntregaData
                                    };
                                }
                            });

                            for (var ent in arrNotEncerramento) {
                                var mesEntrega = arrNotEncerramento[ent].diarioEntregaMes.toLowerCase();

                                if (mesEntrega == priv.options.values.monthNames[mesNumber].toLowerCase()) {
                                    var anotacoes = arrNotEncerramento[ent].diarioAnotacoesEntregaData || '';
                                    var frequencia = arrNotEncerramento[ent].diarioFrequenciaEntregaData || '';

                                    mesContent.find("input[name='anotacoes[]']").val(anotacoes);
                                    mesContent.find("input[name='diarioEntregaId[]']").val(arrNotEncerramento[ent].diarioEntregaId);
                                    mesContent.find("input[name='frequencia[]']").val(frequencia);
                                    break;
                                }
                            }
                        }

                        /** links de relatorios*/
                        var docdiscId = turmas[turma].docdiscId;
                        // frequencia
                        mesContent.find("#downFrequencia").prop("href",
                            priv.options.urlFrequenciasRel + "?docdiscId=" +
                            docdiscId + "&mes=" + (mesNumber + 1)
                        );
                        // anotacoes
                        mesContent.find("#downAnotacoes").prop("href",
                            priv.options.urlAnotacoesRel + "?docdiscId=" +
                            docdiscId + "&mes=" + (mesNumber + 1)
                        );
                    }

                    // adiciona campo de encerramento
                    var encerrContent = $($("#template-encerramento").html().trim());
                    formContent.append(encerrContent);
                    encerrContent.find("input[name='encerramentoId']").val(entregaEncerramento.id);
                    encerrContent.find("input[name='encerramento']").val(entregaEncerramento.data);
                    // adiciona link de relatorio de encerramento
                    encerrContent.find(".downEncerramento").prop("href",
                        priv.options.urlEncerramentoRel + "?docdiscId=" +
                        docdiscId);
                    encerrContent.find(".downEncerramentoAnotacoes").prop("href",
                        priv.options.urlAnotacoesSemestralRel + "?docdiscId=" +
                        docdiscId
                    );
                }
            }
        }

        priv.configInfoDocente = function () {
            $("input[name='frequencia[]']").each(function () {
                var mes = $(this).parent("div").parent().parent().find("input[name='diarioEntregaMes[]']").val();
                mes = mes.charAt(0).toUpperCase() + mes.substr(1);

                var mesNumber = priv.options.values.monthNames.indexOf(mes);
                var periodoAtual = priv.options.values.periodo;

                mesNumber = (mesNumber == periodoAtual.dateFim.getMonth()) ? mesNumber : mesNumber + 1;
                // adiciona datePicker
                $(this)
                    .datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>',
                        minDate: new Date(periodoAtual.dateIni.getFullYear(), mesNumber, 1),
                        maxDate: new Date(),
                    })
                    .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: 'd/m/y'})
                    .change(function () {
                        if (this.value) {
                            priv.validateFields.validateFrequenciaAnotacoes(this);
                        }
                    });
            });

            $("input[name='anotacoes[]']").each(function () {
                var mes = $(this).parent("div").parent().parent().find("input[name='diarioEntregaMes[]']").val();
                mes = mes.charAt(0).toUpperCase() + mes.substr(1);

                var mesNumber = priv.options.values.monthNames.indexOf(mes);
                var periodoAtual = priv.options.values.periodo;

                mesNumber = (mesNumber == periodoAtual.dateFim.getMonth()) ? mesNumber : mesNumber + 1;
                // adiciona datePicker
                $(this)
                    .datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>',
                        minDate: new Date(periodoAtual.dateIni.getFullYear(), mesNumber, 1),
                        maxDate: new Date(),
                    })
                    .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: 'd/m/y'})
                    .change(function () {
                        if (this.value) {
                            priv.validateFields.validateFrequenciaAnotacoes(this);
                        }
                    });
            });

            $("input[name='encerramento']").each(function () {
                var periodoAtual = priv.options.values.periodo;
                // adiciona datePicker
                $(this)
                    .datepicker({
                        prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>',
                        minDate: new Date(periodoAtual.dateFim.getFullYear(), periodoAtual.dateFim.getMonth()),
                        maxDate: new Date(),
                    })
                    .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: 'd/m/y'})
                    .change(function () {
                        if (this.value) {
                            priv.validateFields.validateEncerramento(this);
                        }
                    });
            });

            var turmaContentId = $("#turmas-pill-nav li:first a").attr('href');
            $("#turmas-pill-nav li:first").addClass('active');
            $(turmaContentId).addClass('active');

            $(turmaContentId + " #accordion-2 [href^=#collapse]:first").removeClass("collapsed");
            $(turmaContentId + " #accordion-2 [id^=collapse]:first").addClass('in');

        }

        priv.getInfoBeforeSend = function ($formEl) {
            var docdiscId = $formEl.find("input[name='docdiscId']").val();
            var periodo = priv.options.values.periodo;

            var frequencias = [];
            $formEl.find("input[name='frequencia[]']").each(function (index) {
                if (this.value) {
                    frequencias[index] = this.value;
                }
            });

            var anotacoes = [];
            $formEl.find("input[name='anotacoes[]']").each(function (index) {
                if (this.value) {
                    anotacoes[index] = this.value;
                }
            });

            var meses = [];
            $formEl.find("input[name='diarioEntregaMes[]']").each(function (index) {
                if (frequencias[index] || anotacoes[index]) {
                    meses[index] = this.value;
                }
            });

            var entregaIds = [];
            $formEl.find("input[name='diarioEntregaId[]']").each(function (index) {
                if ((frequencias[index] || anotacoes[index]) && this.value) {
                    entregaIds[index] = this.value;
                }
            });

            var encerramentoInfo = {
                encerramentoId: $formEl.find("input[name='encerramentoId']").val(),
                encerramentoData: $formEl.find("input[name='encerramento']").val(),
                encerramentoEntrega: $formEl.find("input[name='entregaEncerramento']").val()
            };

            return {
                periodo: periodo,
                docdiscId: docdiscId,
                entregaIds: entregaIds,
                meses: meses,
                anotacoes: anotacoes,
                frequencias: frequencias,
                encerramentoInfo: encerramentoInfo
            };
        };

        priv.validateFields = {
            validateFrequenciaAnotacoes: function ($el, notifies) {
                notifies = (typeof notifies == 'undefined') ? true : notifies;

                $el = $($el);
                var valid;

                var dateCurrent = $el.datepicker("getDate");
                var dateMax = $el.datepicker("option", "maxDate");
                var dateMin = $el.datepicker("option", "minDate");

                valid = (dateCurrent > dateMax) ? false : ((dateCurrent < dateMin) ? false : true);
                valid = true;
                if (notifies && !valid) {
                    $.smallBox({
                        title: "Valor inválido!:",
                        content: "<i>A data informada é inválida!</i>",
                        color: "#C46A69",
                        icon: "fa fa-times",
                        timeout: 10000
                    });

                    $el.val('');
                }

                return valid;
            },
            validateEncerramento: function ($el, allFields, notifies) {
                allFields = (typeof allFields == 'undefined') ? true : allFields;
                notifies = (typeof notifies == 'undefined') ? true : notifies;
                $el = $($el);
                var valid;

                var countFrequencias = $("form:visible input[name='frequencia[]']").length;
                var countAnotacoes = $("form:visible input[name='anotacoes[]']").length;
                var notFoundsFrequencias = [];
                var notFoundsAnotacoes = [];

                $("form:visible input[name='frequencia[]']").each(function () {
                    valid = priv.validateFields.validateFrequenciaAnotacoes(this, false);

                    if (!valid) {
                        var nameMes = $(this).parent("div").parent().parent().find("input[name='diarioEntregaMes[]']").val();
                        nameMes = nameMes.charAt(0).toUpperCase() + nameMes.substr(1);
                        notFoundsFrequencias.push(nameMes);
                    }
                });

                $("form:visible input[name='anotacoes[]']").each(function () {
                    valid = priv.validateFields.validateFrequenciaAnotacoes(this, false);
                    if (!valid) {
                        var nameMes = $(this).parent("div").parent().parent().find("input[name='diarioEntregaMes[]']").val();
                        nameMes = nameMes.charAt(0).toUpperCase() + nameMes.substr(1);
                        notFoundsAnotacoes.push(nameMes);
                    }
                });

                valid = false;

                if (!allFields &&
                    (notFoundsAnotacoes.length < countAnotacoes || notFoundsFrequencias.length < countFrequencias)) {
                    valid = true;
                } else if (notFoundsAnotacoes.length || notFoundsFrequencias.length) {
                    $el.val("");

                    if (notifies) {
                        var messageAnotacoes = "<p>As seguintes <strong>Anotações</strong> não foram entregues:</br>" +
                            notFoundsAnotacoes.join(", ") + "<hr/></p>";
                        var messageFrequencias = "<p>As seguintes <strong>Frequências</strong> não foram entregues:</br>" +
                            notFoundsAnotacoes.join(", ") + "<hr/></p>";

                        var message = messageAnotacoes + messageFrequencias;

                        $.smallBox({
                            title: "<strong>Algumas Anotações ou Frequências não foram entregues</strong>!",
                            content: "<i><hr/><p>" + message + "</p></i>",
                            color: "#C46A69",
                            icon: "fa fa-times",
                            timeout: 20000
                        });
                    }
                    $el.datepicker("refresh");
                }

                return valid;
            }
        }

        priv.setSteps = function () {
            $(document).on("click", "#docentsTable tbody tr", function (event) {
                var position = priv.dataTableDocentes.fnGetPosition(this);
                var data = priv.dataTableDocentes.fnGetData(position);
                priv.showModal(data);
            });

            $(document).on('change', "select[name='optionEtapa']", function (event) {
                var etapa = $(this).val();

                $(".etapas-disc :visible").hide();
                $("fieldset[data-etapa='" + etapa + "']").css("display", "block");
            });

            $(document).on("click", "#salvarEtapa", function () {
                var form = $(this).parents('form');
                var data = priv.getInfoBeforeSend(form);
                var validate = priv.validateFields.validateEncerramento(undefined, false, false);

                if (!validate) {
                    $.smallBox({
                        title: "<strong>Nenhuma entrega encontrada</strong>!",
                        content: "<i><p></p></i>",
                        color: "#C46A69",
                        icon: "fa fa-times",
                        timeout: 20000
                    });

                    return;
                }

                pub.addOverlay($(".modal-content"), 'Aguarde...');

                $.ajax(priv.options.urlSaveInfo, {
                    data: data,
                    type: 'POST',
                    success: function (result) {
                        result = result.result;

                        if (result.type == 'success') {
                            pub.removeOverlay($(".modal-content"));
                            $.smallBox({
                                title: "Sucesso!",
                                content: "<i>Alterações Salvas!</i>",
                                color: "#739e73",
                                icon: "fa fa-check-circle",
                                timeout: 10000
                            });
                        } else if (result.type == 'Warning') {
                            pub.removeOverlay($(".modal-content"));
                            $.smallBox({
                                title: "Warning!",
                                content: '<i>' + result.message + '</i>',
                                color: "#f0ad4e",
                                icon: "fa fa-check-circle",
                                timeout: 10000
                            });

                        } else {
                            pub.removeOverlay($(".modal-content"));
                            $.smallBox({
                                title: "Erro ao salvar!:",
                                content: "<i>" + result.message.join('<br>') + "</i>",
                                color: "#C46A69",
                                icon: "fa fa-times",
                                timeout: 10000
                            });
                        }
                    },
                    beforeSend: function () {
                        // $('#ajaxLoad').css({display: "block"});
                    },
                    complete: function () {
                        // $('#ajaxLoad').css({display: "none"});
                    }
                });
            });
        };

        priv.prepareSteps = function () {
            $('#periodo').change(function () {
                priv.options.data.queryPeriodo = $(this).val();
                var docentes = $('#docentsTable');

                if (docentes.dataTable() === true) {
                    docentes.dataTable().destroy();
                }

                priv.dataTableDocentes = docentes.dataTable({
                    processing: true,
                    serverSide: true,
                    searching: true,
                    destroy: true,
                    ajax: {
                        url: priv.options.urlSearch,
                        type: "POST",
                        data: {perId: priv.options.data.queryPeriodo},
                    },
                    columnDefs: [
                        {name: "docente_id", data: "docente_id"},
                        {name: "docente_descricao", targets: 0, data: "docente_descricao"},
                        {name: "discNum", targets: 1, data: "discNum"},
                        {name: "Pendencia", targets: 2, data: "Pendencia"},
                        {name: "anotacao", targets: 3, data: "anotacao"},
                        {name: "Pendencia2", targets: 4, data: "Pendencia2"}
                    ],
                    "dom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
                    "oTableTools": {
                        "aButtons": []
                    },
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0]]
                });
            });
        },

            pub.run = function (opts) {
                priv.setDefaults(opts);
                priv.setSteps();
                priv.prepareSteps();
            };
    };

    $.docentesIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.docentes.index");

        if (!obj) {
            obj = new DocentesIndex();
            obj.run(params);
            $(window).data('universa.docentes.index', obj);
        }

        return obj;
    };

})(window.jQuery, window, document);
