/**
 * Created by eduardo on 12/13/16.
 */

$(document).ready(function(){
    etapaDataTable();
    disciplinasDataTable();
});

/***********************************************************/
// FUNÇÃO QUE CRIA O DATATABLES PARA O INDEX
// E REDIRECIONA PARA AS DISCIPLINAS
/***********************************************************/

function etapaDataTable(){
    var table = $("#dataTableEtapa");
    if(!$.isEmptyObject(table.DataTable())) {
        table.DataTable().destroy();
    }
    table.DataTable({
        "processing": true,
        "serverSide": true,
        "searching": true,
        "ajax": {
            "url": window.arrJson,
            "type": "POST",
            "async": false,
            dataSrc: function (json) {
                var data = json.data;
                var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-danger acadgeralDisciplina-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                for (var row in data) {
                    var urlEdit = 'listagem-disciplinas' + '/' + data[row]['Matricula'];
                    data[row]['acao']            = btnGroup;
                    data[row]['Matricula']         = "<a href='"+urlEdit+"'>"+data[row]['Matricula']+"</a>";
                    data[row]['Nome']       = "<a href='"+urlEdit+"'>"+data[row]['Nome']+"</a>";
                    data[row]['Qtd']      = "<a href='"+urlEdit+"' style=''>"+data[row]['Qtd']+"</a>";
                    data[row]['Turma'] = "<a href='" + urlEdit + "'>" + data[row]['Turma'] + "</a>";
                    data[row]['Periodo'] = "<a href='" + urlEdit + "'>" + data[row]['Periodo'] + "</a>";
                    data[row]['Pendencia'] = "<a href='" + urlEdit + "'>" + data[row]['Pendencia'] + "</a>";
                }

                return data;
            }
        },
        "sDom": "<'dt-toolbar'<'col-xs-6'f>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",
        "columns": [
            {"name": "Matricula", targets: 0, data: "Matricula"},
            {"name": "Nome", targets: 0, data: "Nome"},
            {"name": "Turma", targets: 0, data: "Turma"},
            {"name": "Periodo", targets: 0, data: "Periodo"},
            {"name": "Qtd", targets: 0, data: "Qtd"},
            {"name": "Pendencia", targets: 0, data: "Pendencia"}
        ],
        oLanguage: {
            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
            sLengthMenu: "Mostrar _MENU_ registros por página",
            sZeroRecords: "Nenhum registro encontrado",
            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
            sInfoFiltered: "(filtrado de _MAX_ registros)",
            sSearch: "",
            oPaginate: {
                sFirst: "Início",
                sPrevious: "Anterior",
                sNext: "Próximo",
                sLast: "Último"
            }
        }
    });
}

/***********************************************************/
// FUNÇÃO QUE CRIA O DATATABLES PARA AS DISCIPLINAS
// PEGA A MATRÍCULA QUE VEIO DO INDEX
/***********************************************************/

function disciplinasDataTable(){
    var table = $("#dataTableDisciplinas");
    if(!$.isEmptyObject(table.DataTable())) {
        table.DataTable().destroy();
    }
    table.DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": window.disciplinas,
            "type": "POST",
            data: function (d){
                d.matricula = matricula;
                return d;
            },
            dataSrc: function (json) {
                    var data = json.data;

                    $.map(data,function(t){
                        $('#matricula').text(t.Matricula);
                        $('#turma').text(t.Turma);
                        $('#nome').text(t.Nome);
                    });

                    var btnGroup = '\
                                    <div class="text-center">\
                                        <div class="btn-group">\
                                            <button type="button">\
                                                <i class="fa fa-times fa-inverse"></i>\
                                            </button>\
                                        </div>\
                                    </div>';

                    for (var row in data) {
                        var urlEdit = '/matricula/aluno/index/' + data[row]['Matricula']
                        data[row]['acao']            = btnGroup;
                        data[row]['Disciplina']         = "<a href='" + urlEdit + "'>"+data[row]['Disciplina']+"</a>";
                        data[row]['Periodo']         = "<a href='" + urlEdit + "'>"+data[row]['Periodo']+"</a>";
                    }

                return data;
            }
        },
        "dom": "r" +
               "t" +
               "<'dt-toolbar-footer'<'col-sm-12'p>>",
        "columns": [
            {"name": "Disciplinas", targets: 0, data: "Disciplina"},
            {"name": "Períodos", targets: 0, data: "Periodo"},
        ],
        oLanguage: {
            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
            sLengthMenu: "Mostrar _MENU_ registros por página",
            sZeroRecords: "Nenhum registro encontrado",
            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
            sInfoFiltered: "(filtrado de _MAX_ registros)",
            sSearch: "",
            oPaginate: {
                sFirst: "Início",
                sPrevious: "Anterior",
                sNext: "Próximo",
                sLast: "Último"
            }
        }
    });
}