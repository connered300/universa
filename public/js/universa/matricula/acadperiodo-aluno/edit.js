$(document).ready(function () {
    $("#PeriodoLetivo").on("change", function () {
        EscondeFormBuscaDisciplina();
    });

    $("#alunoTurmaPrincipalId").on("change", function () {
        EscondeFormBuscaDisciplina();
    });


    $("#PessoaFisica").on('submit', function () {
        if ($("input[name='alunoTurmaPrincipal']").val() == undefined) {
            alert("Favor selecionar a turma principal do aluno na seção: Turmas e Disiciplinas!");
            return false;
        }
        $("#next").prop("disabled", true);
    });


    //$("#next").attr('disabled', true);

    var situacoesNotIncluse = ['Matriculado'];
    var disciplinasOriginais = [];
    var arrTiposTurma = [];
    var respNumero = 0;


    //Feito pelo caio - gordo
    $("input[name=pesCpf]:visible").change(function () {
        var cpfAluno = $(this).val();
        $.ajax({
            async: true,
            type: "POST",
            url: "/pessoa/pessoa/busca-dados-pessoais",
            data: {
                cnpjCpf: cpfAluno,
                endereco: true,
                contato: true,
                atributos: {
                    nome: {classe: "Pes", atributo: "PesNome"},
                    id: {classe: "Pes", atributo: "PesId"},
                    rg: {atributo: "PesRg"},
                    rgEmissao: {atributo: "pesRgEmissao"},
                    dataNascimento: {atributo: "pesDataNascimento"},
                    sexo: {atributo: "pesSexo"}
                }
            },

            success: function (data) {
                if (data.nome != undefined) {
                    $("input[name=pes]").val(data.id);
                    $("input[name=pesNome]:visible").val(data.nome);
                    $("input[name=pesRg]:visible").val(data.rg);
                    $("input[name=pesRgEmissao]:visible").val(data.rgEmissao);
                    $("input[name=pesRgEmissao]:visible").val(data.dataNascimento);
                    $("input:radio[name=pesSexo]:visible").filter("[value=" + data.sexo + "]").attr("checked", true);
                    $("input[name='endCep[]']:visible").val(data.cep);
                    $("input[name='endLogradouro[]']:visible").val(data.logradouro);
                    $("input[name='endNumero[]']:visible").val(data.numero);
                    $("input[name='endComplemento[]']:visible").val(data.complemento);
                    $("input[name='endBairro[]']:visible").val(data.bairro);
                    $("input[name='endCidade[]']:visible").val(data.cidade);
                    $("select[name='endEstado[]']:visible").val(data.estado);
                    $("input[name='alunoEmail']:visible").val(data.email);
                    $("input[name='alunoTelefone']:visible").val(data.telefone);
                }
            },
            error: function (a) {
                alert("Erro ao conectar ao servidor verifique sua conexão a internet!")
            }

        })
    });

    function strip(str, c) {
        var tmp = str.split(c);
        return tmp.join("");
    }

    $(".cnpjCpf").keyup(function () {
        var tmp = strip($(this).val(), ".");
        tmp = strip(tmp, "/");
        tmp = strip(tmp, "-");
        if (12 < tmp.length) $(this).val(tmp.substr(0, 2) + '.' + tmp.substr(2, 3) + '.' + tmp.substr(5, 3) + '/' + tmp.substr(8, 4) + '-' + tmp.substr(12, 2));
        else if (9 < tmp.length) $(this).val(tmp.substr(0, 3) + '.' + tmp.substr(3, 3) + '.' + tmp.substr(6, 3) + '-' + tmp.substr(9, 3));
        else if (6 < tmp.length) $(this).val(tmp.substr(0, 3) + '.' + tmp.substr(3, 3) + '.' + tmp.substr(6, 3));
        else if (3 < tmp.length)  $(this).val(tmp.substr(0, 3) + '.' + tmp.substr(3, 3));
        else $(this).val(tmp);
    });
    $(".cnpjCpf").on("keydown", function (event) {
        var aKey;

        if (window.event)
            aKey = event.keyCode;
        else
            aKey = event.which;

        // if das teclas precionadas
        if (!(((aKey == 67) && (event.ctrlKey)) || ((aKey == 86) && (event.ctrlKey)) || (aKey == 8) || (aKey == 9) || (aKey == 13) || (aKey == 16) || (aKey == 17) || (aKey == 92)
            || ((aKey >= 96) && (aKey <= 105)) || ((aKey >= 48) && (aKey <= 57)) || ((aKey >= 37) && (aKey <= 40)) || ((aKey >= 112) && (aKey <= 123)))) {
            return false;
        }
    });
    $(".cnpjCpf").blur(function () {
        var tamanho = this.value.length;
        if (tamanho == 18) {
            $("[name='respPesRg']").parent().parent().hide();
            $("[name='respPesRgEmissao']").parent().parent().hide();
        } else {
            $("[name='respPesRg']").parent().parent().show();
            $("[name='respPesRgEmissao']").parent().parent().show();
        }
    });


    $("input[name='pesDocEstrangeiro']").parent().parent().hide();

    $("select[name='pesNacionalidade']").change(function () {
        if ($(this).val() == 'Estrangeiro') {
            $("input[name='pesCpf']").parent().parent().hide();
            $("input[name='pesDocEstrangeiro']").parent().parent().show();
        } else {
            $("input[name='pesCpf']").parent().parent().show();
            $("input[name='pesDocEstrangeiro']").parent().parent().hide();
        }
    });


    // inicio busca reponsaveis atuais
    var responsaveis = [];
    var i = 0;
    $("input[name='reponsavelAtual[]']").each(function () {
        responsaveis[i] = $(this).val();
        i++;
    });

    $.ajax({
        async: true,
        type: "POST",
        url: "/matricula/acadgeral-responsavel/busca-dados-responveis",
        data: {
            responsaveis: responsaveis
        },

        success: function (data) {
            if (data.responsaveis !== undefined) {
                for (i = 0; i < data.responsaveis.length; i++) {
                    var html = "<tr data-target='" + respNumero + "'>" +
                        "<td>" +
                        "<a name='editarResponsavel' data-target='" + respNumero + "'>Editar</a> | " +
                        "<a name='removeResponsavel' data-target='" + respNumero + "'>Remover</a>" +
                        "<input type='hidden' name='responsavelId[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['respId'] + "'>" +
                        "<input type='hidden' name='responsavelAluno[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['aluno'] + "'>" +
                        "<input type='hidden' name='responsavelPes[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['pes'] + "'>" +
                        "<input type='hidden' name='responsavelNivel[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['respNivel'] + "'>" +
                        "<input type='hidden' name='responsavelCnpjCpf[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['pesCnpjCpf'] + "'>" +
                        "<input type='hidden' name='responsavelNome[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['pesNome'] + "'>" +
                        "<input type='hidden' name='responsavelDocEstrangeiro[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['pesDocEstrangeiro'] + "'>" +
                        "<input type='hidden' name='responsavelVinculo[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['respVinculo'] + "'>" +
                        "<input type='hidden' name='responsavelRg[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['pesRg'] + "'>" +
                        "<input type='hidden' name='responsavelRgEmissao[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['pesRgEmissao'] + "'>" +
                        "<input type='hidden' name='responsavelNacionalidade[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['pesNacionalidade'] + "'>" +
                        "<input type='hidden' name='responsavelTipoEndereco[]' targetOff='" + respNumero + "' value='1'>" +
                        "<input type='hidden' name='responsavelCep[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['endCep'] + "'>" +
                        "<input type='hidden' name='responsavelLogradouro[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['endLogradouro'] + "'>" +
                        "<input type='hidden' name='responsavelCidade[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['endCidade'] + "'>" +
                        "<input type='hidden' name='responsavelEstado[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['endEstadoNome'] + "'>" +
                        "<input type='hidden' name='responsavelBairro[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['endBairro'] + "'>" +
                        "<input type='hidden' name='responsavelNumero[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['endNumero'] + "'>" +
                        "<input type='hidden' name='responsavelComplemento[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['endComplemento'] + "'>";
                    if (data.responsaveis[i]['Telefone']) {
                        html += "<input type='hidden' name='responsavelTelefone[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['Telefone'][0]['conContato'] + "'>";
                    }
                    if (data.responsaveis[i]['Celular']) {
                        html += "<input type='hidden' name='responsavelCelular[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['Celular'][0]['conContato'] + "'>";
                    }
                    if (data.responsaveis[i]['E-mail']) {
                        html += "<input type='hidden' name='responsavelEmail[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['E-mail'][0]['conContato'] + "'>";
                    }
                    html +=
                        "</td>" +
                        "<td>" + data.responsaveis[i]['pesNome'] + "</td>" +
                        "<td>" + data.responsaveis[i]['pesCnpjCpf'] + "</td>" +
                        "<td>" + $("select[name='respVinculo[]']").find("option[value='" + data.responsaveis[i]['respVinculo'] + "']").text() + "</td>" +
                        "</tr>";
                    $('#tableResponsaveis').append(html);
                    var html2 = '';
                    if (data.responsaveis[i]['Telefone']) {
                        html2 += "<input type='hidden' name='responsavelTelefoneId[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['Telefone'][0]['conId'] + "'>" +
                            "<input type='hidden' name='responsavelTipoTelefone[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['Telefone'][0]['tipoContato'] + "'>";
                    }
                    if (data.responsaveis[i]['Celular']) {
                        html2 += "<input type='hidden' name='responsavelCelularId[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['Celular'][0]['conId'] + "'>" +
                            "<input type='hidden' name='responsavelTipoCelular[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['Celular'][0]['tipoContato'] + "'>";
                    }
                    if (data.responsaveis[i]['E-mail']) {
                        html2 += "<input type='hidden' name='responsavelEmailId[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['E-mail'][0]['conId'] + "'>" +
                            "<input type='hidden' name='responsavelTipoEmail[]' targetOff='" + respNumero + "' value='" + data.responsaveis[i]['E-mail'][0]['tipoContato'] + "'>";
                    }
                    $('#tableResponsaveis').parent().append(html2);
                    respNumero++;
                }

            }
        },
        error: function (a) {
            alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
        }

    });

    // fim busca reponsaveis atuais

    //Determina a quantidade inicial de periodos
    var quantPeriodos = $("select[name='curso'] option:selected").attr('data-periodos');
    var serieAluno = $("#serieAluno").val();
    for (i = 1; i <= quantPeriodos; i++) {
        if (serieAluno == i) {
            $("select[name='alunoPeriodo']").append("<option value=" + i + " selected>" + i + "</option>");

        } else {
            $("select[name='alunoPeriodo']").append("<option value=" + i + ">" + i + "</option>");
        }
    }


    //busca turmas para o curso com base no curso, câmpus e periodo
    $("select[name='alunoPeriodo']").change(function (event) {

        var dataDisciplinas = [];
        if (disciplinasOriginais instanceof jQuery) {
            disciplinasOriginais.find("select[name^=disciplinaSituacao]").each(function (index) {
                var $this = $(this);
                var $pai = $this.closest('.row');

                var $tipoDisciplina = $pai.find("input[name^=disciplinaTipo]");
                var $tipoTurmaId = $pai.find("input[name^=tipoTurmaId]");
                var $tipoDisciplinaOpt = $pai[0].outerHTML;

                if (situacoesNotIncluse.indexOf($this.val()) == -1 || $tipoDisciplina.val() != 'Regular' || $tipoTurmaId.val() == arrTiposTurma.Optativa) {
                    var disc = $pai.find("input[name='disciplina[]']").val();
                    dataDisciplinas[disc] = $tipoDisciplinaOpt;
                }
            });
        }

        buscaDisciplinas(dataDisciplinas);
    });

    function buscaDisciplinas(dataDisciplinas) {
        var matriz = $("select[name=alunoTurmaPrincipal] option:selected").attr("data-matriz");
        var periodo = $("select[name=alunoPeriodo] option:selected").val();

        var $turmas = $("select[name=alunoTurmaPrincipal]");
        $("input[name='alunoTurmaPrincipal']").val($turmas.val());

        var turmaConvencional = ($("select[name=alunoTurmaPrincipal] option:selected").attr("data-tipoturma") == "Convencional");
        dataDisciplinas = dataDisciplinas || false;


        if (!dataDisciplinas) {
            $("#blocoDisciplinas > div").detach();
        } else {
            $("#blocoDisciplinas").children().each(function () {
                var $this = $(this);
                var disc = $this.find('[name="disciplina[]"]').val();

                if (!dataDisciplinas[disc]) {
                    $(this).detach();
                }
            });
        }

        // Atualizar a grade de turmas apenas se a turma principal for do tipo Convencional
        if (turmaConvencional) {
            $.ajax({
                async: false,
                type: "POST",
                url: "/matricula/acadperiodo-matriz-disciplina/busca-disciplinas",
                data: function (e) {
                    return {
                        query: e,
                        matriz: matriz,
                        periodo: periodo
                    };
                },
                success: function (data) {
                    var situacoes = "";

                    $("input[name='situacao']").each(function () {
                        situacoes = situacoes + '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';
                    });

                    if (data.disciplinas !== undefined) {
                        for (i = 0; i < data.disciplinas.length; i++) {
                            var disc = data.disciplinas[i].disc;

                            if (!dataDisciplinas[disc]) {
                                var $disc = $('<div class="row">' +
                                    '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                                    '<label class="input">' +
                                    '<input type="hidden" name="novaTurma[]" data-position="' + i + '" value="true">' +
                                    '<input type="hidden" name="disciplina[]" value="' + disc + '">' +
                                    '<input type="hidden" name="disciplinaTipo[]" value="' + data.disciplinas[i].discTipoNome + '">' +
                                    '<i></i> ' + data.disciplinas[i].discNome +
                                    '</label>' +
                                    '</section>' +
                                    '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                                    '<label class="select">' +
                                    '<i></i>' +
                                    '<section class="col-lg-2 col-sm-2 col-xs-2 input" >' +
                                    '<input type="text" name="disciplinaTurma[]">' +
                                    '</section>' +
                                    '</label>' +
                                    '</section>' +
                                    '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                                    '<label class="select">' +
                                    '<i></i>' +
                                    '<select name="disciplinaSituacao[]" >' +
                                    situacoes +
                                    '</select>' +
                                    '</label>' +
                                    '</section>' +
                                    '</div>');
                                $("#blocoDisciplinas").append($disc);
                                $disc.find("select[name='disciplinaTurma[]']").val($turmas.val());
                            } else {
                                //mesmo sendo as mesmas disciplinas pela terceira vez nao entre aqui, somente setar o valor das turmas
                                $("#blocoDisciplinas").find("input[name='disciplina[]']").each(function () {
                                    if ($(this).val() == disc) {
                                        $(this).parent().parent().parent().find("select[name='disciplinaTurma[]']").val($turmas.val());
                                    }
                                });

                            }
                        }

                        $("#blocoDisciplinas").show();

                        $(".select2-container").css("width", "100%");
                        $(".novaDisciplina").css("width", "15%");
                    }
                },
                error: function (a) {
                    alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
                }

            });
        }
    }

    $('#respAdd').click(function () {
        if ($('#resp').hasClass('hidden')) {
            $('#resp').removeClass('hidden');
            $spine().validateForm();
        } else {
            $('#resp').addClass('hidden');
        }
        if (!$('#editaResp').hasClass('hidden')) {
            $('#editaResp').addClass('hidden');
            $('#adicionarResp').removeClass('hidden');
        }
        limpaFormResponsavel();
    });
    $("select[name='respPesNacionalidade']").change(function () {
        if (this.value == 'Estrangeiro') {
            $("#documentoResp").append("<section class='col-lg-8 col-sm-8 col-xs-8' style='padding-left:30px;'><label class='label'>Documento Estrangeiro:</label><label class='input'><i class='icon-prepend fa fa-credit-card'></i><input name='respPesDocEstrangeiro' placeholder='Informe o documento estrangeiro' data-validations='Required' value='' type='text'></label><div class='note'></div></section>");
            $("input[name='respPesCnpjCpf']").parent().parent().addClass('hidden');
        }
        else {
            $("input[name='respPesCnpjCpf']").parent().parent().removeClass('hidden');
            $("input[name='respPesDocEstrangeiro']").parent().parent().detach();
        }
    });
    $('#adicionarResp').click(function () {
        if ($("input[name='respPesCnpjCpf']").val() == $("input[name='pesCpf']").val()) {
            alert("O responsável não pode ser o proprio aluno!");
            $("input[name='respPesCnpjCpf']").focus();
            return;
        }
        if (stepValidate(valida, 0)) {
            adicionaNovoResponsavel();
            $('#resp').addClass('hidden');
        }
    });
    $(document).on('click', "a[name='editarResponsavel']", function () {
        var target = $(this).attr('data-target');
        var resp = {};
        $("input[targetOff='" + target + "']").each(function () {
            resp[this.name] = this.value;
        });

        if ($('#resp').hasClass('hidden')) {
            $('#resp').removeClass('hidden');
        }
        $("select[name='respNivel[]']").val(resp['responsavelNivel[]']);
        $("input[name='respPesCnpjCpf']").val(resp['responsavelCnpjCpf[]']);
        $("input[name='respPesNome']").val(resp['responsavelNome[]']);
        $("input[name='respPesDocEstrangeiro']").val(resp['responsavelDocEstrangeiro[]']);
        $("select[name='respVinculo[]']").val(resp['responsavelVinculo[]']);
        $("input[name='respPesRg']").val(resp['responsavelRg[]']);
        $("input[name='respPesRgEmissao']").val(resp['responsavelRgEmissao[]']);
        $("select[name='respPesNacionalidade']").val(resp['responsavelNacionalidade[]']);
        $("input[name='endCep[]']:visible").val(resp['responsavelCep[]']);
        $("input[name='endLogradouro[]']:visible").val(resp['responsavelLogradouro[]']);
        $("input[name='endCidade[]']:visible").val(resp['responsavelCidade[]']);
        $("select[name='endEstado[]']:visible").val(resp['responsavelEstado[]']).trigger('change');
        $("input[name='endNumero[]']:visible").val(resp['responsavelNumero[]']);
        $("input[name='endBairro[]']:visible").val(resp['responsavelBairro[]']);
        $("input[name='endComplemento[]']:visible").val(resp['responsavelComplemento[]']);
        $("input[name='respTelefone']:visible").val(resp['responsavelTelefone[]']);
        $("input[name='respCelular']:visible").val(resp['responsavelCelular[]']);
        $("input[name='respEmail']:visible").val(resp['responsavelEmail[]']);

        if ($('#editaResp').hasClass('hidden')) {
            $('#editaResp').removeClass('hidden');
            $('#adicionarResp').addClass('hidden');
        }
        $('#editaResp').attr('data-target', target);

    });
    $("#editaResp").click(function () {
        var targ = $(this).attr('data-target');
        respNumero = targ;
        $("tr[data-target='" + targ + "']").detach();
        adicionaNovoResponsavel();
        $('#resp').addClass('hidden');
    });

    function adicionaNovoResponsavel() {
        var responsavel = {};
        $("[name^='resp']:visible").each(function () {
            responsavel[this.name] = this.value;
        });

        responsavel['pesNacionalidade'] = $("select[name='respPesNacionalidade']").val();
        responsavel['endCep[]'] = $("input[name='endCep[]']:visible").val();
        responsavel['endLogradouro[]'] = $("input[name='endLogradouro[]']:visible").val();
        responsavel['endCidade[]'] = $("input[name='endCidade[]']:visible").val();
        responsavel['endEstado[]'] = $("select[name='endEstado[]']:visible").val();
        responsavel['endBairro[]'] = $("input[name='endBairro[]']:visible").val();
        responsavel['endNumero[]'] = $("input[name='endNumero[]']:visible").val();
        responsavel['endComplemento[]'] = $("input[name='endComplemento[]']:visible").val();

        $('#tableResponsaveis').append(
            "<tr data-target='" + respNumero + "'>" +
            "<td>" +
            "<a name='editarResponsavel' data-target='" + respNumero + "'>Editar</a> | " +
            "<a name='removeResponsavel' data-target='" + respNumero + "'>Remover</a>" +
            "<input type='hidden' name='responsavelNivel[]' targetOff='" + respNumero + "' value='" + responsavel['respNivel[]'] + "'>" +
            "<input type='hidden' name='responsavelCnpjCpf[]' targetOff='" + respNumero + "' value='" + responsavel['respPesCnpjCpf'] + "'>" +
            "<input type='hidden' name='responsavelNome[]' targetOff='" + respNumero + "' value='" + responsavel['respPesNome'] + "'>" +
            "<input type='hidden' name='responsavelDocEstrangeiro[]' targetOff='" + respNumero + "' value='" + responsavel['respPesDocEstrangeiro'] + "'>" +
            "<input type='hidden' name='responsavelVinculo[]' targetOff='" + respNumero + "' value='" + responsavel['respVinculo[]'] + "'>" +
            "<input type='hidden' name='responsavelRg[]' targetOff='" + respNumero + "' value='" + responsavel['respPesRg'] + "'>" +
            "<input type='hidden' name='responsavelRgEmissao[]' targetOff='" + respNumero + "' value='" + responsavel['respPesRgEmissao'] + "'>" +
            "<input type='hidden' name='responsavelNacionalidade[]' targetOff='" + respNumero + "' value='" + responsavel['pesNacionalidade'] + "'>" +
            "<input type='hidden' name='responsavelTipoEndereco[]' targetOff='" + respNumero + "' value='1'>" +
            "<input type='hidden' name='responsavelCep[]' targetOff='" + respNumero + "' value='" + responsavel['endCep[]'] + "'>" +
            "<input type='hidden' name='responsavelLogradouro[]' targetOff='" + respNumero + "' value='" + responsavel['endLogradouro[]'] + "'>" +
            "<input type='hidden' name='responsavelCidade[]' targetOff='" + respNumero + "' value='" + responsavel['endCidade[]'] + "'>" +
            "<input type='hidden' name='responsavelEstado[]' targetOff='" + respNumero + "' value='" + responsavel['endEstado[]'] + "'>" +
            "<input type='hidden' name='responsavelBairro[]' targetOff='" + respNumero + "' value='" + responsavel['endBairro[]'] + "'>" +
            "<input type='hidden' name='responsavelNumero[]' targetOff='" + respNumero + "' value='" + responsavel['endNumero[]'] + "'>" +
            "<input type='hidden' name='responsavelComplemento[]' targetOff='" + respNumero + "' value='" + responsavel['endComplemento[]'] + "'>" +
            "<input type='hidden' name='responsavelTelefone[]' targetOff='" + respNumero + "' value='" + responsavel['respTelefone'] + "'>" +
            "<input type='hidden' name='responsavelCelular[]' targetOff='" + respNumero + "' value='" + responsavel['respCelular'] + "'>" +
            "<input type='hidden' name='responsavelEmail[]' targetOff='" + respNumero + "' value='" + responsavel['respEmail'] + "'>" +
            "</td>" +
            "<td>" + responsavel['respPesNome'] + "</td>" +
            "<td>" + responsavel['respPesCnpjCpf'] + "</td>" +
            "<td>" + $("select[name='respVinculo[]']").find("option[value='" + responsavel['respVinculo[]'] + "']").text() + "</td>" +
            "</tr>"
        );
        limpaFormResponsavel();
        respNumero++;
    }

    function limpaFormResponsavel() {
        $("select[name='respNivel[]']").val('NULL');
        $("input[name='respPesCnpjCpf']").val('');
        $("input[name='respPesNome']").val('');
        $("input[name='respPesDocEstrangeiro']").val('');
        $("select[name='respVinculo[]']").val('NULL');
        $("input[name='respPesRg']").val('');
        $("input[name='respPesRgEmissao']").val('');
        $("select[name='respPesNacionalidade']").val("NULL");
        $("input[name='endCep[]']:visible").val("");
        $("input[name='endLogradouro[]']:visible").val("");
        $("input[name='endCidade[]']:visible").val("");
        $("select[name='endEstado[]']:visible").val("NULL");
        $("input[name='endNumero[]']:visible").val("");
        $("input[name='endComplemento[]']:visible").val("");
        $("input[name='respTelefone']:visible").val("");
        $("input[name='respEmail']:visible").val("");
        $("input[name='respCelular']:visible").val("");
    }

    // Script para fazer a mudança de um passo para outro.
    var valida = {
        0: {
            'respNivel[]': 'select',
            respPesNacionalidade: 'select',
            respPesCnpjCpf: 'input',
            respPesNome: 'input',
            respPesDocEstrangeiro: 'input',
            'respVinculo[]': 'select',
            respPesRg: 'input',
            respPesRgEmissao: 'input',
            'endCep[]': 'input',
            'endLogradouro[]': 'input',
            'endBairro[]': 'input',
            'endCidade[]': 'input',
            'endEstado[]': 'select'
        }
    };


    // Funçao que valida os campos de um passo do wizard, recebe como parametro um objeto contendo o passo ex:
    //    validations = {
    //        step1: { Aqui esta o nome do passo, que no caso e o id do <li> da lista de passos.
    //            pesNome: 'input' Aqui temos o nome do campo e o tipo dele.
    //        }
    //    }
    function stepValidate(validacao, passo) {
        var stop = false;
        if (!validacao[passo]) {
            return false;
        }
        for (elementoNome in validacao[passo]) {
            //Recebe o elemento que será validado.
            elemento = $(validacao[passo][elementoNome] + "[name='" + elementoNome + "']:visible");

            //Recebe as funções de validação que serão executadas para o elemento.
            funcao = $(elemento).attr('data-validations');
            if (funcao) {
                if (funcao.indexOf(',')) {
                    funcao = funcao.split(',');
                }
                for (i = 0; i < funcao.length; i++) {
//                retorno = $spine(elemento).funcValidations[ funcao[i] ](elemento.value);
                    if (!$spine(elemento).funcValidations[funcao[i]]($(elemento).val())) {
                        $(elemento).parent().addClass('state-error');
                        $(elemento).parent().focus();
                        $(elemento).parent().parent().find("div[class^='note']").addClass('note-error').html($spine().msgValidations[funcao[i]]);
                        stop = true;
                        break;
                    } else {
                        $(elemento).parent().removeClass('state-error');
                        $(elemento).parent().parent().find("div[class^='note']").removeClass('note-error').html("");
                    }
                }
            }
        }
        return !stop;
    }

    $("#next").click(function () {

        var vs = VersaShared(this);

        var step = $("ul[class='steps']").find("li.active").attr('data-target');

        if ($(this).text() == 'Salvar') {
            if ($("#next")[0].type == "submit") {
                var erro = false;

                if (!$('#novoCursoId').val()) {
                    vs.showNotificacaoWarning("Selecione o curso do aluno!");
                    erro = true;
                }

                if (!$('[name="alunoTurmaPrincipal"]').val()) {
                    vs.showNotificacaoWarning("Selecione a turma principal do aluno!");
                    erro = true;
                }

                if (!$('[name="periodoLetivo"]').val()) {
                    vs.showNotificacaoWarning("Selecione o período letivo!");
                    erro = true;
                }

                var disciplinas = [],
                    disciplinaTurma;

                //if (!$('[name="disciplina[]"]').val()) {
                //    vs.showNotificacaoWarning("Selecione pelo menos uma disciplina!");
                //    erro = true;
                //}

                $.each($("[name='disciplina[]']"), function (index, value) {
                    var $camp = $(value).closest('.row').find('[name^=disciplinaTurma]');

                    if (!$camp.val()) {
                        vs.showNotificacaoWarning("Selecione todas as turmas para as disciplinas!");
                        erro = true;
                    }
                });


                if (erro) {
                    $("#next")[0].type = "button";
                    $("#prev").click();
                    return false;
                }

                vs.addOverlay($(".widget-body"));
                vs.showNotificacaoInfo("Aguarde enquanto os dados estão sendo salvos!");
            }

            $("form[name='PessoaFisica']").submit();
        }
        if (step == "#step3") {
            $("#next").text("Salvar");
        }

        validations = {
            step1: {
                pesNome: 'input',
                alunoFormaIngresso: 'select',
                pesNacionalidade: 'select',
                pesCpf: 'input',
                pesRg: 'input',
                //pesRgEmissao: 'input',
                pesSexo: 'input',
                alunoEtnia: 'select',
                pesFalecido: 'select',
                alunoPai: 'input',
                alunoMae: 'input',
                'endCep[]': 'input',
                'endLogradouro[]': 'input',
                'endNumero[]': 'input',
                'endBairro[]': 'input',
                'endCidade[]': 'input',
                'endEstado[]': 'select',
                //alunoTelefone: 'input',
                alunoCelular: 'input',
                pesDataNascimento: 'input',
                alunoEmail: 'input'
            },
            step2: {
                origem: 'select'
            },
            step3: {
//                    curso: 'select',
//                    alunoPeriodo: 'select',
//                    alunoTurmaPrincipal: 'select'
            }
        };
        if (stepValidate(validations, step.replace('#', ''))) {
            proximoStep = parseInt(step.replace('#step', '')) + 1;
            proximoStep = '#step' + proximoStep;

            if ($("li[data-target='" + proximoStep + "']")) {
                $(step).removeClass('active');
                $('#prev').removeAttr('disabled');
                if ($(proximoStep).length > 0) {
                    $("ul[class='steps']").find("li.active").attr('data-target', step).addClass('complete').removeClass('active');
                    $(proximoStep).addClass('active');
                    $("li[data-target='" + proximoStep + "']").addClass('active');
                    $spine().validateForm();
                }
            }

            if (step == "#step3") {
                $("#next").attr("type", "submit");

                $("form[name='PessoaFisica']").submit(function () {
                    if (stepValidate(validations, 'step3')) {
                        $("select[name='alunoPeriodo']").removeAttr('disabled');
                        return true;
                    } else {
                        $("#next").removeAttr('disabled');
                        return false;
                    }
                });
            }
        }
        $("li[class='complete']").bind('click', function () {
            atual = $('ul.steps').find("li.active").attr('data-target');
            atual = parseInt(atual.replace('#step', ''));
            destino = $(this).attr('data-target');
            destino = parseInt(destino.replace('#step', ''));
            while (atual > destino) {
                $("li[data-target='#step" + atual + "']").removeClass('active complete');
                $("#step" + atual).removeClass('active')
                atual--;
            }
            if (atual == 1) {
                $('#prev').attr('disabled', 'disabled');
            }
            $("li[data-target='#step" + atual + "']").addClass('active');
            $("#step" + atual).addClass('active');
        });

        if (step == "#step1") {
            erro = false;

            if (!$("input[name='alunoEmail']").val()) {
                vs.showNotificacaoWarning("Favor informar um e-mail válido!");
                erro = true;
            }
            if (!$("input[name='alunoCelular']").val()) {
                vs.showNotificacaoWarning("Favor informar um numero celular válido!");
                erro = true;
            }
            //Volta para aba anterior caso um dos campos da lista nao sejam setados
            erro && $("#prev").is(":enabled") ? $("#prev").click() : '';

        }

    });
    $("#prev").click(function () {
        $("#next").text("Proximo");

        step = $("ul[class='steps']").find("li.active").attr('data-target');
        $(step).removeClass('active');
        $("ul[class='steps']").find("li.active").attr('data-target', step).removeClass('active complete');

        proximoStep = parseInt(step.replace('#step', '')) - 1;
        if (proximoStep == 1) {
            $('#prev').attr('disabled', 'disabled');
        }
        proximoStep = '#step' + proximoStep;
        if ($(proximoStep)) {
            $(proximoStep).addClass('active');
            $("li[data-target='" + proximoStep + "']").addClass('active');
        }
    });

    if ($("input[label='Certificado de Reservista']").val() != "Sim") {
        $("input[name='alunoCertMilitar']").parent().parent().hide();
    } else {
        $("input[name='alunoCertMilitar']").parent().parent().show();
    }
    $("input[label='Certificado de Reservista']").click(function () {
        if ($(this).val() != "Sim") {
            $("input[name='alunoCertMilitar']").parent().parent().hide();
        } else {
            $("input[name='alunoCertMilitar']").parent().parent().show();
        }
    });

    $("input[label='Certificado de Reservista']").click(function () {
        if ($(this).val() != "Sim") {
            $("input[name='alunoCertMilitar']").parent().parent().hide();
        } else {
            $("input[name='alunoCertMilitar']").parent().parent().show();
        }
    });
});


function preencheTurmasDisponiveisDependencia() {
    // verifica se alguma disciplina adicionada é dependencia do aluno
    $("select[name='disciplinaTurma[]']").each(function (i, turmaDisc) {
        var $turmaDisc = $(turmaDisc);
        var disciplina = $turmaDisc.closest('div.row').find("input[name='disciplina[]']").val();
        var turmaId = $turmaDisc.closest('div.row').find("input[name='turma[]']").val();

        $("select[name='turmasDep[]']").each(function (j, turmaDep) {
            // seleciona disciplina
            var $turmaDep = $(turmaDep);
            var discDep = $turmaDep.attr('data-dep');

            if (discDep == disciplina) {
                var options = $turmaDep.find('option').clone();
                $turmaDisc.html('').append(options);
                $turmaDisc.val(turmaId); // turma de dependencia

                $turmaDep.closest('div.row').hide();
            }
        });
    });
}

$(document).on('click', "a[name='removeResponsavel']", function () {
    if (confirm("Deseja realmente remover este responsável ?")) {
        $(this).parent().parent().detach();
    }
});

$(document).on("click", "select[name='disciplinaTurma[]']", function () {
    var position = $(this).attr("data-position");
    var novaTurma = $("select[name^=disciplinaTurma]:eq(" + position + ") option:selected").attr("data-novaTurma");
    if (novaTurma != "false") {
        $("input[name^=novaTurma]:eq(" + position + ")").val("true");
    } else {
        $("input[name^=novaTurma]:eq(" + position + ")").val("false");
    }
});

//arrumar uma forma melhor de tratar este evento
$(document).ready(function () {

    var vs = VersaShared(this);

    jQuery(document).on('change', "[name^=endCep]", function () {
        var $element = $(this);
        var $fieldset = $element.closest('fieldset');
        var endCep = $element.val();
        var endCepNums = endCep.replace(/[^0-9]/g, '');

        if ($fieldset.length == 0) {
            $fieldset = $('#resp');
        }

        if (endCepNums.length < 8) {
            vs.showNotificacaoWarning('CEP inválido ou não encontrado!');

            return;
        }

        $element.prop('disabled', true);

        $.ajax({
            url: "/pessoa/cidade/busca-info-cep",
            dataType: 'json',
            type: 'post',
            data: {cep: endCep},
            success: function (data) {
                $element.prop('disabled', false);

                if (!data.dados) {
                    vs.showNotificacaoWarning('CEP inválido ou não encontrado!');
                }

                $fieldset.find("[name^=endLogradouro]").val(
                    ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                );
                $fieldset.find("[name^=endCidade]").val(data.dados.cid_nome || '');
                $fieldset.find("[name^=endEstado]").val(data.dados.est_uf || '').trigger('change');
                $fieldset.find("[name^=endBairro]").val(data.dados.bai_nome || '');
            },
            erro: function () {
                vs.showNotificacaoDanger('Falha ao buscar CEP!');
                $element.prop('disabled', false);
            }
        });
    });
    $('[name^=pesNaturalidade], [name^=endCidade], [name^=formacaoCidade] ')
        .blur(function () {
            $(this).removeClass('ui-autocomplete-loading');
        })
        .autocomplete({
            source: function (request, response) {
                var $element = $(this.element);
                var previous_request = $element.data("jqXHR");

                if (previous_request) {
                    previous_request.abort();
                }

                $element.data("jqXHR", $.ajax({
                    url: "/pessoa/cidade/busca-info-cep",
                    data: {cidade: request.term},
                    type: 'POST',
                    dataType: "json",
                    success: function (data) {
                        var transformed = $.map(data.dados || [], function (el) {
                            el.label = el.cid_nome;

                            return el;
                        });

                        response(transformed);
                    }
                }));
            },
            minLength: 2,
            focus: function (event, ui) {
                var $element = $(this);
                $element.val(ui.item.cid_nome);

                return false;
            },
            select: function (event, ui) {
                var $element = $(this);
                $element.val(ui.item.cid_nome);
                $element.closest('fieldset').find(
                    $element.attr('name').indexOf('endCidade') != -1 ? "[name^=endEstado]" : (
                        $element.attr('name').indexOf('formacaoCidade') != -1 ?
                            "[name=^formacaoEstado]" : "[name^=pesNascUf]"
                    )
                )
                    .val(ui.item.est_uf).trigger('change');

                return false;
            }
        });

    $('[name^=formacaoEstado]').select2({
        language: 'pt-BR',
        allowClear: true,
        data: function () {
            return {
                results: [{"id": "AC", "text": "Acre"}, {"id": "AL", "text": "Alagoas"}, {
                    "id": "AM",
                    "text": "Amazonas"
                }, {"id": "AP", "text": "Amap\u00e1"}, {"id": "BA", "text": "Bahia"}, {
                    "id": "CE",
                    "text": "Cear\u00e1"
                }, {"id": "DF", "text": "Distrito Federal"}, {"id": "ES", "text": "Esp\u00edrito Santo"}, {
                    "id": "GO",
                    "text": "Goi\u00e1s"
                }, {"id": "MA", "text": "Maranh\u00e3o"}, {"id": "MG", "text": "Minas Gerais"}, {
                    "id": "MS",
                    "text": "Mato Grosso do Sul"
                }, {"id": "MT", "text": "Mato Grosso"}, {"id": "PA", "text": "Par\u00e1"}, {
                    "id": "PB",
                    "text": "Paraiba"
                }, {"id": "PE", "text": "Pernambuco"}, {"id": "PI", "text": "Piau\u00ed"}, {
                    "id": "PR",
                    "text": "Paran\u00e1"
                }, {"id": "RJ", "text": "Rio de Janeiro"}, {"id": "RN", "text": "Rio Grande do Norte"}, {
                    "id": "RO",
                    "text": "Rond\u00f4nia"
                }, {"id": "RR", "text": "Roraima"}, {"id": "RS", "text": "Rio Grande do Sul"}, {
                    "id": "SC",
                    "text": "Santa Catarina"
                }, {"id": "SE", "text": "Sergipe"}, {"id": "SP", "text": "S\u00e3o Paulo"}, {
                    "id": "TO",
                    "text": "Tocantins"
                }]
            };
        }
    });
});

function previewFile() {
    var preview = document.getElementById('preview_image');
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
    };

    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
}

function incluiDependencia(dep) {
    dep = dep.split('|');
    dep[3] = parseInt(dep[3]);

    //var turma = $("select[data-dep='" + dep[3] + "'] > option:selected");
    var turma = $("select[data-dep='" + dep[3] + "']");
    var situacoes = "";
    $("input[name='situacao']").each(function () {
        situacoes = situacoes + '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';
    });
    var campoTurmaDisciplina = "disciplinaTurma" + dep[3];

    $("#blocoDisciplinas").append(
        "<div class='row'>" +
        "<section class='col-lg-2 col-sm-2 col-xs-2' style='padding-left:30px;'>" +
        "<label class='input'>" +
        "<input type='hidden' name='disciplina[]' value='" + dep[3] + "'>" +
        "<input type='hidden' name='novaTurma[]' value='true'>" +
        "<i></i> " + dep[4] +
        "</label>" +
        "</section>" +
        '<section class="col-lg-2 col-sm-2 col-xs-2 input" >' +
        '<input type="text" name="disciplinaTurma[]" id=' + campoTurmaDisciplina + '>' +
        '</section>' +
        "<section class='col-lg-2 col-sm-2 col-xs-2' style='padding-left:30px;'>" +
        "<label class='select'>" +
        "<i></i>" +
        "<select name='disciplinaSituacao[]' >" +
        situacoes +
        "</select>" +
        "</label>" +
        "</section>" +
        "<section class='col-lg-3 col-sm-3 col-xs-3' style='padding-left:20px;'>" +
        "<label>" +
        "<button type='button' class='btn btn-sm btn-danger' onclick='removeDependencia(this)'>Remover Diciplina</button>" +
        '</label>' +
        '</section>' +
        "</div>"
    );

    $("select[name='disciplinaTurma[]']:last").append(turma.html()).val(turma.val());

    $("select[name='disciplinaSituacao[]']:last").val('Dependente').attr('readonly', true);

    $("select[data-dep='" + dep[3] + "']").parent().parent().parent().hide();

    $(".select2-container").style.width = '100%';
}

function salvaDisciplina() {

    var vs = new VersaShared();
    var arrTurma = $('[name^=turmasOfertantes]').select2('data'),
        arrDisciplina = $('[name^=disciplinasBusca]').select2('data');

    vs.addOverlay($(".widget-body"));

    if (!arrTurma || !arrDisciplina) {
        vs.removeOverlay($(".widget-body"));
        return false;
    }

    var diciplina = $("#disciplinasBusca:visible").find("option:selected");
    var turma = $("#turmasOfertantes:visible");
    var situacoes = "";
    $("input[name='situacao']").each(function () {
        situacoes = situacoes + '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';
    });

    var i = $("input[name^='novaTurma']").length;
    var disciplinasId = "disciplinaVinculadasAluno" + i,
        campoTurmaDisciplina = "disciplinaTurma" + arrDisciplina['disc_id'];

    $("#blocoDisciplinas").append(
        '<div class="row" id="removeDisicplina' + i + '">' +
        '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
        '<label class="input">' +
        '<input type="hidden" name="disciplina[]" value="' + arrDisciplina['disc_id'] + '">' +
        arrDisciplina['disc_nome'] +
        '</label>' +
        '</section>' +
        '<section class="col-lg-2 col-sm-2 col-xs-2 input" >' +
        '<input type="text" name="disciplinaTurma[]" id=' + campoTurmaDisciplina + '>' +
        '</section>' +
        '</label>' +
        '</section>' +
        '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
        '<label class="select">' +
        '<i></i>' +
        '<select name="disciplinaSituacao[]" >' +
        situacoes +
        '</select>' +
        '</label>' +
        '</section>' +
        '<section class="col-lg-3 col-sm-3 col-xs-3" style="padding-left:20px;">' +
        '<label>' +
        '<button type="button" class="btn btn-sm btn-danger" onclick="removeDisciplina(this)">Remover Diciplina</button>' +
        '</label>' +
        '</section>' +
        '</div>'
    );

    $('[name^=turmasOfertantes]').each(function (i, item) {
        var $item = $(item);

        if ($item.data('select2')) {
            $item.select2('destroy');
        }
    });

    campoTurmaDisciplina = "#" + campoTurmaDisciplina;

    $(campoTurmaDisciplina).select2({
        ajax: {
            url: "/matricula/acadperiodo-turma/search-for-json",
            dataType: 'json',
            delay: 250,
            data: function (query) {

                var arrDados = {
                    query: query,
                    perId: $('[name="periodoLetivo"]').val(),
                    cursoId: $("#novoCursoId").val(),
                    discId: $(this).closest('.row').find('[name^=disciplina]').val(),
                    naoLimitar: true
                };

                return arrDados;
            },
            results: function (data) {
                var transformed = $.map(data, function (el) {
                    el.id = el.turma_id;
                    el.text = el.turma_nome;

                    return el;
                });

                return {results: transformed};
            }
        }
    });

    if (arrTurma) {
        $(campoTurmaDisciplina).select2('data', arrTurma).trigger('change');
    }

    $(".select2-container").css("width", "100%");
    $(".novaDisciplina").css("width", "15%");

    $("select[name='disciplinaTurma[]']:last").each(function () {
        $(this).attr("data-position", i);
        $(this).children().each(function () {
            if (turma.val() == $(this).val()) {
                $(this).attr("selected", "selected");
                $(this).attr("data-novaTurma", "false");
            } else {
                $(this).attr("data-novaTurma", "true");
            }
        });

    });

    EscondeFormBuscaDisciplina();

    vs.removeOverlay($(".widget-body"));
}

function ExibeFormBuscaDisciplina() {
    var vs = new VersaShared();
    var erro = false;

    if (!$("#novoCursoId").val()) {
        vs.showNotificacaoWarning("Selecione o curso do aluno!");
        erro = true;
    }
    if (!$('[name="alunoTurmaPrincipal"]').val()) {

        vs.showNotificacaoWarning("Selecione a turma principal do aluno!");
        erro = true;
    }
    if (!$('[name="periodoLetivo"]').val()) {

        vs.showNotificacaoWarning("Selecione o período letivo!");
        erro = true;
    }

    if (erro) {
        return false;
    }


    $("#formBuscaDisciplina").removeAttr('hidden');
    $("#BtBuscaDisciplinas").attr('hidden', 'hidden');
    $("#BtCancelaBuscaDisciplinas").removeAttr('hidden');
}
function EscondeFormBuscaDisciplina() {
    $("#formBuscaDisciplina").attr('hidden', 'hidden');
    $("#BtBuscaDisciplinas").removeAttr('hidden');
    $("#BtCancelaBuscaDisciplinas").attr('hidden', 'hidden');
}

function removeDependencia($el) {
    var dep = $($el).parent().parent().parent().find("input[name='disciplina[]']").val();
    $("select[data-dep='" + dep + "']").parent().parent().parent().show();

    removeDisciplina($el);
}
function removeDisciplina(element) {
    $(element).parent().parent().parent().detach();
}

function validateEmail(element) {
    var email = $(element).val();
    var regra = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

    if (!regra.test(email)) {
        alert('Favor inserir um email valido!');
        $(element).val("");
        $(element).focus();
    }
}

function validaResponsavelDocumento(element) {
    if ($(element).val() == $("input[name='pesCpf']").val()) {
        alert("O responsável não pode ser o proprio aluno!");
        $(element).focus();
        $(element).val('');
    }
}
