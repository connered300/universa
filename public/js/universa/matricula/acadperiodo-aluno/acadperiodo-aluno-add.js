(function ($, window, document) {
    'use strict';
    var AcadperiodoAlunoAdd = function () {
        VersaShared.call(this);
        var __acadperiodoAlunoAdd = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                add: '',
                edit: '',
                buscaTurma: '',
                agenteEducacional: '',
                unidadeEstudo: '',
                origemCadastro: ''
            },
            value: {
                arrPeriodo: [],
                cursoAluno: '',
                optionsTurma: [],
                arrEstadoCivil: []
            },
            datatables: {
                acadperiodoAluno: null
            }
        };

        this.setSteps = function () {
            var $search = $("input[type^='search']");
            var $curso = $('[name="curso"]');
            var $cursocampus = $('#cursoCampusAddId');
            var $turma = $('[name="alunoTurmaPrincipal"]');
            var $periodo = $('[name="periodoLetivo"]');
            var $agenteEducacional = $('[name="pesIdAgente"]');
            var $unidadeEstudo = $('[name="unidade"]');
            var $origemCadastro = $('[name="origem"]');

            $periodo.select2({
                allowClear: true,
                ajax: {
                    url: __acadperiodoAlunoAdd.options.url.periodo,
                    dataType: 'json',
                    delay: 250,
                    data: function () {
                        return {
                            abertoParaMatricula: true
                        }
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.per_nome, id: el.per_id};
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $curso.select2('val', '').trigger('change');

                __acadperiodoAlunoAdd.limparDisciplinas();

                if (!$periodo.val()) {
                    $curso.select2("disable", true);
                } else {
                    $curso.select2("enable", true);
                }
            });

            $(document).on('click', '#btnAdicionarDisciplina', function () {
                var objTurma = $turma.select2('data');
                objTurma['matCur'] = objTurma['matCur'] ? objTurma['matCur'] : objTurma['mat_cur_id'];
                __acadperiodoAlunoAdd.preencheFormAdicionarDisciplina(objTurma['matCur'])
            });

            $(document).on('click', '#salvarDisciplinaAdicional', function () {

                var arrTurma = $('[name^=turmasOfertantes]').select2('data'),
                    arrDisciplina = $('[name^=disciplinasBusca]').select2('data');

                if (!arrDisciplina) {
                    __acadperiodoAlunoAdd.showNotificacaoWarning("Preencha o campo da Disciplina!");
                }
                if (!arrTurma) {
                    __acadperiodoAlunoAdd.showNotificacaoWarning("Preencha o campo da Turma!");
                }
            });

            $curso.select2({
                ajax: {
                    url: __acadperiodoAlunoAdd.options.url.curso,
                    dataType: 'json',
                    delay: 250,
                    data: function () {
                        return {
                            cursoPossuiPeriodoLetivo: 'Sim',
                            perId: $periodo.val(),
                            alunoId: $("input[name='alunoId']").val()
                        }
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.curso_id;
                            el.text = el.camp_nome + ' / ' + el.curso_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $turma.select2('val', '').trigger('change');

                __acadperiodoAlunoAdd.limparDisciplinas();

                if (!$curso.val()) {
                    $turma.select2("disable", true);
                } else {
                    $turma.select2("enable", true);
                }
            });

            $turma.select2({
                ajax: {
                    url: __acadperiodoAlunoAdd.options.url.turma,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {
                            query: query,
                            perId: $periodo.val(),
                            cursoId: $('[name="curso"]').val()
                        }
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.turma_id;
                            el.text = el.turma_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function (e) {
                $cursocampus.val('');

                if (e.added) {
                    __acadperiodoAlunoAdd.limparDisciplinas();
                    var hiddenTurma = $('#step3').find('input[name="turma"]');
                    var matCurId = e.added['mat_cur_id'];
                    var turmaId = e.added['turma_id'];
                    var cursocampusId = e.added['cursocampus_id'];
                    var serie = e.added['turma_serie'];

                    $cursocampus.val(cursocampusId || '');
                    $(hiddenTurma).val(turmaId);
                    __acadperiodoAlunoAdd.montarGradeDeDisciplinas(matCurId, serie, turmaId);
                }
            }).on('select2-loaded', function (e) {
                __acadperiodoAlunoAdd.options.value.optionsTurma = e.items.results;
            });

            $(document).on('click', '.item-edit', function () {
                var matricula = $(this).attr('data-alunoPerId');

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja reativar o curso ?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        __acadperiodoAlunoAdd.addOverlay(
                            $('.row'),
                            "Aguarde... Estamos redirecionando você"
                        );
                        location.href =
                            __acadperiodoAlunoAdd.options.url.edit + '/' + matricula + '&rematricula=true';
                    }
                });

            });


            $agenteEducacional.select2({
                allowClear: true,
                ajax: {
                    url: __acadperiodoAlunoAdd.options.url.agenteEducacional,
                    dataType: 'json',
                    delay: 250,
                    data: function () {
                        return {query: ""}
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pesNome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });


            $unidadeEstudo.select2({
                allowClear: true,
                ajax: {
                    url: __acadperiodoAlunoAdd.options.url.unidadeEstudo,
                    dataType: 'json',
                    delay: 250,
                    data: function () {
                        return {query: ""}
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.unidade_id;
                            el.text = el.unidade_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $origemCadastro.select2({
                allowClear: true,
                ajax: {
                    url: __acadperiodoAlunoAdd.options.url.origemCadastro,
                    dataType: 'json',
                    delay: 250,
                    data: function () {
                        return {query: ""}
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.origem_id;
                            el.text = el.origem_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $search.focus();
            var aluno = $("input[name^='alunocursoId']").val();
            if (aluno != "") {
                $search.val(aluno).keyup();
            }
            $("tr").on('click', function () {
                var href = $(this).children().find("a").attr('href');
                if (href != undefined) {
                    window.location.replace(href);
                }
            });

            if (!$periodo.val()) {
                $curso.select2('val', "").trigger("change");
                $curso.select2("disable", true);
            }
            if (!$curso.val()) {
                $turma.select2('val', "").trigger("change");
                $turma.select2("disable", true);
            }

            $search.keyup();
        };

        this.preencheFormAdicionarDisciplina = function (matCurId) {
            var $turma = $('#turmasOfertantes');
            var $disciplina = $('#disciplinasBusca');
            var arrTurma = [];
            var arrDisciplina = [];

            $disciplina.select2({
                ajax: {
                    url: __acadperiodoAlunoAdd.options.url.disciplinas,
                    dataType: 'json',
                    delay: 250,
                    data: function () {
                        var arrDados = {
                            matCurId: matCurId,
                            perId: $('[name="periodoLetivo"]').val(),
                            cursoId: $("#novoCursoId").val(),
                            disciplinasOcultar: __acadperiodoAlunoAdd.getDisciplinasVinculadas()
                        };
                        return arrDados;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.discicplina_id;
                            el.text = el.disciplina_nome;

                            return el;
                        });
                        return {results: transformed};
                    }
                }

            });

            $('[name^=turmasOfertantes]').each(function (i, item) {
                var $item = $(item);

                if ($item.data('select2')) {
                    $item.select2('destroy');
                }

            });

            $('[name^=turmasOfertantes]').select2({
                ajax: {
                    url: __acadperiodoAlunoAdd.options.url.buscaTurma,
                    dataType: 'json',
                    delay: 250,
                    data: function () {

                        var arrDados = {
                            perId: $('[name="periodoLetivo"]').val(),
                            cursoId: $("#novoCursoId").val(),
                            discId: $("#disciplinasBusca").val()
                        };
                        return arrDados;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.turma_id;
                            el.text = el.turma_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });
        };

        this.pushUnique = function (arr, obj) {
            for (var row in arr) {
                if (obj.id == arr[row]['id']) {
                    return true;
                }
            }

            arr.push(obj);
        };

        this.createOptionFromSelect2Obj = function (obj, el) {
            var options = "";

            if (obj instanceof Array) {
                for (var row in obj) {
                    var option = new Option(obj[row]['text'], obj[row]['id']);
                    $(el).append($(option));
                }
            } else {
                options += "<options value='" + obj['id'] + "'>" + obj['text'] + "</options>";
            }

            return options;
        };

        this.montarGradeDeDisciplinas = function (matriz, serie, turmaId) {
            $.ajax({
                async: false,
                type: "POST",
                url: "/matricula/acadperiodo-matriz-disciplina/busca-disciplinas",
                data: {
                    matriz: matriz,
                    periodo: serie
                },
                success: function (data) {
                    var situacoes = "";

                    $("input[name='situacao']").each(function () {
                        situacoes =
                            situacoes + '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';
                    });

                    __acadperiodoAlunoAdd.criarEstruturaDaGradeHtml(data.disciplinas, situacoes);

                },
                error: function (a) {
                    alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
                }
            });
        };

        this.criarEstruturaDaGradeHtml = function (disciplinas, situacoes) {
            var arrTurmas = __acadperiodoAlunoAdd.getSelect2TurmaOptionsToSelectFormat();
            var $turmas = $("input[name='alunoTurmaPrincipal']");
            var $blocoDisciplinas = $("#blocoDisciplinas");

            if (disciplinas !== undefined) {
                for (var i = 0; i < disciplinas.length; i++) {
                    var disc = disciplinas[i]['disc'];

                    var campoTurmaDisciplina = "disciplinaTurma" + disc;

                    var $disc = $('<div class="row">' +
                        '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                        '<label class="input">' +
                        '<input type="hidden" name="novaTurma[]" data-position="' + i +
                        '" value="true">' +
                        '<input type="hidden" name="disciplina[]" value="' + disc + '">' +
                        '<input type="hidden" name="disciplinaTipo[]" value="' +
                        disciplinas[i]['discTipoNome'] + '">' +
                        '<i></i> ' + disciplinas[i]['discNome'] +
                        '</label>' +
                        '</section>' +
                        '<section class="col-lg-2 col-sm-2 col-xs-2 input" >' +
                        '<input type="text" name="disciplinaTurma[]"  id=' + campoTurmaDisciplina + '>' +
                        '</section>' +
                        '</label>' +
                        '</section>' +
                        '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                        '<label class="select">' +
                        '<i></i>' +
                        '<select name="disciplinaSituacao[]" >' +
                        situacoes +
                        '</select>' +
                        '</label>' +
                        '</section>' +
                        '</div>');
                    $blocoDisciplinas.append($disc);
                    $disc.find("select[name='disciplinaTurma[]']").val($turmas.val());

                }

                $blocoDisciplinas.show();

                $('[name^=disciplinaTurma]').each(function (i, item) {
                    var $item = $(item);

                    if ($item.data('select2')) {
                        $item.select2('destroy');
                    }

                });

                $('[name^=disciplinaTurma]').select2({
                    ajax: {
                        url: __acadperiodoAlunoAdd.options.url.buscaTurma,
                        dataType: 'json',
                        delay: 250,
                        data: function () {

                            var arrDados = {
                                perId: $('[name="periodoLetivo"]').val(),
                                cursoId: $("#novoCursoId").val(),
                                discId: $(this).closest('.row.disciplinas').find('[name^=disciplina]').val()
                            };
                            return arrDados;
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.turma_id;
                                el.text = el.turma_nome;

                                return el;
                            });
                            return {results: transformed};
                        }
                    }
                });

                if ($("#alunoTurmaPrincipalId").select2("data")) {
                    $('[name^=disciplinaTurma]').select2("data", $("#alunoTurmaPrincipalId").select2("data")).trigger("change");
                }

            }
        };

        this.getSelect2TurmaOptionsToSelectFormat = function () {
            var arrDados = __acadperiodoAlunoAdd.getArrTurmaDoSelect();
            var html = "";
            for (var row in arrDados) {
                html += "<option value='" + arrDados[row]['turma_id'] + "'>" + arrDados[row]['turma_nome'] + "</option>"
            }

            return html;
        };

        this.getArrTurmaDoSelect = function () {
            return __acadperiodoAlunoAdd.options.value.optionsTurma || []
        };

        this.limparDisciplinas = function () {
            $("#blocoDisciplinas").children().each(function () {
                $(this).detach();
            });
        };

        this.getDisciplinasVinculadas = function () {
            var disciplinas = [];

            $.map($("[name='disciplina[]']"), function (e) {
                disciplinas.push(e.value);
            });

            return disciplinas || null;
        };

        this.run = function (opts) {
            __acadperiodoAlunoAdd.setDefaults(opts);
            __acadperiodoAlunoAdd.setSteps();
        };
    };

    $.acadperiodoAlunoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadperiodo-aluno.add");

        if (!obj) {
            obj = new AcadperiodoAlunoAdd();
            obj.run(params);
            $(window).data('universa.matricula.acadperiodo-aluno.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);