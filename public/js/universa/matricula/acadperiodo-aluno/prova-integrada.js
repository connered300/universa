(function ($, window, document) {
    'use strict';

    var ProvaIntegrada = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlSaveInfo: '',
                baseUrl: '',
                urlUpdateGabarito: '',
                gabarito: '',
                gabaritoValidate: '',
                integradoraPrincipal: '',
                integradoraFechada: 'Encerrada'
            }
        };

        priv.__getFunc = function (target, step) {
            step = step || 0;

            if (target[step] && typeof target[step] == 'function') {
                return target[step];
            }

            return false;
        };

        /**
         * retira tela de mensagem de espera em um elemento
         * @param el : elemento a retirar tela de mensagem
         */
        priv.removeOverlay = function (el) {
            el.find('.loader-overlay').remove();
        };

        /**
         * adiciona tela de mensagem de espera em um elemento
         * @param el : elemento a acrescentar mensagem de espera
         * @param mensagem : mensagem a ser exibida durante espera
         */
        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="loader-overlay">' + mensagem + '</div>');
        };

        /**
         * validaFields : Método para validar respostas de gabarito
         * @param {Array|Object} params : parametros de configuração da validação
         *       aceita: {undefined|Object} 'element' -> caso seja pra validar apenas um campo
         *               {true|false}       'allowEmpty' -> permitir campos vazio
         * @return {true|false} true se validou, false caso contrário
         */
        priv.validaFields = function (params) {
            var validate = true;
            var options = {
                element: undefined,
                allowEmpty: true
            };

            params = params || {};
            options = $.extend(options, params);

            if (options.element instanceof Object) {
                var $element = $(options.element);
                if (!$element.val()) {
                    $element.css("border-line", "#e45858");
                    validate = false;
                } else {
                    $element.css("border-line", "");
                }
            } else {
                $("input[name='alunodisc[]']").each(function (index, $alunoDiscField) {
                    var fieldsRespostas = $($alunoDiscField).parent().find(".area-gabarito").eq(0);
                    $(fieldsRespostas).find("input[name='gabaritoAluno[]']").each(function (i, $respField) {
                        $respField.value = $respField.value.trim();
                        if (!$respField.value) {
                            $($respField).addClass('status-error');
                            validate = false;
                        } else {
                            $($respField).removeClass('status-error');
                        }
                    });
                });
            }

            if (!validate) {
                if (!options.allowEmpty) {
                    $.smallBox({
                        title: "<strong>Dados Inálidos!</strong>",
                        content: "<i><p>Algumas questões não foram respodidas</p></i>",
                        color: "#C46A69",
                        icon: "fa fa-times",
                        timeout: 20000
                    });
                }
                validate = options.allowEmpty;
            }

            return validate;
        };

        /**
         * getDataBeforeSend Recupera os dados de resposta do aluno
         * @return {Object} Objeto JSON com os dados
         */
        priv.getDataBeforeSend = function () {
            var alunoDisc = new Array();
            var respostas = new Array();
            var respostasId = new Array();
            var integradorasId = new Array();

            $("input[name='alunodisc[]']").each(function (index, alunoDiscField) {
                var $parentAlunoDisc = $(alunoDiscField).parent();
                // recupera a integradora vinculada as respostas desta disciplina
                var integradoraDisc = $parentAlunoDisc.find("input[name='integradorasId[]']").val();

                var fieldsRespostas = $(alunoDiscField).parent().find(".area-gabarito").eq(0);
                var respostasString = '';

                var countEmpty = 0;
                var gabaritoAluno = $(fieldsRespostas).find("input[name='gabaritoAluno[]']");
                $(gabaritoAluno).each(function (i, respField) {
                    var resp = respField.value;
                    if (!resp) {
                        countEmpty++;
                        resp = '*';
                    }
                    respostasString += "" + resp;
                });

                // desconsidera se a quantidade de repostas vazias for igual 
                // ao n° de respostas possíveis e não possuir integradora nesta disciplina
                if (!(countEmpty == gabaritoAluno.length && !integradoraDisc)) {
                    // acrescenta ID do aluno na disciplina em um array
                    alunoDisc.push(alunoDiscField.value);
                    // acrescenta o ID da resposta do aluno
                    respostasId.push($parentAlunoDisc.find("input[name='respostasId[]']").val());
                    // acrescenta a resposta do aluno concatenada
                    respostas.push(respostasString);
                    // acrescenta o ID da integradora
                    integradorasId.push(integradoraDisc);
                } else {
                    // se não há repostas para a disciplina que consta como ausente
                    // desconcidera o gabarito da mesma
                    priv.options.gabarito.splice(index, 1);
                }
            });

            return {
                alunoperId: $("input[name='alunoperId']").val(),
                alunoDisc: alunoDisc,
                respostas: respostas,
                gabarito: priv.options.gabarito,
                gabaritoValidate: priv.options.gabaritoValidate,
                respostasId: respostasId,
                integradoras: integradorasId,
                integradoraPrincipal: priv.options.integradoraPrincipal
            };
        };

        priv.saveGabarito = function () {
            $.ajax({
                type: "POST",
                url: priv.options.baseUrl + "" + priv.options.urlUpdateGabarito,
                data: priv.getDataBeforeSend(),
                beforeSend: function () {
                    var validate = priv.validaFields();
                    if (validate) {
                        priv.addOverlay($('#gabarito'), 'Aguarde enquanto as alterações são salvas.');
                    }

                    return validate;
                },
                complete: function () {
                    priv.removeOverlay($('#gabarito'));
                },
                success: function (data) {
                    if (data.type == "success") {
                        if (data.saveds) {
                            var saveds = data.saveds;
                            // procura pelas disciplinas em que as respotas foram alteradas e atualiza o id das respostas
                            // caso o registro seja criado e não editado
                            for (var save in saveds) {
                                var $alunoDiscId = $("input[name='alunodisc[]'][value=" + saveds[save].alunoDisc + "]");
                                var $alunoIntegId = $($alunoDiscId).parent().find("input[name='respostasId[]']");

                                $($alunoIntegId).val(saveds[save].alunoIntegId);
                            }
                        }

                        $.smallBox({
                            title: "<strong>Alterações Salvas</strong>!",
                            content: "<i><p>" + data.message + "</p></i>",
                            color: "#659265",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 10000
                        });
                    } else {
                        $.smallBox({
                            title: "<strong>Não foi possível Salvar as Alterações</strong>!",
                            content: "<i><p>" + data.message + "</p></i>",
                            color: "#C46A69",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 10000
                        });
                    }
                }
            });
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.setSteps = function () {
            $("#myModalLabel").append($("input[name='pesNome']").val());

            if (priv.options.integradoraFechada == 'Encerrada') {
                $('#gabarito input').prop('disabled', true);
                $('#updateGabarito').prop('disabled', true);
            }

            $("#updateGabarito").click(function () {
                // realiza a confirmação da ação de salvar
                $.SmartMessageBox({
                    title: "Confirme!",
                    content: "Esta ação impactará nas notas do aluno. Deseja continuar?",
                    buttons: '[Não][Sim]'
                }, function (ButtonPressed) {
                    if (ButtonPressed == "Sim") {
                        priv.saveGabarito();
                    }
                });
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.provaIntegrada = function (params) {
        params = params || [];

        var obj = $(window).data("universa.aluno.provaIntegrada");

        if (!obj) {
            obj = new ProvaIntegrada();
            obj.run(params);
            $(window).data('universa.aluno.provaIntegrada', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);
