(function ($, window, document) {
    'use strict';
    var AcadperiodoAlunoEdit = function () {
            VersaShared.call(this);
            var __acadperiodoAlunoEdit = this;
            this.defaults = {
                ajax: true,
                url: {
                    buscaTurma: '',
                    agenteEducacional: '',
                    unidadeEstudo: '',
                    origemCadastro: '',
                    periodo: ''
                },
                value: {
                    arrPeriodo: '',
                    cursoAluno: '',
                    optionsTurma: [],
                    arrDisciplinasAluno: [],
                    serie: '',
                    alunoPer: '',
                    alunoMigracao: '',
                    editaAgente: false
                },
                data: {
                    arrUnidade: [],
                    arrAgenteEducacional: [],
                    arrOrigemAlunoCurso: [],
                    disciplinasTurmas: [],
                    arrEstadoCivil: [],
                    estadoCivil: [],
                    arrCurso: [],
                    arrPeriodosDisponiveis: [],
                    arrEndereco: []
                },
                datatables: {
                    acadperiodoAluno: null
                },
                arrDisciplinasQuePermanecem: []
            };

            this.setSteps = function () {
                var $curso = $('[name="curso"]');
                var $turma = $('[name="alunoTurmaPrincipal"]');
                var $periodo = $('[name="periodoLetivo"]');
                var $agenteEducacional = $('[name="pesIdAgente"]');
                var $unidadeEstudo = $('[name="unidade"]');
                var $origemCadastro = $('[name="origem"]');
                var $estadoCivil = $('[name="pesEstadoCivil"]');
                var cidadeNascimento = $('[name="pesNaturalidade"]');
                var estadoNascimento = $('[name="pesNascUf"]');

                var arrEndereco = __acadperiodoAlunoEdit.options.data.arrEndereco || [];

                if (arrEndereco) {
                    if (!cidadeNascimento.val()) {
                        cidadeNascimento.val(arrEndereco['endCidade[]']);
                    }

                    if (!estadoNascimento.val()) {
                        estadoNascimento.val(arrEndereco['endEstado[]']);
                    }
                }

                $(document).on('click', '#btn-requerimento-matricula', function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    window.open($(this).attr('href') + '&' + $turma.val(), '_blank');
                });

                $(document).on('click', '.btnRemoveDisc', function () {
                    __acadperiodoAlunoEdit.removeDisciplina(this);
                });

                $(document).on('click', '#salvarDisciplinaAdicional', function () {

                    var arrTurma = $('[name^=turmasOfertantes]').select2('data'),
                        arrDisciplina = $('[name^=disciplinasBusca]').select2('data');

                    if (!arrDisciplina) {
                        __acadperiodoAlunoEdit.showNotificacaoWarning("Preencha o campo da Disciplina!");
                    }
                    if (!arrTurma) {
                        __acadperiodoAlunoEdit.showNotificacaoWarning("Preencha o campo da Turma!");
                    }
                });

                $(document).on('click', '#btnVoltarDisciplinasAluno', function () {
                    __acadperiodoAlunoEdit.addOverlay($(".widget-body"));

                    var arrDisciplinas = __acadperiodoAlunoEdit.getArrAlunoDisciplinas();
                    var turmaAluno = __acadperiodoAlunoEdit.getTurmaAluno();

                    __acadperiodoAlunoEdit.limparTodasDisciplinas();

                    if (arrDisciplinas && arrDisciplinas.length) {
                        __acadperiodoAlunoEdit.criarEstruturaDaGradeHtml(arrDisciplinas, true);
                    }

                    $('.turmaAlunoOptDefault').attr('selected', 'selected');

                    if (turmaAluno) {
                        $turma.select2('data', turmaAluno).trigger('change');
                    }


                    __acadperiodoAlunoEdit.removeOverlay($(".widget-body"));
                });

                $(document).on('click', '#btnAddDisciplinaAluno', function () {
                    var objTurma = $turma.select2('data');
                    objTurma['matCur'] = objTurma['matCur'] ? objTurma['matCur'] : objTurma['mat_cur_id'];
                    __acadperiodoAlunoEdit.preencheFormAdicionarDisciplina(objTurma['matCur']);

                    $("#turmasOfertantes").select2("disable", true);

                    $("#disciplinasBusca").on("change", function () {
                        $("#turmasOfertantes").select2("disable", true);

                        if ($("#disciplinasBusca").val()) {
                            $("#turmasOfertantes").select2("enable", true);
                        }

                    });
                });

                $(document).on('change', '#disciplinasBusca', function () {
                    $("#turmasOfertantes").select2("data", '').trigger("change");
                });

                $estadoCivil.select2('data', __acadperiodoAlunoEdit.options.data.arrEstadoCivil || []);
                $estadoCivil.select2('data', __acadperiodoAlunoEdit.options.data.estadoCivil).trigger("change");

                if (__acadperiodoAlunoEdit.getAlunoMigracao()) {
                    $periodo.select2({
                        allowClear: true,
                        data: __acadperiodoAlunoEdit.options.data.arrPeriodosDisponiveis

                    }).on('change', function () {
                        $turma.select2('val', '').trigger('change');

                        if (!__acadperiodoAlunoEdit.getCursoAlunoSemPeriodo()) {
                            $curso.select2('val', '').trigger('change');

                            if (!$periodo.val()) {
                                $curso.select2("disable", true);
                            } else {
                                $curso.select2("enable", true);
                            }
                        } else {
                            if (!$curso.val()) {
                                $turma.select2("disable", true);
                            } else {
                                $turma.select2("enable", true);
                            }
                        }

                        __acadperiodoAlunoEdit.limparDisciplinas();

                    });
                } else {
                    $periodo.select2({
                        allowClear: false,
                        language: 'pt-BR',
                        data: __acadperiodoAlunoEdit.options.value.arrPeriodo
                    });
                }

                $curso.select2({
                    ajax: {
                        url: __acadperiodoAlunoEdit.options.url.curso,
                        dataType: 'json',
                        delay: 250,
                        data: function () {
                            return {perId: $periodo.val()}
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.curso_id;
                                el.text = el.camp_nome + ' / ' + el.curso_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                }).on('change', function () {
                    $turma.select2('val', '').trigger('change');

                    __acadperiodoAlunoEdit.limparDisciplinas();

                    if (!$curso.val()) {
                        $turma.select2("disable", true);
                    } else {
                        $turma.select2("enable", true);
                    }
                });

                if (__acadperiodoAlunoEdit.getCursoAlunoSemPeriodo()) {
                    $curso.select2("data", __acadperiodoAlunoEdit.getCursoAlunoSemPeriodo()).trigger("change");
                    $curso.select2("disable", true);
                }

                $turma.select2({
                    language: 'pt-BR',
                    ajax: {
                        url: __acadperiodoAlunoEdit.options.url.turma,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {
                                query: query,
                                perId: $periodo.val(),
                                cursoId: $("#novoCursoId").val()
                            }
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.turma_id;
                                el.text = el.turma_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                }).on('change', function (e) {
                    __acadperiodoAlunoEdit.addOverlay($(".widget-body"));

                    if (e.added) {
                        var hiddenTurma = $('#step3').find('input[name="turma"]');
                        var matCurId = e.added['mat_cur_id'];
                        var turmaId = $turma.select2("data");
                        var perId = e.added['turma_serie'];
                        $(hiddenTurma).val(turmaId);
                        __acadperiodoAlunoEdit.limparDisciplinasNaoAdaptante();
                        __acadperiodoAlunoEdit.setArrDisciplinasQuePermanecem(null);
                        __acadperiodoAlunoEdit.montarGradeDeDisciplinas(matCurId, perId, turmaId, '', true);
                    }
                    __acadperiodoAlunoEdit.removeOverlay($(".widget-body"));
                }).on('select2-loaded', function (e) {
                    __acadperiodoAlunoEdit.addOverlay($(".widget-body"));
                    __acadperiodoAlunoEdit.options.value.optionsTurma = e.items.results;
                    __acadperiodoAlunoEdit.removeOverlay($(".widget-body"));
                });

                $agenteEducacional.select2({'data': []});
                var permissao = __acadperiodoAlunoEdit.verificaSeEditaAgente(),
                    edita = __acadperiodoAlunoEdit.getArrAgenteSelect2();

                if (permissao || !edita) {
                    $agenteEducacional.select2({
                        allowClear: true,
                        ajax: {
                            url: __acadperiodoAlunoEdit.options.url.agenteEducacional,
                            dataType: 'json',
                            delay: 250,
                            data: function () {
                                return {query: ""}
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    el.id = el.pes_id;
                                    el.text = el.pesNome;

                                    return el;
                                });

                                return {results: transformed};
                            }
                        }
                    });
                }

                if (edita) {
                    $agenteEducacional.select2('data', __acadperiodoAlunoEdit.getArrAgenteSelect2()).trigger("change");
                }

                $unidadeEstudo.select2({
                    allowClear: true,
                    ajax: {
                        url: __acadperiodoAlunoEdit.options.url.unidadeEstudo,
                        dataType: 'json',
                        delay: 250,
                        data: function () {
                            return {query: ""}
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.unidade_id;
                                el.text = el.unidade_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });

                if (__acadperiodoAlunoEdit.getArrUnidadeEstudoSelect2()) {
                    $unidadeEstudo.select2('data', __acadperiodoAlunoEdit.getArrUnidadeEstudoSelect2()).trigger("change");
                }

                $origemCadastro.select2({
                    allowClear: true,
                    ajax: {
                        url: __acadperiodoAlunoEdit.options.url.origemCadastro,
                        dataType: 'json',
                        delay: 250,
                        data: function () {
                            return {query: ""}
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.origem_id;
                                el.text = el.origem_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });

                if (__acadperiodoAlunoEdit.getArrOrigemSelect2()) {
                    $origemCadastro.select2('data', __acadperiodoAlunoEdit.getArrOrigemSelect2());
                }

                __acadperiodoAlunoEdit.setarDadosDoAluno();

                $curso.select2('disable');

                if (__acadperiodoAlunoEdit.getAlunoMigracao()) {
                    $periodo.select2("data", "").trigger("change");
                    $turma.select2("disable");
                } else {
                    $periodo.select2('disable');
                }
                $("input[name=pesCpf]").prop("disabled", true);

            };

            this.removeDisciplina = function (element) {
                $(element).parent().closest('div').remove();
            };


            this.preencheFormAdicionarDisciplina = function (matCurId) {
                var $disciplina = $('#disciplinasBusca');
                var arrTurma = [];
                var arrDisciplina = [];

                $disciplina.select2({
                    ajax: {
                        url: __acadperiodoAlunoEdit.options.url.disciplinas,
                        dataType: 'json',
                        delay: 250,
                        data: function (e) {
                            return {
                                query: e,
                                perId: $('[name="periodoLetivo"]').val(),
                                cursoId: $("#novoCursoId").val(),
                                disciplinasOcultar: __acadperiodoAlunoEdit.getDisciplinasVinculadas(),
                                naoLimitar: true
                            };
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.discicplina_id;
                                el.text = el.disciplina_nome + ' ( ' + el.turma_serie + ' )';

                                return el;
                            });
                            return {results: transformed};
                        }
                    }
                });

                $('[name^=turmasOfertantes]').each(function (i, item) {
                    var $item = $(item);

                    if ($item.data('select2')) {
                        $item.select2('destroy');
                    }
                });

                $('[name^=turmasOfertantes]').select2({
                    ajax: {
                        url: __acadperiodoAlunoEdit.options.url.buscaTurma,
                        dataType: 'json',
                        delay: 250,
                        data: function (e) {

                            var arrDados = {
                                query: e,
                                perId: $('[name="periodoLetivo"]').val(),
                                cursoId: $("#novoCursoId").val(),
                                discId: $("#disciplinasBusca").val()
                            };
                            return arrDados;
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.turma_id;
                                el.text = el.turma_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });

            };

            this.pushUnique = function (arr, obj) {
                for (var row in arr) {
                    if (obj.id == arr[row]['id']) {
                        return true;
                    }
                }

                arr.push(obj);
            };

            this.verificaSeEditaAgente = function () {
                $('[name="pesIdAgente"]').select2('data', __acadperiodoAlunoEdit.getArrAgenteSelect2()).trigger("change");

                if (!__acadperiodoAlunoEdit.options.value.editaAgente && __acadperiodoAlunoEdit.getArrAgenteSelect2()) {
                    $('[name="pesIdAgente"]').select2("disable", true);

                    return false;
                }
                return true;
            };

            this.createOptionFromSelect2Obj = function (obj, el) {
                var options = "";

                if (obj instanceof Array) {
                    for (var row in obj) {
                        var option = new Option(obj[row]['text'], obj[row]['id']);
                        $(el).append($(option));
                    }
                } else {
                    options += "<option value='" + obj['id'] + "'>" + obj['text'] + "</option>";
                }

                return options;
            };

            this.limparDisciplinasNaoAdaptante = function () {
                var $blocoDisciplinas = $("#blocoDisciplinas");
                var arrDisciplinas = [];
                var arrDisciplinasQuePermanecem = [];
                $blocoDisciplinas.find('div.disciplinas').each(function (index, el) {
                    arrDisciplinas.push(el);
                });

                if (arrDisciplinas) {
                    for (var row in arrDisciplinas) {
                        var el = arrDisciplinas[row];
                        var situacao = $(el).find('[name="disciplinaSituacao[]"]').val();
                        var discId = $(el).find('[name="disciplina[]"]').val();

                        if (situacao != "Adaptante" && situacao != "Dependente") {
                            $(el).remove();
                        } else {
                            arrDisciplinasQuePermanecem.push(discId);
                        }
                    }
                } else {
                    $blocoDisciplinas.children().each(function () {
                        $(this).detach();
                    });
                }

                __acadperiodoAlunoEdit.setArrDisciplinasQuePermanecem(arrDisciplinasQuePermanecem);
            };

            this.limparTodasDisciplinas = function () {
                var $blocoDisciplinas = $("#blocoDisciplinas");
                $blocoDisciplinas.children().each(function () {
                    $(this).detach();
                });
            };

            this.montarGradeDeDisciplinas = function (matriz, periodo, turmaId, cursoId, disciplinasNovas) {
                var arrDisciplinasAluno = __acadperiodoAlunoEdit.getArrAlunoDisciplinas(),
                    alunoper = null;
                disciplinasNovas = disciplinasNovas || false;

                if (arrDisciplinasAluno && !turmaId) {
                    return false;
                } else {
                    __acadperiodoAlunoEdit.limparDisciplinasNaoAdaptante();
                }

                if (!disciplinasNovas) {
                    alunoper = __acadperiodoAlunoEdit.getAlunoPer();
                    __acadperiodoAlunoEdit.criarEstruturaDaGradeHtml(arrDisciplinasAluno, true, turmaId);
                    return
                }

                $.ajax({
                    async: false,
                    type: "POST",
                    url: "/matricula/acadperiodo-matriz-disciplina/busca-disciplinas",
                    data: {
                        matriz: matriz,
                        periodo: periodo,
                        alunoPer: alunoper,
                        situacoesValidas: true,
                        naoLimitar: true
                    },
                    success: function (data) {
                        __acadperiodoAlunoEdit.criarEstruturaDaGradeHtml(data.disciplinas, null, turmaId);
                    },
                    error: function (a) {
                        alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
                    }
                });
            };

            this.verificaSeDisciplinaJaEstaNaInterface = function (disc_id) {
                var arrDisciplinasQuePermanecem = __acadperiodoAlunoEdit.getArrDisciplinasQuePermanecem();

                for (var row in arrDisciplinasQuePermanecem) {
                    if (arrDisciplinasQuePermanecem[row] == disc_id) {
                        return true;
                    }
                }

                return false;
            };

            this.criarEstruturaDaGradeHtml = function (disciplinas, redefinir, turma) {
                var $turmas = $("input[name='alunoTurmaPrincipal']");
                var $blocoDisciplinas = $("#blocoDisciplinas");
                var situacoes = "";
                var disc = '';
                var alunodiscId = '';
                var campoTurmaDisciplina = '';
                var camp = '';
                var campo = '';

                $("input[name='situacao']").each(function () {
                    situacoes =
                        situacoes + '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';
                });

                if (disciplinas !== undefined && disciplinas.length > 0) {
                    for (var i = 0; i < disciplinas.length; i++) {
                        disc = disciplinas[i]['disc'];
                        alunodiscId = disciplinas[i]['alunodiscId'];
                        campoTurmaDisciplina = "disciplinaTurma-" + disc + "-" + alunodiscId;
                        arrDisciplinasTurmas = __acadperiodoAlunoEdit.getArrTurmaDisciplina();

                        if (disciplinas[i]['matDesc']) {
                            var sitAtual = '<option value="' + disciplinas[i]['matDesc'] + '">' + disciplinas[i]['matDesc'] + '</option>';
                            situacoes = situacoes.replace(sitAtual, "");
                            situacoes = sitAtual + situacoes;
                        }

                        var discTipoNome = disciplinas[i]['discTipoNome'] || disciplinas[i]['discTipo'];
                        var $disc = $('<div class="row disciplinas">' +
                            '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                            '<label class="input">' +
                            '<input type="hidden" name="novaTurma[]" data-position="' + i +
                            '" value="true">' +
                            '<input type="hidden" name="disciplina[]" value="' + disc + '">' +
                            '<input type="hidden" name="disciplinaTipo[]" value="' +
                            discTipoNome + '">' +
                            disciplinas[i]['discNome'] +
                            '</label>' +
                            '</section>' +
                            '<section class="col-lg-2 col-sm-2 col-xs-2 input" >' +
                            '<input type="text" name="disciplinaTurma[]" id=' + campoTurmaDisciplina + ' ">' +
                            '</section>' +
                            '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                            '<label class="select">' +
                            '<i></i>' +
                            '<select name="disciplinaSituacao[]" >' +
                            situacoes +
                            '</select>' +
                            '</label>' +
                            '</section>' +
                            '</div>'
                        );

                        $blocoDisciplinas.append($disc);

                        if (disciplinas[i]['situacao']) {
                            $disc.find('[name^=disciplinaSituacao]').val(disciplinas[i]['situacao']);
                        }


                        if (!redefinir) {
                            $disc.find("select[name='disciplinaTurma[]']").val($turmas.val());
                        }
                    }

                    $blocoDisciplinas.show();

                    var arrTurma = __acadperiodoAlunoEdit.getTurmaAluno();

                    $('[name^=disciplinaTurma]').each(function (i, item) {
                        var $item = $(item);

                        if ($item.data('select2')) {
                            $item.select2('destroy');
                        }
                    });

                    $('[name^=disciplinaTurma]').select2({
                        ajax: {
                            url: __acadperiodoAlunoEdit.options.url.buscaTurma,
                            dataType: 'json',
                            delay: 250,
                            data: function (e) {
                                return {
                                    query: e,
                                    perId: $('[name="periodoLetivo"]').val(),
                                    cursoId: $("#novoCursoId").val(),
                                    discId: $(this).closest('.row.disciplinas').find('[name^=disciplina]').val(),
                                    naoLimitar: true
                                };
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    el.id = el.turma_id;
                                    el.text = el.turma_nome;

                                    return el;
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    if (disciplinas[0]['turmaId']) {
                        for (var i = 0; i < disciplinas.length; i++) {
                            disc = disciplinas[i]['disc'];
                            alunodiscId = disciplinas[i]['alunodiscId'];
                            campoTurmaDisciplina = "disciplinaTurma-" + disc + "-" + alunodiscId;
                            camp = "#" + campoTurmaDisciplina;

                            campo = $(camp);

                            var arrDisciplinasTurmas = {
                                'id': disciplinas[i]['turmaId'],
                                'text': disciplinas[i]['turmaNome']
                            };

                            if (arrDisciplinasTurmas['id']) {
                                campo.select2('data', arrDisciplinasTurmas).trigger("change");
                            } else {
                                campo.select2('data', arrTurma).trigger('change');
                            }
                        }
                    } else {
                        if (typeof(turma) == "object" && turma) {
                            $('[name^=disciplinaTurma]').select2('data', turma).trigger('change');
                        } else if (arrDisciplinasTurmas && disciplinas) {
                            for (var i = 0; i < disciplinas.length; i++) {
                                disc = disciplinas[i]['disc'];
                                alunodiscId = disciplinas[i]['alunodiscId'];
                                campoTurmaDisciplina = "disciplinaTurma-" + disc + "-" + alunodiscId;
                                camp = "#" + campoTurmaDisciplina;
                                campo = $(camp);

                                if (arrDisciplinasTurmas[disciplinas[i]['disc']]['id']) {
                                    campo.select2('data', arrDisciplinasTurmas[disciplinas[i]['disc']]).trigger("change");
                                } else {
                                    campo.select2('data', arrTurma).trigger('change');
                                }
                            }
                        } else {
                            $('[name^=disciplinaTurma]').select2('data', arrTurma).trigger('change');
                        }
                    }
                } else {
                    __acadperiodoAlunoEdit.showNotificacaoWarning("Não foi localizado disciplinas disponíveis para esse período,curso e turma!");
                }


                for (var row in arrDisciplinasTurmas) {
                    if (Number.isInteger(parseInt(row))) {
                        if (arrDisciplinasTurmas[row]['id'] != arrTurma['id']) {
                            var temp = String(row);
                            var x = String(row).length;

                            while (x != 10) {
                                temp = '0' + temp;
                                x++;
                            }

                            camp = "#disciplinaTurma-" + temp;
                            campo = $(camp);
                            campo.select2("data", arrDisciplinasTurmas[row]).trigger("change");
                        }

                    } else {
                        break;
                    }
                }
            };

            this.getSelect2TurmaOptionsToSelectFormat = function (redefinir) {
                var arrDados = __acadperiodoAlunoEdit.getArrTurmaDoSelect();
                var turmaAluno = __acadperiodoAlunoEdit.getTurmaAluno();

                if (arrDados.length > 0 && !redefinir) {
                    var html = "";
                    for (var row in arrDados) {
                        html +=
                            "<option value='" + arrDados[row]['turma_id'] + "'>" + arrDados[row]['turma_nome'] +
                            "</option>";
                    }
                } else {
                    html = __acadperiodoAlunoEdit.createOptionFromSelect2Obj(turmaAluno);
                }

                return html;
            };

            this.getAlunoPer = function () {
                return __acadperiodoAlunoEdit.options.value.alunoPer || null;
            };

            this.getArrTurmaDoSelect = function () {
                return __acadperiodoAlunoEdit.options.value.optionsTurma || []
            };

            this.getCursoAluno = function () {
                return __acadperiodoAlunoEdit.options.value.arrCurso || null;
            };

            this.getArrAlunoDisciplinas = function () {
                return __acadperiodoAlunoEdit.options.value.arrDisciplinasAluno || null;
            };

            this.getArrDisciplinasQuePermanecem = function () {
                return __acadperiodoAlunoEdit.options.value.arrDisciplinasQuePermanecem || null;
            };

            this.setArrDisciplinasQuePermanecem = function (arrDisciplinasQuePermanecem) {
                return __acadperiodoAlunoEdit.options.value.arrDisciplinasQuePermanecem = arrDisciplinasQuePermanecem;
            };

            this.getPeriodoAluno = function () {
                return __acadperiodoAlunoEdit.options.value.arrPeriodo || null;
            };

            this.getTurmaAluno = function () {
                return __acadperiodoAlunoEdit.options.value.arrTurma || null;
            };
            this.setTurmaAluno = function (arrTurmaDados) {
                return __acadperiodoAlunoEdit.options.value.arrTurma = arrTurmaDados;
            };
            this.getArrOrigemSelect2 = function () {
                return __acadperiodoAlunoEdit.options.data.arrOrigemAlunoCurso || null;
            };
            this.getArrAgenteSelect2 = function () {
                return __acadperiodoAlunoEdit.options.data.arrAgenteEducacional || null;
            };
            this.getArrUnidadeEstudoSelect2 = function () {
                return __acadperiodoAlunoEdit.options.data.arrUnidade || null;
            };
            this.getArrTurmaDisciplina = function () {
                return __acadperiodoAlunoEdit.options.data.disciplinasTurmas || null;
            };
            this.getSerie = function () {
                return __acadperiodoAlunoEdit.options.value.serie || null;
            };
            this.getDisciplinasVinculadas = function () {
                var disciplinas = [];

                $.map($("[name='disciplina[]']"), function (e) {
                    disciplinas.push(e.value);
                });

                return disciplinas || null;
            };
            this.getAlunoMigracao = function () {
                return __acadperiodoAlunoEdit.options.value.alunoMigracao || false;
            };

            this.limparDisciplinas = function () {
                $("#blocoDisciplinas").children().each(function () {
                    $(this).detach();
                });
            };
            this.getCursoAlunoSemPeriodo = function () {
                return __acadperiodoAlunoEdit.options.data.arrCurso || null;
            };

            this.setarDadosDoAluno = function () {
                var $curso = $('[name="curso"]');
                var $turma = $('[name="alunoTurmaPrincipal"]');
                var $periodo = $('[name="periodoLetivo"]');
                var objCurso = __acadperiodoAlunoEdit.getCursoAluno();
                var objTurma = __acadperiodoAlunoEdit.getTurmaAluno();
                var objPeriodo = __acadperiodoAlunoEdit.getPeriodoAluno();

                if (objPeriodo) {
                    $periodo.select2('data', objPeriodo).trigger('change');
                }

                if (objCurso) {
                    $curso.select2('data', objCurso).trigger('change');
                }

                if (objTurma) {
                    $turma.select2('data', objTurma).trigger('change');
                    __acadperiodoAlunoEdit.montarGradeDeDisciplinas(objTurma['matCur'], __acadperiodoAlunoEdit.getSerie(), objTurma['matCur'], objCurso['id'])
                }
            };

            this.run = function (opts) {
                __acadperiodoAlunoEdit.setDefaults(opts);
                __acadperiodoAlunoEdit.setSteps();
            };
        }
        ;

    $.acadperiodoAlunoEdit = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadperiodo-aluno.edit");

        if (!obj) {
            obj = new AcadperiodoAlunoEdit();
            obj.run(params);
            $(window).data('universa.matricula.acadperiodo-aluno.edit', obj);
        }

        return obj;
    };
})
(window.jQuery, window, document);