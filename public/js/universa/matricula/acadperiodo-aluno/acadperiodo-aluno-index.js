(function ($, window, document) {
    'use strict';
    var AcadperiodoAlunoIndex = function () {
        VersaShared.call(this);
        var __acadperiodoAlunoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                add: '',
                edit: '',
                matriculaAlunoCurso: '',
                turma: ''
            },
            data: {
                ultimoPeriodoLetivo: null,
                arrSituacao: []
            },
            value: {
                arrPeriodo: '',
                cursoAluno: ''
            }
            ,
            datatables: {
                acadperiodoAluno: null
            }
        };

        this.setSteps = function () {
            var $search = $("input[type^='search']");
            var $campusCurso = $('#campusCurso');
            var $perId = $('#perId');
            var $cursoId = $('#cursoId');
            var $turmaId = $('#turmaId');
            var $situacao = $('#situacao');
            var periodoLetivo = __acadperiodoAlunoIndex.options.value.periodoLetivo;

            $situacao.select2({
                allowCelar: true,
                multiple: true,
                data: __acadperiodoAlunoIndex.options.data.arrSituacao
            });

            $perId.select2({
                allowClear: true,
                ajax: {
                    url: __acadperiodoAlunoIndex.options.url.periodo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {abertoParaMatricula: 1, vigente: 1, query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.per_nome, id: el.per_id};
                        });

                        transformed.push({text: 'Sem Período', id: 'null'});
                        transformed.push({text: 'Alunos Desligados', id: 'desligados'});
                        transformed.push({text: 'Alunos Egressos', id: 'egresso'});

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $situacao.select2("enable");

                if ($perId.val() == 'desligados' || $perId.val() == 'null') {
                    if ($situacao.val()) {
                        __acadperiodoAlunoIndex.showNotificacaoInfo("Não é possivel selecionar situação para esse período!");
                    }

                    $situacao.select2("disable");
                }

                $campusCurso.select2('val', '').trigger('change');
                $campusCurso.select2($perId.val() ? 'enable' : 'disable');
            });

            $campusCurso.select2({
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __acadperiodoAlunoIndex.options.url.campus,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var perId = $('#perId').select2('data') || {};
                        perId = perId['id'] || '';

                        return {
                            agruparPorCampus: true,
                            query: query,
                            perId: perId
                        }
                    },
                    results: function (data) {

                        var transformed = $.map(data, function (el) {
                            return {text: el.camp_nome, id: el.camp_id};
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $cursoId.select2('val', '').trigger('change');
                $cursoId.select2($campusCurso.val() ? 'enable' : 'disable');
            });

            $cursoId.select2({
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __acadperiodoAlunoIndex.options.url.curso,
                    dataType: 'json',
                    delay: 250,
                    data: function (e) {
                        var perId = $('#perId').select2('data') || {};
                        perId = perId['id'] || '';

                        if (perId['id'] == "desligados") {
                            perId = '';
                        }

                        return {
                            perId: perId,
                            cursoPossuiPeriodoLetivo: 'Sim',
                            campId: $campusCurso.val()
                        }
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.curso_nome, id: el.curso_id, cursocampus: el.cursocampus_id};
                        });

                        return {results: transformed};
                    }
                }
            }).on('change', function () {
                $turmaId.select2('val', '').trigger('change');
                $turmaId.select2($cursoId.val() ? 'enable' : 'disable');
            });

            $turmaId.select2({
                language: 'pt-BR',
                multiple: true,
                ajax: {
                    url: __acadperiodoAlunoIndex.options.url.turma,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var perId = $('#perId').select2('data') || {};
                        perId = perId['id'] || '';

                        return {
                            query: query,
                            cursoId: $cursoId.val(),
                            perId: perId
                        };
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.turma_id;
                            el.text = el.turma_nome;
                            return el;
                        });
                        return {results: transformed};
                    }
                }
            }).on("change", function () {
                $("#situacao").select2("data", []);
            });

            if (periodoLetivo) {
                $perId.select2('data', periodoLetivo).trigger('change');
            }

            $("#alunosFiltroExecutar").click(function () {
                __acadperiodoAlunoIndex.addOverlay($(".dataTable"));
                __acadperiodoAlunoIndex.atualizarListagem();
            });

            $('#alunosFiltroLimpar').click(__acadperiodoAlunoIndex.limparCampos);

            $(document).on('click', '.item-edit', function () {
                var matricula = $(this).attr('data-alunoPerId');

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja reativar o curso ?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        __acadperiodoAlunoIndex.addOverlay(
                            $('.row'),
                            "Aguarde... Estamos redirecionando você"
                        );
                        location.href =
                            __acadperiodoAlunoIndex.options.url.edit + '/' + matricula + '&rematricula=true';
                    }
                });

            });

            $search.focus();

            var aluno = $("input[name^='alunocursoId']").val();

            if (aluno != "") {
                $search.val(aluno).keyup();
            }

            $("tr").on('click', function () {
                var href = $(this).children().find("a").attr('href');

                if (href != undefined) {
                    window.location.replace(href);
                }
            });

            $search.keyup();

            var ultimoPeriodo = __acadperiodoAlunoIndex.options.data.ultimoPeriodoLetivo || '';

            if (ultimoPeriodo) {
                $perId.val(ultimoPeriodo).trigger("change");
            }

            $situacao.select2("enable");

            if ($perId.val() == 'desligados' || $perId.val() == 'null') {
                $situacao.select2('data', []).trigger("change");
                $situacao.select2("disable");
            }

            __acadperiodoAlunoIndex.iniciarListagem();
        };

        this.limparCampos = function () {
            $('#campusCurso').select2('val', '').trigger('change');
            $('#cursoId').select2('val', '').trigger('change');
            $('#turmaId').select2('val', '').trigger('change');

            __acadperiodoAlunoIndex.atualizarListagem();
        };

        this.atualizarListagem = function () {
            __acadperiodoAlunoIndex.options.datatables.acadperiodoAluno.api().ajax.reload(null, false);
        };

        this.iniciarListagem = function () {
            var $tabela = $("#dataTableAcadperiodoAluno");
            var cont = -1;

            __acadperiodoAlunoIndex.options.datatables.acadperiodoAluno = $tabela.dataTable({
                    order: [[0, "desc"]],
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __acadperiodoAlunoIndex.options.url.search,
                        type: "POST",
                        data: function (d) {
                            d.filter = d.filter || {};
                            d.index = true;

                            var campusCurso = $('#campusCurso').val() || "";
                            var turma = $('#turmaId').val() || "";
                            var cursoId = $('#cursoId').val() || "";
                            var perId = $('#perId').select2('data')['id'] || "";
                            var situacao = $("#situacao").val();

                            if (campusCurso) {
                                d.filter['camp_id'] = campusCurso;
                            }

                            if (turma) {
                                d.filter['turma_id'] = turma;
                            }

                            if (cursoId) {
                                d.filter['curso_id'] = cursoId;
                            }

                            if (perId) {
                                d.filter['per_id'] = perId;
                            }

                            if (situacao) {
                                d.filter['situacao_id'] = situacao
                            }

                            if (Object.keys(d.filter).length == 0) {
                                d.filter['naoTrazerDados'] = true;
                            }
                            return d;
                        },
                        dataSrc: function (json) {
                            var data = json.data;
                            var action = json.action;
                            var situacaoDesligamento = json.situacaoDesligamento || '';

                            for (var row in data) {
                                var matriculaId = data[row]['matricula'];
                                var alunoperId = data[row]['alunoper_id'];
                                var periodoAberto = parseInt(data[row]['periodoAbertoAlteraMatricula'] || 0);

                                for (var j in data[row]) {
                                    if (alunoperId == null || !situacaoDesligamento || periodoAberto == 0) {
                                        data[row][j] =
                                            "<a href='" + __acadperiodoAlunoIndex.options.url.matriculaAlunoCurso + '/' + matriculaId + "'>" +
                                            data[row][j] +
                                            "</a>";
                                    } else {
                                        if (j != "acao") {
                                            data[row][j] =
                                                "<a href='" + __acadperiodoAlunoIndex.options.url.edit + '/' + alunoperId + "'>" +
                                                data[row][j] +
                                                "</a>";
                                        }
                                    }
                                }
                            }

                            __acadperiodoAlunoIndex.removeOverlay($(".dataTable"));
                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "matricula", targets: ++cont, data: "matricula"},
                        {name: "nome", targets: ++cont, data: "nome"},
                        {name: "periodo", targets: ++cont, data: "periodo"},
                        {name: "campus", targets: ++cont, data: "campus"},
                        {name: "curso", targets: ++cont, data: "curso"},
                        {name: "turma", targets: ++cont, data: "turma"},
                        {name: "situacao", targets: ++cont, data: "situacao"}

                    ],
                    // Tabletools options:
                    //   https://datatables.net/extensions/tabletools/button_options
                    "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'T>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

                    "oTableTools": {
                        "aButtons": [],
                        "sSwfPath": "/smart-admin/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                    },
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    }
                }
            );
        };

        this.retornaValorPorCampo = function (campo) {
            var result = "";
            var aux = [];

            if (parseInt(campo.val())) {
                result = parseInt(campo.val());
            } else {
                aux = campo.select2("data");

                if (aux.id) {
                    result = parseInt(aux['id']) ? parseInt(aux['id']) : "";
                }
            }
            return result;
        };

        this.run = function (opts) {
            __acadperiodoAlunoIndex.setDefaults(opts);
            __acadperiodoAlunoIndex.setSteps();
        };
    };

    $.acadperiodoAlunoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadperiodo-aluno.index");

        if (!obj) {
            obj = new AcadperiodoAlunoIndex();
            obj.run(params);
            $(window).data('universa.matricula.acadperiodo-aluno.index', obj);
        }

        return obj;
    };
})
(window.jQuery, window, document);