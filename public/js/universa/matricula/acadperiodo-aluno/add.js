jQuery(document).ready(function () {

    var vs = new VersaShared(this);

    jQuery("[name^=endCep]").on('change', function () {
        var $element = $(this);
        var endCep = $element.val();
        var endCepNums = endCep.replace(/[^0-9]/g, '');

        if (endCepNums.length < 8) {
            vs.showNotificacaoWarning('CEP inválido ou não encontrado!');

            return;
        }

        $element.prop('disabled', true);

        $.ajax({
            url: "/pessoa/cidade/busca-info-cep",
            dataType: 'json',
            type: 'post',
            data: {cep: endCep},
            success: function (data) {
                $element.prop('disabled', false);

                if (!data.dados) {
                    vs.showNotificacaoWarning('CEP inválido ou não encontrado!');
                }

                $element.closest('fieldset').find("[name^=endLogradouro]").val(
                    ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                );

                $element.closest('fieldset').find("[name^=endCidade]").val(data.dados.cid_nome || '');
                $element.closest('fieldset').find("[name^=endEstado]").val(data.dados.est_uf || '').trigger('change');
                $element.closest('fieldset').find("[name^=endBairro]").val(data.dados.bai_nome || '');
            },
            erro: function () {
                vs.showNotificacaoDanger('Falha ao buscar CEP!');
                $element.prop('disabled', false);
            }
        });
    });
    $('[name^=pesNaturalidade], [name^=endCidade], [name^=formacaoCidade] ')
        .blur(function () {
            $(this).removeClass('ui-autocomplete-loading');
        })
        .autocomplete({
            source: function (request, response) {
                var $element = $(this.element);
                var previous_request = $element.data("jqXHR");

                if (previous_request) {
                    previous_request.abort();
                }

                $element.data("jqXHR", $.ajax({
                    url: "/pessoa/cidade/busca-info-cep",
                    data: {cidade: request.term},
                    type: 'POST',
                    dataType: "json",
                    success: function (data) {
                        var transformed = $.map(data.dados || [], function (el) {
                            el.label = el.cid_nome;

                            return el;
                        });

                        response(transformed);
                    }
                }));
            },
            minLength: 2,
            focus: function (event, ui) {
                var $element = $(this);
                $element.val(ui.item.cid_nome);

                return false;
            },
            select: function (event, ui) {
                var $element = $(this);
                $element.val(ui.item.cid_nome);
                $element.closest('fieldset').find(
                    $element.attr('name').indexOf('endCidade') != -1 ? "[name^=endEstado]" : (
                        $element.attr('name').indexOf('formacaoCidade') != -1 ?
                            "[name=^formacaoEstado]" : "[name^=pesNascUf]"
                    )
                )
                    .val(ui.item.est_uf).trigger('change');

                return false;
            }
        });

    $('[name^=formacaoEstado]').select2({
        language: 'pt-BR',
        allowClear: true,
        data: function () {
            return {
                results: [{"id": "AC", "text": "Acre"}, {"id": "AL", "text": "Alagoas"}, {
                    "id": "AM",
                    "text": "Amazonas"
                }, {"id": "AP", "text": "Amap\u00e1"}, {"id": "BA", "text": "Bahia"}, {
                    "id": "CE",
                    "text": "Cear\u00e1"
                }, {"id": "DF", "text": "Distrito Federal"}, {"id": "ES", "text": "Esp\u00edrito Santo"}, {
                    "id": "GO",
                    "text": "Goi\u00e1s"
                }, {"id": "MA", "text": "Maranh\u00e3o"}, {"id": "MG", "text": "Minas Gerais"}, {
                    "id": "MS",
                    "text": "Mato Grosso do Sul"
                }, {"id": "MT", "text": "Mato Grosso"}, {"id": "PA", "text": "Par\u00e1"}, {
                    "id": "PB",
                    "text": "Paraiba"
                }, {"id": "PE", "text": "Pernambuco"}, {"id": "PI", "text": "Piau\u00ed"}, {
                    "id": "PR",
                    "text": "Paran\u00e1"
                }, {"id": "RJ", "text": "Rio de Janeiro"}, {"id": "RN", "text": "Rio Grande do Norte"}, {
                    "id": "RO",
                    "text": "Rond\u00f4nia"
                }, {"id": "RR", "text": "Roraima"}, {"id": "RS", "text": "Rio Grande do Sul"}, {
                    "id": "SC",
                    "text": "Santa Catarina"
                }, {"id": "SE", "text": "Sergipe"}, {"id": "SP", "text": "S\u00e3o Paulo"}, {
                    "id": "TO",
                    "text": "Tocantins"
                }]
            };
        }
    });


    $("input[name=pesCpf]:visible").change(function () {
        vs.addOverlay($('.step-content'), 'Carregando dados do aluno!');
    });

    var respNumero = 0
    //Feito pelo caio - gordo
    $("input[name=pesCpf]:visible").change(function () {
        limparCamposPessoa();

        var cpfAluno = $(this).val();
        $.ajax({
            async: true,
            type: "POST",
            url: "/pessoa/pessoa/busca-dados-pessoais",
            delay: 250,
            data: {
                cnpjCpf: cpfAluno,
                endereco: true,
                contato: true,
                atributos: {
                    nome: {classe: "Pes", atributo: "PesNome"},
                    id: {classe: "Pes", atributo: "PesId"},
                    rg: {atributo: "PesRg"},
                    rgEmissao: {atributo: "pesRgEmissao"},
                    dataNascimento: {atributo: "pesDataNascimento"},
                    sexo: {atributo: "pesSexo"},
                    dadosAluno: true
                }
            },
            success: function (data) {
                if (data.nome != undefined) {
                    $("input[name=pes]").val(data.id);
                    $("input[name=pesNome]:visible").val(data.nome);
                    $("input[name=pesRg]:visible").val(data.rg);
                    $("input[name=pesRgEmissao]:visible").val(data.rgEmissao);
                    $("input[name=pesDataNascimento]:visible").val(data.dataNascimento);
                    $("input:radio[name=pesSexo]:visible").filter("[value=" + data.sexo + "]").attr("checked", true);
                    $("input[name='endCep[]']:visible").val(data.cep);
                    $("input[name='endLogradouro[]']:visible").val(data.logradouro);
                    $("input[name='endNumero[]']:visible").val(data.numero);
                    $("input[name='endComplemento[]']:visible").val(data.complemento);
                    $("input[name='endBairro[]']:visible").val(data.bairro);
                    $("input[name='endCidade[]']:visible").val(data.cidade);
                    $("input[name='pesNaturalidade']:visible").val(data.cidade);
                    $("select[name='endEstado[]']:visible").val(data.estado).trigger('change');
                    $("select[name='pesNascUf']:visible").val(data.estado).trigger('change');
                    $("input[name='alunoEmail']:visible").val(data.email);
                    $("input[name='alunoTelefone']:visible").val(data.telefone);
                    $("input[name='alunoCelular']:visible").val(data.celular);
                    $("input[name='pesRgEmissao']:visible").val(data.pesRgEmissao);

                    if (data.alunoDados) {
                        $("input[name='alunoId']").val(data.alunoDados.alunoId);
                        $("input[name='alunoMae']:visible").val(data.alunoDados.alunoMae);
                        $("input[name='alunoPai']:visible").val(data.alunoDados.alunoPai);
                        $("select[name='alunoEtnia']:visible").val(data.alunoDados.alunoEtnia).trigger('change');
                    }
                }
            },
            error: function (a) {
                //alert("Erro ao conectar ao servidor verifique sua conexão a internet!")

            }

        })
        vs.removeOverlay($('.step-content'));
    });

    function strip(str, c) {
        var tmp = str.split(c);
        return tmp.join("");
    }


    $(".cnpjCpf").keyup(function () {
        var tmp = strip($(this).val(), ".");
        tmp = strip(tmp, "/");
        tmp = strip(tmp, "-");
        if (12 < tmp.length) $(this).val(tmp.substr(0, 2) + '.' + tmp.substr(2, 3) + '.' + tmp.substr(5, 3) + '/' + tmp.substr(8, 4) + '-' + tmp.substr(12, 2));
        else if (9 < tmp.length) $(this).val(tmp.substr(0, 3) + '.' + tmp.substr(3, 3) + '.' + tmp.substr(6, 3) + '-' + tmp.substr(9, 3));
        else if (6 < tmp.length) $(this).val(tmp.substr(0, 3) + '.' + tmp.substr(3, 3) + '.' + tmp.substr(6, 3));
        else if (3 < tmp.length)  $(this).val(tmp.substr(0, 3) + '.' + tmp.substr(3, 3));
        else $(this).val(tmp);
    });
    $(".cnpjCpf").on("keydown", function (event) {
        var aKey;

        if (window.event)
            aKey = event.keyCode;
        else
            aKey = event.which;

        //if das teclas precionadas
        if (!(((aKey == 67) && (event.ctrlKey)) || ((aKey == 86) && (event.ctrlKey)) || (aKey == 8) || (aKey == 9) || (aKey == 13) || (aKey == 16) || (aKey == 17) || (aKey == 92)
            || ((aKey >= 96) && (aKey <= 105)) || ((aKey >= 48) && (aKey <= 57)) || ((aKey >= 37) && (aKey <= 40)) || ((aKey >= 112) && (aKey <= 123)))) {
            return false;
        }
    });


    $(".cnpjCpf").blur(function () {
        var tamanho = this.value.length;
        if (tamanho == 18) {
            $("[name='respPesRg']").parent().parent().hide();
            $("[name='respPesRgEmissao']").parent().parent().hide();
        } else {
            $("[name='respPesRg']").parent().parent().show();
            $("[name='respPesRgEmissao']").parent().parent().show();
        }
    });

    jQuery("input[name='pesDocEstrangeiro']").parent().parent().hide();

    jQuery("select[name='pesNacionalidade']").change(function () {
        if (jQuery(this).val() == 'Estrangeiro') {
            jQuery("input[name='pesCpf']").parent().parent().hide();
            jQuery("input[name='pesDocEstrangeiro']").parent().parent().show();
        } else {
            jQuery("input[name='pesCpf']").parent().parent().show();
            jQuery("input[name='pesDocEstrangeiro']").parent().parent().hide();
        }
    });


    //Determina a quantidade inicial de periodos
    var quantPeriodos = jQuery("select[name='curso'] option:selected").attr('data-periodos');
    for (i = 1; i <= quantPeriodos; i++) {
        jQuery("select[name='alunoPeriodo']").append("<option value=" + i + ">" + i + "</option>");
    }


    jQuery("select[name='curso']").click(function () {
        var quantPeriodos = jQuery("select[name='curso'] option:selected").attr('data-periodos');
        var periodos = "";
        for (i = 1; i <= quantPeriodos; i++) {
            periodos += "<option value=" + i + ">" + i + "</option>";
        }
        jQuery("select[name='alunoPeriodo']").html(periodos);
        buscaTurmas();
    });

    //busca turmas para o curso com base no curso, câmpus e periodo
    buscaTurmas();
    jQuery("select[name='alunoPeriodo']").click(function () {
        buscaTurmas();
    });

    function limparCamposPessoa() {
        $("input[name=pes]").val('');
        $("input[name=pesNome]:visible").val('');
        $("input[name=pesRg]:visible").val('');
        $("input[name=pesRgEmissao]:visible").val('');
        $("input[name=pesDataNascimento]:visible").val('');
        $("input[name='endCep[]']:visible").val('');
        $("input[name='endLogradouro[]']:visible").val('');
        $("input[name='endNumero[]']:visible").val('');
        $("input[name='endComplemento[]']:visible").val('');
        $("input[name='endBairro[]']:visible").val('');
        $("input[name='endCidade[]']:visible").val('');
        $("input[name='pesNaturalidade']:visible").val('');
        $("select[name='endEstado[]']:visible").val('').trigger('change');
        $("select[name='pesNascUf']:visible").val('').trigger('change');
        $("input[name='alunoEmail']:visible").val('');
        $("input[name='alunoTelefone']:visible").val('');
        $("input[name='alunoCelular']:visible").val('');
        $("input[name='pesRgEmissao']:visible").val('');

        $("input[name='alunoId']").val('');
        $("input[name='alunoMae']:visible").val('');
        $("input[name='alunoPai']:visible").val('');
        $("select[name='alunoEtnia']:visible").val('').trigger('change');
    }

    jQuery("select[name='alunoTurmaPrincipal']").click(function () {
        buscaDisciplinas();
    });

    function buscaTurmas() {
        $.ajax({
            async: true,
            type: "POST",
            url: "/matricula/acadperiodo-turma/busca-turmas",
            data: {
                cursoCampusId: jQuery("select[name='curso'] option:selected").attr('data-campus'),
                periodo: jQuery("select[name='alunoPeriodo']").val(),
                periodoLetivoAtual: false
            },
            success: function (data) {
                if (data.turmas != undefined) {
                    var turmas = "";
                    for (i = 0; i < data.turmas.length; i++) {
                        turmas = turmas + "<option value=" + data.turmas[i].turmaId + " data-matriz=" + data.turmas[i].matrizId + ">" + data.turmas[i].turmaNome + "</option>";
                    }
                    jQuery("select[name='alunoTurmaPrincipal']").html(turmas);
                    buscaDisciplinas();
                }
            },
            error: function (a) {
                alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
            }

        });
    }

    function buscaDisciplinas(dataDisciplinas) {
        if ($("select[name=alunoTurmaPrincipal] option:selected").attr("data-tipoturma") == "Dependencia") {
            $("#blocoDisciplinas > div").detach();
            return false;
        }

        dataDisciplinas = dataDisciplinas || false;
        $.ajax({
            async: false,
            type: "POST",
            url: "/matricula/acadperiodo-matriz-disciplina/busca-disciplinas",
            data: {
                matriz: $("select[name=alunoTurmaPrincipal] option:selected").attr("data-matriz"),
                periodo: $("select[name=alunoPeriodo] option:selected").val()
            },
            success: function (data) {
                var $turmas = $("select[name=alunoTurmaPrincipal]");
                var situacoes = "";
                $("input[name='situacao']").each(function () {
                    situacoes = situacoes + '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';
                });

                if (data.disciplinas != undefined) {
                    if (!dataDisciplinas) {
                        $("#blocoDisciplinas > div").detach();
                    } else {
                        $("#blocoDisciplinas").children().each(function () {
                            var $this = $(this);
                            var disc = $this.find('[name="disciplina[]"]').val();

                            if (!dataDisciplinas[disc]) {
                                $(this).detach();
                            }
                        });
                    }

                    for (i = 0; i < data.disciplinas.length; i++) {
                        var disc = data.disciplinas[i].disc;
                        if (!dataDisciplinas[disc]) {
                            var $disc = $('<div class="row">' +
                                '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                                '<label class="input">' +
                                '<input type="hidden" name="novaTurma[]" data-position="' + i + '" value="true">' +
                                '<input type="hidden" name="disciplina[]" value="' + disc + '">' +
                                '<input type="hidden" name="disciplinaTipo[]" value="' + data.disciplinas[i].discTipoNome + '">' +
                                '<i></i> ' + data.disciplinas[i].discNome +
                                '</label>' +
                                '</section>' +
                                '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                                '<label class="select">' +
                                '<i></i>' +
                                '<section class="col-lg-2 col-sm-2 col-xs-2 input" >' +
                                '<input type="text" name="disciplinaTurma[]" >' +
                                '</section>' +
                                '</label>' +
                                '</section>' +
                                '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
                                '<label class="select">' +
                                '<i></i>' +
                                '<select name="disciplinaSituacao[]" >' +
                                situacoes +
                                '</select>' +
                                '</label>' +
                                '</section>' +
                                '</div>');
                            $("#blocoDisciplinas").append($disc);
                            $disc.find("select[name='disciplinaTurma[]']").val($turmas.val());
                        }
                    }


                    $("#blocoDisciplinas").show();
                }
            },
            error: function (a) {
                alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
            }

        });
    }


    function buscaTurmasParaDisciplina() {
        $.ajax({
            async: true,
            type: "POST",
            url: "/matricula/acadperiodo-matriz-disciplina/busca-disciplinas",
            data: {
                matriz: jQuery("select[name=alunoTurmaPrincipal] option:selected").attr("data-matriz"),
                periodo: jQuery("select[name=alunoPeriodo] option:selected").val()
            },
            success: function (data) {
                if (data.turmas != undefined) {
                }
            },
            error: function (a) {
                alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
            }

        });
    }

    function adicionaNovoResponsavel() {
        var responsavel = {};
        $("[name^='resp']:visible").each(function () {
            responsavel[this.name] = this.value;
        });
        responsavel['pesNacionalidade'] = $("select[name='respPesNacionalidade']").val();
        responsavel['endCep[]'] = $("input[name='endCep[]']:visible").val();
        responsavel['endLogradouro[]'] = $("input[name='endLogradouro[]']:visible").val();
        responsavel['endCidade[]'] = $("input[name='endCidade[]']:visible").val();
        responsavel['endEstado[]'] = $("select[name='endEstado[]']:visible").val();
        responsavel['endNumero[]'] = $("input[name='endNumero[]']:visible").val();
        responsavel['endComplemento[]'] = $("input[name='endComplemento[]']:visible").val();
        $('#tableResponsaveis').append(
            "<tr data-target='" + respNumero + "'>" +
            "<td>" +
            "<a name='editarResponsavel' data-target='" + respNumero + "'>Editar</a> | " +
            "<a name='removeResponsavel' data-target='" + respNumero + "'>Remover</a>" +
            "<input type='hidden' name='responsavelNivel[]' targetOff='" + respNumero + "' value='" + responsavel['respNivel[]'] + "'>" +
            "<input type='hidden' name='responsavelCnpjCpf[]' targetOff='" + respNumero + "' value='" + responsavel['respPesCnpjCpf'] + "'>" +
            "<input type='hidden' name='responsavelNome[]' targetOff='" + respNumero + "' value='" + responsavel['respPesNome'] + "'>" +
            "<input type='hidden' name='responsavelDocEstrangeiro[]' targetOff='" + respNumero + "' value='" + responsavel['respPesDocEstrangeiro'] + "'>" +
            "<input type='hidden' name='responsavelVinculo[]' targetOff='" + respNumero + "' value='" + responsavel['respVinculo[]'] + "'>" +
            "<input type='hidden' name='responsavelRg[]' targetOff='" + respNumero + "' value='" + responsavel['respPesRg'] + "'>" +
            "<input type='hidden' name='responsavelRgEmissao[]' targetOff='" + respNumero + "' value='" + responsavel['respPesRgEmissao'] + "'>" +
            "<input type='hidden' name='responsavelNacionalidade[]' targetOff='" + respNumero + "' value='" + responsavel['pesNacionalidade'] + "'>" +
            "<input type='hidden' name='responsavelTipoEndereco[]' targetOff='" + respNumero + "' value='1'>" +
            "<input type='hidden' name='responsavelCep[]' targetOff='" + respNumero + "' value='" + responsavel['endCep[]'] + "'>" +
            "<input type='hidden' name='responsavelLogradouro[]' targetOff='" + respNumero + "' value='" + responsavel['endLogradouro[]'] + "'>" +
            "<input type='hidden' name='responsavelCidade[]' targetOff='" + respNumero + "' value='" + responsavel['endCidade[]'] + "'>" +
            "<input type='hidden' name='responsavelEstado[]' targetOff='" + respNumero + "' value='" + responsavel['endEstado[]'] + "'>" +
            "<input type='hidden' name='responsavelBairro[]' targetOff='" + respNumero + "' value='" + responsavel['endBairro[]'] + "'>" +
            "<input type='hidden' name='responsavelNumero[]' targetOff='" + respNumero + "' value='" + responsavel['endNumero[]'] + "'>" +
            "<input type='hidden' name='responsavelComplemento[]' targetOff='" + respNumero + "' value='" + responsavel['endComplemento[]'] + "'>" +
            "<input type='hidden' name='responsavelTelefone[]' targetOff='" + respNumero + "' value='" + responsavel['respTelefone'] + "'>" +
            "<input type='hidden' name='responsavelCelular[]' targetOff='" + respNumero + "' value='" + responsavel['respCelular'] + "'>" +
            "<input type='hidden' name='responsavelEmail[]' targetOff='" + respNumero + "' value='" + responsavel['respEmail'] + "'>" +
            "</td>" +
            "<td>" + responsavel['respPesNome'] + "</td>" +
            "<td>" + responsavel['respPesCnpjCpf'] + "</td>" +
            "<td>" + $("select[name='respVinculo[]']").find("option[value='" + responsavel['respVinculo[]'] + "']").text() + "</td>" +
            "</tr>"
        );
        limpaFormResponsavel();
        respNumero++;
    }

    function limpaFormResponsavel() {
        $("select[name='respNivel[]']").val('NULL');
        $("input[name='respPesCnpjCpf']").val('');
        $("input[name='respPesNome']").val('');
        $("input[name='respPesDocEstrangeiro']").val('');
        $("select[name='respVinculo[]']").val('NULL');
        $("input[name='respPesRg']").val('');
        $("input[name='respPesRgEmissao']").val('');
        $("select[name='respPesNacionalidade']").val("NULL");
        $("input[name='endCep[]']:visible").val("");
        $("input[name='endLogradouro[]']:visible").val("");
        $("input[name='endCidade[]']:visible").val("");
        $("select[name='endEstado[]']:visible").val("NULL");
        $("input[name='endNumero[]']:visible").val("");
        $("input[name='endComplemento[]']:visible").val("");
        $("input[name='respTelefone']:visible").val("");
        $("input[name='respEmail']:visible").val("");
        $("input[name='respCelular']:visible").val("");
    }

    $('#respAdd').click(function () {
        if ($('#resp').hasClass('hidden')) {
            $('#resp').removeClass('hidden');
            $spine().validateForm();
        } else {
            $('#resp').addClass('hidden');
        }
        if (!$('#editaResp').hasClass('hidden')) {
            $('#editaResp').addClass('hidden');
            $('#adicionarResp').removeClass('hidden');
        }
        limpaFormResponsavel();
    });
    $("select[name='respPesNacionalidade']").change(function () {
        if (this.value == 'Estrangeiro') {
            $("#documentoResp").append("<section class='col-lg-8 col-sm-8 col-xs-8' style='padding-left:30px;'><label class='label'>Documento Estrangeiro:</label><label class='input'><i class='icon-prepend fa fa-credit-card'></i><input name='respPesDocEstrangeiro' placeholder='Informe o documento estrangeiro' data-validations='Required' value='' type='text'></label><div class='note'></div></section>");
            $("input[name='respPesCnpjCpf']").parent().parent().addClass('hidden');
//            <label class='label'>CPF</label><label class='input'><i class='icon-prepend fa fa-credit-card'></i><input name='pesCpf' placeholder='Informe o CPF' data-validations='Required,Cpf' data-masked='Cpf' value='' type='text'></label><div class='note'></div>
        }
        else {
            $("input[name='respPesCnpjCpf']").parent().parent().removeClass('hidden');
            $("input[name='respPesDocEstrangeiro']").parent().parent().detach();
        }
    });
    $('#adicionarResp').click(function () {
        if (stepValidate(valida, 0)) {
            adicionaNovoResponsavel();
            $('#resp').addClass('hidden');
        }
    });
    $(document).on('click', "a[name='editarResponsavel']", function () {
        var target = $(this).attr('data-target');
        var resp = {};
        $("input[targetOff='" + target + "']").each(function () {
            resp[this.name] = this.value;
        });

        if ($('#resp').hasClass('hidden')) {
            $('#resp').removeClass('hidden');
        }
        $("select[name='respNivel[]']").val(resp['responsavelNivel[]']);
        $("input[name='respPesCnpjCpf']").val(resp['responsavelCnpjCpf[]']);
        $("input[name='respPesNome']").val(resp['responsavelNome[]']);
        $("input[name='respPesDocEstrangeiro']").val(resp['responsavelDocEstrangeiro[]']);
        $("select[name='respVinculo[]']").val(resp['responsavelVinculo[]']);
        $("input[name='respPesRg']").val(resp['responsavelRg[]']);
        $("input[name='respPesRgEmissao']").val(resp['responsavelRgEmissao[]']);
        $("select[name='respPesNacionalidade']").val(resp['responsavelNacionalidade[]']);
        $("input[name='endCep[]']:visible").val(resp['responsavelCep[]']);
        $("input[name='endLogradouro[]']:visible").val(resp['responsavelLogradouro[]']);
        $("input[name='endCidade[]']:visible").val(resp['responsavelCidade[]']);
        $("select[name='endEstado[]']:visible").val(resp['responsavelEstado[]']);
        $("input[name='endNumero[]']:visible").val(resp['responsavelNumero[]']);
        $("input[name='endComplemento[]']:visible").val(resp['responsavelComplemento[]']);
        $("input[name='respTelefone']:visible").val(resp['responsavelTelefone[]']);
        $("input[name='respCelular']:visible").val(resp['responsavelCelular[]']);
        $("input[name='respEmail']:visible").val(resp['responsavelEmail[]']);

        if ($('#editaResp').hasClass('hidden')) {
            $('#editaResp').removeClass('hidden');
            $('#adicionarResp').addClass('hidden');
        }
        $('#editaResp').attr('data-target', target);

    });
    $("#editaResp").click(function () {
        var targ = $(this).attr('data-target');
        adicionaNovoResponsavel();
        $("tr[data-target='" + targ + "']").detach();
        $('#resp').addClass('hidden');
    });

    /**
     * Script para fazer a mudança de um passo para outro.
     * */
    var valida = {
        0: {
            'respNivel[]': 'select',
            respPesNacionalidade: 'select',
            respPesCnpjCpf: 'input',
            respPesNome: 'input',
            respPesDocEstrangeiro: 'input',
            'respVinculo[]': 'select',
            respPesRg: 'input',
            respPesRgEmissao: 'input',
            'endCep[]': 'input',
            'endLogradouro[]': 'input',
            'endBairro[]': 'input',
            'endCidade[]': 'input',
            'endEstado[]': 'select'
        }
    };

    $("#next").click(function () {
        step = $("ul[class='steps']").find("li.active").attr('data-target');

        if (step == "#step2") {
            $("#next").text("Salvar");

        }
        validations = {
            step1: {
                pesNome: 'input',
                alunoFormaIngresso: 'select',
                pesNacionalidade: 'select',
                pesCpf: 'input',
                pesRg: 'input',
                //pesRgEmissao: 'input',
                pesSexo: 'input',
                alunoEtnia: 'select',
                pesFalecido: 'select',
                alunoPai: 'input',
                alunoMae: 'input',
                'endCep[]': 'input',
                'endLogradouro[]': 'input',
                'endNumero[]': 'input',
                'endBairro[]': 'input',
                'endCidade[]': 'input',
                'endEstado[]': 'select',
                //alunoTelefone: 'input',
                //alunoCelular: 'input'
            },
            step2: {},
            step3: {
                curso: 'select',
                alunoPeriodo: 'select',
                alunoTurmaPrincipal: 'select'
            }
        };

        if (stepValidate(validations, step.replace('#', ''))) {
            proximoStep = parseInt(step.replace('#step', '')) + 1;
            proximoStep = '#step' + proximoStep;

            if ($("li[data-target='" + proximoStep + "']")) {
                $(step).removeClass('active');
                $('#prev').removeAttr('disabled');
                if ($(proximoStep).length > 0) {
                    $("ul[class='steps']").find("li.active").attr('data-target', step).addClass('complete').removeClass('active');
                    $(proximoStep).addClass('active');
                    $("li[data-target='" + proximoStep + "']").addClass('active');
                    $spine().validateForm();
                }
            }
            if (step == "#step3") {
                $("#next").attr("type", "submit");
                $("form[name='PessoaFisica']").submit(function () {
                    $("#cursoCampus").val(jQuery("select[name='curso'] option:selected").attr('data-campus'));
                    if (stepValidate(validations, 'step3')) {
                        return true;
                    } else {
                        return false;
                    }
                });
            }
            var vs = VersaShared(this);

            if ($("#next")[0].type == "submit") {

                var erro = false;

                if (!$("#cursoId").val()) {
                    vs.showNotificacaoWarning("Selecione o curso do aluno!");
                    erro = true;
                }
                if (!$('[name="alunoTurmaPrincipal"]').val()) {

                    vs.showNotificacaoWarning("Selecione a turma principal do aluno!");
                    erro = true;
                }
                if (!$('[name="periodoLetivo"]').val()) {

                    vs.showNotificacaoWarning("Selecione o período letivo!");
                    erro = true;
                }

                var disciplinas,
                    disciplinaTurma;

                /*if (!$('[name="disciplina[]"]').val()) {
                    vs.showNotificacaoWarning("Selecione pelo menos uma disciplina!");
                    erro = true;
                }*/

                var disciplinas = [];
                $.map($("[name='disciplina[]']"), function (e) {
                    disciplinas.push(e.value);
                });

                $.each(disciplinas, function (index, value) {
                    var camp = "#disciplinaTurma" + value;
                    if (!$(camp).val()) {
                        vs.showNotificacaoWarning("Selecione todas as turmas para as  disciplinas!");
                        erro = true;
                    }
                });


                if (erro) {
                    $("#next")[0].type = "button";
                    $("#prev").click();
                    return false;
                }

                vs.addOverlay($(".widget-body"));
                vs.showNotificacaoSuccess("Aguarde enquanto a matricula é registrada!");
            }

        }
        $("li[class='complete']").bind('click', function () {
            atual = $('ul.steps').find("li.active").attr('data-target');
            atual = parseInt(atual.replace('#step', ''));
            destino = $(this).attr('data-target');
            destino = parseInt(destino.replace('#step', ''));
            while (atual > destino) {
                $("li[data-target='#step" + atual + "']").removeClass('active complete');
                $("#step" + atual).removeClass('active')
                atual--;
            }
            if (atual == 1) {
                $('#prev').attr('disabled', 'disabled');
            }
            $("li[data-target='#step" + atual + "']").addClass('active');
            $("#step" + atual).addClass('active');
        });

    });
    $("#prev").click(function () {
        $("#next").text("Proximo");

        step = $("ul[class='steps']").find("li.active").attr('data-target');
        $(step).removeClass('active');
        $("ul[class='steps']").find("li.active").attr('data-target', step).removeClass('active complete');

        proximoStep = parseInt(step.replace('#step', '')) - 1;
        if (proximoStep == 1) {
            $('#prev').attr('disabled', 'disabled');
        }
        proximoStep = '#step' + proximoStep;
        if ($(proximoStep)) {
            $(proximoStep).addClass('active');
            $("li[data-target='" + proximoStep + "']").addClass('active');
        }
    });

    /**
     * Funçao que valida os campos de um passo do wizard,
     * recebe como parametro um objeto contendo o passo ex:
     //    validations = {
     //        step1: { Aqui esta o nome do passo, que no caso e o id do <li> da lista de passos.
     //            pesNome: 'input' Aqui temos o nome do campo e o tipo dele.
     //        }
     //    }
     * */
    function stepValidate(validacao, passo) {
        var stop = false;
        if (!validacao[passo]) {
            console.log("Não foi encontrada nenhuma validação para o step passado.");
            return false;
        }
        for (elementoNome in validacao[passo]) {
            //Recebe o elemento que será validado.
            elemento = $(validacao[passo][elementoNome] + "[name='" + elementoNome + "']:visible");

            //Recebe as funções de validação que serão executadas para o elemento.
            funcao = $(elemento).attr('data-validations');
            if (funcao) {
                if (funcao.indexOf(',')) {
                    funcao = funcao.split(',');
                }
                for (i = 0; i < funcao.length; i++) {
//                retorno = $spine(elemento).funcValidations[ funcao[i] ](elemento.value);
                    if (!$spine(elemento).funcValidations[funcao[i]]($(elemento).val())) {
                        $(elemento).parent().addClass('state-error');
                        $(elemento).parent().focus();
                        $(elemento).parent().parent().find("div[class^='note']").addClass('note-error').html($spine().msgValidations[funcao[i]]);
                        stop = true;
                        break;
                    } else {
                        $(elemento).parent().removeClass('state-error');
                        $(elemento).parent().parent().find("div[class^='note']").removeClass('note-error').html("");
                    }
                }
            }
        }
        return !stop;
    }

    jQuery("input[label='Certificado de Reservista']").click(function () {
        if (jQuery(this).val() != "Sim") {
            jQuery("input[name='alunoCertMilitar']").parent().parent().hide();
        } else {
            jQuery("input[name='alunoCertMilitar']").parent().parent().show();
        }
    });
    //jQuery("select[name='alunoFormaIngresso']").attr('disabled','disabled');
});

$(document).on('click', "a[name='removeResponsavel']", function () {
    if (confirm("Deseja realmente remover este responsável ?")) {
        $(this).parent().parent().detach();
    }
});

function previewFile(element) {
    var preview = document.getElementById('preview_image');
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        preview.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
}

function buscaTurmas() {
    $.ajax({
        async: false,
        type: "POST",
        url: "/matricula/acadperiodo-matriz-disciplina/busca-turmas-que-ofertam-disciplina",
        data: {
            disciplinasId: $("#disciplinasBusca").find("optgroup").find("option:selected").val()
        },
        success: function (data) {
            if (data.turmas != undefined) {
                var options = "";

                for (var i = 0; i < data.turmas.length; i++) {
                    options = options + "<option value='" + data.turmas[i].id + "'>" + data.turmas[i].nome + "</option>";
                }

                $("#turmasOfertantes").children().remove();
                $("#turmasOfertantes").append(options);
            }
        },
        error: function (a) {
            alert("Erro ao conectar ao servidor verifique sua conexão a internet!");
        }

    });
}

function incluiDependencia(dep) {
    dep = dep.split('|');
    dep[3] = parseInt(dep[3]);

    //var turma = $("select[data-dep='" + dep[3] + "'] > option:selected");
    var turma = $("select[data-dep='" + dep[3] + "']");
    var situacoes = "";
    $("input[name='situacao']").each(function () {
        situacoes = situacoes + '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';
    });

    $("#blocoDisciplinas").append(
        "<div class='row'>" +
        "<section class='col-lg-2 col-sm-2 col-xs-2' style='padding-left:30px;'>" +
        "<label class='input'>" +
        "<input type='hidden' name='disciplina[]' value='" + dep[3] + "'>" +
        "<input type='hidden' name='novaTurma[]' value='true'>" +
        dep[4] +
        "</label>" +
        "</section>" +
        '<section class="col-lg-2 col-sm-2 col-xs-2 input" >' +
        '<input type="text" name="disciplinaTurma[]" >' +
        '</section>' +
        "<section class='col-lg-2 col-sm-2 col-xs-2' style='padding-left:30px;'>" +
        "<label class='select'>" +
        "<select name='disciplinaSituacao[]' >" +
        situacoes +
        "</select>" +
        "</label>" +
        "</section>" +
        "<section class='col-lg-3 col-sm-3 col-xs-3' style='padding-left:20px;'>" +
        "<label>" +
        "<button type='button' class='btn btn-sm btn-danger' onclick='removeDependencia(this)'>Remover Diciplina</button>" +
        '</label>' +
        '</section>' +
        "</div>"
    );

    $("select[name='disciplinaTurma[]']:last").append(turma.html()).val(turma.val());

    $("select[name='disciplinaSituacao[]']:last").val('Dependente').attr('readonly', true);

    $("select[data-dep='" + dep[3] + "']").parent().parent().parent().hide();

}

function salvaDisciplina() {
    var vs = new VersaShared();
    var arrTurma = $('[name^=turmasOfertantes]').select2('data'),
        arrDisciplina = $('[name^=disciplinasBusca]').select2('data'),
        erro = false;

    if (!arrTurma) {
        vs.showNotificacaoWarning("Selecione uma disciplicina!");
        erro = true;
    }

    if (!arrDisciplina) {
        vs.showNotificacaoWarning("Selecione a uma turma para essa disciplicina!");
        erro = true;
    }

    if (erro) {
        return false;
    }

    vs.addOverlay($(".widget-body"));

    var diciplina = $("#disciplinasBusca:visible").find("option:selected");
    var turma = $("#turmasOfertantes:visible");
    var situacoes = "";
    $("input[name='situacao']").each(function () {
        situacoes = situacoes + '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';
    });
    var i = $("input[name^='novaTurma']").length;
    var discNome = arrDisciplina['disc_nome'] ? arrDisciplina['disc_nome'] : arrDisciplina['disciplina_nome'],
        discId = arrDisciplina['id'] ? arrDisciplina['id'] : arrDisciplina['disc_id'],
        campoTurmaDisciplina = "disciplinaTurma" + discId;

    $("#blocoDisciplinas").append(
        '<div class="row" id="removeDisicplina' + i + '">' +
        '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
        '<label class="input">' +
        '<input type="hidden" name="disciplina[]" value="' + discId + '">' +
        discNome +
        '</label>' +
        '</section>' +
        '<section class="col-lg-2 col-sm-2 col-xs-2 input" >' +
        '<input type="text" name="disciplinaTurma[]" id=' + campoTurmaDisciplina + '>' +
        '</section>' +
        '</label>' +
        '</section>' +
        '<section class="col-lg-2 col-sm-2 col-xs-2" style="padding-left:30px;">' +
        '<label class="select">' +
        '<i></i>' +
        '<select name="disciplinaSituacao[]" >' +
        situacoes +
        '</select>' +
        '</label>' +
        '</section>' +
        '<section class="col-lg-3 col-sm-3 col-xs-3" style="padding-left:20px;">' +
        '<label>' +
        '<button type="button" class="btn btn-sm btn-danger" onclick="removeDisciplina(this)">Remover Diciplina</button>' +
        '</label>' +
        '</section>' +
        '</div>'
    );

    campoTurmaDisciplina = '#' + campoTurmaDisciplina;
    $(campoTurmaDisciplina).select2({
        ajax: {
            url: "/matricula/acadperiodo-turma/search-for-json",
            dataType: 'json',
            delay: 250,
            data: function (query) {

                var arrDados = {
                    query: query,
                    perId: $('[name="periodoLetivo"]').val(),
                    cursoId: $("#novoCursoId").val(),
                    discId: $(this).closest('.row.disciplinas').find('[name^=disciplina]').val()
                };
                return arrDados;
            },
            results: function (data) {
                var transformed = $.map(data, function (el) {
                    el.id = el.turma_id;
                    el.text = el.turma_nome;

                    return el;
                });
                return {results: transformed};
            }
        }
    });

    EscondeFormBuscaDisciplina();

    $(campoTurmaDisciplina).select2("data", arrTurma).trigger("change");
    vs.removeOverlay($(".widget-body"));
}

function ExibeFormBuscaDisciplina() {

    var vs = new VersaShared();
    var erro = false;

    if (!$("#cursoId").val()) {
        vs.showNotificacaoWarning("Selecione o curso do aluno!");
        erro = true;
    }
    if (!$('[name="alunoTurmaPrincipal"]').val()) {

        vs.showNotificacaoWarning("Selecione a turma principal do aluno!");
        erro = true;
    }
    if (!$('[name="periodoLetivo"]').val()) {

        vs.showNotificacaoWarning("Selecione o período letivo!");
        erro = true;
    }

    if (erro) {
        return false;
    }


    $("#formBuscaDisciplina").removeAttr('hidden');
    $("#BtBuscaDisciplinas").attr('hidden', 'hidden');
    $("#BtCancelaBuscaDisciplinas").removeAttr('hidden');
}
function EscondeFormBuscaDisciplina() {
    $("#formBuscaDisciplina").attr('hidden', 'hidden');
    $("#BtBuscaDisciplinas").removeAttr('hidden');
    $("#BtCancelaBuscaDisciplinas").attr('hidden', 'hidden');
}

function removeDependencia($el) {
    var dep = $($el).parent().parent().parent().find("input[name='disciplina[]']").val();
    $("select[data-dep='" + dep + "']").parent().parent().parent().show();

    removeDisciplina($el);
}
function removeDisciplina(element) {
    $(element).parent().parent().parent().detach();
}

function validateEmail(element) {
    var email = $(element).val();
    var regra = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

    if (!regra.test(email)) {
        alert('Favor inserir um email valido!');
        $(element).val("");
        $(element).focus();
    }
}

function validaResponsavelDocumento(element) {
    if ($(element).val() == $("input[name='pesCpf']").val()) {
        alert("O responsável não pode ser o proprio aluno!");
        $(element).focus();
        $(element).val('');
    }
}