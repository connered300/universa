(function ($, window, document) {
    'use strict';

    var IntegradoraAdd = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlAutocompleteTurma: '',
                urlAutocompleteDisciplina: '',
                check: false,
                turmasSelecionadas: null,
                cadernosSelecionadas: null,
                gruposSelecionados: null,
                avaliacoesCadastradas: null,
                urlGerenciadorDeArquivosDownload: '',
                urlGerenciadorDeArquivosThumb: '',
                urlPreview: '',
                arrTurmas: {},
                arrDisciplinas: {}
            },
            mensagens: {
                Searching: 'Pesquisando...',
                NoMatches: 'Pesquisa não encontrada.'
            }

        };

        priv.setDefaults = function (opts) {
            opts = opts || [];

            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };

        priv.iniciaFormulario = function () {
            var periodoOld;
            var turmaOld = "";
            var matrizId = "";
            var changed = false;
            var getDisciplinas = function () {
                var itens = $("#cadernos").componente('exportJSON');
                var disciplinasSelecionadas = [];

                $.map(itens, function (el) {
                    if (el.disciplinas) {
                        disciplinasSelecionadas = $.merge(disciplinasSelecionadas, el.disciplinas.split(','));
                    }
                });

                var disciplinas = [];

                $.grep(priv.options.arrDisciplinas, function (el) {
                    var disciplina = el.id + "";

                    if ($.inArray(disciplina, disciplinasSelecionadas) == -1) {
                        disciplinas.push(el);
                    }
                });
                return disciplinas;
            };


            var organizaCadernos = function (e) {
                e.preventDefault();
                if (e.added) {
                    if (e.added['matrizId'] != matrizId && matrizId) {
                        $("#turmas").select2('val', turmaOld);
                        $.smallBox({
                            title: "Não foi possível adicionar turma",
                            content: "<i>A matriz da turma selecionada diverge da matriz das turmas selecionadas.</i>",
                            color: "#C46A69",
                            icon: "fa fa-times",
                            timeout: 5000
                        });

                        return false;
                    }
                }

                var val = (e.val || []);

                if (val.length == 0) {
                    var cadernos = $("#cadernos").componente('items');

                    if (cadernos.length > 0 && !changed) {
                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Ao mudar o remover todas as turmas, os cadernos incluídos na avaliação ' +
                            'serao removidos.\n Deseja Prosseguir?',
                            buttons: "[Cancelar][Ok]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Cancelar") {
                                $("#turmas").select2('val', turmaOld);
                                return false;
                            }

                            matrizId = null;
                            $("#cadernos .btn-add").prop('disabled', true).addClass('disabled');
                            $("#cadernos").componente('removeAll');
                        });

                        return false;
                    } else {
                        matrizId = null;
                        $("#cadernos .btn-add").prop('disabled', true).addClass('disabled');
                        $("#cadernos").componente('removeAll');
                    }
                }

                turmaOld = $("#turmas").val().split(',') || false;
                var matrizIdNew = $("#turmas").select2('data')[0] || null;

                if (matrizIdNew) {
                    matrizIdNew = matrizIdNew['matrizId'];

                    if (matrizIdNew != matrizId) {
                        matrizId = matrizIdNew;

                        $("#cadernos .btn-add").prop('disabled', true).addClass('disabled');

                        $.ajax({
                            url: priv.options.urlAutocompleteDisciplina,
                            type: 'POST',
                            data: {
                                periodo: $("#periodo").val(),
                                matriz: matrizId
                            },
                            success: function (data) {
                                priv.options.arrDisciplinas = $.map(data.disciplinas, function (el) {
                                    return {id: el.disc, text: el.discNome};
                                });

                                $("#cadernos .btn-add").prop('disabled', false).removeClass('disabled');
                            }
                        });
                    }
                } else if (!matrizId) {
                    $("#cadernos .btn-add").prop('disabled', true).addClass('disabled');
                }

                changed = false;
            };
            var organizaGabarito = function (e) {
                var disciplinas = $(this).val();
                var componenteItem = $(this).closest('.componente-item');
                var $gabarito = componenteItem.find('[name*="[gabarito]"]');
                var $gabaritoBtn = componenteItem.find('[name*="[gabarito-btn]"]');
                var gabarito = JSON.parse($gabarito.val() || '{}');
                var novoGabarito = {};
                var novoIndice = 0;
                if (disciplinas) {
                    disciplinas = disciplinas.split(',');

                    for (var chave in gabarito) {
                        var disciplina = (chave.split('-')[0] || '') + '';

                        if ($.inArray(disciplina + '', disciplinas) >= 0) {
                            novoGabarito[chave] = gabarito[chave];
                            novoGabarito[chave]['ordemGeral'] = novoIndice;
                        }

                        novoIndice++;
                    }
                }

                if (e.added) {
                    var numQuestaoPorDisciplina = parseInt($('#quantidaQuestoes').val());

                    for (var numQuestao = 0; numQuestao < numQuestaoPorDisciplina; numQuestao++) {
                        var chave = e.added.id + '-' + numQuestao;
                        novoGabarito[chave] = {
                            ordemGeral: novoIndice, ordem: numQuestao, disciplina: e.added.id, resposta: ''
                        };
                        novoIndice++;
                    }
                }

                $gabaritoBtn.prop('disabled', (disciplinas == ""));
                $gabarito.val(JSON.stringify(novoGabarito));
            };
            var montaGabarito = function (e) {
                var tableTpl = '\
                    <table class="table table-bordered">\
                        <thead>\
                            <tr><th colspan="3" class="nome-disciplina text-center"></th></tr>\
                            <tr><th>Ordem</th><th>Resposta</th><th>Inválida</th></tr>\
                        </thead>\
                        <tbody></tbody>\
                    </table>';
                var numQuestaoPorDisciplina = parseInt($('#quantidaQuestoes').val());
                var numQuestao = 0;
                var componenteItem = $(e.currentTarget).closest('.componente-item');
                var disciplinas = componenteItem.find('[name*="[disciplinas]"]').val().split(',');
                var $gabarito = componenteItem.find('[name*="[gabarito]"]');
                var valores = JSON.parse($gabarito.val() || '{}');
                $('#modal-gabarito .modal-body').html('');

                for (var i in disciplinas) {
                    var item = $.grep(priv.options.arrDisciplinas, function (el) {
                        return el.id + '' == disciplinas[i];
                    })[0];
                    var $tabela = $(tableTpl);

                    $tabela.find('.nome-disciplina').html(item.text);

                    for (var j = 0; j < numQuestaoPorDisciplina; j++) {
                        var identificador = 'questao[' + item.id + '-' + j + ']';
                        var resposta = '';
                        var checked = false;

                        if (valores[item.id + '-' + j]) {
                            resposta = valores[item.id + '-' + j]['resposta'] || '';
                            checked = valores[item.id + '-' + j]['invalida'] || false;
                        }

                        var lineTpl = '\
                            <tr>\
                                <td class="text-center">\
                                    <div class="form-group">\
                                        <p class="form-control-static">' + (numQuestao + 1) + '</p>\
                                        <input type="hidden" name="' + identificador + '[ordemGeral]" value="' + numQuestao + '">\
                                        <input type="hidden" name="' + identificador + '[ordem]" value="' + j + '">\
                                        <input type="hidden" name="' + identificador + '[disciplina]" value="' + item.id + '">\
                                    </div>\
                                </td>\
                                <td>\
                                     <input class="form-control" type="text" name="' + identificador +
                            '[resposta]" style="text-transform: uppercase" value="' + resposta + '">\
                                </td>\
                                <td class="text-center">\
                                    <input type="checkbox" name="' + identificador + '[invalida]" value="1"' +
                            (checked == 0 ? '' : 'checked') + '>\
                                </td>\
                            </tr>';
                        $tabela.find('tbody').append(lineTpl);
                        numQuestao += 1;
                    }

                    $('#modal-gabarito .modal-body').append($tabela);
                    $('#gabarito-save').unbind('click');
                    $('#gabarito-save').bind('click', function () {
                        var form = $('#gabarito-form').serializeJSON();
                        $gabarito.val(JSON.stringify(form.questao));

                        $('#modal-gabarito').modal('hide');
                    });
                }

                $('#modal-gabarito').modal();
            };

            var recarregaTurmas = function () {
                changed = true;
                matrizId = null;
                turmaOld = null;
                $("#turmas").prop('disabled', true).trigger("change").select2("val", '');

                $.ajax({
                    url: priv.options.urlAutocompleteTurma,
                    type: 'POST',
                    async: false,
                    data: {
                        periodoLetivo: $("#periodoLetivo").val(),
                        periodo: $("#periodo").val(),
                        cursoCampusId: $("#cursosCampus").val(),
                        periodoLetivoAtual: true,
                        turno: $("#turno").val()
                    },
                    success: function (data) {
                        priv.options.arrTurmas = $.map(data.turmas, function (el) {
                            return {id: el.turmaId, text: el.turmaNome, matrizId: el.matrizId};
                        });
                        var turmas = new Array();
                        for(var i = 0; i < data.turmas.length; i++){
                            turmas[i] =  data.turmas[i].turmaId
                        }

                        $("#turmas").prop('disabled', true);
                        $("#turmas").select2('val',turmas).trigger("change");
                    }
                });
            };


            $("#periodo").select2({
                allowClear: true,
                formatSearching: function () {
                    return priv.mensagens.Searching;
                },
                formatNoMatches: function () {
                    return priv.mensagens.NoMatches;
                },
            }).on('select2-open', function () {
                periodoOld = $('#periodo').val() || false;
            }).on("change", function (e) {
                e.preventDefault();
                var cadernos = $("#cadernos").componente('items');

                if (cadernos.length > 0) {
                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: 'Ao mudar o periodo, turma ou turno, os cadernos incluídos na avaliação ' +
                        'serao removidos.\n Deseja Prosseguir?',
                        buttons: "[Cancelar][Ok]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Cancelar") {
                            $("#periodo").select2("val", periodoOld);
                            return false;
                        }

                        recarregaTurmas();
                    });
                    return false;
                }

                recarregaTurmas();
            });


            $("#turno").select2({
                allowClear: true,
                formatSearching: function () {
                    return priv.mensagens.Searching;
                },
                formatNoMatches: function () {
                    return priv.mensagens.NoMatches;
                }
            }).on('change', function () {
                recarregaTurmas();
            });


            $("#turmas").select2({
                allowClear: true,
                formatSearching: function () {
                    return priv.mensagens.Searching;
                },
                formatNoMatches: function () {
                    return priv.mensagens.NoMatches;
                },
                multiple: true,
                data: function () {
                    return {results: priv.options.arrTurmas};
                }
            }).on("change", organizaCadernos);



            if (priv.options.turmasSelecionadas) {
                $("#turmas").select2('data', priv.options.turmasSelecionadas);
            }


            $("#cadernos").componente({
                inputName: 'caderno',
                classCardExtra: 'col-sm-12',
                labelContainer: '<div class="form-group"/>',
                tplItem: '\
                        <div class="well">\
                            <div class="remover-caderno"">\
                                <a href="#" class="btn btn-danger btn-xs item-remove">\
                                        <i class="fa fa-times fa-inverse"></i>\
                                </a>\
                             </div>\
                            <div class="componente-content"">\
                            </div >\
                        </div>',
                fieldClass: 'form-control',
                fields: [
                    {
                        name: 'nome', label: 'Nome', type: 'text',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-4');
                        }
                    },
                    {
                        name: 'data-aplicacao', label: 'Data de aplicação', type: 'text',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-2');
                            field.mask("99/99/9999");
                            field.datepicker();
                        }
                    },
                    {
                        name: 'disciplinas', label: 'Disciplinas', type: 'text',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();

                            $parent.addClass('col-sm-4');

                            field.select2({
                                allowClear: true,
                                formatSearching: function () {
                                    return priv.mensagens.Searching;
                                },
                                formatNoMatches: function () {
                                    return priv.mensagens.NoMatches;
                                },
                                tags: true,
                                data: function () {
                                    return {results: getDisciplinas()};
                                }
                            }).on('change', organizaGabarito);

                            if (itemConfig['disciplinas']) {
                                field.select2("val", itemConfig['disciplinas']).trigger("change");
                            }


                        }
                    },
                    {
                        name: 'gabarito', label: 'Gabarito', type: 'textarea', addCallback: function (e) {
                        var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                        var $parent = field.parent();
                        $parent.hide();
                    }
                    },
                    {
                        name: 'gabarito-btn', label: 'gabarito-btn', type: 'button',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-2');
                            $parent.find('.componente-label').html('&nbsp;');
                            field.attr('class', 'btn btn-primary').val('Vincular gabarito').prop('disabled', false);
                            field.click(montaGabarito);
                        }
                    }
                ],
                removeCallback: function (parametersCallback) {
                    var item = parametersCallback.item,
                        event = parametersCallback.event,
                        settings = parametersCallback.settings,
                        data = parametersCallback.data;

                    var confirmacao = item.data('confirmacaoRemocao') || false;

                    if (!confirmacao) {
                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Deseja realmente remover este questão selecionada?',
                            buttons: "[Cancelar][Ok]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Ok") {
                                $(item).data('confirmacaoRemocao', true);
                                $(event.currentTarget).click();
                            }
                        });

                        return true;
                    }

                    return false;
                }

            });

            if (priv.options.cadernosSelecionadas) {
                $.each(priv.options.cadernosSelecionadas, function (i, item) {
                    $('#cadernos').componente(['add', item]);
                });
            } else {
                $("#periodo").trigger("change");
            }
            if (priv.options.turmasSelecionadas) {
                $("#turmas").select2('val', priv.options.turmasSelecionadas).prop('disabled', true);
            }

        };

        priv.setValidations = function () {
            var submitHandler = function (form) {
                var error = 0;
                var componente = $("#cadernos").componente();
                var cadernos = componente.items();
                var removeErro = function (elemento) {
                    if (elemento.find('>.component-item').length > 0) {
                        elemento.find('>.component-item').removeClass('bg-danger');
                        elemento.find('>.component-item .form-group .help-block').remove();
                    } else {
                        elemento.removeClass('bg-danger');
                    }
                };

                var addErro = function (elemento) {
                    var $card = null;

                    if (elemento.find('>.component-item').length > 0) {
                        $card = elemento.find('>.component-item');
                        $card.addClass('bg-danger');
                    } else {
                        $card = elemento;
                        $card.addClass('bg-danger');
                    }
                };

                var errors = [];

                if (cadernos.length < 1) {
                    errors.push('Inclua ao menos um caderno em sua avaliação!');
                }
                var totalDisc = 0;
                $.each(cadernos, function (i, item) {
                    var $item = $(item);
                    var id = $(item).data('componente.id');
                    removeErro($item);

                    var campoNome = $item.find('[name="' + componente.makeFieldName({id: id, name: 'nome'}) + '"]');
                    var campoDataAplicacao = $item.find('[name="' + componente.makeFieldName({
                            id: id,
                            name: 'data-aplicacao'
                        }) + '"]');
                    var campoDisciplinas = $item.find('[name="' + componente.makeFieldName({
                            id: id,
                            name: 'disciplinas'
                        }) + '"]');
                    var campoGabarito = $item.find('[name="' + componente.makeFieldName({
                            id: id,
                            name: 'gabarito'
                        }) + '"]');

                    var nomeEmpty = (campoNome.val() == "");
                    var dataEmpty = (campoDataAplicacao.val() == "");
                    var disciplinasEmpty = (campoDisciplinas.val() == "");
                    var gabaritoEmpty = (campoGabarito.val() == "");
                    var discArray = campoDisciplinas.val().split(",");
                    totalDisc += discArray.length;


                    if (nomeEmpty || dataEmpty || disciplinasEmpty || gabaritoEmpty) {
                        errors.push("Por favor preencha os dados do caderno " + (i + 1) + "!");
                        addErro($item, true);
                    }
                });

                if(priv.options.avaliacoesCadastradas){
                    $.each(priv.options.avaliacoesCadastradas, function (i, item) {
                        if(item.integavaliacaoPeriodo == $("#periodo").val() && item.integavaliacaoTurno == $("#turno").val()){
                            errors.push('As provas para este período e turno já foram cadastradas! Para alterar algum dado, por favor edite a prova já cadastrada!');
                            return false;
                        }
                    });
                }

                if(totalDisc < priv.options.arrDisciplinas.length){
                    errors.push("Insira todas disciplinas no caderno para que se possa salvar o caderno!");
                }
                if (Object.keys(errors).length > 0) {
                    if (errors) {
                        addErro($("#cadernos"));

                        $.smallBox({
                            title: "Não foi possível salvar a avaliação",
                            content: "<i>" + errors.join('<br>') + "</i>",
                            color: "#C46A69",
                            icon: "fa fa-times",
                            timeout: 5000
                        });

                        return false;
                    }

                    return false;
                }

                removeErro($("#cadernos"));
                $("#turmas").prop('disabled', false);
                form.submit();
            };

            $("#avaliacao-form").validate({
                submitHandler: submitHandler,
                rules: {
                    periodo: 'required',
                    turmas: 'required',
                    turno: 'required'
                },
                messages: {
                    periodo: 'Por favor, selecione o periodo ao qual deseja vincular a avaliacão',
                    turmas: 'Por favor, selecione a turma ao qual deseja criar a avaliação',
                    turno: 'Por favor, informe pelo menos uma curta descrição sobre a avaliação'
                },
                ignore: '.ignore'
            });
        };


        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.iniciaFormulario();
            priv.setValidations();
        };
    };

    $.integradoraAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.acadperiodo-integradora-caderno.add");

        if (!obj) {
            obj = new IntegradoraAdd();
            obj.run(params);
            $(window).data('universa.acadperiodo-integradora-caderno.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);
