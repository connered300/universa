(function ($, window, document) {
    'use strict';

    var AcadgeralAlunoAdd = function () {
        VersaShared.call(this);

        var __AcadgeralAlunoAdd = this;

        this.defaults = {
            url: {
                buscaCEP: '',
                pesquisaPF: '',
                buscaFormacao: '',
                gerenciadorDeArquivosThumb: '',
                gerenciadorDeArquivosDownload: '',
                cadastroOrigem: '',
                agenteEducacional: '',
                unidadeEstudo: '',
                campusCurso: '',
                pessoasFisica: ''
            },
            data: {
                selecaoTipo: [],
                pessoaPesquisa: null,
                pessoaJuridicaPesquisa: null,
                necessidadesEspeciais: [],
                arrEstados: [],
                origem: [],
                pesIdAgenciador: [],
                unidade: [],
                arrAlunocursoSituacao: [],
                formaIngressoPadrao: null,
                arrCamposPersonalizados: [],
                arrCamposPersonalizadosValidate: []
            },
            value: {
                arq: null,
                responsaveis: null,
                alunoCurso: null,
                formacoes: null,
                documentoGrupo: null,
                pessoaNecessidades: null,
                editaAlunoIndicacao: null,
                editaObservacaoDocumentoAluno: null,
                editaAgente: null
            },
            datatables: {
                responsaveis: null,
                formacoes: null,
                alunoCurso: null
            },
            wizardElement: '#acadgeral-aluno-wizard',
            formElement: '#acadgeral-aluno-form',
            validator: null
        };

        this.steps = {
            dadosPessoais: {
                init: function () {
                    var $form = $(__AcadgeralAlunoAdd.options.formElement);
                    var vfile = $('#arq').vfile({
                        hideText: true,
                        thumbBaseUrl: __AcadgeralAlunoAdd.options.url.gerenciadorDeArquivosThumb,
                        downloadBaseUrl: __AcadgeralAlunoAdd.options.url.gerenciadorDeArquivosDownload
                    });

                    if (__AcadgeralAlunoAdd.options.value.arq) {
                        vfile.setSelection(__AcadgeralAlunoAdd.options.value.arq);
                    }

                    $form.find("[name=pesCpf]").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['999.999.999-99']
                    });
                    $form.find("[name=pesCnpj]").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['99.999.999/9999-99']
                    });
                    $form.find("[name=pesNacionalidade], [name=alunoEtnia], [name=pesSexo], [name=pesEstadoCivil]").select2({
                        language: 'pt-BR'
                    });
                    $form.find("[name=pesNacionalidade]").change(function () {
                        var pesNacionalidade = this.value;
                        $form.find(".campoPesCpf")
                            [pesNacionalidade == 'Estrangeiro' ? 'addClass' : 'removeClass']('hidden');
                        $form.find(".campoPesDocEstrangeiro")
                            [pesNacionalidade != 'Estrangeiro' ? 'addClass' : 'removeClass']('hidden');
                    }).trigger('change');

                    if ($form.find("[name=pesId]").val() != "") {
                        $form.find('.buscarDocumentoPessoa').parent().addClass('hide');
                    } else {
                        $form.find('[name=pesNome]')
                            .blur(function () {
                                $(this).removeClass('ui-autocomplete-loading');
                            })
                            .autocomplete({
                                source: function (request, response) {
                                    var $element = $(this.element);
                                    var previous_request = $element.data("jqXHR");

                                    if (previous_request) {
                                        previous_request.abort();
                                    }

                                    $element.data("jqXHR", $.ajax({
                                        url: __AcadgeralAlunoAdd.options.url.pesquisaPF,
                                        data: {query: request.term},
                                        type: 'POST',
                                        dataType: "json",
                                        success: function (data) {
                                            var transformed = $.map(data || [], function (el) {
                                                el.label = el.pesNome;

                                                return el;
                                            });

                                            response(transformed);
                                        }
                                    }));
                                },
                                minLength: 2,
                                select: function (event, ui) {
                                    var $element = $(this);
                                    __AcadgeralAlunoAdd.options.data.pessoaPesquisa = ui.item;
                                    __AcadgeralAlunoAdd.steps.dadosPessoais.selecaoPessoa($element.closest('form'));
                                }
                            });
                    }

                    $form.find(".buscarDocumentoPessoa").click(function () {
                        var pesNacionalidade = $form.find("[name=pesNacionalidade]").val();
                        var arrPesquisa = {};
                        var pesCpf = '';
                        var pesCpfNums = '';
                        var pesDocEstrangeiro = '';

                        if (pesNacionalidade != 'Estrangeiro') {
                            pesCpf = $form.find("[name=pesCpf]").val();
                            pesCpfNums = pesCpf.replace(/[^0-9]/g, '');

                            if (pesCpfNums.length < 11) {
                                __AcadgeralAlunoAdd.showNotificacaoWarning('CPF inválido!');

                                return;
                            }
                            arrPesquisa.pesCpf = pesCpf;
                        } else {
                            pesDocEstrangeiro = $form.find("[name=pesDocEstrangeiro]").val();
                            arrPesquisa.pesDocEstrangeiro = pesDocEstrangeiro;
                        }

                        __AcadgeralAlunoAdd.steps.dadosPessoais.pesquisarPesssoaPeloDocumento(arrPesquisa, $form);
                    });

                    __AcadgeralAlunoAdd.iniciarElementoDatePicker(
                        $form.find("[name=pesDataNascimento], [name=pesRgEmissao] ")
                    );

                    $form.find("[name=pessoaNecessidades]").select2({
                        language: 'pt-BR',
                        multiple: true,
                        data: function () {
                            return {results: __AcadgeralAlunoAdd.options.data.necessidadesEspeciais};
                        }
                    });
                    $form.find("[name=pessoaNecessidades]").select2(
                        'data',
                        __AcadgeralAlunoAdd.options.value.pessoaNecessidades || []
                    );
                    $form.find("[name=origem]").select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __AcadgeralAlunoAdd.options.url.cadastroOrigem,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.origem_nome, id: el.origem_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $form.find('[name=origem]').select2("data", __AcadgeralAlunoAdd.getOrigem());

                    $form.find("[name=pesIdAgenciador]").select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __AcadgeralAlunoAdd.options.url.agenteEducacional,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.pes_nome, id: el.pes_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $form.find('[name=pesIdAgenciador]').select2("data", __AcadgeralAlunoAdd.getPesIdAgenciador());

                    if (!__AcadgeralAlunoAdd.options.value.editaAgente && __AcadgeralAlunoAdd.getPesIdAgenciador()) {
                        $form.find('[name=pesIdAgenciador]').select2("disable");
                    }

                    $form.find("[name=unidade]").select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __AcadgeralAlunoAdd.options.url.unidadeEstudo,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.unidade_nome, id: el.unidade_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $form.find('[name=unidade]').select2("data", __AcadgeralAlunoAdd.getUnidade());

                    var pessoaIndicacao = $("#alunoPessoaIndicacao");

                    if (!__AcadgeralAlunoAdd.options.value.editaAlunoIndicacao && pessoaIndicacao.val()) {
                        pessoaIndicacao.prop("disabled", true);
                    }

                    if (Object.keys(__AcadgeralAlunoAdd.getCamposPersonalizados()).length > 0) {
                        var regras = __AcadgeralAlunoAdd.camposPersonalizados('dadosPessoais');

                        if (regras) {
                            __AcadgeralAlunoAdd.setCamposPersonalizadosValidate('dadosPessoais', regras);
                        }
                    }

                },
                selecaoPessoa: function ($form) {
                    var dados = __AcadgeralAlunoAdd.options.data.pessoaPesquisa || {};

                    $.SmartMessageBox({
                        title: "Confirme a operação:",
                        content: '' +
                        'Deseja realmente carregar os dados no formulário?' +
                        '<br>Nome:<b> ' + dados['pesNome'] + '</b>' +
                        '<br>Sexo:<b> ' + dados['pesSexo'] + '</b>' +
                        '<br>CPF:<b> ' + dados['pesCpf'] + '</b>' +
                        '<br>Data de nascimento:<b> ' + dados['pesDataNascimento'] + '</b>' +
                        '<br>Cidade/Estado:<b> ' + dados['endCidade'] + ' - ' + dados['endEstado'] + '</b>',
                        buttons: "[Não][Sim]"
                    }, function (ButtonPress) {
                        if (ButtonPress == "Sim") {
                            var dados = __AcadgeralAlunoAdd.options.data.pessoaPesquisa || {};
                            __AcadgeralAlunoAdd.steps.dadosPessoais.buscarDadosAlunoPelaPessoa(
                                dados['pesId'], $form
                            );
                        }

                        __AcadgeralAlunoAdd.options.data.pessoaPesquisa = null;
                    });
                },
                pesquisarPesssoaPeloDocumento: function (criterioBusca, $form) {
                    $form = $form || $(__AcadgeralAlunoAdd.options.formElement);
                    criterioBusca = criterioBusca || {};
                    __AcadgeralAlunoAdd.addOverlay($form, 'Aguarde. Pesquisando pessoa pelo documento...');

                    $.ajax({
                        url: __AcadgeralAlunoAdd.options.url.pesquisaPF,
                        dataType: 'json',
                        type: 'post',
                        data: criterioBusca,
                        success: function (data) {
                            __AcadgeralAlunoAdd.removeOverlay($form);

                            if (Object.keys(data).length == 0) {
                                __AcadgeralAlunoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                            } else {
                                __AcadgeralAlunoAdd.options.data.pessoaPesquisa = data[0];
                                __AcadgeralAlunoAdd.steps.dadosPessoais.selecaoPessoa($form);
                            }
                        },
                        erro: function () {
                            __AcadgeralAlunoAdd.showNotificacaoDanger('Erro desconhecido!');
                            __AcadgeralAlunoAdd.removeOverlay($form);
                        }
                    });
                },
                buscarDadosAlunoPelaPessoa: function (pesId, $form) {
                    $form = $form || $(__AcadgeralAlunoAdd.options.formElement);

                    __AcadgeralAlunoAdd.addOverlay($form, 'Aguarde. Buscando informações adicionais...');

                    $.ajax({
                        url: __AcadgeralAlunoAdd.options.url.pesquisaPF,
                        dataType: 'json',
                        type: 'post',
                        data: {pesId: pesId, dadosDeAluno: true},
                        success: function (data) {
                            __AcadgeralAlunoAdd.removeOverlay($form);

                            if (Object.keys(data).length == 0) {
                                __AcadgeralAlunoAdd.showNotificacaoInfo('Dados não encontrados!');
                            } else {
                                var dados = data;
                                $form[0].reset();
                                $form.deserialize(dados);
                                $form.find("[name=pesSexo],[name=pesEstadoCivil]").trigger('change');

                                __AcadgeralAlunoAdd.options.value.formacoes = dados['formacoes'] || [];
                                __AcadgeralAlunoAdd.options.value.responsaveis = dados['responsaveis'] || [];
                                __AcadgeralAlunoAdd.options.value.alunoCurso = dados['alunoCurso'] || [];
                                __AcadgeralAlunoAdd.options.value.documentoGrupo = dados['documentoGrupo'] || [];
                                __AcadgeralAlunoAdd.options.value.vestibular = dados['vestibular'] || [];
                                __AcadgeralAlunoAdd.options.value.pessoaNecessidades =
                                    dados['pessoaNecessidades'] || [];
                                __AcadgeralAlunoAdd.options.value.unidade = dados['unidade'] || null;
                                __AcadgeralAlunoAdd.options.value.pesIdAgenciador =
                                    dados['pesIdAgenciador'] || null;
                                __AcadgeralAlunoAdd.options.value.origem = dados['origem'] || null;

                                $("#alunoObservacaoDocumentos").val(dados['alunoObservacaoDocumentos'] || '');

                                var camposVestibular = [
                                    'inscricaoResultado', 'inscricaoNota', 'inscricaoId', 'edicaoDescricao'
                                ];

                                $.each(camposVestibular, function (i, campo) {
                                    $('.' + campo).html(
                                        __AcadgeralAlunoAdd.options.value.vestibular[campo] || '-'
                                    );
                                });

                                var camposEstaticos = [
                                    'alunoDataCadastro', 'usuarioCadastroLogin', 'usuarioAlteracaoLogin',
                                    'alunoDataAlteracao'
                                ];

                                $.each(camposEstaticos, function (i, campo) {
                                    $('.' + campo).html(dados[campo] || '-');
                                });

                                var vfile = $('#arq').vfile();

                                if (dados['arq']) {
                                    vfile.setSelection(dados['arq']);
                                } else {
                                    vfile.doRemove();
                                }

                                $.each(__AcadgeralAlunoAdd.options.value.documentoGrupo,
                                    function (docIndex, docItem) {
                                        var nomeCampo = '[name="documentoGrupo[' + docItem['docgrupoId'] + ']"]';
                                        nomeCampo += '[value="' + docItem['docpessoaStatus'] + '"]';
                                        $(nomeCampo).prop('checked', true);
                                    });

                                __AcadgeralAlunoAdd.steps.alunoCurso.redrawDatatableAlunoCurso();
                                __AcadgeralAlunoAdd.steps.formacoes.redrawDatatableFormacoes();
                                __AcadgeralAlunoAdd.steps.responsaveis.redrawDatatableResponsaveis();

                                $form.find("[name=pessoaNecessidades]").select2(
                                    'data',
                                    __AcadgeralAlunoAdd.options.value.pessoaNecessidades || []
                                );

                                $form.find("[name=origem]")
                                    .select2('data', __AcadgeralAlunoAdd.options.value.origem || null);
                                $form.find("[name=unidade]")
                                    .select2('data', __AcadgeralAlunoAdd.options.value.unidade || null);
                                $form.find("[name=pesIdAgenciador]")
                                    .select2('data', __AcadgeralAlunoAdd.options.value.pesIdAgenciador || null);

                                __AcadgeralAlunoAdd.validate();
                            }
                        },
                        erro: function () {
                            __AcadgeralAlunoAdd.showNotificacaoDanger('Erro desconhecido!');
                            __AcadgeralAlunoAdd.removeOverlay($form);
                        }
                    });
                },
                validate: function () {
                    var $form = $(__AcadgeralAlunoAdd.options.formElement);
                    __AcadgeralAlunoAdd.options.validator.settings.rules = {
                        pesNacionalidade: {required: true},
                        pesCpf: {
                            cpf: true,
                            required: function () {
                                var obrigatoriedade = $form.find('[name=pesNacionalidade]').val() != 'Estrangeiro';
                                var desabilitarObrigatoriedadeCpf = __AcadgeralAlunoAdd.options.value.desabilitarObrigatoriedadeCpf || 0;

                                return obrigatoriedade && !desabilitarObrigatoriedadeCpf;
                            }
                        },
                        pesDocEstrangeiro: {
                            required: function () {
                                return $form.find('[name=pesNacionalidade]').val() == 'Estrangeiro';
                            }
                        },
                        pesNome: {required: true, maxlength: 255},
                        pesNaturalidade: {required: true},
                        pesNascUf: {required: true},
                        alunoEtnia: {required: true},
                        pesDataNascimento: {required: true, dateBR: true},
                        pesSexo: {required: true},
                        pesRg: {required: true},
                        pesEmissorRg: {maxlength: 10},
                        pesRgEmissao: {required: false, dateBR: true},
                        alunoPai: {required: false},
                        alunoMae: {required: false},
                        pesIdAgenciador: {required: true},
                        unidade: {required: true}
                    };
                    __AcadgeralAlunoAdd.options.validator.settings.messages = {
                        pesNacionalidade: {required: 'Campo obrigatório!'},
                        pesCpf: {
                            required: 'Campo obrigatório!',
                            cpf: 'CPF inválido!'
                        },
                        pesDocEstrangeiro: {required: 'Campo obrigatório!'},
                        pesNome: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 255!'},
                        pesNaturalidade: {required: 'Campo obrigatório!'},
                        pesNascUf: {required: 'Campo obrigatório!'},
                        alunoEtnia: {required: 'Campo obrigatório!'},
                        pesDataNascimento: {
                            required: 'Campo obrigatório!',
                            dateBR: 'Data inválida!'
                        },
                        pesSexo: {required: 'Campo obrigatório!'},
                        pesRg: {required: 'Campo obrigatório!'},
                        pesEmissorRg: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 10!'},
                        pesRgEmissao: {
                            required: 'Campo obrigatório!',
                            dateBR: 'Data inválida!'
                        },
                        alunoPai: {required: 'Campo obrigatório!'},
                        alunoMae: {required: 'Campo obrigatório!'},
                        pesIdAgenciador: {required: 'Campo obrigatório!'},
                        unidade: {required: 'Campo obrigatório!'}
                    };

                    __AcadgeralAlunoAdd.setarValidacoesCamposPersonalizados('dadosPessoais');

                    return !$(__AcadgeralAlunoAdd.options.formElement).valid();
                }
            },
            contatoEndereco: {
                init: function () {
                    $("[name=endCep]").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']});
                    $('[name=endNumero]').inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
                    $("[name=conContatoTelefone], [name=conContatoCelular]").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                    });
                    $(".buscarCep").click(function () {
                        var $element = $(this);
                        var endCep = $(this).closest('form').find("[name=endCep]").val();
                        var endCepNums = endCep.replace(/[^0-9]/g, '');

                        if (endCepNums.length < 8) {
                            __AcadgeralAlunoAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');

                            return;
                        }

                        $element.prop('disabled', true);

                        $.ajax({
                            url: __AcadgeralAlunoAdd.options.url.buscaCEP,
                            dataType: 'json',
                            type: 'post',
                            data: {cep: endCep},
                            success: function (data) {
                                $element.prop('disabled', false);

                                if (!data.dados) {
                                    __AcadgeralAlunoAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');
                                }

                                $element.closest('form').find("[name=endLogradouro]").val(
                                    ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                                );
                                $element.closest('form').find("[name=endCidade]").val(data.dados.cid_nome || '');
                                $element.closest('form').find("[name=endEstado]").val(data.dados.est_uf || '').trigger('change');
                                $element.closest('form').find("[name=endBairro]").val(data.dados.bai_nome || '');
                            },
                            erro: function () {
                                __AcadgeralAlunoAdd.showNotificacaoDanger('Falha ao buscar CEP!');
                                $element.prop('disabled', false);
                            }
                        });
                    });
                    $('[name=pesNaturalidade], [name=endCidade], [name=formacaoCidade] ')
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                $element.data("jqXHR", $.ajax({
                                    url: __AcadgeralAlunoAdd.options.url.buscaCEP,
                                    data: {cidade: request.term},
                                    type: 'POST',
                                    dataType: "json",
                                    success: function (data) {
                                        var transformed = $.map(data.dados || [], function (el) {
                                            el.label = el.cid_nome;

                                            return el;
                                        });

                                        response(transformed);
                                    }
                                }));
                            },
                            minLength: 2,
                            focus: function (event, ui) {
                                var $element = $(this);
                                $element.val(ui.item.cid_nome);

                                return false;
                            },
                            select: function (event, ui) {
                                var $element = $(this);
                                $element.val(ui.item.cid_nome);
                                $element.closest('form').find(
                                    $element.attr('id') == 'endCidade' ? "[name=endEstado]" : (
                                        $element.attr('id') == 'formacaoCidade' ?
                                            "[name=formacaoEstado]" : "[name=pesNascUf]"
                                    )
                                )
                                    .val(ui.item.est_uf).trigger('change');

                                return false;
                            }
                        });

                    $('[name=pesNascUf], [name=endEstado], [name=formacaoEstado]').select2({
                        language: 'pt-BR',
                        allowClear: true,
                        data: function () {
                            return {results: __AcadgeralAlunoAdd.options.data.arrEstados || []};
                        }
                    });

                    if (Object.keys(__AcadgeralAlunoAdd.getCamposPersonalizados()).length > 0) {
                        var regras = __AcadgeralAlunoAdd.camposPersonalizados('contatoEndereco');

                        if (regras) {
                            __AcadgeralAlunoAdd.setCamposPersonalizadosValidate('contatoEndereco', regras);
                        }
                    }
                },
                validate: function () {
                    var $form = $(__AcadgeralAlunoAdd.options.formElement);
                    __AcadgeralAlunoAdd.options.validator.settings.rules = {
                        endCep: {required: false},
                        endLogradouro: {required: true},
                        endNumero: {
                            required: function () {
                                return $form.find('[name=endComplemento]').val() == '';
                            }, number: true
                        },
                        endComplemento: {
                            required: function () {
                                return $form.find('[name=endNumero]').val() == '';
                            }
                        },
                        endBairro: {required: false},
                        endCidade: {required: true},
                        endEstado: {required: true},
                        conContatoTelefone: {telefoneValido: true, required: false},
                        conContatoCelular: {celular: true, required: true},
                        conContatoEmail: {required: true, email: true}
                    };
                    __AcadgeralAlunoAdd.options.validator.settings.messages = {
                        endCep: {required: 'Campo obrigatório!'},
                        endLogradouro: {required: 'Campo obrigatório!'},
                        endNumero: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        endComplemento: {required: 'Campo obrigatório!'},
                        endBairro: {required: 'Campo obrigatório!'},
                        endCidade: {required: 'Campo obrigatória!'},
                        endEstado: {required: 'Campo obrigatório!'},
                        conContatoTelefone: {required: 'Campo obrigatório!'},
                        conContatoCelular: {required: 'Campo obrigatório!'},
                        conContatoEmail: {required: 'Campo obrigatório!', email: 'Email inválido!'}
                    };

                    __AcadgeralAlunoAdd.setarValidacoesCamposPersonalizados('contatoEndereco');

                    return !$(__AcadgeralAlunoAdd.options.formElement).valid();
                }
            },
            documentoGrupo: {
                init: function () {
                    $('#alunocursoCarteira')
                        .inputmask({showMaskOnHover: false, mask: ['9{0,20}']});
                    $('#alunoSecaoEleitoral, #alunoZonaEleitoral')
                        .inputmask({showMaskOnHover: false, mask: ['9{0,9}']});

                    if (!__AcadgeralAlunoAdd.options.value.editaObservacaoDocumentoAluno) {
                        $('[name=alunoObservacaoDocumentos]').prop("disabled", true);
                    }

                    if (Object.keys(__AcadgeralAlunoAdd.getCamposPersonalizados()).length > 0) {
                        var regras = __AcadgeralAlunoAdd.camposPersonalizados('documentos');

                        if (regras) {
                            __AcadgeralAlunoAdd.setCamposPersonalizadosValidate('documentos', regras);
                        }
                    }
                },
                validate: function () {
                    __AcadgeralAlunoAdd.options.validator.settings.rules = {
                        alunocursoCarteira: {number: true},
                        alunoSecaoEleitoral: {number: true},
                        alunoZonaEleitoral: {number: true}
                    };
                    __AcadgeralAlunoAdd.options.validator.settings.messages = {
                        alunocursoCarteira: {number: 'Número inválido!'},
                        alunoSecaoEleitoral: {number: 'Número inválido!'},
                        alunoZonaEleitoral: {number: 'Número inválido!'}
                    };

                    __AcadgeralAlunoAdd.setarValidacoesCamposPersonalizados('documentos');

                    return !$(__AcadgeralAlunoAdd.options.formElement).valid();
                }
            },
            responsaveis: {
                iniciarCamposFormulario: function () {
                    var $pessoaForm = $('#pessoa-form');
                    var $responsavelAlunoForm = $('#responsavel-aluno-form');
                    var $pesNomeResp = $('#pesNomeResp');

                    $("[name=respNivel],[name=respVinculo]").select2({
                        language: 'pt-BR', allowClear: true,
                        minimumInputLength: -1
                    });

                    $pesNomeResp.select2({
                        language: 'pt-BR',
                        allowClear: true,
                        minimumInputLength: 1,
                        ajax: {
                            url: __AcadgeralAlunoAdd.options.url.pessoaFisica,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {
                                    query: query,
                                    pesquisaPFPJ: true
                                };
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    el.id = el.pes_id;
                                    el.text = el.pes_nome;

                                    return el;
                                });

                                return {results: transformed};
                            }
                        },
                        createSearchChoice: function (term) {
                            return {
                                id: term,
                                pesId: '',
                                pesNome: term,
                                text: term + ' (Criar novo registro)'
                            };
                        }
                    });

                    $pesNomeResp.change(function () {
                        var data = $(this).select2('data') || [];

                        if (Object.keys(data).length > 0) {
                            $(this).next('.editarPessoa').removeClass('hidden');

                            if (!$.isNumeric(data['id']) && data['pesNome']) {
                                $(this).next('.editarPessoa').click();
                            } else {
                                var pesDoc = '';

                                if (data['pesNacionalidade'] == 'Brasileiro') {
                                    pesDoc = 'B' + (data['pesCpf'] || data['pesCnpj']);
                                } else {
                                    pesDoc = 'E' + data['pesDocEstrangeiro'];
                                }

                                data['pesDoc'] = pesDoc;

                                $responsavelAlunoForm.find('[name=pesDoc]').val(data['pesDoc'] || '');
                                $responsavelAlunoForm.find('[name=pesId]').val(data['pesId'] || '');
                            }
                        } else {
                            $(this).next('.editarPessoa').addClass('hidden');
                        }
                    }).trigger('change');

                    $('.editarPessoa').click(function (e) {
                        var $elemento = $(this).parent().find('>input');
                        var data = $elemento.select2('data') || [];
                        $('#pessoa-editar').modal('show');

                        var pessoaAdd = $.pessoaAdd();
                        pessoaAdd.setCallback('aposSalvar', function (dados) {
                            var text = [];

                            if (dados['pesCnpj']) {
                                text.push(dados['pesCnpj'])
                            }
                            if (dados['pesCpf']) {
                                text.push(dados['pesCpf'])
                            }
                            if (dados['pesNome']) {
                                text.push(dados['pesNome'])
                            }

                            var pesDoc = '';

                            if (dados['pesNacionalidade'] == 'Brasileiro') {
                                pesDoc = 'B' + (dados['pesCpf'] || dados['pesCnpj']);
                            } else {
                                pesDoc = 'E' + dados['pesDocEstrangeiro'];
                            }

                            dados['pesDoc'] = pesDoc;
                            dados['text'] = text.join(' - ');
                            dados['id'] = dados['pesId'];

                            $elemento.select2('data', dados);
                            $responsavelAlunoForm.find('input[name=pesDoc]').val(data['pesDoc'] || '');
                            $responsavelAlunoForm.find('input[name=pesId]').val(data['pesId'] || '');

                            $('#pessoa-editar').modal('hide');
                        });

                        pessoaAdd.buscarDadosPessoa($elemento.val());

                        $pessoaForm.find('input[name=pesNome]').val(data['pesNome'] || '');

                        e.preventDefault();
                    });
                },
                init: function () {
                    __AcadgeralAlunoAdd.steps.responsaveis.iniciarCamposFormulario();
                    var colNum = -1;
                    var $dataTableResponsaveis = $('#dataTableResponsaveis');

                    __AcadgeralAlunoAdd.options.datatables.responsaveis = $dataTableResponsaveis.dataTable({
                        processing: true,
                        columnDefs: [
                            {name: "respNivel", targets: ++colNum, data: "respNivel"},
                            {name: "pesNome", targets: ++colNum, data: "pesNome"},
                            {name: "respVinculo", targets: ++colNum, data: "respVinculo"},
                            {name: "respAcoes", targets: ++colNum, data: 'respAcoes'}
                        ],
                        "dom": "<'dt-toolbar'<'col-xs-3'B><'col-xs-6 responsavel-aviso'><'col-xs-3 no-padding'>r>t",
                        "autoWidth": true,
                        oLanguage: {
                            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                            sLengthMenu: "Mostrar _MENU_ registros por página",
                            sZeroRecords: "Nenhum registro encontrado",
                            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                            sInfoFiltered: "(filtrado de _MAX_ registros)",
                            sSearch: "Procurar: ",
                            oPaginate: {
                                sFirst: "Início",
                                sPrevious: "Anterior",
                                sNext: "Próximo",
                                sLast: "Último"
                            }
                        },
                        errMode: 'none',
                        order: [[1, 'desc']]
                    });

                    $('#btn-add-responsavel').click(function () {
                        __AcadgeralAlunoAdd.steps.responsaveis.showModalResponsavel();
                    });

                    $dataTableResponsaveis.on('click', '.btn-item-edit', function () {
                        var $pai = $(this).closest('tr');
                        var data = __AcadgeralAlunoAdd.options.datatables.responsaveis.fnGetData($pai);
                        data['GUID'] = data['GUID'] || __AcadgeralAlunoAdd.createGUID();
                        __AcadgeralAlunoAdd.steps.responsaveis.showModalResponsavel(data);
                    });

                    $('#btn-salvar-responsavel').click(function () {
                        var $form = $('#responsavel-aluno-form');
                        var $formAluno = $(__AcadgeralAlunoAdd.options.formElement);
                        var validacao = $form.validate({ignore: '.ignore'});

                        validacao.settings.rules = {
                            pesNomeResp: {required: true},
                            respNivel: {required: true},
                            respVinculo: {required: true}
                        };
                        validacao.settings.messages = {
                            pesNomeResp: {required: 'Campo obrigatório!'},
                            respNivel: {required: 'Campo obrigatório!'},
                            respVinculo: {required: 'Campo obrigatório!'}
                        };

                        if (!$form.valid()) {
                            return false;
                        }

                        var arrDados = $form.serializeJSON();

                        arrDados['GUID'] = arrDados['GUID'] || __AcadgeralAlunoAdd.createGUID();

                        var pesDoc = '';

                        var arrPesResp = $('#pesNomeResp').select2('data');
                        arrDados['pesNome'] = (arrDados['pesNome'] || arrPesResp['pesNome']);
                        arrDados['pesId'] = (arrDados['pesId'] || arrPesResp['pesId']);
                        arrDados['pesCnpj'] = (arrDados['pesCnpj'] || arrPesResp['pesCnpj']);
                        arrDados['pesCpf'] = (arrDados['pesCpf'] || arrPesResp['pesCpf']);
                        arrDados['pesNome'] = (arrDados['pesNome'] || arrPesResp['pesNome']);
                        arrDados['pesNacionalidade'] = (arrDados['pesNacionalidade'] || arrPesResp['pesNacionalidade']);
                        arrDados['pesDocEstrangeiro'] = (
                            arrDados['pesDocEstrangeiro'] || arrPesResp['pesDocEstrangeiro']
                        );

                        if (arrPesResp['pesNacionalidade'] == 'Brasileiro') {
                            pesDoc = 'B' + (arrPesResp['pesCpf'] || arrPesResp['pesCnpj']);
                        } else {
                            pesDoc = 'E' + arrPesResp['pesDocEstrangeiro'];
                        }

                        arrPesResp['pesDoc'] = pesDoc;

                        if ($formAluno.find('[name=pesNacionalidade]').val() == 'Brasileiro') {
                            pesDoc = 'B' + $formAluno.find('[name=pesCpf]').val();
                        } else {
                            pesDoc = 'E' + $formAluno.find('[name=pesDocEstrangeiro]').val();
                        }

                        if (pesDoc == arrDados['pesDoc']) {
                            __AcadgeralAlunoAdd.showNotificacaoDanger(
                                'Caso o aluno seja o seu próprio responsável não é necessário cadastrá-lo!',
                                'Ação inválida!'
                            );
                            return;
                        }

                        $('#modal-responsavel').modal('hide');
                        __AcadgeralAlunoAdd.steps.responsaveis.editarResponsavelItem(arrDados);
                    });

                    $dataTableResponsaveis.on('click', '.item-remove', function (e) {
                        var $pai = $(this).closest('tr');
                        var responsavelData = __AcadgeralAlunoAdd.options.datatables.responsaveis.fnGetData($pai);
                        responsavelData['GUID'] = responsavelData['GUID'] || __AcadgeralAlunoAdd.createGUID();

                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Deseja realmente remover este responsável?',
                            buttons: "[Cancelar][Ok]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Ok") {
                                __AcadgeralAlunoAdd.options.value.responsaveis = (
                                    $.grep(
                                        __AcadgeralAlunoAdd.options.value.responsaveis || [],
                                        function (item) {
                                            item['GUID'] = item['GUID'] || __AcadgeralAlunoAdd.createGUID();
                                            return (item['GUID'] != responsavelData['GUID']);
                                        }
                                    )
                                );

                                __AcadgeralAlunoAdd.steps.responsaveis.redrawDatatableResponsaveis();
                            }
                        });

                        e.preventDefault();
                    });

                    if (Object.keys(__AcadgeralAlunoAdd.options.data.arrCamposPersonalizados).length > 0) {
                        var regras = __AcadgeralAlunoAdd.camposPersonalizados('responsavel');

                        if (regras) {
                            __AcadgeralAlunoAdd.setCamposPersonalizadosValidate('responsavel', regras);
                        }
                    }

                    __AcadgeralAlunoAdd.steps.responsaveis.redrawDatatableResponsaveis();
                },
                validate: function () {
                    __AcadgeralAlunoAdd.options.validator.settings.rules = {};
                    __AcadgeralAlunoAdd.options.validator.settings.messages = {};

                    __AcadgeralAlunoAdd.setarValidacoesCamposPersonalizados('responsavel');

                    return !$(__AcadgeralAlunoAdd.options.formElement).valid();
                },
                showModalResponsavel: function (data) {
                    data = data || {};
                    var $modal = $('#modal-responsavel');
                    var actionTitle = 'Cadastrar';
                    var $form = $('#responsavel-aluno-form');
                    var text = [];
                    data['GUID'] = data['GUID'] || __AcadgeralAlunoAdd.createGUID();

                    if (Object.keys(data).length > 0) {
                        actionTitle = 'Editar';
                    }

                    $modal.find('.modal-title').html(actionTitle + ' responsável');

                    $form[0].reset();
                    $form.deserialize(data);

                    data['pesNomeResp'] = data['pesNomeResp'] || null;

                    if (data['pesId']) {
                        if (data['pesCnpj']) {
                            text.push(data['pesCnpj'])
                        }
                        if (data['pesCpf']) {
                            text.push(data['pesCpf'])
                        }
                        if (data['pesNome']) {
                            text.push(data['pesNome'])
                        }

                        var pesDoc = '';

                        if (data['pesNacionalidade'] == 'Brasileiro') {
                            pesDoc = 'B' + (data['pesCpf'] || data['pesCnpj']);
                        } else {
                            pesDoc = 'E' + data['pesDocEstrangeiro'];
                        }

                        data['pesDoc'] = pesDoc;

                        data['pesNomeResp'] = {id: data['pesId'], text: text.join(' - ')};
                        data['pesNomeResp'] = $.extend([], data, data['pesNomeResp']);
                    }

                    $("[name=respNivel]").select2('val', data['respNivel'] || '').trigger('change');
                    $("[name=respVinculo]").select2('val', data['respVinculo'] || '').trigger('change');
                    $('[name=pesNomeResp]').select2('data', data['pesNomeResp'] || '').trigger('change');

                    $modal.modal();
                },
                editarResponsavelItem: function (dados) {
                    var novo = true;
                    __AcadgeralAlunoAdd.options.value.responsaveis =
                        __AcadgeralAlunoAdd.options.value.responsaveis || [];

                    for (var i in __AcadgeralAlunoAdd.options.value.responsaveis) {
                        var item = __AcadgeralAlunoAdd.options.value.responsaveis[i];

                        if (item['GUID'] == dados['GUID']) {
                            __AcadgeralAlunoAdd.options.value.responsaveis[i] = dados;
                            novo = false;
                            break;
                        }
                    }

                    if (novo) {
                        __AcadgeralAlunoAdd.options.value.responsaveis.push(dados);
                    }

                    __AcadgeralAlunoAdd.steps.responsaveis.redrawDatatableResponsaveis();
                },
                redrawDatatableResponsaveis: function () {
                    __AcadgeralAlunoAdd.options.datatables.responsaveis.fnClearTable();
                    var data = [];

                    $.each(__AcadgeralAlunoAdd.options.value.responsaveis || [], function (i, item) {
                        item['GUID'] = item['GUID'] || __AcadgeralAlunoAdd.createGUID();
                        item['pesDoc'] = item['pesDoc'] || '-';
                        item['respId'] = item['respId'] || '';
                        item['respAcoes'] = '';
                        item['respAcoes'] = '\
                <div class="btn-group btn-group-xs text-center">\
                    <a href="#" class="btn btn-primary btn-xs btn-item-edit" title="Editar">\
                        <i class="fa fa-pencil fa-inverse"></i>\
                    </a>\
                    <a href="#" class="btn btn-danger btn-xs item-remove" title="Remover responsavel" >\
                        <i class="fa fa-times fa-inverse"></i>\
                    </a>\
                </div>';

                        data.push(item);
                    });

                    if (data.length > 0) {
                        __AcadgeralAlunoAdd.options.datatables.responsaveis.fnAddData(data);
                    }

                    __AcadgeralAlunoAdd.options.datatables.responsaveis.fnDraw();
                }
            },
            alunoCurso: {
                iniciarCamposModalCursos: function () {
                    var $dataTableAlunocurso = $('#dataTableAlunocurso');
                    var $formModalCurso = $('#form-modal-curso');

                    $("#tiposelId").select2({
                        data: function () {
                            return {results: __AcadgeralAlunoAdd.options.data.selecaoTipo};
                        }
                    });

                    $("#cursocampusId").select2({
                        language: 'pt-BR',
                        allowClear: true,
                        ajax: {
                            url: __AcadgeralAlunoAdd.options.url.campusCurso,
                            dataType: 'json',
                            type: 'POST',
                            delay: 250,
                            data: function (query) {
                                return {
                                    query: query,
                                    naoIncluirCampusCurso: __AcadgeralAlunoAdd.steps.alunoCurso.getCampusCurso()
                                };
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.camp_nome + ' / ' + el.curso_nome, id: el.cursocampus_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $("#pesIdAgente").select2({
                        ajax: {
                            url: __AcadgeralAlunoAdd.options.url.agenteEducacional,
                            dataType: 'json',
                            data: function (query) {
                                return {
                                    query: query
                                };
                            },
                            results: function (data) {
                                var resultado = $.map(data, function (elements) {
                                    elements.id = elements.pes_id;
                                    elements.text = elements.pes_nome;

                                    return elements;
                                });

                                return {
                                    results: resultado
                                };
                            }
                        }
                    });

                    $formModalCurso.find("[name=origem]").select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __AcadgeralAlunoAdd.options.url.cadastroOrigem,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.origem_nome, id: el.origem_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    $('#form-modal-curso').validate({
                        submitHandler: __AcadgeralAlunoAdd.steps.alunoCurso.salvarCursoAluno,
                        rules: {
                            cursocampusId: {required: true},
                            pesIdAgente: {required: false},
                            tiposelId: {required: true}
                        },
                        messages: {
                            cursocampusId: {required: 'Campo obrigatório!'},
                            pesIdAgente: {required: 'Campo obrigatório!'},
                            tiposelId: {required: 'Campo obrigatório!'}
                        },
                        ignore: '.ignore'
                    });

                    $dataTableAlunocurso.on('click', '.btn-item-edit', function () {
                        var $pai = $(this).closest('tr');
                        var data = __AcadgeralAlunoAdd.options.datatables.alunoCurso.fnGetData($pai);
                    });

                    $dataTableAlunocurso.on('click', '.item-remove', function () {
                        var $pai = $(this).closest('tr');
                        var data = __AcadgeralAlunoAdd.options.datatables.alunoCurso.fnGetData($pai);
                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: 'Deseja realmente remover este curso?',
                            buttons: "[Cancelar][Ok]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Ok") {
                                __AcadgeralAlunoAdd.options.value.alunoCurso = (
                                    $.grep(__AcadgeralAlunoAdd.options.value.alunoCurso || [], function (item) {
                                        item['GUID'] = item['GUID'] || __AcadgeralAlunoAdd.createGUID();
                                        return (data['GUID'] != item['GUID']);
                                    })
                                );
                                __AcadgeralAlunoAdd.steps.alunoCurso.redrawDatatableAlunoCurso();
                            }
                        });
                    });
                },
                exibirModalCadastroCursos: function (arrDados) {
                    arrDados = arrDados || {};
                    arrDados['GUID'] = arrDados['GUID'] || __AcadgeralAlunoAdd.createGUID();
                    arrDados['alunocursoId'] = arrDados['alunocursoId'] || '';
                    arrDados['cursocampusId'] = arrDados['cursocampusId'] || '';
                    arrDados['alunocursoSituacao'] = arrDados['alunocursoSituacao'] || 'Pendente';

                    var $formModalCurso = $('#form-modal-curso');
                    $formModalCurso.deserialize(arrDados);

                    $formModalCurso
                        .find('.alunocursoDataMatricula')
                        .html(__AcadgeralAlunoAdd.formatDate(arrDados['alunocursoDataMatricula'], 'dd/mm/yy'));
                    $formModalCurso
                        .find('.alunocursoDataAlteracao')
                        .html(__AcadgeralAlunoAdd.formatDateTime(arrDados['alunocursoDataAlteracao'], 'dd/mm/yy') || '-');
                    $formModalCurso
                        .find('.alunocursoDataCadastro')
                        .html(__AcadgeralAlunoAdd.formatDateTime(arrDados['alunocursoDataCadastro'], 'dd/mm/yy') || '-');
                    $formModalCurso
                        .find('.alunocursoDataColacao')
                        .html(__AcadgeralAlunoAdd.formatDateTime(arrDados['alunocursoDataColacao'], 'dd/mm/yy') || '-');
                    $formModalCurso
                        .find('.alunocursoDataExpedicaoDiploma')
                        .html(__AcadgeralAlunoAdd.formatDateTime(arrDados['alunocursoDataExpedicaoDiploma'], 'dd/mm/yy') || '-');
                    $formModalCurso
                        .find('.usuarioCadastroLogin')
                        .html(arrDados['usuarioCadastroLogin'] || '-');
                    $formModalCurso
                        .find('.usuarioAlteracaoLogin')
                        .html(arrDados['usuarioAlteracaoLogin'] || '-');
                    $formModalCurso
                        .find('.alunocursoSituacao')
                        .html(arrDados['alunocursoSituacao'] || '-');
                    $formModalCurso
                        .find('.alunocursoCarteira')
                        .html(arrDados['alunocursoCarteira'] || '-');
                    $formModalCurso
                        .find('.alunocursoMediador')
                        .html(arrDados['alunocursoMediador'] || '-');

                    var arrCampuscurso = null, arrAgente = null, arrOrigem = null;
                    var alunocursoEnade = ($formModalCurso.find('[name=enadeInformacoes]')).val('');
                    var $cursocampusId = $formModalCurso.find('[name=cursocampusId]');
                    var $origem = $formModalCurso.find('[name=origem]');

                    $cursocampusId.select2('disable');

                    if (arrDados['alunocursoEnade']) {
                        alunocursoEnade.val(arrDados['alunocursoEnade']);
                    }

                    if (!arrDados['alunocursoId'] || arrDados['alunocursoId'] == '-') {
                        $cursocampusId.select2('enable');
                    }

                    if (arrDados['cursocampusId']) {
                        arrCampuscurso = {id: arrDados['cursocampusId'] || '', text: arrDados['cursocampusNome'] || ''};
                    }

                    $("#pesIdAgente").select2("enable");

                    if (arrDados['pesIdAgente']) {
                        arrAgente = {id: arrDados['pesIdAgente'] || '', text: arrDados['pesNomeAgente'] || ''};
                        arrAgente = $.extend([], arrAgente, arrDados);

                        if (!__AcadgeralAlunoAdd.options.value.editaAgente) {
                            $("#pesIdAgente").select2("disable");
                        }
                    }

                    if (arrDados['origemId']) {
                        arrOrigem = {id: arrDados['origemId'] || '', text: arrDados['origemNome'] || ''};
                        arrOrigem = $.extend([], arrOrigem);
                    }

                    $("#tiposelId").select2(
                        'val', arrDados['tiposelId'] || __AcadgeralAlunoAdd.options.data.formaIngressoPadrao || null
                    );

                    $cursocampusId.select2('data', arrCampuscurso);
                    $origem.select2('data', arrOrigem);
                    $("#pesIdAgente").select2('data', arrAgente);

                    $('#curso-modal').modal('show');
                },
                init: function () {
                    var colNum = -1;
                    var $dataTableAlunocurso = $('#dataTableAlunocurso');

                    __AcadgeralAlunoAdd.steps.alunoCurso.iniciarCamposModalCursos();

                    $(document).on('click', '#add-curso', function () {
                        __AcadgeralAlunoAdd.steps.alunoCurso.exibirModalCadastroCursos();
                    });

                    __AcadgeralAlunoAdd.options.datatables.alunoCurso = $dataTableAlunocurso.dataTable({
                        processing: false,
                        serverSide: false,
                        data: [],
                        columns: [
                            {name: "alunocursoId", targets: ++colNum, data: "alunocursoId"},
                            {name: "cursocampusNome", targets: ++colNum, data: "cursocampusNome"},
                            {name: "alunocursoSituacao", targets: ++colNum, data: "alunocursoSituacao"},
                            {name: "pesNomeAgente", targets: ++colNum, data: "pesNomeAgente"},
                            {name: "alunocursoId", targets: ++colNum, data: "alunoCursoAcoes"}
                        ],
                        "columnDefs": [{
                            "targets": 0,
                            "createdCell": function (td, cellData, rowData, row, col) {
                                if (rowData['alunocursoSituacao'] == 'Cancelado') {
                                    $(td).parent().addClass('text-danger');
                                } else if (rowData['alunocursoSituacao'] == 'Transferencia') {
                                    $(td).parent().addClass('text-warning');
                                } else if (rowData['alunocursoSituacao'] == 'Trancado') {
                                    $(td).parent().addClass('text-success');
                                } else if (rowData['alunocursoSituacao'] == 'Pendente') {
                                    $(td).parent().addClass('text-info');
                                } else if (rowData['alunocursoSituacao'] == 'Indeferido') {
                                    $(td).parent().addClass('text-muted');
                                }
                            }
                        }],
                        "dom": "fr" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                        "autoWidth": true,
                        oLanguage: {
                            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                            sLengthMenu: "Mostrar _MENU_ registros por página",
                            sZeroRecords: "Nenhum registro encontrado",
                            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                            sInfoFiltered: "(filtrado de _MAX_ registros)",
                            sSearch: "Procurar: ",
                            oPaginate: {
                                sFirst: "Início",
                                sPrevious: "Anterior",
                                sNext: "Próximo",
                                sLast: "Último"
                            }
                        }
                    });

                    $dataTableAlunocurso.on('click', '.btn-item-edit', function () {
                        var $pai = $(this).closest('tr');
                        var data = __AcadgeralAlunoAdd.options.datatables.alunoCurso.fnGetData($pai);
                        data['GUID'] = data['GUID'] || __AcadgeralAlunoAdd.createGUID();
                        __AcadgeralAlunoAdd.steps.alunoCurso.exibirModalCadastroCursos(data);
                    });

                    if (Object.keys(__AcadgeralAlunoAdd.getCamposPersonalizados()).length > 0) {
                        var regras = __AcadgeralAlunoAdd.camposPersonalizados('cursos');
                        if (regras) {
                            __AcadgeralAlunoAdd.setCamposPersonalizadosValidate('cursos', regras);
                        }
                    }

                    __AcadgeralAlunoAdd.steps.alunoCurso.redrawDatatableAlunoCurso();
                },
                salvarCursoAluno: function () {
                    var $formModalCurso = $('#form-modal-curso');
                    var arrDados = $formModalCurso.serializeJSON();
                    var arrCampuscurso = $("#cursocampusId").select2('data') || {};
                    var arrAgente = $("#pesIdAgente").select2('data') || {};
                    var arrOrigem = $formModalCurso.find("[name=origem]").select2('data') || {};

                    arrDados['alunoCursoNovo'] = true;

                    arrDados['cursocampusId'] = arrCampuscurso['cursocampusId'] || arrCampuscurso['id'] || '';
                    arrDados['cursocampusNome'] = arrCampuscurso['cursocampusNome'] || arrCampuscurso['text'] || '';
                    arrDados['pesIdAgente'] = arrAgente['id'] || arrAgente['pesId'] || arrAgente['pesIdAgente'] || '';
                    arrDados['pesNomeAgente'] = arrAgente['pesNomeAgente'] || arrAgente['pes_nome_agente'] || arrAgente['pesNome'] || arrAgente['pes_nome'] || '';
                    arrDados['origemId'] = arrOrigem['origemId'] || arrOrigem['id'] || '';
                    arrDados['origemNome'] = arrOrigem['origemNome'] || arrOrigem['text'] || '';
                    arrDados['alunocursoSituacao'] = arrDados['alunocursoSituacao'] || 'Pendente';
                    arrDados['alunocursoMediador'] = arrDados['alunocursoMediador'] || 'captacao-interna';
                    arrDados['alunocursoDataMatricula'] = arrDados['alunocursoDataMatricula'] || '';
                    arrDados['alunocursoPessoaIndicacao'] = arrDados['alunocursoPessoaIndicacao'] || '';

                    $('#curso-modal').modal('hide');
                    $formModalCurso.trigger("reset");
                    __AcadgeralAlunoAdd.steps.alunoCurso.editarCursoItem(arrDados);
                },
                validate: function () {
                    __AcadgeralAlunoAdd.options.validator.settings.rules = {};
                    __AcadgeralAlunoAdd.options.validator.settings.messages = {};

                    __AcadgeralAlunoAdd.setarValidacoesCamposPersonalizados('cursos');

                    var errors = [];
                    var alunoCurso = __AcadgeralAlunoAdd.options.value.alunoCurso || [];
                    var camposVazios = [], retorno = false;

                    $.each(alunoCurso, function (i, item) {

                        if (item['cursocampusid'] == "") {
                            camposVazios.push("Curso");
                        }

                        if (item['tiposelId'] == "") {
                            camposVazios.push("Forma de ingresso");
                        }

                        if (camposVazios.length > 0) {
                            errors.push(
                                'Existe um registro de curso do aluno ' +
                                ' com campo(s) vazio(s). Por favor preencha o(s) campo(s): <b>' +
                                camposVazios.join(', ') + '</b>.'
                            );
                        }
                    });

                    retorno = !$(__AcadgeralAlunoAdd.options.formElement).valid();

                    if (Object.keys(errors).length > 0) {
                        if (errors) {
                            __AcadgeralAlunoAdd.showNotificacaoWarning(errors.join('<br>'));

                            return true;
                        }

                        return true;
                    }

                    return retorno;
                },

                getCampusCurso: function () {
                    var alunoCurso = __AcadgeralAlunoAdd.options.value.alunoCurso || [];
                    var arrCursosSelecionados = [];

                    $.each(alunoCurso, function (index, item) {
                        if (item['cursocampusId']) {
                            arrCursosSelecionados.push(item['cursocampusId']);
                        }
                    });

                    return arrCursosSelecionados;
                },
                editarCursoItem: function (dados) {
                    var novo = true;
                    __AcadgeralAlunoAdd.options.value.alunoCurso = __AcadgeralAlunoAdd.options.value.alunoCurso || [];
                    var arrAlunoCurso = [];

                    $.each(__AcadgeralAlunoAdd.options.value.alunoCurso, function (i, item) {
                        item['GUID'] = item['GUID'] || __AcadgeralAlunoAdd.createGUID();

                        if (dados['GUID'] == item['GUID']) {
                            dados = $.extend({}, item, dados);
                            arrAlunoCurso.push(dados);
                            novo = false;
                        } else {
                            arrAlunoCurso.push(item);
                        }
                    });

                    if (novo) {
                        arrAlunoCurso.push(dados);
                    }

                    __AcadgeralAlunoAdd.options.value.alunoCurso = arrAlunoCurso;

                    __AcadgeralAlunoAdd.steps.alunoCurso.redrawDatatableAlunoCurso();
                },
                redrawDatatableAlunoCurso: function () {
                    __AcadgeralAlunoAdd.options.datatables.alunoCurso.fnClearTable();

                    var data = [];

                    $.each(__AcadgeralAlunoAdd.options.value.alunoCurso || [], function (i, item) {
                        var btns = [
                            {class: 'btn-primary btn-xs btn-item-edit', icon: 'fa-pencil', title: 'Editar'}
                        ];

                        if (!item['alunocursoId']) {
                            btns.push({class: 'btn-danger btn-xs item-remove', icon: 'fa-times', title: 'Remover'});
                        }

                        item['alunocursoId'] = item['alunocursoId'] || '';
                        item['GUID'] = item['GUID'] || __AcadgeralAlunoAdd.createGUID();
                        item['cursocampusNome'] = item['cursocampusNome'] || "-";
                        item['alunocursoSituacao'] = item['alunocursoSituacao'] || "Pendente";
                        item['pesNomeAgente'] = item['pesNomeAgente'] || "-";
                        item['alunoCursoAcoes'] = __AcadgeralAlunoAdd.createBtnGroup(btns);

                        data.push(item);
                    });

                    if (data.length > 0) {
                        __AcadgeralAlunoAdd.options.datatables.alunoCurso.fnAddData(data);
                    }

                    __AcadgeralAlunoAdd.options.datatables.alunoCurso.fnDraw();
                }

            },
            formacoes: {
                init: function () {
                    var $formacaoAlunoForm = $('#formacao-aluno-form');
                    var $dataTableFormacoes = $('#dataTableFormacoes');
                    $("[name=formacaoAno], [name=formacaoDuracao]").inputmask({
                        showMaskOnHover: false, mask: ['9{0,9}']
                    });

                    $formacaoAlunoForm.find('[name=formacaoInstituicao], [name=formacaoCurso]')
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                var arrData = {};
                                var chave = '';

                                if ($element.attr('name') == 'formacaoInstituicao') {
                                    chave = 'instituicao';
                                } else if ($element.attr('name') == 'formacaoCurso') {
                                    chave = 'curso';
                                }

                                arrData[chave] = request.term;

                                $element.data("jqXHR",
                                    $.ajax({
                                        url: __AcadgeralAlunoAdd.options.url.buscaFormacao,
                                        data: arrData,
                                        type: 'POST',
                                        dataType: "json",
                                        success: function (data) {
                                            var transformed = $.map(data.dados || [], function (el) {
                                                return el[chave];
                                            });

                                            response(transformed);
                                        }
                                    }));
                            },
                            minLength: 0
                        });

                    $formacaoAlunoForm.find("[name=formacaoTipo]").select2({
                        language: 'pt-BR'
                    });
                    $formacaoAlunoForm.find("[name=formacaoTipoEnsino], [name=formacaoRegime], [name=formacaoConcluido]").select2({
                        allowClear: true,
                        language: 'pt-BR'
                    });

                    $formacaoAlunoForm.find("[name=formacaoTipo]").change(function () {
                        var $el = $(this);

                        if ($el.val() == 'Ensino Médio') {
                            $formacaoAlunoForm.find('[name=formacaoDuracao]').prop('disabled', true).val('3');
                            $formacaoAlunoForm.find('[name=formacaoRegime]').prop('disabled', true).val('Anual');
                            $formacaoAlunoForm.find('[name=formacaoCurso]').prop('disabled', true).val('Ensino Médio');
                        } else {
                            $formacaoAlunoForm.find('[name=formacaoDuracao]').prop('disabled', false);
                            $formacaoAlunoForm.find('[name=formacaoRegime]').prop('disabled', false);
                            $formacaoAlunoForm.find('[name=formacaoCurso]').prop('disabled', false);
                        }
                    }).trigger('change');

                    var colNum = -1;

                    __AcadgeralAlunoAdd.options.datatables.formacoes = $dataTableFormacoes.dataTable({
                        processing: true,
                        columnDefs: [
                            {name: "formacaoInstituicao", targets: ++colNum, data: "formacaoInstituicao"},
                            {name: "formacaoCurso", targets: ++colNum, data: "formacaoCurso"},
                            {name: "formacaoTipo", targets: ++colNum, data: "formacaoTipo"},
                            {name: "formacaoAno", targets: ++colNum, data: "formacaoAno"},
                            {name: "formacaoRegime", targets: ++colNum, data: "formacaoRegime"},
                            {name: "formacaoDuracao", targets: ++colNum, data: "formacaoDuracao"},
                            {name: "formacaoAcoes", targets: ++colNum, data: 'formacaoAcoes'}
                        ],
                        "dom": "<'dt-toolbar'<'col-xs-3 'B><'col-xs-6 formacao-aviso'><'col-xs-3 no-padding'f>r>t",
                        "autoWidth": true,
                        oLanguage: {
                            sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                            sLengthMenu: "Mostrar _MENU_ registros por página",
                            sZeroRecords: "Nenhum registro encontrado",
                            sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                            sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                            sInfoFiltered: "(filtrado de _MAX_ registros)",
                            sSearch: "Procurar: ",
                            oPaginate: {
                                sFirst: "Início",
                                sPrevious: "Anterior",
                                sNext: "Próximo",
                                sLast: "Último"
                            }
                        },
                        order: [[2, 'desc']]
                    });

                    $('#btn-add-formacao').click(function () {
                        __AcadgeralAlunoAdd.steps.formacoes.showModalFormacao();
                        var data = {};
                        __AcadgeralAlunoAdd.steps.formacoes.showModalFormacao(data);
                    });

                    $dataTableFormacoes.on('click', '.btn-item-edit', function () {
                        var $pai = $(this).closest('tr');
                        var data = __AcadgeralAlunoAdd.options.datatables.formacoes.fnGetData($pai);
                        data['GUID'] = data['GUID'] || __AcadgeralAlunoAdd.createGUID();
                        __AcadgeralAlunoAdd.steps.formacoes.showModalFormacao(data);
                    });

                    $('#btn-salvar-formacao').click(function () {
                        var validacao = $formacaoAlunoForm.validate({ignore: '.ignore'});
                        validacao.settings.rules = {
                            formacaoTipo: {required: true},
                            formacaoCurso: {required: true},
                            formacaoInstituicao: {required: true},
                            formacaoConcluido: {required: true},
                            formacaoAno: {required: false, number: true, min: 1900},
                            formacaoTipoEnsino: {required: false},
                            formacaoCidade: {required: false},
                            formacaoEstado: {required: false},
                            formacaoRegime: {required: false},
                            formacaoDuracao: {required: false, number: true}
                        };
                        validacao.settings.messages = {
                            formacaoTipoEnsino: {required: 'Campo obrigatório!'},
                            formacaoTipo: {required: 'Campo obrigatório!'},
                            formacaoInstituicao: {required: 'Campo obrigatório!'},
                            formacaoCidade: {required: 'Campo obrigatório!'},
                            formacaoEstado: {required: 'Campo obrigatório!'},
                            formacaoCurso: {required: 'Campo obrigatório!'},
                            formacaoAno: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                            formacaoRegime: {required: 'Campo obrigatório!'},
                            formacaoConcluido: {required: 'Campo obrigatório!'},
                            formacaoDuracao: {required: 'Campo obrigatório!', number: 'Número inválido!'}
                        };

                        if (!$formacaoAlunoForm.valid()) {
                            __AcadgeralAlunoAdd.showNotificacaoWarning('Existem um ou mais erros no formulário!');
                            return;
                        }

                        $('[name=formacaoDuracao]').prop('disabled', false);
                        $('[name=formacaoRegime]').prop('disabled', false);
                        $('[name=formacaoCurso]').prop('disabled', false);

                        var arrDados = $formacaoAlunoForm.serializeJSON();
                        arrDados['GUID'] = arrDados['GUID'] || __AcadgeralAlunoAdd.createGUID();

                        $('#modal-formacao').modal('hide');
                        $formacaoAlunoForm.trigger("reset");
                        __AcadgeralAlunoAdd.steps.formacoes.editarFormacaoItem(arrDados);
                    });

                    $dataTableFormacoes.on('click', '.item-remove', function (e) {
                        var $pai = $(this).closest('tr');
                        var formacaoData = __AcadgeralAlunoAdd.options.datatables.formacoes.fnGetData($pai);
                        formacaoData['GUID'] = formacaoData['GUID'] || __AcadgeralAlunoAdd.createGUID();

                        var mensagem = 'Deseja realmente remover esta formação?';

                        if ((formacaoData['numeroEquivalencias'] || 0) > 0) {
                            mensagem = '<b>Esta formação está vinculada a registros de histórico.<b><br>' + mensagem;
                        }

                        $.SmartMessageBox({
                            title: "Confirme a operação:",
                            content: mensagem,
                            buttons: "[Cancelar][Ok]"
                        }, function (ButtonPress) {
                            if (ButtonPress == "Ok") {
                                __AcadgeralAlunoAdd.options.value.formacoes = (
                                    $.grep(
                                        __AcadgeralAlunoAdd.options.value.formacoes || [],
                                        function (item) {
                                            item['GUID'] = item['GUID'] || __AcadgeralAlunoAdd.createGUID();
                                            return (item['GUID'] != formacaoData['GUID']);
                                        }
                                    )
                                );
                                __AcadgeralAlunoAdd.steps.formacoes.redrawDatatableFormacoes();
                            }
                        });

                        e.preventDefault();
                    });

                    if (Object.keys(__AcadgeralAlunoAdd.getCamposPersonalizados()).length > 0) {
                        var regras = __AcadgeralAlunoAdd.camposPersonalizados('formacao');

                        if (regras) {
                            __AcadgeralAlunoAdd.setCamposPersonalizadosValidate('formacao', regras);
                        }
                    }
                    __AcadgeralAlunoAdd.steps.formacoes.redrawDatatableFormacoes();
                },
                validate: function () {
                    var erros = [],
                        ensinoMedio = 0,
                        graduacao = 0,
                        formacoesValidacao = {},
                        arrTipoSel = [];

                    __AcadgeralAlunoAdd.options.validator.settings.rules = {};
                    __AcadgeralAlunoAdd.options.validator.settings.messages = {};

                    __AcadgeralAlunoAdd.setarValidacoesCamposPersonalizados('formacao');

                    var alunoCurso = __AcadgeralAlunoAdd.options.value.alunoCurso || [];
                    var retorno = !$(__AcadgeralAlunoAdd.options.formElement).valid();

                    $.each(alunoCurso, function (i, item) {
                        var tiposelId = item['tiposelId'] || 0;
                        arrTipoSel[tiposelId] = arrTipoSel[tiposelId] || 0;
                        arrTipoSel[tiposelId] += 1;
                    });

                    $.each(__AcadgeralAlunoAdd.options.value.formacoes || [], function (i, formacao) {
                        if (formacao['formacaoTipo'] == 'Ensino Médio') {
                            ensinoMedio++;
                        }

                        if (formacao['formacaoTipo'] == 'Graduação') {
                            graduacao++;
                        }

                        var chave = formacao['formacaoInstituicao'].trim() + ' - ' + formacao['formacaoCurso'].trim();

                        formacoesValidacao[chave] = formacoesValidacao[chave] || 0;
                        formacoesValidacao[chave] += 1;
                    });

                    if (alunoCurso.length > 0) {
                        //Registro de formação no ensino médio é obrigatório para o censo educacional
                        /*if (ensinoMedio == 0) {
                         erros.push('Insira a formação do aluno no ensino médio!');
                         }

                         //Múltiplos registros de ensino médio
                         if (ensinoMedio > 1) {
                         erros.push('Insira somente o último registro de formação do aluno no ensino médio!');
                         }*/

                        //Transferência
                        if (graduacao == 0 && arrTipoSel[7] >= 0) {
                            erros.push('Alunos de transferência devem possuir ao menos uma formação do tipo graduação!');
                        }
                    }

                    for (var formacao in formacoesValidacao) {
                        if (formacoesValidacao[formacao] > 1) {
                            erros.push(
                                formacao + '<br>' + 'Registrado ' + formacoesValidacao[formacao] +
                                ' vezes.<br> Remova os itens duplicados.'
                            );
                        }
                    }

                    if (erros.length > 0) {
                        $.each(erros, function (i, erro) {
                            __AcadgeralAlunoAdd.showNotificacaoWarning(erro);
                        });

                        return true;
                    }

                    return retorno;
                },
                showModalFormacao: function (data) {
                    data = data || {};
                    var $modal = $('#modal-formacao');
                    var actionTitle = 'Cadastrar';
                    var $form = $('#formacao-aluno-form');

                    if (Object.keys(data).length > 0) {
                        actionTitle = 'Editar';
                    }

                    $modal.find('.modal-title').html(actionTitle + ' formação');
                    $form[0].reset();

                    data['formacaoId'] = data['formacaoId'] || '';
                    data['GUID'] = data['GUID'] || __AcadgeralAlunoAdd.createGUID();

                    $form.deserialize(data);

                    $("[name=formacaoTipoEnsino]")
                        .select2('val', data['formacaoTipoEnsino'] || '')
                        .trigger('change');
                    $("[name=formacaoTipo]").trigger('change');
                    $("[name=formacaoEstado]")
                        .select2('val', data['formacaoEstado'] || '')
                        .trigger('change');
                    $('[name=formacaoRegime]').trigger('change');
                    $('[name=formacaoConcluido]')
                        .select2('val', data['formacaoConcluido'] || '')
                        .trigger('change').trigger('change');

                    if ((data['numeroEquivalencias'] || 0) > 0) {
                        __AcadgeralAlunoAdd.showNotificacaoWarning(
                            'Esta formação está vinculada a registros de histórico.<br>' +
                            'A alteração refletirá nas mesmas.'
                        )
                    }

                    $modal.modal();
                },
                editarFormacaoItem: function (dados) {
                    var novo = true;
                    __AcadgeralAlunoAdd.options.value.formacoes = __AcadgeralAlunoAdd.options.value.formacoes || [];
                    var arrFormacoes = [];

                    $.each(__AcadgeralAlunoAdd.options.value.formacoes, function (i, item) {
                        item['GUID'] = item['GUID'] || __AcadgeralAlunoAdd.createGUID();

                        if (dados['GUID'] == item['GUID']) {
                            arrFormacoes.push(dados);
                            novo = false;
                        } else {
                            arrFormacoes.push(item);
                        }
                    });

                    if (novo) {
                        arrFormacoes.push(dados);
                    }
                    __AcadgeralAlunoAdd.options.value.formacoes = arrFormacoes;

                    __AcadgeralAlunoAdd.steps.formacoes.redrawDatatableFormacoes();
                },
                redrawDatatableFormacoes: function () {
                    __AcadgeralAlunoAdd.options.datatables.formacoes.fnClearTable();
                    var data = [];
                    var btnRemove = '\
                    <a href="#" class="btn btn-danger btn-xs item-remove" title="Remover formacao" >\
                        <i class="fa fa-times fa-inverse"></i>\
                    </a>';

                    $.each(__AcadgeralAlunoAdd.options.value.formacoes || [], function (i, item) {
                        item['formacaoId'] = item['formacaoId'] || '';
                        item['GUID'] = item['GUID'] || __AcadgeralAlunoAdd.createGUID();
                        item['formacaoAcoes'] = '';
                        item['formacaoAcoes'] = '\
                <div class="btn-group btn-group-xs text-center">\
                    <a href="#" class="btn btn-primary btn-xs btn-item-edit" title="Editar">\
                        <i class="fa fa-pencil fa-inverse"></i>\
                    </a>\
                    ' + ((item['numeroEquivalencias'] || 0) > 0 ? '' : btnRemove) + '\
                </div>';

                        data.push(item);
                    });

                    if (data.length > 0) {
                        __AcadgeralAlunoAdd.options.datatables.formacoes.fnAddData(data);
                    }

                    __AcadgeralAlunoAdd.options.datatables.formacoes.fnDraw();
                }
            }
        };

        this.setValidations = function () {
            $(document).on("change", ".form-control:hidden", function () {
                $(__AcadgeralAlunoAdd.options.formElement).valid();
            });

            $(__AcadgeralAlunoAdd.options.formElement).submit(function (e) {
                var ok = __AcadgeralAlunoAdd.validate();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                $('[name=alunoObservacaoDocumentos]').prop("disabled", false);
                $('#responsaveis').val(JSON.stringify(__AcadgeralAlunoAdd.options.value.responsaveis));
                $('#formacoes').val(JSON.stringify(__AcadgeralAlunoAdd.options.value.formacoes));
                $('#alunoCurso').val(JSON.stringify(__AcadgeralAlunoAdd.options.value.alunoCurso));
            });
        };

        this.setOrigem = function (origem) {
            this.options.value.origem = origem || null;
        };

        this.getOrigem = function () {
            return this.options.value.origem || null;
        };

        this.setPesIdAgenciador = function (pesIdAgenciador) {
            this.options.value.pesIdAgenciador = pesIdAgenciador || null;
        };

        this.getPesIdAgenciador = function () {
            return this.options.value.pesIdAgenciador || null;
        };

        this.setUnidade = function (unidade) {
            this.options.value.unidade = unidade || null;
        };

        this.getUnidade = function () {
            return this.options.value.unidade || null;
        };

        this.setarValidacoesCamposPersonalizados = function (key) {
            key = key || false;

            if (!key) {
                return false;
            }

            var regrasAdicionais = __AcadgeralAlunoAdd.getCamposPersonalizadosValidate(key);

            $.each(regrasAdicionais, function (index, value) {
                var campoNome = value['campoNome'] || '';

                if (campoNome) {
                    __AcadgeralAlunoAdd.options.validator.settings.rules[campoNome] = value['rules'];
                    __AcadgeralAlunoAdd.options.validator.settings.messages[campoNome] = value['msg'];
                }
            });
        };

        this.getCamposPersonalizadosValidate = function (key) {
            key = key || false;
            var arrData = this.options.data.arrCamposPersonalizadosValidate || [];

            if (key) {
                return arrData[key] || [];
            }

            return arrData;
        };

        this.setCamposPersonalizadosValidate = function (key, rules) {
            key = key || false;
            rules = rules || [];

            if (!key) {
                return false;
            }

            this.options.data.arrCamposPersonalizadosValidate[key] = rules;

            return true;
        };

        this.getCamposPersonalizados = function (key) {
            key = key || false;
            var arrData = this.options.data.arrCamposPersonalizados || [];

            if (key) {
                return arrData[key] || [];
            }

            return arrData;
        };

        this.setCamposPersonalizados = function (key, rules) {
            key = key || false;
            rules = rules || [];

            if (!key) {
                return false;
            }

            this.options.data.arrCamposPersonalizados[key] = rules;

            return true;
        };

        /*Tratamento para campos personalizados*/
        this.camposPersonalizados = function (posicao) {

            var arrCampos = __AcadgeralAlunoAdd.getCamposPersonalizados(),
                arrRetorno = [],
                regras = [],
                msg = [],
                tamanho = '',
                idCampo = '',
                campoNome = '',
                nomeCampoFormatado = null;

            try {
                if (arrCampos[posicao]) {
                    $.each(arrCampos[posicao], function (index, value) {

                        regras = {};
                        msg = {};
                        nomeCampoFormatado = null;

                        idCampo = value.campo_id;
                        campoNome = "#campo-" + idCampo;

                        /*Adciona expressão regular*/
                        if (value.campo_expressao_regular) {
                            $(campoNome).inputmask('Regex', {regex: value.campo_expressao_regular});
                        }

                        /*Se for do tipo select, transforma em select2*/
                        if (value.type == 'select') {

                            if (value.campo_multiplo == "Sim") {
                                $(campoNome).select2({multiple: true});
                            } else {
                                $(campoNome).select2();
                            }
                        }

                        /*Adiciona valor padrao se ele também tiver um tipo*/
                        if (value.resposta && value.type) {
                            if (value.type == 'select') {
                                $(campoNome).select2("val", value.resposta).trigger("change");
                            } else {
                                $(campoNome).val(value.resposta);
                            }
                        }

                        /*Tratamento para o validate do formulario*/
                        if (value.campo_obrigatorio) {
                            regras['required'] = true;
                            msg['required'] = "Campo obrigatório!";
                            nomeCampoFormatado = "camposPersonalizados[" + idCampo + "]";
                        }
                        if (tamanho = parseInt(value.campo_tamanho_max)) {
                            regras['maxlength'] = parseInt(value.campo_tamanho_max);
                            msg['maxlength'] = "Tamanho máximo" + tamanho + "!";
                            nomeCampoFormatado = "camposPersonalizados[" + idCampo + "]";
                        }
                        if (tamanho = parseInt(value.campo_tamanho_min)) {
                            regras['minlength'] = parseInt(value.campo_tamanho_min);
                            msg['minlength'] = "Tamanho máximo" + tamanho + "!";
                            nomeCampoFormatado = "camposPersonalizados[" + idCampo + "]";
                        }

                        /*Caso tenha entrado em alguma if, tera valor para esse variavel*/
                        if (nomeCampoFormatado) {
                            /*Crio uma nova posição no array*/
                            arrRetorno.push({
                                'rules': regras,
                                'msg': msg,
                                'campoNome': nomeCampoFormatado,
                                'id': value.id
                            });
                        }
                    })
                }
            } catch (ex) {
                __AcadgeralAlunoAdd.showNotificacaoWarning(ex);
                setTimeout(function () {

                }, 500);
            }
            return arrRetorno;
        };


        this.run = function (opts) {
            this.setDefaults(opts);
            this.loadSteps();
            this.setValidations();
            this.wizard();
        };
    };

    $.acadgeralAlunoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.acadgeralAluno.add");

        if (!obj) {
            obj = new AcadgeralAlunoAdd();
            obj.run(params);
            $(window).data('universa.acadgeralAluno.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);