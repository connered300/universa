(function ($, window, document) {
    'use strict';

    var AcadgeralAlunoIndex = function () {
        VersaShared.call(this);

        var __AcadgeralAlunoIndex = this;

        this.defaults = {
            steps: [],
            validations: [],
            url: {
                index: '',
                search: '',
                alunocursoSituacao: '',
                motivoAlteracao: '',
                deferirAlunoCurso: '',
                integracaoManual: '',
                alterarSituacao: '',
                alterarDatas: '',
                edit: '',
                remove: '',
                tipoTitulo: '',
                alunoCurso: '',
                campusCurso: '',
                acessoPessoas: '',
                agentesEducacionais: '',
                orgUnidadeEstudo: '',
                fichaInscricao: '',
                documentoModelo: '',
                documentoEmitir: '',
                gerenciadorDeArquivosThumb: '',
                gerenciadorDeArquivosDownload: '',
                buscaCEP: '',
                alunoCursoInformacoes: '',
                areaConhecimento: '',
                comunicacaoModelo: '',
                envioComunicacao: '',
                outrasInformacoes: '',
                financeiroValores: '',
                integracaoNotaManual: ''
            },
            data: {
                tipoTitulo: null,
                alunoConfigPgtoCurso: null,
                arrAlunocursoSituacao: null,
                arrAlunocursoSituacaoAlteracao: null,
                arrCampusCurso: null,
                arrPeriodoLetivo: null,
                arrSituacoesCor: {
                    Cancelado: {
                        descricao: 'Cancelado',
                        cssClass: 'text-danger'
                    },
                    Transferencia: {
                        descricao: 'Transferência',
                        cssClass: 'text-warning'
                    },
                    Trancado: {
                        descricao: 'Trancado',
                        cssClass: 'text-muted'
                    },
                    Pendente: {
                        descricao: 'Pendente',
                        cssClass: 'text-info'
                    },
                    Indeferido: {
                        descricao: 'Indeferido',
                        cssClass: 'text-muted'
                    },
                    Concluido: {
                        descricao: 'Concluído',
                        cssClass: 'text-success'
                    },
                }
            },
            value: {
                tipoTitulo: null,
                agente: null,
                agenteEducacional: null,
                arrAluno: null,
                arrPermissoes: {
                    editarAluno: 0,
                    deferirCurso: 0,
                    alterarSituacao: 0,
                    alterarDatas: 0,
                    emitirDocumento: 0,
                    fichaInscricao: 0,
                    integracaoMaxiPago: 0
                },
                cartaoId: null,
                mensagemDataTable: 'Use os filtros para realizar a busca!',
                buscaComFiltro: 1
            },
            dataTableAcadgeralAluno: null
        };

        this.verificaPermissao = function (permissao) {
            permissao = permissao || '';

            if (!permissao) {
                return false;
            }

            var arrPermissoes = __AcadgeralAlunoIndex.options.value.arrPermissoes || {};

            return (arrPermissoes[permissao] || 0);
        };

        this.listagem = {
            iniciarFuncionalidades: function () {
                __AcadgeralAlunoIndex.listagem.iniciarFiltros();
                __AcadgeralAlunoIndex.listagem.iniciarDatatables();
                __AcadgeralAlunoIndex.listagem.iniciarAcoesDatatables();
            },
            iniciarFiltros: function () {
                var $formFiltro = $('#form-aluno-index');
                var $situacao = $formFiltro.find('[name=situacao]'),
                    $nivel = $formFiltro.find('[name=nivel]'),
                    $campusCurso = $formFiltro.find('[name=campusCurso]'),
                    $agentes = $formFiltro.find('[name=agentes]'),
                    $unidadeEstudo = $formFiltro.find('[name=unidadeEstudo]'),
                    $cidadeAluno = $formFiltro.find('[name=cidadeAluno]'),
                    $estadoAluno = $formFiltro.find('[name=estadoAluno]'),
                    $areaConhecimentoCurso = $formFiltro.find('[name=areaConhecimentoCurso]'),
                    $sexoAluno = $formFiltro.find('[name=sexoAluno]'),
                    $mesAniversario = $formFiltro.find('[name=mesAniversario]');

                $nivel.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    multiple: true,
                    data: __AcadgeralAlunoIndex.options.data.arrNivel || []
                }).change(function () {
                    $campusCurso.select2('val', '');
                });

                $areaConhecimentoCurso.select2({
                    language: 'pt-BR',
                    minimumInputLength: 1,
                    allowClear: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.areaConhecimento,
                        type: 'POST',
                        dataType: "json",
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.area_descricao, id: el.area_id};
                            });

                            return {results: transformed};
                        }
                    }
                }).change(function () {
                    $campusCurso.select2('val', '');
                });

                $sexoAluno.select2({
                    data: [
                        {id: 'masculino', text: 'Masculino'},
                        {id: 'feminino', text: 'Feminino'}
                    ]
                });

                $mesAniversario.select2({
                    allowClear: true,
                    data: [
                        {id: '01', text: 'Janeiro'},
                        {id: '02', text: 'Fevereiro'},
                        {id: '03', text: 'Março'},
                        {id: '04', text: 'Abril'},
                        {id: '05', text: 'Maio'},
                        {id: '06', text: 'Junho'},
                        {id: '07', text: 'Julho'},
                        {id: '08', text: 'Agosto'},
                        {id: '09', text: 'Setembro'},
                        {id: '10', text: 'Outubro'},
                        {id: '11', text: 'Novembro'},
                        {id: '12', text: 'Dezembro'}
                    ]
                });

                $estadoAluno.select2({
                    language: 'pt-BR',
                    minimumInputLength: 1,
                    allowClear: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.buscaCEP,
                        type: 'POST',
                        dataType: "json",
                        data: function (query) {
                            return {
                                estado: query
                            };
                        },
                        results: function (data) {
                            var transformed = $.map(data.dados || [], function (el) {
                                return {text: el.est_nome, id: el.est_uf};
                            });

                            return {results: transformed};
                        }
                    }
                }).on('change', function () {
                    var estado = $(this).val();
                    if (estado != '') {
                        $cidadeAluno.select2('enable');
                    } else {
                        $cidadeAluno.select2('disable');
                        $cidadeAluno.select2('val', '').trigger('change');
                    }
                });

                $cidadeAluno.select2({
                    language: 'pt-BR',
                    minimumInputLength: 1,
                    allowClear: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.buscaCEP,
                        type: 'POST',
                        dataType: "json",
                        data: function (query) {
                            return {
                                estado: $estadoAluno.val(),
                                cidade: query
                            };
                        },
                        results: function (data) {
                            var transformed = $.map(data.dados || [], function (el) {
                                return {
                                    text: el.cid_nome,
                                    id: el.cid_nome,
                                    estadoId: el.est_id,
                                    estadoNome: el.est_nome
                                };
                            });

                            return {results: transformed};
                        }
                    }
                });

                $situacao.select2({
                    language: 'pt-BR',
                    multiple: true,
                    data: __AcadgeralAlunoIndex.options.data.arrAlunocursoSituacao
                });

                $campusCurso.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    placeholder: '',
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.campusCurso,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {area: $areaConhecimentoCurso.val(), nivel: $nivel.val(), query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.camp_nome + ' / ' + el.curso_nome, id: el.cursocampus_id};
                            });

                            return {results: transformed};
                        }
                    }
                });

                $agentes.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    multiple: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.agentesEducacionais,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.pesNome, id: el.pesId};
                            });

                            transformed.push({id: '-', text: 'Sem Agente'});

                            return {results: transformed};
                        }
                    }
                });

                $unidadeEstudo.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    multiple: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.orgUnidadeEstudo,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            var arrQuery = {query: query};

                            if (__AcadgeralAlunoIndex.options.value.agente) {
                                arrQuery['pesId'] = __AcadgeralAlunoIndex.options.value.agenteEducacional.id || '';
                            }

                            return arrQuery;
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.unidade_nome, id: el.unidade_id};
                            });

                            transformed.push({id: '-', text: 'Sem Unidade'});

                            return {results: transformed};
                        }
                    }
                });

                $('#operadoresCadastro, #operadoresAlteracao').select2({
                    language: 'pt-BR',
                    allowClear: true,
                    multiple: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.acessoPessoas,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query, agruparPorUsuario: true};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.login + ' | ' + el.pes_nome, id: el.id};
                            });
                            return {results: transformed};
                        }
                    }
                });

                $cidadeAluno.select2('disable');

                $(document).on('change select2-selected select2-clearing select2-removed', 'input, select',
                    function () {
                        var $item = $(this);

                        if ($item.val() != '') {
                            $item.parent().find('.select2-container').addClass('select2-allowclear');
                        } else {
                            $item.parent().find('.select2-container').removeClass('select2-allowclear');
                        }
                    }
                );

                __AcadgeralAlunoIndex.iniciarElementoDatePicker($("#dataCadastroInicial"));
                __AcadgeralAlunoIndex.iniciarElementoDatePicker($("#dataCadastroFinal"));
                __AcadgeralAlunoIndex.iniciarElementoDatePicker($("#dataAlteracaoInicial"));
                __AcadgeralAlunoIndex.iniciarElementoDatePicker($("#dataAlteracaoFinal"));

                if (__AcadgeralAlunoIndex.options.value.agente) {
                    $('#aluno-acoes-botoes').hide();
                    $agentes.select2('data', __AcadgeralAlunoIndex.options.value.agenteEducacional);
                    $('#agentes, #operadoresCadastro, #operadoresAlteracao').select2('disable');
                    $('[name=agentesOpcao]').prop('disabled', true);
                    $('#btn-exportar-alunos').hide();
                }

                $('.exportar-alunos').click(function () {
                    var $formAlunoIndex = $('#form-aluno-index');
                    var tipo = ($(this).html() || '').trim().toLowerCase();
                    tipo = tipo || 'pdf';
                    $formAlunoIndex.find('[name=tipo]').val(tipo);
                    $formAlunoIndex.submit();
                });

                $("#alunosFiltroExecutar").click(function () {
                    __AcadgeralAlunoIndex.options.value.buscaComFiltro = 0;
                    __AcadgeralAlunoIndex.listagem.atualizarListagem();
                });

                $("#alunosFiltroLimpar").click(function () {
                    $campusCurso.select2('val', '').trigger('change');
                    $situacao.select2('val', '').trigger('change');
                    $unidadeEstudo.select2('val', '').trigger('change');
                    $cidadeAluno.select2('val', '').trigger('change');
                    $estadoAluno.select2('val', '').trigger('change');
                    $areaConhecimentoCurso.select2('val', '').trigger('change');
                    $nivel.select2('val', '').trigger('change');
                    $sexoAluno.select2('val', '').trigger('change');
                    $mesAniversario.select2('val', '').trigger('change');
                    $('#idadeInicial').val('').trigger('change');
                    $('#idadeFinal').val('').trigger('change');
                    $('#diaAniversario').val('').trigger('change');

                    if (!__AcadgeralAlunoIndex.options.value.agente) {
                        $('#agentes').select2('val', '').trigger('change');
                    }

                    $('#operadoresCadastro').select2('val', '').trigger('change');
                    $('#operadoresAlteracao').select2('val', '').trigger('change');
                    $('#dataCadastroInicial').val('').trigger('change');
                    $('#dataCadastroFinal').val('').trigger('change');
                    $('#dataAlteracaoInicial').val('').trigger('change');
                    $('#dataAlteracaoFinal').val('').trigger('change');
                });

            },
            atualizarListagem: function () {
                __AcadgeralAlunoIndex.dataTableAcadgeralAluno.api().ajax.reload(null, false);
            },
            iniciarDatatables: function () {
                var colNum = -1, $dataTableAcadgeralAluno = $('#dataTableAcadgeralAluno');
                var $footCell = $dataTableAcadgeralAluno.find('tfoot td');
                $footCell.html('');

                $.each(__AcadgeralAlunoIndex.options.data.arrSituacoesCor, function (i, item) {
                    $footCell.append('<span class="' + item.cssClass + '">' + item.descricao + '</span> &nbsp; ');
                });

                __AcadgeralAlunoIndex.dataTableAcadgeralAluno = $dataTableAcadgeralAluno.dataTable({
                    processing: true,
                    serverSide: true,
                    searchDelay: 350,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.search,
                        data: function (d) {
                            __AcadgeralAlunoIndex.options.value.mensagemDataTable = 'Use os filtros para realizar a busca!';
                            d.filter = d.filter || {};
                            d.index = true;
                            var campusCurso = $('#campusCurso').val() || "";
                            var situacao = $('#situacao').val() || "";
                            var unidadeEstudo = $('#unidadeEstudo').val() || "";
                            var cidadeAluno = $('#cidadeAluno').val() || "";
                            var estadoAluno = $('#estadoAluno').val() || "";
                            var agentes = $('#agentes').val() || "";
                            var agentesOpcao = $('[name="agentesOpcao"]:checked').val() || "incluirAgente";
                            var operadoresCadastro = $('#operadoresCadastro').val() || "";
                            var operadoresAlteracao = $('#operadoresAlteracao').val() || "";
                            var dataAlteracaoFinal = $('#dataAlteracaoFinal').val() || "";
                            var dataCadastroFinal = $('#dataCadastroFinal').val() || "";
                            var dataAlteracaoInicial = $('#dataAlteracaoInicial').val() || "";
                            var dataCadastroInicial = $('#dataCadastroInicial').val() || "";

                            var areaConhecimentoCurso = $('#areaConhecimentoCurso').val() || "";
                            var nivel = $('#nivel').val() || "";
                            var sexoAluno = $('#sexoAluno').val() || "";
                            var mesAniversario = $('#mesAniversario').val() || "";
                            var idadeInicial = $('#idadeInicial').val() || "";
                            var idadeFinal = $('#idadeFinal').val() || "";
                            var diaAniversario = $('#diaAniversario').val() || "";

                            if (cidadeAluno) {
                                d.filter['cidadeAluno'] = cidadeAluno;
                            }

                            if (estadoAluno) {
                                d.filter['estadoAluno'] = estadoAluno;
                            }

                            if (areaConhecimentoCurso) {
                                d.filter['areaConhecimentoCurso'] = areaConhecimentoCurso;
                            }

                            if (nivel) {
                                d.filter['nivel'] = nivel;
                            }

                            if (diaAniversario) {
                                d.filter['diaAniversario'] = diaAniversario;
                            }

                            if (sexoAluno) {
                                d.filter['sexoAluno'] = sexoAluno;
                            }

                            if (mesAniversario) {
                                d.filter['mesAniversario'] = mesAniversario;
                            }

                            if (idadeInicial) {
                                d.filter['idadeInicial'] = idadeInicial;
                            }

                            if (idadeFinal) {
                                d.filter['idadeFinal'] = idadeFinal;
                            }

                            if (campusCurso) {
                                d.filter['campusCurso'] = campusCurso;
                            }

                            if (situacao) {
                                d.filter['situacao'] = situacao;
                            }

                            if (unidadeEstudo) {
                                d.filter['unidadeEstudo'] = unidadeEstudo;
                            }

                            if (agentesOpcao) {
                                d.filter['agentesOpcao'] = agentesOpcao;
                            }

                            if (agentes) {
                                d.filter['agentes'] = agentes;
                            }

                            if (operadoresCadastro) {
                                d.filter['operadoresCadastro'] = operadoresCadastro;
                            }

                            if (operadoresAlteracao) {
                                d.filter['operadoresAlteracao'] = operadoresAlteracao;
                            }

                            if (dataAlteracaoFinal) {
                                d.filter['dataAlteracaoFinal'] = dataAlteracaoFinal;
                            }

                            if (dataCadastroFinal) {
                                d.filter['dataCadastroFinal'] = dataCadastroFinal;
                            }

                            if (dataAlteracaoInicial) {
                                d.filter['dataAlteracaoInicial'] = dataAlteracaoInicial;
                            }

                            if (dataCadastroInicial) {
                                d.filter['dataCadastroInicial'] = dataCadastroInicial;
                            }

                            d.buscaComFiltro = __AcadgeralAlunoIndex.options.value.buscaComFiltro;

                            if ((Object.keys(d.filter)).length > 1) {
                                __AcadgeralAlunoIndex.options.value.mensagemDataTable = 'Nenhum registro encontrado!';
                            }

                            return d;
                        },
                        type: "POST",
                        dataSrc: function (json) {
                            var data = json.data;

                            for (var row in data) {
                                data[row]['alunocurso_data_matricula_formatado'] = (
                                    __AcadgeralAlunoIndex.formatDate(data[row]['alunocurso_data_matricula'])
                                );

                                var btns = [];

                                var permissaoDeferirCurso = __AcadgeralAlunoIndex.verificaPermissao('deferirCurso');
                                var permissaoEditarAluno = __AcadgeralAlunoIndex.verificaPermissao('editarAluno');

                                if (!__AcadgeralAlunoIndex.options.value.agente) {
                                    if (data[row]['alunocurso_situacao'] == 'Pendente' && permissaoDeferirCurso) {
                                        btns.push({class: 'btn-success acadgeralAluno-deferir', icon: 'fa-check'});
                                    }

                                    if (permissaoEditarAluno) {
                                        btns.push({class: 'btn-primary acadgeralAluno-edit', icon: 'fa-pencil'});
                                    }
                                }

                                btns.push({class: 'btn-info acadgeralAluno-acoes', icon: 'fa-list-alt'});

                                data[row]['acao'] = __AcadgeralAlunoIndex.createBtnGroup(btns);

                            }

                            __AcadgeralAlunoIndex.removeOverlay($('.widget-body'));

                            return data;
                        }
                    },
                    columns: [
                        {name: "aluno_id", targets: ++colNum, data: "aluno_id"},
                        {name: "alunocurso_id", targets: ++colNum, data: "alunocurso_id"},
                        {name: "campus_curso_nome", targets: ++colNum, data: "campus_curso_nome"},
                        {
                            name: "alunocurso_data_matricula",
                            targets: ++colNum,
                            data: "alunocurso_data_matricula_formatado",
                            searchable: false
                        },
                        {name: "pes_nome", targets: ++colNum, data: "pes_nome"},
                        {name: "pes_cpf", targets: ++colNum, data: "pes_cpf"},
                        {name: "con_contato_celular", targets: ++colNum, data: "con_contato_celular"},
                        {name: "con_contato_email", targets: ++colNum, data: "con_contato_email"},
                        {name: "aluno_id", targets: ++colNum, data: "acao", orderable: false, searchable: false}
                    ],
                    "columnDefs": [
                        {
                            "targets": 0,
                            "createdCell": function (td, cellData, rowData) {
                                var arrCor = __AcadgeralAlunoIndex.options.data.arrSituacoesCor[rowData['alunocurso_situacao']] || null;

                                if (arrCor) {
                                    $(td).parent().addClass(arrCor['cssClass'] || '');
                                }
                            }
                        }
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: __AcadgeralAlunoIndex.options.value.mensagemDataTable,
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    "fnDrawCallback": function (oSettings) {
                        $(".dataTables_empty").text(__AcadgeralAlunoIndex.options.value.mensagemDataTable);
                    },
                    order: [[3, 'desc']]
                });
            },
            iniciarAcoesDatatables: function () {
                var $dataTableAcadgeralAluno = $('#dataTableAcadgeralAluno');

                $dataTableAcadgeralAluno.on('click', '.acadgeralAluno-edit', function () {
                    var $pai = $(this).closest('tr');
                    var data = __AcadgeralAlunoIndex.dataTableAcadgeralAluno.fnGetData($pai);

                    location.href = __AcadgeralAlunoIndex.options.url.edit + '/' + data['aluno_id'];
                });

                $dataTableAcadgeralAluno.on('click', '.acadgeralAluno-deferir', function () {
                    var $pai = $(this).closest('tr');
                    var data = __AcadgeralAlunoIndex.dataTableAcadgeralAluno.fnGetData($pai);

                    __AcadgeralAlunoIndex.deferimento.carregarDados(data);
                });

                $dataTableAcadgeralAluno.on('click', '.acadgeralAluno-acoes', function () {
                    var $pai = $(this).closest('tr');

                    var data = __AcadgeralAlunoIndex.dataTableAcadgeralAluno.fnGetData($pai);
                    __AcadgeralAlunoIndex.acoes.carregarDados(data);
                });

                var permissaoRelatorioAlunos = __AcadgeralAlunoIndex.verificaPermissao('relatorioAlunos');

                if (!permissaoRelatorioAlunos) {
                    $('#btn-exportar-alunos').hide();
                }

                $(".panel-title a").click(function () {
                    $(".panel-title .fa").toggleClass('fa-chevron-right').toggleClass('fa-chevron-down');
                });
            }
        };

        this.deferimento = {
            carregarDados: function (arrAlunoCurso) {
                var $form = $('#deferimento-curso-aluno-form');
                var $modal = $('#modal-deferimento-curso');
                __AcadgeralAlunoIndex.addOverlay(
                    $modal.find('.modal-content'),
                    'Buscando informações de pagamento do curso. Aguarde...'
                );
                $modal.modal('show');
                $('#alunocursoId').val(arrAlunoCurso['alunocurso_id']);
                $('#pesNome').html(arrAlunoCurso['pes_nome']);
                $('#campNome').html(arrAlunoCurso['camp_nome']);
                $('#cursoNome').html(arrAlunoCurso['curso_nome']);
                $('#conContatoCelular').html(arrAlunoCurso['con_contato_celular']);
                $('#conContatoEmail').html(arrAlunoCurso['con_contato_email']);
                $('#loginSupervisor, #senhaSupervisor,#configPgtoCursoJustificativa').val('');
                $('#componentId,#numeroParcelas,#valorParcelas,#configPgtoCursoObs').val('');

                $.ajax({
                    url: __AcadgeralAlunoIndex.options.url.alunoCurso,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        alunoConfigPagto: true,
                        alunocursoId: arrAlunoCurso['alunocurso_id']
                    },
                    success: function (data) {
                        __AcadgeralAlunoIndex.options.data.tipoTitulo = data['tipoTitulo'] || [];
                        __AcadgeralAlunoIndex.options.data.alunoConfigPgtoCurso = data['alunoConfigPgtoCurso'] || [];
                        data['cursocampus'] = data['cursocampus'] || [];

                        __AcadgeralAlunoIndex.deferimento.limparCampoTiposTitulo();

                        if (data['cursocampus']["cursoSituacao"] == 'Inativo') {
                            __AcadgeralAlunoIndex.showNotificacaoDanger("Este curso está inativo!");
                            $('#informacoes-pagamento').addClass('hidden');
                            $('#btn-efetuar-deferimento-curso').prop('disabled', true);
                        } else if (!__AcadgeralAlunoIndex.deferimento.carregarTiposTitulo()) {
                            $modal.modal('hide');
                        }

                        __AcadgeralAlunoIndex.removeOverlay($modal);
                    },
                    erro: function () {
                        __AcadgeralAlunoIndex.removeOverlay($modal);
                    }
                });
            },
            alterarPagamento: function () {
                var $componente = $(this).closest('.componente-item');
                var id = $componente.data('componente.id');
                var componente = $("#tipoTitulo").componente();

                var $configPgtoCursoObs = $componente.find(
                    '[name="' + componente.makeFieldName({id: id, name: 'configPgtoCursoObs'}) + '"]'
                );

                var $configPgtoCursoValor = $componente.find(
                    '[name="' + componente.makeFieldName({id: id, name: 'configPgtoCursoValor'}) + '"]'
                );

                var $configPgtoCursoParcela = $componente.find(
                    '[name="' + componente.makeFieldName({id: id, name: 'configPgtoCursoParcela'}) + '"]'
                );
                var $valorParcelas = $('#valorParcelas');

                $('#componentId').val(id);
                $('#numeroParcelas').val($configPgtoCursoParcela.val());
                $('#configPgtoCursoObs').val($configPgtoCursoObs.val());
                $valorParcelas.val($configPgtoCursoValor.val());
                $valorParcelas.data('valor', $configPgtoCursoValor.val() * $configPgtoCursoParcela.val());

                $("#numeroParcelas, #valorParcelas").trigger('change');

                $('#modal-alteracao-pagto-curso').modal('show');
            },
            deferirCursoAluno: function () {
                var $form = $('#deferimento-curso-aluno-form'),
                    $loginSupervisor = $('#loginSupervisor'),
                    $modal = $('#modal-deferimento-curso');

                if (__AcadgeralAlunoIndex.deferimento.camposTiposTituloPossuiErros()) {
                    return false;
                }

                var requerLogin = $loginSupervisor.is(':visible');
                var loginSupervisor = $loginSupervisor.val();
                var senhaSupervisor = $('#senhaSupervisor').val();
                var configPgtoCursoJustificativa = $('#configPgtoCursoJustificativa').val();

                if (requerLogin && (!senhaSupervisor || !loginSupervisor || !configPgtoCursoJustificativa)) {
                    __AcadgeralAlunoIndex.showNotificacaoDanger(
                        "Por favor forneça as credenciais do supervisor e a justificativa!"
                    );

                    return false;
                }

                __AcadgeralAlunoIndex.addOverlay($modal.find('.modal-content'), 'Deferindo curso de aluno. Aguarde...');

                var arrData = {
                    alunocursoId: $('#alunocursoId').val(),
                    alunoConfigPagtoCurso: $("#tipoTitulo").componente().exportJSON(),
                    loginSupervisor: loginSupervisor,
                    senhaSupervisor: senhaSupervisor,
                    justificativa: configPgtoCursoJustificativa
                };

                $.ajax({
                    url: __AcadgeralAlunoIndex.options.url.deferirAlunoCurso,
                    dataType: 'json',
                    type: 'post',
                    data: arrData,
                    success: function (data) {
                        if (data.erro) {
                            __AcadgeralAlunoIndex.showNotificacaoDanger(
                                data['mensagem'] || "Não foi possível deferir curso!"
                            );
                        } else {
                            __AcadgeralAlunoIndex.showNotificacaoSuccess(
                                data['mensagem'] || "Curso Deferido!"
                            );
                            $('#modal-deferimento-curso').modal('hide');
                            __AcadgeralAlunoIndex.dataTableAcadgeralAluno.api().ajax.reload(null, false);
                        }

                        __AcadgeralAlunoIndex.removeOverlay($modal);
                    },
                    erro: function () {
                        __AcadgeralAlunoIndex.removeOverlay($modal);
                        $modal.modal('hide');
                        __AcadgeralAlunoIndex.dataTableAcadgeralAluno.api().ajax.reload(null, false);
                    }
                });
            },
            camposTiposTituloPossuiErros: function () {
                var errors = [];
                var $tipoTitulo = $("#tipoTitulo");
                var componente = $tipoTitulo.componente();
                var tipoTitulo = componente.items();

                __AcadgeralAlunoIndex.removeErroComponente($tipoTitulo);

                $.each(tipoTitulo, function (i, item) {
                    var $item = $(item);
                    var id = $(item).data('componente.id');
                    __AcadgeralAlunoIndex.removeErroComponente($item);

                    var camposVazios = [];

                    var $eltipotituloNome = $item
                        .find('[name="' + componente.makeFieldName({id: id, name: 'tipotituloNome'}) + '"]');
                    var $elvaloresId = $item
                        .find('[name="' + componente.makeFieldName({id: id, name: 'valoresId'}) + '"]');
                    var $elvencimento = $item
                        .find('[name="' + componente.makeFieldName({id: id, name: 'vencimento'}) + '"]');
                    var $tipotituloMatriculaEmissao = $item
                        .find('[name="' + componente.makeFieldName({
                            id: id,
                            name: 'tipotituloMatriculaEmissao'
                        }) + '"]');

                    var vencimentoVal = $elvencimento.val();
                    var tipotituloNomeVal = $eltipotituloNome.val();
                    var valoresIdVal = $elvaloresId.val();
                    var tituloObrigatorio = $tipotituloMatriculaEmissao.val();

                    if (tituloObrigatorio == 'Obrigatório') {
                        if (valoresIdVal == "") {
                            camposVazios.push("Pagamento");
                        }
                    }

                    if (tituloObrigatorio == 'Obrigatório' || valoresIdVal) {
                        if (vencimentoVal == "") {
                            camposVazios.push("Vencimento");
                        } else {

                            var data = new Date();
                            var dtVencimentoVal = __AcadgeralAlunoIndex.formatDate(vencimentoVal, 'yy-mm-dd');
                            data = __AcadgeralAlunoIndex.formatDate(data, 'yy-mm-dd');
                            dtVencimentoVal = new Date(dtVencimentoVal);
                            data = new Date(data);

                            if (dtVencimentoVal < data) {
                                errors.push('O ' + (i + 1) + '&ordm ' + 'campo de vencimento possui valor inválido ou está vazio!');
                            }
                        }
                    }

                    if (camposVazios.length > 0) {
                        errors.push(
                            'O titulo ' + tipotituloNomeVal +
                            ' possui campos vazios. Por favor preencha os campos: <b>' +
                            camposVazios.join(', ') + '</b>.'
                        );
                        __AcadgeralAlunoIndex.adicionaErroComponente($item, true);
                    }
                });

                if (errors.length > 0) {
                    __AcadgeralAlunoIndex.showNotificacaoDanger(errors.join('<br>'));
                    return true;
                }

                return false;
            },
            iniciarFuncionalidades: function () {
                var $numeroParcelas = $("#numeroParcelas");
                var $valorParcelas = $("#valorParcelas");

                $numeroParcelas.inputmask({showMaskOnHover: false, mask: ['9{0,9}'], rightAlign: true});
                $valorParcelas.inputmask({
                    showMaskOnHover: false,
                    alias: 'currency',
                    prefix: '',
                    groupSeparator: ''
                });

                $("#numeroParcelas, #valorParcelas").change(function () {
                    $('#valorFinal').html(
                        __AcadgeralAlunoIndex.formatarMoeda(
                            parseFloat($numeroParcelas.val()) *
                            parseFloat($valorParcelas.val())
                        )
                    )
                });
                $numeroParcelas.change(function () {
                    $valorParcelas.val(
                        parseFloat($valorParcelas.data('valor')) /
                        parseFloat($numeroParcelas.val())
                    );
                    $valorParcelas.trigger('change');
                });

                $('#alteracao-pagto-form').validate({
                    submitHandler: function () {
                        var $modal = $('#modal-alteracao-pagto-curso');
                        var id = $('#componentId').val();
                        var componente = $("#tipoTitulo").componente();
                        var itens = componente.items();

                        $.each(itens, function (i, item) {
                            var $item = $(item);

                            if ($(item).data('componente.id') == id) {
                                var $configPgtoCursoObs = $item.find(
                                    '[name="' + componente.makeFieldName({id: id, name: 'configPgtoCursoObs'}) + '"]'
                                );
                                var $configPgtoCursoValor = $item.find(
                                    '[name="' + componente.makeFieldName({id: id, name: 'configPgtoCursoValor'}) + '"]'
                                );
                                var $configPgtoCursoAlterado = $item.find(
                                    '[name="' + componente.makeFieldName({
                                        id: id,
                                        name: 'configPgtoCursoAlterado'
                                    }) + '"]'
                                );
                                var $configPgtoCursoParcela = $item.find(
                                    '[name="' + componente.makeFieldName({
                                        id: id,
                                        name: 'configPgtoCursoParcela'
                                    }) + '"]'
                                );
                                var $valoresId = $item.find(
                                    '[name="' + componente.makeFieldName({id: id, name: 'valoresId'}) + '"]'
                                );
                                var $vencimento = $item.find(
                                    '[name="' + componente.makeFieldName({id: id, name: 'vencimento'}) + '"]'
                                );

                                $configPgtoCursoParcela.val($numeroParcelas.val());
                                $configPgtoCursoValor.val($valorParcelas.val());
                                $configPgtoCursoObs.val($('#configPgtoCursoObs').val());

                                $configPgtoCursoAlterado.val('Sim');

                                $valoresId.select2('data', {
                                    id: $valoresId.val() || '0',
                                    valoresParcela: $numeroParcelas.val(),
                                    valoresPreco: $valorParcelas.val(),
                                    text: $('#numeroParcelas').val() + ' x ' + $valorParcelas.val()
                                });

                                __AcadgeralAlunoIndex.deferimento.verificarNecessidadeAutenticacaoSupervisor();
                            }
                        });

                        $modal.modal('hide');
                        return false;
                    },
                    ignore: '.ignore',
                    rules: {
                        numeroParcelas: {required: true},
                        valorParcelas: {required: true}
                    },
                    messages: {
                        numeroParcelas: {required: 'Campo obrigatório!'},
                        valorParcelas: {required: 'Campo obrigatório!'}
                    }
                });


                var arrCampos = [];

                arrCampos.push({name: 'tipotituloId', label: 'tipotituloId', type: 'hidden'});
                arrCampos.push({name: 'configPgtoCursoId', label: 'configPgtoCursoId', type: 'hidden'});
                arrCampos.push({name: 'configPgtoCursoValor', label: 'configPgtoCursoValor', type: 'hidden'});
                arrCampos.push({name: 'configPgtoCursoParcela', label: 'configPgtoCursoParcela', type: 'hidden'});
                arrCampos.push({name: 'configPgtoCursoObs', label: 'configPgtoCursoObs', type: 'hidden'});
                arrCampos.push({name: 'tipotituloMatriculaEmissao', label: 'Emissão Obrigatória', type: 'hidden'});
                arrCampos.push({
                    name: 'tipotituloNome', label: 'Título',
                    addCallback: function (e) {
                        var field = e.data.field, itemConfig = e.data.itemSettings;
                        var $parent = field.parent();
                        $parent.addClass('col-sm-5');
                        field.addClass('hidden');
                        field.before(
                            '<p class="form-control-static">' + itemConfig['tipotituloNome'] + '</p>'
                        );
                    }
                });
                arrCampos.push({
                    name: 'valoresId', label: 'Pagamento',
                    addCallback: function (e) {
                        var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                        var $parent = field.parent();
                        var $item = field.closest('.componente-item');

                        if (__AcadgeralAlunoIndex.verificaPermissao('integracaoMaxiPago')) {
                            $parent.addClass('col-sm-2');
                        } else {
                            $parent.addClass('col-sm-3');
                        }

                        var id = itemConfig.id;

                        itemConfig['valoresId'] = itemConfig['valoresId'] || null;

                        field.select2({
                            language: 'pt-BR',
                            minimumResultsForSearch: -1,
                            allowClear: true,
                            data: function () {
                                var encontrou = false;
                                var arrResults = $.map(itemConfig['financeiroValores'], function (el) {
                                    el.id = el['valoresId'];
                                    el.text = el['valoresParcela'] + ' x ' + el['valoresPreco'];

                                    if (
                                        encontrou == false &&
                                        parseInt(el.id) == parseInt(itemConfig['valoresId'])
                                    ) {
                                        encontrou = true;
                                    }

                                    return el;
                                });

                                return {results: arrResults};
                            }
                        }).change(function () {
                            var arrData = field.select2('data') || {};
                            var parcelas = arrData['valoresParcela'] || $('#numeroParcelas').val();
                            var valor = arrData['valoresPreco'] || $('#valorParcelas').val();
                            var manual = arrData['manual'] || false;

                            var $configPgtoCursoAlterado = $item.find(
                                '[name="' + config.makeFieldName({
                                    id: id,
                                    name: 'configPgtoCursoAlterado'
                                }) + '"]'
                            );
                            var $configPgtoCursoValor = $item.find(
                                '[name="' + config.makeFieldName({id: id, name: 'configPgtoCursoValor'}) + '"]'
                            );
                            var $configPgtoCursoParcela = $item.find(
                                '[name="' + config.makeFieldName({
                                    id: id,
                                    name: 'configPgtoCursoParcela'
                                }) + '"]'
                            );
                            var $valoresId = $item.find(
                                '[name="' + config.makeFieldName({id: id, name: 'valoresId'}) + '"]'
                            );

                            if (!manual) {
                                $configPgtoCursoValor.data('total', valor * parcelas);
                            }

                            $configPgtoCursoParcela.val(parcelas);
                            $configPgtoCursoValor.val(valor);

                            $configPgtoCursoAlterado.parent().find('.btn-alterar-valor-pgto').prop('disabled',
                                false);

                            if (!$valoresId.val()) {
                                $configPgtoCursoAlterado.parent().find('.btn-alterar-valor-pgto').prop('disabled',
                                    true);
                            }

                            __AcadgeralAlunoIndex.deferimento.verificarNecessidadeAutenticacaoSupervisor();
                        });

                        field.select2('val', itemConfig['valoresId'] || null).trigger('change');
                    }
                });
                arrCampos.push({
                    name: 'configPgtoCursoAlterado', label: 'Alterar',
                    addCallback: function (e) {
                        var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                        var $parent = field.parent();
                        var $item = field.closest('.componente-item');
                        var id = itemConfig.id;
                        $parent.addClass('col-sm-1');
                        field.addClass('hidden');

                        var $valoresId = $item.find(
                            '[name="' + config.makeFieldName({id: id, name: 'valoresId'}) + '"]'
                        );

                        var disabledAttr = $valoresId.val() ? '' : 'disabled';

                        field.after(
                            '<button type="button" ' + disabledAttr +
                            ' class="form-control btn btn-secondary btn-alterar-valor-pgto">' +
                            '<span class="fa fa-edit"></span>' +
                            '</button>'
                        );
                    }
                });
                arrCampos.push({
                    name: 'vencimento', label: 'Melhor Vencimento',
                    addCallback: function (e) {
                        var field = e.data.field, itemConfig = e.data.itemSettings;
                        var $parent = field.parent();

                        if (__AcadgeralAlunoIndex.verificaPermissao('integracaoMaxiPago')) {
                            $parent.addClass('col-sm-2');
                        } else {
                            $parent.addClass('col-sm-3');
                        }
                        __AcadgeralAlunoIndex.iniciarElementoDatePicker(field, {minDate: 0});
                    }
                });

                if (__AcadgeralAlunoIndex.verificaPermissao('integracaoMaxiPago')) {
                    arrCampos.push({
                        name: 'configPgtoCursoCartao', label: 'Usar Cartão?',
                        addCallback: function (e) {

                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-2');

                            field.select2({
                                allowClear: false,
                                data: [
                                    {'id': 'sim', 'text': 'Sim'},
                                    {'id': 'nao', 'text': 'Não'}
                                ]
                            });

                            field.select2('val', (__AcadgeralAlunoIndex.options.value.cartaoId ? 'sim' : 'nao')).trigger('change');
                            field.select2("disable").trigger('change');
                        }
                    });
                }

                $("#tipoTitulo").componente({
                    inputName: 'tipoTitulo',
                    classCardExtra: '',
                    labelContainer: '<div class="form-group"/>',
                    tplItem: '\
                        <div>\
                            <div class="componente-content col-sm-12">\
                            </div>\
                        </div>',
                    fieldClass: 'form-control',
                    fields: arrCampos
                });

                $(document).on('click', '.btn-alterar-valor-pgto', __AcadgeralAlunoIndex.deferimento.alterarPagamento);
                $("#btn-efetuar-deferimento-curso").click(__AcadgeralAlunoIndex.deferimento.deferirCursoAluno);
            },
            verificarNecessidadeAutenticacaoSupervisor: function () {
                var componente = $("#tipoTitulo").componente();
                var itens = componente.items();
                var alteracoesDeValores = 0;

                $.each(itens, function (i, item) {
                    var $item = $(item);
                    var id = $item.data('componente.id');

                    var $configPgtoCursoValor = $item.find(
                        '[name="' + componente.makeFieldName({id: id, name: 'configPgtoCursoValor'}) + '"]'
                    );
                    var $configPgtoCursoAlterado = $item.find(
                        '[name="' + componente.makeFieldName({id: id, name: 'configPgtoCursoAlterado'}) + '"]'
                    );
                    var $configPgtoCursoParcela = $item.find(
                        '[name="' + componente.makeFieldName({id: id, name: 'configPgtoCursoParcela'}) + '"]'
                    );

                    var totalOriginal = $configPgtoCursoValor.data('total');
                    var totalAtual = ($configPgtoCursoParcela.val() * $configPgtoCursoValor.val());

                    if (totalOriginal != totalAtual) {
                        alteracoesDeValores++;
                        $configPgtoCursoAlterado.val('Sim');
                    } else {
                        $configPgtoCursoAlterado.val('-');
                    }
                });

                if (alteracoesDeValores > 0) {
                    $('#dados-supervisor').removeClass('hidden');
                } else {
                    $('#dados-supervisor').addClass('hidden');
                }
            },
            limparCampoTiposTitulo: function () {
                $("#tipoTitulo").componente().removeAll();
                $(".cartaoTitular").text('-');
                $(".cartaoFinal").text('-');
                $(".cartaoVencimento").text('-');
                $(".infoCartao").addClass('hidden');
            },
            carregarTiposTitulo: function () {
                $('#informacoes-pagamento').removeClass('hidden');
                $('#btn-efetuar-deferimento-curso').prop('disabled', false);
                __AcadgeralAlunoIndex.deferimento.limparCampoTiposTitulo();

                var errors = [];

                if (__AcadgeralAlunoIndex.options.data.tipoTitulo) {
                    $.each(__AcadgeralAlunoIndex.options.data.tipoTitulo || [], function (i, item) {
                        var itemAdd = $.extend({}, item);
                        var obrigatorio = 'Obrigatório';

                        if (itemAdd['tipotituloMatriculaEmissao'] != 'Sim') {
                            obrigatorio = 'Opcional';
                        }

                        itemAdd['tipotituloNome'] = itemAdd['tipotituloNome'] + ' (' + obrigatorio + ')';
                        itemAdd['tipotituloMatriculaEmissao'] = obrigatorio;

                        if ((item['financeiroValores'] || []).length == 0) {
                            errors.push(
                                'O tipo de título <b>' +
                                item['tipotituloNome'] +
                                '</b> não possui valores financeiros definidos!'
                            );
                            return;
                        }

                        $.each(__AcadgeralAlunoIndex.options.data.alunoConfigPgtoCurso || [], function (i2, item2) {
                            if (item['tipotituloId'] == item2['tipotitulo']) {
                                itemAdd = $.extend(itemAdd, item2);

                                itemAdd['vencimento'] = item2['configPgtoCursoVencimento'];
                                itemAdd['configPgtoCursoId'] = item2['configPgtoCursoId'];

                                $.each(item['financeiroValores'] || [], function (i3, item3) {
                                    if (
                                        item3['valoresParcela'] == item2['configPgtoCursoParcela'] &&
                                        parseFloat(item3['valoresPreco']) ==
                                        parseFloat(item2['configPgtoCursoValor'])
                                    ) {
                                        itemAdd['valoresId'] = item3['valoresId'];
                                    }
                                });

                                if (!itemAdd['valoresId'] && itemAdd['valores']) {
                                    itemAdd['valoresId'] = itemAdd['valores'];
                                }
                            }
                        });
                        __AcadgeralAlunoIndex.options.value.cartaoId = itemAdd['cartaoId'];

                        $('#tipoTitulo').componente(['add', itemAdd]);
                        /*Por hora só faz uso de um cartão*/
                        if (itemAdd['cartaoId']) {
                            $(".infoCartao").removeClass('hidden');
                            $(".cartaoTitular").text(itemAdd['cartaoTitular']);
                            $(".cartaoFinal").text(itemAdd['cartaoFinal']);
                            $(".cartaoVencimento").text(itemAdd['cartaoVencimento']);
                        }
                    });
                }

                __AcadgeralAlunoIndex.deferimento.verificarNecessidadeAutenticacaoSupervisor();

                if (errors.length > 0) {
                    __AcadgeralAlunoIndex.showNotificacaoDanger(errors.join('<br>'));
                    return false;
                }

                return true;
            }
        };

        this.acoes = {
            dadosAlunoAcao: function () {
                return __AcadgeralAlunoIndex.options.value.arrAluno || {};
            },
            carregarDados: function (arrAluno, dadosEstaticos) {
                dadosEstaticos = dadosEstaticos || false;
                arrAluno = arrAluno || {};
                arrAluno = $.extend([], arrAluno, __AcadgeralAlunoIndex.keysToCamelCase(arrAluno));

                var $form = $('#aluno-curso-acoes-form');
                var $modal = $('#modal-aluno-curso-acoes');
                var $modalContent = $modal.find('.modal-content');

                arrAluno['alunocursoId'] = arrAluno['alunocursoId'] || '';

                if (!arrAluno['alunocursoId']) {
                    __AcadgeralAlunoIndex.showNotificacaoDanger("O aluno selecionado não possui curso!");
                    return false
                }

                $modal.modal();
                __AcadgeralAlunoIndex.addOverlay($modalContent, 'Buscando informações de aluno. Aguarde...');

                if (!dadosEstaticos) {
                    $.ajax({
                        url: __AcadgeralAlunoIndex.options.url.alunoCursoInformacoes,
                        data: {
                            alunocursoId: arrAluno['alunocursoId'] || '',
                            tituloAberto: 1,
                            verificarDependencias: 1,
                            verificarFinanceiro: 1,
                            verificarDocumentos: 1,
                            retornarIntegracoes: 1,
                            pendenciasConclusao: 0
                        },
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            if (data.erro == true) {
                            } else {
                                arrAluno = $.extend([], arrAluno, data['alunoCurso'] || []);
                                __AcadgeralAlunoIndex.acoes.carregarDados(arrAluno, true);
                            }
                        }
                    });
                }

                __AcadgeralAlunoIndex.options.value.arrAluno = arrAluno || {};

                __AcadgeralAlunoIndex.acoes.exibirDadosAluno($form);

                if (dadosEstaticos) {
                    __AcadgeralAlunoIndex.removeOverlay($modalContent);
                }
            },
            iniciarFuncionalidadeModalAlteracao: function () {
                var $formAcoes = $('#aluno-curso-acoes-form'),
                    $periodoLetivoAlteracao = $('[name="periodoLetivoAlteracao"]'),
                    $alunocursoSituacao = $('[name="alunocursoSituacao"]'),
                    $motivoAlteracao = $('[name="motivoAlteracao"]'),
                    $form = $('#form-alterar-situacao'),
                    $modal = $('#alteracao-modal');

                $periodoLetivoAlteracao.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    data: __AcadgeralAlunoIndex.options.data.arrPeriodoLetivo
                });

                $periodoLetivoAlteracao.select2('disable');

                $alunocursoSituacao.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    data: __AcadgeralAlunoIndex.options.data.arrAlunocursoSituacaoAlteracao
                }).change(function () {
                    $motivoAlteracao.select2('data', null).trigger('change');
                });

                $motivoAlteracao.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.motivoAlteracao,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {
                                motivoSituacao: $alunocursoSituacao.val(),
                                query: query
                            }
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.text = el.motivo_descricao;
                                el.id = el.motivo_id;
                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });

                $form.validate({
                    ignore: '.ignore',
                    rules: {
                        alunocursoSituacao: {required: true},
                        motivoAlteracao: {required: true}
                    },
                    messages: {
                        alunocursoSituacao: {required: 'Campo obrigatório!'},
                        motivoAlteracao: {required: 'Campo obrigatório!'}
                    }
                });

                $form.on('submit', function (e) {
                    var ok = $form.valid();


                    if (!ok) {
                        e.stopPropagation();
                        e.preventDefault();
                        return false;
                    }

                    var dados = $form.serializeJSON();
                    dados['alunocursoId'] = $formAcoes.find('[name=alunocursoId]').val();

                    __AcadgeralAlunoIndex.addOverlay($modal.find('.modal-content'),
                        'Alterando situação de curso de aluno. Aguarde...');

                    $.ajax({
                        url: __AcadgeralAlunoIndex.options.url.alterarSituacao,
                        type: 'POST',
                        dataType: 'json',
                        data: dados,
                        success: function (data) {
                            if (!data['erro']) {
                                __AcadgeralAlunoIndex.showNotificacaoSuccess(
                                    data['mensagem'] || "Situação do aluno Alterada com Sucesso!"
                                );

                                __AcadgeralAlunoIndex.dataTableAcadgeralAluno.api().ajax.reload(null, false);

                                $alunocursoSituacao.select2('val', '').trigger('change');
                                $motivoAlteracao.select2('val', '').trigger('change');

                                $modal.modal('hide');
                                __AcadgeralAlunoIndex.acoes.carregarDados(__AcadgeralAlunoIndex.acoes.dadosAlunoAcao());
                            } else {
                                __AcadgeralAlunoIndex.showNotificacaoDanger(
                                    data['mensagem'] || "Falha ao alterar situação do aluno!"
                                );
                            }

                            __AcadgeralAlunoIndex.removeOverlay($modal);
                        }
                    });

                    e.stopPropagation();
                    e.preventDefault();
                });

                $('#btnModalAlterarSituacao').click(function (e) {
                    var $form = $('#form-alterar-situacao');
                    var $alunocursoSituacao = $form.find('[name="alunocursoSituacao"]'),
                        $motivoAlteracao = $form.find('[name="motivoAlteracao"]');

                    $alunocursoSituacao.select2('val', '').trigger('change');
                    $motivoAlteracao.select2('val', '').trigger('change');

                    $('[name="alteracaoObservacao"]').val("");

                    __AcadgeralAlunoIndex.acoes.exibirDadosAluno($form);

                    $('#alteracao-modal').modal('show');
                    e.stopPropagation();
                    e.preventDefault();
                });
            },
            iniciarFuncionalidadeModalOutrasInformacoes: function () {
                var $form = $('#form-outras-informacoes'),
                    $modal = $('#outras-informacoes-modal'),
                    $alunocursoCarteira = $form.find('[name="alunocursoCarteira"]'),
                    $alunocursoObservacoes = $form.find('[name="alunocursoObservacoes"]'),
                    $alunocursoEnade = $form.find('[name="enadeInformacoes"]');

                $alunocursoCarteira.inputmask({showMaskOnHover: false, mask: ['9{0,20}']});

                $alunocursoObservacoes.val('');
                $alunocursoEnade.val('');
                $alunocursoCarteira.val('');

                $form.validate({
                    ignore: '.ignore',
                    rules: {
                        alunocursoCarteira: {number: true}
                    },
                    messages: {
                        alunocursoCarteira: {number: 'Numero inválido!'}
                    }
                });

                $form.on('submit', function (e) {
                    var ok = $form.valid();

                    if (!ok) {
                        e.stopPropagation();
                        e.preventDefault();
                        return false;
                    }

                    var dados = $form.serializeJSON();

                    __AcadgeralAlunoIndex.addOverlay(
                        $modal.find('.modal-content'),
                        'Salvando as informações do aluno curso. Aguarde...'
                    );

                    $.ajax({
                        url: __AcadgeralAlunoIndex.options.url.outrasInformacoes,
                        type: 'POST',
                        dataType: 'json',
                        data: dados,
                        success: function (data) {
                            if (!data['erro']) {
                                __AcadgeralAlunoIndex.showNotificacaoSuccess(
                                    data['mensagem'] || "Informações salvas com Sucesso!"
                                );

                                __AcadgeralAlunoIndex.dataTableAcadgeralAluno.api().ajax.reload(null, false);

                                $modal.modal('hide');
                                __AcadgeralAlunoIndex.acoes.carregarDados(__AcadgeralAlunoIndex.acoes.dadosAlunoAcao());
                            } else {
                                __AcadgeralAlunoIndex.showNotificacaoDanger(
                                    data['mensagem'] || "Falha ao alterar as informações deste aluno!"
                                );
                            }

                            __AcadgeralAlunoIndex.removeOverlay($modal);
                        }
                    });

                    e.stopPropagation();
                    e.preventDefault();
                });

                $("#btnModalOutrasInformacoes").click(function (e) {
                    var arrAluno = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao();

                    __AcadgeralAlunoIndex.acoes.exibirDadosAluno($form);

                    $alunocursoCarteira.val(arrAluno['alunocursoCarteira']);
                    $alunocursoObservacoes.val(arrAluno['alunocursoObservacoes']);
                    $alunocursoEnade.val(arrAluno['alunocursoEnade']);

                    $modal.modal('show');

                    e.stopPropagation();
                    e.preventDefault();
                });
            },
            iniciarFuncionalidadeModalAlteracaoDatas: function () {
                var $form = $('#form-alterar-datas'),
                    $alunocursoDataMatricula = $form.find('[name="alunocursoDataMatricula"]'),
                    $alunocursoDataColacao = $form.find('[name="alunocursoDataColacao"]'),
                    $modal = $('#alunocurso-datas-alteracao-modal'),
                    $alunocursoDataExpedicaoDiploma = $form.find('[name="alunocursoDataExpedicaoDiploma"]');

                __AcadgeralAlunoIndex.iniciarElementoDatePicker($alunocursoDataMatricula, {maxDate: "today"});
                __AcadgeralAlunoIndex.iniciarElementoDatePicker($alunocursoDataColacao);
                __AcadgeralAlunoIndex.iniciarElementoDatePicker($alunocursoDataExpedicaoDiploma);

                $form.validate({
                    ignore: '.ignore',
                    rules: {
                        alunocursoDataMatricula: {required: true, maxDate: true}
                    },
                    messages: {
                        alunocursoDataMatricula: {required: 'Campo obrigatório!'}
                    }
                });

                $form.on('submit', function (e) {
                    var ok = $form.valid();
                    var dataMatricula = parseInt(__AcadgeralAlunoIndex.formatDate($alunocursoDataMatricula.val(), 'yymmdd'));
                    var dataColacao = parseInt(__AcadgeralAlunoIndex.formatDate($alunocursoDataColacao.val(), 'yymmdd'));
                    var dataExpedicao = parseInt(__AcadgeralAlunoIndex.formatDate($alunocursoDataExpedicaoDiploma.val(), 'yymmdd'));

                    if (dataColacao && dataColacao <= dataMatricula) {
                        __AcadgeralAlunoIndex.showNotificacaoWarning(
                            'A data de Colação não pode ser menor ou igual a data de Matrícula!'
                        );
                        return false;

                    }

                    if (dataExpedicao && dataExpedicao < dataColacao) {
                        __AcadgeralAlunoIndex.showNotificacaoWarning(
                            'A data de Expedição não pode ser menor a data de Colação!'
                        );
                        return false;
                    }

                    if (!ok) {
                        e.stopPropagation();
                        e.preventDefault();
                        return false;
                    }

                    var dados = $form.serializeJSON();

                    __AcadgeralAlunoIndex.addOverlay(
                        $modal.find('.modal-content'),
                        'Alterando datas de curso de aluno. Aguarde...'
                    );

                    $.ajax({
                        url: __AcadgeralAlunoIndex.options.url.alterarDatas,
                        type: 'POST',
                        dataType: 'json',
                        data: dados,
                        success: function (data) {
                            if (!data['erro']) {
                                __AcadgeralAlunoIndex.showNotificacaoSuccess(
                                    data['mensagem'] || "Datas do curso do aluno alteradas com sucesso!"
                                );

                                __AcadgeralAlunoIndex.dataTableAcadgeralAluno.api().ajax.reload(null, false);

                                $modal.modal('hide');
                                __AcadgeralAlunoIndex.acoes.carregarDados(__AcadgeralAlunoIndex.acoes.dadosAlunoAcao());
                            } else {
                                __AcadgeralAlunoIndex.showNotificacaoDanger(
                                    data['mensagem'] || "Falha ao alterar datas do curso do aluno!"
                                );
                            }

                            __AcadgeralAlunoIndex.removeOverlay($modal);
                        }
                    });

                    e.stopPropagation();
                    e.preventDefault();
                });

                $("#btnModalAlterarDatas").click(function () {
                    var arrAluno = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao();

                    $alunocursoDataMatricula.val(arrAluno['alunocursoDataMatricula'] || '');
                    $alunocursoDataColacao.val(arrAluno['alunocursoDataColacao'] || '');
                    $alunocursoDataExpedicaoDiploma.val(arrAluno['alunocursoDataExpedicaoDiploma'] || '');

                    __AcadgeralAlunoIndex.acoes.exibirDadosAluno($form);

                    $("#alunocurso-datas-alteracao-modal").modal('show');
                });

                $("#btnEmitirDocumentos").click(function () {
                    $('#emitirDocumento').select2('val', '').trigger('change');
                    $('#documentoValor').select2('val', '').trigger('change');
                    $('#documentoVencimento').val(__AcadgeralAlunoIndex.formatDate(new Date()));
                    $("#emissao-documentos-modal").modal('show');
                });
            },
            iniciarFuncionalidadeIntegracaoManual: function () {
                var $form = $('#aluno-curso-acoes-form'),
                    $modal = $('#modal-aluno-curso-acoes');

                $("#btnModalIntegracao").click(function (e) {
                    var arrAluno = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao();
                    __AcadgeralAlunoIndex.addOverlay(
                        $modal.find('.modal-content'),
                        'Registrando integração manual de aluno. Aguarde...'
                    );

                    $.ajax({
                        url: __AcadgeralAlunoIndex.options.url.integracaoManual,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            alunoId: arrAluno.alunoId,
                            filaIntegracao: true
                        },
                        success: function (data) {
                            if (!data['erro']) {
                                __AcadgeralAlunoIndex.showNotificacaoSuccess(
                                    data['mensagem'] || "Aluno foi adicionado na fila de integração!"
                                );
                            } else {
                                __AcadgeralAlunoIndex.showNotificacaoDanger(
                                    data['mensagem'] || "Falha ao adicionar aluno a fila de integração!"
                                );
                            }

                            __AcadgeralAlunoIndex.removeOverlay($modal);
                        }
                    });

                    e.stopPropagation();
                    e.preventDefault();

                });
            },

            iniciarFuncionalidadeIntegracaoManualNota: function () {
                var $form = $('#aluno-curso-acoes-form'),
                    $modal = $('#modal-aluno-curso-acoes');

                var permissaoIntegracaonota = __AcadgeralAlunoIndex.verificaPermissao('integracaoNota');

                if (permissaoIntegracaonota) {
                    $("#btnModalIntegracaoNota").click(function (e) {
                        var arrAluno = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao();
                        __AcadgeralAlunoIndex.addOverlay(
                            $modal.find('.modal-content'),
                            'Registrando integração de nota do aluno. Aguarde, essa ação pode levar alguns minutos!'
                        );

                        $.ajax({
                            url: __AcadgeralAlunoIndex.options.url.integracaoNotaManual,
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                alunoCursoId: arrAluno.alunocursoId,
                                integracaoManualNota: true
                            },
                            success: function (data) {
                                if (data['sucessoComErro']) {
                                    __AcadgeralAlunoIndex.showNotificacaoSuccess(
                                        data['mensagemSucesso'] || "A integração de nota foi realizada!"
                                    );
                                    __AcadgeralAlunoIndex.showNotificacaoWarning(
                                        data['mensagemErro'] || "Algumas disciplinas apresentaram problema ao realizar a integração!"
                                    );

                                } else if (!data['erro']) {
                                    __AcadgeralAlunoIndex.showNotificacaoSuccess(
                                        data['mensagem'] || "A integração de nota foi realizada!"
                                    );
                                } else {
                                    __AcadgeralAlunoIndex.showNotificacaoDanger(
                                        data['mensagem'] || "Erro ao realizar a integração de notas do aluno!"
                                    );
                                }

                                __AcadgeralAlunoIndex.removeOverlay($modal);
                            }
                        });

                        e.stopPropagation();
                        e.preventDefault();

                    });
                } else {
                    $("#btnModalIntegracaoNota").addClass('hidden');
                }


            },
            iniciarFuncionalidadeModalEmissaoDocumentos: function () {
                var $form = $('#form-emissao-documentos'),
                    $formAcoes = $('#aluno-curso-acoes-form'),
                    $modal = $('#emissao-documentos-modal'),
                    dataAtual = new Date(),
                    $documentoValor = $('#documentoValor'),
                    $emitirDocumento = $('#emitirDocumento'),
                    $documentoVencimento = $("#documentoVencimento");

                $emitirDocumento.select2('val', '').trigger('change');

                $documentoValor.select2('val', '').trigger('change');

                __AcadgeralAlunoIndex.iniciarElementoDatePicker($documentoVencimento, {minDate: 'today'});

                $documentoVencimento.val(__AcadgeralAlunoIndex.formatDate(dataAtual));

                $('.divPagamento').addClass('hidden');

                $emitirDocumento.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.documentoModelo,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {
                                query: query,
                                modeloContexto: ['Aluno', 'Aluno Curso', 'Pessoa'],
                                campId: $formAcoes.find('[name=campId]').val(),
                                cursoId: $formAcoes.find('[name=cursoId]').val()
                            };
                        },
                        results: function (data) {

                            var transformed = $.map(data, function (el) {
                                return {
                                    text: el.modelo_descricao,
                                    id: el.modelo_id,
                                    tipoTitulo: el.tipotitulo_id
                                };
                            });
                            return {results: transformed};
                        }
                    }
                }).change(function () {
                    var arrObjEmitirDocumento = $emitirDocumento.select2('data') || {};
                    $documentoValor.select2('val', '').trigger('change');

                    if (arrObjEmitirDocumento.tipoTitulo) {
                        $('.divPagamento').removeClass('hidden')
                    } else {
                        $('.divPagamento').addClass('hidden');
                    }
                });

                $documentoValor.select2({
                    language: 'pt-br',
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.financeiroValores,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            var arrObjEmitirDocumento = $emitirDocumento.select2('data') || {},
                                arrAluno = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao();

                            return {
                                query: query,
                                titulotipoId: arrObjEmitirDocumento['tipoTitulo'] || '',
                                cursocampusId: arrAluno['cursocampusId'] || '',
                                periodoLetivo: arrAluno['perId'] || ''
                            };
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                if (el) {
                                    return {
                                        text: el.valoresParcela + 'x' + el.valoresPreco,
                                        id: el.valoresId,
                                        valoresPreco: el.valoresPreco,
                                        valoresParcela: el.valoresParcela,
                                        valoresId: el.valoresId
                                    };
                                }

                            });
                            return {results: transformed};
                        }
                    }
                });

                $('#btnEmitirCancelar').click(function () {
                    $modal.modal('hide');
                });

                $('#btnEmitir').click(function () {
                    if ($('.divPagamento').is(":visible") && !$documentoValor.val()) {
                        __AcadgeralAlunoIndex.showNotificacaoWarning("A emissão deste título implica na geração de título!" +
                            "\n Porfavor preencha todos os campos!");
                        return false;
                    }

                    if (!$emitirDocumento.val()) {
                        __AcadgeralAlunoIndex.showNotificacaoWarning("Selecione um modelo de documento!");
                        return false;
                    }

                    var
                        arrValores = $documentoValor.select2('data') || {},
                        arrDocumento = $emitirDocumento.select2('data') || {},
                        arrAluno = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao(),
                        arrDados = {};

                    arrDados['pesId'] = (arrAluno['pesId'] || $formAcoes.find('[name=pesId]').val() || '');
                    arrDados['modeloId'] = (arrDocumento['id'] || '');
                    arrDados['alunocursoId'] = (arrAluno['alunocursoId'] || $formAcoes.find('[name=alunocursoId]').val() || '');
                    arrDados['alunoId'] = (arrAluno['alunoId'] || $formAcoes.find('[name=alunoId]').val() || '');
                    arrDados['cursoCampusId'] = (arrAluno['cursoId'] || '');
                    arrDados['valorId'] = (arrValores['id'] || '');
                    arrDados['dataVencimento'] = $documentoVencimento.val();

                    __AcadgeralAlunoIndex.addOverlay($modal.find('.modal-content'), 'Aguarde...');

                    var link = __AcadgeralAlunoIndex.options.url.documentoEmitir + '?' +
                        '&modeloId=' + arrDados['modeloId'] +
                        '&pesId=' + arrDados['pesId'] +
                        '&alunoId=' + arrDados['alunoId'] +
                        '&alunocursoId=' + arrDados['alunocursoId'] +
                        '&valorId=' + arrDados['valorId'] +
                        '&dataVencimento=' + arrDados['dataVencimento'];

                    var a = document.createElement("a");
                    a.href = link;
                    a.target = '_blank';
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);

                    $('#emissao-documentos-modal').modal('hide');
                    __AcadgeralAlunoIndex.removeOverlay($modal.find('.modal-content'));
                    return false;

                    /* TODO: IMPLEMENTAÇÃO DESATIVADA DEVIDA A ATUALIZAÇÃO, ATUALIZAR USABILIDADE FUTURA.

                     $.post(__AcadgeralAlunoIndex.options.url.documentoEmitir, arrDados,
                     function (data, status, request) {
                     if (!data.erro) {
                     __AcadgeralAlunoIndex.showNotificacaoSuccess('Documento Gerado!');

                     var filename = request.getResponseHeader('Content-Disposition').split('filename=');
                     filename = filename[1] || '';

                     var blob = new Blob([data], {type: 'application/octet-stream'});
                     var downloadUrl = window.URL.createObjectURL(blob);

                     var a = document.createElement("a");

                     if (typeof a.download === '') {
                     window.location = downloadUrl;
                     } else {
                     a.href = downloadUrl;
                     a.download = filename;
                     document.body.appendChild(a);
                     a.click();
                     document.body.removeChild(a);
                     }
                     $modal.modal('hide');
                     } else {
                     __AcadgeralAlunoIndex.showNotificacaoDanger(
                     'Ocorreu um erro!<br>' + (data['mensagem'] || '')
                     );
                     }



                     __AcadgeralAlunoIndex.removeOverlay($modal.find('.modal-content'));
                     }
                     );*/
                });
            },
            iniciarFuncionalidades: function () {
                __AcadgeralAlunoIndex.acoes.iniciarFuncionalidadeModalAlteracao();
                __AcadgeralAlunoIndex.acoes.iniciarFuncionalidadeModalEmissaoDocumentos();
                __AcadgeralAlunoIndex.acoes.iniciarFuncionalidadeModalAlteracaoDatas();
                __AcadgeralAlunoIndex.acoes.iniciarFuncionalidadeModalOutrasInformacoes();
                __AcadgeralAlunoIndex.acoes.iniciarFuncionalidadeIntegracaoManual();
                __AcadgeralAlunoIndex.acoes.iniciarFuncionalidadeIntegracaoManualNota();

                var permissaoAlterarSituacao = __AcadgeralAlunoIndex.verificaPermissao('alterarSituacao'),
                    permissaoAlterarDatas = __AcadgeralAlunoIndex.verificaPermissao('alterarDatas'),
                    permissaoEmitirDocumento = __AcadgeralAlunoIndex.verificaPermissao('emitirDocumento'),
                    permissaoTrocarCurso = __AcadgeralAlunoIndex.verificaPermissao('trocarCurso'),
                    permissaoFichaInscricao = __AcadgeralAlunoIndex.verificaPermissao('fichaInscricao'),
                    permissaoOutrasInformacoes = __AcadgeralAlunoIndex.verificaPermissao('outrasInformacoes'),
                    permissaoIntegracaoManual = __AcadgeralAlunoIndex.verificaPermissao('integracaoManual'),
                    permissaoEnvioComunicacao = __AcadgeralAlunoIndex.verificaPermissao('envioComunicacao');
                if (
                    !permissaoAlterarSituacao && !permissaoAlterarDatas && !permissaoEmitirDocumento && //
                    !permissaoTrocarCurso && !permissaoFichaInscricao && !permissaoOutrasInformacoes && //
                    !permissaoIntegracaoManual && !permissaoEnvioComunicacao
                ) {
                    $('#aluno-acoes-botoes').hide();
                } else {
                    if (!permissaoAlterarSituacao) {
                        $('#btnModalAlterarSituacao').hide();
                    }

                    if (!permissaoAlterarDatas) {
                        $('#btnModalAlterarDatas').hide();
                    }

                    if (!permissaoEmitirDocumento) {
                        $('#btnEmitirDocumentos').hide();
                    }

                    if (!permissaoTrocarCurso) {
                        $('#btnModalTrocarCurso').hide();
                    }

                    if (!permissaoFichaInscricao) {
                        $('#alunoCursoFichaInscricao').hide();
                    }

                    if (!permissaoOutrasInformacoes) {
                        $('#btnModalOutrasInformacoes').hide();
                    }

                    if (!permissaoIntegracaoManual) {
                        $('#btnModalIntegracao').hide();
                    }

                    if (!permissaoEnvioComunicacao) {
                        $('#btnModalEnviarComunicacao').hide();
                    }
                }
                $("#expandeAccordion").click();
            },
            setPerId: function (perId) {
                var $periodoLetivoAlteracao = $('[name="periodoLetivoAlteracao"]');

                $.map(__AcadgeralAlunoIndex.options.data.arrPeriodoLetivo, function (el) {
                    if (perId == el['id']) {
                        $periodoLetivoAlteracao.select2('data', el).trigger('change');
                    }
                });
            },
            exibirDadosAluno: function ($form) {
                $form = $form || $('#aluno-curso-acoes-form');
                var arrAluno = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao();

                var arrCampos = {
                    'alunoId': 'alunoId',
                    'alunocursoId': 'alunocursoId',
                    'pesNome': 'pesNome',
                    'campusCurso': 'campusCurso',
                    'alunocursoSituacao': 'alunocursoSituacao',
                    'alunocursoDataCadastro': 'alunocursoDataCadastro',
                    'alunocursoDataAlteracao': 'alunocursoDataAlteracao',
                    'alunocursoDataMatricula': 'alunocursoDataMatricula',
                    'alunocursoDataExpedicaoDiploma': 'alunocursoDataExpedicaoDiploma',
                    'alunocursoDataColacao': 'alunocursoDataColacao',
                    'alunocursoObservacoes': 'alunocursoObservacoes',
                    'enadeInformacoes': 'alunocursoEnade',
                    'pesDataNascimento': 'pesDataNascimento',
                    'usuarioCadastro': 'usuarioCadastroLogin',
                    'unidadeNome': 'unidadeNome',
                    'agenteEducacional': 'pesNomeAgente',
                    'pesCpf': 'pesCpf',
                    'pesRg': 'pesRg',
                    'origemNome': 'origemNome',
                    'conContatoCelular': 'conContatoCelular',
                    'conContatoTelefone': 'conContatoTelefone',
                    'conContatoEmail': 'conContatoEmail',
                    'endCidadeEstado': 'endCidadeEstado',
                    'quantidadeTitulosAbertos': 'quantidadeTitulosAbertos',
                    'quantidadeTitulosVencidos': 'quantidadeTitulosVencidos',
                    'quantidadeDependencias': 'quantidadeDependencias',
                    'codigosIntegracao': 'codigosIntegracao',
                    'alunocursoCarteira': 'alunocursoCarteira',
                    'documentosNaoEntegues': 'documentosNaoEntegues'

                };

                $form.find('[name=pesId]').val(arrAluno['pesId'] || '');
                $form.find('[name=campId]').val(arrAluno['campId'] || '');
                $form.find('[name=cursoId]').val(arrAluno['cursoId'] || '');
                $form.find('[name=alunoId]').val(arrAluno['alunoId'] || '');
                $form.find('[name=alunocursoId]').val(arrAluno['alunocursoId'] || '');
                $form.find('[name=enadeInformacoes]').val(arrAluno['alunocursoEnade'] || '');

                $('#alunoCursoFichaInscricao').attr(
                    'href', __AcadgeralAlunoIndex.options.url.fichaInscricao + '/' + arrAluno['alunocursoId']
                );
                if (arrAluno['camposPersonalizados']) {
                    var campoHtml = '';
                    $.each(arrAluno['camposPersonalizados'], function (index, value) {
                        campoHtml += (
                            ' <div class="col-sm-4">' +
                            ' <div class="form-group"> ' +
                            '<label>' + value.pergunta + '</label> ' +
                            '<p class="form-control-static">' + value.resposta + '</p>' +
                            '</div>' +
                            '</div> '
                        );
                    });

                    $(".camposPersonalizados").html(campoHtml);
                }

                $form.find('.alunocursoSituacao').attr(
                    'title', arrAluno['alunocursoAlteracaoObservacao'] || ' Situação ' + arrAluno['alunocursoSituacao']
                );
                $form.find('.alunocursoSituacao').tooltip('destroy');
                $form.find('.alunocursoSituacao').tooltip();

                arrAluno['endCidadeEstado'] = (arrAluno['endCidade'] || '') + '/' + (arrAluno['endEstado'] || '');
                arrAluno['campusCurso'] = (arrAluno['campNome'] || '') + '/' + (arrAluno['cursoNome'] || '');

                for (var chave in arrCampos) {

                    var chaveValor = arrCampos[chave];
                    var valor = arrAluno[chaveValor] || '-';

                    if (chaveValor.indexOf('DataNascimento') != -1) {
                        valor = __AcadgeralAlunoIndex.formatDate(valor);
                    } else if (chaveValor.indexOf('Data') != -1) {
                        valor = __AcadgeralAlunoIndex.formatDateTime(valor);
                    }

                    $form.find('.' + chave).html(valor);
                }
            }
        };

        this.transferencia = {
            iniciarFuncionalidades: function () {
                var $form = $('#form-trocar-curso');
                var $modal = $('.modal-overlay'),
                    $campusCurso = $form.find('[name="campusCurso"]'),
                    $motivo = $form.find('[name="motivoId"]');

                $campusCurso.select2({
                    allowClear: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.campusCurso,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            var arrAluno = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao();

                            return {
                                query: query,
                                naoIncluirCampusCurso: arrAluno['alunoCursosMatriculado']
                            };
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.camp_nome + ' / ' + el.curso_nome, id: el.cursocampus_id};
                            });

                            return {results: transformed};
                        }
                    }
                });

                $motivo.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.motivoAlteracao,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {
                                motivoSituacao: 'Transferencia',
                                query: query
                            }
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.text = el.motivo_descricao;
                                el.id = el.motivo_id;
                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });

                $form.on('submit', function (e) {
                    e.stopPropagation()
                }).validate({
                    ignore: '.ignore',
                    submitHandler: function () {
                        __AcadgeralAlunoIndex.addOverlay($modal, 'Aguarde... ');

                        var arrDados = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao();
                        var dados = $form.serializeJSON();

                        dados['alunoId'] = arrDados['alunoId'] || '';
                        dados['alunocursoId'] = arrDados['alunocursoId'] || '';
                        dados['pesId'] = arrDados['pesId'] || '';

                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                $('#trocar-curso-modal').find('.close').click();
                                if (!data.result['erro']) {
                                    __AcadgeralAlunoIndex.showNotificacaoSuccess(
                                        data.result['message'] + '<br>Matricula: ' + data.result['alunocursoId'] || 'Transferência concluída'
                                    );

                                    __AcadgeralAlunoIndex.acoes.carregarDados(data.result);
                                    __AcadgeralAlunoIndex.listagem.atualizarListagem();
                                } else {
                                    __AcadgeralAlunoIndex.showNotificacaoDanger(
                                        data.result['message'] || 'Não foi possível transferir o aluno de curso'
                                    );
                                }

                                __AcadgeralAlunoIndex.removeOverlay($modal);
                            }
                        });
                    },
                    rules: {
                        campusCurso: {required: true},
                        motivoId: {required: true},
                        alteracaoObservacao: {required: true}
                    },
                    messages: {
                        campusCurso: {required: 'Campo Obrigatório'},
                        motivoId: {required: 'Campo Obrigatório'},
                        alteracaoObservacao: {required: 'Campo Obrigatório'}
                    }
                });

                $('#btnModalTrocarCurso').on('click', function (e) {
                    var situacao = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao().alunocursoSituacao;

                    if (situacao == "Deferido" || situacao == "Pendente") {
                        $('.help-block').remove();
                        $('.has-error').removeClass('has-error');

                        $motivo.select2('val', '').trigger('change');
                        $campusCurso.select2('val', '').trigger('change');

                        $('[name="alunocursoId"]').val(($('.alunocursoId').text() || '').replace(/^0+/g, '') || '');
                        $('#trocar-curso-modal').modal('show');
                    } else {
                        e.stopPropagation();
                        e.preventDefault();
                        __AcadgeralAlunoIndex.showNotificacaoWarning(
                            "Não é possível trocar alunos com situação do tipo: " + situacao
                        );
                        return false;
                    }

                    e.stopPropagation();
                    e.preventDefault();
                });
            }
        };

        this.comunicacao = {
            iniciarFuncionalidades: function () {
                var $form = $('#form-enviar-comunicacao');
                var $modeloComunicacao = $form.find('[name="modeloId"]'),
                    $modal = $('.modal-overlay');

                $modeloComunicacao.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __AcadgeralAlunoIndex.options.url.comunicacaoModelo,
                        dataType: 'json',
                        type: 'POST',
                        delay: 250,
                        data: function (query) {
                            return {
                                tipoContexto: ['Aluno', 'Aluno Curso', 'Pessoa'],
                                query: query
                            };
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.modelo_nome, id: el.modelo_id};
                            });

                            return {results: transformed};
                        }
                    }
                });

                $form.on('submit', function (e) {
                    e.stopPropagation()
                }).validate({
                    ignore: '.ignore',
                    submitHandler: function () {
                        __AcadgeralAlunoIndex.addOverlay($modal, 'Aguarde... ');

                        var arrDados = __AcadgeralAlunoIndex.acoes.dadosAlunoAcao();
                        var dados = $form.serializeJSON();

                        dados['alunoId'] = arrDados['alunoId'] || '';
                        dados['alunocursoId'] = arrDados['alunocursoId'] || '';
                        dados['pesId'] = arrDados['pesId'] || '';

                        $.ajax({
                            url: __AcadgeralAlunoIndex.options.url.envioComunicacao,
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (!data.erro) {
                                    __AcadgeralAlunoIndex.showNotificacaoSuccess(
                                        data.message || 'Registrado na fila de envio com Sucesso!'
                                    );
                                } else {
                                    __AcadgeralAlunoIndex.showNotificacaoDanger(
                                        data.message || 'Não foi possível registrar na fila de envio'
                                    );
                                }

                                __AcadgeralAlunoIndex.removeOverlay($modal);
                                $('#enviar-comunicacao-modal').modal('hide');
                            }
                        });
                    },
                    rules: {
                        modeloId: {required: true}
                    },
                    messages: {
                        modeloId: {required: 'Campo Obrigatório'}
                    }
                });

                $('#btnModalEnviarComunicacao').on('click', function (e) {
                    $modeloComunicacao.select2('val', '');
                    $('#enviar-comunicacao-modal').modal('show');
                    e.stopPropagation();
                    e.preventDefault();
                });
            }
        };

        __AcadgeralAlunoIndex.run = function (opts) {
            __AcadgeralAlunoIndex.setDefaults(opts);
            __AcadgeralAlunoIndex.acoes.iniciarFuncionalidades();
            __AcadgeralAlunoIndex.deferimento.iniciarFuncionalidades();
            __AcadgeralAlunoIndex.listagem.iniciarFuncionalidades();
            __AcadgeralAlunoIndex.comunicacao.iniciarFuncionalidades();
            __AcadgeralAlunoIndex.transferencia.iniciarFuncionalidades();
        };
    };

    $.acadgeralAlunoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-aluno.index");

        if (!obj) {
            obj = new AcadgeralAlunoIndex();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-aluno.index', obj);
        }

        return obj;
    };

    $(document).on('click', '#acessarBiblioteca', function () {
        var url = window.urlBibliotecaVirtual;
        var $modal = $("#modal-minha-biblioteca"), $iframe = $("#iframe-minha-biblioteca");
        var pesId = $modal.find("#loaded").val() || false;

        if (pesId) {
            $modal.modal('show');
        }

        $.post(url, function (data) {
            if (pesId == data['pesId']) {
                $modal.modal('show');
                return false;
            }

            if (data['url']) {
                $iframe.attr("src", data['url']);
                $modal.modal('show');
            }

            $modal.find("#loaded").val(data['pesId'] || false);
        })
    });
})(window.jQuery, window, document);