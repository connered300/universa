(function ($, window, document) {
    'use strict';

    var AcadgeralAlunoCaptacao = function () {
        VersaShared.call(this);

        var __AcadgeralAlunoCaptacao = this;

        this.defaults = {
            url: {
                buscaCEP: '',
                pesquisaPF: '',
                buscaFormacao: '',
                cadastroOrigem: '',
                tipoTitulo: '',
                agenteEducacional: '',
                unidadeEstudo: '',
                campusCurso: '',
                validaCartao: ''
            },
            data: {
                necessidadesEspeciais: [],
                origem: [],
                arrEstados: [],
                tipoTitulo: [],
                arrCamposPersonalizados: [],
                arrCamposPersonalizadosValidate: []
            },
            value: {
                pessoaNecessidades: null,
                tipoTitulo: null,
                pesIdAgente: [],
                unidade: [],
                agente: false,
                campusCursoSelecao: [],
                maxiPagoAtivo: null,
                data: null
            },
            wizardElement: '#acadgeral-aluno-wizard',
            formElement: '#acadgeral-aluno-form',
            validator: null
        };

        this.steps = {
            dadosPessoais: {
                init: function () {
                    $("[name=pesCpf]").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['999.999.999-99']
                    });

                    $("[name=pesCpf]").change(function () {
                        var $form = $(this).closest('form');

                        var pesCpf = $form.find("[name=pesCpf]").val();
                        var pesCpfNums = pesCpf.replace(/[^0-9]/g, '');

                        if (pesCpfNums.length < 11) {
                            __AcadgeralAlunoCaptacao.showNotificacaoWarning('CPF inválido!');

                            return;
                        }

                        $.ajax({
                            url: __AcadgeralAlunoCaptacao.options.url.buscaPF,
                            dataType: 'json',
                            type: 'post',
                            data: {pesCpf: pesCpf},
                            success: function (data) {
                                if (Object.keys(data).length == 0) {
                                    $('#jaRegistrado').val('');
                                } else {
                                    __AcadgeralAlunoCaptacao.showNotificacaoWarning('Aluno já registrado no sistema!');
                                    __AcadgeralAlunoCaptacao.showNotificacaoInfo('Acesse o ambiente do aluno e faça a adição de curso!');
                                    $('#jaRegistrado').val('1');
                                }
                            },
                            erro: function () {
                                __AcadgeralAlunoCaptacao.showNotificacaoDanger('Erro desconhecido!');
                            }
                        });
                    });

                    $("[name=pesDataNascimento]")
                        .datepicker({
                            prevText: '<i class="fa fa-chevron-left"></i>',
                            nextText: '<i class="fa fa-chevron-right"></i>'
                        })
                        .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});

                    $("[name=pesSexo],[name=alunoEtnia]").select2({
                        language: 'pt-BR',
                        minimumResultsForSearch: -1,
                    });
                    $("[name=pessoaNecessidades]").select2({
                        language: 'pt-BR',
                        minimumResultsForSearch: -1,
                        multiple: true,
                        data: function () {
                            return {results: __AcadgeralAlunoCaptacao.options.data.necessidadesEspeciais};
                        }
                    });

                    $("#origem").select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __AcadgeralAlunoCaptacao.options.url.cadastroOrigem,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                return {query: query};
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.origem_nome, id: el.origem_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });

                    var $pesIdAgente = $('#pesIdAgente'),
                        $unidade = $("#unidade");

                    $unidade.select2({
                        language: 'pt-BR',
                        ajax: {
                            url: __AcadgeralAlunoCaptacao.options.url.unidadeEstudo,
                            dataType: 'json',
                            delay: 250,
                            data: function (query) {
                                var arrQuery = {query: query};

                                if (__AcadgeralAlunoCaptacao.options.value.agente) {
                                    arrQuery['pesId'] = $pesIdAgente.val();
                                }

                                return arrQuery;
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.unidade_nome, id: el.unidade_id};
                                });

                                return {results: transformed};
                            }
                        }
                    });
                    $('#origem').select2("data", __AcadgeralAlunoCaptacao.getOrigem());
                    $("[name=pessoaNecessidades]")
                        .select2('data', __AcadgeralAlunoCaptacao.options.value.pessoaNecessidades || []);
                    $unidade.select2("data", __AcadgeralAlunoCaptacao.getUnidade());

                    if (Object.keys(__AcadgeralAlunoCaptacao.getCamposPersonalizados()).length > 0) {
                        var regras = __AcadgeralAlunoCaptacao.camposPersonalizados('dadosPessoais');

                        if (regras) {
                            __AcadgeralAlunoCaptacao.setCamposPersonalizadosValidate('dadosPessoais', regras);
                        }
                    }
                },
                validate: function () {
                    var $form = $(__AcadgeralAlunoCaptacao.options.formElement);
                    __AcadgeralAlunoCaptacao.options.validator.settings.rules = {
                        pesNacionalidade: {required: true},
                        pesCpf: {
                            cpf: true,
                            required: function () {
                                return $form.find('[name=pesNacionalidade]').val() != 'Estrangeiro';
                            }
                        },
                        pesDocEstrangeiro: {
                            required: function () {
                                return $form.find('[name=pesNacionalidade]').val() == 'Estrangeiro';
                            }
                        },
                        pesNome: {required: true, maxlength: 255},
                        pesNaturalidade: {required: true},
                        pesNascUf: {required: true},
                        alunoEtnia: {required: true},
                        pesDataNascimento: {required: true, dateBR: true},
                        pesSexo: {required: true},
                        pesRg: {required: true},
                        pesRgEmissao: {required: false, dateBR: true},
                        alunoPai: {required: false},
                        alunoMae: {required: false},
                        origem: {required: true},
                        unidade: {required: true}
                    };
                    __AcadgeralAlunoCaptacao.options.validator.settings.messages = {
                        pesNacionalidade: {required: 'Campo obrigatório!'},
                        pesCpf: {
                            required: 'Campo obrigatório!',
                            cpf: 'CPF inválido!'
                        },
                        pesDocEstrangeiro: {required: 'Campo obrigatório!'},
                        pesNome: {required: 'Campo obrigatório!', maxlength: 'Tamanho máximo: 255!'},
                        pesNaturalidade: {required: 'Campo obrigatório!'},
                        pesNascUf: {required: 'Campo obrigatório!'},
                        alunoEtnia: {required: 'Campo obrigatório!'},
                        pesDataNascimento: {
                            required: 'Campo obrigatório!',
                            dateBR: 'Data inválida!'
                        },
                        pesSexo: {required: 'Campo obrigatório!'},
                        pesRg: {required: 'Campo obrigatório!'},
                        pesRgEmissao: {
                            required: 'Campo obrigatório!',
                            dateBR: 'Data inválida!'
                        },
                        alunoPai: {required: 'Campo obrigatório!'},
                        alunoMae: {required: 'Campo obrigatório!'},
                        origem: {required: 'Campo obrigatório!'},
                        unidade: {required: 'Campo obrigatório!'}
                    };

                    if ($('#jaRegistrado').val()) {
                        __AcadgeralAlunoCaptacao.showNotificacaoWarning('Aluno já registrado no sistema!');
                        __AcadgeralAlunoCaptacao.showNotificacaoInfo('Acesse o ambiente do aluno e faça a adição de curso!');

                        return true;
                    }

                    __AcadgeralAlunoCaptacao.setarValidacoesCamposPersonalizados('dadosPessoais');

                    return !$(__AcadgeralAlunoCaptacao.options.formElement).valid();
                }
            },
            contatoEndereco: {
                init: function () {
                    $("[name=endCep]").inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']});
                    $('[name=endNumero]').inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
                    $("[name=conContatoTelefone], [name=conContatoCelular]").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                    });
                    $(".buscarCep").click(function () {
                        var $element = $(this);
                        var endCep = $(this).closest('form').find("[name=endCep]").val();
                        var endCepNums = endCep.replace(/[^0-9]/g, '');

                        if (endCepNums.length < 8) {
                            __AcadgeralAlunoCaptacao.showNotificacaoWarning('CEP inválido ou não encontrado!');

                            return;
                        }

                        $element.prop('disabled', true);

                        $.ajax({
                            url: __AcadgeralAlunoCaptacao.options.url.buscaCEP,
                            dataType: 'json',
                            type: 'post',
                            data: {cep: endCep},
                            success: function (data) {
                                $element.prop('disabled', false);

                                if (!data.dados) {
                                    __AcadgeralAlunoCaptacao.showNotificacaoWarning('CEP inválido ou não encontrado!');
                                }

                                $element.closest('form').find("[name=endLogradouro]").val(
                                    ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                                );
                                $element.closest('form').find("[name=endCidade]").val(data.dados.cid_nome || '');
                                $element.closest('form').find("[name=endEstado]").val(data.dados.est_uf || '').trigger('change');
                                $element.closest('form').find("[name=endBairro]").val(data.dados.bai_nome || '');
                            },
                            erro: function () {
                                __AcadgeralAlunoCaptacao.showNotificacaoDanger('Falha ao buscar CEP!');
                                $element.prop('disabled', false);
                            }
                        });
                    });
                    $('[name=pesNaturalidade], [name=endCidade], [name=formacaoCidade] ')
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $this = $(this);
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                $element.data("jqXHR", $.ajax({
                                    url: __AcadgeralAlunoCaptacao.options.url.buscaCEP,
                                    data: {cidade: request.term},
                                    type: 'POST',
                                    dataType: "json",
                                    success: function (data) {
                                        var transformed = $.map(data.dados || [], function (el) {
                                            el.label = el.cid_nome;

                                            return el;
                                        });

                                        response(transformed);
                                    }
                                }));
                            },
                            minLength: 2,
                            focus: function (event, ui) {
                                var $element = $(this);
                                $element.val(ui.item.cid_nome);

                                return false;
                            },
                            select: function (event, ui) {
                                var $element = $(this);
                                $element.val(ui.item.cid_nome);
                                $element.closest('form').find(
                                    $element.attr('id') == 'endCidade' ? "[name=endEstado]" : (
                                        $element.attr('id') == 'formacaoCidade' ?
                                            "[name=formacaoEstado]" : "[name=pesNascUf]"
                                    )
                                )
                                    .val(ui.item.est_uf).trigger('change');

                                return false;
                            }
                        });

                    $('[name=pesNascUf], [name=endEstado], [name=formacaoEstado]').select2({
                        language: 'pt-BR',
                        allowClear: true,
                        data: function () {
                            return {results: __AcadgeralAlunoCaptacao.options.data.arrEstados || []};
                        }
                    });

                    if (Object.keys(__AcadgeralAlunoCaptacao.getCamposPersonalizados()).length > 0) {
                        var regras = __AcadgeralAlunoCaptacao.camposPersonalizados('contatoEndereco');

                        if (regras) {
                            __AcadgeralAlunoCaptacao.setCamposPersonalizadosValidate('contatoEndereco', regras);
                        }
                    }
                },
                validate: function () {
                    var $form = $(__AcadgeralAlunoCaptacao.options.formElement);
                    __AcadgeralAlunoCaptacao.options.validator.settings.rules = {
                        endCep: {required: false},
                        endLogradouro: {required: true},
                        endNumero: {
                            required: function () {
                                return $form.find('[name=endComplemento]').val() == '';
                            }, number: true
                        },
                        endComplemento: {
                            required: function () {
                                return $form.find('[name=endNumero]').val() == '';
                            }
                        },
                        endBairro: {required: false},
                        endCidade: {required: true},
                        endEstado: {required: true},
                        conContatoTelefone: {telefoneValido: true, required: false},
                        conContatoCelular: {celular: true, required: true},
                        conContatoEmail: {required: true, email: true}
                    };
                    __AcadgeralAlunoCaptacao.options.validator.settings.messages = {
                        endCep: {required: 'Campo obrigatório!'},
                        endLogradouro: {required: 'Campo obrigatório!'},
                        endNumero: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        endComplemento: {required: 'Campo obrigatório!'},
                        endBairro: {required: 'Campo obrigatório!'},
                        endCidade: {required: 'Campo obrigatória!'},
                        endEstado: {required: 'Campo obrigatório!'},
                        conContatoTelefone: {required: 'Campo obrigatório!'},
                        conContatoCelular: {required: 'Campo obrigatório!'},
                        conContatoEmail: {required: 'Campo obrigatório!', email: 'Email inválido!'},
                    };

                    __AcadgeralAlunoCaptacao.setarValidacoesCamposPersonalizados('contatoEndereco');

                    return !$(__AcadgeralAlunoCaptacao.options.formElement).valid();
                }
            },
            responsavel: {
                tratarCamposResponsavel: function () {

                    var arrCampos = ['Vinculo', 'Email', 'Fixo', 'Celular', 'Nome', 'Documento','Naturalidade'],
                        arrCamposFinanceiro = [],
                        arrCamposAcademico = [],
                        campo = '';

                    $.each(arrCampos, function (index, value) {
                        campo = '#academico' + value;
                        $(campo).prop("disabled", true);

                        arrCamposAcademico[index] = campo;

                        campo = '#financeiro' + value;
                        $(campo).prop("disabled", true);

                        arrCamposFinanceiro[index] = campo;
                    });

                    $(".fixo,.celular").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                    });

                    $("#academicoDocumento,#financeiroDocumento").inputmask({
                        showMaskOnHover: false, clearIncomplete: true, mask: ['999.999.999-99', '99.999.999/999-99']
                    });

                    $("[name='respAcademicoInforma']").on("click", function (e) {
                        var val = !$("#respAcademicoInformaAtivo").is(':checked');

                        $.each(arrCamposAcademico, function (index, value) {
                            if (val) {
                                $(value).val('');
                            }

                            $(value).prop("disabled", val);
                        });
                    });

                    $("#financeiroNaturalidade,#academicoNaturalidade").select2({
                        data: [
                            {'id': 'Brasileiro', 'text': 'Brasileiro'},
                            {'id': 'Estrangeiro', 'text': 'Estrangeiro'},
                            {'id': 'Naturalizado', 'text': 'Naturalizado'}
                        ]
                    });

                    var arrVinculos = [
                        {'id': 'Pai', 'text': 'Pai'},
                        {'id': 'Mãe', 'text': 'Mãe'},
                        {'id': 'Avo', 'text': ' Avô(ó)'},
                        {'id': 'Irmão', 'text': 'Irmão'},
                        {'id': 'Responsavel Legal', 'text': 'Responsável'},
                        {'id': 'Outros', 'text': 'Outros'}];

                    $("#academicoVinculo, #financeiroVinculo").select2({data:arrVinculos});


                    $("[name='respFinanceiroInforma']").on("click", function (e) {

                        var val = !$("#respFinanceiroInformaAtivo").is(':checked');

                        $.each(arrCamposFinanceiro, function (index, value) {
                            if (val) {
                                $(value).val('');
                            }

                            $(value).prop("disabled", val);
                        });
                    });
                },
                validate: function () {
                    __AcadgeralAlunoCaptacao.options.validator.settings.rules = {
                        "responsavel[academico][documento]": {
                            required: function () {
                                return $("#respAcademicoInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[academico][nome]": {
                            required: function () {
                                return $("#respAcademicoInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[academico][celular]": {
                            required: function () {
                                return $("#respAcademicoInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[academico][naturalidade]": {
                            required: function () {
                                return $("#respAcademicoInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[academico][emai]": {
                            required: function () {
                                return $("#respAcademicoInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[academico][respVinculo]": {
                            required: function () {
                                return $("#respAcademicoInformaAtivo").is(':checked');
                            }
                        },
                        /*Financeiro*/
                        "responsavel[financeiro][documento]": {
                            required: function () {
                                return $("#respFinanceiroInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[financeiro][nome]": {
                            required: function () {
                                return $("#respFinanceiroInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[financeiro][naturalidade]": {
                            required: function () {
                                return $("#respFinanceiroInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[financeiro][celular]": {
                            required: function () {
                                return $("#respFinanceiroInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[financeiro][email]": {
                            required: function () {
                                return $("#respFinanceiroInformaAtivo").is(':checked');
                            }
                        },
                        "responsavel[financeiro][respVinculo]": {
                            required: function () {
                                return $("#respFinanceiroInformaAtivo").is(':checked');
                            }
                        }
                    };
                    __AcadgeralAlunoCaptacao.options.validator.settings.messages = {
                        "responsavel[academico][documento]": {required: 'Campo obrigatório!'},
                        "responsavel[academico][nome]": {required: 'Campo obrigatório!'},
                        "responsavel[academico][celular]": {required: 'Campo obrigatório!'},
                        "responsavel[financeiro][nome]": {required: 'Campo obrigatório!'},
                        "responsavel[financeiro][documento]": {required: 'Campo obrigatório!'},
                        "responsavel[academico][respVinculo]": {required: 'Campo obrigatório!'},
                        "responsavel[academico][emai]": {required: 'Campo obrigatório!'},
                        "responsavel[academico][naturalidade]": {required: 'Campo obrigatório!'},
                        "responsavel[financeiro][naturalidade]": {required: 'Campo obrigatório!'},
                        "responsavel[financeiro][celular]": {required: 'Campo obrigatório!'},
                        "responsavel[financeiro][email]": {required: 'Campo obrigatório!'},
                        "responsavel[financeiro][respVinculo]": {required: 'Campo obrigatório!'}
                    };

                    return !$(__AcadgeralAlunoCaptacao.options.formElement).valid();
                },
                init: function () {
                    __AcadgeralAlunoCaptacao.steps.responsavel.tratarCamposResponsavel();
                    __AcadgeralAlunoCaptacao.steps.responsavel.validate();
                }
            },
            cursoFormacao: {
                tratamentoCamposFormacao: function () {
                    $("[name=formacaoAno], [name=formacaoDuracao]").inputmask({
                        showMaskOnHover: false, mask: ['9{0,9}']
                    });

                    $('[name=formacaoInstituicao], [name=formacaoCurso]')
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $this = $(this);
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                var arrData = {};
                                var chave = '';

                                if ($element.attr('name') == 'formacaoInstituicao') {
                                    chave = 'instituicao';
                                } else if ($element.attr('name') == 'formacaoCurso') {
                                    chave = 'curso';
                                }

                                arrData[chave] = request.term;

                                $element.data("jqXHR", $.ajax({
                                    url: __AcadgeralAlunoCaptacao.options.url.buscaFormacao,
                                    data: arrData,
                                    type: 'POST',
                                    dataType: "json",
                                    success: function (data) {
                                        var transformed = $.map(data.dados || [], function (el) {
                                            return el[chave];
                                        });

                                        response(transformed);
                                    }
                                }));
                            },
                            minLength: 0
                        });

                    $("[name=formacaoTipo], [name=formacaoTipoEnsino], [name=formacaoRegime], [name=formacaoConcluido]").select2({
                        language: 'pt-BR',
                        minimumResultsForSearch: -1,
                    });

                    $("[name=formacaoTipo]").change(function () {
                        var $el = $(this);

                        if ($el.val() == 'Ensino Médio') {
                            $('[name=formacaoDuracao]').prop('disabled', true).val('3');
                            $('[name=formacaoRegime]').prop('disabled', true).val('Anual');
                            $('[name=formacaoCurso]').prop('disabled', true).val('Ensino Médio');
                        } else {
                            $('[name=formacaoDuracao]').prop('disabled', false).val('');
                            $('[name=formacaoRegime]').prop('disabled', false).val('');
                            $('[name=formacaoCurso]').prop('disabled', false).val('');
                        }
                    });

                    if ($("[name=formacaoTipo]").val() == "" || $("[name=formacaoTipo]").val() == "Ensino Médio") {
                        $("[name=formacaoTipo]").trigger('change');
                    }
                },
                trataModalPagtoObservacao: function () {
                    $('#alteracao-pagto-obs-form').validate({
                        submitHandler: function () {
                            var $modal = $('#modal-pagto-obs');
                            var id = $('#componentId').val();
                            var componente = $("#tipoTitulo").componente();
                            var itens = componente.items();

                            $.each(itens, function (i, item) {
                                var $item = $(item);

                                if ($(item).data('componente.id') == id) {
                                    var $configPgtoCursoObs = $item.find(
                                        '[name="' +
                                        componente.makeFieldName({id: id, name: 'configPgtoCursoObs'}) + '"]'
                                    );
                                    $configPgtoCursoObs.val($('#configPgtoCursoObs').val());
                                }
                            });

                            $modal.modal('hide');
                            return false;
                        },
                        ignore: '.ignore',
                        rules: {
                            configPgtoCursoObs: {required: true}
                        },
                        messages: {
                            configPgtoCursoObs: {required: 'Campo obrigatório!'}
                        }
                    });
                },
                init: function () {
                    __AcadgeralAlunoCaptacao.steps.cursoFormacao.tratamentoCamposFormacao();
                    __AcadgeralAlunoCaptacao.steps.cursoFormacao.trataModalPagtoObservacao();

                    $("#campusCurso").select2({
                        language: 'pt-BR',
                        allowClear: true,
                        ajax: {
                            url: __AcadgeralAlunoCaptacao.options.url.campusCurso,
                            dataType: 'json',
                            delay: 250,
                            type: 'POST',
                            data: function (query) {
                                return {
                                    naoIncluirCampusCurso: (
                                        __AcadgeralAlunoCaptacao.options.value.campusCursoSelecao || []
                                    ),
                                    agruparPorCursocampus: true,
                                    query: query,
                                    agente: __AcadgeralAlunoCaptacao.options.value.agente
                                };
                            },
                            results: function (data) {
                                var transformed = $.map(data, function (el) {
                                    return {text: el.camp_nome + ' / ' + el.curso_nome, id: el.cursocampus_id};
                                });

                                return {results: transformed};
                            }
                        }
                    }).change(function () {
                        if (!$("#campusCurso").val()) {
                            return;
                        }

                        var $form = $(__AcadgeralAlunoCaptacao.options.formElement);
                        __AcadgeralAlunoCaptacao.addOverlay($form, 'Buscando informações de pagamento. Aguarde...');

                        $.ajax({
                            url: __AcadgeralAlunoCaptacao.options.url.tipoTitulo,
                            dataType: 'json',
                            type: 'post',
                            data: {
                                emissaoMatricula: true,
                                cursocampus: $("#campusCurso").select2('val'),
                                prioridade: true
                            },
                            success: function (data) {
                                __AcadgeralAlunoCaptacao.options.data.tipoTitulo = data || [];
                                __AcadgeralAlunoCaptacao.steps.cursoFormacao.carregarTiposTitulo();
                                __AcadgeralAlunoCaptacao.removeOverlay($form);
                            },
                            erro: function () {
                                __AcadgeralAlunoCaptacao.removeOverlay($form);
                            }
                        });
                    });

                    var arrCampos = [];
                    arrCampos.push({name: 'tipotituloId', label: 'tipotituloId', type: 'hidden'});
                    arrCampos.push({name: 'configPgtoCursoId', label: 'configPgtoCursoId', type: 'hidden'});
                    arrCampos.push({name: 'configPgtoCursoValor', label: 'configPgtoCursoValor', type: 'hidden'});
                    arrCampos.push({name: 'configPgtoCursoParcela', label: 'configPgtoCursoParcela', type: 'hidden'});
                    arrCampos.push({name: 'configPgtoCursoAlterado', label: 'configPgtoCursoAlterado', type: 'hidden'});
                    arrCampos.push({name: 'tipotituloMatriculaEmissao', label: 'Emissão Obrigatória', type: 'hidden'});
                    arrCampos.push({
                        name: 'tipotituloNome', label: 'Título',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-5');
                            field.addClass('hidden');
                            field.before(
                                '<p class="form-control-static">' + itemConfig['tipotituloNome'] + '</p>'
                            );
                        }
                    });

                    arrCampos.push({
                        name: 'valoresId', label: 'Pagamento',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            var $item = field.closest('.componente-item');
                            var agente = __AcadgeralAlunoCaptacao.options.value.agente,
                                maxiPagoAtivo = __AcadgeralAlunoCaptacao.options.value.maxiPagoAtivo;

                            if (agente && maxiPagoAtivo) {
                                $parent.addClass('col-sm-2');
                            } else if (agente || maxiPagoAtivo) {
                                $parent.addClass('col-sm-3');
                            } else {
                                $parent.addClass('col-sm-4');
                            }

                            itemConfig['valoresId'] = itemConfig['valoresId'] || null;

                            field.select2({
                                language: 'pt-BR',
                                allowClear: true,
                                minimumResultsForSearch: -1,
                                data: function () {
                                    var encontrou = false;
                                    var arrResults = $.map(itemConfig['financeiroValores'], function (el) {
                                        el.id = el['valoresId'];
                                        el.text = el['valoresParcela'] + ' x ' + el['valoresPreco'];

                                        if (encontrou == false &&
                                            parseInt(el.id) == parseInt(itemConfig['valoresId'])) {
                                            encontrou = true;
                                        }

                                        return el;
                                    });

                                    return {results: arrResults};
                                }
                            });

                            field.select2('val', itemConfig['valoresId'] || null).trigger('change');
                        }
                    });
                    arrCampos.push({
                        name: 'vencimento', label: 'Vencimento',
                        addCallback: function (e) {
                            var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                            var $parent = field.parent();
                            $parent.addClass('col-sm-3');
                            __AcadgeralAlunoCaptacao.iniciarElementoDatePicker(field, {minDate: 0});
                            field.prop("disabled", false);

                            if (!itemConfig['dataVencimentoAlteravel']) {
                                field.prop("disabled", true);
                                field.attr("title", "A configuração do tipo de título \"" + itemConfig['tipotituloNome'] + "\") não permite editar o vencimento!");
                            }

                            if (itemConfig['dataVencimento']) {
                                field.val(itemConfig['dataVencimento']);
                            }
                        }
                    });

                    if (__AcadgeralAlunoCaptacao.options.value.agente) {
                        arrCampos.push({
                            name: 'configPgtoCursoObs', label: 'Obs.',
                            addCallback: function (e) {
                                var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                var $parent = field.parent();
                                $parent.addClass('col-sm-1');
                                field.addClass('hidden');
                                field.after(
                                    '<button type="button" class="form-control btn btn-secondary btn-alterar-obs-pgto">' +
                                    '<span class="fa fa-comment"></span>' +
                                    '</button>'
                                );
                            }
                        });
                    }

                    if (__AcadgeralAlunoCaptacao.options.value.maxiPagoAtivo) {
                        arrCampos.push({
                            name: 'configPgtoCursoCartao', label: 'Usar Cartão?',
                            addCallback: function (e) {

                                var field = e.data.field, itemConfig = e.data.itemSettings, config = e.data.settings;
                                var $parent = field.parent();
                                $parent.addClass('col-sm-1');
                                field[0]['allowClear'] = false;

                                itemConfig['valoresId'] = itemConfig['valoresId'] || null;
                                field.select2({
                                    allowClear: false,
                                    data: [
                                        {'id': 'sim', 'text': 'Sim'},
                                        {'id': 'nao', 'text': 'Não'}
                                    ]
                                }).on("change", function () {
                                    __AcadgeralAlunoCaptacao.habilataCartao();
                                });

                                field.select2('val', 'nao').trigger('change');
                            }
                        });
                    }

                    $("#tipoTitulo").componente({
                        inputName: 'tipoTitulo',
                        classCardExtra: '',
                        labelContainer: '<div class="form-group"/>',
                        tplItem: '\
                        <div>\
                            <div class="componente-content col-sm-12">\
                            </div>\
                        </div>',
                        fieldClass: 'form-control',
                        fields: arrCampos,
                        addCallback: function (e) {
                            var item = e.data.item,
                                itemSettings = e.data.itemSettings,
                                settings = e.data.settings;
                            var id = item.data('componente.id') || '';
                            var valoresIdNome = '[name="' + settings.makeFieldName({id: id, name: 'valoresId'}) + '"]';

                            itemSettings['financeiroValores'] = itemSettings['financeiroValores'] || [];
                        }
                    });

                    __AcadgeralAlunoCaptacao.steps.cursoFormacao.carregarTiposTitulo();

                    $("#campusCurso")
                        .select2("data", __AcadgeralAlunoCaptacao.options.value['campusCurso'])
                        .trigger('change');

                    if (__AcadgeralAlunoCaptacao.options.value['cursocampusId']) {
                        $("#campusCurso")
                            .select2("val", 1 * __AcadgeralAlunoCaptacao.options.value['cursocampusId'])
                            .trigger('change');
                    }

                    $(document).on('click', '.btn-alterar-obs-pgto',
                        __AcadgeralAlunoCaptacao.steps.cursoFormacao.alterarPagamento);
                    $(document).on('change select2-selected select2-clearing select2-removed', 'input, select', function () {
                            var $item = $(this);

                            if ($item.val() != '' && !($item[0]['allowClear'] === false)) {
                                $item.parent().find('.select2-container').addClass('select2-allowclear');
                            } else {
                                $item.parent().find('.select2-container').removeClass('select2-allowclear');
                            }
                        }
                    );

                    $("#cartaoNumero").on("change", function () {
                        __AcadgeralAlunoCaptacao.addOverlay($("#widget-grid"));
                        /*Amex 13 a 16 */
                        $.ajax({
                            url: __AcadgeralAlunoCaptacao.options.url.validaCartao,
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                'cartaoNumero': $("#cartaoNumero").val(),
                                'cartaoCvv': $("#cartaoCvv").val()
                            },
                            success: function (data) {
                                __AcadgeralAlunoCaptacao.removeOverlay($("#widget-grid"));
                                if (data['retorno']['erro']) {
                                    $("#cartaoCvv").val("");
                                    $("#cartaoCvv").prop("disabled", true);

                                    __AcadgeralAlunoCaptacao.showNotificacaoWarning(data['retorno']['erro']);

                                } else if (data['retorno']) {
                                    var cvv = data['retorno']['cvc'];

                                    $("#cartaoCvv").prop("disabled", false);
                                    $("#cartaoCvv").inputmask('Regex', {regex: "^[0-9]{" + cvv + "}"});

                                }
                            }
                        });
                    });

                    $(".submit").on("click", function (e) {
                        __AcadgeralAlunoCaptacao.addOverlay($(__AcadgeralAlunoCaptacao.options.formElement));

                        window.setTimeout(function () {
                            $(__AcadgeralAlunoCaptacao.options.formElement).submit();
                        }, 2000);
                    });
                },
                alterarPagamento: function () {
                    var $componente = $(this).closest('.componente-item');
                    var id = $componente.data('componente.id');
                    var componente = $("#tipoTitulo").componente();

                    var $configPgtoCursoObs = $componente.find(
                        '[name="' + componente.makeFieldName({id: id, name: 'configPgtoCursoObs'}) + '"]'
                    );

                    $('#componentId').val(id);
                    $('#configPgtoCursoObs').val($configPgtoCursoObs.val());
                    $('#modal-pagto-obs').modal('show');
                },
                carregarTiposTitulo: function () {
                    $("#tipoTitulo").componente().removeAll();

                    if (__AcadgeralAlunoCaptacao.options.data.tipoTitulo) {
                        $.each(__AcadgeralAlunoCaptacao.options.data.tipoTitulo || [], function (i, item) {
                            var obrigatorio = 'Opcional';
                            var itemAdd = $.extend({}, item);
                            itemAdd['configPgtoCursoAlterado'] = 'Não';

                            if (__AcadgeralAlunoCaptacao.options.value.agente && itemAdd['tipotituloMatriculaEmissao'] != 'Sim') {
                                obrigatorio = 'Opcional';
                            } else if (itemAdd['tipotituloMatriculaEmissao'] != 'Opcional') {
                                obrigatorio = 'Obrigatório';
                            }

                            itemAdd['tipotituloNome'] = itemAdd['tipotituloNome'] + ' (' + obrigatorio + ')';
                            itemAdd['tipotituloMatriculaEmissao'] = obrigatorio;

                            $.each(__AcadgeralAlunoCaptacao.options.value.tipoTitulo || [], function (i2, item2) {
                                if (item['tipotituloId'] == item2['tipotituloId']) {
                                    itemAdd = $.extend(itemAdd, item2);
                                    itemAdd['vencimento'] = item2['vencimento'];
                                    itemAdd['valoresId'] = item2['valoresId'];
                                }
                            });

                            $('#tipoTitulo').componente(['add', itemAdd]);
                        });
                    }

                    $("#cartaoNomeTitular").inputmask("Regex", {regex: "[a-zA-Z- ]*"});
                    $("#cartaoNumero").inputmask('Regex', {regex: "^[0-9]*"});
                    $("#cartaoVencimento").mask('99/9999');
                    $("#cartaoCvv").inputmask('Regex', {regex: "^[0-9]{3}"});
                    $("#cartaoCvv").prop("disabled", true);

                    new Card({
                        // a selector or DOM element for the form where users will
                        // be entering their information
                        form: '#acadgeral-aluno-form', // *required*
                        // a selector or DOM element for the container
                        // where you want the card to appear
                        container: '.exibeCartao', // *required*

                        formSelectors: {
                            numberInput: '#cartaoNumero', // optional — default input[name="number"]
                            expiryInput: '#cartaoVencimento', // optional — default input[name="expiry"]
                            cvcInput: '#cartaoCvv', // optional — default input[name="cvc"]
                            nameInput: '#cartaoNomeTitular' // optional - defaults input[name="name"]
                        },

                        width: 200, // optional — default 350px
                        formatting: true, // optional - default true

                        // Strings for translation - optional
                        messages: {
                            validDate: 'valid\ndate', // optional - default 'valid\nthru'
                            monthYear: 'mm/yyyy', // optional - default 'month/year'
                        },

                        // Default placeholders for rendered fields - optional
                        placeholders: {
                            number: '•••• •••• •••• ••••',
                            name: 'Full Name',
                            expiry: '••/••',
                            cvc: '•••'
                        },

                        masks: {
                            cardNumber: '•' // optional - mask card number
                        },

                        // if true, will log helpful messages for setting up Card
                        debug: false // optional - default false
                    });

                    if (Object.keys(__AcadgeralAlunoCaptacao.getCamposPersonalizados()).length > 0) {
                        var regras = __AcadgeralAlunoCaptacao.camposPersonalizados('formacao');

                        if (regras) {
                            __AcadgeralAlunoCaptacao.setCamposPersonalizadosValidate('formacao', regras);
                        }

                        regras = __AcadgeralAlunoCaptacao.camposPersonalizados('cursos');

                        if (regras) {
                            __AcadgeralAlunoCaptacao.setCamposPersonalizadosValidate('cursos', regras);
                        }
                    }
                },
                validate: function () {
                    __AcadgeralAlunoCaptacao.options.validator.settings.rules = {
                        formacaoTipo: {required: true},
                        formacaoCurso: {required: true},
                        formacaoInstituicao: {required: true},
                        formacaoConcluido: {required: true},
                        formacaoAno: {required: false, number: true, min: 1900},
                        formacaoTipoEnsino: {required: false},
                        formacaoCidade: {required: false},
                        formacaoEstado: {required: false},
                        formacaoRegime: {required: false},
                        formacaoDuracao: {required: false, number: true},
                        campusCurso: {required: true, number: true},
                        cartaoNumero: {maxlength: 16, minlength: 13}
                    };
                    __AcadgeralAlunoCaptacao.options.validator.settings.messages = {
                        formacaoTipoEnsino: {required: 'Campo obrigatório!'},
                        formacaoTipo: {required: 'Campo obrigatório!'},
                        formacaoInstituicao: {required: 'Campo obrigatório!'},
                        formacaoCidade: {required: 'Campo obrigatório!'},
                        formacaoEstado: {required: 'Campo obrigatório!'},
                        formacaoCurso: {required: 'Campo obrigatório!'},
                        formacaoAno: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        formacaoRegime: {required: 'Campo obrigatório!'},
                        formacaoConcluido: {required: 'Campo obrigatório!'},
                        formacaoDuracao: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        campusCurso: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                        cartaoNumero: {maxlength: 'Maximo 16 digitos!', minlength: 'Minimo 13 digitos!'}
                    };

                    __AcadgeralAlunoCaptacao.setarValidacoesCamposPersonalizados('cursos');
                    __AcadgeralAlunoCaptacao.setarValidacoesCamposPersonalizados('formacao');

                    if (!$(__AcadgeralAlunoCaptacao.options.formElement).valid()) {
                        return true;
                    }

                    var errors = [];
                    var componente = $("#tipoTitulo").componente();
                    var tipoTitulo = componente.items();

                    __AcadgeralAlunoCaptacao.removeErroComponente($("#tipoTitulo"));

                    $.each(tipoTitulo, function (i, item) {
                        var $item = $(item);
                        var id = $(item).data('componente.id');
                        __AcadgeralAlunoCaptacao.removeErroComponente($item);

                        var camposVazios = [];

                        var $eltipotituloNome = $item
                            .find('[name="' + componente.makeFieldName({id: id, name: 'tipotituloNome'}) + '"]');
                        var $elvaloresId = $item
                            .find('[name="' + componente.makeFieldName({id: id, name: 'valoresId'}) + '"]');
                        var $elvencimento = $item
                            .find('[name="' + componente.makeFieldName({id: id, name: 'vencimento'}) + '"]');
                        var $tituloObrigatorio = $item.find('[name="' + componente.makeFieldName({
                                id: id,
                                name: 'tipotituloMatriculaEmissao'
                            }) + '"]');

                        var tipotituloNomeVal = $eltipotituloNome.val();
                        var vencimentoVal = $elvencimento.val();
                        var valoresIdVal = $elvaloresId.val();
                        var tituloObrigatorio = $tituloObrigatorio.val();

                        if (tituloObrigatorio == 'Obrigatório') {
                            if (valoresIdVal == "") {
                                camposVazios.push("Pagamento");
                            }
                        }

                        if (tituloObrigatorio == 'Obrigatório' || valoresIdVal) {
                            if (vencimentoVal == "") {
                                camposVazios.push("Vencimento");
                            } else {

                                var data = new Date();
                                var dtVencimentoVal = __AcadgeralAlunoCaptacao.formatDate(vencimentoVal, 'yy-mm-dd');
                                data = __AcadgeralAlunoCaptacao.formatDate(data, 'yy-mm-dd');
                                dtVencimentoVal = new Date(dtVencimentoVal);
                                data = new Date(data);

                                if (dtVencimentoVal < data) {
                                    errors.push('O' + '&nbsp' + (i + 1) + '&ordm &nbsp' + 'campo de vencimento possui valor inválido ou está vazio!');
                                }
                            }
                        }

                        if (camposVazios.length > 0) {
                            errors.push(
                                'O titulo ' + tipotituloNomeVal +
                                ' possui campos vazios. Por favor preencha os campos: <b>' +
                                camposVazios.join(', ') + '</b>.'
                            );
                            __AcadgeralAlunoCaptacao.adicionaErroComponente($item, true);
                        }
                    });

                    if (errors.length > 0) {
                        __AcadgeralAlunoCaptacao.showNotificacaoDanger(errors.join('<br>'));
                        return true;
                    }

                    return false;
                }
            }
        };

        this.habilataCartao = function () {
            var desabiltar = true;
            var divCartao = $(".pagamentoCartao");

            $.each($("input[name*='configPgtoCursoCartao']"), function (item, item2) {
                if ($(item2).val() == "sim") {
                    desabiltar = false;
                    return 'paraLoop';
                }
            });

            if (!desabiltar && !divCartao.is(":visible")) {
                divCartao.removeClass("hidden");
                __AcadgeralAlunoCaptacao.limparCamposCartao();
            } else if (desabiltar) {
                divCartao.addClass("hidden");
                __AcadgeralAlunoCaptacao.limparCamposCartao();
            }
        };

        this.limparCamposCartao = function () {
            $("#cartaoNomeTitular,#cartaoNumero,#cartaoVencimento,#cartaoCvv").val('');
        };

        this.validaCartao = function () {
            var msgErro = [],
                retorno = true,
                msgAdicional = [];
            __AcadgeralAlunoCaptacao.habilataCartao();

            if ($(".pagamentoCartao").is(":visible")) {
                if (!$("#cartaoNomeTitular").val()) {
                    msgErro.push(" Nome do Titular");
                }

                if (!$("#cartaoNumero").val()) {
                    msgErro.push('Número do Cartão');
                } else {
                    $.ajax({
                        url: __AcadgeralAlunoCaptacao.options.url.validaCartao,
                        type: 'POST',
                        dataType: 'json',
                        async: false,
                        data: {
                            'cartaoNumero': $("#cartaoNumero").val(),
                            'cartaoCvv': $("#cartaoCvv").val()
                        },
                        success: function (data) {
                            if (data['retorno']['erro']) {

                                msgErro.push(data['retorno']['erro']);
                            }
                        }
                    });
                }

                var cartaoVencimento = $("#cartaoVencimento").val();

                if (!cartaoVencimento) {
                    msgErro.push('Vencimento');
                } else {
                    var arrVencimento = ($("#cartaoVencimento").val()).split('/');

                    if (arrVencimento[0] && (arrVencimento[0] > 12 || arrVencimento[0] < 1)) {
                        msgAdicional.push(' Mês de Vencimento Inválido');

                    } else if (!arrVencimento[0]) {
                        msgAdicional.push('Mês de Vencimento');
                    }

                    var arrDataAtual = (__AcadgeralAlunoCaptacao.options.value.data).split("/");

                    if (arrVencimento[1] < arrDataAtual[1] || arrVencimento[1] < 1) {
                        msgAdicional.push('Ano de Vencimento Inválido');

                    } else if (!arrVencimento[1]) {
                        msgAdicional.push('Ano de Vencimento');
                    }

                    if ((parseInt(arrVencimento[0]) + (arrVencimento[1] * 12)) <
                        (parseInt(arrDataAtual[0]) + (arrDataAtual[1] * 12))) {
                        msgAdicional.push('Cartão Vencido');
                    }
                }

                if (!$("#cartaoCvv").val()) {
                    msgErro.push('Código Verificador');
                }
            }

            msgErro = msgErro.join(" | ");

            if (msgErro) {
                __AcadgeralAlunoCaptacao.showNotificacaoInfo("Por favor estes campos:" + msgErro + " do cartão de cŕedito !");
                retorno = false;
            }

            msgAdicional = msgAdicional.join(" | ");

            if (msgAdicional) {
                __AcadgeralAlunoCaptacao.showNotificacaoInfo("Erro no vencimento: " + msgAdicional);
                retorno = false;
            }

            return retorno;
        };

        this.setPesIdAgente = function (pesIdAgente) {
            this.options.value.pesIdAgente = pesIdAgente || null;
        };

        this.getPesIdAgente = function () {
            return this.options.value.pesIdAgente || null;
        };

        this.setUnidade = function (unidade) {
            this.options.value.unidade = unidade || null;
        };

        this.getUnidade = function () {
            return this.options.value.unidade || null;
        };

        this.setOrigem = function (origem) {
            this.options.value.origem = origem || null;
        };

        this.getOrigem = function () {
            return this.options.value.origem || null;
        };

        this.setValidations = function () {
            $(document).on("change", ".form-control:hidden", function () {
                $(__AcadgeralAlunoCaptacao.options.formElement).valid();
            });

            $(__AcadgeralAlunoCaptacao.options.formElement).submit(function (e) {
                __AcadgeralAlunoCaptacao.addOverlay($(__AcadgeralAlunoCaptacao.options.formElement));
                var ok = __AcadgeralAlunoCaptacao.validate();
                var cartao = __AcadgeralAlunoCaptacao.validaCartao();

                if (!ok || !cartao) {
                    __AcadgeralAlunoCaptacao.removeOverlay($(__AcadgeralAlunoCaptacao.options.formElement));

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var componente = $("#tipoTitulo").componente();
                var tipoTitulo = componente.items();

                $.each(tipoTitulo, function (i, item) {
                    var $item = $(item);
                    var id = $(item).data('componente.id');
                    var $valoresId = $item
                        .find('[name="' + componente.makeFieldName({id: id, name: 'valoresId'}) + '"]');
                    var $vencimento = $item
                        .find('[name="' + componente.makeFieldName({id: id, name: 'vencimento'}) + '"]');

                    $($valoresId).select2('enable');
                    $vencimento.prop("disabled", false);
                });

                $('#pesIdAgente').select2('enable');
            });
        };

        this.getUnidade = function () {
            return this.options.value.unidade || null;
        };

        this.setarValidacoesCamposPersonalizados = function (key) {
            key = key || false;

            if (!key) {
                return false;
            }

            var regrasAdicionais = __AcadgeralAlunoCaptacao.getCamposPersonalizadosValidate(key);

            $.each(regrasAdicionais, function (index, value) {
                var campoNome = value['campoNome'] || '';

                if (campoNome) {
                    __AcadgeralAlunoCaptacao.options.validator.settings.rules[campoNome] = value['rules'];
                    __AcadgeralAlunoCaptacao.options.validator.settings.messages[campoNome] = value['msg'];
                }
            });
        };

        this.getCamposPersonalizadosValidate = function (key) {
            key = key || false;
            var arrData = this.options.data.arrCamposPersonalizadosValidate || [];

            if (key) {
                return arrData[key] || [];
            }

            return arrData;
        };

        this.setCamposPersonalizadosValidate = function (key, rules) {
            key = key || false;
            rules = rules || [];

            if (!key) {
                return false;
            }

            this.options.data.arrCamposPersonalizadosValidate[key] = rules;

            return true;
        };

        this.getCamposPersonalizados = function (key) {
            key = key || false;
            var arrData = this.options.data.arrCamposPersonalizados || [];

            if (key) {
                return arrData[key] || [];
            }

            return arrData;
        };

        this.setCamposPersonalizados = function (key, rules) {
            key = key || false;
            rules = rules || [];

            if (!key) {
                return false;
            }

            this.options.data.arrCamposPersonalizados[key] = rules;

            return true;
        };


        /*Tratamento para campos personalizados*/
        this.camposPersonalizados = function (posicao) {
            var arrCampos = __AcadgeralAlunoCaptacao.options.data.arrCamposPersonalizados,
                arrRetorno = [],
                regras = [],
                msg = [],
                tamanho = '',
                idCampo = '',
                campoNome = '',
                nomeCampoFormatado = null;

            try {
                if (arrCampos[posicao]) {
                    $.each(arrCampos[posicao], function (index, value) {

                        regras = {};
                        msg = {};
                        nomeCampoFormatado = null;

                        idCampo = value.campo_id;
                        campoNome = "#campo-" + idCampo;

                        /*Adciona expressão regular*/
                        if (value.campo_expressao_regular) {
                            $(campoNome).inputmask('Regex', {regex: value.campo_expressao_regular});
                        }

                        /*Se for do tipo select, transforma em select2*/
                        if (value.type == 'select') {
                            $(campoNome).select2();
                        }

                        /*Adiciona valor padrao se ele também tiver um tipo*/
                        if (value.campo_valor_padrao && value.type) {
                            if (value.type == 'select') {
                                $(campoNome).select2("val", value.campo_valor_padrao).trigger("change");
                            } else {
                                $(campoNome).val(value.campo_valor_padrao);
                            }
                        }

                        /*Tratamento para o validate do formulario*/
                        if (value.campo_obrigatorio) {
                            regras['required'] = true;
                            msg['required'] = "Campo obrigatório!";
                            nomeCampoFormatado = "camposPersonalizados[" + idCampo + "]";
                        }
                        if (tamanho = parseInt(value.campo_tamanho_max)) {
                            regras['maxlength'] = parseInt(value.campo_tamanho_max);
                            msg['maxlength'] = "Tamanho máximo" + tamanho + "!";
                            nomeCampoFormatado = "camposPersonalizados[" + idCampo + "]";
                        }
                        if (tamanho = parseInt(value.campo_tamanho_min)) {
                            regras['minlength'] = parseInt(value.campo_tamanho_min);
                            msg['minlength'] = "Tamanho máximo" + tamanho + "!";
                            nomeCampoFormatado = "camposPersonalizados[" + idCampo + "]";
                        }

                        /*Caso tenha entrado em alguma if, tera valor para esse variavel*/
                        if (nomeCampoFormatado) {
                            /*Crio uma nova posição no array*/
                            arrRetorno.push({
                                'rules': regras,
                                'msg': msg,
                                'campoNome': nomeCampoFormatado,
                                'id': value.id
                            });
                        }
                    })
                }
            } catch (ex) {
                __AcadgeralAlunoCaptacao.showNotificacaoWarning(ex);
                setTimeout(function () {

                }, 500);
            }
            return arrRetorno;
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.loadSteps();
            this.setValidations();
            this.wizard();
        };

        return this;
    };

    $.acadgeralAlunoCaptacao = function (params) {
        params = params || [];

        var obj = $(window).data("universa.acadgeralAluno.captacao");
        if (!obj) {
            obj = new AcadgeralAlunoCaptacao();
            obj.run(params);
            $(window).data('universa.acadgeralAluno.captacao', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);