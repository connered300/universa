(function ($, window, document) {
    'use strict';

    var AcadgeralDocenteAdd = function () {
        VersaShared.call(this);
        var __acadgeralDocenteAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {pessoaFisica: ''},
            data: {},
            value: {pes: null},
            datatables: {},
            wizardElement: '#acadgeral-docente-wizard',
            formElement: '#acadgeral-docente-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $pes = $("#pes");


            $pes.select2({
                language: 'pt-BR',
                ajax: {
                    url: __acadgeralDocenteAdd.options.url.pessoaFisica,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            var text = [];

                            if (el['pesCpf']) {
                                text.push(el['pesCpf'])
                            }
                            if (el['pesNome']) {
                                text.push(el['pesNome'])
                            }

                            return {id: el.pesId, text: text.join(' - ')};
                        });

                        return {results: transformed};
                    },
                },
                createSearchChoice: function (term) {
                    return {
                        id: term,
                        pesId: '',
                        pesNome: term,
                        text: term + ' (Criar novo registro)'
                    };
                },
                allowClear: true,
                minimumInputLength: 1
            });

            $pes.select2('data', __acadgeralDocenteAdd.options.value.pes || null);

            $('.editarPessoa').click(function (e) {
                var $elemento = $(this).parent().find('>input');
                var id = $elemento.attr('id');
                var data = $elemento.select2('data') || [];

                $('#pessoa-fisica-editar').modal('show');
                var pessoaFisicaAdd = $.pessoaFisicaAdd();
                pessoaFisicaAdd.setCallback('aposSalvar', function (dados, retorno) {
                    var text = [];

                    if (dados['pesCpf']) {
                        text.push(dados['pesCpf'])
                    }
                    if (dados['pesNome']) {
                        text.push(dados['pesNome'])
                    }

                    dados['id'] = dados['pesId'];
                    dados['text'] = text.join(' - ');

                    $elemento.select2('data', dados);

                    $('#pessoa-fisica-editar').modal('hide');
                });
                pessoaFisicaAdd.buscarDadosPessoa($elemento.val());

                $('#pessoa-fisica-editar [name=pesNome]').val(data['pesNome'] || '');

                e.preventDefault();
            });

            $pes.change(function () {
                var data = $(this).select2('data') || [];

                if (Object.keys(data).length > 0) {
                    $(this).next('.editarPessoa').removeClass('hidden');

                    if (!$.isNumeric(data['id']) && data['pesNome']) {
                        $(this).next('.editarPessoa').click();
                    }
                } else {
                    $(this).next('.editarPessoa').addClass('hidden');
                }
            }).trigger('change');


            var $docenteAtivo = $("#docenteAtivo");
            $docenteAtivo.select2({allowClear: true, language: 'pt-BR'}).trigger("change");

            //var $docenteDataFim = $("#docenteDataFim");
            //var $docenteDataInicio = $("#docenteDataInicio");
            //__acadgeralDocenteAdd.iniciarElementoDatePicker($docenteDataInicio);
            //__acadgeralDocenteAdd.iniciarElementoDatePicker($docenteDataFim);

            var $docenteNumMec = $("#docenteNumMec");
            __acadgeralDocenteAdd.inciarElementoInteiro($docenteNumMec);
        };

        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };
        this.setValidations = function () {
            __acadgeralDocenteAdd.options.validator.settings.rules = {
                pes: {number: true, required: true},
                docenteDataInicio: {required: false/*, dateBR: true*/},
                docenteAtivo: {maxlength: 3, required: true},
                docenteDataFim: {required: false/*, dateBR: true*/},
                docenteCurLattes: {maxlength: 255},
                docenteNumMec: {maxlength: 11, number: true},
                docenteTituto: {maxlength: 45}
            };
            __acadgeralDocenteAdd.options.validator.settings.messages = {
                pes: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                docenteDataInicio: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                docenteAtivo: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
                docenteDataFim: {dateBR: 'Informe uma data válida!'},
                docenteCurLattes: {maxlength: 'Tamanho máximo: 255!'},
                docenteNumMec: {maxlength: 'Tamanho máximo: 11!', number: 'Número inválido!'},
                docenteTituto: {maxlength: 'Tamanho máximo: 45!'}
            };

            $(__acadgeralDocenteAdd.options.formElement).submit(function (e) {
                var ok = $(__acadgeralDocenteAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__acadgeralDocenteAdd.options.formElement);

                if (__acadgeralDocenteAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __acadgeralDocenteAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __acadgeralDocenteAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__acadgeralDocenteAdd.options.listagem) {
                                    $.acadgeralDocenteIndex().reloadDataTableAcadgeralDocente();
                                    __acadgeralDocenteAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#acadgeral-docente-modal').modal('hide');
                                }

                                __acadgeralDocenteAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaAcadgeralDocente = function (docente_id, callback) {
            var $form = $(__acadgeralDocenteAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __acadgeralDocenteAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + docente_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __acadgeralDocenteAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __acadgeralDocenteAdd.removeOverlay($form);
                },
                erro: function () {
                    __acadgeralDocenteAdd.showNotificacaoDanger('Erro desconhecido!');
                    __acadgeralDocenteAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.acadgeralDocenteAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-docente.add");

        if (!obj) {
            obj = new AcadgeralDocenteAdd();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-docente.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);