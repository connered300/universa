(function ($, window, document) {
    'use strict';
    var AcadgeralDocenteIndex = function () {
        VersaShared.call(this);
        var __acadgeralDocenteIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                acadgeralDocente: null
            }
        };

        this.setSteps = function () {
            var $dataTableAcadgeralDocente = $('#dataTableAcadgeralDocente');
            var colNum = -1;
            __acadgeralDocenteIndex.options.datatables.acadgeralDocente = $dataTableAcadgeralDocente.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __acadgeralDocenteIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            //{class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __acadgeralDocenteIndex.createBtnGroup(btns);
                        }

                        __acadgeralDocenteIndex.removeOverlay($('#container-acadgeral-docente'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "docente_id", targets: ++colNum, data: "docente_id"},
                    {name: "pes_nome", targets: ++colNum, data: "pes_nome"},
                    {name: "docente_ativo", targets: ++colNum, data: "docente_ativo"},
                    {name: "docente_tituto", targets: ++colNum, data: "docente_tituto"},
                    {name: "docente_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableAcadgeralDocente.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __acadgeralDocenteIndex.options.datatables.acadgeralDocente.fnGetData($pai);

                if (!__acadgeralDocenteIndex.options.ajax) {
                    location.href = __acadgeralDocenteIndex.options.url.edit + '/' + data['docente_id'];
                } else {
                    __acadgeralDocenteIndex.addOverlay(
                        $('#container-acadgeral-docente'), 'Aguarde, carregando dados para edição...'
                    );
                    $.acadgeralDocenteAdd().pesquisaAcadgeralDocente(
                        data['docente_id'],
                        function (data) {
                            __acadgeralDocenteIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-acadgeral-docente-add').click(function (e) {
                if (__acadgeralDocenteIndex.options.ajax) {
                    __acadgeralDocenteIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableAcadgeralDocente.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __acadgeralDocenteIndex.options.datatables.acadgeralDocente.fnGetData($pai);
                var arrRemover = {
                    docenteId: data['docente_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de docente "' + data['pes_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __acadgeralDocenteIndex.addOverlay(
                                $('#container-acadgeral-docente'), 'Aguarde, solicitando remoção de registro de docente...'
                            );
                            $.ajax({
                                url: __acadgeralDocenteIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __acadgeralDocenteIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de docente:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __acadgeralDocenteIndex.removeOverlay($('#container-acadgeral-docente'));
                                    } else {
                                        __acadgeralDocenteIndex.reloadDataTableAcadgeralDocente();
                                        __acadgeralDocenteIndex.showNotificacaoSuccess(
                                            "Registro de docente removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#acadgeral-docente-form'),
                $modal = $('#acadgeral-docente-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['docenteId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['docenteId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $pes = $("#pes");
            $pes.select2('data', data['pes']).trigger("change");

            var $docenteAtivo = $("#docenteAtivo");
            $docenteAtivo.select2('val', data['docenteAtivo']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' docente');
            $modal.modal();
            __acadgeralDocenteIndex.removeOverlay($('#container-acadgeral-docente'));
        };

        this.reloadDataTableAcadgeralDocente = function () {
            this.getDataTableAcadgeralDocente().api().ajax.reload();
        };

        this.getDataTableAcadgeralDocente = function () {
            if (!__acadgeralDocenteIndex.options.datatables.acadgeralDocente) {
                if (!$.fn.dataTable.isDataTable('#dataTableAcadgeralDocente')) {
                    __acadgeralDocenteIndex.options.datatables.acadgeralDocente = $('#dataTableAcadgeralDocente').DataTable();
                } else {
                    __acadgeralDocenteIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __acadgeralDocenteIndex.options.datatables.acadgeralDocente;
        };

        this.run = function (opts) {
            __acadgeralDocenteIndex.setDefaults(opts);
            __acadgeralDocenteIndex.setSteps();
        };
    };

    $.acadgeralDocenteIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-docente.index");

        if (!obj) {
            obj = new AcadgeralDocenteIndex();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-docente.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);