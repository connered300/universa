(function ($, window, document) {
    'use strict';

    var AcadgeralDisciplinaIndex = function () {
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            options: {
                urlSearch: '',
                urlEdit: '',
                urlRemove: ''
            }
        };

        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);
        };

        priv.addOverlay = function (el, mensagem) {
            mensagem = mensagem || 'Aguarde...';
            el.append('<div class="app-overlay">' + mensagem + '</div>');
        };

        priv.removeOverlay = function (el) {
            el.find('>.app-overlay').remove();
        };

        priv.setSteps = function () {
            var colNum = 0;
            priv.dataTableAcadgeralDisciplina = $('#dataTableAcadgeralDisciplina').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlSearch,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary acadgeralDisciplina-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                            </div>\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-danger acadgeralDisciplina-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            var urlEdit = priv.options.urlEdit + '/' + data[row]['disc_id'];

                            data[row]["disc_id_value"] = data[row]['disc_id'];
                            data[row]['acao'] = btnGroup;
                            data[row]['disc_id'] = "<a href='" + urlEdit + "'>" + data[row]['disc_id'] + "</a>";
                            data[row]['disc_nome'] = "<a href='" + urlEdit + "'>" + data[row]['disc_nome'] + "</a>";
                            data[row]['disc_sigla'] = "<a href='" + urlEdit + "'>" + data[row]['disc_sigla'] + "</a>";
                            data[row]["urlEdit"] = urlEdit;
                        }
                        priv.removeOverlay($('.widget-body'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "disc_id", targets: colNum++, data: "disc_id"},
                    {name: "disc_nome", targets: colNum++, data: "disc_nome"},
                    {name: "disc_sigla", targets: colNum++, data: "disc_sigla"},
                    {name: "disc_id", targets: colNum++, data: "acao"}
                ],
                "dom": "<'dt-toolbar'<'col-xs-12'f>r>" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "_MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableAcadgeralDisciplina').on('click', '.acadgeralDisciplina-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = priv.dataTableAcadgeralDisciplina.fnGetData($pai);
                var arrRemover = {
                    discId: data['disc_id_value']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de disciplina "' + data['disc_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        priv.addOverlay($('.widget-body'),
                                        'Aguarde, solicitando remoção de registro de disciplina...');
                        $.ajax({
                            url: priv.options.urlRemove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    $.smallBox({
                                        title: "Não foi possível remover registro de disciplina:",
                                        content: "<i>" + data['erroDescricao'] + "</i>",
                                        color: "#C46A69",
                                        icon: "fa fa-times",
                                        timeout: 10000
                                    });

                                    priv.removeOverlay($('.widget-body'));
                                } else {
                                    priv.dataTableAcadgeralDisciplina.api().ajax.reload(null, false);
                                    $.smallBox({
                                        title: "Sucesso!",
                                        content: "<i>Registro de disciplina removido!</i>",
                                        color: "#739e73",
                                        icon: "fa fa-check-circle",
                                        timeout: 10000
                                    });
                                }
                            }
                        });
                    }
                });

                e.preventDefault();
            });

            $(document).on("click", ".acadgeralDisciplina-edit", function(){
                var $pai = $(this).closest('tr');
                var data = priv.dataTableAcadgeralDisciplina.fnGetData($pai);
                location.href = data["urlEdit"];
            });
        };

        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
        };
    };

    $.acadgeralDisciplinaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-disciplina.index");

        if (!obj) {
            obj = new AcadgeralDisciplinaIndex();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-disciplina.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);