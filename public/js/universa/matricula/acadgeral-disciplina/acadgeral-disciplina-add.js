(function ($, window, document) {
    'use strict';

    var AcadgeralDisciplinaAdd = function () {
        VersaShared.call(this);
        var pub = this;
        var priv = {
            steps: [],
            validations: [],
            datatables: {
                dataTableAcadgeralDisciplinaCursos: null,
            },
            values: {
                acao: "",
                cursosSelected: null,
                tipoDisciplinaSelected: null,
            },
            options: {
                urlPesquisaDisciplinaCursos: '',
                urlPesquisaCursos: '',
                urlPesquisaDisciplinaCursosDatatable: '',
                urlPesquisaDocente: '',
                urlDisciplinaAdd: '',
                urlDisciplinaCursosEdit: '',
                urlDisciplinaCursosAdd: '',
                urlDisciplinaCursosRemove: '',
                discId: '',
            }
        };
        priv.setDefaults = function (opts) {
            opts = opts || [];
            priv.options = $.extend(priv.options, opts);

            $.validator.setDefaults({
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.parent('label').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                }
            });
        };
        priv.setSteps = function () {
            $("#doc_padrao").select2({
                language: 'pt-BR',
                ajax: {
                    url: priv.options.urlPesquisaDocente,
                    dataType: 'json',
                    delay: 250,
                    data: function (q) {
                        return {q: q};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            return {text: el.pes_nome, id: el.docente_id};
                        });

                        return {results: transformed};
                    },
                },
                minimumInputLength: 1,
            });

            $("#cursos").select2({
                language: 'pt-BR',
                ajax: {
                    url: priv.options.urlPesquisaCursos,
                    dataType: 'json',
                    delay: 250,
                    data: function (q) {
                        return {q: q};
                    },
                    results: function (data) {
                        var todasRowDataTable = priv.datatables.dataTableAcadgeralDisciplinaCursos.api().rows().data();
                        var transformed = $.map(data, function (el) {
                            var exists = false;
                            todasRowDataTable.each( function( dataRow, rowNumber ) {
                                if(dataRow.curso_id == el.cursoId){
                                    exists = true;
                                    return;
                                }
                            });
                            return exists ? null : {text: el.cursoNome, id: el.cursoId} ;
                        });
                        return {results: transformed};
                    },
                },
                minimumInputLength: 1,
            }).change(function (e) {

                //eliminando os casos de trigger sem valor
                var cursoId = (priv.values.cursosSelected ? priv.values.cursosSelected.id : e.added.id)

                $("#tdiscId").prop("disabled", false);
                $("#coRequisito, #preRequisito, #equivalencia").select2({
                    language: 'pt-BR',
                    ajax: {
                        url: priv.options.urlPesquisaDisciplinaCursos,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            var arrOptionsDiscInvalid = $.merge(
                                [],
                                $.merge(
                                    $("#coRequisito").select2('val'),
                                    $.merge(
                                        $("#preRequisito").select2('val'),
                                        $("#equivalencia").select2('val')
                                    )
                                )
                            );
                            return {query: query, cursoId: cursoId, optionsInvalid: arrOptionsDiscInvalid};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                return {text: el.disc_nome, id: el.disc_curso_id};
                            });

                            return {results: transformed};
                        },
                    },
                    minimumInputLength: 1,
                });
            });

            $("#tdiscId").select2({language: 'pt-BR'}).on("change",function(e){
                var selecao = e.added;
                if(!e.added ){
                    selecao = priv.values.tipoDisciplinaSelected;
                }
                $("#coRequisito, #preRequisito, #equivalencia").prop("disabled", true);
                $("#coRequisito, #preRequisito, #equivalencia").select2("enable", false).trigger("change");
                if(selecao && $.trim(selecao.text) == "Equivalência"){
                    $("#equivalencia").prop("disabled", false);
                    $("#equivalencia").select2("enable", true).trigger("change");
                } else if(selecao && $.trim(selecao.text) != "Base Comum"){
                    $("#coRequisito, #preRequisito, #equivalencia").prop("disabled", false);
                    $("#coRequisito, #preRequisito, #equivalencia").select2("enable", true).trigger("change");
                }
            });

            $(document).on('click', "#btn-add-curso",function (e) {
                priv.values.acao = 'add';

                if($("#acadgeral-disciplina-form").valid()){
                    priv.openModal();
                }

                e.stopImmediatePropagation();
                e.preventDefault();
                return false;
            });
            priv.setDatatable(priv.options.discId);

        };
        priv.openModal = function (dados) {
            var textAcao = "Adicionar";

            //limpando campos
            priv.values.cursosSelected = "";
            priv.values.tipoDisciplinaSelected = "";

            $("#coRequisito, #preRequisito, #tdiscId, #equivalencia").prop("disabled", true);
            $("#coRequisito, #preRequisito, #tdiscId, #equivalencia").select2("enable", false).trigger("change");


            if(dados){
                //colocando os valores nos campos para o caso de edit
                if(dados.cursosSelected){
                    priv.values.cursosSelected = dados.cursosSelected;
                    $("#cursos").select2('data', dados.cursosSelected).trigger("change");
                    textAcao = "Editar";
                }

                if(dados.tipoDisciplinaSelected){
                    priv.values.tipoDisciplinaSelected = dados.tipoDisciplinaSelected;
                    $("#tdiscId").select2('val', parseInt(dados.tipoDisciplinaSelected.id)).trigger("change");
                }

                if(dados.coRequisitosSelected){
                    $("#coRequisito").select2('data', dados.coRequisitosSelected);
                }

                if(dados.preRequisitosSelected){
                    $("#preRequisito").select2('data', dados.preRequisitosSelected);
                }

                if(dados.equivalenciasSelected){
                    $("#equivalencia").select2('data', dados.equivalenciasSelected);
                }
            } else{
                $("#cursos, #tdiscId, #coRequisito, #preRequisito, #equivalencia").select2("data", "");
            }

            $("[name='disc_ativo'][value='"+(dados && dados.discAtivo || "Sim")+"']").prop("checked", true)

            $(".modal-title").text(textAcao+ " cursos a disciplina") ;
            $("#btn-enviar-curso").text(textAcao) ;

            $('#modal-disc-curso').modal('show');

            $(".close").click(function (e) {
                $('#modal-disc-curso').modal('hide');
                e.stopImmediatePropagation();
                e.preventDefault();
            });

            $("#btn-enviar-curso").click(function (e) {
                $("#form-modal").submit()
                //evitando double event
                e.stopImmediatePropagation();
                e.preventDefault();
            });
        };
        priv.setDatatable = function (discId) {
            var colNum = 0;
            var dataTableAux = $('#dataTableAcadgeralDisciplinaCursos').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: priv.options.urlPesquisaDisciplinaCursosDatatable,
                    type: "POST",
                    data: function(data){
                        data["discId"] = discId;
                        return data;
                    },
                    dataSrc: function (json) {
                        var data = json.data;
                        for (var row in data) {
                            var cursoId = data[row]['curso_id'];
                            var btns = [{type: "button", class: "btn btn-xs btn-primary edit", icon: 'fa-pencil fa-inverse'},
                                        {type: "button", class: "btn btn-xs btn-danger remove",icon: 'fa-times fa-inverse'}];

                            data[row]['acao'] = pub.createBtnGroup(btns);
                        }
                        return data;
                    }
                },
                columnDefs: [
                    {name: "curso_nome", targets: colNum++, data: "curso_nome"},
                    {name: "tdisc_descricao", targets: colNum++, data: "tdisc_descricao"},
                    {name: "disc_ativo", targets: colNum++, data: "disc_ativo"},
                    {name: "curso_id", targets: colNum++, data: "acao"}
                ],
                "dom": "<'dt-toolbar'<'col-xs-6 botaoDisc'><'col-xs-6'f>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "_MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']],
            });

            priv.datatables.dataTableAcadgeralDisciplinaCursos = dataTableAux;
            priv.editarRemover();

            $('.botaoDisc').html('<button type="button" class="btn btn-primary" id="btn-add-curso">Adicionar</button>');
        };
        //bind feito apenas quando estou criando uma disciplina com os vinculos de curso
        priv.editarRemover = function(){
            var datatable = priv.datatables.dataTableAcadgeralDisciplinaCursos;

            var editButtonEvent = function (e) {
                //pegando pelo button o id
                var $pai = $(this).closest('tr');
                var data = priv.datatables.dataTableAcadgeralDisciplinaCursos.fnGetData($pai);
                var dados = [];
                var coRequisito = "";
                var preRequisito = "";
                var equivalencia = "";

                dados['cursosSelected'] = {id: data['curso_id'], text: data["curso_nome"]};
                dados['tipoDisciplinaSelected'] = {id: data["tdisc_id"], text: data["tdisc_descricao"]};
                dados['discAtivo'] = data["disc_ativo"];

                if(data["disc_co_requisito"]){
                    coRequisito  = {id: data['disc_co_requisito'], text: data['disc_co_requisito_nome']};
                }
                if(data["disc_pre_requisito"]){
                    preRequisito = {id: data['disc_pre_requisito'], text: data['disc_pre_requisito_nome']};
                }
                if(data["disc_equivalencia"]){
                    equivalencia = {id: data['disc_equivalencia'], text: data['disc_equivalencia_nome']};
                }

                dados['coRequisitosSelected']  = coRequisito;
                dados['preRequisitosSelected'] = preRequisito;
                dados['equivalenciasSelected'] = equivalencia;

                priv.values.acao = 'edit';
                priv.openModal(dados);
                e.stopImmediatePropagation();
                e.preventDefault();
            };
            var removeButtonEvent = function (e) {

                var $pai = $(this).closest('tr');
                var data = priv.datatables.dataTableAcadgeralDisciplinaCursos.fnGetData($pai);

                priv.values.acao = 'remove';

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover o registro do curso de ' + data['curso_nome'] + ' nesta disciplina "?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim"){
                        priv.submitAjax([{name: "curso", value: data['curso_id']}]);
                    }
                    else{
                        pub.removeOverlay($('#content'));
                    }
                });

                e.stopImmediatePropagation();
                e.preventDefault();
            };
            $(document).on("click", "button.edit", editButtonEvent);
            $(document).on("click", "button.remove", removeButtonEvent);
        },
        //usado quando estou com o datatable com ajax
        priv.atualizaDatatables = function () {
            priv.datatables.dataTableAcadgeralDisciplinaCursos.api().ajax.reload();
        },
        priv.submitAjax = function(dados){
            var urlDiscCurso = "";
            var msgSuccess = "";
            pub.addOverlay($('#content'), 'Verificando o registro. Aguarde...');

            //aqui uso o campo discId ao invés da variável contida no priv, pois eu posso remover
            //um curso disciplina enquanto ainda estou apenas adicionando a disciplina e não só na
            //edição, o campo é automaticamente alimentado na adição de disciplina no momento que
            // faço o vínculo do primeiro curso, na edição, o valor já está contido no campo escondido
            dados.push({name: "disc_id", value: $("#discId").val()});

            if(priv.values.acao == "edit"){
                urlDiscCurso = priv.options.urlDisciplinaCursosEdit;
                msgSuccess = "Registro editado!";
            } else if(priv.values.acao == "add"){
                urlDiscCurso = priv.options.urlDisciplinaCursosAdd;
                msgSuccess = "Registro salvo!";
                dados = dados.concat($("#acadgeral-disciplina-form").serializeArray());
            } else if(priv.values.acao == "remove"){
                urlDiscCurso = priv.options.urlDisciplinaCursosRemove;
                msgSuccess = "Registro removido!";
            }

            $.ajax({
                url: urlDiscCurso,
                type: 'POST',
                dataType: 'json',
                data: dados,
                async: false,
                success: function (data) {
                    if (data.erro && data.erro['mensagem'] && ($.isArray(data.erro['mensagem']) || typeof data.erro['mensagem'] == "object") ) {
                        $.each(data.erro['mensagem'], function (index, msg) {
                            pub.showNotificacaoDanger(
                                msg || "Não foi possível salvar registro!"
                            );
                        });
                    } else if (data.erro && data.erro['mensagem'] && typeof data.erro['mensagem'] == "string") {
                        pub.showNotificacaoDanger(
                            data.erro['mensagem'] || "Não foi possível salvar registro"
                        );
                    }

                    if (data.success) {
                        pub.showNotificacaoSuccess(msgSuccess);

                        if(priv.values.acao == "add"){
                            $("[name='discId']").val(data.disc);
                            priv.datatables.dataTableAcadgeralDisciplinaCursos.api().destroy();
                            priv.setDatatable(data.disc);
                        }
                    }

                    priv.atualizaDatatables();
                }
            });
            pub.removeOverlay($('#content'));
            return false;
        },
        priv.setValidationsModal = function () {
            var submitHandler = function (form) {
                $(".close").click();
                priv.submitAjax($(form).serializeArray());
                return false;
            };
            $("#form-modal").validate({
                submitHandler: submitHandler,
                rules: {
                    tdisc_id: {maxlength: 10, number: true, required: true},
                    disc_ativo: {maxlength: 3, required: true},
                    curso_id: {required: true},
                },
                messages: {
                    tdisc_id: {
                        maxlength: 'Tamanho máximo: 10!',
                        number: 'Número inválido!',
                        required: 'Campo obrigatório!'
                    },
                    disc_ativo: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
                    curso_id: {required: "Selecione ao menos um curso!"},
                },
                ignore: '.ignore'
            });
        };
        priv.setValidations = function () {
            var submitHandler = function (form) {
                form.submit();
            };

            $("#acadgeral-disciplina-form").validate({
                submitHandler: submitHandler,
                rules: {
                    discNome: {maxlength: 128, required: true},
                    discSigla: {maxlength: 45, required: true},
                },
                messages: {
                    discNome: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                    discSigla: {maxlength: 'Tamanho máximo: 45!', required: 'Campo obrigatório!'},
                },
                ignore: '.ignore'
            });
        };
        pub.run = function (opts) {
            priv.setDefaults(opts);
            priv.setSteps();
            priv.setValidations();
            priv.setValidationsModal();
        };
    };

    $.acadgeralDisciplinaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.matricula.acadgeral-disciplina.add");

        if (!obj) {
            obj = new AcadgeralDisciplinaAdd();
            obj.run(params);
            $(window).data('universa.matricula.acadgeral-disciplina.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);