(function ($, window, document) {
    'use strict';

    var CrmLeadAtendimento = function () {
        VersaShared.call(this);
        var __crmLeadAtendimento = this;
        this.defaults = {
            url: {
                crmAtividadeTipo: '',
                crmInteresse: '',
                crmLeadAtualizar: '',
                crmLeadConverter: '',
                crmOperadorChamar: '',
                crmLeadAtividadeAdd: '',
                atividadesDatatables: ''
            },
            data: {},
            ajaxSubmit: false,
            value: {
                pes: '-1',
                operador: null,
                click2CallAtivo: false
            },
            callback: {},
            datatables: {
                atividades: null
            },
            formElement: '#crm-lead-atendimento-form',
            validator: null
        };

        this.atividades = {
            iniciarCampos: function () {
                var $form = $('#crm-lead-atividade-form');

                var $atividadeTipo = $form.find("[name=atividadeTipo]"),
                    $atividadeFormaContato = $form.find("[name=atividadeFormaContato]"),
                    $atividadeAgendamento = $form.find("[name=atividadeAgendamento]"),
                    $atividadeFormaContatoAgendamento = $form.find("[name=atividadeFormaContatoAgendamento]");

                $atividadeTipo.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __crmLeadAtendimento.options.url.crmAtividadeTipo,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query, apenasFolhas: true};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.atividade_tipo_id;
                                el.text = el.atividade_tipo_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                }).change(function () {
                    var atividadeTipo = $atividadeTipo.select2('data') || {};

                    var comentarioObrigatorio = atividadeTipo['atividade_comentario_obrigatorio'] || 'Não',
                        caracterizaDesistencia = atividadeTipo['atividade_caracteriza_desistencia'] || 'Não',
                        agendamento = atividadeTipo['atividade_agendamento'] || 'Indisponível';

                    var $leadAgendamento = $('#leadAgendamento');
                    var $leadDesistencia = $('#leadDesistencia');

                    $leadAgendamento.hide();
                    $leadDesistencia.hide();

                    if (agendamento != 'Indisponível') {
                        $leadAgendamento.show();
                    }

                    if (caracterizaDesistencia != 'Não') {
                        $leadDesistencia.show();
                    }


                });

                $atividadeFormaContato.select2({
                    minimumResultsForSearch: -1, allowClear: true, language: 'pt-BR'
                });

                if ($atividadeFormaContato.val() == "") {
                    $atividadeFormaContato.val('Outros').trigger('change');
                }

                __crmLeadAtendimento.iniciarElementoDateTimePicker($atividadeAgendamento, {minDate: 0});

                $atividadeFormaContatoAgendamento.select2({
                    minimumResultsForSearch: -1, allowClear: true, language: 'pt-BR'
                });

                if ($atividadeFormaContatoAgendamento.val() == "") {
                    $atividadeFormaContatoAgendamento.val('Outros').trigger('change');
                }

                var campoDesistenciaObrigatorio = function () {
                    var atividadeTipo = $atividadeTipo.select2('data') || {};
                    var caracterizaDesistencia = atividadeTipo['atividade_caracteriza_desistencia'] || 'Não';

                    return (caracterizaDesistencia != 'Não');
                };

                var campoComentarioObrigatorio = function () {
                    var atividadeTipo = $atividadeTipo.select2('data') || {};
                    var comentarioObrigatorio = atividadeTipo['atividade_comentario_obrigatorio'] || 'Não';

                    return comentarioObrigatorio != 'Não';
                };

                var campoAgendamentoObrigatorio = function () {
                    var atividadeTipo = $atividadeTipo.select2('data') || {};
                    var agendamento = atividadeTipo['atividade_agendamento'] || 'Indisponível';

                    return (agendamento == 'Obrigatório');
                };

                $form.validate({
                    submitHandler: function () {
                        __crmLeadAtendimento.atividades.salvar();

                        return false;
                    },
                    rules: {
                        atividadeFormaContato: {required: true},
                        atividadeTipo: {required: true},
                        atividadeAgendamento: {required: campoAgendamentoObrigatorio},
                        atividadeFormaContatoAgendamento: {required: campoAgendamentoObrigatorio},
                        atividadeDesistencia: {required: campoDesistenciaObrigatorio},
                        atividadeComentario: {required: campoComentarioObrigatorio}
                    },
                    messages: {
                        atividadeTipo: {required: 'Campo obrigatório!'},
                        atividadeFormaContato: {required: 'Campo obrigatório!'},
                        atividadeAgendamento: {required: 'Campo obrigatório!'},
                        atividadeFormaContatoAgendamento: {required: 'Campo obrigatório!'},
                        atividadeDesistencia: {required: 'Campo obrigatório!'},
                        atividadeComentario: {required: 'Campo obrigatório!'}
                    },
                    ignore: '.ignore'
                });
            },
            salvar: function () {
                var $form = $('#crm-lead-atividade-form');
                var $formLead = $(__crmLeadAtendimento.options.formElement);
                var $modal = $('#crm-lead-atividade-modal');

                var arrDados = $form.serializeJSON();
                arrDados['ajax'] = true;

                __crmLeadAtendimento.addOverlay($modal.find('.modal-content'), 'Salvando atividade. Aguarde...');

                $.ajax({
                    url: __crmLeadAtendimento.options.url.crmLeadAtividadeAdd,
                    type: 'POST',
                    dataType: 'json',
                    data: arrDados,
                    success: function (data) {
                        var retorno = data['arrDados'] || [];
                        var lead = retorno['pes'] || [];

                        if (data.erro) {
                            __crmLeadAtendimento.showNotificacaoDanger(
                                data['mensagem'] || 'Não foi possível salvar registro.'
                            );
                        } else {
                            __crmLeadAtendimento.showNotificacaoSuccess(
                                data['mensagem'] || 'Registro salvo com sucesso!'
                            );

                            $formLead.find('.btn-lead-converter').hide();
                            $formLead.find('.btn-lead-aluno').hide();
                            $formLead.find('.leadStatus').html(lead['leadStatus'] || '-');

                            if (lead['leadStatus'] == 'Convertido') {
                                if (arrDados['alunoId']) {
                                    $formLead.find('.btn-lead-aluno').show();
                                } else {
                                    $formLead.find('.btn-lead-converter').show();
                                }
                            }

                            $modal.modal('hide');
                            __crmLeadAtendimento.atividades.atualizarDatatables();
                            $.crmLeadIndex().reloadDataTableCrmLead();
                        }

                        __crmLeadAtendimento.removeOverlay($modal);
                    }
                });
            },
            atualizarDatatables: function () {
                __crmLeadAtendimento.options.datatables.atividades.api().ajax.reload(null, false);
            },
            iniciarDatatables: function () {
                var $form = $(__crmLeadAtendimento.options.formElement);
                var $datatables = $('#dataTableAtendimentoAtividade');
                var $containerDatatables = $datatables.closest('div');
                var colNum = -1;

                __crmLeadAtendimento.options.datatables.atividades = $datatables.dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __crmLeadAtendimento.options.url.atividadesDatatables,
                        type: "POST",
                        data: function (d) {
                            d.filter = {};
                            d.filter['pes'] = __crmLeadAtendimento.getPes();
                            d.index = true;

                            __crmLeadAtendimento.addOverlay($containerDatatables, 'Aguarde, carregando atividades do lead.');

                            return d;
                        },
                        dataSrc: function (json) {
                            var data = json.data;
                            var btns = [{class: 'btn-primary item-view', icon: 'fa-eye'}];

                            for (var row in data) {
                                data[row]['acao'] = __crmLeadAtendimento.createBtnGroup(btns);
                                data[row]['atividade_data_cadastro_formatada'] = (
                                    __crmLeadAtendimento.formatDateTime(data[row]['atividade_data_cadastro'])
                                );
                                data[row]['atividade_agendamento_formatada'] = (
                                    __crmLeadAtendimento.formatDateTime(data[row]['atividade_agendamento'])
                                );
                            }

                            __crmLeadAtendimento.removeOverlay($containerDatatables);

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "atividade_data_cadastro", targets: ++colNum, data: "atividade_data_cadastro_formatada"},
                        {name: "operador_nome", targets: ++colNum, data: "operador_nome"},
                        {name: "atividade_tipo_nome", targets: ++colNum, data: "atividade_tipo_nome"},
                        {name: "atividade_forma_contato", targets: ++colNum, data: "atividade_forma_contato"},
                        {name: "atividade_agendamento", targets: ++colNum, data: "atividade_agendamento_formatada"},
                        {name: "atividadeId", targets: ++colNum, data: "acao"}
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'desc']]
                });

                $datatables.on('click', '.item-view', function () {
                    var $pai = $(this).closest('tr');
                    var data = __crmLeadAtendimento.options.datatables.atividades.fnGetData($pai);
                    __crmLeadAtendimento.atividades.visualizar(data);
                });

                $form.on('click', '.btn-atividade-add', function () {
                    __crmLeadAtendimento.atividades.lancar();
                });
            },
            visualizar: function (arrDados) {
                var $modal = $('#crm-lead-atividade-visualizar-modal');
                var arrCampos = [
                    'pes_id',
                    'pes_nome',
                    'pes_cpf',
                    'atividade_tipo_nome',
                    'lead_data_cadastro',
                    'operador_nome',
                    'atividade_data_cadastro',
                    'atividade_forma_contato',
                    'atividade_agendamento',
                    'atividade_forma_contato_agendamento',
                    'atividade_comentario',
                    'atividade_desistencia'
                ];

                arrDados = arrDados || [];

                $.each(arrCampos, function (index, campo) {
                    var valor = arrDados[campo] || '-';

                    if (campo.indexOf('data') != -1 || campo == "atividade_agendamento") {
                        valor = __crmLeadAtendimento.formatDateTime(valor);
                    }

                    $modal.find('.' + campo).html(valor);
                });

                $modal.modal('show');
            },
            lancar: function (arrDados) {
                arrDados = arrDados || {};
                arrDados['GUID'] = arrDados['GUID'] || __crmLeadAtendimento.createGUID();
                arrDados['atividadeTipo'] = arrDados['atividadeTipo'] || '';
                arrDados['pesId'] = arrDados['pesId'] || __crmLeadAtendimento.getPes();
                arrDados['atividadeFormaContato'] = arrDados['atividadeFormaContato'] || '';
                arrDados['atividadeDataCadastro'] = __crmLeadAtendimento.formatDateTime(arrDados['atividadeDataCadastro'] || new Date());
                arrDados['atividadeAgendamento'] = __crmLeadAtendimento.formatDateTime(arrDados['atividadeAgendamento'] || '');
                arrDados['atividadeFormaContatoAgendamento'] = arrDados['atividadeFormaContatoAgendamento'] || '';

                var $form = $('#crm-lead-atividade-form');

                arrDados['atividadeDesistencia'] = arrDados['atividadeDesistencia'] || 'Não';
                arrDados['atividadeComentario'] = arrDados['atividadeComentario'] || '';

                $form.deserialize(arrDados);

                $form.find('.operadorLogin').html(arrDados['operadorLogin'] || '-');
                $form.find('.atividadeDataCadastro').html(arrDados['atividadeDataCadastro'] || '-');
                $form.find('.atividadeSituacao').html(arrDados['atividadeSituacao'] || '-');

                var arrAtividadeTipo = null;

                if (arrDados['atividadeTipoId']) {
                    arrAtividadeTipo = {
                        id: arrDados['atividadeTipoId'] || '',
                        text: arrDados['atividadeTipoNome'] || ''
                    };
                }

                $form
                    .find("[name=atividadeTipo]")
                    .select2('data', arrAtividadeTipo)
                    .trigger('change');
                $form
                    .find("[name=atividadeFormaContato]")
                    .select2('val', arrDados['atividadeFormaContato'] || 'Outro')
                    .trigger('change');
                $form
                    .find("[name=atividadeFormaContatoAgendamento]")
                    .select2('val', arrDados['atividadeFormaContatoAgendamento'] || 'Outro')
                    .trigger('change');

                $('#crm-lead-atividade-modal').modal('show');
            }
        };


        this.lead = {
            iniciarCampos: function () {
                var $form = $(__crmLeadAtendimento.options.formElement);
                var $interesse = $form.find('[name=interesse]');

                __crmLeadAtendimento.lead.tratarBotaoChamar();

                $interesse.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    multiple: true,
                    ajax: {
                        url: __crmLeadAtendimento.options.url.crmInteresse,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.interesse_id;
                                el.text = el.interesse_descricao;

                                return el;
                            });

                            return {results: transformed};
                        }
                    },
                    createSearchChoice: function (term) {
                        return {id: term, text: term};
                    }
                });

                $form.find('.btn-lead-salvar-informacoes').click(__crmLeadAtendimento.lead.salvar);
                $form.find('.btn-lead-converter').click(__crmLeadAtendimento.lead.converter);
                $form.find('.btn-lead-aluno').click(__crmLeadAtendimento.lead.editarAluno);
            },
            converter: function () {
                var $form = $(__crmLeadAtendimento.options.formElement);
                var $modal = $('#crm-lead-atendimento');

                __crmLeadAtendimento.addOverlay($modal.find('.modal-content'), 'Iniciando processo de conversão. Aguarde...');

                var dados = $form.serializeJSON();

                location.href = (
                    __crmLeadAtendimento.options.url.crmLeadConverter + '?alunoMediador=conversao-lead&pesId=' + dados['pesId']
                );
            },
            editarAluno: function () {
                var $form = $(__crmLeadAtendimento.options.formElement);
                var $modal = $('#crm-lead-atendimento');

                __crmLeadAtendimento.addOverlay($modal.find('.modal-content'), 'Redirecionando para o cadastro do aluno. Aguarde...');

                var dados = $form.serializeJSON();

                location.href = __crmLeadAtendimento.options.url.alunoEditar + '/' + dados['alunoId'];
            },
            salvar: function () {
                var $form = $(__crmLeadAtendimento.options.formElement);
                var $modal = $('#crm-lead-atendimento');

                __crmLeadAtendimento.addOverlay($modal.find('.modal-content'), 'Salvando registro. Aguarde...');

                var dados = $form.serializeJSON();
                dados['ajax'] = true;
                dados['atualizarInfomacoes'] = true;

                $.ajax({
                    url: __crmLeadAtendimento.options.url.crmLeadAtualizar,
                    type: 'POST',
                    dataType: 'json',
                    data: dados,
                    success: function (data) {
                        var retorno = data['arrDados'] || dados;

                        if (data.erro) {
                            __crmLeadAtendimento.showNotificacaoDanger(
                                data['mensagem'] || 'Não foi possível salvar registro.'
                            );
                        } else {
                            if (__crmLeadAtendimento.options.listagem) {
                                $.crmLeadIndex().reloadDataTableCrmLead();
                            }

                            __crmLeadAtendimento.showNotificacaoSuccess(
                                data['mensagem'] || 'Registro salvo com sucesso!'
                            );

                            $modal.modal('hide');
                        }

                        __crmLeadAtendimento.removeOverlay($modal);
                    }
                });
            },
            tratarBotaoChamar: function () {
                var $modal = $('#crm-lead-atendimento');
                $(document).on('click', '.btn-click2call', function () {
                    var numero = $(this).prev('span').text();
                    numero = numero.replace(/[^0-9]/g, '');
                    var url = __crmLeadAtendimento.options.url.crmOperadorChamar + '/' + numero;
                    __crmLeadAtendimento.addOverlay($modal.find('.modal-content'), 'Efetuando chamada. Aguarde...');

                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        success: function (data) {
                            if (data.erro) {
                                __crmLeadAtendimento.showNotificacaoDanger(
                                    data['mensagem'] || 'Não foi possível efetuar chamada.'
                                );
                            } else {
                                if (__crmLeadAtendimento.options.listagem) {
                                    $.crmLeadIndex().reloadDataTableCrmLead();
                                }

                                __crmLeadAtendimento.showNotificacaoSuccess(
                                    data['mensagem'] || 'Chamada efetuada com sucesso!'
                                );
                            }

                            __crmLeadAtendimento.removeOverlay($modal);
                        }
                    });
                });
            },
            carregarInformacoes: function (arrDados) {
                var $form = $(__crmLeadAtendimento.options.formElement);

                var arrCampos = [
                    'pesId',
                    'leadDataCadastro',
                    'leadDataAtribuicao',
                    'pesNome',
                    'conContatoEmail',
                    'conContatoTelefone',
                    'conContatoCelular',
                    'endCidade',
                    'endEstado',
                    'pesDataNascimento',
                    'pesEstadoCivil',
                    'pesEmpresa',
                    'mediadorNome',
                    'leadStatus',
                    'pesProfissao'
                ];

                arrDados = arrDados || [];

                $.each(arrCampos, function (index, campo) {
                    var valor = arrDados[campo] || '-';

                    if (campo.indexOf('data') != -1 || campo.indexOf('Data') != -1) {
                        valor = __crmLeadAtendimento.formatDate(valor);
                    }

                    if (
                        valor != '-' &&
                        campo.match(/(Celular|Telefone)/i) &&
                        __crmLeadAtendimento.options.value.click2CallAtivo
                    ) {
                        valor = (
                            '<span>' + valor + ' </span>' +
                            '<button type="button" class="btn btn-primary btn-xs btn-click2call" title="Ligar">' +
                            '<span class="fa fa-phone"></span>' +
                            '</button>'
                        );
                    }

                    $form.find('.' + campo).html(valor);
                });

                __crmLeadAtendimento.setPes(arrDados['pesId'] || '-1');
                __crmLeadAtendimento.setInteresse(arrDados['interesse'] || '');

                $form.find('[name=leadObs]').val(arrDados['leadObs'] || '');
                $form.find('[name=alunoId]').val(arrDados['alunoId'] || '');
                $form.find('[name=interesse]').select2('data', __crmLeadAtendimento.getInteresse());
                $form.find('[name=pesId]').val(__crmLeadAtendimento.getPes());
                $form.find('.btn-lead-converter').hide();
                $form.find('.btn-lead-aluno').hide();

                if (arrDados['leadStatus'] == 'Convertido') {
                    if (arrDados['alunoId']) {
                        $form.find('.btn-lead-aluno').show();
                    } else {
                        $form.find('.btn-lead-converter').show();
                    }
                }

                if (__crmLeadAtendimento.getPes() && __crmLeadAtendimento.getPes() != "-1") {
                    __crmLeadAtendimento.atividades.atualizarDatatables();
                    $('#crm-lead-atendimento').modal('show');
                }

            }
        };

        this.carregarInformacoesLead = __crmLeadAtendimento.lead.carregarInformacoes;
        this.visuaAtividadeLead = __crmLeadAtendimento.atividades.visualizar;


        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };

        this.setInteresse = function (interesse) {
            this.options.value.interesse = interesse || null;
        };

        this.getInteresse = function () {
            return this.options.value.interesse || null;
        };

        this.run = function (opts) {
            __crmLeadAtendimento.setDefaults(opts);

            __crmLeadAtendimento.atividades.iniciarCampos();
            __crmLeadAtendimento.atividades.iniciarDatatables();

            __crmLeadAtendimento.lead.iniciarCampos();
            __crmLeadAtendimento.lead.carregarInformacoes();
        };
    };

    $.crmLeadAtendimento = function (params) {
        params = $.extend([], arguments);

        if (params.length == 1) {
            params = params[0];
        }

        var method = false;
        var retval = false;

        if (typeof params === 'string' || typeof CrmLeadAtendimento[params] === 'function') {
            method = params;
            params = undefined;
        } else {
            method = (params || [false])[0];

            if (typeof method === 'string' || typeof CrmLeadAtendimento[method] === 'function') {
                params = params.slice(1)[0];
            }
        }

        var ret = [];

        var obj = $(window).data("universa.crm.lead.atendimento");

        if (!obj) {
            obj = new CrmLeadAtendimento();
            obj.run(params);
            $(window).data('universa.crm.lead.atendimento', obj);
        } else if (method) {
            retval = params ? obj[method](params) : obj[method]();
        }

        ret.push(retval === false ? obj : retval);

        if (ret.length == 1) {
            ret = ret.pop();
        }

        return ret;
    };
})(window.jQuery, window, document);