(function ($, window, document) {
    'use strict';

    var CrmLeadAdd = function () {
        VersaShared.call(this);
        var __crmLeadAdd = this;
        this.defaults = {
            url: {
                crmFonte: '',
                crmMediador: '',
                crmCampanha: '',
                crmOperador: '',
                crmInteresse: '',
                crmAtividadeTipo: '',
                buscaCEP: '',
                pesquisaPF: '',
                atividadesDatatables: ''
            },
            data: {
                pessoaPesquisa: null,
                necessidadesEspeciais: []
            },
            ajaxSubmit: false,
            desativaAutocompletePessoa: false,
            value: {
                pessoaNecessidades: null,
                pes: null,
                fonte: null,
                mediador: null,
                campanha: null,
                operador: null
            },
            callback: {},
            datatables: {
                atividades: null
            },
            wizardElement: '#crm-lead-wizard',
            formElement: '#crm-lead-form',
            validator: null
        };

        this.steps = {};

        this.steps.dadosPessoais = {
            init: function () {
                var $form = $(__crmLeadAdd.options.formElement);

                $form.find("[name=pesCpf]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['999.999.999-99']
                });

                $form.find("[name=pesSexo],[name=pesEstadoCivil]").select2({language: 'pt-BR'});

                if ($form.find("[name=pesId]").val() == "") {
                    $form.find("[name=pesNome]")
                        .blur(function () {
                            $(this).removeClass('ui-autocomplete-loading');
                        })
                        .autocomplete({
                            source: function (request, response) {
                                var $element = $(this.element);
                                var previous_request = $element.data("jqXHR");

                                if (previous_request) {
                                    previous_request.abort();
                                }

                                $element.data("jqXHR", $.ajax({
                                    url: __crmLeadAdd.options.url.pesquisaPF,
                                    data: {query: request.term},
                                    type: 'POST',
                                    dataType: "json",
                                    success: function (data) {
                                        var transformed = $.map(data || [], function (el) {
                                            el.label = el.pesNome;

                                            return el;
                                        });

                                        response(transformed);
                                    }
                                }));
                            },
                            minLength: 2,
                            select: function (event, ui) {
                                __crmLeadAdd.options.data.pessoaPesquisa = ui.item;
                                __crmLeadAdd.steps.dadosPessoais.selecaoPessoa($form);
                            }
                        });
                }

                __crmLeadAdd.iniciarElementoDatePicker($form.find("[name=pesDataNascimento], [name=pesRgEmissao]"));

                var $operador = $form.find('[name=operador]'),
                    $mediador = $form.find('[name=mediador]'),
                    $campanha = $form.find('[name=campanha]'),
                    $fonte = $form.find('[name=fonte]');

                $fonte.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __crmLeadAdd.options.url.crmFonte,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.fonte_id;
                                el.text = el.fonte_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });
                $mediador.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __crmLeadAdd.options.url.crmMediador,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.mediador_id;
                                el.text = el.mediador_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });
                $campanha.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    ajax: {
                        url: __crmLeadAdd.options.url.crmCampanha,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.campanha_id;
                                el.text = el.campanha_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });
                $operador.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    minimumInputLength: 1,
                    ajax: {
                        url: __crmLeadAdd.options.url.crmOperador,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.operador_id;
                                el.text = el.login + " / " + el.time_nome;

                                return el;
                            });

                            return {results: transformed};
                        }
                    }
                });

                var $interesse = $form.find('[name=interesse]');

                $interesse.select2({
                    language: 'pt-BR',
                    allowClear: true,
                    multiple: true,
                    ajax: {
                        url: __crmLeadAdd.options.url.crmInteresse,
                        dataType: 'json',
                        delay: 250,
                        data: function (query) {
                            return {query: query};
                        },
                        results: function (data) {
                            var transformed = $.map(data, function (el) {
                                el.id = el.interesse_id;
                                el.text = el.interesse_descricao;

                                return el;
                            });

                            return {results: transformed};
                        }
                    },
                    createSearchChoice: function (term) {
                        return {id: term, text: term};
                    }
                });

                $fonte.select2("data", __crmLeadAdd.getFonte());
                $mediador.select2("data", __crmLeadAdd.getMediador());
                $campanha.select2("data", __crmLeadAdd.getCampanha());
                $operador.select2("data", __crmLeadAdd.getOperador());
                $interesse.select2("data", __crmLeadAdd.getInteresse());

                $form.find("[name=leadStatus]").select2({
                    allowClear: true, language: 'pt-BR'
                }).trigger("change");
            },
            selecaoPessoa: function ($form) {
                var dados = __crmLeadAdd.options.data.pessoaPesquisa || {};

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: '' +
                    'Deseja realmente carregar os dados no formulário?' +
                    '<br>Nome:<b> ' + dados['pesNome'] + '</b>' +
                    '<br>Data de nascimento:<b> ' + dados['pesDataNascimento'] + '</b>' +
                    '<br>Sexo:<b> ' + dados['pesSexo'] + '</b>' +
                    '<br>Email:<b> ' + dados['conContatoEmail'] + '</b>' +
                    '<br>Celular:<b> ' + dados['conContatoCelular'] + '</b>' +
                    '<br>Cidade/Estado:<b> ' + dados['endCidade'] + ' - ' + dados['endEstado'] + '</b>',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        var dados = __crmLeadAdd.options.data.pessoaPesquisa || {};
                        __crmLeadAdd.steps.dadosPessoais.buscarDadosPessoa(
                            dados['pesId'], $form
                        );
                    }

                    __crmLeadAdd.options.data.pessoaPesquisa = null;
                });

            },
            pesquisarPesssoaPeloDocumento: function (criterioBusca, $form) {
                $form = $(__crmLeadAdd.options.formElement);
                criterioBusca = criterioBusca || {};
                __crmLeadAdd.addOverlay($form, 'Aguarde. Pesquisando pessoa pelo documento...');

                $.ajax({
                    url: __crmLeadAdd.options.url.pesquisaPF,
                    dataType: 'json',
                    type: 'post',
                    data: criterioBusca,
                    success: function (data) {
                        __crmLeadAdd.removeOverlay($form);

                        if (Object.keys(data).length == 0) {
                            __crmLeadAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                            __crmLeadAdd.steps.dadosPessoais.buscarDadosPessoa('', $form);
                        } else {
                            __crmLeadAdd.options.data.pessoaPesquisa = data[0];
                            __crmLeadAdd.steps.dadosPessoais.selecaoPessoa($form);
                        }
                    },
                    erro: function () {
                        __crmLeadAdd.showNotificacaoDanger('Erro desconhecido!');
                        __crmLeadAdd.removeOverlay($form);
                    }
                });
            },
            buscarDadosPessoa: function (pesId, $form) {
                $form = $form || $(__crmLeadAdd.options.formElement);
                pesId = pesId || '';

                if (!$.isNumeric(pesId)) {
                    $form[0].reset();
                    __crmLeadAdd.validate();

                    return;
                }

                var $parent = $form.closest('.modal');

                if ($parent.length == 0) {
                    $parent = $form;
                }

                __crmLeadAdd.addOverlay($parent, 'Aguarde. Buscando informações adicionais...');

                $.ajax({
                    url: __crmLeadAdd.options.url.pesquisaPF,
                    dataType: 'json',
                    type: 'post',
                    data: {pesId: pesId, dadosCompletos: true},
                    success: function (data) {
                        __crmLeadAdd.removeOverlay($parent);

                        if (Object.keys(data).length == 0) {
                            __crmLeadAdd.showNotificacaoInfo('Dados não encontrados!');
                        } else {
                            var dados = data;
                            $form[0].reset();
                            $form.deserialize(dados);
                            $form.find("[name=pesSexo],[name=pesEstadoCivil]").trigger('change');
                            __crmLeadAdd.validate();
                        }
                    },
                    erro: function () {
                        __crmLeadAdd.showNotificacaoDanger('Erro desconhecido!');
                        __crmLeadAdd.removeOverlay($parent);
                    }
                });
            },
            validate: function () {
                __crmLeadAdd.options.validator.settings.rules = {
                    pesNome: {required: true},
                    pesNaturalidade: {required: false},
                    pesNascUf: {required: false},
                    pesDataNascimento: {required: false, dateBR: true},
                    pesSexo: {required: true},
                    fonte: {number: true, required: true},
                    mediador: {number: true},
                    campanha: {number: false},
                    operador: {number: false},
                    leadStatus: {maxlength: 9, required: true}
                };

                __crmLeadAdd.options.validator.settings.messages = {
                    pesNome: {required: 'Campo obrigatório!'},
                    pesNaturalidade: {required: 'Campo obrigatório!'},
                    pesNascUf: {required: 'Campo obrigatório!'},
                    pesDataNascimento: {
                        required: 'Campo obrigatório!',
                        dateBR: 'Data inválida!'
                    },
                    pesSexo: {required: 'Campo obrigatório!'},
                    fonte: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                    mediador: {number: 'Número inválido!'},
                    campanha: {number: 'Número inválido!'},
                    operador: {number: 'Número inválido!'},
                    leadStatus: {maxlength: 'Tamanho máximo: 9!', required: 'Campo obrigatório!'}
                };

                return !$(__crmLeadAdd.options.formElement).valid();
            }
        };

        this.steps.contatoEndereco = {
            init: function () {
                var $form = $(__crmLeadAdd.options.formElement);
                $form.find("[name=endCep]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['99999-999']
                });
                $form.find("[name=endNumero]").inputmask({showMaskOnHover: false, mask: ['9{0,9}']});
                $form.find("[name=conContatoTelefone], [name=conContatoCelular]").inputmask({
                    showMaskOnHover: false, clearIncomplete: true, mask: ['(99) 9999-9999', '(99) 99999-9999']
                });
                $form.find(".buscarCep").click(function () {
                    var $element = $(this);
                    var endCep = $form.find("[name=endCep]").val();
                    var endCepNums = endCep.replace(/[^0-9]/g, '');

                    if (endCepNums.length < 8) {
                        __crmLeadAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');

                        return;
                    }

                    $element.prop('disabled', true);

                    $.ajax({
                        url: __crmLeadAdd.options.url.buscaCEP,
                        dataType: 'json',
                        type: 'post',
                        data: {cep: endCep},
                        success: function (data) {
                            $element.prop('disabled', false);

                            if (!data.dados) {
                                __crmLeadAdd.showNotificacaoWarning('CEP inválido ou não encontrado!');
                            }

                            $form.find("[name=endLogradouro]").val(
                                ((data.dados.tip_logr_nome || '') + ' ' + (data.dados.logr_nome || '')).trim()
                            );
                            $form.find("[name=endCidade]").val(data.dados.cid_nome || '');
                            $form.find("[name=endEstado]").val(data.dados.est_uf || '').trigger('change');
                            $form.find("[name=endBairro]").val(data.dados.bai_nome || '');
                        },
                        erro: function () {
                            __crmLeadAdd.showNotificacaoDanger('Falha ao buscar CEP!');
                            $element.prop('disabled', false);
                        }
                    });
                });
                $form.find("[name=pesNaturalidade], [name=endCidade]")
                    .blur(function () {
                        $(this).removeClass('ui-autocomplete-loading');
                    })
                    .autocomplete({
                        source: function (request, response) {
                            var $element = $(this.element);
                            var previous_request = $element.data("jqXHR");

                            if (previous_request) {
                                previous_request.abort();
                            }

                            $element.data("jqXHR", $.ajax({
                                url: __crmLeadAdd.options.url.buscaCEP,
                                data: {cidade: request.term},
                                type: 'POST',
                                dataType: "json",
                                success: function (data) {
                                    var transformed = $.map(data.dados || [], function (el) {
                                        el.label = el.cid_nome;

                                        return el;
                                    });

                                    response(transformed);
                                }
                            }));
                        },
                        minLength: 2,
                        focus: function (event, ui) {
                            var $element = $(this);
                            $element.val(ui.item.cid_nome);

                            return false;
                        },
                        select: function (event, ui) {
                            var $element = $(this);
                            var campo = $element.attr('name') == 'endCidade' ? "endEstado" : "pesNascUf";
                            $element.val(ui.item.cid_nome);
                            $form.find('[name=' + campo + ']').val(ui.item.est_uf).trigger('change');

                            return false;
                        }
                    });

                $form.find("[name=pesNascUf], [name=endEstado]")
                    .blur(function () {
                        $(this).removeClass('ui-autocomplete-loading');
                    })
                    .autocomplete({
                        source: function (request, response) {
                            var $element = $(this.element);
                            var previous_request = $element.data("jqXHR");

                            if (previous_request) {
                                previous_request.abort();
                            }

                            $element.data("jqXHR", $.ajax({
                                url: __crmLeadAdd.options.url.buscaCEP,
                                data: {estado: request.term},
                                type: 'POST',
                                dataType: "json",
                                success: function (data) {
                                    var transformed = $.map(data.dados || [], function (el) {
                                        return el.est_uf;
                                    });

                                    response(transformed);
                                }
                            }));
                        },
                        minLength: 0
                    });
            },
            validate: function () {
                var $form = $(__crmLeadAdd.options.formElement);
                __crmLeadAdd.options.validator.settings.rules = {
                    endCep: {required: false},
                    endLogradouro: {required: false},
                    endNumero: {required: false, number: true},
                    endComplemento: {required: false},
                    endBairro: {required: false},
                    endCidade: {required: false},
                    endEstado: {required: false},
                    conContatoTelefone: {telefoneValido:true, required: false},
                    conContatoCelular: {
                        celular: true,
                        required: function () {
                            return $form.find('[name=conContatoEmail]').val() == "";
                        }
                    },
                    conContatoEmail: {
                        required: function () {
                            return $form.find('[name=conContatoCelular]').val() == "";
                        },
                        email: true
                    }
                };
                __crmLeadAdd.options.validator.settings.messages = {
                    endCep: {required: 'Campo obrigatório!'},
                    endLogradouro: {required: 'Campo obrigatório!'},
                    endNumero: {required: 'Campo obrigatório!', number: 'Número inválido!'},
                    endComplemento: {required: 'Campo obrigatório!'},
                    endBairro: {required: 'Campo obrigatório!'},
                    endCidade: {required: 'Campo obrigatória!'},
                    endEstado: {required: 'Campo obrigatório!'},
                    conContatoTelefone: {required: 'Campo obrigatório!'},
                    conContatoCelular: {required: 'Campo obrigatório!'},
                    conContatoEmail: {required: 'Campo obrigatório!', email: 'Email inválido!'}
                };

                return !$(__crmLeadAdd.options.formElement).valid();
            }
        };

        this.steps.atividades = {
            init: function () {
                var $form = $(__crmLeadAdd.options.formElement);
                var $datatables = $('#dataTableLeadAtividade');
                var $containerDatatables = $datatables.closest('div');
                var colNum = -1;

                __crmLeadAdd.options.datatables.atividades = $datatables.dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: __crmLeadAdd.options.url.atividadesDatatables,
                        type: "POST",
                        data: function (d) {
                            d.filter = {};
                            d.filter['pes'] = $form.find('[name=pesId]').val() || '-1';
                            d.index = true;

                            __crmLeadAdd.addOverlay($containerDatatables, 'Aguarde, carregando atividades do lead.');

                            return d;
                        },
                        dataSrc: function (json) {
                            var data = json.data;
                            var btns = [{class: 'btn-primary item-view', icon: 'fa-eye'}];

                            for (var row in data) {
                                data[row]['acao'] = __crmLeadAdd.createBtnGroup(btns);
                                data[row]['atividade_data_cadastro_formatada'] = (
                                    __crmLeadAdd.formatDateTime(data[row]['atividade_data_cadastro'])
                                );
                                data[row]['atividade_agendamento_formatada'] = (
                                    __crmLeadAdd.formatDateTime(data[row]['atividade_agendamento'])
                                );
                            }

                            __crmLeadAdd.removeOverlay($containerDatatables);

                            return data;
                        }
                    },
                    columnDefs: [
                        {name: "atividade_data_cadastro", targets: ++colNum, data: "atividade_data_cadastro_formatada"},
                        {name: "operador_nome", targets: ++colNum, data: "operador_nome"},
                        {name: "atividade_tipo_nome", targets: ++colNum, data: "atividade_tipo_nome"},
                        {name: "atividade_forma_contato", targets: ++colNum, data: "atividade_forma_contato"},
                        {name: "atividade_agendamento", targets: ++colNum, data: "atividade_agendamento_formatada"},
                        {name: "atividadeId", targets: ++colNum, data: "acao"}
                    ],
                    "dom": "fr" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                    "autoWidth": true,
                    oLanguage: {
                        sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                        sLengthMenu: "Mostrar _MENU_ registros por página",
                        sZeroRecords: "Nenhum registro encontrado",
                        sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                        sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                        sInfoFiltered: "(filtrado de _MAX_ registros)",
                        sSearch: "Procurar: ",
                        oPaginate: {
                            sFirst: "Início",
                            sPrevious: "Anterior",
                            sNext: "Próximo",
                            sLast: "Último"
                        }
                    },
                    order: [[0, 'desc']]
                });

                $datatables.on('click', '.item-view', function () {
                    var $pai = $(this).closest('tr');
                    var data = __crmLeadAdd.options.datatables.atividades.fnGetData($pai);
                    $.crmLeadAtendimento().visuaAtividadeLead(data);
                });

            },
            atualizarDatatables: function () {
                __crmLeadAdd.options.datatables.atividades.api().ajax.reload(null, false);
            }
        };

        this.salvar = function (form) {
            var $form = $(__crmLeadAdd.options.formElement);

            if ($form.data('noSubmit') == undefined) {
                __crmLeadAdd.validate();
            }

            if ($form.data('noSubmit')) {
                return false;
            }

            if (__crmLeadAdd.options.ajaxSubmit) {
                var $parent = $form.closest('form');

                if ($parent.length == 0) {
                    $parent = $form;
                }

                __crmLeadAdd.addOverlay($parent, 'Salvando registro. Aguarde...');

                var dados = $form.serializeJSON();
                dados['ajax'] = true;

                $.ajax({
                    url: $form.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: dados,
                    success: function (data) {
                        var retorno = data['arrDados'] || dados;

                        if (data.erro) {
                            __crmLeadAdd.showNotificacaoDanger(
                                data['mensagem'] || 'Não foi possível salvar registro.'
                            );
                        } else if (__crmLeadAdd.options.listagem) {
                            $.crmLeadIndex().reloadDataTableCrmLead();
                            var aposSalvarFN = __crmLeadAdd
                                .getFunction(__crmLeadAdd.options.callback, 'aposSalvar');

                            __crmLeadAdd.showNotificacaoSuccess(
                                data['mensagem'] || 'Registro salvo com sucesso!'
                            );

                            if (aposSalvarFN) {
                                aposSalvarFN(retorno, data);
                            }

                            $('#crm-lead-modal').modal('hide');
                        }

                        __crmLeadAdd.removeOverlay($parent);
                    }
                });
            } else {
                form.submit();
            }
        };


        this.setFonte = function (fonte) {
            this.options.value.fonte = fonte || null;
        };

        this.getFonte = function () {
            return this.options.value.fonte || null;
        };

        this.setMediador = function (mediador) {
            this.options.value.mediador = mediador || null;
        };

        this.getMediador = function () {
            return this.options.value.mediador || null;
        };

        this.setCampanha = function (campanha) {
            this.options.value.campanha = campanha || null;
        };

        this.getCampanha = function () {
            return this.options.value.campanha || null;
        };

        this.setOperador = function (operador) {
            this.options.value.operador = operador || null;
        };

        this.getOperador = function () {
            return this.options.value.operador || null;
        };

        this.setInteresse = function (interesses) {
            this.options.value.interesse = interesses || null;
        };

        this.getInteresse = function () {
            return this.options.value.interesse || null;
        };

        this.setAtendimento = function (atendimento) {
            this.options.value.atendimento = atendimento || false;
        };

        this.getAtendimento = function () {
            return this.options.value.atendimento || false;
        };

        this.submitHandler = this.salvar;
        this.atualizarAtividades = this.steps.atividades.atualizarDatatables;

        this.buscarDadosPessoa = function (pesId) {
            __crmLeadAdd.steps.dadosPessoais.buscarDadosPessoa(pesId);
        };

        this.setCallback = function (callback, func) {
            __crmLeadAdd.options.callback[callback] = func;
        };

        this.setValidations = function () {
            $(document).on("change", __crmLeadAdd.options.formElement + " .form-control:hidden", function () {
                $(__crmLeadAdd.options.formElement).valid();
            });

            $(__crmLeadAdd.options.formElement).submit(function (e) {
                var ok = __crmLeadAdd.validate();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }
            });
        };

        this.pesquisaCrmLead = function (pes_id, callback) {
            var $form = $(__crmLeadAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmLeadAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + pes_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true, atendimento: __crmLeadAdd.getAtendimento()},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmLeadAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmLeadAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmLeadAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmLeadAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            __crmLeadAdd.setDefaults(opts);
            __crmLeadAdd.loadSteps();
            __crmLeadAdd.setValidations();
            __crmLeadAdd.wizard();
        };
    };

    $.crmLeadAdd = function (params) {
        params = $.extend([], arguments);

        if (params.length == 1) {
            params = params[0];
        }

        var method = false;
        var retval = false;

        if (typeof params === 'string' || typeof CrmLeadAdd[params] === 'function') {
            method = params;
            params = undefined;
        } else {
            method = (params || [false])[0];

            if (typeof method === 'string' || typeof CrmLeadAdd[method] === 'function') {
                params = params.slice(1)[0];
            }
        }

        var ret = [];

        var obj = $(window).data("universa.crm.lead.add");

        if (!obj) {
            obj = new CrmLeadAdd();
            obj.run(params);
            $(window).data('universa.crm.lead.add', obj);
        } else if (method) {
            retval = params ? obj[method](params) : obj[method]();
        }

        ret.push(retval === false ? obj : retval);

        if (ret.length == 1) {
            ret = ret.pop();
        }

        return ret;
    };
})(window.jQuery, window, document);