(function ($, window, document) {
    'use strict';
    var CrmLeadIndex = function () {
        VersaShared.call(this);
        var __crmLeadIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: '',
                capturaLeadEmail: '',
                importacaoCSV: '',
                leadDistribuir: '',
                crmFonte: '',
                crmMediador: '',
                crmCampanha: '',
                crmOperador: ''
            },
            value: {
                operador: null,
                atendimento: false
            },
            datatables: {
                crmLead: null
            },
            formElement: '#crm-lead-form-filtro'
        };

        this.inciarCamposFiltrosAvancado = function () {
            var $form = $(__crmLeadIndex.options.formElement);

            var $operador = $form.find('[name=operador]'),
                $mediador = $form.find('[name=mediador]'),
                $campanha = $form.find('[name=campanha]'),
                $fonte = $form.find('[name=fonte]'),
                $atividadeTipo = $form.find('[name=ultimaAtividade]');

            __crmLeadIndex.iniciarElementoDatePicker($("#dataAtribuicaoInicial, #dataAtribuicaoFinal"));

            $fonte.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __crmLeadIndex.options.url.crmFonte,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.fonte_id;
                            el.text = el.fonte_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });
            $mediador.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __crmLeadIndex.options.url.crmMediador,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.mediador_id;
                            el.text = el.mediador_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });
            $campanha.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __crmLeadIndex.options.url.crmCampanha,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.campanha_id;
                            el.text = el.campanha_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $atividadeTipo.select2({
                language: 'pt-BR',
                multiple: true,
                allowClear: true,
                ajax: {
                    url: __crmLeadIndex.options.url.crmAtividadeTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query, apenasFolhas: true};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.atividade_tipo_id;
                            el.text = el.atividade_tipo_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $operador.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __crmLeadIndex.options.url.crmOperador,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.operador_id;
                            el.text = el.login + " / " + el.time_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            __crmLeadIndex.iniciarElementoDatePicker($form.find(
                "[name=dataCadastroInicial],[name=dataCadastroFinal]," +
                "[name=agendamentoInicial],[name=agendamentoFinal]," +
                "[name=dataAtividadeInicial],[name=dataAtividadeFinal]"
            ));

            $form.find("[name=leadStatus]").select2({
                allowClear: true, language: 'pt-BR'
            }).trigger("change");

            $("#leadFiltroExecutar").click(function () {
                __crmLeadIndex.atualizarListagem();
            });

            $("#leadFiltroLimpar").click(function () {
                if (!__crmLeadIndex.options.value.atendimento) {
                    $operador.select2('val', '').trigger('change');
                }

                $form.find('[name=mediador],[name=campanha],[name=ultimaAtividade],[name=fonte],[name=leadStatus]')
                    .select2('val', '').trigger('change');
                $form.find(
                    '[name=dataCadastroInicial],[name=dataCadastroFinal],' +
                    '[name=agendamentoInicial],[name=agendamentoFinal],' +
                    '[name=dataAtividadeInicial],[name=dataAtividadeFinal], [name=dataAtribuicaoInicial],[name=dataAtribuicaoFinal]'
                ).val('').trigger('change');
            });

            $form.find('[name=operador],[name=mediador],[name=campanha],[name=fonte],[name=ultimaAtividade],[name=leadStatus]').on(
                'change select2-selected select2-clearing select2-removed',
                function () {
                    var $item = $(this);

                    if ($item.val() != '') {
                        $item.parent().find('.select2-container').addClass('select2-allowclear');
                    } else {
                        $item.parent().find('.select2-container').removeClass('select2-allowclear');
                    }
                }
            );


            if (__crmLeadIndex.options.value.atendimento) {
                $('#crm-lead-btn-list').hide();
                $operador.select2('data', __crmLeadIndex.options.value.operador);
                $operador.select2('disable');
            }
        };

        this.atualizarListagem = function () {
            __crmLeadIndex.options.datatables.crmLead.api().ajax.reload(null, false);
        };

        this.retornaFiltrosAtivos = function () {
            var $form = $(__crmLeadIndex.options.formElement);
            var filtros = {};
            var operador = $form.find('[name=operador]').val(),
                mediador = $form.find('[name=mediador]').val(),
                campanha = $form.find('[name=campanha]').val(),
                fonte = $form.find('[name=fonte]').val(),
                ultimaAtividade = $form.find('[name=ultimaAtividade]').val(),
                leadStatus = $form.find('[name=leadStatus]').val(),
                dataCadastroInicial = $form.find('[name=dataCadastroInicial]').val(),
                dataCadastroFinal = $form.find('[name=dataCadastroFinal]').val(),
                agendamentoInicial = $form.find('[name=agendamentoInicial]').val(),
                agendamentoFinal = $form.find('[name=agendamentoFinal]').val(),
                dataAtividadeInicial = $form.find('[name=dataAtividadeInicial]').val(),
                dataAtribuicaoInicial = $form.find('[name=dataAtribuicaoInicial]').val(),
                dataAtribuicaoFinal = $form.find('[name=dataAtribuicaoFinal]').val(),
                dataAtividadeFinal = $form.find('[name=dataAtividadeFinal]').val();

            if (operador) {
                filtros['operador'] = operador;
            }
            if (mediador) {
                filtros['mediador'] = mediador;
            }
            if (campanha) {
                filtros['campanha'] = campanha;
            }
            if (fonte) {
                filtros['fonte'] = fonte;
            }
            if (ultimaAtividade) {
                filtros['ultimaAtividade'] = ultimaAtividade;
            }
            if (leadStatus) {
                filtros['leadStatus'] = leadStatus;
            }
            if (dataCadastroInicial) {
                filtros['dataCadastroInicial'] = dataCadastroInicial;
            }
            if (dataCadastroFinal) {
                filtros['dataCadastroFinal'] = dataCadastroFinal;
            }
            if (agendamentoInicial) {
                filtros['agendamentoInicial'] = agendamentoInicial;
            }
            if (agendamentoFinal) {
                filtros['agendamentoFinal'] = agendamentoFinal;
            }
            if (dataAtividadeInicial) {
                filtros['dataAtividadeInicial'] = dataAtividadeInicial;
            }
            if (dataAtividadeFinal) {
                filtros['dataAtividadeFinal'] = dataAtividadeFinal;
            }
            if (dataAtribuicaoInicial) {
                filtros['dataAtribuicaoInicial'] = dataAtribuicaoInicial;
            }
            if (dataAtribuicaoFinal) {
                filtros['dataAtribuicaoFinal'] = dataAtribuicaoFinal;
            }

            return filtros;
        };

        this.setSteps = function () {
            var $form = $(__crmLeadIndex.options.formElement),
                $dataTableCrmLead = $('#dataTableCrmLead');

            var colNum = 0;
            __crmLeadIndex.options.datatables.crmLead = $dataTableCrmLead.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmLeadIndex.options.url.search,
                    type: "POST",
                    data: function (d) {
                        d.filter = __crmLeadIndex.retornaFiltrosAtivos();
                        d.index = true;

                        return d;
                    },
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [];

                        if (!__crmLeadIndex.options.value.atendimento) {
                            btns.push({class: 'btn-primary crmLead-edit', icon: 'fa-pencil'});
                        }

                        btns.push({class: 'btn-info crm-lead-atendimento', icon: 'fa-comment'});

                        for (var row in data) {
                            data[row]['acao'] = __crmLeadIndex.createBtnGroup(btns);
                            data[row]['lead_data_cadastro_formatada'] = __crmLeadIndex
                                .formatDate(data[row]['lead_data_cadastro']);
                            data[row]['atividade_data_cadastro_formatada'] = __crmLeadIndex
                                .formatDate(data[row]['atividade_data_cadastro']);
                            data[row]['atividade_agendamento_formatada'] = __crmLeadIndex
                                .formatDate(data[row]['atividade_agendamento']);
                        }

                        __crmLeadIndex.removeOverlay($('#container-crm-lead'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "pes_nome", targets: colNum++, data: "pes_nome"},
                    {name: "operador_nome", targets: colNum++, data: "operador_nome"},
                    {name: "lead_data_cadastro", targets: colNum++, data: "lead_data_cadastro_formatada"},
                    {name: "atividade_tipo_nome", targets: colNum++, data: "atividade_tipo_nome"},
                    {name: "atividade_data_cadastro", targets: colNum++, data: "atividade_data_cadastro_formatada"},
                    {name: "atividade_agendamento", targets: colNum++, data: "atividade_agendamento_formatada"},
                    {name: "lead_status", targets: colNum++, data: "lead_status"},
                    {name: "pes_id", targets: colNum++, data: "acao"}
                ],
                "dom": "<'dt-toolbar-header'<'col-sm-4'l><'col-sm-4'r><'col-sm-4'f>>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[2, 'desc']]
            });

            $dataTableCrmLead.on('click', '.crmLead-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmLeadIndex.options.datatables.crmLead.fnGetData($pai);

                if (!__crmLeadIndex.options.ajax) {
                    location.href = __crmLeadIndex.options.url.edit + '/' + data['pes_id'];
                } else {
                    __crmLeadIndex.addOverlay(
                        $('#container-crm-lead'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmLeadAdd().setAtendimento(false);
                    $.crmLeadAdd().pesquisaCrmLead(
                        data['pes_id'],
                        function (data) {
                            __crmLeadIndex.showModal(data);
                        }
                    );
                }
            });

            $dataTableCrmLead.on('click', '.crm-lead-atendimento', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmLeadIndex.options.datatables.crmLead.fnGetData($pai);

                __crmLeadIndex.addOverlay(
                    $('#container-crm-lead'), 'Aguarde, carregando dados para atendimento...'
                );
                $.crmLeadAdd().setAtendimento(true);
                $.crmLeadAdd().pesquisaCrmLead(
                    data['pes_id'],
                    function (data) {
                        $.crmLeadAtendimento().carregarInformacoesLead(data);
                        __crmLeadIndex.removeOverlay($('#container-crm-lead'));
                    }
                );
            });

            $dataTableCrmLead.on('click', '.crmLead-atendimento', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmLeadIndex.options.datatables.crmLead.fnGetData($pai);

                if (!__crmLeadIndex.options.ajax) {
                    location.href = __crmLeadIndex.options.url.atendimento + '/' + data['pes_id'];
                } else {
                    __crmLeadIndex.addOverlay(
                        $('#container-crm-lead'), 'Aguarde, carregando dados para atendimento...'
                    );
                    $.crmLeadAdd().setAtendimento(false);
                    $.crmLeadAdd().pesquisaCrmLead(
                        data['pes_id'],
                        function (data) {
                            __crmLeadIndex.showModal(data);
                        }
                    );
                }
            });

            $('#btn-crm-lead-distribuir').click(__crmLeadIndex.distribuirLeads);
            $('#btn-crm-lead-importar-lead-email').click(function (e) {
                __crmLeadIndex.addOverlay(
                    $('#container-crm-lead'), 'Aguarde, executando importação de leads...'
                );

                $.ajax({
                    url: __crmLeadIndex.options.url.capturaLeadEmail,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        data['emails'] = data['emails'] || 0;
                        data['leads'] = data['leads'] || 0;

                        __crmLeadIndex.showNotificacaoSuccess(
                            "" + data['emails'] + " emails processados!<br>" +
                            "" + data['leads'] + " leads processados!"
                        );
                        __crmLeadIndex.reloadDataTableCrmLead();
                    }
                });

                e.preventDefault();
                e.stopPropagation();
            });

            $('#btn-crm-lead-add').click(function (e) {
                if (__crmLeadIndex.options.ajax) {
                    __crmLeadIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableCrmLead.on('click', '.crmLead-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmLeadIndex.options.datatables.crmLead.fnGetData($pai);
                var arrRemover = {
                    pesId: data['pes_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de lead "' + data['fonte_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmLeadIndex.addOverlay(
                                $('#container-crm-lead'), 'Aguarde, solicitando remoção de registro de lead...'
                            );
                            $.ajax({
                                url: __crmLeadIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmLeadIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de lead:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmLeadIndex.removeOverlay($('#container-crm-lead'));
                                    } else {
                                        __crmLeadIndex.reloadDataTableCrmLead();
                                        __crmLeadIndex.showNotificacaoSuccess(
                                            "Registro de lead removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.distribuirLeads = function () {
            $.SmartMessageBox({
                title: "Confirme a operação:",
                content: 'Deseja realmente TODOS os leads da listagem?',
                buttons: "[Não][Sim]"
            }, function (ButtonPress) {
                if (ButtonPress == "Sim") {
                    setTimeout(function () {
                        __crmLeadIndex.addOverlay(
                            $('#container-crm-lead'), 'Aguarde, distribuindo/redistribuindo registros de lead...'
                        );
                        $.ajax({
                            url: __crmLeadIndex.options.url.leadDistribuir,
                            type: 'POST',
                            dataType: 'json',
                            data: __crmLeadIndex.retornaFiltrosAtivos(),
                            success: function (data) {
                                if (data.erro) {
                                    __crmLeadIndex.showNotificacaoDanger(
                                        "Não foi possível distribuir registros de leads:<br>" +
                                        "<i>" + data['mensagem'] + "</i>"
                                    );
                                    __crmLeadIndex.removeOverlay($('#container-crm-lead'));
                                } else {
                                    __crmLeadIndex.reloadDataTableCrmLead();
                                    __crmLeadIndex.showNotificacaoSuccess(
                                        "Registros de leads removido!<br>" +
                                        (data['mensagem'] ? "<i>" + data['mensagem'] + "</i>" : '')
                                    );
                                }
                            }
                        });
                    }, 400);
                }
            });
        };

        this.iniciaFormImportacaoCSV = function () {
            var $form = $('#crm-lead-enviar-csv');
            var $modal = $('#crm-lead-modal-import-csv');
            $('#arq').vfile({
                hideText: true,
                defaultMessage: 'Sem arquivo selecionado',
                invalidMessage: 'Arquivo inválido!',
                allowedTypes: [
                    'csv',
                    'text/csv',
                    'text/x-csv',
                    'text/plain',
                    'application/csv',
                    'application/x-csv',
                    'text/comma-separated-values',
                    'text/x-comma-separated-values',
                    'application/vnd.ms-excel',
                    'text/tab-separated-values'
                ]
            });

            $form.validate({
                submitHandler: function () {
                    __crmLeadIndex.addOverlay(
                        $modal, 'Aguarde, processando arquivo...'
                    );
                    $form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            if (data['erro']) {
                                __crmLeadIndex.showNotificacaoDanger(
                                    "Não foi possível importar arquivo:\n" +
                                    "<i>" + data['mensagem'] + "</i>"
                                );
                                __crmLeadIndex.removeOverlay($('#container-crm-lead'));
                            } else {
                                __crmLeadIndex.showNotificacaoSuccess(
                                    "" + data['contatos'] + " contatos processados!<br>" +
                                    "" + data['leads'] + " leads tratados!"
                                );

                                __crmLeadIndex.reloadDataTableCrmLead();
                                $modal.modal('hide');
                            }

                            __crmLeadIndex.removeOverlay($modal);
                        }
                    });
                    return false;
                },
                rules: {'[name^=vfile]': {required: true}},
                messages: {'[name^=vfile]': {required: 'Selecione o arquivo!'}},
                ignore: '.ignore'
            });

            $('#btn-crm-lead-importar-csv').click(function (e) {
                $modal.modal('show');
                e.preventDefault();
                e.stopPropagation();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#crm-lead-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['pesId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['pesId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#fonte").select2('data', data['fonte']).trigger("change");
            $("#mediador").select2('data', data['mediador']).trigger("change");
            $("#campanha").select2('data', data['campanha']).trigger("change");
            $("#operador").select2('data', data['operador']).trigger("change");
            $("#interesse").select2('data', data['interesse']).trigger("change");
            $("#leadStatus").select2('val', data['leadStatus']).trigger("change");
            $("#pesSexo").select2('val', data['pesSexo']).trigger("change");
            $("#pesEstadoCivil").select2('val', data['pesEstadoCivil']).trigger("change");
            $(".leadDataCadastro").html(__crmLeadIndex.formatDateTime(data['leadDataCadastro']));
            $(".leadDataAlteracao").html(__crmLeadIndex.formatDateTime(data['leadDataAlteracao']));
            $(".leadDataAtribuicao").html(__crmLeadIndex.formatDateTime(data['leadDataAtribuicao']));

            $.crmLeadAdd().atualizarAtividades();

            $('#crm-lead-modal').find('.modal-title').html(actionTitle + ' lead');
            $('#crm-lead-modal').modal();
            __crmLeadIndex.removeOverlay($('#container-crm-lead'));
        };

        this.reloadDataTableCrmLead = function () {
            this.getDataTableCrmLead().api().ajax.reload(null, false);
        };

        this.getDataTableCrmLead = function () {
            if (!__crmLeadIndex.options.datatables.crmLead) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmLead')) {
                    __crmLeadIndex.options.datatables.crmLead = $('#dataTableCrmLead').DataTable();
                } else {
                    __crmLeadIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmLeadIndex.options.datatables.crmLead;
        };

        this.run = function (opts) {
            __crmLeadIndex.setDefaults(opts);
            __crmLeadIndex.inciarCamposFiltrosAvancado();
            __crmLeadIndex.setSteps();
            __crmLeadIndex.iniciaFormImportacaoCSV();
        };
    };

    $.crmLeadIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-lead.index");

        if (!obj) {
            obj = new CrmLeadIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-lead.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);