(function ($, window, document) {
    'use strict';
    var CrmLeadAtividadeIndex = function () {
        VersaShared.call(this);
        var __crmLeadAtividadeIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: '',
                crmFonte: '',
                crmMediador: '',
                crmCampanha: '',
                crmOperador: ''
            },
            datatables: {
                crmLeadAtividade: null
            },
            formElement: '#crm-lead-form-filtro'
        };

        this.inciarCamposFiltrosAvancado = function () {
            var $form = $(__crmLeadAtividadeIndex.options.formElement);

            var $operador      = $form.find('[name=operador]'),
                $mediador      = $form.find('[name=mediador]'),
                $campanha      = $form.find('[name=campanha]'),
                $fonte         = $form.find('[name=fonte]'),
                $atividadeTipo = $form.find('[name=atividadeTipo]');

            $fonte.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __crmLeadAtividadeIndex.options.url.crmFonte,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.fonte_id;
                            el.text = el.fonte_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });
            $mediador.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __crmLeadAtividadeIndex.options.url.crmMediador,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.mediador_id;
                            el.text = el.mediador_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });
            $campanha.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __crmLeadAtividadeIndex.options.url.crmCampanha,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.campanha_id;
                            el.text = el.campanha_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $atividadeTipo.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                ajax: {
                    url: __crmLeadAtividadeIndex.options.url.crmAtividadeTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query, apenasFolhas: true};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.atividade_tipo_id;
                            el.text = el.atividade_tipo_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $operador.select2({
                language: 'pt-BR',
                allowClear: true,
                multiple: true,
                minimumInputLength: 1,
                ajax: {
                    url: __crmLeadAtividadeIndex.options.url.crmOperador,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.operador_id;
                            el.text = el.login + " / " + el.time_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            __crmLeadAtividadeIndex.iniciarElementoDatePicker($form.find(
                "[name=dataCadastroInicial],[name=dataCadastroFinal]," +
                "[name=agendamentoInicial],[name=agendamentoFinal]," +
                "[name=dataAtividadeInicial],[name=dataAtividadeFinal]"
            ));

            $form.find("[name=leadStatus]").select2({
                allowClear: true, language: 'pt-BR'
            }).trigger("change");

            $("#leadFiltroExecutar").click(function () {
                __crmLeadAtividadeIndex.atualizarListagem();
            });

            $("#leadFiltroLimpar").click(function () {
                $form.find('[name=operador],[name=mediador],[name=campanha],[name=atividadeTipo],[name=fonte],[name=leadStatus]')
                    .select2('val', '').trigger('change');
                $form.find(
                    '[name=dataCadastroInicial],[name=dataCadastroFinal],' +
                    '[name=agendamentoInicial],[name=agendamentoFinal],' +
                    '[name=dataAtividadeInicial],[name=dataAtividadeFinal]'
                ).val('').trigger('change');
            });

            $form.find('[name=operador],[name=mediador],[name=campanha],[name=fonte],[name=atividadeTipo],[name=leadStatus]').on(
                'change select2-selected select2-clearing select2-removed',
                function () {
                    var $item = $(this);

                    if ($item.val() != '') {
                        $item.parent().find('.select2-container').addClass('select2-allowclear');
                    } else {
                        $item.parent().find('.select2-container').removeClass('select2-allowclear');
                    }
                }
            );
        };

        this.atualizarListagem = function () {
            __crmLeadAtividadeIndex.options.datatables.crmLeadAtividade.api().ajax.reload(null, false);
        };

        this.retornaFiltrosAtivos = function () {
            var $form = $(__crmLeadAtividadeIndex.options.formElement);
            var filtros = {};
            var operador             = $form.find('[name=operador]').val(),
                mediador             = $form.find('[name=mediador]').val(),
                campanha             = $form.find('[name=campanha]').val(),
                fonte                = $form.find('[name=fonte]').val(),
                atividadeTipo        = $form.find('[name=atividadeTipo]').val(),
                leadStatus           = $form.find('[name=leadStatus]').val(),
                dataCadastroInicial  = $form.find('[name=dataCadastroInicial]').val(),
                dataCadastroFinal    = $form.find('[name=dataCadastroFinal]').val(),
                agendamentoInicial   = $form.find('[name=agendamentoInicial]').val(),
                agendamentoFinal     = $form.find('[name=agendamentoFinal]').val(),
                dataAtividadeInicial = $form.find('[name=dataAtividadeInicial]').val(),
                dataAtividadeFinal   = $form.find('[name=dataAtividadeFinal]').val();

            if (operador) {
                filtros['operador'] = operador;
            }
            if (mediador) {
                filtros['mediador'] = mediador;
            }
            if (campanha) {
                filtros['campanha'] = campanha;
            }
            if (fonte) {
                filtros['fonte'] = fonte;
            }
            if (atividadeTipo) {
                filtros['atividadeTipo'] = atividadeTipo;
            }
            if (leadStatus) {
                filtros['leadStatus'] = leadStatus;
            }
            if (dataCadastroInicial) {
                filtros['dataCadastroInicial'] = dataCadastroInicial;
            }
            if (dataCadastroFinal) {
                filtros['dataCadastroFinal'] = dataCadastroFinal;
            }
            if (agendamentoInicial) {
                filtros['agendamentoInicial'] = agendamentoInicial;
            }
            if (agendamentoFinal) {
                filtros['agendamentoFinal'] = agendamentoFinal;
            }
            if (dataAtividadeInicial) {
                filtros['dataAtividadeInicial'] = dataAtividadeInicial;
            }
            if (dataAtividadeFinal) {
                filtros['dataAtividadeFinal'] = dataAtividadeFinal;
            }

            return filtros;
        };

        this.setSteps = function () {
            var colNum = -1;

            var $datatables = $('#dataTableCrmLeadAtividade');
            var $containerDatatables = $datatables.closest('div');
            __crmLeadAtividadeIndex.options.datatables.crmLeadAtividade = $datatables.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmLeadAtividadeIndex.options.url.search,
                    type: "POST",
                    data: function (d) {
                        d.filter = __crmLeadAtividadeIndex.retornaFiltrosAtivos();
                        d.index = true;

                        __crmLeadAtividadeIndex.addOverlay($containerDatatables, 'Aguarde, carregando atividades do lead.');

                        return d;
                    },
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [{class: 'btn-primary item-view', icon: 'fa-eye'}];

                        for (var row in data) {
                            data[row]['acao'] = __crmLeadAtividadeIndex.createBtnGroup(btns);
                            data[row]['atividade_data_cadastro_formatada'] = (
                                __crmLeadAtividadeIndex.formatDateTime(data[row]['atividade_data_cadastro'])
                            );
                            data[row]['atividade_agendamento_formatada'] = (
                                __crmLeadAtividadeIndex.formatDateTime(data[row]['atividade_agendamento'])
                            );
                        }


                        __crmLeadAtividadeIndex.removeOverlay($('#container-crm-lead-atividade'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "pes_nome", targets: ++colNum, data: "pes_nome"},
                    {name: "atividade_data_cadastro", targets: ++colNum, data: "atividade_data_cadastro_formatada"},
                    {name: "operador_nome", targets: ++colNum, data: "operador_nome"},
                    {name: "atividade_tipo_nome", targets: ++colNum, data: "atividade_tipo_nome"},
                    {name: "atividade_forma_contato", targets: ++colNum, data: "atividade_forma_contato"},
                    {name: "atividade_agendamento", targets: ++colNum, data: "atividade_agendamento_formatada"},
                    {name: "atividadeId", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $datatables.on('click', '.crmLeadAtividade-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmLeadAtividadeIndex.options.datatables.crmLeadAtividade.fnGetData($pai);

                if (!__crmLeadAtividadeIndex.options.ajax) {
                    location.href = __crmLeadAtividadeIndex.options.url.edit + '/' + data['atividade_id'];
                } else {
                    __crmLeadAtividadeIndex.addOverlay(
                        $('#container-crm-lead-atividade'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmLeadAtividadeAdd().pesquisaCrmLeadAtividade(
                        data['atividade_id'],
                        function (data) {
                            __crmLeadAtividadeIndex.showModal(data);
                        }
                    );
                }
            });

            $datatables.on('click', '.item-view', function () {
                var $pai = $(this).closest('tr');
                var data = __crmLeadAtividadeIndex.options.datatables.crmLeadAtividade.fnGetData($pai);
                $.crmLeadAtendimento().visuaAtividadeLead(data);
            });

            $('#btn-crm-lead-atividade-add').click(function (e) {
                if (__crmLeadAtividadeIndex.options.ajax) {
                    __crmLeadAtividadeIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $datatables.on('click', '.crmLeadAtividade-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmLeadAtividadeIndex.options.datatables.crmLeadAtividade.fnGetData($pai);
                var arrRemover = {
                    atividadeId: data['atividade_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de atividade de lead  "' + data['atividade_descricao'] +
                    '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmLeadAtividadeIndex.addOverlay(
                                $('#container-crm-lead-atividade'),
                                'Aguarde, solicitando remoção de registro de atividade de lead ...'
                            );
                            $.ajax({
                                url: __crmLeadAtividadeIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmLeadAtividadeIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de atividade de lead :\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmLeadAtividadeIndex.removeOverlay($('#container-crm-lead-atividade'));
                                    } else {
                                        __crmLeadAtividadeIndex.reloadDataTableCrmLeadAtividade();
                                        __crmLeadAtividadeIndex.showNotificacaoSuccess(
                                            "Registro de atividade de lead  removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#crm-lead-atividade-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['atividadeId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['atividadeId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#pes").select2('data', data['pes']).trigger("change");
            $("#operador").select2('data', data['operador']).trigger("change");
            $("#atividadeTipo").select2('data', data['atividadeTipo']).trigger("change");
            $("#atividadeFormaContato").select2('val', data['atividadeFormaContato']).trigger("change");
            $("#atividadeSituacao").select2('val', data['atividadeSituacao']).trigger("change");

            $('#crm-lead-atividade-modal .modal-title').html(actionTitle + ' atividade de lead ');
            $('#crm-lead-atividade-modal').modal();
            __crmLeadAtividadeIndex.removeOverlay($('#container-crm-lead-atividade'));
        };

        this.reloadDataTableCrmLeadAtividade = function () {
            this.getDataTableCrmLeadAtividade().api().ajax.reload(null, false);
        };

        this.getDataTableCrmLeadAtividade = function () {
            if (!__crmLeadAtividadeIndex.options.datatables.crmLeadAtividade) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmLeadAtividade')) {
                    __crmLeadAtividadeIndex.options.datatables.crmLeadAtividade =
                        $('#dataTableCrmLeadAtividade').DataTable();
                } else {
                    __crmLeadAtividadeIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmLeadAtividadeIndex.options.datatables.crmLeadAtividade;
        };

        this.run = function (opts) {
            __crmLeadAtividadeIndex.setDefaults(opts);
            __crmLeadAtividadeIndex.inciarCamposFiltrosAvancado();
            __crmLeadAtividadeIndex.setSteps();
        };
    };

    $.crmLeadAtividadeIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-lead-atividade.index");

        if (!obj) {
            obj = new CrmLeadAtividadeIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-lead-atividade.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);