(function ($, window, document) {
    'use strict';

    var CrmLeadAtividadeAdd = function () {
        VersaShared.call(this);
        var __crmLeadAtividadeAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                crmLead: '',
                crmOperador: '',
                crmAtividadeTipo: ''
            },
            data: {},
            value: {
                pes: null,
                operador: null,
                atividadeTipo: null
            },
            datatables: {},
            wizardElement: '#crm-lead-atividade-wizard',
            formElement: '#crm-lead-atividade-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#pes").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __crmLeadAtividadeAdd.options.url.crmLead,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__crmLeadAtividadeAdd.getPes()) {
                $('#pes').select2("data", __crmLeadAtividadeAdd.getPes());
            }

            $("#atividadeDataCadastro")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#operador").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __crmLeadAtividadeAdd.options.url.crmOperador,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.operador_id;
                            el.text = el.operador_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__crmLeadAtividadeAdd.getOperador()) {
                $('#operador').select2("data", __crmLeadAtividadeAdd.getOperador());
            }
            $("#atividadeTipo").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __crmLeadAtividadeAdd.options.url.crmAtividadeTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.atividade_tipo_id;
                            el.text = el.atividade_tipo_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__crmLeadAtividadeAdd.getAtividadeTipo()) {
                $('#atividadeTipo').select2("data", __crmLeadAtividadeAdd.getAtividadeTipo());
            }
            $("#atividadeFormaContato").select2({
                minimumInputLength: 1, allowClear: true, language: 'pt-BR'
            }).trigger("change");
            $("#atividadeAgendamento")
                .datepicker({
                    prevText: '<i class="fa fa-chevron-left"></i>', nextText: '<i class="fa fa-chevron-right"></i>'
                })
                .inputmask({showMaskOnHover: false, clearIncomplete: true, mask: ['99/99/9999']});
            $("#atividadeSituacao").select2({
                minimumInputLength: 1, allowClear: true, language: 'pt-BR'
            }).trigger("change");
        };

        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };

        this.setOperador = function (operador) {
            this.options.value.operador = operador || null;
        };

        this.getOperador = function () {
            return this.options.value.operador || null;
        };

        this.setAtividadeTipo = function (atividadeTipo) {
            this.options.value.atividadeTipo = atividadeTipo || null;
        };

        this.getAtividadeTipo = function () {
            return this.options.value.atividadeTipo || null;
        };
        this.setValidations = function () {
            __crmLeadAtividadeAdd.options.validator.settings.rules = {
                pes: {number: true, required: true}, atividadeDataCadastro: {required: true, dateBR: true},
                operador: {number: true, required: true}, atividadeTipo: {number: true, required: true},
                atividadeFormaContato: {maxlength: 8}, atividadeDescricao: {maxlength: 200, required: true},
                atividadeAgendamento: {dateBR: true}, atividateAgendamentoTitulo: {maxlength: 200},
                atividadeSituacao: {maxlength: 10, required: true},
            };
            __crmLeadAtividadeAdd.options.validator.settings.messages = {
                pes: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                atividadeDataCadastro: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                operador: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                atividadeTipo: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                atividadeFormaContato: {maxlength: 'Tamanho máximo: 8!'},
                atividadeDescricao: {maxlength: 'Tamanho máximo: 200!', required: 'Campo obrigatório!'},
                atividadeAgendamento: {dateBR: 'Informe uma data válida!'},
                atividateAgendamentoTitulo: {maxlength: 'Tamanho máximo: 200!'},
                atividadeSituacao: {maxlength: 'Tamanho máximo: 10!', required: 'Campo obrigatório!'},
            };

            $(__crmLeadAtividadeAdd.options.formElement).submit(function (e) {
                var ok = $(__crmLeadAtividadeAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__crmLeadAtividadeAdd.options.formElement);

                if (__crmLeadAtividadeAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __crmLeadAtividadeAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __crmLeadAtividadeAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__crmLeadAtividadeAdd.options.listagem) {
                                    $.crmLeadAtividadeIndex().reloadDataTableCrmLeadAtividade();
                                    __crmLeadAtividadeAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#crm-lead-atividade-modal').modal('hide');
                                }

                                __crmLeadAtividadeAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaCrmLeadAtividade = function (atividade_id, callback) {
            var $form = $(__crmLeadAtividadeAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmLeadAtividadeAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + atividade_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmLeadAtividadeAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmLeadAtividadeAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmLeadAtividadeAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmLeadAtividadeAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.crmLeadAtividadeAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-lead-atividade.add");

        if (!obj) {
            obj = new CrmLeadAtividadeAdd();
            obj.run(params);
            $(window).data('universa.crm.crm-lead-atividade.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);