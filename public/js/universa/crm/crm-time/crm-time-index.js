(function ($, window, document) {
    'use strict';
    var CrmTimeIndex = function () {
        VersaShared.call(this);
        var __crmTimeIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                crmTime: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __crmTimeIndex.options.datatables.crmTime = $('#dataTableCrmTime').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmTimeIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary crmTime-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger crmTime-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __crmTimeIndex.removeOverlay($('#container-crm-time'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "time_id", targets: colNum++, data: "time_id"},
                    {name: "time_nome", targets: colNum++, data: "time_nome"},
                    {name: "time_localizacao", targets: colNum++, data: "time_localizacao"},
                    {name: "time_situacao", targets: colNum++, data: "time_situacao"},
                    {name: "time_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableCrmTime').on('click', '.crmTime-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmTimeIndex.options.datatables.crmTime.fnGetData($pai);

                if (!__crmTimeIndex.options.ajax) {
                    location.href = __crmTimeIndex.options.url.edit + '/' + data['time_id'];
                } else {
                    __crmTimeIndex.addOverlay(
                        $('#container-crm-time'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmTimeAdd().pesquisaCrmTime(
                        data['time_id'],
                        function (data) { __crmTimeIndex.showModal(data); }
                    );
                }
            });


            $('#btn-crm-time-add').click(function (e) {
                if (__crmTimeIndex.options.ajax) {
                    __crmTimeIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableCrmTime').on('click', '.crmTime-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmTimeIndex.options.datatables.crmTime.fnGetData($pai);
                var arrRemover = {
                    timeId: data['time_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de time "' + data['time_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmTimeIndex.addOverlay(
                                $('#container-crm-time'), 'Aguarde, solicitando remoção de registro de time...'
                            );
                            $.ajax({
                                url: __crmTimeIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmTimeIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de time:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmTimeIndex.removeOverlay($('#container-crm-time'));
                                    } else {
                                        __crmTimeIndex.reloadDataTableCrmTime();
                                        __crmTimeIndex.showNotificacaoSuccess(
                                            "Registro de time removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#crm-time-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['timeId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['timeId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#timeSituacao").select2('val', data['timeSituacao']).trigger("change");

            $('#crm-time-modal .modal-title').html(actionTitle + ' time');
            $('#crm-time-modal').modal();
            __crmTimeIndex.removeOverlay($('#container-crm-time'));
        };

        this.reloadDataTableCrmTime = function () {
            this.getDataTableCrmTime().api().ajax.reload(null, false);
        };

        this.getDataTableCrmTime = function () {
            if (!__crmTimeIndex.options.datatables.crmTime) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmTime')) {
                    __crmTimeIndex.options.datatables.crmTime = $('#dataTableCrmTime').DataTable();
                } else {
                    __crmTimeIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmTimeIndex.options.datatables.crmTime;
        };

        this.run = function (opts) {
            __crmTimeIndex.setDefaults(opts);
            __crmTimeIndex.setSteps();
        };
    };

    $.crmTimeIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-time.index");

        if (!obj) {
            obj = new CrmTimeIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-time.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);