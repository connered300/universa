(function ($, window, document) {
    'use strict';

    var CrmTimeAdd = function () {
        VersaShared.call(this);
        var __crmTimeAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#crm-time-wizard',
            formElement: '#crm-time-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#timeSituacao").select2({minimumInputLength: -1, allowClear: true, language: 'pt-BR'}).trigger("change");
        };

        this.setValidations = function () {
            __crmTimeAdd.options.validator.settings.rules =
            {timeNome: {maxlength: 70}, timeLocalizacao: {maxlength: 200}, timeSituacao: {maxlength: 7},};
            __crmTimeAdd.options.validator.settings.messages = {
                timeNome: {maxlength: 'Tamanho máximo: 70!'}, timeLocalizacao: {maxlength: 'Tamanho máximo: 200!'},
                timeSituacao: {maxlength: 'Tamanho máximo: 7!'},
            };

            $(__crmTimeAdd.options.formElement).submit(function (e) {
                var ok = $(__crmTimeAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__crmTimeAdd.options.formElement);

                if (__crmTimeAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __crmTimeAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __crmTimeAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__crmTimeAdd.options.listagem) {
                                    $.crmTimeIndex().reloadDataTableCrmTime();
                                    __crmTimeAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#crm-time-modal').modal('hide');
                                }

                                __crmTimeAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaCrmTime = function (time_id, callback) {
            var $form = $(__crmTimeAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmTimeAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + time_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmTimeAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmTimeAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmTimeAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmTimeAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.crmTimeAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-time.add");

        if (!obj) {
            obj = new CrmTimeAdd();
            obj.run(params);
            $(window).data('universa.crm.crm-time.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);