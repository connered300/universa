(function ($, window, document) {
    'use strict';

    var CrmLeadInteresseAdd = function () {
        VersaShared.call(this);
        var __crmLeadInteresseAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                crmLead: '',
                crmInteresse: ''
            },
            data: {},
            value: {
                pes: null,
                interesse: null
            },
            datatables: {},
            wizardElement: '#crm-lead-interesse-wizard',
            formElement: '#crm-lead-interesse-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#pes").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __crmLeadInteresseAdd.options.url.crmLead,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.pes_id;
                            el.text = el.pes_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__crmLeadInteresseAdd.getPes()) {
                $('#pes').select2("data", __crmLeadInteresseAdd.getPes());
            }
            $("#interesse").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __crmLeadInteresseAdd.options.url.crmInteresse,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        var data = {query: query};
                        return data;
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.interesse_id;
                            el.text = el.interesse_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            if (__crmLeadInteresseAdd.getInteresse()) {
                $('#interesse').select2("data", __crmLeadInteresseAdd.getInteresse());
            }
        };

        this.setPes = function (pes) {
            this.options.value.pes = pes || null;
        };

        this.getPes = function () {
            return this.options.value.pes || null;
        };

        this.setInteresse = function (interesse) {
            this.options.value.interesse = interesse || null;
        };

        this.getInteresse = function () {
            return this.options.value.interesse || null;
        };
        this.setValidations = function () {
            __crmLeadInteresseAdd.options.validator.settings.rules =
            {pes: {number: true, required: true}, interesse: {number: true, required: true},};
            __crmLeadInteresseAdd.options.validator.settings.messages = {
                pes: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                interesse: {number: 'Número inválido!', required: 'Campo obrigatório!'},
            };

            $(__crmLeadInteresseAdd.options.formElement).submit(function (e) {
                var ok = $(__crmLeadInteresseAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__crmLeadInteresseAdd.options.formElement);

                if (__crmLeadInteresseAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __crmLeadInteresseAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __crmLeadInteresseAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__crmLeadInteresseAdd.options.listagem) {
                                    $.crmLeadInteresseIndex().reloadDataTableCrmLeadInteresse();
                                    __crmLeadInteresseAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#crm-lead-interesse-modal').modal('hide');
                                }

                                __crmLeadInteresseAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaCrmLeadInteresse = function (lead_interesse_id, callback) {
            var $form = $(__crmLeadInteresseAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmLeadInteresseAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + lead_interesse_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmLeadInteresseAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmLeadInteresseAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmLeadInteresseAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmLeadInteresseAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.crmLeadInteresseAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-lead-interesse.add");

        if (!obj) {
            obj = new CrmLeadInteresseAdd();
            obj.run(params);
            $(window).data('universa.crm.crm-lead-interesse.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);