(function ($, window, document) {
    'use strict';
    var CrmLeadInteresseIndex = function () {
        VersaShared.call(this);
        var __crmLeadInteresseIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                crmLeadInteresse: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __crmLeadInteresseIndex.options.datatables.crmLeadInteresse = $('#dataTableCrmLeadInteresse').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmLeadInteresseIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary crmLeadInteresse-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger crmLeadInteresse-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __crmLeadInteresseIndex.removeOverlay($('#container-crm-lead-interesse'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "lead_interesse_id", targets: colNum++, data: "lead_interesse_id"},
                    {name: "pes_id", targets: colNum++, data: "pes_id"},
                    {name: "interesse_id", targets: colNum++, data: "interesse_id"},
                    {name: "lead_interesse_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableCrmLeadInteresse').on('click', '.crmLeadInteresse-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmLeadInteresseIndex.options.datatables.crmLeadInteresse.fnGetData($pai);

                if (!__crmLeadInteresseIndex.options.ajax) {
                    location.href = __crmLeadInteresseIndex.options.url.edit + '/' + data['lead_interesse_id'];
                } else {
                    __crmLeadInteresseIndex.addOverlay(
                        $('#container-crm-lead-interesse'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmLeadInteresseAdd().pesquisaCrmLeadInteresse(
                        data['lead_interesse_id'],
                        function (data) { __crmLeadInteresseIndex.showModal(data); }
                    );
                }
            });


            $('#btn-crm-lead-interesse-add').click(function (e) {
                if (__crmLeadInteresseIndex.options.ajax) {
                    __crmLeadInteresseIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableCrmLeadInteresse').on('click', '.crmLeadInteresse-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmLeadInteresseIndex.options.datatables.crmLeadInteresse.fnGetData($pai);
                var arrRemover = {
                    leadInteresseId: data['lead_interesse_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de lead interesse "' + data['pes_id'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmLeadInteresseIndex.addOverlay(
                                $('#container-crm-lead-interesse'),
                                'Aguarde, solicitando remoção de registro de lead interesse...'
                            );
                            $.ajax({
                                url: __crmLeadInteresseIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmLeadInteresseIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de lead interesse:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmLeadInteresseIndex.removeOverlay($('#container-crm-lead-interesse'));
                                    } else {
                                        __crmLeadInteresseIndex.reloadDataTableCrmLeadInteresse();
                                        __crmLeadInteresseIndex.showNotificacaoSuccess(
                                            "Registro de lead interesse removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#crm-lead-interesse-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['leadInteresseId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['leadInteresseId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#pes").select2('data', data['pes']).trigger("change");
            $("#interesse").select2('data', data['interesse']).trigger("change");

            $('#crm-lead-interesse-modal .modal-title').html(actionTitle + ' lead interesse');
            $('#crm-lead-interesse-modal').modal();
            __crmLeadInteresseIndex.removeOverlay($('#container-crm-lead-interesse'));
        };

        this.reloadDataTableCrmLeadInteresse = function () {
            this.getDataTableCrmLeadInteresse().api().ajax.reload(null, false);
        };

        this.getDataTableCrmLeadInteresse = function () {
            if (!__crmLeadInteresseIndex.options.datatables.crmLeadInteresse) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmLeadInteresse')) {
                    __crmLeadInteresseIndex.options.datatables.crmLeadInteresse =
                        $('#dataTableCrmLeadInteresse').DataTable();
                } else {
                    __crmLeadInteresseIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmLeadInteresseIndex.options.datatables.crmLeadInteresse;
        };

        this.run = function (opts) {
            __crmLeadInteresseIndex.setDefaults(opts);
            __crmLeadInteresseIndex.setSteps();
        };
    };

    $.crmLeadInteresseIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-lead-interesse.index");

        if (!obj) {
            obj = new CrmLeadInteresseIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-lead-interesse.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);