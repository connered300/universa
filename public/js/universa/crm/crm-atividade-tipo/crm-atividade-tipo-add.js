(function ($, window, document) {
    'use strict';

    var CrmAtividadeTipoAdd = function () {
        VersaShared.call(this);
        var __crmAtividadeTipoAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {crmAtividadeTipo: ''},
            data: {},
            value: {atividadePai: null},
            datatables: {},
            wizardElement: '#crm-atividade-tipo-wizard',
            formElement: '#crm-atividade-tipo-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            $("#atividadePai").select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __crmAtividadeTipoAdd.options.url.crmAtividadeTipo,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query, atividadeTipoId: $('#atividadeTipoId').val(), apenasNivel1: true};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.atividade_tipo_id;
                            el.text = el.atividade_tipo_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $('#atividadePai').select2("data", __crmAtividadeTipoAdd.getAtividadePai());

            $(
                "#atividadeTipo,#atividadeComentarioObrigatorio, #atividadeCaracterizaDesistencia," +
                "#atividadeAgendamento, #atividadeLeadStatus"
            ).select2(
                {allowClear: true, language: 'pt-BR', minimumInputLength: -1}
            ).trigger("change");
        };

        this.setAtividadePai = function (atividadePai) {
            this.options.value.atividadePai = atividadePai || null;
        };

        this.getAtividadePai = function () {
            return this.options.value.atividadePai || null;
        };
        this.setValidations = function () {
            __crmAtividadeTipoAdd.options.validator.settings.rules = {
                atividadeTipoNome: {maxlength: 255}, atividadePai: {number: true},
                atividadeTipo: {maxlength: 8, required: true},
                atividadeComentarioObrigatorio: {maxlength: 3, required: true},
                atividadeCaracterizaDesistencia: {maxlength: 3, required: true},
                atividadeAgendamento: {maxlength: 8, required: true},
                atividadeLeadStatus: {required: true}
            };
            __crmAtividadeTipoAdd.options.validator.settings.messages = {
                atividadeTipoNome: {maxlength: 'Tamanho máximo: 255!'}, atividadePai: {number: 'Número inválido!'},
                atividadeTipo: {maxlength: 'Tamanho máximo: 8!', required: 'Campo obrigatório!'},
                atividadeComentarioObrigatorio: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
                atividadeCaracterizaDesistencia: {maxlength: 'Tamanho máximo: 3!', required: 'Campo obrigatório!'},
                atividadeAgendamento: {maxlength: 'Tamanho máximo: 8!', required: 'Campo obrigatório!'},
                atividadeLeadStatus: {required: 'Campo obrigatório!'}
            };

            $(__crmAtividadeTipoAdd.options.formElement).submit(function (e) {
                var ok = $(__crmAtividadeTipoAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__crmAtividadeTipoAdd.options.formElement);

                if (__crmAtividadeTipoAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __crmAtividadeTipoAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __crmAtividadeTipoAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__crmAtividadeTipoAdd.options.listagem) {
                                    $.crmAtividadeTipoIndex().reloadDataTableCrmAtividadeTipo();
                                    __crmAtividadeTipoAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#crm-atividade-tipo-modal').modal('hide');
                                }

                                __crmAtividadeTipoAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaCrmAtividadeTipo = function (atividade_tipo_id, callback) {
            var $form = $(__crmAtividadeTipoAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmAtividadeTipoAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + atividade_tipo_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmAtividadeTipoAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmAtividadeTipoAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmAtividadeTipoAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmAtividadeTipoAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.crmAtividadeTipoAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-atividade-tipo.add");

        if (!obj) {
            obj = new CrmAtividadeTipoAdd();
            obj.run(params);
            $(window).data('universa.crm.crm-atividade-tipo.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);