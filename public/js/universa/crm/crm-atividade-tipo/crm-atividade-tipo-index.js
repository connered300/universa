(function ($, window, document) {
    'use strict';
    var CrmAtividadeTipoIndex = function () {
        VersaShared.call(this);
        var __crmAtividadeTipoIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                crmAtividadeTipo: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __crmAtividadeTipoIndex.options.datatables.crmAtividadeTipo = $('#dataTableCrmAtividadeTipo').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmAtividadeTipoIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __crmAtividadeTipoIndex.createBtnGroup(btns);
                        }

                        __crmAtividadeTipoIndex.removeOverlay($('#container-crm-atividade-tipo'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "atividade_tipo_id", targets: colNum++, data: "atividade_tipo_id"},
                    {name: "atividade_tipo_nome", targets: colNum++, data: "atividade_tipo_nome"},
                    {name: "atividade_pai_nome", targets: colNum++, data: "atividade_pai_nome"},
                    {name: "atividade_tipo", targets: colNum++, data: "atividade_tipo"},
                    {
                        name: "atividade_comentario_obrigatorio", targets: colNum++,
                        data: "atividade_comentario_obrigatorio"
                    },
                    {
                        name: "atividade_caracteriza_desistencia", targets: colNum++,
                        data: "atividade_caracteriza_desistencia"
                    },
                    {name: "atividade_agendamento", targets: colNum++, data: "atividade_agendamento"},
                    {name: "atividade_tipo_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableCrmAtividadeTipo').on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmAtividadeTipoIndex.options.datatables.crmAtividadeTipo.fnGetData($pai);

                if (!__crmAtividadeTipoIndex.options.ajax) {
                    location.href = __crmAtividadeTipoIndex.options.url.edit + '/' + data['atividade_tipo_id'];
                } else {
                    __crmAtividadeTipoIndex.addOverlay(
                        $('#container-crm-atividade-tipo'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmAtividadeTipoAdd().pesquisaCrmAtividadeTipo(
                        data['atividade_tipo_id'],
                        function (data) {
                            __crmAtividadeTipoIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-crm-atividade-tipo-add').click(function (e) {
                if (__crmAtividadeTipoIndex.options.ajax) {
                    __crmAtividadeTipoIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableCrmAtividadeTipo').on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmAtividadeTipoIndex.options.datatables.crmAtividadeTipo.fnGetData($pai);
                var arrRemover = {
                    atividadeTipoId: data['atividade_tipo_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de tipo atividade "' + data['atividade_tipo_nome'] +
                    '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmAtividadeTipoIndex.addOverlay(
                                $('#container-crm-atividade-tipo'),
                                'Aguarde, solicitando remoção de registro de tipo atividade...'
                            );
                            $.ajax({
                                url: __crmAtividadeTipoIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmAtividadeTipoIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de tipo atividade:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmAtividadeTipoIndex.removeOverlay($('#container-crm-atividade-tipo'));
                                    } else {
                                        __crmAtividadeTipoIndex.reloadDataTableCrmAtividadeTipo();
                                        __crmAtividadeTipoIndex.showNotificacaoSuccess(
                                            "Registro de tipo atividade removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';

            var $form  = $('#crm-atividade-tipo-form'),
                $modal = $('#crm-atividade-tipo-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['atividadeTipoId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['atividadeTipoId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#atividadePai").select2('data', data['atividadePai']).trigger("change");
            $("#atividadeTipo").select2('val', data['atividadeTipo']).trigger("change");
            $("#atividadeComentarioObrigatorio").select2('val',
                data['atividadeComentarioObrigatorio']).trigger("change");
            $("#atividadeCaracterizaDesistencia").select2('val',
                data['atividadeCaracterizaDesistencia']).trigger("change");
            $("#atividadeLeadStatus").select2('val',
                data['atividadeLeadStatus'] || '').trigger("change");


            var $atividadeAgendamento = $("#atividadeAgendamento");
            $atividadeAgendamento.select2('val', data['atividadeAgendamento']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' tipo de atividade');
            $modal.modal();

            __crmAtividadeTipoIndex.removeOverlay($('#container-crm-atividade-tipo'));
        };

        this.reloadDataTableCrmAtividadeTipo = function () {
            this.getDataTableCrmAtividadeTipo().api().ajax.reload(null, false);
        };

        this.getDataTableCrmAtividadeTipo = function () {
            if (!__crmAtividadeTipoIndex.options.datatables.crmAtividadeTipo) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmAtividadeTipo')) {
                    __crmAtividadeTipoIndex.options.datatables.crmAtividadeTipo =
                        $('#dataTableCrmAtividadeTipo').DataTable();
                } else {
                    __crmAtividadeTipoIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmAtividadeTipoIndex.options.datatables.crmAtividadeTipo;
        };

        this.run = function (opts) {
            __crmAtividadeTipoIndex.setDefaults(opts);
            __crmAtividadeTipoIndex.setSteps();
        };
    };

    $.crmAtividadeTipoIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-atividade-tipo.index");

        if (!obj) {
            obj = new CrmAtividadeTipoIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-atividade-tipo.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);