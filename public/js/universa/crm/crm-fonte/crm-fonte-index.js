(function ($, window, document) {
    'use strict';
    var CrmFonteIndex = function () {
        VersaShared.call(this);
        var __crmFonteIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                crmFonte: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __crmFonteIndex.options.datatables.crmFonte = $('#dataTableCrmFonte').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmFonteIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary crmFonte-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger crmFonte-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __crmFonteIndex.removeOverlay($('#container-crm-fonte'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "fonte_id", targets: colNum++, data: "fonte_id"},
                    {name: "fonte_nome", targets: colNum++, data: "fonte_nome"},
                    {name: "fonte_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableCrmFonte').on('click', '.crmFonte-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmFonteIndex.options.datatables.crmFonte.fnGetData($pai);

                if (!__crmFonteIndex.options.ajax) {
                    location.href = __crmFonteIndex.options.url.edit + '/' + data['fonte_id'];
                } else {
                    __crmFonteIndex.addOverlay(
                        $('#container-crm-fonte'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmFonteAdd().pesquisaCrmFonte(
                        data['fonte_id'],
                        function (data) { __crmFonteIndex.showModal(data); }
                    );
                }
            });


            $('#btn-crm-fonte-add').click(function (e) {
                if (__crmFonteIndex.options.ajax) {
                    __crmFonteIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableCrmFonte').on('click', '.crmFonte-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmFonteIndex.options.datatables.crmFonte.fnGetData($pai);
                var arrRemover = {
                    fonteId: data['fonte_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de fonte "' + data['fonte_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmFonteIndex.addOverlay(
                                $('#container-crm-fonte'), 'Aguarde, solicitando remoção de registro de fonte...'
                            );
                            $.ajax({
                                url: __crmFonteIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmFonteIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de fonte:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmFonteIndex.removeOverlay($('#container-crm-fonte'));
                                    } else {
                                        __crmFonteIndex.reloadDataTableCrmFonte();
                                        __crmFonteIndex.showNotificacaoSuccess(
                                            "Registro de fonte removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#crm-fonte-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['fonteId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['fonteId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $('#crm-fonte-modal .modal-title').html(actionTitle + ' fonte');
            $('#crm-fonte-modal').modal();
            __crmFonteIndex.removeOverlay($('#container-crm-fonte'));
        };

        this.reloadDataTableCrmFonte = function () {
            this.getDataTableCrmFonte().api().ajax.reload(null, false);
        };

        this.getDataTableCrmFonte = function () {
            if (!__crmFonteIndex.options.datatables.crmFonte) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmFonte')) {
                    __crmFonteIndex.options.datatables.crmFonte = $('#dataTableCrmFonte').DataTable();
                } else {
                    __crmFonteIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmFonteIndex.options.datatables.crmFonte;
        };

        this.run = function (opts) {
            __crmFonteIndex.setDefaults(opts);
            __crmFonteIndex.setSteps();
        };
    };

    $.crmFonteIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-fonte.index");

        if (!obj) {
            obj = new CrmFonteIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-fonte.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);