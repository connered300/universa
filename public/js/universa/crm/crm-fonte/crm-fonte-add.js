(function ($, window, document) {
    'use strict';

    var CrmFonteAdd = function () {
        VersaShared.call(this);
        var __crmFonteAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#crm-fonte-wizard',
            formElement: '#crm-fonte-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __crmFonteAdd.options.validator.settings.rules = {fonteNome: {maxlength: 70},};
            __crmFonteAdd.options.validator.settings.messages = {fonteNome: {maxlength: 'Tamanho máximo: 70!'},};

            $(__crmFonteAdd.options.formElement).submit(function (e) {
                var ok = $(__crmFonteAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__crmFonteAdd.options.formElement);

                if (__crmFonteAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __crmFonteAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __crmFonteAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__crmFonteAdd.options.listagem) {
                                    $.crmFonteIndex().reloadDataTableCrmFonte();
                                    __crmFonteAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#crm-fonte-modal').modal('hide');
                                }

                                __crmFonteAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaCrmFonte = function (fonte_id, callback) {
            var $form = $(__crmFonteAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmFonteAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + fonte_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmFonteAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmFonteAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmFonteAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmFonteAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.crmFonteAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-fonte.add");

        if (!obj) {
            obj = new CrmFonteAdd();
            obj.run(params);
            $(window).data('universa.crm.crm-fonte.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);