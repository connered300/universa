(function ($, window, document) {
    'use strict';

    var CrmInteresseAdd = function () {
        VersaShared.call(this);
        var __crmInteresseAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#crm-interesse-wizard',
            formElement: '#crm-interesse-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __crmInteresseAdd.options.validator.settings.rules = {
                interesseDescricao: {maxlength: 200, required: true},
            };
            __crmInteresseAdd.options.validator.settings.messages = {
                interesseDescricao: {maxlength: 'Tamanho máximo: 200!', required: 'Campo obrigatório!'},
            };

            $(__crmInteresseAdd.options.formElement).submit(function (e) {
                var ok = $(__crmInteresseAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__crmInteresseAdd.options.formElement);

                if (__crmInteresseAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __crmInteresseAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __crmInteresseAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__crmInteresseAdd.options.listagem) {
                                    $.crmInteresseIndex().reloadDataTableCrmInteresse();
                                    __crmInteresseAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#crm-interesse-modal').modal('hide');
                                }

                                __crmInteresseAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaCrmInteresse = function (interesse_id, callback) {
            var $form = $(__crmInteresseAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmInteresseAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + interesse_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmInteresseAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmInteresseAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmInteresseAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmInteresseAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.crmInteresseAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-interesse.add");

        if (!obj) {
            obj = new CrmInteresseAdd();
            obj.run(params);
            $(window).data('universa.crm.crm-interesse.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);