(function ($, window, document) {
    'use strict';
    var CrmInteresseIndex = function () {
        VersaShared.call(this);
        var __crmInteresseIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                crmInteresse: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __crmInteresseIndex.options.datatables.crmInteresse = $('#dataTableCrmInteresse').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmInteresseIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary crmInteresse-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger crmInteresse-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __crmInteresseIndex.removeOverlay($('#container-crm-interesse'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "interesse_id", targets: colNum++, data: "interesse_id"},
                    {name: "interesse_descricao", targets: colNum++, data: "interesse_descricao"},
                    {name: "interesse_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableCrmInteresse').on('click', '.crmInteresse-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmInteresseIndex.options.datatables.crmInteresse.fnGetData($pai);

                if (!__crmInteresseIndex.options.ajax) {
                    location.href = __crmInteresseIndex.options.url.edit + '/' + data['interesse_id'];
                } else {
                    __crmInteresseIndex.addOverlay(
                        $('#container-crm-interesse'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmInteresseAdd().pesquisaCrmInteresse(
                        data['interesse_id'],
                        function (data) { __crmInteresseIndex.showModal(data); }
                    );
                }
            });


            $('#btn-crm-interesse-add').click(function (e) {
                if (__crmInteresseIndex.options.ajax) {
                    __crmInteresseIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableCrmInteresse').on('click', '.crmInteresse-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmInteresseIndex.options.datatables.crmInteresse.fnGetData($pai);
                var arrRemover = {
                    interesseId: data['interesse_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de interesse "' + data['interesse_descricao'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmInteresseIndex.addOverlay(
                                $('#container-crm-interesse'),
                                'Aguarde, solicitando remoção de registro de interesse...'
                            );
                            $.ajax({
                                url: __crmInteresseIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmInteresseIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de interesse:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmInteresseIndex.removeOverlay($('#container-crm-interesse'));
                                    } else {
                                        __crmInteresseIndex.reloadDataTableCrmInteresse();
                                        __crmInteresseIndex.showNotificacaoSuccess(
                                            "Registro de interesse removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#crm-interesse-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['interesseId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['interesseId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $('#crm-interesse-modal .modal-title').html(actionTitle + ' interesse');
            $('#crm-interesse-modal').modal();
            __crmInteresseIndex.removeOverlay($('#container-crm-interesse'));
        };

        this.reloadDataTableCrmInteresse = function () {
            this.getDataTableCrmInteresse().api().ajax.reload(null, false);
        };

        this.getDataTableCrmInteresse = function () {
            if (!__crmInteresseIndex.options.datatables.crmInteresse) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmInteresse')) {
                    __crmInteresseIndex.options.datatables.crmInteresse = $('#dataTableCrmInteresse').DataTable();
                } else {
                    __crmInteresseIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmInteresseIndex.options.datatables.crmInteresse;
        };

        this.run = function (opts) {
            __crmInteresseIndex.setDefaults(opts);
            __crmInteresseIndex.setSteps();
        };
    };

    $.crmInteresseIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-interesse.index");

        if (!obj) {
            obj = new CrmInteresseIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-interesse.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);