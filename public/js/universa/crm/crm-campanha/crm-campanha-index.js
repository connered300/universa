(function ($, window, document) {
    'use strict';
    var CrmCampanhaIndex = function () {
        VersaShared.call(this);
        var __crmCampanhaIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                crmCampanha: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            var $dataTables = $('#dataTableCrmCampanha');

            __crmCampanhaIndex.options.datatables.crmCampanha = $dataTables.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmCampanhaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary crmCampanha-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger crmCampanha-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __crmCampanhaIndex.removeOverlay($('#container-crm-campanha'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "campanha_id", targets: colNum++, data: "campanha_id"},
                    {name: "campanha_nome", targets: colNum++, data: "campanha_nome"},
                    {name: "campanha_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTables.on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmCampanhaIndex.options.datatables.crmCampanha.fnGetData($pai);

                if (!__crmCampanhaIndex.options.ajax) {
                    location.href = __crmCampanhaIndex.options.url.edit + '/' + data['campanha_id'];
                } else {
                    __crmCampanhaIndex.addOverlay(
                        $('#container-crm-campanha'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmCampanhaAdd().pesquisaCrmCampanha(
                        data['campanha_id'],
                        function (data) { __crmCampanhaIndex.showModal(data); }
                    );
                }
            });


            $('#btn-crm-campanha-add').click(function (e) {
                if (__crmCampanhaIndex.options.ajax) {
                    __crmCampanhaIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTables.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmCampanhaIndex.options.datatables.crmCampanha.fnGetData($pai);
                var arrRemover = {
                    campanhaId: data['campanha_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de campanha "' + data['campanha_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmCampanhaIndex.addOverlay(
                                $('#container-crm-campanha'), 'Aguarde, solicitando remoção de registro de campanha...'
                            );
                            $.ajax({
                                url: __crmCampanhaIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmCampanhaIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de campanha:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmCampanhaIndex.removeOverlay($('#container-crm-campanha'));
                                    } else {
                                        __crmCampanhaIndex.reloadDataTableCrmCampanha();
                                        __crmCampanhaIndex.showNotificacaoSuccess(
                                            "Registro de campanha removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#crm-campanha-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['campanhaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['campanhaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $('#crm-campanha-modal .modal-title').html(actionTitle + ' campanha');
            $('#crm-campanha-modal').modal();
            __crmCampanhaIndex.removeOverlay($('#container-crm-campanha'));
        };

        this.reloadDataTableCrmCampanha = function () {
            this.getDataTableCrmCampanha().api().ajax.reload(null, false);
        };

        this.getDataTableCrmCampanha = function () {
            if (!__crmCampanhaIndex.options.datatables.crmCampanha) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmCampanha')) {
                    __crmCampanhaIndex.options.datatables.crmCampanha = $('#dataTableCrmCampanha').DataTable();
                } else {
                    __crmCampanhaIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmCampanhaIndex.options.datatables.crmCampanha;
        };

        this.run = function (opts) {
            __crmCampanhaIndex.setDefaults(opts);
            __crmCampanhaIndex.setSteps();
        };
    };

    $.crmCampanhaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-campanha.index");

        if (!obj) {
            obj = new CrmCampanhaIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-campanha.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);