(function ($, window, document) {
    'use strict';

    var CrmCampanhaAdd = function () {
        VersaShared.call(this);
        var __crmCampanhaAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#crm-campanha-wizard',
            formElement: '#crm-campanha-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __crmCampanhaAdd.options.validator.settings.rules = {campanhaNome: {maxlength: 70}};
            __crmCampanhaAdd.options.validator.settings.messages = {campanhaNome: {maxlength: 'Tamanho máximo: 70!'}};

            $(__crmCampanhaAdd.options.formElement).submit(function (e) {
                var ok = $(__crmCampanhaAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__crmCampanhaAdd.options.formElement);

                if (__crmCampanhaAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __crmCampanhaAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __crmCampanhaAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__crmCampanhaAdd.options.listagem) {
                                    $.crmCampanhaIndex().reloadDataTableCrmCampanha();
                                    __crmCampanhaAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#crm-campanha-modal').modal('hide');
                                }

                                __crmCampanhaAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaCrmCampanha = function (campanha_id, callback) {
            var $form = $(__crmCampanhaAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmCampanhaAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + campanha_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmCampanhaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmCampanhaAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmCampanhaAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmCampanhaAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.crmCampanhaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-campanha.add");

        if (!obj) {
            obj = new CrmCampanhaAdd();
            obj.run(params);
            $(window).data('universa.crm.crm-campanha.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);