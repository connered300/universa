(function ($, window, document) {
    'use strict';

    var CrmOperadorAdd = function () {
        VersaShared.call(this);
        var __crmOperadorAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                acessoPessoas: '',
                crmTime: ''
            },
            data: {},
            value: {
                operador: null,
                time: null
            },
            datatables: {},
            wizardElement: '#crm-operador-wizard',
            formElement: '#crm-operador-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $operador = $("#operador"), $time = $('#time'), $operadorSituacao = $("#operadorSituacao");

            $operador.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __crmOperadorAdd.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.text = el.login;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $operador.select2("data", __crmOperadorAdd.getOperador());

            $time.select2({
                language: 'pt-BR',
                allowClear: true,
                ajax: {
                    url: __crmOperadorAdd.options.url.crmTime,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.time_id;
                            el.text = el.time_nome;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $time.select2("data", __crmOperadorAdd.getTime());

            $operadorSituacao.select2({
                allowClear: true,
                language: 'pt-BR'
            }).trigger("change");

            __crmOperadorAdd.inciarElementoMoeda($("#operadorPercentual"));
            __crmOperadorAdd.inciarElementoInteiro($("#operadorRamal"));
            __crmOperadorAdd.iniciarElementoTimePicker(
                $('#operadorEntrada1,#operadorEntrada2,#operadorSaida1,#operadorSaida2')
            );
        };

        this.setOperador = function (operador) {
            this.options.value.operador = operador || null;
        };

        this.getOperador = function () {
            return this.options.value.operador || null;
        };

        this.setTime = function (time) {
            this.options.value.time = time || null;
        };

        this.getTime = function () {
            return this.options.value.time || null;
        };
        this.setValidations = function () {
            __crmOperadorAdd.options.validator.settings.rules = {
                operador: {number: true, required: true},
                time: {number: true, required: true},
                operadorSituacao: {maxlength: 7, required: true},
                operadorRamal: {maxlength: 7},
                operadorPercentual: {required: true}
            };
            __crmOperadorAdd.options.validator.settings.messages = {
                operador: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                time: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                operadorSituacao: {maxlength: 'Tamanho máximo: 7!', required: 'Campo obrigatório!'},
                operadorRamal: {maxlength: 'Tamanho máximo: 6!'},
                operadorPercentual: {required: 'Campo obrigatório!'}
            };

            $(__crmOperadorAdd.options.formElement).submit(function (e) {
                var ok = $(__crmOperadorAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__crmOperadorAdd.options.formElement);

                if (__crmOperadorAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __crmOperadorAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __crmOperadorAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__crmOperadorAdd.options.listagem) {
                                    $.crmOperadorIndex().reloadDataTableCrmOperador();
                                    __crmOperadorAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#crm-operador-modal').modal('hide');
                                }

                                __crmOperadorAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaCrmOperador = function (operador_id, callback) {
            var $form = $(__crmOperadorAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmOperadorAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + operador_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmOperadorAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmOperadorAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmOperadorAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmOperadorAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.crmOperadorAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-operador.add");

        if (!obj) {
            obj = new CrmOperadorAdd();
            obj.run(params);
            $(window).data('universa.crm.crm-operador.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);