(function ($, window, document) {
    'use strict';
    var CrmOperadorIndex = function () {
        VersaShared.call(this);
        var __crmOperadorIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                crmOperador: null
            }
        };

        this.setSteps = function () {
            var colNum = 0;
            __crmOperadorIndex.options.datatables.crmOperador = $('#dataTableCrmOperador').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmOperadorIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;

                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['operador_data_ultimo_recebimento_formatado'] = __crmOperadorIndex
                                .formatDateTime(data[row]['operador_data_ultimo_recebimento']);
                            data[row]['acao'] = __crmOperadorIndex.createBtnGroup(btns);
                        }

                        __crmOperadorIndex.removeOverlay($('#container-crm-operador'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "login", targets: colNum++, data: "login"},
                    {name: "time_nome", targets: colNum++, data: "time_nome"},
                    {name: "operador_percentual", targets: colNum++, data: "operador_percentual"},
                    {
                        name: "operador_data_ultimo_recebimento",
                        targets: colNum++,
                        data: "operador_data_ultimo_recebimento_formatado"
                    },
                    {name: "operador_situacao", targets: colNum++, data: "operador_situacao"},
                    {name: "operador_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableCrmOperador').on('click', '.item-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmOperadorIndex.options.datatables.crmOperador.fnGetData($pai);

                if (!__crmOperadorIndex.options.ajax) {
                    location.href = __crmOperadorIndex.options.url.edit + '/' + data['operador_id'];
                } else {
                    __crmOperadorIndex.addOverlay(
                        $('#container-crm-operador'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmOperadorAdd().pesquisaCrmOperador(
                        data['operador_id'],
                        function (data) {
                            __crmOperadorIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-crm-operador-add').click(function (e) {
                if (__crmOperadorIndex.options.ajax) {
                    __crmOperadorIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableCrmOperador').on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmOperadorIndex.options.datatables.crmOperador.fnGetData($pai);
                var arrRemover = {
                    operadorId: data['operador_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de operador "' + data['login'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmOperadorIndex.addOverlay(
                                $('#container-crm-operador'), 'Aguarde, solicitando remoção de registro de operador...'
                            );
                            $.ajax({
                                url: __crmOperadorIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmOperadorIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de operador:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmOperadorIndex.removeOverlay($('#container-crm-operador'));
                                    } else {
                                        __crmOperadorIndex.reloadDataTableCrmOperador();
                                        __crmOperadorIndex.showNotificacaoSuccess(
                                            "Registro de operador removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#crm-operador-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['operadorId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['operadorId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            $("#operador").select2('data', data['operador']).trigger("change");
            $("#time").select2('data', data['time']).trigger("change");
            $("#operadorSituacao").select2('val', data['operadorSituacao']).trigger("change");

            $('#crm-operador-modal').find('.modal-title').html(actionTitle + ' operador');
            $('#crm-operador-modal').modal();
            __crmOperadorIndex.removeOverlay($('#container-crm-operador'));
        };

        this.reloadDataTableCrmOperador = function () {
            this.getDataTableCrmOperador().api().ajax.reload(null, false);
        };

        this.getDataTableCrmOperador = function () {
            if (!__crmOperadorIndex.options.datatables.crmOperador) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmOperador')) {
                    __crmOperadorIndex.options.datatables.crmOperador = $('#dataTableCrmOperador').DataTable();
                } else {
                    __crmOperadorIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmOperadorIndex.options.datatables.crmOperador;
        };

        this.run = function (opts) {
            __crmOperadorIndex.setDefaults(opts);
            __crmOperadorIndex.setSteps();
        };
    };

    $.crmOperadorIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-operador.index");

        if (!obj) {
            obj = new CrmOperadorIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-operador.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);