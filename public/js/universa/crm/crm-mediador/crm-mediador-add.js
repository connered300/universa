(function ($, window, document) {
    'use strict';

    var CrmMediadorAdd = function () {
        VersaShared.call(this);
        var __crmMediadorAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {},
            data: {},
            value: {},
            datatables: {},
            wizardElement: '#crm-mediador-wizard',
            formElement: '#crm-mediador-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
        };

        this.setValidations = function () {
            __crmMediadorAdd.options.validator.settings.rules = {mediadorNome: {maxlength: 70},};
            __crmMediadorAdd.options.validator.settings.messages = {mediadorNome: {maxlength: 'Tamanho máximo: 70!'},};

            $(__crmMediadorAdd.options.formElement).submit(function (e) {
                var ok = $(__crmMediadorAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__crmMediadorAdd.options.formElement);

                if (__crmMediadorAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __crmMediadorAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __crmMediadorAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__crmMediadorAdd.options.listagem) {
                                    $.crmMediadorIndex().reloadDataTableCrmMediador();
                                    __crmMediadorAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#crm-mediador-modal').modal('hide');
                                }

                                __crmMediadorAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaCrmMediador = function (mediador_id, callback) {
            var $form = $(__crmMediadorAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __crmMediadorAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + mediador_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __crmMediadorAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __crmMediadorAdd.removeOverlay($form);
                },
                erro: function () {
                    __crmMediadorAdd.showNotificacaoDanger('Erro desconhecido!');
                    __crmMediadorAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.crmMediadorAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-mediador.add");

        if (!obj) {
            obj = new CrmMediadorAdd();
            obj.run(params);
            $(window).data('universa.crm.crm-mediador.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);