(function ($, window, document) {
    'use strict';
    var CrmMediadorIndex = function () {
        VersaShared.call(this);
        var __crmMediadorIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                crmMediador: null
            },
        };

        this.setSteps = function () {
            var colNum = 0;
            __crmMediadorIndex.options.datatables.crmMediador = $('#dataTableCrmMediador').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __crmMediadorIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btnGroup = '\
                        <div class="text-center">\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-xs btn-primary crmMediador-edit">\
                                    <i class="fa fa-pencil fa-inverse"></i>\
                                </button>\
                                <button type="button" class="btn btn-xs btn-danger crmMediador-remove">\
                                    <i class="fa fa-times fa-inverse"></i>\
                                </button>\
                            </div>\
                        </div>';

                        for (var row in data) {
                            data[row]['acao'] = btnGroup;
                        }

                        __crmMediadorIndex.removeOverlay($('#container-crm-mediador'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "mediador_id", targets: colNum++, data: "mediador_id"},
                    {name: "mediador_nome", targets: colNum++, data: "mediador_nome"},
                    {name: "mediador_id", targets: colNum++, data: "acao"}
                ],
                "dom": "fr" +
                       "t" +
                       "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $('#dataTableCrmMediador').on('click', '.crmMediador-edit', function (event) {
                var $pai = $(this).closest('tr');
                var data = __crmMediadorIndex.options.datatables.crmMediador.fnGetData($pai);

                if (!__crmMediadorIndex.options.ajax) {
                    location.href = __crmMediadorIndex.options.url.edit + '/' + data['mediador_id'];
                } else {
                    __crmMediadorIndex.addOverlay(
                        $('#container-crm-mediador'), 'Aguarde, carregando dados para edição...'
                    );
                    $.crmMediadorAdd().pesquisaCrmMediador(
                        data['mediador_id'],
                        function (data) { __crmMediadorIndex.showModal(data); }
                    );
                }
            });


            $('#btn-crm-mediador-add').click(function (e) {
                if (__crmMediadorIndex.options.ajax) {
                    __crmMediadorIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $('#dataTableCrmMediador').on('click', '.crmMediador-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __crmMediadorIndex.options.datatables.crmMediador.fnGetData($pai);
                var arrRemover = {
                    mediadorId: data['mediador_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de mediador "' + data['mediador_nome'] + '"?',
                    buttons: "[Não][Sim]",
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __crmMediadorIndex.addOverlay(
                                $('#container-crm-mediador'), 'Aguarde, solicitando remoção de registro de mediador...'
                            );
                            $.ajax({
                                url: __crmMediadorIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __crmMediadorIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de mediador:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __crmMediadorIndex.removeOverlay($('#container-crm-mediador'));
                                    } else {
                                        __crmMediadorIndex.reloadDataTableCrmMediador();
                                        __crmMediadorIndex.showNotificacaoSuccess(
                                            "Registro de mediador removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#crm-mediador-form');
            var actionForm = $form.attr('action');
            actionForm = actionForm.replace(/\/[0-9]+/g, '');
            actionForm = actionForm.split('/');

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['mediadorId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['mediadorId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            $('#crm-mediador-modal .modal-title').html(actionTitle + ' mediador');
            $('#crm-mediador-modal').modal();
            __crmMediadorIndex.removeOverlay($('#container-crm-mediador'));
        };

        this.reloadDataTableCrmMediador = function () {
            this.getDataTableCrmMediador().api().ajax.reload(null, false);
        };

        this.getDataTableCrmMediador = function () {
            if (!__crmMediadorIndex.options.datatables.crmMediador) {
                if (!$.fn.dataTable.isDataTable('#dataTableCrmMediador')) {
                    __crmMediadorIndex.options.datatables.crmMediador = $('#dataTableCrmMediador').DataTable();
                } else {
                    __crmMediadorIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __crmMediadorIndex.options.datatables.crmMediador;
        };

        this.run = function (opts) {
            __crmMediadorIndex.setDefaults(opts);
            __crmMediadorIndex.setSteps();
        };
    };

    $.crmMediadorIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.crm.crm-mediador.index");

        if (!obj) {
            obj = new CrmMediadorIndex();
            obj.run(params);
            $(window).data('universa.crm.crm-mediador.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);