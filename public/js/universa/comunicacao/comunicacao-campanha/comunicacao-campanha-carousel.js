(function ($, window, document) {
    'use strict';
    var ComunicacaoCampanhaCarousel = function () {
        VersaShared.call(this);
        var __comunicacaoCampanhaCarousel = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                exibirArquivo: '',
                captacaoAluno: '',
                estatistica: '',
                editarAluno: ''
            },
            data: {
                campanhaVisualizacao: {}
            },
            values: {
                exibirModal: false
            },
            cookies: [],
            formElement: '#form-campanha'
        };

        this.getCookies = function () {
            return __comunicacaoCampanhaCarousel.options.cookies;
        };

        this.inicializaCookies = function () {
            $.map(document.cookie.replace(' ', '').split(';'), function (data) {
                var campos = data.split('=');

                if (!campos[0].match(/\w+/)) {
                    return true;
                }

                __comunicacaoCampanhaCarousel.options.cookies[campos[0]] = campos[1];
            })
        };

        this.getVisualizacoesCampanha = function () {
            return __comunicacaoCampanhaCarousel.options.data.campanhaVisualizacao || [];
        };

        this.getExibirModal = function () {
            var data = window.location.href.replace(/.*\?/, '').split("&");

            for (var row in data) {
                var params = data[row].split("=");

                if (params[0] == "exibirCampanhas") {
                    return true;
                }
            }
            ;

            return false;
        };

        this.incrementarAcaoVisualizacao = function (campanhaId) {
            var arrCampanhas = __comunicacaoCampanhaCarousel.options.data.campanhaVisualizacao || [];

            for (var campanha in arrCampanhas) {
                if (campanha == campanhaId) {
                    __comunicacaoCampanhaCarousel.options.data.campanhaVisualizacao[campanhaId]++;
                    return true;
                }
            }

            __comunicacaoCampanhaCarousel.options.data.campanhaVisualizacao[campanhaId] = 1;
        };

        this.setSteps = function () {
            localStorage.clear();

            __comunicacaoCampanhaCarousel.inicializaCookies();
            __comunicacaoCampanhaCarousel.inicializaCarousel();

            $(document).on('click', '.item.active img', function (e) {
                e.preventDefault();

                __comunicacaoCampanhaCarousel.enviarDados($(this));
            });

            $(document).on('mouseover', '.item.active img', function () {
                __comunicacaoCampanhaCarousel.incrementarAcaoVisualizacao($(this).attr('id'));
            });
        };

        this.enviarDados = function (element) {
            var cookie = __comunicacaoCampanhaCarousel.getCookies();
            var dados = {};

            if (cookie['pesCpf']) {
                $('#pesCpf').val(cookie['pesCpf']);
            }

            if (cookie['alunoId']) {
                dados['alunoId'] = cookie['alunoId'];
            }

            dados['visualizacoes'] = __comunicacaoCampanhaCarousel.getVisualizacoesCampanha();
            dados['campanhaId'] = $(element).attr('id');
            //PREPARANDO OS DADOS PRA SALVAR EM LOCALSTORAGE OU INSERIR NO FORMULÀRIO
            dados = JSON.stringify(dados);

            var form = document.getElementById('form-captacao-inicio');

            if (form) {
                var input = document.createElement('input');
                input.value = dados;
                input.name = 'campanhaEstatistica';
                input.type = 'hidden';

                form.appendChild(input);
                form.action = $(element.parent()).attr('href');
                form.submit();
            } else {
                document.cookie = "campanhaEstatistica=" + dados;
                location.href = __comunicacaoCampanhaCarousel.options.url.editarAluno + '/' + cookie['alunoId'];
            }
        };

        this.inicializaCarousel = function () {
            var src = __comunicacaoCampanhaCarousel.options.url.exibirArquivo + '/';

            if (__comunicacaoCampanhaCarousel.getExibirModal()) {
                $.ajax({
                    url: __comunicacaoCampanhaCarousel.options.url.search,
                    method: 'Post',
                    type: 'Json',
                    data: {carousel: true},
                    async: false,
                    success: function (json) {
                        var data = json.data;

                        for (var row in data) {
                            var element = data[row];
                            var id = element['campanha_id'];
                            var link = element['campanha_link'] || __comunicacaoCampanhaCarousel.options.url.captacaoAluno;
                            var arquivo = src + element['arq_chave'];
                            var active = (row == 0 ? ' active' : '');
                            var indicator = '<li data-target="#myCarousel" data-slide-to="' + row + '" class="' + active + '"></li>';
                            var imagem = element['arq_chave'] ? 'style="background-image:url(' + arquivo + ')"' : "";
                            var descricao = (element['campanha_descricao'] || '');

                            descricao = (descricao ? '<div class="item-descricao">' + descricao + '</div>' : '&nbsp;');

                            var inner = '<div class="item' + active + '" ' + imagem + '>' + descricao + '</div>';

                            $('.carousel-indicators').append(indicator);
                            $('.carousel-inner').append(inner);
                        }

                        if (Object.keys(data).length > 0) {
                            $('#modal-campanha').modal('show');
                        }
                    }
                });
            }
        };

        this.reloadDataTableComunicacaoCampanha = function () {
            this.getDataTableComunicacaoCampanha().api().ajax.reload();
        };

        this.run = function (opts) {
            __comunicacaoCampanhaCarousel.setDefaults(opts);
            __comunicacaoCampanhaCarousel.setSteps();
        };
    };

    $.comunicacaoCampanhaCarousel = function (params) {
        params = params || [];

        var obj = $(window).data("universa.comunicacao.comunicacao-campanha.campanha");

        if (!obj) {
            obj = new ComunicacaoCampanhaCarousel();
            obj.run(params);
            $(window).data('universa.comunicacao.comunicacao-campanha.campanha', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);