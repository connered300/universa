(function ($, window, document) {
    'use strict';

    var ComunicacaoCampanhaAdd = function () {
        VersaShared.call(this);
        var __comunicacaoCampanhaAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                acessoPessoas: '',
                gerenciadorDeArquivosThumb: '',
                gerenciadorDeArquivosDownload: ''
            },
            data: {},
            value: {
                campanhaArquivo: null
            },
            datatables: {},
            wizardElement: '#comunicacao-campanha-wizard',
            formElement: '#comunicacao-campanha-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var campanhaArquivo = $('#campanhaArquivo').vfile({
                thumbBaseUrl: __comunicacaoCampanhaAdd.options.url.gerenciadorDeArquivosThumb,
                downloadBaseUrl: __comunicacaoCampanhaAdd.options.url.gerenciadorDeArquivosDownload
            });

            var $campanhaDescricao = $("#campanhaDescricao");
            $campanhaDescricao.ckeditor({
                height: '300px', linkShowAdvancedTab: false,
                autoStartup: true,
                enterMode: Number(2),
                toolbar: [
                    [
                        'Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-',
                        'NumberedList', 'BulletedList'
                    ],
                    ['Table'],
                    ['colors'],
                    ['Link', 'Unlink'],
                    ['Undo', 'Redo', '-', 'SelectAll']
                ]

            });

            if (__comunicacaoCampanhaAdd.options.value.campanhaArquivo) {
                campanhaArquivo.setSelection(__comunicacaoCampanhaAdd.options.value.campanhaArquivo);
            }


            var $campanhaAlvo = $("#campanhaAlvo");
            $campanhaAlvo.select2({
                multiple: true,
                allowClear: true,
                language: 'pt-BR',
                data: __comunicacaoCampanhaAdd.options.data.arrCampanhaAlvo
            }).trigger("change");

            var $campanhaDataAlteracao = $("#campanhaDataAlteracao");
            __comunicacaoCampanhaAdd.iniciarElementoDatePicker($campanhaDataAlteracao);

            var $campanhaDataCadastro = $("#campanhaDataCadastro");
            __comunicacaoCampanhaAdd.iniciarElementoDatePicker($campanhaDataCadastro);

            var $campanhaSituacao = $("#campanhaSituacao");
            $campanhaSituacao.select2({minimumInputLength: 1, allowClear: true, language: 'pt-BR'}).trigger("change");
        };

        this.setValidations = function () {
            __comunicacaoCampanhaAdd.options.validator.settings.rules = {
                campanhaNome: {maxlength: 255, required: true},
                campanhaAlvo: {required: true},
                campanhaLink: {url: true},
                campanhaSituacao: {maxlength: 7}
            };
            __comunicacaoCampanhaAdd.options.validator.settings.messages = {
                campanhaNome: {maxlength: 'Tamanho máximo: 255!', required: 'Campo obrigatório!'},
                campanhaAlvo: {maxlength: 'Campo obrigatório!'},
                campanhaLink: {url: 'Link inválido!'},
                campanhaSituacao: {maxlength: 'Tamanho máximo: 7!'}
            };

            $(__comunicacaoCampanhaAdd.options.formElement).submit(function (e) {
                var ok = $(__comunicacaoCampanhaAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__comunicacaoCampanhaAdd.options.formElement);

                if (__comunicacaoCampanhaAdd.options.ajaxSubmit || $form.data('ajax')) {
                    __comunicacaoCampanhaAdd.addOverlay($form, 'Aguarde. registrando informações...');

                    $form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            if (data.erro) {
                                __comunicacaoCampanhaAdd.showNotificacaoDanger(
                                    data['mensagem'] || "Não foi possível salvar registro"
                                );
                            } else if (__comunicacaoCampanhaAdd.options.listagem) {
                                $.comunicacaoCampanhaIndex().reloadDataTableComunicacaoCampanha();
                                __comunicacaoCampanhaAdd.showNotificacaoSuccess(
                                    data['mensagem'] || "Registro Salvo!"
                                );
                                $('#comunicacao-campanha-modal').modal('hide');
                            }

                            __comunicacaoCampanhaAdd.removeOverlay($form);
                        }
                    });

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaComunicacaoCampanha = function (campanha_id, callback) {
            var $form = $(__comunicacaoCampanhaAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __comunicacaoCampanhaAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + campanha_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __comunicacaoCampanhaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __comunicacaoCampanhaAdd.removeOverlay($form);
                },
                erro: function () {
                    __comunicacaoCampanhaAdd.showNotificacaoDanger('Erro desconhecido!');
                    __comunicacaoCampanhaAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.comunicacaoCampanhaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.comunicacao.comunicacao-campanha.add");

        if (!obj) {
            obj = new ComunicacaoCampanhaAdd();
            obj.run(params);
            $(window).data('universa.comunicacao.comunicacao-campanha.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);