(function ($, window, document) {
    'use strict';
    var ComunicacaoCampanhaIndex = function () {
        VersaShared.call(this);
        var __comunicacaoCampanhaIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: '',
                gerenciadorDeArquivosThumb: '',
                gerenciadorDeArquivosDownload: ''
            },
            datatables: {
                comunicacaoCampanha: null
            }
        };

        this.setSteps = function () {
            var $dataTableComunicacaoCampanha = $('#dataTableComunicacaoCampanha');
            var colNum = -1;

            __comunicacaoCampanhaIndex.options.datatables.comunicacaoCampanha = $dataTableComunicacaoCampanha.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __comunicacaoCampanhaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __comunicacaoCampanhaIndex.createBtnGroup(btns);
                        }

                        __comunicacaoCampanhaIndex.removeOverlay($('#container-comunicacao-campanha'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "campanha_id", targets: ++colNum, data: "campanha_id"},
                    {name: "campanha_nome", targets: ++colNum, data: "campanha_nome"},
                    {name: "campanha_alvo", targets: ++colNum, data: "campanha_alvo"},
                    {name: "campanha_data_cadastro", targets: ++colNum, data: "campanha_data_cadastro"},
                    {name: "campanha_data_alteracao", targets: ++colNum, data: "campanha_data_alteracao"},
                    {name: "campanha_situacao", targets: ++colNum, data: "campanha_situacao"},
                    {name: "campanha_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableComunicacaoCampanha.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __comunicacaoCampanhaIndex.options.datatables.comunicacaoCampanha.fnGetData($pai);

                if (!__comunicacaoCampanhaIndex.options.ajax) {
                    location.href = __comunicacaoCampanhaIndex.options.url.edit + '/' + data['campanha_id'];
                } else {
                    __comunicacaoCampanhaIndex.addOverlay(
                        $('#container-comunicacao-campanha'), 'Aguarde, carregando dados para edição...'
                    );
                    $.comunicacaoCampanhaAdd().pesquisaComunicacaoCampanha(
                        data['campanha_id'],
                        function (data) {
                            __comunicacaoCampanhaIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-comunicacao-campanha-add').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                __comunicacaoCampanhaIndex.showModal();
            });

            $dataTableComunicacaoCampanha.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __comunicacaoCampanhaIndex.options.datatables.comunicacaoCampanha.fnGetData($pai);
                var arrRemover = {
                    campanhaId: data['campanha_id']
                };

                __comunicacaoCampanhaIndex.systemDialog(
                    {
                        message: "Deseja remover este registro?"
                    },
                    function () {
                        __comunicacaoCampanhaIndex.addOverlay(
                            $('#container-comunicacao-campanha'),
                            'Aguarde, removendo registro...'
                        );

                        $.ajax({
                            url: __comunicacaoCampanhaIndex.options.url.remove,
                            type: 'POST',
                            dataType: 'json',
                            data: arrRemover,
                            success: function (data) {
                                if (data.erro) {
                                    __comunicacaoCampanhaIndex.showNotificacaoDanger(
                                        "Não foi possível remover registro de campanha:\n" +
                                        "<i>" + data['erroDescricao'] + "</i>"
                                    );
                                    __comunicacaoCampanhaIndex.removeOverlay($('#container-comunicacao-campanha'));
                                } else {
                                    __comunicacaoCampanhaIndex.reloadDataTableComunicacaoCampanha();
                                    __comunicacaoCampanhaIndex.showNotificacaoSuccess(
                                        "Registro de campanha removido!"
                                    );
                                }
                            }
                        });
                    }
                );

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var editar = false;
            var actionTitle = 'Cadastrar';
            var $form = $('#comunicacao-campanha-form'),
                $modal = $('#comunicacao-campanha-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                editar = true;
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['campanhaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['campanhaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);

            var $campanhaArquivo = $('#campanhaArquivo').vfile();

            if (data['campanhaArquivo']) {
                $campanhaArquivo.setSelection(data['campanhaArquivo']);
            } else {
                $campanhaArquivo.doRemove();
            }

            var $campanhaDescricao = $('#campanhaDescricao'),
                $campanhaAlvo = $("#campanhaAlvo"),
                $campanhaSituacao = $("#campanhaSituacao");

            $campanhaDescricao.val(data['campanhaDescricao'] || '');
            $campanhaDescricao.trigger('change');

            $campanhaAlvo.val(data['campanhaAlvo'] || '');
            $campanhaAlvo.trigger("change");

            $campanhaSituacao.select2('val', data['campanhaSituacao']);
            $campanhaSituacao.trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' campanha');
            $modal.modal();
            __comunicacaoCampanhaIndex.removeOverlay($('#container-comunicacao-campanha'));
        };

        this.reloadDataTableComunicacaoCampanha = function () {
            this.getDataTableComunicacaoCampanha().api().ajax.reload();
        };

        this.getDataTableComunicacaoCampanha = function () {
            if (!__comunicacaoCampanhaIndex.options.datatables.comunicacaoCampanha) {
                if (!$.fn.dataTable.isDataTable('#dataTableComunicacaoCampanha')) {
                    __comunicacaoCampanhaIndex.options.datatables.comunicacaoCampanha = $('#dataTableComunicacaoCampanha').DataTable();
                } else {
                    __comunicacaoCampanhaIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __comunicacaoCampanhaIndex.options.datatables.comunicacaoCampanha;
        };

        this.run = function (opts) {
            __comunicacaoCampanhaIndex.setDefaults(opts);
            __comunicacaoCampanhaIndex.setSteps();
        };
    };

    $.comunicacaoCampanhaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.comunicacao.comunicacao-campanha.index");

        if (!obj) {
            obj = new ComunicacaoCampanhaIndex();
            obj.run(params);
            $(window).data('universa.comunicacao.comunicacao-campanha.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);