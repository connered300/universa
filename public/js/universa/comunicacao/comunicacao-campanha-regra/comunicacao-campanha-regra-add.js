(function ($, window, document) {
    'use strict';

    var ComunicacaoCampanhaRegraAdd = function () {
        VersaShared.call(this);
        var __comunicacaoCampanhaRegraAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {comunicacaoCampanha: ''},
            data: {},
            value: {campanha: null},
            datatables: {},
            wizardElement: '#comunicacao-campanha-regra-wizard',
            formElement: '#comunicacao-campanha-regra-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $campanha = $("#campanha");
            $campanha.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __comunicacaoCampanhaRegraAdd.options.url.comunicacaoCampanha,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.campanha_id;
                            el.text = el.campanha_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $campanha.select2("data", __comunicacaoCampanhaRegraAdd.getCampanha());

            var $regraChave = $("#regraChave");
            $regraChave.select2({minimumInputLength: 1, allowClear: true, language: 'pt-BR'}).trigger("change");
        };

        this.setCampanha = function (campanha) {
            this.options.value.campanha = campanha || null;
        };

        this.getCampanha = function () {
            return this.options.value.campanha || null;
        };
        this.setValidations = function () {
            __comunicacaoCampanhaRegraAdd.options.validator.settings.rules = {
                campanha: {number: true, required: true},
                regraChave: {maxlength: 5},
                regraValor: {required: true}
            };
            __comunicacaoCampanhaRegraAdd.options.validator.settings.messages = {
                campanha: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                regraChave: {maxlength: 'Tamanho máximo: 5!'},
                regraValor: {required: 'Campo obrigatório!'}
            };

            $(__comunicacaoCampanhaRegraAdd.options.formElement).submit(function (e) {
                var ok = $(__comunicacaoCampanhaRegraAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__comunicacaoCampanhaRegraAdd.options.formElement);

                if (__comunicacaoCampanhaRegraAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __comunicacaoCampanhaRegraAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __comunicacaoCampanhaRegraAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__comunicacaoCampanhaRegraAdd.options.listagem) {
                                    $.comunicacaoCampanhaRegraIndex().reloadDataTableComunicacaoCampanhaRegra();
                                    __comunicacaoCampanhaRegraAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#comunicacao-campanha-regra-modal').modal('hide');
                                }

                                __comunicacaoCampanhaRegraAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaComunicacaoCampanhaRegra = function (regra_id, callback) {
            var $form = $(__comunicacaoCampanhaRegraAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __comunicacaoCampanhaRegraAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + regra_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __comunicacaoCampanhaRegraAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __comunicacaoCampanhaRegraAdd.removeOverlay($form);
                },
                erro: function () {
                    __comunicacaoCampanhaRegraAdd.showNotificacaoDanger('Erro desconhecido!');
                    __comunicacaoCampanhaRegraAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.comunicacaoCampanhaRegraAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.comunicacao.comunicacao-campanha-regra.add");

        if (!obj) {
            obj = new ComunicacaoCampanhaRegraAdd();
            obj.run(params);
            $(window).data('universa.comunicacao.comunicacao-campanha-regra.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);