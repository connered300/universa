(function ($, window, document) {
    'use strict';
    var ComunicacaoCampanhaRegraIndex = function () {
        VersaShared.call(this);
        var __comunicacaoCampanhaRegraIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                comunicacaoCampanhaRegra: null
            }
        };

        this.setSteps = function () {
            var $dataTableComunicacaoCampanhaRegra = $('#dataTableComunicacaoCampanhaRegra');
            var colNum = -1;
            __comunicacaoCampanhaRegraIndex.options.datatables.comunicacaoCampanhaRegra = $dataTableComunicacaoCampanhaRegra.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __comunicacaoCampanhaRegraIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __comunicacaoCampanhaRegraIndex.createBtnGroup(btns);
                        }

                        __comunicacaoCampanhaRegraIndex.removeOverlay($('#container-comunicacao-campanha-regra'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "regra_id", targets: ++colNum, data: "regra_id"},
                    {name: "campanha_id", targets: ++colNum, data: "campanha_id"},
                    {name: "regra_chave", targets: ++colNum, data: "regra_chave"},
                    {name: "regra_valor", targets: ++colNum, data: "regra_valor"},
                    {name: "regra_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableComunicacaoCampanhaRegra.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __comunicacaoCampanhaRegraIndex.options.datatables.comunicacaoCampanhaRegra.fnGetData($pai);

                if (!__comunicacaoCampanhaRegraIndex.options.ajax) {
                    location.href = __comunicacaoCampanhaRegraIndex.options.url.edit + '/' + data['regra_id'];
                } else {
                    __comunicacaoCampanhaRegraIndex.addOverlay(
                        $('#container-comunicacao-campanha-regra'), 'Aguarde, carregando dados para edição...'
                    );
                    $.comunicacaoCampanhaRegraAdd().pesquisaComunicacaoCampanhaRegra(
                        data['regra_id'],
                        function (data) {
                            __comunicacaoCampanhaRegraIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-comunicacao-campanha-regra-add').click(function (e) {
                if (__comunicacaoCampanhaRegraIndex.options.ajax) {
                    __comunicacaoCampanhaRegraIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableComunicacaoCampanhaRegra.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __comunicacaoCampanhaRegraIndex.options.datatables.comunicacaoCampanhaRegra.fnGetData($pai);
                var arrRemover = {
                    regraId: data['regra_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de campanha regra "' + data['campanha_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __comunicacaoCampanhaRegraIndex.addOverlay(
                                $('#container-comunicacao-campanha-regra'), 'Aguarde, solicitando remoção de registro de campanha regra...'
                            );
                            $.ajax({
                                url: __comunicacaoCampanhaRegraIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __comunicacaoCampanhaRegraIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de campanha regra:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __comunicacaoCampanhaRegraIndex.removeOverlay($('#container-comunicacao-campanha-regra'));
                                    } else {
                                        __comunicacaoCampanhaRegraIndex.reloadDataTableComunicacaoCampanhaRegra();
                                        __comunicacaoCampanhaRegraIndex.showNotificacaoSuccess(
                                            "Registro de campanha regra removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#comunicacao-campanha-regra-form'),
                $modal = $('#comunicacao-campanha-regra-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['regraId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['regraId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $campanha = $("#campanha");
            $campanha.select2('data', data['campanha']).trigger("change");

            var $regraChave = $("#regraChave");
            $regraChave.select2('val', data['regraChave']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' campanha regra');
            $modal.modal();
            __comunicacaoCampanhaRegraIndex.removeOverlay($('#container-comunicacao-campanha-regra'));
        };

        this.reloadDataTableComunicacaoCampanhaRegra = function () {
            this.getDataTableComunicacaoCampanhaRegra().api().ajax.reload();
        };

        this.getDataTableComunicacaoCampanhaRegra = function () {
            if (!__comunicacaoCampanhaRegraIndex.options.datatables.comunicacaoCampanhaRegra) {
                if (!$.fn.dataTable.isDataTable('#dataTableComunicacaoCampanhaRegra')) {
                    __comunicacaoCampanhaRegraIndex.options.datatables.comunicacaoCampanhaRegra = $('#dataTableComunicacaoCampanhaRegra').DataTable();
                } else {
                    __comunicacaoCampanhaRegraIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __comunicacaoCampanhaRegraIndex.options.datatables.comunicacaoCampanhaRegra;
        };

        this.run = function (opts) {
            __comunicacaoCampanhaRegraIndex.setDefaults(opts);
            __comunicacaoCampanhaRegraIndex.setSteps();
        };
    };

    $.comunicacaoCampanhaRegraIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.comunicacao.comunicacao-campanha-regra.index");

        if (!obj) {
            obj = new ComunicacaoCampanhaRegraIndex();
            obj.run(params);
            $(window).data('universa.comunicacao.comunicacao-campanha-regra.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);