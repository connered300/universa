(function ($, window, document) {
    'use strict';
    var ComunicacaoCampanhaEstatisticaIndex = function () {
        VersaShared.call(this);
        var __comunicacaoCampanhaEstatisticaIndex = this;
        this.defaults = {
            ajax: true,
            url: {
                search: '',
                edit: '',
                remove: ''
            },
            datatables: {
                comunicacaoCampanhaEstatistica: null
            }
        };

        this.setSteps = function () {
            var $dataTableComunicacaoCampanhaEstatistica = $('#dataTableComunicacaoCampanhaEstatistica');
            var colNum = -1;
            __comunicacaoCampanhaEstatisticaIndex.options.datatables.comunicacaoCampanhaEstatistica = $dataTableComunicacaoCampanhaEstatistica.dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: __comunicacaoCampanhaEstatisticaIndex.options.url.search,
                    type: "POST",
                    dataSrc: function (json) {
                        var data = json.data;
                        var btns = [
                            {class: 'btn-primary item-edit', icon: 'fa-pencil'},
                            {class: 'btn-danger item-remove', icon: 'fa-times'}
                        ];

                        for (var row in data) {
                            data[row]['acao'] = __comunicacaoCampanhaEstatisticaIndex.createBtnGroup(btns);
                        }

                        __comunicacaoCampanhaEstatisticaIndex.removeOverlay($('#container-comunicacao-campanha-estatistica'));

                        return data;
                    }
                },
                columnDefs: [
                    {name: "estatistica_id", targets: ++colNum, data: "estatistica_id"},
                    {name: "campanha_id", targets: ++colNum, data: "campanha_id"},
                    {name: "usuario_id", targets: ++colNum, data: "usuario_id"},
                    {name: "aluno_id", targets: ++colNum, data: "aluno_id"},
                    {name: "curso_id", targets: ++colNum, data: "curso_id"},
                    {name: "estatistica_data", targets: ++colNum, data: "estatistica_data"},
                    {name: "estatistica_alvo", targets: ++colNum, data: "estatistica_alvo"},
                    {name: "estatistica_acao", targets: ++colNum, data: "estatistica_acao"},
                    {name: "estatistica_id", targets: ++colNum, data: "acao"}
                ],
                "dom": "fr" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6'i><'col-sm-6'p>>",
                "autoWidth": true,
                oLanguage: {
                    sProcessing: "<i class='fa fa-lg fa-spinner fa-spin'></i>",
                    sLengthMenu: "Mostrar _MENU_ registros por página",
                    sZeroRecords: "Nenhum registro encontrado",
                    sInfo: "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    sInfoEmpty: "Mostrando 0 / 0 de 0 registros",
                    sInfoFiltered: "(filtrado de _MAX_ registros)",
                    sSearch: "Procurar: ",
                    oPaginate: {
                        sFirst: "Início",
                        sPrevious: "Anterior",
                        sNext: "Próximo",
                        sLast: "Último"
                    }
                },
                order: [[0, 'asc']]
            });

            $dataTableComunicacaoCampanhaEstatistica.on('click', '.item-edit', function () {
                var $pai = $(this).closest('tr');
                var data = __comunicacaoCampanhaEstatisticaIndex.options.datatables.comunicacaoCampanhaEstatistica.fnGetData($pai);

                if (!__comunicacaoCampanhaEstatisticaIndex.options.ajax) {
                    location.href = __comunicacaoCampanhaEstatisticaIndex.options.url.edit + '/' + data['estatistica_id'];
                } else {
                    __comunicacaoCampanhaEstatisticaIndex.addOverlay(
                        $('#container-comunicacao-campanha-estatistica'), 'Aguarde, carregando dados para edição...'
                    );
                    $.comunicacaoCampanhaEstatisticaAdd().pesquisaComunicacaoCampanhaEstatistica(
                        data['estatistica_id'],
                        function (data) {
                            __comunicacaoCampanhaEstatisticaIndex.showModal(data);
                        }
                    );
                }
            });


            $('#btn-comunicacao-campanha-estatistica-add').click(function (e) {
                if (__comunicacaoCampanhaEstatisticaIndex.options.ajax) {
                    __comunicacaoCampanhaEstatisticaIndex.showModal();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });

            $dataTableComunicacaoCampanhaEstatistica.on('click', '.item-remove', function (e) {
                var $pai = $(this).closest('tr');
                var data = __comunicacaoCampanhaEstatisticaIndex.options.datatables.comunicacaoCampanhaEstatistica.fnGetData($pai);
                var arrRemover = {
                    estatisticaId: data['estatistica_id']
                };

                $.SmartMessageBox({
                    title: "Confirme a operação:",
                    content: 'Deseja realmente remover registro de campanha estatistica "' + data['campanha_id'] + '"?',
                    buttons: "[Não][Sim]"
                }, function (ButtonPress) {
                    if (ButtonPress == "Sim") {
                        setTimeout(function () {
                            __comunicacaoCampanhaEstatisticaIndex.addOverlay(
                                $('#container-comunicacao-campanha-estatistica'), 'Aguarde, solicitando remoção de registro de campanha estatistica...'
                            );
                            $.ajax({
                                url: __comunicacaoCampanhaEstatisticaIndex.options.url.remove,
                                type: 'POST',
                                dataType: 'json',
                                data: arrRemover,
                                success: function (data) {
                                    if (data.erro) {
                                        __comunicacaoCampanhaEstatisticaIndex.showNotificacaoDanger(
                                            "Não foi possível remover registro de campanha estatistica:\n" +
                                            "<i>" + data['erroDescricao'] + "</i>"
                                        );
                                        __comunicacaoCampanhaEstatisticaIndex.removeOverlay($('#container-comunicacao-campanha-estatistica'));
                                    } else {
                                        __comunicacaoCampanhaEstatisticaIndex.reloadDataTableComunicacaoCampanhaEstatistica();
                                        __comunicacaoCampanhaEstatisticaIndex.showNotificacaoSuccess(
                                            "Registro de campanha estatistica removido!"
                                        );
                                    }
                                }
                            });
                        }, 400);
                    }
                });

                e.preventDefault();
            });
        };

        this.showModal = function (data) {
            data = data || {};
            var actionTitle = 'Cadastrar';
            var $form = $('#comunicacao-campanha-estatistica-form'),
                $modal = $('#comunicacao-campanha-estatistica-modal');
            var actionForm = $form.attr('action');
            actionForm = actionForm.split('/');

            if (actionForm[actionForm.length - 2] == 'edit') {
                actionForm = actionForm.slice(0, actionForm.length - 1);
            }

            if (Object.keys(data).length > 0) {
                actionTitle = 'Editar';
                actionForm[actionForm.length - 1] = 'edit';
                actionForm.push(data['estatisticaId']);
            } else {
                actionForm[actionForm.length - 1] = 'add';
                data['estatisticaId'] = "";
            }

            actionForm = actionForm.join('/');
            $form.attr('action', actionForm);


            $form[0].reset();
            $form.deserialize(data);


            var $campanha = $("#campanha");
            $campanha.select2('data', data['campanha']).trigger("change");

            var $usuario = $("#usuario");
            $usuario.select2('data', data['usuario']).trigger("change");

            var $aluno = $("#aluno");
            $aluno.select2('data', data['aluno']).trigger("change");

            var $curso = $("#curso");
            $curso.select2('data', data['curso']).trigger("change");

            var $estatisticaAlvo = $("#estatisticaAlvo");
            $estatisticaAlvo.select2('val', data['estatisticaAlvo']).trigger("change");

            var $estatisticaAcao = $("#estatisticaAcao");
            $estatisticaAcao.select2('val', data['estatisticaAcao']).trigger("change");

            $modal.find('.modal-title').html(actionTitle + ' campanha estatistica');
            $modal.modal();
            __comunicacaoCampanhaEstatisticaIndex.removeOverlay($('#container-comunicacao-campanha-estatistica'));
        };

        this.reloadDataTableComunicacaoCampanhaEstatistica = function () {
            this.getDataTableComunicacaoCampanhaEstatistica().api().ajax.reload();
        };

        this.getDataTableComunicacaoCampanhaEstatistica = function () {
            if (!__comunicacaoCampanhaEstatisticaIndex.options.datatables.comunicacaoCampanhaEstatistica) {
                if (!$.fn.dataTable.isDataTable('#dataTableComunicacaoCampanhaEstatistica')) {
                    __comunicacaoCampanhaEstatisticaIndex.options.datatables.comunicacaoCampanhaEstatistica = $('#dataTableComunicacaoCampanhaEstatistica').DataTable();
                } else {
                    __comunicacaoCampanhaEstatisticaIndex.showNotificacaoDanger("Listagem não inicializada!");
                }
            }

            return __comunicacaoCampanhaEstatisticaIndex.options.datatables.comunicacaoCampanhaEstatistica;
        };

        this.run = function (opts) {
            __comunicacaoCampanhaEstatisticaIndex.setDefaults(opts);
            __comunicacaoCampanhaEstatisticaIndex.setSteps();
        };
    };

    $.comunicacaoCampanhaEstatisticaIndex = function (params) {
        params = params || [];

        var obj = $(window).data("universa.comunicacao.comunicacao-campanha-estatistica.index");

        if (!obj) {
            obj = new ComunicacaoCampanhaEstatisticaIndex();
            obj.run(params);
            $(window).data('universa.comunicacao.comunicacao-campanha-estatistica.index', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);