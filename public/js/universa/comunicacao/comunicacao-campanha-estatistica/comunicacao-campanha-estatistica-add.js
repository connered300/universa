(function ($, window, document) {
    'use strict';

    var ComunicacaoCampanhaEstatisticaAdd = function () {
        VersaShared.call(this);
        var __comunicacaoCampanhaEstatisticaAdd = this;
        this.defaults = {
            ajaxSubmit: 0,
            listagem: 0,
            url: {
                comunicacaoCampanha: '',
                acessoPessoas: '',
                acadgeralAluno: '',
                acadCurso: ''
            },
            data: {},
            value: {
                campanha: null,
                usuario: null,
                aluno: null,
                curso: null
            },
            datatables: {},
            wizardElement: '#comunicacao-campanha-estatistica-wizard',
            formElement: '#comunicacao-campanha-estatistica-form',
            validator: null
        };

        this.steps = {};

        this.setSteps = function () {
            var $campanha = $("#campanha");
            $campanha.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __comunicacaoCampanhaEstatisticaAdd.options.url.comunicacaoCampanha,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.campanha_id;
                            el.text = el.campanha_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $campanha.select2("data", __comunicacaoCampanhaEstatisticaAdd.getCampanha());

            var $usuario = $("#usuario");
            $usuario.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __comunicacaoCampanhaEstatisticaAdd.options.url.acessoPessoas,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.id;
                            el.text = el.id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $usuario.select2("data", __comunicacaoCampanhaEstatisticaAdd.getUsuario());

            var $aluno = $("#aluno");
            $aluno.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __comunicacaoCampanhaEstatisticaAdd.options.url.acadgeralAluno,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.aluno_id;
                            el.text = el.aluno_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $aluno.select2("data", __comunicacaoCampanhaEstatisticaAdd.getAluno());

            var $curso = $("#curso");
            $curso.select2({
                language: 'pt-BR',
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: __comunicacaoCampanhaEstatisticaAdd.options.url.acadCurso,
                    dataType: 'json',
                    delay: 250,
                    data: function (query) {
                        return {query: query};
                    },
                    results: function (data) {
                        var transformed = $.map(data, function (el) {
                            el.id = el.curso_id;
                            el.text = el.curso_id;

                            return el;
                        });

                        return {results: transformed};
                    }
                }
            });

            $curso.select2("data", __comunicacaoCampanhaEstatisticaAdd.getCurso());

            var $estatisticaData = $("#estatisticaData");
            __comunicacaoCampanhaEstatisticaAdd.iniciarElementoDatePicker($estatisticaData);

            var $estatisticaAlvo = $("#estatisticaAlvo");
            $estatisticaAlvo.select2({minimumInputLength: 1, allowClear: true, language: 'pt-BR'}).trigger("change");

            var $estatisticaAcao = $("#estatisticaAcao");
            $estatisticaAcao.select2({minimumInputLength: 1, allowClear: true, language: 'pt-BR'}).trigger("change");
        };

        this.setCampanha = function (campanha) {
            this.options.value.campanha = campanha || null;
        };

        this.getCampanha = function () {
            return this.options.value.campanha || null;
        };

        this.setUsuario = function (usuario) {
            this.options.value.usuario = usuario || null;
        };

        this.getUsuario = function () {
            return this.options.value.usuario || null;
        };

        this.setAluno = function (aluno) {
            this.options.value.aluno = aluno || null;
        };

        this.getAluno = function () {
            return this.options.value.aluno || null;
        };

        this.setCurso = function (curso) {
            this.options.value.curso = curso || null;
        };

        this.getCurso = function () {
            return this.options.value.curso || null;
        };
        this.setValidations = function () {
            __comunicacaoCampanhaEstatisticaAdd.options.validator.settings.rules = {
                campanha: {number: true, required: true},
                usuario: {number: true},
                aluno: {number: true},
                curso: {number: true},
                estatisticaData: {required: true, dateBR: true},
                estatisticaAlvo: {maxlength: 10},
                estatisticaAcao: {maxlength: 12}
            };
            __comunicacaoCampanhaEstatisticaAdd.options.validator.settings.messages = {
                campanha: {number: 'Número inválido!', required: 'Campo obrigatório!'},
                usuario: {number: 'Número inválido!'},
                aluno: {number: 'Número inválido!'},
                curso: {number: 'Número inválido!'},
                estatisticaData: {required: 'Campo obrigatório!', dateBR: 'Informe uma data válida!'},
                estatisticaAlvo: {maxlength: 'Tamanho máximo: 10!'},
                estatisticaAcao: {maxlength: 'Tamanho máximo: 12!'}
            };

            $(__comunicacaoCampanhaEstatisticaAdd.options.formElement).submit(function (e) {
                var ok = $(__comunicacaoCampanhaEstatisticaAdd.options.formElement).valid();

                if (!ok) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                var $form = $(__comunicacaoCampanhaEstatisticaAdd.options.formElement);

                if (__comunicacaoCampanhaEstatisticaAdd.options.ajaxSubmit || $form.data('ajax')) {
                    var dados = $form.serializeJSON();
                    dados['ajax'] = true;

                    if (!$form.data('ajax')) {
                        $form.data('ajax', true);
                        __comunicacaoCampanhaEstatisticaAdd.addOverlay($form, 'Salvando registro. Aguarde...');
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: dados,
                            success: function (data) {
                                if (data.erro) {
                                    __comunicacaoCampanhaEstatisticaAdd.showNotificacaoDanger(
                                        data['mensagem'] || "Não foi possível salvar registro"
                                    );
                                } else if (__comunicacaoCampanhaEstatisticaAdd.options.listagem) {
                                    $.comunicacaoCampanhaEstatisticaIndex().reloadDataTableComunicacaoCampanhaEstatistica();
                                    __comunicacaoCampanhaEstatisticaAdd.showNotificacaoSuccess(
                                        data['mensagem'] || "Registro Salvo!"
                                    );
                                    $('#comunicacao-campanha-estatistica-modal').modal('hide');
                                }

                                __comunicacaoCampanhaEstatisticaAdd.removeOverlay($form);
                                $form.data('ajax', false);
                            }
                        });
                    } else {
                        $form.data('ajax', false);
                    }

                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

                this.submit();
                return true;
            });
        };

        this.pesquisaComunicacaoCampanhaEstatistica = function (estatistica_id, callback) {
            var $form = $(__comunicacaoCampanhaEstatisticaAdd.options.formElement);
            var action = $form.attr('action');
            action = action.replace(/\/(edit|add)((&|\/).*|)/i, '');

            __comunicacaoCampanhaEstatisticaAdd.addOverlay($form, 'Aguarde. Pesquisando dados do registro...');

            $.ajax({
                url: action + "/edit/" + estatistica_id,
                dataType: 'json',
                type: 'get',
                data: {ajax: true},
                success: function (data) {
                    if (Object.keys(data).length == 0) {
                        __comunicacaoCampanhaEstatisticaAdd.showNotificacaoInfo('Nenhum registro encontrado!');
                    } else {
                        callback(data['arrDados']);
                    }

                    __comunicacaoCampanhaEstatisticaAdd.removeOverlay($form);
                },
                erro: function () {
                    __comunicacaoCampanhaEstatisticaAdd.showNotificacaoDanger('Erro desconhecido!');
                    __comunicacaoCampanhaEstatisticaAdd.removeOverlay($form);
                }
            });
        };

        this.run = function (opts) {
            this.setDefaults(opts);
            this.setSteps();
            this.setValidations();
        };
    };

    $.comunicacaoCampanhaEstatisticaAdd = function (params) {
        params = params || [];

        var obj = $(window).data("universa.comunicacao.comunicacao-campanha-estatistica.add");

        if (!obj) {
            obj = new ComunicacaoCampanhaEstatisticaAdd();
            obj.run(params);
            $(window).data('universa.comunicacao.comunicacao-campanha-estatistica.add', obj);
        }

        return obj;
    };
})(window.jQuery, window, document);