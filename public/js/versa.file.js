(function ($, window, document, undefined) {
    var vfile = function () {
        var pub = this, priv = {};
        priv.defaults = {
            vfileId: null,
            thumbBaseUrl: '',
            downloadBaseUrl: '',
            element: null,
            defaultMessage: 'Sem imagem selecionada',
            invalidMessage: 'Imagem inválida!',
            textLabelSelect: 'Selecionar',
            textLabelRemove: 'Remover',
            hideText: false,
            html: '<div class="card file-upload">\
                        <div class="card-body text-center">\
                            <div class="message-container">\
                                <div class="message"></div>\
                                <div class="preview">&nbsp;</div>\
                            </div>\
                            <div class="btn-group">\
                                <button type="button" class="btn btn-flat file-select"><span class="fa fa-folder"></span> <span class="file-label"></span></button>\
                                <button type="button" class="btn btn-flat btn-danger file-remove"><span class="fa fa-times"></span> <span class="file-label"></span></button>\
                            </div>\
                        </div>\
                    </div>',
            loadCallback: null,
            addCallback: null,
            removeCallback: null,
            thumbWidth: 250,
            thumbHeight: 250,
            allowedTypes: ['image/png', 'image/jpeg', 'image/jpg', 'image/gif', 'image/bmp'],
            checkTypeCallback: null
        };

        priv.options = priv.defaults;

        priv.doSelection = function (e) {
            var $file = $("#" + priv.options.containerId + '_file');
            $file.click();
            e.preventDefault();
        };

        priv.checkType = function (type) {
            var valid = false;

            if (typeof priv.options.checkTypeCallback == 'function') {
                valid = priv.options.checkTypeCallback(type, priv);
            } else if (jQuery.inArray(type, priv.options.allowedTypes) !== -1) {
                valid = true;
            }

            return valid;
        };

        priv.fileSelection = function (e) {
            var $container = $('#' + priv.options.containerId);
            var $file = $("#" + priv.options.containerId + '_file');
            var $raw = $("#" + priv.options.containerId + '_raw');

            var files = this.files;

            var strType = files[0].type || '';

            if (!strType) {
                strType = (files[0].name || '').split('.');
                strType = strType[strType.length - 1];
            }

            if (!priv.checkType(strType)) {
                $container.find('.message').html(priv.options.invalidMessage);
                e.preventDefault();
            } else {
                $raw.val('');
                $container.find('.message').html(files[0].name + '<br>' + priv.humanFileSize(files[0].size));
                $container.find('.message-container .preview').css('background-image', '');
                $container.find('.file-remove').show();

                if (files[0].type.match('image.*')) {
                    try {
                        var reader = new FileReader();
                        reader.onload = (function () {
                            return function (e) {
                                $container.find('.message-container .preview').css('background-image',
                                    'url(' + e.target.result + ')');
                            };
                        })(files[0]);
                        reader.readAsDataURL(files[0]);
                    } catch (e) {
                        console.log(e);
                    }
                }
            }

            if (typeof priv.options.addCallback == 'function') {
                priv.options.addCallback();
            }
        };

        pub.setSelection = function (param) {
            param = param || [];
            $(priv.options.element).val(priv.options.id);
            var $container = $('#' + priv.options.containerId);
            var $file = $('#' + priv.options.containerId + '_file');
            var $raw = $('#' + priv.options.containerId + '_raw');

            if (!priv.checkType(param.tipo)) {
                $container.find('.message').html(priv.options.invalidMessage);
            } else {
                var thumb = priv.options.thumbBaseUrl + '/' + param.chave + '?width=' + priv.options.thumbWidth +
                    '&height=' + priv.options.thumbHeight;
                var download = priv.options.downloadBaseUrl + '/' + param.chave;

                $container.find('.message').html(
                    '<a href="' + download + '" title="Baixar arquivo">' + param.nome + '</a><br>' +
                    priv.humanFileSize(param.tamanho)
                );
                $container.find('.message-container .preview').css('background-image', 'url(' + thumb + ')');
                $container.find('.file-remove').show();
            }

            $raw.val(param.id || param.arqId || '');

            if (typeof priv.options.addCallback == 'function') {
                priv.options.addCallback();
            }
        };

        priv.doRemove = function (e) {
            $(priv.options.element).val(priv.options.id);
            var $container = $('#' + priv.options.containerId);
            var $trash = $('#' + priv.options.containerId + '_trash');
            var $file = $('#' + priv.options.containerId + '_file');
            var $raw = $('#' + priv.options.containerId + '_raw');

            $trash.val($raw.val() || false);
            $raw.val('');
            $file.val('');

            $container.find('.file-remove').hide();
            $container.find('.message').html(priv.options.defaultMessage);
            $container.find('.message-container .preview').css('background-image', '');

            if (typeof priv.options.removeCallback == 'function') {
                priv.options.removeCallback();
            }

            if (e) {
                e.preventDefault();
            }
        };

        pub.doRemove = function () {
            priv.doRemove(false);
        };

        priv.humanFileSize = function (bytes, si) {
            si = si || true;

            var thresh = !si ? 1000 : 1024;

            if (Math.abs(bytes) < thresh) {
                return bytes + ' B';
            }

            var units = si
                ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
                : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
            var u = -1;

            do {
                bytes /= thresh;
                ++u;
            } while (Math.abs(bytes) >= thresh && u < units.length - 1);

            return bytes.toFixed(1) + ' ' + units[u];
        };

        priv.transformField = function () {
            var $this = $(priv.options.element);
            var $container = $(priv.options.html);
            var id = 'vfile_' + priv.options.id;

            priv.options.containerId = id;

            $this.addClass('hidden');
            $container.find('.file-remove').hide();

            $container.find('.file-remove .file-label').attr('title', priv.options.textLabelRemove);
            $container.find('.file-select .file-label').attr('title', priv.options.textLabelSelect);

            if (!priv.options.hideText) {
                $container.find('.file-remove .file-label').html(priv.options.textLabelRemove);
                $container.find('.file-select .file-label').html(priv.options.textLabelSelect);
            }

            $container.find('.message').html(priv.options.defaultMessage);

            var raw = $('<input type="hidden" name="vfile[' + priv.options.id + '][raw]" id="' + id + '_raw" />');
            var trash = $('<input type="hidden" name="vfile[' + priv.options.id + '][trash]" id="' + id + '_trash" />');
            var file = $('<input type="file" style="display:none" name="vfile[' + priv.options.id + '][file]" id="' +
                id + '_file" />');

            $container.append(raw);
            $container.append(trash);
            $container.append(file);

            $this.before($container);
            $container.attr('id', id);
            $this.data('vfile', id);
            $this.attr('type', 'hidden');
            $this.val(priv.options.id);
        };

        priv.activeActions = function () {
            var $container = $("#" + priv.options.containerId);
            var $file = $("#" + priv.options.containerId + "_file");

            $file.change(priv.fileSelection);
            $container.on('click', '.file-select', {}, priv.doSelection);
            $container.on('click', '.file-remove', {}, priv.doRemove);

        };

        pub.init = function (param) {
            param = param || [];
            priv.options = $.extend({}, priv.defaults, param);

            var $this = $(priv.options.element);
            priv.options.id = $.md5($this.getPath());

            priv.transformField();
            priv.activeActions();

            if (typeof priv.options.loadCallback == 'function') {
                priv.options.loadCallback();
            }
        };
    };

    $.fn.vfile = function () {
        var params = $.extend([], arguments);

        if (params.length == 1) {
            params = params[0];
        }

        var method = false;
        var retval = false;

        if (typeof params === 'string' || typeof vfile[params] === 'function') {
            method = params;
            params = undefined;
        } else {
            method = (params || [false])[0];

            if (typeof method === 'string' || typeof vfile[method] === 'function') {
                params = params.slice(1)[0];
            }
        }
        var ret = [];

        this.each(function () {
            var $this = $(this);
            var obj = $this.data("versa.vfile");

            if (!obj) {
                obj = new vfile();

                var initParam = params || [];
                initParam['element'] = $this;
                obj.init(initParam);

                $this.data("versa.vfile", obj);
            } else if (method) {
                retval = params ? obj[method](params) : obj[method]();
            }

            ret.push(retval === false ? obj : retval);
        });

        if (ret.length == 1) {
            ret = ret.pop();
        }

        return ret;
    };
})(window.jQuery, window, document);

