<?php

error_reporting(E_ERROR);

if (!function_exists('curl_file_create')) {
    function curl_file_create($filename, $mimetype = '', $postname = '')
    {
        return "@$filename;filename="
        . ($postname ?: basename($filename))
        . ($mimetype ? ";type=$mimetype" : '');
    }
}

function getFileContentFromPath($path, $fileName)
{
    $data = file_get_contents($path);

    $tempFolder = tempnam(sys_get_temp_dir(), '');
    $tempHandle = fopen($tempFolder, 'r+');

    fwrite($tempHandle, $data);
    $result['tmp_name'] = stream_get_meta_data($tempHandle)['uri'];
    fclose($tempHandle);

    $result['size'] = strlen($data);
    $result['name'] = $fileName;

    return $result;
}

function prepareFilesToSend($arrArquivos)
{
    if (!$arrArquivos['name'][0]) {
        return false;
    }

    $arrArquivoServidor = array();
    $arrArquivo         = array();

    /** QUANDO MAIS DE UM ARQUIVO, O $_FILES AGRUPA AS CHAVES EM ARRAY, O QUE IMPOSSIBILITA O ENVIO */
    /** ESSE LAÇO DESEMBARAÇA O ARRAY E PREPARA PARA O ENVIO */
    foreach ($arrArquivos as $key => $arrValue) {
        foreach ($arrValue as $index => $value) {
            $arrArquivoServidor[$index][$key] = $value;
        }
    }

    return $arrArquivoServidor;
}

$tokenUniversa = '18654c201c59ebe928d75ad5a6f1908b7e41d4b5';

$codigoAluno = "2532";
$codigoCurso = "2616";

$SK = md5($tokenUniversa . $codigoAluno);

$codigoCursos = ['id' => 'descricao'];

$host = "http://universa.versatecnologia.com.br";

$url = $host . "/api/v1/aluno-protocolo?id=$codigoAluno";

$urlHome = $host . $_SERVER['PHP_SELF'];

if ($_REQUEST['novaMensagem']) {
    $url     = $host . "/api/v1/protocolo-mensagem";
    $process = curl_init($url);

    $dados = prepareFilesToSend($_FILES['arrArquivo']);

    foreach ($dados as $pos => $f) {
        $_POST['arrArquivo[' . $pos . ']'] = curl_file_create($f['tmp_name'], $f['type'], $f['name']);
    }

    $dados = $_POST;

    curl_setopt_array(
        $process,
        array(
            CURLOPT_USERPWD        => $tokenUniversa . ":" . $tokenUniversa,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $dados,
            CURLOPT_RETURNTRANSFER => true
        )
    );

    $return = curl_exec($process);
    curl_close($process);

    $return = json_decode($return, true);
}

if ($_REQUEST['novoProtocolo']) {
    $url     = $host . "/api/v1/protocolo";
    $process = curl_init($url);

    $dados = prepareFilesToSend($_FILES['arrArquivo']);

    foreach ($dados as $pos => $f) {
        $_POST['arrArquivo[' . $pos . ']'] = curl_file_create($f['tmp_name'], $f['type'], $f['name']);
    }

    $dados = $_POST;

    curl_setopt_array(
        $process,
        array(
            CURLOPT_USERPWD        => $tokenUniversa . ":" . $tokenUniversa,
            CURLOPT_POST           => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POSTFIELDS     => $dados
        )
    );
    
    $return = curl_exec($process);
    curl_close($process);

    $return = json_decode($return, true);
}

$urlDownload = $host . "/gerenciador-arquivos/arquivo/download/";

if ($protocoloId = $_GET['detalharProtocolo']) {
    $url = $host . "/api/v1/protocolo?id=$protocoloId";
}

$process = curl_init($url);

curl_setopt_array(
    $process,
    array(
        CURLOPT_USERPWD        => $tokenUniversa . ":" . $tokenUniversa,
        CURLOPT_RETURNTRANSFER => true
    )
);

$return = curl_exec($process);
curl_close($process);

$return = json_decode($return, true);

echo "<h1>Gestão de Protocolos</h1>";

echo '<table>';
echo '<thead><tr><th>Protocolo</th><th>Setor</th><th>Assunto</th><th>Data</th><th>Estado</th><th>Email</th><th>Telefone</th><th>&nbsp;</th></tr></thead><tbody>';

if ($protocoloId) {
    foreach ($return['aluno'] as $arrProtocolo) {

        echo '<tr>
            <td>' . $arrProtocolo['protocolo']['protocoloId'] . '</td>
            <td>-</td>
            <td>' . $arrProtocolo['protocolo']['protocoloAssunto'] . '</td>
            <td>' . $arrProtocolo['protocolo']['protocoloDataAlteracao'] . '</td>
            <td>' . $arrProtocolo['protocolo']['protocoloSituacao'] . '</td>
            <td>' . $arrProtocolo['protocolo']['protocoloSolicitanteEmail'] . '</td>
            <td>' . $arrProtocolo['protocolo']['protocoloSolicitanteTelefone'] . '</td>
            <td><a href="?detalharProtocolo=' . $arrProtocolo['protocolo']['protocoloId'] . '">Detalhar</a></td>
        </tr>';

        echo "<table><thead><tr><th>Id</th><th>Autor</th><th>Mensagem</th><th>Origem</th><th>Data</th><th>Anexos</th></tr></thead><tbody>";
        foreach ($arrProtocolo['mensagem'] as $mensagem) {
            echo "<tr>" .
                "<td>" . $mensagem['mensagemId'] . "</td>" .
                "<td>" . $mensagem['autor'] . "</td>" .
                "<td>" . $mensagem['mensagem'] . "</td>" .
                "<td>" . $mensagem['origem'] . "</td>" .
                "<td>" . $mensagem['data'] . "</td>" .
                "<td><ul style='list-style: none'>";

            foreach ($mensagem['anexos'] as $anexo) {
                echo "<li><a href='" . $urlDownload . $anexo['arq_chave'] . "'>" . $anexo['arq_nome'] . "</a></li>";
            }

            echo "</ul></td></tr>";
        }
        echo "</tbody></table>";

        echo "<form action='?novaMensagem=true' method='post' enctype='multipart/form-data'>
                <h3>Nova Mensagem</h3>
                  <input type='hidden' name='protocolo' value={$arrProtocolo['protocolo']['protocoloId']}>
                  <input type='hidden' name='mensagemOrigem' value='Solicitante'>
                  Arquivos: <input type='file' name='arrArquivo[]' value=false multiple><br>
                  Mensagem *: <textarea name='mensagemConteudo'></textarea>
                  <button type='submit'>Nova Mensagem</button>
              </form>
            ";
    }
}

foreach ($return['protocolos'] as $protocolo) {
    echo '<tr>
            <td>' . $protocolo['protocolo_id'] . '</td>
            <td>-</td>
            <td>' . $protocolo['protocolo_assunto'] . '</td>
            <td>' . $protocolo['protocolo_data_alteracao'] . '</td>
            <td>' . $protocolo['protocolo_situacao'] . '</td>
            <td><a href="?detalharProtocolo=' . $protocolo['protocolo_id'] . '">Detalhar</a></td>
        </tr>';
}

echo '</tbody></table><br><br>';

$url = $url = $host . "/api/v1/protocolo-solicitacao?";

$process = curl_init($url);

curl_setopt_array(
    $process,
    array(
        CURLOPT_USERPWD        => $tokenUniversa . ":" . $tokenUniversa,
        CURLOPT_RETURNTRANSFER => true
    )
);

$return = curl_exec($process);
curl_close($process);

$return = json_decode($return, true);

$setores = $return['setores'];
echo "<form action='?novoProtocolo=true' method='post' enctype='multipart/form-data'id='novoProtocoloId'>";
echo "<h3>Novo Protocolo</h3>";
echo "<select name='solicitacao' form='novoProtocoloId'>";
foreach ($setores as $setor) {
    foreach ($setor['solicitacao'] as $solicitacao) {
        echo "<option id='{$setor['setor_id']}' value='{$solicitacao['solicitacao_id']}'>
                {$solicitacao['solicitacao_descricao']} / {$setor['setor_descricao']}
              </option>";
    }
}
echo "</select><br>";

echo "      <input type='hidden' name='mensagemOrigem' value='Solicitante'>
            protocoloSolicitanteVisivel:
                <select name='protocoloSolicitanteVisivel' form='novoProtocoloId'>
                    <option value='Sim' selected>Sim</option>
                    <option value='Não'>Não</option>
                </select><br>
            protocoloSituacao:
                <select name='protocoloSituacao' form='novoProtocoloId'>
                    <option value='Aberto' selected>Aberto</option>
                    <option value='Em Andamento'>Em Andamento</option>
                    <option value='Concluído'>Concluído</option>
                </select><br>
            mensagemSolicitanteVisivel:
                <select name='mensagemSolicitanteVisivel' form='novoProtocoloId'>
                    <option value='Sim' selected>Sim</option>
                    <option value='Não'>Não</option>
                </select><br>
            protocoloAssunto *: <input name='protocoloAssunto'><br>
            codigoAluno *: <input name='codigoAluno' value='" . $codigoAluno . "'><br>
            codigoCurso *: <input name='codigoCurso' value='" . $codigoCurso . "'><br>
            usuarioIdResponsavel: <input name='usuarioIdResponsavel'><br>
            protocoloSolicitantePes: <input name='protocoloSolicitantePes'><br>
            protocoloSolicitanteAlunocurso: <input name='protocoloSolicitanteAlunocurso'><br>
            protocoloAvaliacao: <input name='protocoloAvaliacao'><br>
            protocoloSolicitanteNome: <input name='protocoloSolicitanteNome'><br>
            protocoloSolicitanteEmail: <input name='protocoloSolicitanteEmail'><br>
            protocoloSolicitanteTelefone: <input name='protocoloSolicitanteTelefone'><br>
            mensagemUsuario: <input name='mensagemUsuario'><br>
            Arquivos: <input type='file' name='arrArquivo[]' value=false multiple>
            Mensagem *: <textarea name='protocoloMensagem'></textarea>
            <button type='submit'>Nova Protocolo</button>
      </form>";

echo "<a href='#' onclick='javascript:history.back();'>Voltar</a><br>";
?>