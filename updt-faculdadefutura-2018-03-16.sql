ALTER TABLE `crm__lead`
ADD COLUMN `lead_distribuindo` ENUM('Sim', 'Nao') NOT NULL DEFAULT 'Nao' AFTER `lead_status`;

--  ADICIONANDO A COLUNA NA TABELA
ALTER TABLE crm__lead ADD COLUMN `lead_data_atribuicao` DATETIME NULL DEFAULT NULL;
ALTER TABLE crm__lead CHANGE COLUMN `lead_data_atribuicao` `lead_data_atribuicao` DATETIME NULL DEFAULT NULL;

--  SCRIPT QUE ATUALIZA AS DATAS
UPDATE crm__lead
  LEFT JOIN
  (
    SELECT
      least(
          lead_data_atribuicao,
          lead_data_alteracao,
          coalesce(min(atividade_data_cadastro), lead_data_alteracao)
      ) AS lead_data_atribuicao,
      pes_id
    FROM
      crm__lead
      LEFT JOIN crm__lead_atividade USING (pes_id)
    GROUP BY pes_id
  ) AS ultimaAtividadeLead USING (pes_id)
SET crm__lead.lead_data_atribuicao = ultimaAtividadeLead.lead_data_atribuicao
WHERE pes_id > 0;

-- View de configuração de títulos
DROP VIEW IF EXISTS `view__financeiro_configuracao_titulos`;
DROP TABLE IF EXISTS `view__financeiro_configuracao_titulos`;
CREATE OR REPLACE VIEW `view__financeiro_configuracao_titulos` AS
  SELECT
    tc.*,
    -- prioriza configuração de campus/curso
    if(cursocampus_id IS NOT NULL, 1, 2)              AS prioridade_por_curso,
    -- prioriza configuração de tipo de título
    if(tipotitulo_id IS NOT NULL, 1, 2)               AS prioridade_por_tipo,
    -- prioriza configuração de periodo letivo
    if(pl.per_id IS NOT NULL, 1, 2)                   AS prioridade_por_periodo,
    -- prioriza configuração que iniciou primeiro
    COALESCE(per_data_inicio, tituloconf_data_inicio) AS prioridade_por_inicio,
    -- prioriza configuração que termina antes
    COALESCE(per_data_fim, tituloconf_data_fim)       AS prioridade_por_fim
  FROM financeiro__titulo_config tc
    LEFT JOIN acadperiodo__letivo pl ON tc.per_id = pl.per_id
  WHERE (
    (date(now()) >= date(tc.tituloconf_data_inicio) AND date(now()) <= date(tc.tituloconf_data_fim)) OR
    (date(now()) >= date(pl.per_data_inicio) AND date(now()) <= date(pl.per_data_fim))
  )
  ORDER BY
    prioridade_por_curso ASC,
    prioridade_por_tipo ASC,
    prioridade_por_periodo ASC,
    prioridade_por_inicio ASC,
    prioridade_por_fim ASC,
    tituloconf_id ASC;


-- view com informações sobre os descontos de títulos
DROP VIEW IF EXISTS `view__financeiro_descontos_titulos`;
DROP TABLE IF EXISTS `view__financeiro_descontos_titulos`;
CREATE OR REPLACE VIEW `view__financeiro_descontos_titulos` AS
  SELECT
    fdt.*,
    u1.login      AS usuario_criacao_login,
    u2.login      AS usuario_alteracao_login,
    CONCAT(
        desconto_id, ' - ', desctipo_descricao, ' / ', pes_nome, ' / ',
        IF(desctipo_percmax IS NULL, desconto_valor, concat(desconto_percentual, '%'))
    )             AS desconto_dados,
    CONCAT(
        titulo_id, ' - ', titulo_descricao
    )             AS titulo_dados,
    fd.desconto_valor,
    fd.desconto_percentual,
    fd.desconto_status,
    fd.desconto_dia_limite,
    fdtp.desctipo_id,
    fdtp.desctipo_limita_vencimento,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto está ativo e se o desconto do título foi deferido
       if(fd.desconto_status = 'Inativo' OR desconto_titulo_situacao <> 'Deferido',
          'Não',
          -- verifica se o desconto está limitado ao vencimento e se o título está vencido
          if(fdtp.desctipo_limita_vencimento = 'Sim' AND titulo_data_vencimento < date(now()),
             'Não',
             -- verifica se o dia limite do desconto relativo ao mês da data de vencimento já passou
             if(fd.desconto_dia_limite IS NOT NULL AND
                DATE(CONCAT(
                         year(titulo_data_vencimento), '-',
                         month(titulo_data_vencimento), '-',
                         fd.desconto_dia_limite
                     )) < curdate(),
                'Não',
                'Sim'
             )

          )
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', 'Sim', 'Não')
    )             AS desconto_aplicacao_valida,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto é monetário ou percentual
       if(fd.desconto_valor IS NOT NULL,
          fd.desconto_valor,
          titulo_valor * (fd.desconto_percentual / 100)
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', desconto_titulo_valor, 0)
    )             AS desconto_aplicacao_valor,
    titulo_id        tituloId,
    titulo_valor,
    titulo_data_vencimento,
    titulo_estado,
    titulo_estado AS tituloEstado
  FROM financeiro__desconto_titulo fdt
    INNER JOIN financeiro__desconto fd USING (desconto_id)
    INNER JOIN financeiro__desconto_tipo fdtp USING (desctipo_id)
    LEFT JOIN acesso_pessoas u1 ON u1.id = fdt.usuario_criacao
    LEFT JOIN acesso_pessoas u2 ON u2.id = fdt.usuario_alteracao
    LEFT JOIN financeiro__titulo ft USING (titulo_id)
    LEFT JOIN acadgeral__aluno aa USING (aluno_id)
    LEFT JOIN pessoa p ON p.pes_id = aa.pes_id
  ORDER BY titulo_id DESC;


-- view com informações sobre os descontos de títulos
DROP VIEW IF EXISTS `view__financeiro_descontos_titulos_simplificado`;
DROP TABLE IF EXISTS `view__financeiro_descontos_titulos_simplificado`;
CREATE OR REPLACE VIEW `view__financeiro_descontos_titulos_simplificado` AS
  SELECT
    fdt.*,
    fd.aluno_id,
    fd.desconto_valor,
    fd.desconto_percentual,
    fd.desconto_status,
    fd.desconto_dia_limite,
    fdtp.desctipo_id,
    fdtp.desctipo_limita_vencimento,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto está ativo e se o desconto do título foi deferido
       if(fd.desconto_status = 'Inativo' OR desconto_titulo_situacao <> 'Deferido',
          'Não',
          -- verifica se o desconto está limitado ao vencimento e se o título está vencido
          if(fdtp.desctipo_limita_vencimento = 'Sim' AND titulo_data_vencimento < date(now()),
             'Não',
             -- verifica se o dia limite do desconto relativo ao mês da data de vencimento já passou
             if(fd.desconto_dia_limite IS NOT NULL AND
                DATE(CONCAT(
                         year(titulo_data_vencimento), '-',
                         month(titulo_data_vencimento), '-',
                         fd.desconto_dia_limite
                     )) < curdate(),
                'Não',
                'Sim'
             )

          )
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', 'Sim', 'Não')
    )             AS desconto_aplicacao_valida,
    -- Verifica se o título está aberto
    if(titulo_estado = 'Aberto',
       -- verifica se o desconto é monetário ou percentual
       if(fd.desconto_valor IS NOT NULL,
          fd.desconto_valor,
          titulo_valor * (fd.desconto_percentual / 100)
       ),
       -- verifica se o desconto foi aplicado
       if(fdt.desconto_titulo_aplicado = 'Sim', desconto_titulo_valor, 0)
    )             AS desconto_aplicacao_valor,
    titulo_id        tituloId,
    titulo_valor,
    titulo_data_vencimento,
    titulo_estado,
    titulo_estado AS tituloEstado
  FROM financeiro__desconto_titulo fdt
    INNER JOIN financeiro__desconto fd USING (desconto_id)
    INNER JOIN financeiro__desconto_tipo fdtp USING (desctipo_id)
    LEFT JOIN financeiro__titulo ft USING (titulo_id)
  ORDER BY titulo_id DESC;


-- view com informações sobre os títulos
DROP VIEW IF EXISTS `view__financeiro_titulos`;
DROP TABLE IF EXISTS `view__financeiro_titulos`;
CREATE OR REPLACE VIEW `view__financeiro_titulos` AS
  SELECT
    t.titulo_id,
    t.pes_id,
    p.pes_nome,
    t.tipotitulo_id,
    tt.tipotitulo_nome,
    t.usuario_baixa,
    ub.login       usuario_baixa_login,
    t.usuario_autor,
    ua.login       usuario_autor_login,
    t.titulo_novo,
    t.alunocurso_id,
    ac.aluno_id,
    a.pes_id as pes_id_aluno,
    c.curso_nome,
    c.curso_sigla,
    t.titulo_descricao,
    t.titulo_parcela,
    t.titulo_valor_pago,
    t.titulo_valor_pago_dinheiro,
    t.titulo_multa,
    t.titulo_juros,
    t.titulo_desconto,
    t.titulo_observacoes,
    t.titulo_desconto_manual,
    t.titulo_acrescimo_manual,
    t.titulo_data_processamento,
    t.titulo_data_vencimento,
    t.titulo_data_pagamento,
    t.titulo_data_baixa,
    t.titulo_tipo_pagamento,
    t.titulo_valor,
    t.titulo_estado,
    cc.cursocampus_id,
    greatest(
        if(
            titulo_data_pagamento IS NULL,
            datediff(date(now()), date(t.titulo_data_vencimento)),
            datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
        ),
        0
    )           AS dias_atraso,
    coalesce(
        if(titulo_data_pagamento IS NULL AND
           greatest(
               if(
                   titulo_data_pagamento IS NULL,
                   datediff(date(now()), date(t.titulo_data_vencimento)),
                   datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
               ),
               0
           ) > 0,
           -- configuração de juros e multa para título
           coalesce(tituloconf_multa, 0) * t.titulo_valor,
           titulo_multa
        ),
        0
    )           AS titulo_multa_calc,
    if(
        titulo_data_pagamento IS NULL,
        -- configuração de juros e multa para título
        coalesce(tituloconf_juros, 0) * greatest(
            if(
                titulo_data_pagamento IS NULL,
                datediff(date(now()), date(t.titulo_data_vencimento)),
                datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
            ),
            0
        ),
        coalesce(titulo_juros, 0)
    )           AS titulo_juros_calc,
    (
      SELECT coalesce(sum(desconto_aplicacao_valor), 0)
      FROM view__financeiro_descontos_titulos_simplificado fdt
      WHERE
        desconto_aplicacao_valida = 'Sim' AND
        fdt.titulo_id = t.titulo_id
    )           AS descontos,
    tc.tituloconf_id,
    tc.tituloconf_dia_venc,
    tc.tituloconf_dia_desc,
    tc.tituloconf_valor_desc,
    tc.tituloconf_percent_desc,
    tc.tituloconf_multa,
    tc.tituloconf_juros,
    tc.confcont_id,
    b.bol_id
  FROM financeiro__titulo t
    INNER JOIN financeiro__titulo_tipo tt ON t.tipotitulo_id = tt.tipotitulo_id
    -- dados aluno
    INNER JOIN pessoa p ON t.pes_id = p.pes_id
    -- curso aluno
    LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
    LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id
    LEFT JOIN campus_curso cc ON cc.cursocampus_id = ac.cursocampus_id
    LEFT JOIN acad_curso c ON c.curso_id = cc.curso_id
    -- boleto
    LEFT JOIN boleto b ON t.titulo_id = b.titulo_id
    -- usuário baixa
    LEFT JOIN acesso_pessoas ua ON ua.id = t.usuario_autor
    LEFT JOIN acesso_pessoas ub ON ub.id = t.usuario_baixa
    LEFT JOIN financeiro__titulo_config AS tc ON tc.tituloconf_id=t.tituloconf_id
  -- GROUP BY titulo_id
  ORDER BY
    titulo_data_vencimento DESC,
    titulo_data_processamento DESC,
    titulo_data_pagamento DESC,
    alunocurso_id ASC,
    tituloconf_id ASC;

-- view com informações sobre os títulos
DROP VIEW IF EXISTS `view__financeiro_titulos_simplificado`;
DROP TABLE IF EXISTS `view__financeiro_titulos_simplificado`;
CREATE OR REPLACE VIEW `view__financeiro_titulos_simplificado` AS
  SELECT
    t.titulo_id,
    t.pes_id,
    t.tipotitulo_id,
    t.usuario_baixa,
    t.usuario_autor,
    t.titulo_novo,
    t.alunocurso_id,
    ac.aluno_id,
    a.pes_id as pes_id_aluno,
    t.titulo_descricao,
    t.titulo_parcela,
    t.titulo_valor_pago,
    t.titulo_valor_pago_dinheiro,
    t.titulo_multa,
    t.titulo_juros,
    t.titulo_desconto,
    t.titulo_observacoes,
    t.titulo_desconto_manual,
    t.titulo_acrescimo_manual,
    t.titulo_data_processamento,
    t.titulo_data_vencimento,
    t.titulo_data_pagamento,
    t.titulo_data_baixa,
    t.titulo_tipo_pagamento,
    t.titulo_valor,
    t.titulo_estado,
    ac.cursocampus_id,
    greatest(
        if(
            titulo_data_pagamento IS NULL,
            datediff(date(now()), date(t.titulo_data_vencimento)),
            datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
        ),
        0
    )           AS dias_atraso,
    coalesce(
        if(titulo_data_pagamento IS NULL AND
           greatest(
               if(
                   titulo_data_pagamento IS NULL,
                   datediff(date(now()), date(t.titulo_data_vencimento)),
                   datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
               ),
               0
           ) > 0,
           -- configuração de juros e multa para título
           coalesce(tituloconf_multa, 0) * t.titulo_valor,
           titulo_multa
        ),
        0
    )           AS titulo_multa_calc,
    if(
        titulo_data_pagamento IS NULL,
        -- configuração de juros e multa para título
        coalesce(tituloconf_juros, 0) * greatest(
            if(
                titulo_data_pagamento IS NULL,
                datediff(date(now()), date(t.titulo_data_vencimento)),
                datediff(date(titulo_data_pagamento), date(t.titulo_data_vencimento))
            ),
            0
        ),
        coalesce(titulo_juros, 0)
    )           AS titulo_juros_calc,
    (
      SELECT coalesce(sum(desconto_aplicacao_valor), 0)
      FROM view__financeiro_descontos_titulos_simplificado fdt
      WHERE
        desconto_aplicacao_valida = 'Sim' AND
        fdt.titulo_id = t.titulo_id
    )           AS descontos,
    tc.tituloconf_id,
    tc.tituloconf_dia_venc,
    tc.tituloconf_dia_desc,
    tc.tituloconf_valor_desc,
    tc.tituloconf_percent_desc,
    tc.tituloconf_multa,
    tc.tituloconf_juros,
    tc.confcont_id,
    b.bol_id
  FROM financeiro__titulo t
    -- curso aluno
    LEFT JOIN acadgeral__aluno_curso ac ON ac.alunocurso_id = t.alunocurso_id
    LEFT JOIN acadgeral__aluno a ON a.aluno_id = ac.aluno_id
    -- boleto
    LEFT JOIN boleto b ON t.titulo_id = b.titulo_id
    -- usuário baixa
    LEFT JOIN financeiro__titulo_config AS tc ON tc.tituloconf_id=t.tituloconf_id
  -- GROUP BY titulo_id
  ORDER BY
    titulo_id ASC,
    alunocurso_id ASC,
    tituloconf_id ASC;

DROP VIEW IF EXISTS `view__financeiro_titulos_totais`;
DROP TABLE IF EXISTS `view__financeiro_titulos_totais`;
CREATE OR REPLACE VIEW `view__financeiro_titulos_totais` AS
  SELECT
    t.*,
    (
      (t.titulo_valor + t.titulo_juros_calc + t.titulo_multa_calc + coalesce(t.titulo_acrescimo_manual, 0)) -
      (t.descontos + coalesce(t.titulo_desconto, 0) + coalesce(t.titulo_desconto_manual, 0))
    ) AS titulo_valor_calc
  FROM view__financeiro_titulos t
  GROUP BY titulo_id
  ORDER BY
    titulo_data_vencimento DESC,
    titulo_data_processamento DESC,
    titulo_data_pagamento DESC,
    alunocurso_id ASC;


-- migrations/S031-C001-SQL-001.sql


ALTER TABLE `selecao_cursos` DROP FOREIGN KEY `fk_selecao_cursos_campus_curso1`;
ALTER TABLE `inscricao_cursos` DROP FOREIGN KEY fk_inscricao_cursos_selecao_cursos1;
ALTER TABLE `prova_edicao` DROP FOREIGN KEY fk_prova_edicao_selecao_cursos1;
ALTER TABLE `prova_edicao_caderno` DROP FOREIGN KEY fk_prova_edicao_caderno_prova_edicao1;
ALTER TABLE `selecao_cursos`
ADD CONSTRAINT `fk_selecao_cursos_campus_curso1` FOREIGN KEY (`cursocampus_id`) REFERENCES `campus_curso` (`cursocampus_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `inscricao_cursos`
ADD CONSTRAINT `fk_inscricao_cursos_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `prova_edicao`
ADD CONSTRAINT `fk_prova_edicao_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `prova_edicao_caderno`
ADD CONSTRAINT `fk_prova_edicao_caderno_prova_edicao1` FOREIGN KEY (`cad_id`) REFERENCES `prova_caderno` (`cad_id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `selecao_cursos` DROP FOREIGN KEY `fk_selecao_cursos_campus_curso1`;
ALTER TABLE `inscricao_cursos` DROP FOREIGN KEY fk_inscricao_cursos_selecao_cursos1;
ALTER TABLE `prova_edicao` DROP FOREIGN KEY fk_prova_edicao_selecao_cursos1;
ALTER TABLE `prova_edicao_caderno` DROP FOREIGN KEY fk_prova_edicao_caderno_prova_edicao1;
ALTER TABLE `selecao_cursos` ADD CONSTRAINT `fk_selecao_cursos_campus_curso1` FOREIGN KEY (`cursocampus_id`) REFERENCES `campus_curso` (`cursocampus_id`) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE `inscricao_cursos` ADD CONSTRAINT `fk_inscricao_cursos_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE `prova_edicao` ADD CONSTRAINT `fk_prova_edicao_selecao_cursos1` FOREIGN KEY (`selcursos_id`) REFERENCES `selecao_cursos` (`selcursos_id`) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE `prova_edicao_caderno` ADD CONSTRAINT `fk_prova_edicao_caderno_prova_edicao1` FOREIGN KEY (`cad_id`) REFERENCES `prova_caderno` (`cad_id`) ON UPDATE CASCADE ON DELETE CASCADE;

-- migrations/S031-C003-SQL-001.sql


CREATE TABLE sis__integracao_disciplina (
  curso_disc_id INT(10) UNSIGNED ZEROFILL NOT NULL,
  integracao_id INT(10) UNSIGNED ZEROFILL NOT NULL,
  codigo        INT                       NOT NULL,
  PRIMARY KEY (curso_disc_id, integracao_id),
  CONSTRAINT fk_CursoDiscIdDiscId FOREIGN KEY (curso_disc_id) REFERENCES acadgeral__disciplina (disc_id),
  CONSTRAINT fk_IntegracaoDisciplinaIntegracao FOREIGN KEY (integracao_id) REFERENCES sis__integracao (integracao_id)
);


ALTER TABLE acadgeral__disciplina_curso
ADD COLUMN disc__nucleo_comum ENUM('Sim', 'Nao') NOT NULL DEFAULT 'Sim';

ALTER TABLE `sis__integracao_disciplina`
DROP FOREIGN KEY `fk_CursoDiscIdDiscId`,
DROP FOREIGN KEY `fk_IntegracaoDisciplinaIntegracao`;
ALTER TABLE `sis__integracao_disciplina`
ADD CONSTRAINT `fk_CursoDiscIdDiscId`
FOREIGN KEY (`curso_disc_id`)
REFERENCES `acadgeral__disciplina_curso` (`disc_curso_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_IntegracaoDisciplinaIntegracao`
FOREIGN KEY (`integracao_id`)
REFERENCES `sis__integracao` (`integracao_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `org__email_tipo`
ADD CONSTRAINT `fk_org__email_tipo__org__email_conta`
FOREIGN KEY (`conta_id`)
REFERENCES `org__email_conta`(`conta_id`)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `org__email_tipo`
ADD CONSTRAINT `fk_org__email_tipo__org__comunicacao_tipo`
FOREIGN KEY (`tipo_id`)
REFERENCES `org__comunicacao_tipo`(`tipo_id`)
  ON DELETE CASCADE ON UPDATE CASCADE;

-- migrations/S031-C004-SQL-001.sql


--
-- Estrutura da tabela `sis__integracao_turma`
--

CREATE TABLE IF NOT EXISTS `sis__integracao_turma` (
  `integracao_id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `turma_id`      INT(10) UNSIGNED ZEROFILL NOT NULL,
  `codigo`        VARCHAR(45)               NOT NULL,
  PRIMARY KEY (`integracao_id`, `turma_id`),
  KEY `turma_id` (`turma_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `sis__integracao_turma`
--
ALTER TABLE `sis__integracao_turma`
ADD CONSTRAINT `sis__integracao_turma_ibfk_1` FOREIGN KEY (`integracao_id`) REFERENCES `sis__integracao` (`integracao_id`),
ADD CONSTRAINT `sis__integracao_turma_ibfk_2` FOREIGN KEY (`turma_id`) REFERENCES `acadperiodo__turma` (`turma_id`);


ALTER TABLE `acadperiodo__turma`
ADD COLUMN `unidade_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL;


SET foreign_key_checks = 0;

ALTER TABLE `acadperiodo__turma`
ADD CONSTRAINT `fk_unidade__turma_123`
FOREIGN KEY (`unidade_id`)
REFERENCES `org__unidade_estudo` (`unidade_id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

SET foreign_key_checks = 1;

-- migrations/S031-C006-SQL-001.sql


CREATE TABLE IF NOT EXISTS `acadgeral__atividade_log` (
  `log_id` INT NOT NULL AUTO_INCREMENT,
  `log_data` DATETIME NOT NULL,
  `log_action` VARCHAR(200) NOT NULL,
  `log_user_agent` VARCHAR(200) NOT NULL,
  `log_ip` VARCHAR(45) NOT NULL,
  `log_info` TEXT(3000) NULL,
  `aluno_id` INT UNSIGNED ZEROFILL NOT NULL,
  `disc_id` INT UNSIGNED ZEROFILL NULL,
  `curso_id` INT ZEROFILL NULL,
  PRIMARY KEY (`log_id`),
  INDEX `fk_acadgeral__atividade_log_acadgeral__aluno1_idx` (`aluno_id` ASC),
  INDEX `fk_acadgeral__atividade_log_acadgeral__disciplina1_idx` (`disc_id` ASC),
  INDEX `fk_acadgeral__atividade_log_acad_curso1_idx` (`curso_id` ASC),
  CONSTRAINT `fk_acadgeral__atividade_log_acadgeral__aluno1`
    FOREIGN KEY (`aluno_id`)
    REFERENCES `acadgeral__aluno` (`aluno_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadgeral__atividade_log_acadgeral__disciplina1`
    FOREIGN KEY (`disc_id`)
    REFERENCES `acadgeral__disciplina` (`disc_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadgeral__atividade_log_acad_curso1`
    FOREIGN KEY (`curso_id`)
    REFERENCES `acad_curso` (`curso_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- migrations/S031-C007-SQL-001.sql


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `acadperiodo__letivo`
-- -----------------------------------------------------
ALTER TABLE `acadperiodo__letivo`
ADD `per_periodicidade` enum('semestral','anual','Quadrimestral','Trimestral','Nao Aplicado') not null,
ADD `per_descricao` VARCHAR(45) NULL,
ADD `per_data_finalizacao` DATETIME NULL,
ADD `nivel_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD INDEX `fk_acadperiodo__letivo_acad_nivel1_idx` (`nivel_id` ASC),
ADD CONSTRAINT `fk_acadperiodo__letivo_acad_nivel1` FOREIGN KEY (`nivel_id`) REFERENCES `acad_nivel` (`nivel_id`)
ON DELETE CASCADE
ON UPDATE CASCADE;

-- -----------------------------------------------------
-- Table `acadperiodo__letivo_campus_curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `acadperiodo__letivo_campus_curso` (
  `per_id` INT UNSIGNED ZEROFILL NOT NULL,
  `cursocampus_id` INT ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`per_id`, `cursocampus_id`),
  INDEX `fk_acadperiodo__letivo_campus_curso_campus_curso1_idx` (`cursocampus_id` ASC),
  INDEX `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1_idx` (`per_id` ASC),
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1`
  FOREIGN KEY (`per_id`)
  REFERENCES `acadperiodo__letivo` (`per_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_campus_curso1`
  FOREIGN KEY (`cursocampus_id`)
  REFERENCES `campus_curso` (`cursocampus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `acadperiodo__letivo_selecao_edicao` (
  `per_id` INT UNSIGNED ZEROFILL NOT NULL,
  `edicao_id` INT UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`per_id`, `edicao_id`),
  INDEX `fk_acadperiodo__letivo_selecao_edicao_selecao_edicao1_idx` (`edicao_id` ASC),
  INDEX `fk_acadperiodo__letivo_selecao_edicao_acadperiodo__letivo1_idx` (`per_id` ASC),
  CONSTRAINT `fk_acadperiodo__letivo_selecao_edicao_acadperiodo__letivo1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acadperiodo__letivo` (`per_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__letivo_selecao_edicao_selecao_edicao1`
    FOREIGN KEY (`edicao_id`)
    REFERENCES `selecao_edicao` (`edicao_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- Atualiza Registro de Semestralidade

UPDATE acadperiodo__letivo
SET
  per_periodicidade = 'semestral'
WHERE
  per_periodicidade IS NULL;

-- Atualiza Registros de Nivel de Ensino

UPDATE acadperiodo__letivo
SET
  nivel_id = (SELECT
                nivel_id
              FROM
                acad_nivel
              WHERE
                nivel_nome = 'Bacharelado')
WHERE
  nivel_id IS NULL OR nivel_id < 1;

-- Atualiza Registros de Finalização de Periodo

set sql_safe_updates=0;

UPDATE acadperiodo__letivo
SET
  per_data_finalizacao = per_data_fim
WHERE
  per_data_finalizacao IS NULL OR per_data_finalizacao < 1;

set sql_safe_updates=1;




-- migrations/S032-C001-SQL-001.sql


ALTER TABLE `acad_curso`
ADD `curso_possui_periodo_letivo` ENUM('Sim', 'Não')  NOT NULL DEFAULT 'Sim' COMMENT 'É o período em que o curso forma novas turmas';

ALTER TABLE `acad_curso`
ADD `curso_unidade_medida` ENUM('Mensal', 'Bimestral', 'Trimestral', 'Quadrimestral', 'Semestral', 'Anual') NOT NULL DEFAULT 'Mensal';

-- migrations/S032-C003-SQL-001.sql


REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Meus Protocolos' as descricao, 'Protocolo\\Controller\\ProtocoloVinculado' as controller from dual
    ) as func
  where mod_nome like 'Protocolo';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Index' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'edit' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'add' as act,'Adicionar' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-mensagem' as act,'Busca' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'add-mensagem' as act,'Adicionar Mensagem' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-protocolo-arquivo' as act,'Buscar arquivos' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'atualiza-situacao' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Protocolo\\Controller\\ProtocoloVinculado');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Protocolo\\Controller\\ProtocoloVinculado'
  )
  and grup_nome LIKE 'admin%';

-- migrations/S032-C004-SQL-001.sql


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT DISTINCT
  acesso_funcionalidades.id,
  actions.act,
  actions.descricao,
  actions.tipo,
  actions.usr
FROM acesso_funcionalidades,
  (
    SELECT
      'relatorio-desconto'    AS act,
      'Relatório de Descontos' AS descricao,
      'Leitura'  AS tipo,
      'N'        AS usr
    FROM dual
  ) AS actions
WHERE func_controller IN (
  'Financeiro\\Controller\\Relatorio'
);
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
SELECT
  acesso_actions.id,
  acesso_grupo.id
FROM acesso_funcionalidades
  INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
  ,
  acesso_grupo
WHERE func_controller IN (
  'Financeiro\\Controller\\Relatorio'
)
      AND grup_nome LIKE 'Admin%';

-- migrations/S032-C008-SQL-001.sql


ALTER TABLE `acadperiodo__aluno_resumo`
  MODIFY `resaluno_origem` ENUM('Legado','Transferência','Novo título','Manual','Sistema', 'WebService') NOT NULL DEFAULT 'Sistema',
  MODIFY `resaluno_serie` int(11) DEFAULT NULL;


ALTER TABLE `acadperiodo__aluno_resumo`
ADD COLUMN `resaluno_detalhe` VARCHAR(100) NULL ;


ALTER TABLE `acadperiodo__aluno_resumo`
CHANGE COLUMN `resaluno_origem` `resaluno_origem` ENUM('Legado', 'Transferência', 'Novo título', 'Manual', 'Sistema', 'WebService') NOT NULL DEFAULT 'WebService' ;


-- migrations/S032-C015-SQL-001.sql


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'adimplentes'    AS act,
        'Relatório dos alunos Adimplentes' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Financeiro\\Controller\\Relatorio'
  );
REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Financeiro\\Controller\\Relatorio'
  )
        AND grup_nome LIKE 'Admin%';

-- migrations/S032-C025-SQL-001.sql


ALTER TABLE `acadgeral__motivo_alteracao`
ADD COLUMN `motivo_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia', 'Concluido') NOT NULL DEFAULT 'Deferido'
AFTER `motivo_descricao`;

ALTER TABLE `acadgeral__aluno_curso`
CHANGE COLUMN `alunocurso_situacao` `alunocurso_situacao` ENUM('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia', 'Concluido') NULL DEFAULT NULL;

ALTER TABLE `acadgeral__motivo_alteracao`
CHANGE COLUMN `motivo_situacao` `motivo_situacao` SET('Deferido', 'Pendente', 'Trancado', 'Cancelado', 'Indeferido', 'Transferencia', 'Concluido') NOT NULL DEFAULT 'Deferido,Pendente,Trancado,Cancelado,Indeferido,Transferencia,Concluido';

UPDATE `acadgeral__motivo_alteracao`
SET `motivo_situacao` = 'Deferido,Pendente,Trancado,Cancelado,Indeferido,Transferencia,Concluido'
WHERE motivo_id > 0;


/*
Script para alterar a situação dos alunos, na tabela acadgeral__aluno_curso
*/
UPDATE `acadgeral__aluno_curso` SET `alunocurso_situacao`='Concluido'
WHERE (alunocurso_data_expedicao_diploma is not null OR alunocurso_data_colacao is not null) and alunocurso_id>0;

-- migrations/S032-C027-SQL-001.sql


ALTER TABLE acadgeral__aluno
  CHANGE COLUMN `aluno_etnia` `aluno_etnia` ENUM('Branca', 'Preta', 'Amarela', 'Parda', 'Indígena', 'Não dispõe da informação', 'Não declarado') NOT NULL DEFAULT 'Não declarado' COMMENT 'Cor da pele do aluno.' ;

-- migrations/S032-C034-SQL-001.sql

ALTER TABLE `financeiro__titulo_config`
ADD COLUMN `tituloconf_instrucao_boleto_automatica` ENUM('Sim', 'Nao') NULL DEFAULT 'Sim';


ALTER TABLE `financeiro__titulo_config`
ADD COLUMN `tituloconf_nome` VARCHAR(45) NOT NULL;



UPDATE `financeiro__titulo_config` SET `tituloconf_nome`=concat('Configuração de Título: ',tituloconf_id) WHERE `tituloconf_id`>0;

ALTER TABLE `financeiro__titulo_config`
ADD COLUMN `tituloconf_dia_desc2` INT(11) NULL DEFAULT NULL AFTER `tituloconf_nome`,
ADD COLUMN `tituloconf_valor_desc2` FLOAT NULL DEFAULT NULL AFTER `tituloconf_dia_desc2`,
ADD COLUMN `tituloconf_percent_desc2` FLOAT NULL DEFAULT NULL AFTER `tituloconf_valor_desc2`;


-- migrations/NOVA-INTERFACE-MATRICULA.sql


ALTER TABLE `acad_curso`
CHANGE curso_integralizacao_medida curso_integralizacao_medida TEXT NULL DEFAULT  NULL;

ALTER TABLE acad_curso
CHANGE curso_periodicidade curso_periodicidade TEXT NULL DEFAULT  NULL;

UPDATE acad_curso
SET curso_possui_periodo_letivo=IF(curso_possui_periodo_letivo='Não' OR curso_possui_periodo_letivo='Não Aplicado', 'Não', 'Sim') where curso_id>0;

ALTER TABLE acad_curso
CHANGE curso_possui_periodo_letivo curso_possui_periodo_letivo enum('Sim', 'Não') NOT NULL COMMENT 'É o período em que o curso forma novas turmas';

CREATE TABLE `acadperiodo__letivo_campus_curso` (
  `per_id` int(10) unsigned zerofill NOT NULL,
  `cursocampus_id` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`per_id`,`cursocampus_id`),
  KEY `fk_acadperiodo__letivo_campus_curso_campus_curso1_idx` (`cursocampus_id`),
  KEY `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1_idx` (`per_id`),
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_acadperiodo__letivo1` FOREIGN KEY (`per_id`) REFERENCES `acadperiodo__letivo` (`per_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_acadperiodo__letivo_campus_curso_campus_curso1` FOREIGN KEY (`cursocampus_id`) REFERENCES `campus_curso` (`cursocampus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



## A SEGUINTE CONSULTA SÓ DEVE SER USADA PARA EFEITO DE TESTE

REPLACE INTO acadperiodo__letivo_campus_curso (per_id, cursocampus_id)
SELECT DISTINCT
  periodo_campus.periodo,
  periodo_campus.campus
FROM
  (
    SELECT per_id periodo, cursocampus_id campus FROM acadperiodo__letivo JOIN campus_curso
  ) as periodo_campus;

## É NECESSÁRIO EXECUTAR ESSA MIGRATION PARA QUE MODALIDADE E TIPO DE CURSO FUNCIONEM

REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Não'  from acesso_modulos,
    (
      select 'Nível' as descricao, 'Matricula\\Controller\\AcadNivel' as controller UNION ALL
      select 'Modalidade' as descricao, 'Matricula\\Controller\\AcadModalidade' as controller from dual
    ) as func
  where mod_nome like 'Matricula';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Index' as descricao,'Leitura' as tipo,'N' as usr UNION ALL
      select 'search-for-json' as act,'Pesquisa' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Matricula\\Controller\\AcadNivel', 'Matricula\\Controller\\AcadModalidade');

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'search-for-json' as act,'Pesquisa' as descricao,'Leitura' as tipo,'N' as usr FROM dual
    ) as actions
  where func_controller IN('Vestibular\\Controller\\SelecaoEdicao');


REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Matricula\\Controller\\AcadNivel','Matricula\\Controller\\AcadModalidade', 'Vestibular\\Controller\\SelecaoEdicao'
  )
        and grup_nome LIKE 'admin%';



-- migrations/NOVA-INTERFACE-MATRICULA-2.sql


ALTER TABLE `acadgeral__aluno_curso`
ADD `origem_id` INT(11) UNSIGNED ZEROFILL NULL DEFAULT NULL,
ADD `alunocurso_pessoa_indicacao` VARCHAR(255) NULL,
ADD `alunocurso_mediador` VARCHAR(255) NULL,
ADD INDEX `fk_acadgeral__aluno_curso_acadgeral__cadastro_origem1_idx` (`origem_id` ASC);

ALTER TABLE `acadgeral__aluno_curso`
ADD CONSTRAINT `fk_acadgeral__aluno_curso_acadgeral__cadastro_origem1`
FOREIGN KEY (`origem_id`) REFERENCES `acadgeral__cadastro_origem` (`origem_id`);

-- migrations/S032-EXTRA-1-SQL.sql


REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
(
	select 'index' AS act, 'Matrícula' AS descricao, 'Escrita' as tipo,'N' as usr from dual UNION
	select 'matricula' AS act, 'Acadêmico' AS descricao, 'Escrita' as tipo,'N' as usr from dual
) as actions
where func_controller IN('Matricula\\Controller\\AcadgeralAlunoCurso');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
acesso_grupo
where func_controller IN(
		'Matricula\\Controller\\AcadgeralAlunoCurso'
)
and grup_nome LIKE 'admin%';

ALTER TABLE `acadperiodo__letivo`
  ADD `per_data_vencimento_inicial` DATETIME NOT NULL,
  ADD `per_data_vencimento_inicial_editavel` ENUM('Sim', 'Não') NOT NULL;

-- migrations/S033-C006-SQL-001.sql


SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS  atividadegeral__tipo;
SET FOREIGN_KEY_CHECKS=1;

 CREATE TABLE IF NOT EXISTS `atividadeperiodo__evento` (
  `evento_id` INT  primary KEY AUTO_INCREMENT ,
  `evento_secretaria` int(11) NOT NULL,
  `evento_nome` VARCHAR(45) NOT NULL,
  `evento_horas_validas` INT NOT NULL,
  `evento_lotacao` INT NULL,
  `evento_responsavel` VARCHAR(45) NULL,
  `evento_tipo_atividade` INT(10) UNSIGNED NOT NULL,
  `evento_data` DATETIME NULL,
   evento_periodo_letivo INT(10) UNSIGNED NOT NULL,
  `evento_tipo` ENUM('Interno', 'Externo') NOT NULL,
  `evento_descricao` TEXT NULL,
  `usuario_cadastro` INT(11) UNSIGNED ZEROFILL NOT NULL,
  INDEX `fk_atividadeperiodo__evento_atividadegeral__atividades1_idx` (`evento_tipo_atividade` ASC),
  INDEX `fk_atividadeperiodo__evento_acesso_pessoas1_idx` (`usuario_cadastro` ASC),
  INDEX `fk_atividadeperiodo__evento_secretaria_secretaria1_idx` (`evento_secretaria` ASC),
  INDEX `fk_atividadeperiodo__evento_periodo_letivo_idx` (`evento_periodo_letivo` ASC),

 CONSTRAINT `fk_atividadeperiodo__evento_secretaria_secretaria1_idx`
    FOREIGN KEY (`evento_secretaria`)
    REFERENCES `atividadegeral__secretaria` (`secretaria_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
 CONSTRAINT `fk_atividadeperiodo__evento_atividadegeral__atividades1_idx`
    FOREIGN KEY (`evento_tipo_atividade`)
    REFERENCES `atividadegeral__atividades` (`atividadeatividade_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
 CONSTRAINT `fk_atividadeperiodo__evento_acesso_pessoas1`
    FOREIGN KEY (`usuario_cadastro`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
CONSTRAINT `fk_atividadeperiodo__evento_periodo_letivo_idx`
    FOREIGN KEY (`evento_periodo_letivo`)
    REFERENCES `acadperiodo__letivo` (`per_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
    )ENGINE = InnoDB;

/*
  SE JÁ ESTIVER SIDO CRIADA APENAS SQL DE CORREÇÃO PARA RESTRIÇÃO DA CHAVE REFERENTE AO EVENTO_TIPO_ATIVIDADE
*/

ALTER TABLE `atividadeperiodo__evento` DROP FOREIGN KEY `fk_atividadeperiodo__evento_atividadegeral__tipo1`;
ALTER TABLE `atividadeperiodo__evento` ADD INDEX `fk_atividadeperiodo__evento_atividadegeral__atividades1_idx_idx` (`evento_tipo_atividade` ASC);
ALTER TABLE `atividadeperiodo__evento` DROP INDEX `fk_atividadeperiodo__evento_atividadegeral__tipo1_idx` ;
ALTER TABLE `atividadeperiodo__evento`
ADD CONSTRAINT `fk_atividadeperiodo__evento_atividadegeral__atividades1_idx`
FOREIGN KEY (`evento_tipo_atividade`)
REFERENCES `atividadegeral__atividades` (`atividadeatividade_id`)
ON DELETE RESTRICT
ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS `atividadeperiodo__aluno` (
  `aluno_atividade_id` INT PRIMARY KEY AUTO_INCREMENT,
  `usuario_cadastro` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `aluno_atividade_data_lancamento` DATETIME NOT NULL,
  `alunoper_id` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `evento_id` INT NOT NULL ,
  `aluno_atividade_data_realizacao` DATETIME NOT NULL,
  `aluno_atividade_horas_reais` INT NOT NULL,
  `aluno_atividade_horas_validas` INT NOT NULL,
  `aluno_atividade_observacao` VARCHAR(45) NULL,
  INDEX `fk_atividadeperiodo__aluno_acadperiodo__aluno1_idx` (`alunoper_id` ASC),
  INDEX `fk_atividadeperiodo__aluno_atividadeperiodo__evento1_idx` (`evento_id` ASC),
  INDEX `fk_atividadeperiodo__aluno_acesso_pessoas1_idx` (`usuario_cadastro` ASC),

  CONSTRAINT `fk_atividadeperiodo__aluno_acadperiodo__aluno1`
    FOREIGN KEY (`alunoper_id`)
    REFERENCES `acadperiodo__aluno` (`alunoper_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,

  CONSTRAINT `fk_atividadeperiodo__aluno_atividadeperiodo__evento1`
    FOREIGN KEY (`evento_id`)
    REFERENCES `atividadeperiodo__evento` (`evento_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,

  CONSTRAINT `fk_atividadeperiodo__aluno_acesso_pessoas1`
    FOREIGN KEY (`usuario_cadastro`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)

ENGINE = InnoDB;


/*
Se a tabela já estiver sido criada
*/

ALTER TABLE `atividadeperiodo__aluno`
CHANGE COLUMN `evento_data_lancamento` `aluno_atividade_data_lancamento` DATETIME NOT NULL;
ALTER TABLE `atividadeperiodo__aluno`
CHANGE COLUMN `evento_data_realizacao` `aluno_atividade_data_realizacao` DATETIME NOT NULL;
ALTER TABLE `atividadeperiodo__aluno`
CHANGE COLUMN `evento_horas_reais` `aluno_atividade_horas_reais` INT(11) NOT NULL;
ALTER TABLE `atividadeperiodo__aluno`
CHANGE COLUMN `evento_horas_validas` `aluno_atividade_horas_validas` INT(11) NOT NULL ;


/*
Se o modulo de Atividades Praticas não estiver visivel executar o script S024-C002-SQL-002.sql
*/



REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Atividades' as descricao, 'Atividades\\Controller\\AtividadePeriodoAluno' as controller from dual
    ) as func
  where mod_nome like 'Atividades Práticas';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr  union all
      select 'add' as act,'Criar' as descricao,'Escrita' as tipo,'N' as usr  union all
      select 'edit' as act,'Editar' as descricao,'Escrita' as tipo,'N' as usr  union all
      select 'remove' as act,'Remover' as descricao,'Escrita' as tipo,'N' as usr  from dual
    ) as actions
  where func_controller IN('Atividades\\Controller\\AtividadePeriodoAluno');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Atividades\\Controller\\AtividadePeriodoAluno'
  )
  and grup_nome LIKE 'admin%';


  REPLACE INTO `acesso_funcionalidades` (modulo, func_nome, func_controller, func_status, func_visivel)
  SELECT distinct acesso_modulos.id, func.descricao, func.controller,'Ativa','Sim'  from acesso_modulos,
    (
      select 'Eventos' as descricao, 'Atividades\\Controller\\AtividadePeriodoEvento' as controller from dual
    ) as func
  where mod_nome like 'Atividades Práticas';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'index' as act,'Listagem' as descricao,'Leitura' as tipo,'N' as usr  union all
      select 'add' as act,'Criar' as descricao,'Escrita' as tipo,'N' as usr  union all
      select 'edit' as act,'Editar' as descricao,'Escrita' as tipo,'N' as usr  union all
      select 'remove' as act,'Remover' as descricao,'Escrita' as tipo,'N' as usr  from dual
    ) as actions
  where func_controller IN('Atividades\\Controller\\AtividadePeriodoEvento');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Atividades\\Controller\\AtividadePeriodoEvento'
  )
  and grup_nome LIKE 'admin%';



/* ATENÇÃO FALTOU ESSA COLUNA E NÃO HÁ LOCALIZEI NAS MIGRATIONS, NÃO SEI SE AINDA ESTA EM USO OU VOU ADICIONADA RECENTEMENTE*/

ALTER TABLE `atividadeperiodo__aluno_nucleo`
ADD COLUMN `alunoperiodo_status` ENUM('Ativo', 'Inativo') NOT NULL DEFAULT 'Ativo' ;


-- migrations/S033-C016-SQL-001.sql


ALTER TABLE org_ies
  ADD COLUMN `ies_credenciamento` TEXT DEFAULT NULL;

ALTER TABLE campus_curso
  ADD COLUMN cursocampus_autorizacao TEXT DEFAULT NULL,
  ADD COLUMN cursocampus_reconhecimento TEXT DEFAULT NULL;

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'emissao-certificados'    AS act,
        'Emissão de certificados' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Matricula\\Controller\\Relatorios'
  );

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Matricula\\Controller\\Relatorios'
  )
        AND grup_nome LIKE 'Admin%';

-- migrations/S033-C018-SQL-001.sql


CREATE TABLE IF NOT EXISTS `acesso_pessoas_campus_curso` (
  `acesso_pessoas_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `cursocampus_id` INT ZEROFILL UNSIGNED NOT NULL,
  PRIMARY KEY (`acesso_pessoas_id`, `cursocampus_id`),
  INDEX `fk_acesso_pessoas_campus_curso_campus_curso1_idx` (`cursocampus_id` ASC),
  INDEX `fk_acesso_pessoas_campus_curso_acesso_pessoas1_idx` (`acesso_pessoas_id` ASC),
  CONSTRAINT `fk_acesso_pessoas_campus_curso_acesso_pessoas1`
    FOREIGN KEY (`acesso_pessoas_id`)
    REFERENCES `acesso_pessoas` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_acesso_pessoas_campus_curso_campus_curso1`
    FOREIGN KEY (`cursocampus_id`)
    REFERENCES `campus_curso` (`cursocampus_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- migrations/remessa-tables.sql


--
-- Estrutura da tabela `financeiro__remessa`
--

CREATE TABLE IF NOT EXISTS `financeiro__remessa` (
  `remessa_id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `confcont_id` int(11) NOT NULL,
  `remessa_codigo` int(11) NOT NULL,
  `remessa_data` datetime NOT NULL,
  `arq_id` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`remessa_id`),
  UNIQUE KEY `remessa_codigo_UNIQUE` (`remessa_codigo`,`confcont_id`),
  KEY `fk_financeiro__remessa_arquivo1_idx` (`arq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `financeiro__remessa_lote`
--
CREATE TABLE IF NOT EXISTS `financeiro__remessa_lote` (
  `remessa_lote_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bol_id` int(11) NOT NULL,
  `remessa_id` int(10) unsigned zerofill NOT NULL,
  `lote_codigo` int(11) NOT NULL,
  PRIMARY KEY (`remessa_lote_id`),
  UNIQUE KEY `uniq` (`bol_id`,`remessa_id`,`lote_codigo`),
  KEY `fk_boleto_financeiro__remessa_financeiro__remessa1_idx` (`remessa_id`),
  KEY `fk_boleto_financeiro__remessa_boleto1_idx` (`bol_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `financeiro__remessa`
--
ALTER TABLE `financeiro__remessa`
  ADD CONSTRAINT `fk_financeiro__remessa_arquivo1` FOREIGN KEY (`arq_id`) REFERENCES `arquivo` (`arq_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_financeiro_remessa_boleto_conf_conta1` FOREIGN KEY (`confcont_id`) REFERENCES `boleto_conf_conta` (`confcont_id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `financeiro__remessa_lote`
--
ALTER TABLE `financeiro__remessa_lote`
  ADD CONSTRAINT `fk_boleto_financeiro__remessa_boleto1` FOREIGN KEY (`bol_id`) REFERENCES `boleto` (`bol_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_boleto_financeiro__remessa_financeiro__remessa1` FOREIGN KEY (`remessa_id`) REFERENCES `financeiro__remessa` (`remessa_id`) ON UPDATE CASCADE;

update acad_curso set curso_possui_periodo_letivo='Não' where curso_id>=0;
update acad_curso set curso_unidade_medida='Mensal' where curso_id>=0;
-- update acad_curso set mod_id=2 where curso_id>=0;

-- migrations/S033-defeito#185-SQL-001.sql

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT distinct acesso_funcionalidades.id, actions.act, actions.descricao, actions.tipo, actions.usr  from acesso_funcionalidades,
    (
      select 'search' AS act, 'Dados para o datatables' AS descricao, 'Escrita' as tipo,'N' as usr from dual
    ) as actions
  where func_controller IN('Matricula\\Controller\\AcadperiodoTurma');

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Matricula\\Controller\\AcadperiodoTurma'
  )
        and grup_nome LIKE 'Admin%';

REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'baixa'    AS act,
        'Realizar baixa de multas' AS descricao,
        'Leitura'  AS tipo,
        'N'        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Biblioteca\\Controller\\Multa'
  );

REPLACE INTO `acesso_privilegios_grupo`  (`acesso_actions_id`, `acesso_grupo_id`)
  select acesso_actions.id, acesso_grupo.id from acesso_funcionalidades
    inner join acesso_actions on funcionalidade=acesso_funcionalidades.id,
    acesso_grupo
  where func_controller IN(
    'Biblioteca\\Controller\\Multa'
  )
        and grup_nome LIKE 'admin%';



REPLACE INTO `acesso_actions` (funcionalidade, act_nome, act_label, act_tipo, act_visivel_usuario_final)
  SELECT DISTINCT
    acesso_funcionalidades.id,
    actions.act,
    actions.descricao,
    actions.tipo,
    actions.usr
  FROM acesso_funcionalidades,
    (
      SELECT
        'busca-historico-escolar'       AS act,
        'Listar registros do histórico' AS descricao,
        'Escrita'                       AS tipo,
        'N'                             AS usr
      FROM dual
      UNION ALL
      SELECT
        'historico-remover'         AS act,
        'Remover item do histórico' AS descricao,
        'Escrita'                   AS tipo,
        'N'                         AS usr
      FROM dual
      UNION ALL
      SELECT
        'salva-historico-escolar'  AS act,
        'Salvar Item no histórico' AS descricao,
        'Escrita'                  AS tipo,
        'N'                        AS usr
      FROM dual
    ) AS actions
  WHERE func_controller IN (
    'Matricula\\Controller\\Aluno'
  );

REPLACE INTO `acesso_privilegios_grupo` (`acesso_actions_id`, `acesso_grupo_id`)
  SELECT
    acesso_actions.id,
    acesso_grupo.id
  FROM acesso_funcionalidades
    INNER JOIN acesso_actions ON funcionalidade = acesso_funcionalidades.id
    ,
    acesso_grupo
  WHERE func_controller IN (
    'Matricula\\Controller\\Aluno'
  )
        AND grup_nome LIKE 'Admin';

DROP VIEW IF EXISTS `view__financeiro_descontos_titulos_atualizada`;
CREATE VIEW `view__financeiro_descontos_titulos_atualizada` AS
  SELECT
    `fdt`.`desconto_titulo_id` AS `desconto_titulo_id`,
    `fdt`.`desconto_id` AS `desconto_id`,
    `fdt`.`titulo_id` AS `titulo_id`,
    `fdtp`.`desctipo_desconto_acumulativo` AS `desctipo_desconto_acumulativo`,
    `fdtp`.`desctipo_limita_vencimento`,
    `ft`.`titulo_data_vencimento` AS `titulo_data_vencimento`,
    `fd`.`desconto_valor` AS `desconto_valor`,
    `fd`.`desconto_percentual` AS `desconto_percentual`,
    `fd`.`aluno_id` AS `aluno_id`,
    `fdtp`.`desctipo_id` AS `desctipo_id`,
    `ft`.`titulo_valor` AS `titulo_valor`,
    ROUND(
        IF (
            (`ft`.`titulo_estado` != 'Aberto'),
            -- Se o t´titulo está fechado, pega o valor aplicado ou 0
            IF((`fdt`.`desconto_titulo_aplicado` = 'Sim'), `fdt`.`desconto_titulo_valor`, 0 ),
            IF(
            -- Verifica se o desconto é limitado ao vencimento e se o título já venceu
                (`fdtp`.`desctipo_limita_vencimento` = 'Sim' AND `ft`.`titulo_data_vencimento` < curdate()),
                0,
                -- Joga o valor do desconto ou calcula o desconto sobre o valor bruto do título
                IF((`fd`.`desconto_valor`>0), `fd`.`desconto_valor`, (`ft`.`titulo_valor` * (`fd`.`desconto_percentual` / 100)))
            )
        ),
        2
    ) AS `desconto_aplicacao_valor`
  FROM `financeiro__desconto_titulo` `fdt`
    JOIN `financeiro__desconto` `fd` ON `fdt`.`desconto_id` = `fd`.`desconto_id`
    JOIN `financeiro__desconto_tipo` `fdtp` ON `fd`.`desctipo_id` = `fdtp`.`desctipo_id`
    LEFT JOIN `financeiro__titulo` `ft` ON `fdt`.`titulo_id` = `ft`.`titulo_id`
  WHERE `fd`.`desconto_status` != 'Inativo' AND `fdt`.`desconto_titulo_situacao` = 'Deferido';